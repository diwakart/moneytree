<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

//Moke module for getting data
Route::group(['prefix' => 'v2', 'middleware' => ['auth:api']], function () {
	
	Route::get('/user/details.json', 'API\UserController@userDetails')->name('UserDetails');
	Route::get('/child/parents.json', 'API\UserController@getAllParentsData')->name('getAllParentsData');
	Route::get('/parent/child.json', 'API\UserController@getAllChildData')->name('getAllChildData');
	Route::post('/user/changePassword.json', 'API\UserController@changePassword')->name('changePassword');
	Route::post('/user/updateProfile.json', 'API\UserController@updateProfile')->name('updateProfile');
	Route::post('/user/profilePicUpdate.json', 'API\UserController@updateProfilePic')->name('updateProfilePic');
	Route::get('/user/childlist.json', 'API\UserController@childList')->name('childList');
	Route::get('/parents.json', 'API\UserController@getAllParents')->name('getAllParents');
	Route::get('/child.json', 'API\UserController@getAllChild')->name('getAllChild');
	Route::get('/child/dashboard.json', 'API\UserController@childDashboardDeatails')->name('childDashboardDeatails');
	Route::get('/user/request.json', 'API\UserController@addRequest')->name('addRequest');
	Route::post('/user/response.json', 'API\UserController@userResponse')->name('userResponse');
	Route::get('/child/tag.json', 'API\UserController@tagUser')->name('addChild');
	Route::get('/user/search.json', 'API\UserController@searchUser')->name('searchUser');
	Route::get('/user/notification.json', 'API\UserController@getAllRequest')->name('getAllRequest');
	Route::get('/user_notification.json', 'API\UserController@getAllNotifications')->name('getAllNotifications');
	Route::get('/user/payment.json', 'API\UserController@stripePayment')->name('stripePayment');
	Route::get('/user/packages.json', 'API\UserController@getAllPackages')->name('getAllPackages');
	Route::get('/user/passbook.json', 'API\UserController@getAllTransaction')->name('getAllTransaction');
	Route::get('/user/earn_list.json', 'API\UserController@getAllCreditTransaction')->name('getAllCreditTransaction');
	Route::get('/user/spent_list.json', 'API\UserController@getAllDebitTransaction')->name('getAllDebitTransaction');
	Route::post('/user/transaction_details.json', 'API\UserController@getTransactionDetails')->name('getTransactionDetails');
	Route::post('/user/refer_friend.json', 'API\UserController@referFriendRequest')->name('referFriendRequest');
	Route::get('/user/logout.json', 'API\UserController@userLogout')->name('userLogout');
	Route::get('/user/account_details.json','API\UserController@accountDetails')->name('getUserAccountDetails');
	Route::post('/user/point_disbursal.json', 'API\UserController@pointDisbursalToChild')->name('pointDisbursalToChild');
	Route::post('/parent/kid_setting.json', 'API\UserController@setKidDefaultSetting')->name('setKidDefaultSetting');
	
	/***************************FCM Integrated API *************************/
	
	Route::post('/user/device_register.json', 'API\UserController@deviceRegisterFcm')->name('deviceRegisterFcm');

	Route::get('/user/payment_sub.json','API\UserController@payment_sub')->name('payment_sub');
	
	Route::get('/user/product_redemption.json','API\UserController@userProductRedemption')->name('userProductRedemption');
	
	//Route::get('/user/redeem_history.json','API\UserController@userProductRedemption')->name('userProductRedemption');

	
	/***************************Task API Route ***************************/
	Route::get('/tasks.json', 'API\TaskController@index')->name('getAllTasks');
	Route::get('/tasks/{id}.json', 'API\TaskController@getTaskDetails')->name('getSingleTask');
	Route::get('/task/search.json', 'API\TaskController@searchTask')->name('searchTask');
	Route::post('/tasks/{id}/answer.json', 'API\TaskController@submit_task')->name('SubmitTask');
	Route::post('tasks/{id}/addBonusPoint.json', 'API\TaskController@addAppreciation')->name('addAppreciationOnTask');
	
	/*****************************Lesson API Route ****************************/
	Route::get('/lessons.json', 'API\LessonController@index')->name('getAllLessons');
	Route::post('/lesson/{id}/answer.json', 'API\LessonController@submit_lesson')->name('SubmitLesson');
	Route::get('/lessons_of_day.json', 'API\LessonController@getCurrentDayLesson')->name('getCurrentDayLesson');
	Route::get('/user/lesson_history.json', 'API\LessonController@lessonHistory')->name('lessonHistory');
	

	/***************************Categories API Route ********************/
	Route::get('/categories.json', 'API\CategoriesController@index')->name('getAllCategories');
	Route::get('/category/search.json', 'API\CategoriesController@searchCategory')->name('searchCategory');
	//Route::get('/categories.json', 'API\CategoriesController@index')->name('getAllCategories');
	Route::get('/categories/{id}.json', 'API\CategoriesController@getCategoryDetails')->name('getSingleCategories');
	Route::get('/categories/{id}/tasks.json', 'API\CategoriesController@getCategoryTaskDetails')->name('getCategoriesTasks');
	Route::post('categories/{id}/addBonusPoint.json', 'API\CategoriesController@addAppreciation')->name('addAppreciationOnCategories');

	/***************************Auction API Route ***********************/
	Route::get('/auction/categories.json', 'API\AuctionController@index')->name('getAllAuction');
	Route::get('/auction/products.json', 'API\AuctionController@getAllProducts')->name('getAllProducts');
	Route::get('/category/{id}/products.json', 'API\AuctionController@getAllCategoryProducts')->name('getAllCategoryProducts');
	Route::get('/user/addwishlist.json', 'API\AuctionController@addWishlistProduct')->name('addWishlistProduct');
	Route::get('/user/removewishlist.json', 'API\AuctionController@deleteWishlistProduct')->name('deleteWishlistProduct');
	Route::get('/user/wishlistproducts.json', 'API\AuctionController@getAllWishlistProducts')->name('getAllWishlistProducts');
	Route::get('/child/{id}/details.json', 'API\AuctionController@getAllChildDetails')->name('getAllChildDetails');
	Route::get('/child/{id}/wishlist.json', 'API\AuctionController@getChildWishlist')->name('getChildWishlist');
	Route::get('/child/{id}/spentlist.json', 'API\AuctionController@getChildSpentlist')->name('getChildSpentlist');
	Route::get('/child/{id}/earnlist.json', 'API\AuctionController@getChildEarnlist')->name('getChildEarnlist');
	Route::get('/auctions.json', 'API\AuctionController@getAllAuctions')->name('getAllAuctions');
	//Route::get('/{archieve}/auction.json', 'API\AuctionController@getAllAuctions')->name('getAllAuctions');
	Route::get('/user/auction_history.json', 'API\AuctionController@auctionHistory')->name('auctionHistory');

	/**********************************Auction Bidding API***************/

	Route::get('/auction/{id}/participate.json', 'API\BiddingController@userParticipate')->name('userParticipateInAuction');
	Route::get('/auction/{id}/check_wishlist.json', 'API\BiddingController@checkWishlistProducts')->name('checkWishlistProducts');
	Route::post('/product/set_priority.json', 'API\BiddingController@setPriorityOfProducts')->name('setPriorityOfProducts');
	Route::post('/user/bid.json', 'API\BiddingController@userRequestBid')->name('userRequestBid');
	Route::get('/user/{id}/bid_details.json', 'API\BiddingController@getUserBidDetails')->name('getUserBidDetails');
	Route::get('/auction/bid_details.json', 'API\BiddingController@auctionBidDetails')->name('auctionBidDetails');
	
	//Route::post('/login', 'API\APIController@login')->name('Login');
});

Route::any('/test', 'API\PassportController@test')->middleware('auth:api');

Route::post('/user/forgotPassword.json', 'API\UserController@forgotPassword')->name('forgotPassword');
Route::post('/check_availability.json', 'API\UserController@checkAvailability')->name('checkAvailability');
Route::post('/send/device_notification.json', 'API\UserController@sendUserNotification')->name('sendUserNotification');
Route::get('/check/userexist.json', 'API\UserController@checkUserExist')->name('checkUserExist');
Route::post('/register', 'API\UserController@create')->name('registerUser');
Route::get('/user/avtars.json', 'API\HomeController@getAllAvtars')->name('getAllAvtars');
Route::get('/save/api_routes.json', 'API\HomeController@save')->name('saveRoutes');

/****************************Country API request*************************/
	
	Route::get('/country_details.json','API\HomeController@getAllCountryDetails')->name('getAllCountryDetails');
// Route::get('/policy.html', 'API\HomeController@index')->name('policyPrivacy');
// Route::get('/terms.html', 'API\HomeController@terms')->name('policyTerms');


Route::get('/login', 'API\PassportController@login')->name('LoginUser');

