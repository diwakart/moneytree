<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/register/{digital_key}', 'Admin\HomeController@register')->name('index');
Route::post('/refer_child/store', 'Admin\HomeController@store')->name('registerReferChild');
Route::get('/', 'Admin\HomeController@index')->name('index');
Route::get('/policy', 'Admin\HomeController@policy')->name('getPolicy');
Route::get('/terms', 'Admin\HomeController@terms')->name('getTerms');
Route::get('/logout', 'Admin\HomeController@logout')->name('Logout');
Route::get('/admin/dologin', 'Admin\HomeController@login')->name('Login');
Route::post('/admin/change_pwd', 'Admin\HomeController@changePassword')->name('changePassword');
Route::post('/admin/settings', 'Admin\SettingController@saveSettings')->name('saveSettings');
Route::get('/admin/passbook', 'Admin\HomeController@getAdminTransaction')->name('getAdminTransaction');
Route::get('/admin/getAllAdminTransaction', 'Admin\HomeController@getAllAdminTransaction')->name('getAllAdminTransaction');
Route::get('/dashboard', 'Admin\HomeController@dashboard')->name('Dashboard')->middleware('isAdmin');

Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => '7',
        'redirect_uri' => 'http://127.0.0.1:8000/testPassport/callback',
        'response_type' => 'code',
        'scope' => '',
    ]);

    return redirect('http://127.0.0.1:8000/oauth/authorize?'.$query);
});

Route::get('/testPassport/callback', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->post('http://127.0.0.1:8000/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => '7',
            'client_secret' => 'n8vQIgZrt6CDk70qfFcAdpF2nRQrlbr1FFtGEkZ4',
            'redirect_uri' => 'http://127.0.0.1:8000/success',
            'code' => $request->code,
        ],
    ]);
    dd($response);

    return json_decode((string) $response->getBody(), true);
});

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/success', function(){
	echo 'Success';
});

Auth::routes();


Route::group(['prefix' => 'admin'], function () {

Route::get('/auction/send_notificattion', 'Admin\UsersController@sendNotification')->name('sendNotification');

Route::get('/cron/send_notificattion', 'Admin\NotificationController@index')->name('sendNotificationProgress');


/**************************************User Routes**********************/

Route::get('/users', 'Admin\UsersController@index')->name('getAllStudents');

Route::get('/child/profile/{id}', 'Admin\UsersController@childProfile')->name('childProfile');
Route::get('/parent/profile/{id}', 'Admin\UsersController@parentProfile')->name('parentProfile');
Route::get('/getAllParentChild/{id}', 'Admin\UsersController@getAllParentChild')->name('getAllParentChild');
Route::get('/getAllUserTransaction/{id}', 'Admin\UsersController@getAllUserTransaction')->name('getAllUserTransaction');
Route::get('/getAllChildTransaction/{id}', 'Admin\UsersController@getAllChildTransaction')->name('getAllChildTransaction');
Route::get('/getAllUserRequest/{id}', 'Admin\UsersController@getAllUserRequest')->name('getAllUserRequest');
Route::post('/add_user', 'Admin\UsersController@add')->name('add_user');
Route::post('/user_delete', 'Admin\UsersController@delete')->name('user_delete');
Route::post('/user_detail', 'Admin\UsersController@userDetails')->name('user_detail');
Route::post('/getUserID', 'Admin\UsersController@getUserID')->name('getUserID');
Route::post('/changePassword', 'Admin\UsersController@changePassword')->name('changePassword');
Route::get('/getAllUsers', 'Admin\UsersController@getAllUsers')->name('getAllUsers');
Route::get('/getAllCountriesData', 'Admin\HomeController@getAllCountriesData')->name('getAllCountriesData');
Route::post('/getCountryCode', 'Admin\HomeController@getCountryCode')->name('getCountryCode');
Route::post('/add_reward_points', 'Admin\UsersController@addRewardPoints')->name('add_reward_points');


/**************************************Categories Routes**********************/

Route::get('/categories', 'Admin\CategoriesController@index')->name('Category');
Route::get('category/change_status', 'Admin\CategoriesController@changeStatus')->name('changeStatus');
Route::post('/categoryDetails', 'Admin\CategoriesController@categoryDetails')->name('categoryDetails');
Route::get('/categories/{id}/tasks', 'Admin\CategoriesController@getCategoryTasks')->name('getCategoryTasks');
Route::get('/getAllCategoriesTasks/{id}', 'Admin\CategoriesController@getAllCategoriesTasks')->name('getAllCategoriesTasks');
Route::post('/add_category', 'Admin\CategoriesController@add')->name('add_category');
Route::post('/update_category', 'Admin\CategoriesController@update')->name('update_category');
Route::post('/category_delete', 'Admin\CategoriesController@delete')->name('category_delete');
Route::get('/category/add_points', 'Admin\CategoriesController@addPoints')->name('addPoints');
Route::get('/getAllCategories', 'Admin\CategoriesController@getAllCategories')->name('getAllCategories');

/**************************************Setting Routes**********************/

Route::get('/settings', 'Admin\SettingController@index')->name('AdminSettingPage');
Route::get('/system_log', 'Admin\SettingController@systemLogPage')->name('systemLogPage');
Route::get('/getAllEvents', 'Admin\SettingController@getAllEvents')->name('getAllEvents');

/**************************************Tasks Routes**********************/

Route::get('/tasks', 'Admin\TasksController@index')->name('TaskPage');
Route::get('/getAllTasks', 'Admin\TasksController@getAllTasks')->name('getAllTasks');
Route::post('/store_question_fill_blanks', 'Admin\TasksController@store_question_fill_blanks')->name('store_question_fill_blanks');
Route::post('/task_delete', 'Admin\TasksController@delete')->name('task_delete');
Route::post('/store_question_multi_choice', 'Admin\TasksController@store_question_multi_choice')->name('store_question_multi_choice');
Route::post('/store_question_arrange_order', 'Admin\TasksController@store_question_arrange_order')->name('store_question_arrange_order');
Route::post('/getquestionDetail', 'Admin\TasksController@getquestionDetail')->name('getquestionDetail');
Route::get('/tasks/add_points', 'Admin\TasksController@addPoints')->name('addPoints');

/*********************************Lesson Route*********************************/

Route::get('/lesson', 'Admin\LessonController@index')->name('LessonPage');
Route::get('/getAllLessons', 'Admin\LessonController@getAllLessons')->name('getAllLessons');
Route::post('/store_lesson_fill_blanks', 'Admin\LessonController@store_lesson_fill_blanks')->name('store_lesson_fill_blanks');
Route::post('/store_lesson_multi_choice', 'Admin\LessonController@store_lesson_multi_choice')->name('store_lesson_multi_choice');
Route::post('/store_lesson_arrange_order', 'Admin\LessonController@store_lesson_arrange_order')->name('store_lesson_arrange_order');
Route::post('/getLessonDetail', 'Admin\LessonController@getLessonDetail')->name('getLessonDetail');
Route::post('/lesson_delete', 'Admin\LessonController@delete')->name('lesson_delete');

/*********************************Package Routes*******************************/

Route::get('/packages', 'Admin\PackagesController@index')->name('PackagePage');
Route::get('/getAllPackages', 'Admin\PackagesController@getAllPackages')->name('getAllPackages');
Route::post('/add_package', 'Admin\PackagesController@add')->name('add_package');
Route::post('/package_detail', 'Admin\PackagesController@getPackageDetails')->name('getPackageDetails');
Route::post('/package_delete', 'Admin\PackagesController@delete')->name('deletePackage');

/*********************************Auction Routes*******************************/

Route::get('/auction', 'Admin\AuctionController@index')->name('AuctionCategories');
Route::get('/getAllAuctionCategories', 'Admin\AuctionController@getAllAuctionCategories')->name('getAllAuctionCategories');
Route::post('/add_auction_category', 'Admin\AuctionController@add')->name('add_auction_category');
Route::get('auction_category/change_status', 'Admin\AuctionController@changeStatus')->name('changeStatus');
Route::post('/auctionCategoryDetails', 'Admin\AuctionController@categoryDetails')->name('auctionCategoryDetails');
Route::post('/update_auctionCategory', 'Admin\AuctionController@update')->name('update_auctionCategory');
Route::post('/delete_category', 'Admin\AuctionController@delete')->name('delete_category');
Route::get('/auction/{id}/products', 'Admin\AuctionController@auctionProducts')->name('auctionProducts');

Route::get('/auction/getAllProducts/{id}', 'Admin\AuctionController@getAllProducts')->name('getAllProducts');
Route::post('/auction/add_product/{id}', 'Admin\AuctionController@addProducts')->name('addProducts');
Route::post('/auction/edit_product/{id}', 'Admin\AuctionController@editProducts')->name('editProducts');
Route::post('/auction/product_details', 'Admin\AuctionController@productDetails')->name('productDetails');
Route::post('/auction/delete_products', 'Admin\AuctionController@deleteProduct')->name('delete_products');

/************************************Bidding Routes*************************/

Route::get('/bidding', 'Admin\BiddingController@index')->name('BiddingPage');
Route::get('/auction/{id}/users', 'Admin\BiddingController@viewAllBiddingUsers')->name('viewAllBiddingUsers');
Route::get('/auction/{id}/getAllBiders', 'Admin\BiddingController@getAllBiders')->name('getAllBiders');
Route::get('/getAllAuctions', 'Admin\BiddingController@getAllAuctions')->name('getAllAuctions');
Route::post('/add_auction', 'Admin\BiddingController@addAuction')->name('addAuction');
Route::post('/update_auction', 'Admin\BiddingController@update')->name('update_auction');
Route::post('/auctionDetails', 'Admin\BiddingController@auctionDetails')->name('auctionDetails');
Route::post('/delete_auction', 'Admin\BiddingController@delete')->name('delete');
Route::get('auction/change_status', 'Admin\BiddingController@changeStatus')->name('changeStatus');

/****************************Countries Routes*******************************/

Route::get('/countries', 'Admin\CountriesController@index')->name('CountriesPage');
Route::get('/getAllCountries', 'Admin\CountriesController@getAllCountries')->name('getAllCountries');
Route::post('/add_country', 'Admin\CountriesController@add')->name('add_country');
Route::post('/country_detail', 'Admin\CountriesController@getCountryDetails')->name('getCountryDetails');
Route::post('/country_delete', 'Admin\CountriesController@delete')->name('deleteCountry');

});


