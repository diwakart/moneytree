<ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
	<li class="m-menu__item "  aria-haspopup="true">
		<a  href="{{ url('/dashboard') }}" class="m-menu__link ">
			<span class="m-menu__item-here"></span>
			<span class="m-menu__link-text">
				Dashboard
			</span>
		</a>
	</li>
	<li class="m-menu__item "  aria-haspopup="true">
		<a  href="{{ url('/admin/countries') }}" class="m-menu__link ">
			<span class="m-menu__item-here"></span>
			<span class="m-menu__link-text">
				Countries
			</span>
		</a>
	</li>
	<li class="m-menu__item"  aria-haspopup="true">
		<a  href="{{ url('/admin/users') }}" class="m-menu__link ">
			<span class=""></span>
			<span class="m-menu__link-text">
				Users
			</span>
		</a>
	</li>
	<li class="m-menu__item"  aria-haspopup="true">
		<a  href="{{ url('/admin/categories') }}" class="m-menu__link ">
			<span class=""></span>
			<span class="m-menu__link-text">
				Categories
			</span>
		</a>
	</li>
	<li class="m-menu__item"  aria-haspopup="true">
		<a  href="{{ url('/admin/tasks') }}" class="m-menu__link ">
			<span class=""></span>
			<span class="m-menu__link-text">
				Tasks
			</span>
		</a>
	</li>
	<li class="m-menu__item"  aria-haspopup="true">
		<a  href="{{ url('/admin/lesson') }}" class="m-menu__link ">
			<span class=""></span>
			<span class="m-menu__link-text">
				Lessons
			</span>
		</a>
	</li>
	<li class="m-menu__item"  aria-haspopup="true">
		<a  href="{{ url('/admin/packages') }}" class="m-menu__link ">
			<span class=""></span>
			<span class="m-menu__link-text">
				Packages
			</span>
		</a>
	</li>
	
	<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel"  data-menu-submenu-toggle="click" aria-haspopup="true">
		<a  href="#" class="m-menu__link m-menu__toggle">
			<span class="m-menu__item-here"></span>
			<span class="m-menu__link-text">
				Auction
			</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
			<i class="m-menu__ver-arrow la la-angle-right"></i>
		</a>
		<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
			<span class="m-menu__arrow m-menu__arrow--adjust"></span>
			<ul class="m-menu__subnav">
				<li class="m-menu__item "  aria-haspopup="true">
					<a  href="{{ url('/admin/auction') }}" class="m-menu__link ">
						<i class="m-menu__link-icon flaticon-open-box"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">
									Auction Categories
								</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item "  aria-haspopup="true">
					<a  href="{{ url('/admin/bidding') }}" class="m-menu__link ">
						<i class="m-menu__link-icon fa fa-gavel"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">
									Auction Bidding
								</span>
							</span>
						</span>
					</a>
				</li>
			</ul>
		</div>
	</li>
	<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel"  data-menu-submenu-toggle="click" aria-haspopup="true">
		<a  href="#" class="m-menu__link m-menu__toggle">
			<span class="m-menu__item-here"></span>
			<span class="m-menu__link-text">
				Settings
			</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
			<i class="m-menu__ver-arrow la la-angle-right"></i>
		</a>
		<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
			<span class="m-menu__arrow m-menu__arrow--adjust"></span>
			<ul class="m-menu__subnav">
				<li class="m-menu__item "  aria-haspopup="true">
					<a  href="{{ url('/admin/settings') }}" class="m-menu__link ">
						<i class="m-menu__link-icon fa fa-gears"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">
									Admin Settings
								</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item "  aria-haspopup="true">
					<a  href="{{ url('/admin/system_log') }}" class="m-menu__link ">
						<i class="m-menu__link-icon fa fa-gear"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">
									System Log Settings
								</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item "  aria-haspopup="true">
					<a  href="{{ url('/permission') }}" class="m-menu__link ">
						<i class="m-menu__link-icon fa fa-user-plus"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">
									Permission Settings
								</span>
							</span>
						</span>
					</a>
				</li>
			</ul>
		</div>
	</li>
</ul>