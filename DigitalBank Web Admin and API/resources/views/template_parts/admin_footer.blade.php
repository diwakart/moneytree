      <!-- end::Body -->
<!-- begin::Footer -->
      <div class="modal fade" id="ChangePasswordAdminModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                   
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form name="password_form" id="password_form">
            <div class="modal-body">
              {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name" class="form-control-label">
                            Password:
                        </label>
                        <input type="password" class="form-control" id="change_pwd" name="change_pwd" accept=".csv">
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <button type="submit" class="btn btn-primary" id="ChangePwd">
                    Save changes
                </button>
            </div>
            </form>
        </div>
    </div>
    </div>
      <footer class="m-grid__item m-footer ">
        <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
          <div class="m-footer__wrapper">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
              <div class="m-stack__item m-stack__item--center m-stack__item--middle m-stack__item--last">
                <span class="m-footer__copyright">
                  2018 &copy;
                  <!-- <a href="" class="m-link"> -->
                    Digital Bank
                  <!-- </a> -->
                </span>
              </div>
<!--               <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                  <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                      <span class="m-nav__link-text">
                        About
                      </span>
                    </a>
                  </li>
                  <li class="m-nav__item">
                    <a href="#"  class="m-nav__link">
                      <span class="m-nav__link-text">
                        Privacy
                      </span>
                    </a>
                  </li>
                  <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                      <span class="m-nav__link-text">
                        T&C
                      </span>
                    </a>
                  </li>
                  <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                      <span class="m-nav__link-text">
                        Purchase
                      </span>
                    </a>
                  </li>
                  <li class="m-nav__item m-nav__item--last">
                    <a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">
                      <i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
                    </a>
                  </li>
                </ul>
              </div> -->
            </div>
          </div>
        </div>
      </footer>
      <!-- end::Footer -->
    </div>
    <!-- end:: Page -->
                  <!-- begin::Quick Sidebar -->
    <div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">
      <div class="m-quick-sidebar__content m--hide">
        <span id="m_quick_sidebar_close" class="m-quick-sidebar__close">
          <i class="la la-close"></i>
        </span>
      </div>
    </div>
    <!-- end::Quick Sidebar -->         
      <!-- begin::Scroll Top -->
    <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
      <i class="la la-arrow-up"></i>
    </div>
    <!-- begin::Quick Nav --> 
      <!--begin::Base Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{url('/assets/admin/js/dashboard.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/demo/demo5/base/scripts.bundle.js')}}" type="text/javascript"></script>
    <!--end::Base Scripts -->   
        <!--begin::Page Vendors -->
    <script src="{{url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
    <!--end::Page Vendors -->  
        <!--begin::Page Snippets -->
    <script src="{{url('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <!--end::Page Snippets -->
  </body>
  <!-- end::Body -->
</html>