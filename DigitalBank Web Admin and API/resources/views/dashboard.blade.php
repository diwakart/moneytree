<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Dashboard
		</title>
		@extends('layouts.admin')
    	@section('content')
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						<!-- BEGIN: Subheader -->
						<div class="m-subheader ">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title ">
										Dashboard
									</h3>
								</div>
								<!-- <div class="pull-right" style="margin-top: 20px;">
									<button class="btn btn-primary m-btn m-btn--icon" data-toggle="modal" data-target="#ChangePasswordAdminModal">
										<span>
											<i class="la la-key"></i>
											<span>
												Change Password
											</span>
										</span>
									</button>
								</div> -->
								<div>
								</div>
							</div>
						</div>
						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row">
								<div class="col-xl-12">
								    <div class="pull-right" style="margin: 0px 20px;">
									<a class="btn btn-primary m-btn m-btn--icon" href="http://quizme.com.my/quiz/login/{{$user->quiz_key}}">
										<span>
											<i class="fa fa-user"></i>
											<span>
												Quiz Admin
											</span>
										</span>
									</a>
									</div>
									<div class="pull-right" style="margin: 0px 20px;">
									<button class="btn btn-primary m-btn m-btn--icon" data-toggle="modal" data-target="#ChangePasswordAdminModal">
										<span>
											<i class="la la-key"></i>
											<span>
												Change Password
											</span>
										</span>
									</button>
									</div>
									<div class="pull-right" style="margin: 0px 20px;">
									<a href="{{url('/admin/passbook')}}" class="btn btn-primary">
										<span>
											<i class="la la-key"></i>
											<span>
												Admin Passbook
											</span>
										</span>
									</a>
									</div>
								</div>
								<div class="col-xl-12">
								<div class="m-portlet ">
    								<div class="m-portlet__body  m-portlet__body--no-padding">
        
        								<div class="row m-row--no-padding m-row--col-separator-xl">
        									<div class="col-md-12 col-lg-6 col-xl-4 card_height">
                							<!--begin::Total Users-->
								                <div class="m-widget24">
								                    <div class="m-widget24__item">
								                    	<a href="{{url('admin/users')}}">
								                        <h4 class="m-widget24__title">
								                            Users(s)
								                        </h4>
								                    	</a>
								                        <br>
								                        <span class="m-widget24__desc">
								                            Total Users
								                        </span>
								                        <span class="m-widget24__stats m--font-brand">
								                            {{$count_users}}
								                        </span>
								                        <div class="m--space-10"></div>
								                    </div>
								                </div>
							                <!--end::Total Profit-->
							            	</div>
							            	<div class="col-md-12 col-lg-6 col-xl-4 card_height">
                							<!--begin::Total Users-->
								                <div class="m-widget24">
								                    <div class="m-widget24__item">
								                        <h4 class="m-widget24__title">
								                            Childs(s)
								                        </h4>
								                        <br>
								                        <span class="m-widget24__desc">
								                            Total Child
								                        </span>
								                        <span class="m-widget24__stats m--font-brand">
								                            {{$count_child}}
								                        </span>
								                        <div class="m--space-10"></div>
								                    </div>
								                </div>
							                <!--end::Total Profit-->
							            	</div>
							            	<div class="col-md-12 col-lg-6 col-xl-4 card_height">
                							<!--begin::Total Users-->
								                <div class="m-widget24">
								                    <div class="m-widget24__item">
								                        <h4 class="m-widget24__title">
								                            Parents(s)
								                        </h4>
								                        <br>
								                        <span class="m-widget24__desc">
								                            Total Parents
								                        </span>
								                        <span class="m-widget24__stats m--font-brand">
								                            {{$count_parents}}
								                        </span>
								                        <div class="m--space-10"></div>
								                    </div>
								                </div>
							                <!--end::Total Profit-->
							            	</div>
							            	
							            </div>
							        </div>
							    </div>
							</div>
                                <div class="col-xl-12">
                                    <div class="m-portlet ">
                                        <div class="m-portlet__body  m-portlet__body--no-padding">
                                        
                                            <div class="row m-row--no-padding m-row--col-separator-xl">
                                                
							            	<div class="col-md-12 col-lg-6 col-xl-4 card_height">
                							<!--begin::Total Users-->
								                <div class="m-widget24">
								                    <div class="m-widget24__item">
								                    	<a href="{{url('admin/categories')}}">
								                        <h4 class="m-widget24__title">
								                            Categories(s)
								                        </h4>
								                    	</a>
								                        <br>
								                        <span class="m-widget24__desc">
								                            Total Categories
								                        </span>
								                        <span class="m-widget24__stats m--font-brand">
								                            {{$count_categories}}
								                        </span>
								                        <div class="m--space-10"></div>
								                    </div>
								                </div>
							                <!--end::Total Profit-->
							            	</div>
							            	<div class="col-md-12 col-lg-6 col-xl-4 card_height">
                							<!--begin::Total Users-->
								                <div class="m-widget24">
								                    <div class="m-widget24__item">
								                    	<a href="{{url('admin/tasks')}}">
								                        <h4 class="m-widget24__title">
								                            Tasks(s)
								                        </h4>
								                    	</a>
								                        <br>
								                        <span class="m-widget24__desc">
								                            Total Tasks
								                        </span>
								                        <span class="m-widget24__stats m--font-brand">
								                            {{$count_tasks}}
								                        </span>
								                        <div class="m--space-10"></div>
								                    </div>
								                </div>
							                <!--end::Total Profit-->
							            	</div>
                                            <div class="col-md-12 col-lg-6 col-xl-4 card_height">
                							<!--begin::Total Users-->
								                <div class="m-widget24">
								                    <div class="m-widget24__item">
								                    	<a href="{{url('admin/lesson')}}">
								                        <h4 class="m-widget24__title">
								                            Lesson(s)
								                        </h4>
								                    	</a>
								                        <br>
								                        <span class="m-widget24__desc">
								                            Total Lessons
								                        </span>
								                        <span class="m-widget24__stats m--font-brand">
								                            {{$count_lessons}}
								                        </span>
								                        <div class="m--space-10"></div>
								                    </div>
								                </div>
							                <!--end::Total Profit-->
							            	</div>    
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
							<!--End::Main Portlet-->   
							<!--Begin::Main Portlet-->
							
							<!--End::Main Portlet-->
						</div>
					</div>
				</div>
			</div>
		@endsection
			
