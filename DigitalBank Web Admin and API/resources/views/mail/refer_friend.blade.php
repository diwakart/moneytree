@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            DIGITAL BANK MANAGEMENT
        @endcomponent
    @endslot

    {{-- Body --}}
        # Hello {{$to_name}}
        
        Your Friend {{$from_name}} Requested to register in digital bank.
        For Register click on given Link.
        {{$refer_url}}
        Thanks Moneytree Asia,
        Malaysia

    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy here -->
        @endcomponent
    @endslot


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} DIGITAL BANK. All rights reserved.
        @endcomponent
    @endslot
@endcomponent
