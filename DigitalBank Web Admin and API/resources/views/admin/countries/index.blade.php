<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Countries
		</title>
		@extends('layouts.admin')
    	@section('content')
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						<!-- BEGIN: Subheader -->
						<div class="m-subheader ">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title m-subheader__title--separator">
										All Countries
									</h3>
									<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							            <li class="m-nav__item m-nav__item--home">
							                <a href="{{url('/dashboard')}}" class="m-nav__link m-nav__link--icon">
							                  <i class="m-nav__link-icon la la-home"></i>
							                </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a href="{{url('/admin/countries')}}" class="m-nav__link">
							                <span class="m-nav__link-text">
							                  All Countries
							                </span>
							              </a>
							            </li>
									</ul>
								</div>
								<div>
								</div>
							</div>
						</div>
						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row">
								<div class="col-xl-12">
									<div class="m-portlet m-portlet--mobile">
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption">
												<div class="m-portlet__head-title float-left col-xl-2">
													<h3 class="m-portlet__head-text">Country</h3>
												</div>
												<div class="float-left col-xl-3">
													<div class="m-input-icon m-input-icon--left" style="margin-top: 15px;">
														<input type="text" class="form-control m-input m-input--solid" placeholder="Search Country Name" id="generalSearch">
														<span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
													</div>
												</div>
											</div>
											<div class="pull-right" style="margin-top: 20px;">
												<button class="btn btn-primary m-btn m-btn--icon" id="addCountry">
													<span>
														<i class="la la-plus"></i>
														<span>
															Add Countries
														</span>
													</span>
												</button>
											</div>
										</div>
										<div class="m-portlet__body">
											<div class="country_datatable" id="local_data">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--End::Main Portlet-->   
							<!--Begin::Main Portlet-->
							
							<!--End::Main Portlet-->
						</div>
					</div>
				</div>
			</div>
		<div class="modal fade" id="addCountryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
		    <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title" id="exampleModalLabel">
		               Add Country
		            </h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">
		                    &times;
		                </span>
		            </button>
		        </div>
		        <div class="modal-body">
		          {{ csrf_field() }}
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Name *:
		                    </label>
		                    <input type="text" class="form-control" id="country_name" name="country_name" placeholder="Please enter country name">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Short Code *:
		                    </label>
		                    <input type="text" class="form-control" id="short_code" name="short_code" placeholder="Please enter short code max 2 character" maxlength="2">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Country Code *:
		                    </label>
		                    <input type="text" class="form-control" id="country_code" name="country_code" placeholder="Please enter country code max 6 character" maxlength="6">
		                </div>		            
		        </div>
		        <div class="modal-footer">
		            <button type="button" class="btn btn-secondary" data-dismiss="modal">
		                Close
		            </button>
		            <button type="button" class="btn btn-primary" id="addCountryBtn">
		                Save changes
		            </button>
		        </div>
		    </div>
		</div>
		</div>
			<!-- Modal for success response -->
	    <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                
	                <form name="fm-student" id="arrange-order-form1">
	                <div class="modal-body">
	                    <h5 id="ResponseHeading"></h5>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadCountryDatatable">
	                        OK
	                    </button>
	                </div>
	                </form>
	            </div>
	        </div>
	    </div>
		@endsection
		@section('js')
		<script type="text/javascript" src="{{url('/assets/admin/js/countries.js')}}"></script>
		<script type="text/javascript">
			$('#addCountry').click(function(e){
			    e.preventDefault();
			    $('#addCountryModal').modal('show');
			    $('#addCountryModal').data('id','');
			    $('#country_name').val('');
			    $('#short_code').val('');
			    $('#country_code').val(''); 
		  	});
		</script>
		@endsection
			
