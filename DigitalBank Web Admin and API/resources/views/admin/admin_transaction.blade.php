<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Passbook
		</title>
		@extends('layouts.admin')
    	@section('content')
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						<!-- BEGIN: Subheader -->
						<div class="m-subheader ">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title m-subheader__title--separator">
										Admin Transactions
									</h3>
									<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							            <li class="m-nav__item m-nav__item--home">
							                <a href="{{url('/dashboard')}}" class="m-nav__link m-nav__link--icon">
							                  <i class="m-nav__link-icon la la-home"></i>
							                </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a class="m-nav__link">
							                <span class="m-nav__link-text">
							                  {{$data->name}} Passbook
							                </span>
							              </a>
							            </li>
									</ul>
								</div>
								<div>
								</div>
							</div>
						</div>
						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row">
								<div class="col-xl-12">
									<div class="m-portlet m-portlet--mobile col-xl-12">
										<div class="m-portlet__head col-xl-12">
											<div class="m-portlet__head-caption col-xl-12">
												<div class="m-portlet__head-title">
													<h3 class="m-portlet__head-text">Passbook</h3>
												</div>
												<!--<div>
													<select class="form-control mx-auto" id="categorylist">
												    <option value="">Select Month</option>
  													</select>
												</div>-->
											</div>
										</div>
										<div class="m-portlet__body">
											<div class="passbook_datatable" id="local_data">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--End::Main Portlet-->   
							<!--Begin::Main Portlet-->
							
							<!--End::Main Portlet-->
						</div>
					</div>
				</div>
			</div>
		
			<!-- Modal for success response -->
		@endsection
		@section('js')
		<script type="text/javascript" src="{{url('/assets/admin/js/admin_passbook.js')}}"></script>
		@endsection
			
