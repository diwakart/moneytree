<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Auction
		</title>
		@extends('layouts.admin')
    	@section('content')
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						<!-- BEGIN: Subheader -->
						<div class="m-subheader">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title m-subheader__title--separator">
										All Auction Users
									</h3>
									<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							            <li class="m-nav__item m-nav__item--home">
							                <a href="{{url('/dashboard')}}" class="m-nav__link m-nav__link--icon">
							                  <i class="m-nav__link-icon la la-home"></i>
							                </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a href="{{url('/admin/bidding')}}" class="m-nav__link">
							                <span class="m-nav__link-text">
							                  All Auctions
							                </span>
							              </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a class="m-nav__link">
							                <span class="m-nav__link-text">
							                 {{$data->category_name}}
							                </span>
							              </a>
							            </li>
									</ul>
								</div>
								<div>
								</div>
							</div>
						</div>
						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row">
								<div class="col-xl-12">
									<div class="m-portlet m-portlet--mobile">
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption col-xl-4">
												<div class="m-portlet__head-title">
													<h3 class="m-portlet__head-text">Bidding Details</h3>
												</div>
											</div>
										</div>
										<div class="m-portlet__body">
											<div class="users_datatable" id="local_data" data_id="{{1}}">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--End::Main Portlet-->   
							<!--Begin::Main Portlet-->
							
							<!--End::Main Portlet-->
						</div>
					</div>
				</div>
			</div>
		@endsection
		@section('js')
		<script type="text/javascript" src="{{url('/assets/admin/js/auction_users.js')}}"></script>
		<script type="text/javascript">
			function editProducts(id){
		        var path = "../product_details";
		        $.ajax({
		          type: "POST",
		          url: path,
		          data: {
		            id: id
		          },
		           headers: {
		          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		          success: function(result){
		            //console.log(result);
		            var res = $.parseJSON(result);

		            if(res.status == 'error'){

		            }else{
		              var data = $.parseJSON(JSON.stringify(res.message));
		              $('#EditProductModal').data('id',data.id);
		              $('#EditProductModal').find('.modal-title').html('Update Product ' + data.product_name);
		              $('#name').val(data.product_name);
		              $('#price').val(data.product_price);
		              $('#description').val(data.description);
		              $('#category_id').val(data.auction_category_id);
		              $('#product_image').val('');
		              if(data.product_image!='')
		              {
		              	var image_input = '<label for="image_selected" class="form-control-label" style="width:100px;">Image:</label><img src="../../../storage/app/products/'+data.product_image+'" height="100" width="100"></img>';
						$("#image_selected").html(image_input);
						$("#image_selected").show();
		              }
		              $('#EditProductModal').modal('show');
		              //datatable.reload();
		            }
		          },
		          error: function(){
		            alert("Error");
		          }
		        }); 
		  	}

		</script>
		@endsection
			
