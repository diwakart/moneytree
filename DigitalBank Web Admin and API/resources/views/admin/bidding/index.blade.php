<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Auction
		</title>
		@extends('layouts.admin')
    	@section('content')
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						<!-- BEGIN: Subheader -->
						<div class="m-subheader">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title m-subheader__title--separator">
										All Auctions
									</h3>
									<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							            <li class="m-nav__item m-nav__item--home">
							                <a href="{{url('/dashboard')}}" class="m-nav__link m-nav__link--icon">
							                  <i class="m-nav__link-icon la la-home"></i>
							                </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a href="{{url('/admin/auction')}}" class="m-nav__link">
							                <span class="m-nav__link-text">
							                  All Auctions
							                </span>
							              </a>
							            </li>
									</ul>
								</div>
								<div>
								</div>
							</div>
						</div>
						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row">
								<div class="col-xl-12">
									<div class="m-portlet m-portlet--mobile">
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption col-xl-4">
												<div class="m-portlet__head-title float-left col-xl-2">
													<h3 class="m-portlet__head-text">Auction</h3>
												</div>
												<div class="float-left col-xl-3" style="margin-top: 15px;">
													<select class="form-control mx-auto" id="country_id" name="country_id">
                        		                        <option value="">Select Country</option>
                        		                         @inject('countries','App\Model\Countries')
                                         @php($countries = $countries->get())
            						    @foreach($countries as $item)
                            							<option value="{{$item->id}}">{{$item->country_name}}</option>
                            							@endforeach
                        							</select>
												</div>
											</div>
											<!-- <div class="m-portlet__head-caption col-xl-4">
												<button class="btn btn-primary m-btn m-btn--icon" id="addPoints" data-toggle="modal" data-target="#AddPointsModal" style="margin-top: 7px;">
													<span>
														<i class="la la-plus"></i>
														<span>
															Add Points
														</span>
													</span>
												</button>
											</div> -->
											<div class="pull-right" style="margin-top: 20px;">
												<button class="btn btn-primary m-btn m-btn--icon" data-toggle="modal" data-target="#AddAuctionModal">
													<span>
														<i class="fa fa-gavel"></i>
														<span>
															Add Auction
														</span>
													</span>
												</button>
											</div>
										</div>
										<div class="m-portlet__body">
											<div class="auction_datatable" id="local_data">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--End::Main Portlet-->   
							<!--Begin::Main Portlet-->
							
							<!--End::Main Portlet-->
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="AddAuctionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
		    <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title" id="exampleModalLabel">
		               Add Auction
		            </h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">
		                    &times;
		                </span>
		            </button>
		        </div>
		        <div class="modal-body">
		          {{ csrf_field() }}
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Category Name *:
		                    </label>
		                    <select class="form-control" id="fill_category_id" name="fill_category_id">
							<option value="">Select Category</option>
							@foreach($categories as $item)
							<option value="{{$item->id}}">{{$item->category_name}}</option>
							@endforeach
							</select>
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Auction Time <button type="button" class="btn btn-primary" id="auction_range">Select Range</button>
		                    </label>
		                    <div class='input-group pull-right' id='start_date'>
								<input type='text' class="form-control m-input" readonly  placeholder="Selected Start Date" id="start_time">
								<span class="input-group-addon">
									<i class="la la-calendar-check-o"></i>
								</span>
							</div>
							<div class='input-group pull-right' id='end_date' style="margin-top: 20px; margin-bottom: 10px;">
								<input type='text' class="form-control m-input" readonly  placeholder="Select Expiration Date" id="end_time">
								<span class="input-group-addon">
									<i class="la la-calendar-check-o"></i>
								</span>
							</div>
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Per Bid Points *:
		                    </label>
		                    <input type='text' class="form-control" placeholder="Please enter per bid value" id="single_bid_value" name="single_bid_value">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Increment Bid Value in Minute *:
		                    </label>
		                    <select class="form-control" id="incremented_time" name="incremented_time">
								<option value="">Select Incremented time</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
		                </div>

		        </div>
		        <div class="modal-footer">
		            <button type="button" class="btn btn-secondary" data-dismiss="modal">
		                Close
		            </button>
		            <button type="button" class="btn btn-primary" id="addBtn">
		                Save changes
		            </button>
		        </div>
		    </div>
		</div>
		</div>
			<div class="modal fade" id="EditAuctionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
		    <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title" id="exampleModalLabel">
		               
		            </h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">
		                    &times;
		                </span>
		            </button>
		        </div>
		        <form name="fm-student" id="edit_auction_form">
		        <div class="modal-body">
		          {{ csrf_field() }}
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Category Name:
		                    </label>
		                    <select class="form-control" id="fill_category_id_edit" name="fill_category_id_edit">
							<option value="">Select Category</option>
							@foreach($categories as $item)
							<option value="{{$item->id}}">{{$item->category_name}}</option>
							@endforeach
							</select>
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Auction Time * <button type="button" class="btn btn-primary" id="auction_range_edit">Select Range</button>
		                    </label>
		                    <div class='input-group pull-right' id='start_date_edit'>
								<input type='text' class="form-control m-input" readonly  placeholder="Selected Start Date" id="start_time_edit">
								<span class="input-group-addon">
									<i class="la la-calendar-check-o"></i>
								</span>
							</div>
							<div class='input-group pull-right' id='end_date_edit' style="margin-top: 20px; margin-bottom: 10px;">
								<input type='text' class="form-control m-input" readonly  placeholder="Select Expiration Date" id="end_time_edit">
								<span class="input-group-addon">
									<i class="la la-calendar-check-o"></i>
								</span>
							</div>
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Per Bid Points *:
		                    </label>
		                    <input type='text' class="form-control" placeholder="Please enter per bid value" id="single_bid_value_edit" name="single_bid_value">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Increment Bid Value in Minute *:
		                    </label>
		                    <select class="form-control" id="incremented_time_edit" name="incremented_time_edit">
								<option value="">Select Incremented time</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
		                </div>

		        </div>
		        <div class="modal-footer">
		            <button type="button" class="btn btn-secondary" data-dismiss="modal">
		                Close
		            </button>
		            <button type="submit" class="btn btn-primary" id="Updatebtn">
		                Save changes
		            </button>
		        </div>
		        </form>
		    </div>
		</div>
		</div>
		<div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		        <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                
		                <form name="fm-student" id="arrange-order-form">
		                <div class="modal-body">
		                    <h5 id="ResponseHeading"></h5>
		                </div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadAuctionDatatable">
		                        OK
		                    </button>
		                </div>
		                </form>
		            </div>
		        </div>
		    </div>
		@endsection
		@section('js')
		<script type="text/javascript" src="{{url('/assets/admin/js/auction_bidding.js')}}"></script>
		<script src="{{url('assets/demo/default/custom/components/forms/widgets/bootstrap-daterangepicker.js')}}" type="text/javascript"></script>
		<script type="text/javascript">
			function editAuction(id){
		        var path = "auctionDetails";
		        $.ajax({
		          type: "POST",
		          url: path,
		          data: {
		            id: id
		          },
		           headers: {
		          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		          success: function(result){
		            //console.log(result);
		            var res = $.parseJSON(result);

		            if(res.status == 'error'){

		            }else{
		              var data = $.parseJSON(JSON.stringify(res.message));
		              $('#EditAuctionModal').data('id',data.id);
		              $('#EditAuctionModal').find('.modal-title').html('Update Auction');
		              $('#fill_category_id_edit').val(data.auction_category_id);
		              $('#single_bid_value_edit').val(data.single_bid_value);
		              $('#incremented_time_edit').val(data.incremented_bid_time);
		              $('#start_time_edit').val(data.start_time);
		              $('#end_time_edit').val(data.end_time);
		              $('#EditAuctionModal').modal('show');
		              //datatable.reload();
		            }
		          },
		          error: function(){
		            swal('Error','Something is wrong please try again','error')
		          }
		        }); 
		  	}

		</script>
		@endsection
			
