<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DUMO-privacy-policy</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <style>
        .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
            font-weight: 600;
        }

        .background-color {
            background: #ed7d31;
            color: #fff;
            padding: 20px;
        }

        .border_section{
            border: 1px solid #c16a31; 
            margin-bottom:-1px;
        }
            .border_section img {
                padding: 5%;
            }

            .border_section .col-md-2 {
                border-right: 1px solid #ed7d31;
            }
        .aboutright {
            background: #ed7d31;
            padding: 25px 20px;
            color: #fff;
            text-align: left;
        }
            .aboutright img { 
                margin: 10px auto;
            }


        .aboutright_1 {
            background: #ed7d31;
            padding: 25px 20px;
            color: #fff;
            text-align: left;
        }
            .aboutright_1 p {
                display: flex;
                justify-content: left;
                align-items: center;
            }
                .aboutright_1 img {
                width: 40px;
                margin-right: 10px;
                height: 45px;
            }
    </style>
</head>

<body>
    <section class="main-sec">
        <div class="container">
            <div class="main-heading">
                <h1 class="text-center">Dumo Privacy Policy </h1>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-all paragraph-all">
                        <h2>Introduction</h2>
                        <p>At Dumo we want to be clear and transparent about the information we collect, how we use the information, where we store the data and how it is protected.</p>
                    </div>
                    <div class="background-color">
                        <h2>Creating a family friendly privacy policy</h2>
                        <p>We want to make sure that children and parents understand how we look after your information and how we use your information.</p>
                        <p>To do this, we have created a family friendly version of our privacy policy. This means that a young person, or their parent, can read the boxes in purple. This will give them a simple summary of each part of this policy.</p>
                        <p>In fact, we have even checked the text using an onpne tool to make sure this is readable for children as young as 8 years old.</p>
                    </div>
                </div>
                </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Simple Summary</h2>
                    <div class="border_section clearfix">
                        <div class="col-md-2"><img src="{{url('assets/app/media/img/policy/1_01.jpg')}}" class="img-responsive"></div>
                        <div class="col-md-10">
                            <div class="sub-heading">
                                <h3>What information do we need?</h3>
                                <p>When you join Dumo we need information such as your name, address and when you were born. This helps us make your account. We also keep information when you load money onto Dumo or any time you use the Dumo card. This helps us to show you where you have spent money and keep your money safe. We keep information about how you use the Dumo app so we can make it better. We keep details of any time you talk to us by email, phone or chat.</p>
                            </div>
                        </div>
                    </div>
                    <div class="border_section clearfix">
                        <div class="col-md-2"><img src="{{url('assets/app/media/img/policy/1_03.jpg')}}" class="img-responsive"></div>
                        <div class="col-md-10">
                            <div class="sub-heading">
                                <h3>How do we keep your information safe?</h3>
                                <p>
                                    When we store your information, we jumble it up to stop other people from seeing it. We teach our staff about keeping your information safe.<br>
                                    Sometimes we share your information with other people to help us run Dumo. When we do this, we make sure they stick to our rules.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="border_section clearfix">
                        <div class="col-md-2"><img src="{{url('assets/app/media/img/policy/1_05.jpg')}}" class="img-responsive"></div>
                        <div class="col-md-10">
                            <div class="sub-heading">
                                <h3>Can you change or delete your information?</h3>
                                <p>
                                    You can change your email address or phone number in the Dumo app. If you would pke to change anything else, please talk to our support team who will be able to help.<br>
                                    When you leave Dumo, we need to keep some information for six years. When we don’t need your information any more we will delete it.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="border_section clearfix">
                        <div class="col-md-2"><img src="{{url('assets/app/media/img/policy/1_07.jpg')}}" class="img-responsive"></div>
                        <div class="col-md-10">
                            <div class="sub-heading">
                                <h3>How can I get more information?</h3>
                                <p>You can read our full privacy policy below to see how we keep your information safe. We have simple notes in the purple boxes. If you have any questions you can talk to us by sending an email to <a href="#">Help@Dumo.com</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>About this policy</h2>
                </div>
                    <div class="col-md-8">
                        <p>This policy apppes to the personal information processed by or on behalf of Dumo. This covers the Dumo App, Dumo website and other features.</p>
                        <p>Personal information means information which on its own or in combination with other information can be used to identify you.</p>
                        <p>We may change this privacy policy from time to time by updating this document. If we are making a major change we will notify you by email or an in-app message.</p>
                        <p>If you have any questions regarding this privacy policy, you can contact us at privacy@Dumo.com</p>
                    </div>
                    <div class="col-md-4">
                        <div class="aboutright">
                            <p>We want every to know how we get and use your information.</p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_2.png')}}" class="img-responsive"></p>
                            <p>You can read our notes in the purple boxes. They are not rules but will make it easier to read our policy.</p>
                        </div>
                    </div>
                
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Who are we?</h2>
                </div>
                    <div class="col-md-8">
                        <p>We are Dumo Ltd registered at 128 Prinsep Street, #01-01, Singapore 188655 (Company No. 20180455M).</p>
                        <p>We provide services to</p>
                        <p>(a) empower young people to manage their money responsibly with the help of their family and </p>
                        <p>(b) help famipes manage money across all family members in a simple, safe and easy to use way. This includes providing information and tools to parents, guardians, young people, family members and extended family members (such as carers and nannies) to help them manage money, make choices about money, stay connected with each other, and help young people to grow up and feel empowered.</p>
                        <p>We achieve all of this through mobile apps, websites for interacting with prepaid cards, emails, in-app chat, mobile notifications, SMSs and other digital and payments tools.</p>
                        <p>We act as a data controller which means we decide on the purpose for which your personal data is processed. However, each of our partners may be data controllers for the purposes of their own privacy popcies. </p>
                    </div>
                    <div class="col-md-4">
                        <div class="aboutright">
                            <p>We are Dumo and are based in the Singapore.</p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_3.png')}}" class="img-responsive"></p>
                            <p>We make it simpler, safer and better for famipes to manage money.</p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_19.png')}}" class="img-responsive"></p>
                            <p>We decide how to manage your information.</p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_2.png')}}" class="img-responsive"></p>
                        </div>
                    </div>

                </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Notice to parents or guardians</h2>
                </div>
                    <div class="col-md-8">
                        <p>As a parent or guardian who has signed up a Young Person, under the age of 18, to use the Dumo service, you are giving us permission to collect, use and share their data in the ways specified in this policy.</p>
                        <p>By providing a set of verified contact details for a Young Person, parents and guardians are exppcitly giving their permission for their children to be contacted in the ways described below.</p>
                        <p>We encourage parents and guardians to explain to their children how their information will be used, as set out in this policy.</p>
                        <p>Protecting the privacy of the data of Young People is important to Dumo. We therefore commit to the following principles regarding how we respect their personal data:</p>
                        <ul>
                            <li>
                                <p>We will send the following communications to Young People:</p>
                                <ul>
                                    <li>Essential service communications, e.g. attempted spend at non-permitted merchant, responses to enquiries</li>
                                    <li>Dumo usage alerts, e.g. allowance switched on/off; receipt of Dumo Gifts</li>
                                    <li>Dumo service updates</li>
                                </ul>
                            </li>
                            <li>Parents or guardians may opt-out of marketing on behalf of their children. Please see “Unsubscribing from services” for further details.</li>
                        </ul>
                        <p>We are reliant on you providing accurate information in order to implement our Privacy Policy and cannot be held responsible if you circumvent age-restrictions by providing incorrect age information.</p>
                    </div>
                    <div class="col-md-4">
                        <div class="aboutright">
                            <p>An adult will provide Dumo with a child’s information.</p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_6.png')}}" class="img-responsive"></p>
                            <p>If we have a child's email address or phone number, we may use this to send the child some messages.</p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_8.png')}}" class="img-responsive"></p>
                            <p>You can choose if we send some of these messages.</p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_9.png')}}" class="img-responsive"></p>
                        </div>
                    </div>
               
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Information we collect</h2>
                </div>
                    <div class="col-md-8">
                        <p>We collect personal information from you when you sign up and use our services. When you log into the Dumo app or use the Dumo website. We also collect data when you contact Dumo or when we need to contact you. This includes:</p>
                        <h4>Information that you provide to Dumo when you sign up</h4>
                        <ul>
                            <li>Account holder and personal information such as: Name, date of birth, gender, relationship to account holder, email address, postal/billing address and mobile number</li>
                            <li>Payment information such as: debit card number, expiry date, CVC, bank account number.</li>
                            <li>Personal documentation provided on request such as: Passport, Driving Licence, utility bill, bank statement.</li>
                        </ul>
                        <h4> Information you provide in contact/correspondence with Dumo </h4>
                        <ul>
                            <li>Information you give to us in telephone calls, emails, instant messages, text messages or other means of communication</li>
                            <li>
                                Media (should you choose to enable these features once available) o Profile photos
                                <ul>
                                    <li>Other photos, e.g. to tag transactions, capture receipts</li>
                                </ul>
                            </li>
                            <li> Contact information you provide if you refer friends or family to use Dumo or gift to an Dumo card. You confirm that you have the consent of anyone whose contact information you provide for this purpose </li>
                        </ul>
                        <h4>Technical information collected automatically when you use Dumo</h4>
                        <ul>
                            <li>
                                Information collected when visiting Dumo’s website such as:
                                <ul>
                                    <li>o	IP address, browser type, operating system, browser language, time zone, service provider, system settings, system tokens/keys</li>
                                    <li>Information about activities you undertake, such as the pages you visit, searches you make, marketing or links you click</li>
                                </ul>
                            </li>
                            <li>Information collected when using the Dumo app such as: mobile device, a unique device identifier, operating system version, mobile carrier</li>
                        </ul>
                        <h4>Usage data collected when you use Dumo</h4>
                        <ul>
                            <li>
                                Information related to individual transactions, e.g.
                                <ul>
                                    <li>Loading information: time and date of load, load type, notes</li>
                                    <li>Purchase information: retailers used, value of transactions, category of retailer, time of transaction</li>
                                    <li>Withdrawals: cash machine used, how much has been withdrawn, time of withdrawal</li>
                                    <li>Savings: amount saved, savings goal name, when saved, frequency of automatic saving</li>
                                </ul>
                            </li>
                            <li>
                                Dumo App usage information, e.g.
                                <ul>
                                    <li>Pages visited, functions selected</li>
                                    <li>Login attempts</li>
                                </ul>
                            </li>
                            <li>
                                All communications between Dumo and users or directly between users, including gifters
                                <ul>
                                    <li>Communications via text message, in-app messages from Dumo to Dumo users, messages on transfers or loads</li>
                                </ul>
                            </li>
                            <li>
                                If you enable features of Dumo that are location-enabled, we may collect and process information about the location of your device, e.g.
                                <ul>
                                    <li>
                                        Location of the mobile device, or other technologies such as nearby Wi-Fi access points, mobile transmitters and sensor data
                                        <ul>
                                            <li>Note: You will only be able to use location-based services if you have opted-in to do so and you will be able to switch them off again at any time you wish</li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>We may also store and access information on your device, e.g. Application data caches, browser web storage</li>
                        </ul>
                        <h4>Testing/pre-release research initiatives</h4>
                        <ul>
                            <li>
                                We may require additional information if you choose to take part in a pilot programme, e.g.
                                <ul>
                                    <li>Personal data, device information and other types of data which will be specified as part of the pilot</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <div class="aboutright_1">
                            <p>We keep information when you sign up and use Dumo.</p>
                            <p>Here is a list of the type of information we keep:</p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_10.png')}}" class="img-responsive"><span>Your name, age, where you live and how to contact you.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_12.png')}}" class="img-responsive"><span>Sometimes we might ask to see something to show who you are.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_11.png')}}" class="img-responsive"><span>Messages you send to us and messages we send to you.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_14.png')}}" class="img-responsive"><span>Images that you select or upload to us.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_15.png')}}" class="img-responsive"><span>Information of anyone you invite to Dumo.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_16.png')}}" class="img-responsive"><span>Information about what you do on the Dumo website</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_17.png')}}" class="img-responsive"><span>The mobile phone or tablet you use the Dumo app on.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_19.png')}}" class="img-responsive"><span>Information about every time you use spend or get money on Dumo.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_2.png')}}" class="img-responsive"><span>Information about the pages you visit on the Dumo app.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_11.png')}}" class="img-responsive"><span>Any messages you send or get from Dumo or other Dumo users.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_21.png')}}" class="img-responsive"><span>If you let us, where your mobile phone or tablet is used.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_17.png')}}" class="img-responsive"><span>Sometimes we store information or your phone, tablet or computer.</span></p>
                        </div>
                    </div>
                </div> 
            <div class="row">
                <div class="col-md-12">
                    <h2>How we use your information</h2>
                </div>
                    <div class="col-md-8">
                        <h3>We use your information to fulfil our contract with you, provide you with our services, meet our legal obligations, protect your vital interests and where necessary for the public interest. This includes:</h3>
                        <ul>
                            <li>To create your Dumo account </li>
                            <li>To enable you to use the Dumo app. This will let you load and spend on the Dumo app, manage savings, review transactions and make transfers</li>
                            <li>To enable you to easily, safely and simply manage money as an extended family </li>
                            <li>To enable you to access, manage, spend, save and send your money through the app and all types of notifications</li>
                            <li>To enable you to make choices about money, stay connected with each other, and help young people to grow up and feel empowered, for example, by sharing data insight, tips and advice</li>
                            <li>
                                To contact you regarding your account
                                <ul>
                                    <li>Provide you with essential services communications, e.g. suspicious activit y on your account, billing, responding to enquiries, etc</li>
                                    <li>Send you usage alerts to notify you of relevant  account activity, e.g. allowance switched on/off, attempted card use at non-permitted merchants</li>
                                    <li>To provide you with customer service when we notice a problem</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <div class="aboutright_1">
                            <p>We use your information so you can use Dumo and for rules we have to follow.</p>
                            <p>The reasons we use your information are:</p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_23.png')}}" class="img-responsive"><span>To keep your account working.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_17.png')}}" class="img-responsive"><span>So you can use the Dumo app.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_12.png')}}" class="img-responsive"><span>To contact you if something is wrong.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/ri_27.png')}}" class="img-responsive"><span>To check who you are and that you are following the rules.</span></p>
                            <p><img src="{{url('assets/app/media/img/policy/thumb.png')}}" alt="thumb" class="img-responsive"><span>To see how you use Dumo so we can make it better.</span></p>
                        </div>
                    </div>
                </div> 
            <div class="row">
                <br />
                <div class="col-md-12">
                    <ul>

                        <li>To verify your identity and to identify, prevent, detect or tackle financial crime, including fraud, money laundering and other unauthorised conduct. We may also access, use and store any identity verification or other check made, and do anything else required for these purposes.</li>
                        <li>To understand your use of the service so we can provide a safe, reliable and convenient Dumo experience. We will also use your data to make sure content is presented on your device is the most effective manner.</li>
                        <li>We may access, read, preserve, and disclose information that we reasonably believe is necessary to comply with law or a court order; enforce or apply our conditions of use and other agreements; or protect the rights, property, or safety of Dumo, our employees, our customers, or others.</li>
                        <li>We will use your data to carry out our obligations to you or any other users of the Dumo service, perform any services you have requested and allow you to participate in interactive features of the service when you choose to do so. For example, if you contribute money to a Young Person’s account we may communicate data about you and your gift to the relevant account-holder (parent / guardian of the Young Person).</li>
                        <li>When you contact the Dumo Support team for assistance via any means we may keep a record of your communications to respond to your query and to improve our service to you. Your call may be recorded for training or quality purposes.</li>
                    </ul>
                    <h3>We use your information for our legitimate interests where those interests are not outweighed by your interests or rights</h3>
                    <ul>
                        <li>We may use your contact details to communicate to you Dumo updates which let you know about changes to the Dumo service</li>
                        <li>We may also use anonymised data collected to improve our service, for example to carry out statistical analysis, including data analytics, developing and analysing data metrics and publishing the results in an aggregated form.</li>
                        <li>To ensure a consistent Dumo experience we will use information collected when you use Dumo via our apps or website using such technologies as cookies and pixel tags. This information may be used to customise any messaging you are presented with. Please see the Dumo Cookie policy for further information.</li>
                    </ul>
                    <h3>We use your information, with your consent to:</h3>
                    <ul>
                        <li>Contact you by email, SMS or in-app message to inform you of relevant offers and other marketing-related items from Dumo</li>
                        <li>Contact you by email, SMS or in-app message to inform you of relevant offers and other marketing-related items from carefully selected third-parties</li>
                    </ul>
                    <p>You have the right to remove your consent for these purposes at any time by emailing us at unsubscribe@Dumo.com</p>

                    <h2>Cookies</h2>
                    <p>
                        Cookies are data files containing small amounts of information which are downloaded to the device you use when you visit a website.<br>
                        Cookies are then sent back to the originating website on each subsequent visit, or to another website that recognises that cookie. We use cookies in order to ensure our website and app function correctly and to improve our understanding of how you use our service in order to make improvements.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>How we share information</h2>
                    <h3>We use third-party providers who process personal information on our behalf to help us provide the following services:</h3>
                    <ul>
                        <li>Customer-service</li>
                        <li>IT infrastructure, including cloud computing services</li>
                        <li>Marketing to customers where we have consent</li>
                        <li>Develop our service</li>
                        <li>Back-up and store our data</li>
                    </ul>
                    <p>
                        We have agreements in place with all third-party providers to make sure your information is properly protected. Third-party providers will only process your data for the reasons we have agreed with them.<br>
                        We may add additional services from time to time, in which case we will update this Privacy Policy.
                    </p>
                    <h3>To comply with the law or in the public interest</h3>
                    <p>Should we believe our service is being used illegally, we may provide evidence to the authorities to enable them to investigate suspected illegal activity. We will also disclose personal information (which may include purchase and location data) if we are required to do so by law. We may pass on data in an emergency where in good faith we believe that revealing personal information is needed to protect the safety of you or someone else.</p>
                    <h3>Business Transfers</h3>
                    <p>We may share customer information if we choose to buy or sell assets. Also, if we (or some of our assets) are acquired, or if we go out of business, enter bankruptcy, or go through some other change of control, personal information would be one of the assets transferred to a third party in those circumstances. You will be notified via email and/or a prominent notice on our website of any change in ownership.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>How we share information</h2>
                    <h3>We use third-party providers who process personal information on our behalf to help us provide the following services:</h3>
                    <ul>
                        <li>Customer-service</li>
                        <li> IT infrastructure, including cloud computing services</li>
                        <li> Marketing to customers where we have consent</li>
                        <li> Develop our service</li>
                        <li> Back-up and store our data</li>
                    </ul>
                    <p>
                        We have agreements in place with all third-party providers to make sure your information is properly protected. Third-party providers will only process your data for the reasons we have agreed with them. <br>
                        We may add additional services from time to time, in which case we will update this Privacy Policy.
                    </p>
                    <h3>To comply with the law or in the public interest</h3>
                    <p>Should we believe our service is being used illegally, we may provide evidence to the authorities to enable them to investigate suspected illegal activity. We will also disclose personal information (which may include purchase and location data) if we are required to do so by law. We may pass on data in an emergency where in good faith we believe that revealing personal information is needed to protect the safety of you or someone else.</p>
                    <h3>Business Transfers</h3>
                    <p>We may share customer information if we choose to buy or sell assets. Also, if we (or some of our assets) are acquired, or if we go out of business, enter bankruptcy, or go through some other change of control, personal information would be one of the assets transferred to a third party in those circumstances. You will be notified via email and/or a prominent notice on our website of any change in ownership.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>How we keep information secure</h2>
                    <p>
                        Dumo understands the importance of keeping your personal data secure. We will take appropriate technical and organisational measures to protect against unauthorised or unlawful processing, accidental loss or destruction of, or damage to personal data, although we cannot guarantee these things will never happen. No system is 100% safe from a sophisticated and sustained attack by determined hackers.<br>
                        A selection of measures we take to protect your personal data are provided below:
                    </p>
                    <h3>Using industry standard security technology and procedures to protect your information</h3>
                    <ul>
                        <li>High-security password protection on internal Dumo systems</li>
                        <li>Encryption of our services using SSL (Secure Sockets Layer)</li>
                        <li>Training of our employees on information security</li>
                        <li>Continual review of our policies and procedures in response to advances in technology and security best practice</li>
                    </ul>
                    <h3>Dumo App access password protection</h3>
                    <ul>
                        <li>Built in password protection on the Dumo App for parents / guardians and Young People to gain access to their account</li>
                    </ul>
                    <h3>Limiting access to your data</h3>
                    <ul>
                        <li>Measures to ensure access to Dumo systems is limited to approved Dumo team members to undertake their job</li>
                        <li>All employees undergo data protection training and sign confidentiality agreements</li>
                    </ul>
                    <h3>To comply with the law or in the public interest</h3>
                    <p>Should we believe our service is being used illegally, we may provide evidence to the authorities to enable them to investigate suspected illegal activity. We will also disclose personal information (which may include purchase and location data) if we are required to do so by law. We may pass on data in an emergency where in good faith we believe that revealing personal information is needed to protect the safety of you or someone else.</p>
                    <h3>Business Transfers</h3>
                    <p>We may share customer information if we choose to buy or sell assets. Also, if we (or some of our assets) are acquired, or if we go out of business, enter bankruptcy, or go through some other change of control, personal information would be one of the assets transferred to a third party in those circumstances. You will be notified via email and/or a prominent notice on our website of any change in ownership. </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>How we keep information secure</h2>
                    <p>
                        Dumo understands the importance of keeping your personal data secure. We will take appropriate technical and organisational measures to protect against unauthorised or unlawful processing, accidental loss or destruction of, or damage to personal data, although we cannot guarantee these things will never happen. No system is 100% safe from a sophisticated and sustained attack by determined hackers.<br>
                        A selection of measures we take to protect your personal data are provided below:
                    </p>
                    <ul>
                        <li>Using industry standard security technology and procedures to protect your information</li>
                        <li>High-security password protection on internal Dumo systems</li>
                        <li>Encryption of our services using SSL (Secure Sockets Layer)</li>
                        <li>Training of our employees on information security</li>
                        <li>Continual review of our policies and procedures in response to advances in technology and security best practice</li>
                        <li>Dumo App access password protection</li>
                        <li>Built in password protection on the Dumo App for parents / guardians and Young People to gain access to their account</li>
                        <li>Limiting access to your data</li>
                        <li>Measures to ensure access to Dumo systems is limited to approved Dumo team members to undertake their job</li>
                        <li>All employees undergo data protection training and sign confidentiality agreements</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Keeping your data up to date</h2>
                    <p>
                        If you are an Dumo parent / guardian or Young Person, most of your personal information can be accessed by logging into your account and some can be changed directly. However, for compliance reasons, certain types of information cannot be changed or deleted online. To request an amendment of this information you will need to contact us (see “Contact us” page) so we can verify your identity.<br>
                        Please be aware that for legal or risk reasons the Dumo Support team may be unable to amend or delete certain types of data. Furthermore, for legal reasons we may be required to keep your data for a fixed period of time after your account has been closed (currently 6 years).<br>
                        In addition, we use back-up systems to prevent service interruptions or data loss. We cannot guarantee that your data will be changed or deleted in those archives.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Your rights</h2>
                    <p>If we have processed your personal information you are entitled to the below rights:</p>
                    <ul>
                        <li>You have a right to access the information we hold, obtain a copy and obtain details on how your data is processed within 30 days of a request.</li>
                        <li>If any data we hold is incorrect, you can request this is corrected without delay.</li>
                        <li>You can request that we remove any unnecessary information about you. Dumo is required to retain some information to comply with financial regulations.</li>
                        <li>You can object to Dumo processing data in its interests</li>
                        <li>You can ask us to stop processing your data where it is inaccurate, our processing is unlawful, we no longer need the data or you have objected to us processing your data in our interest. When you do this, you will be unable to access any of the Dumo services.</li>
                        <li>You have the right to receive your data in a machine-readable format so that it can be used by another service.</li>
                        <li>You can object to automated decision-making which may significantly affect you.</li>
                    </ul>
                    <p>If you would like to exercise any of your rights, including obtaining a copy of the information we hold about you, please send an email to <a href="#">privacy@Dumo.com</a>.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Unsubscribing from services</h2>
                    <p>
                        You may opt-out from some of the communications we send you.<br>
                        Where we process your data with your consent, you may withdraw that<br>
                        consent at any time.
                    </p>
                    <h3>Essential service communications</h3>
                    <p>Dumo may communicate with you about important activity which has taken place on your account, for example, to alert you about suspicious transactions or problems with your account. You cannot opt-out of these communications.</p>
                    <h3>Usage alerts</h3>
                    <p>Dumo may send you alerts regarding your Dumo account usage. These notifications are designed to keep you informed about important account activity, and may alert you to suspicious activity so they cannot be switched-off. (In the future we may build functionality to enable you to control these alerts although we will recommend that you continue to keep them on).</p>
                    <h3>Dumo updates</h3>
                    <p>To unsubscribe from optional Dumo updates you can simply use the unsubscribe link which will be at the bottom of any service update email we send you.</p>
                    <h3>Marketing</h3>
                    <p>You have the right to ask us not to process your personal data for marketing purposes. If you have previously consented to marketing emails, but do not wish for you or your family to receive marketing communications, then please send an email with one of the following subject headings to <a href="#">privacy@Dumo.com:</a></p>
                    <ul>
                        <li>&ldquo;Unsubscribe me from third party marketing&rdquo;</li>
                        <li>&ldquo;Unsubscribe me from Dumo marketing&rdquo;</li>
                        <li>&ldquo;Unsubscribe me from all marketing&rdquo;</li>
                    </ul>
                    <p>
                        To unsubscribe from Dumo marketing you can also use the unsubscribe link which will be at the bottom of any marketing emails we send.<br>
                        We will need 5 working days to process these requests in most cases but please allow up to 2 weeks to take account of pre-prepared mailing lists.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Keeping your own data secure</h2>
                    <p>
                        We encourage parents / guardians to speak to their Children password security to ensure ongoing safety while using the service. Parents / guardians should ensure their Children understand that they should never share the password for their Dumo App. We recommend they do not write down their password but if they need to, this must be kept securely and separately from the device that they use to access the Dumo App. Equally parents / guardians must follow good password management practice themselves and not write down or share their Dumo password.<br>
                        If we believe your Dumo account passwords have not been kept secret, we may not be responsible for losses incurred on your account.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>The rules and guidelines we follow</h2>
                    <p>As a Singapore company, Dumo is subject to data protection regulations in Singapore. </p>

                    <h2>Contact us</h2>
                    <p>If you have any general enquiries or questions about how we use your personal information or about this Privacy Policy, please contact the Dumo Support Team by email at <a href="#">help@Dumo.com</a> </p>
                    <p>You can also contact our Data Protection Officer directly at:</p>
                    <p><strong>Email:</strong> <a href="#">privacy@Dumo.com</a></p>
                    <p><strong>Address:</strong> 128 Prinsep Street, #01-01, Singapore 188655</p>

                    <h2>Description of some terms</h2>
                    <p>
                        <strong>Data Controller</strong><br>
                        Decides what information is needed and how it is used.
                    </p>
                    <p>
                        <strong>IP Address</strong><br>
                        A set of numbers which makes up an address. The address tells us where you are connecting to the internet from.
                    </p>
                    <p>
                        <strong>Personal information</strong><br>
                        Information which on its own or in combination with other information can be used to identify you.
                    </p>
                    <p>
                        <strong>SSL (Secure Sockets Layer)</strong><br>
                        Rules which let your computer or phone to send or get information from Dumo without anyone else from seeing it.
                    </p>
                    <p>
                        Acknowledgements:<br>
                        Icons used created by Nithinan Tatah, Becris, Dinosoft Labs and Iconbunny from the <a href="#">Noun Project</a>.<br>
                        Emojis provided free by <a href="#">http://emojione.com</a>
                    </p>
                    <br /><br />
                </div>
            </div>
        </div>
    </section>
</body>
</html>
