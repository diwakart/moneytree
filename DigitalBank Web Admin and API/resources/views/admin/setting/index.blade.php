<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Settings
		</title>
		@extends('layouts.admin')
    	@section('content')
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						<!-- BEGIN: Subheader -->
						<div class="m-subheader ">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title m-subheader__title--separator">
										All Settings
									</h3>
									<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							            <li class="m-nav__item m-nav__item--home">
							                <a href="{{url('/dashboard')}}" class="m-nav__link m-nav__link--icon">
							                  <i class="m-nav__link-icon la la-home"></i>
							                </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a href="{{url('/admin/settings')}}" class="m-nav__link">
							                <span class="m-nav__link-text">
							                  All Settings
							                </span>
							              </a>
							            </li>
									</ul>
								</div>
								<div>
								</div>
							</div>
						</div>
						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row" style="display: none;">
								<div class="col-xl-12">
									<div class="m-portlet m-portlet--mobile">
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption">
												<div class="m-portlet__head-title col-xl-2">
													Settings
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--End::Main Portlet-->   
							<!--Begin::Main Portlet-->
							
							<!--End::Main Portlet-->
							<div class="row">
                <div class="col-lg-12">
                  <button id="save_settings" class="float-right btn btn-primary">Save Settings</button>
                </div>
                <div class="col-lg-11 bhoechie-tab-container">
                    <form class="col-lg-11" id="setting_form" style="display: flex;">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                      <div class="list-group">
                        <a class="list-group-item active text-center">
                          <h4 class="fa fa-user"></h4><p>Kid Awards</p>
                        </a>
                        <!--<a class="list-group-item text-center">
                          <h4 class="fa fa-gears"></h4><p>System Event Awards</p>
                        </a>-->
                        <a class="list-group-item text-center">
                          <h4 class="fa fa-gear"></h4><p>Auto Disbursal Threshold</p>
                        </a>
                        <a  class="list-group-item text-center">
                          <h4 class="fa fa-cc-stripe"></h4><p>Auto Purchase Threshold</p>
                        </a>
                      </div>
                    </div>
                    <div class="col-lg-8 bhoechie-tab">
                        <!-- Refer section -->
                        <div class="bhoechie-tab-content active">
                              <h3>Kid Rewards</h3>
                              <div class="form-group">
                                <label for="name" class="form-control-label">
                                    Refer Reward Points*:
                                </label>
                                <input type="text" class="form-control" id="refer_point_reward" name="refer_point_reward" value="@php if(!empty($default_setting)){ echo $default_setting['kid_setting']['refer_points']; } @endphp" placeholder="Please enter How much points earn for refer">
                              </div>
                              <div class="form-group">
                                <label for="name" class="form-control-label">
                                    Limit of task attempt in a day*:
                                </label>
                                <input type="text" class="form-control" id="limit_task" name="limit_task" value="@php if(!empty($default_setting)){ echo $default_setting['kid_setting']['limit_task']; } @endphp" placeholder="Please enter How much points earn for refer">
                              </div>
                        </div>
                        <!-- Auto Disbursal search -->
                        <div class="bhoechie-tab-content">
                            <h3>Pool Disbursal</h3>
                            <div class="form-group">
                                <label for="name" class="form-control-label">
                                    Auto Assigned Pool balance*:
                                </label>
                                <input type="text" class="form-control" id="auto_assigned_pool_points" name="auto_assigned_pool_points" value="@php if(!empty($default_setting)){ echo $default_setting['kid_setting']['auto_assigned_pool_points']; } @endphp" placeholder="Please enter default value to assign pool balance automatic">
                              </div>
                        </div>
                        <div class="bhoechie-tab-content">
                            <h3>Pool Disbursal</h3>
                            <div class="form-group">
                                <label for="name" class="form-control-label">
                                    Minimum Account balance*:
                                </label>
                                <input type="text" class="form-control" id="min_account_balance" name="min_account_balance" value="@php if(!empty($default_setting)){ echo $default_setting['parent_setting']['min_account_balance']; } @endphp" placeholder="Please enter default percent to assign pool balance automatic">
                              </div>
                        </div>
                    </div>
                  </form>
                </div>
          </div>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal for success response -->
	    <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                
	                <form name="fm-student" id="arrange-order-form1">
	                <div class="modal-body">
	                    <h5 id="ResponseHeading"></h5>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadCountryDatatable">
	                        OK
	                    </button>
	                </div>
	                </form>
	            </div>
	        </div>
	    </div>
		@endsection
		@section('js')
		<style>
		    
/*  bhoechie tab */
div.bhoechie-tab-container{
  display: flex;
  z-index: 10;
  background-color: #ffffff;
  padding: 10px !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid #ddd;
  margin-top: 20px;
  margin-left: 50px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
  -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
}
div.bhoechie-tab-menu{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
}
div.bhoechie-tab-menu div.list-group{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a .glyphicon,
div.bhoechie-tab-menu div.list-group>a .fa {
  color: #5A55A3;
}
div.bhoechie-tab-menu div.list-group>a:first-child{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a:last-child{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a.active,
div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
div.bhoechie-tab-menu div.list-group>a.active .fa{
  background-color: #5A55A3;
  background-image: #5A55A3;
  color: #ffffff;
}
div.bhoechie-tab-menu div.list-group>a.active:after{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid #5A55A3;
}

div.bhoechie-tab-content{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 10px;
  padding-top: 20px;
}
div.bhoechie-tab-content h3{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  text-align: center;
  text-decoration: underline;
}

div.bhoechie-tab div.bhoechie-tab-content:not(.active){
  display: none;
}
		</style>
		<script>
		    $(document).ready(function() {
                $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                    e.preventDefault();
                    $(this).siblings('a.active').removeClass("active");
                    $(this).addClass("active");
                    var index = $(this).index();
                    $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                    $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
                });
                $('#save_settings').on('click',function(){
                  var form = $('#setting_form')[0];
                  var data = new FormData(form);
                  $.ajax({
                    type: 'POST',
                    url: 'settings',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                    success: function(data) {
                      var res = $.parseJSON(data);
                      if(res.status == 'error'){
                        swal('Error',res.message,'error');
                      }else{
                        $('.sweet-overlay').remove();
                        $('.showSweetAlert ').remove();
                        swal('Success',res.message,'success');
                        setTimeout(function(){ location.reload(); }, 2000);
                        //$("#ResponseSuccessModal").modal('show');
                        //$("#ResponseSuccessModal #ResponseHeading").text(res.message);
                      } 
                    },
                    error: function(data) {
                      swal('Error',data,'error');
                    }
                  });
                });
            });
		</script>
		@endsection
			
