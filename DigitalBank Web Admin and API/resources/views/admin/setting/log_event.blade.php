<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Categories
		</title>
		@extends('layouts.admin')
    	@section('content')
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						<!-- BEGIN: Subheader -->
						<div class="m-subheader">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title m-subheader__title--separator">
										All Events
									</h3>
									<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							            <li class="m-nav__item m-nav__item--home">
							                <a href="{{url('/dashboard')}}" class="m-nav__link m-nav__link--icon">
							                  <i class="m-nav__link-icon la la-home"></i>
							                </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a href="{{url('/admin/system_log')}}" class="m-nav__link">
							                <span class="m-nav__link-text">
							                  All Events
							                </span>
							              </a>
							            </li>
									</ul>
								</div>
								<div>
								</div>
							</div>
						</div>
						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row">
								<div class="col-xl-12">
									<div class="m-portlet m-portlet--mobile">
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption col-xl-4">
												<div class="m-portlet__head-title float-left col-xl-2">
													<h3 class="m-portlet__head-text">Events</h3>
												</div>
											</div>
										</div>
										<div class="m-portlet__body">
											<div class="event_datatable" id="local_data">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--End::Main Portlet-->   
							<!--Begin::Main Portlet-->
							
							<!--End::Main Portlet-->
						</div>
					</div>
				</div>
			</div>
		<div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		        <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                
		                <form name="fm-student" id="arrange-order-form">
		                <div class="modal-body">
		                    <h5 id="ResponseHeading"></h5>
		                </div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadCategoryDatatable">
		                        OK
		                    </button>
		                </div>
		                </form>
		            </div>
		        </div>
	    </div>
		@endsection
		@section('js')
		<script type="text/javascript">
		    $(document).ready(function() {
                var datatable='';
                var categoryData;
                  datatable = $('.event_datatable').mDatatable({
                    // datasource definition
                  data: {
                  type: 'remote',
                  source: {
                      read: {
                          url: 'getAllEvents',
                          method: 'GET',
                          // custom headers
                          headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                          params: {
                              // custom query params
                              query: {
                                  generalSearch: ''
                              }
                          },
                          map: function(raw) {
                              var dataSet = raw;
                              if (typeof raw.data !== 'undefined') {
                                   dataSet = raw.data;
                              }
                              return dataSet;
                          },
                      }
                  },
                  pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
            
                    serverPaging: true,
                    serverFiltering: false,
                    serverSorting: false
                },
                // layout definition
                layout: {
                  theme: 'default', // datatable theme
                  class: '', // custom wrapper class
                  scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                  // height: 450, // datatable's body's fixed height
                  footer: false // display/hide footer
                },
            
                // column sorting
                sortable: false,
            
                pagination: true,
            
                // search: {
                //   input: $('#generalSearch')
                // },
            
                // inline and bactch editing(cooming soon)
                // editable: false,
            
                // columns definition
                columns: [
                {
                  field: "id",
                  title: "#",
                  width: 50,
                  selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                },
                {
                  field: "route_name",
                  title: "Route Name"      
                },
                {
                  field: "reward_points",
                  title: "Reward Points"      
                },
                {
                  field: "event_logged",
                  title: "Event Logged",
                  template: function(row) {
                      if(row.event_logged=='1')
                      {
                        return '\
                        <span>Yes</span>\
                      ';  
                      }
                      else
                      {
                         return '\
                        <span>No</span>\
                      ';  
                      }
                  }
                },
                {
                      field: 'Actions',
                      width: 110,
                      title: 'Actions',
                      sortable: false,
                      overflow: 'visible',
                      template: function(row) {
                        var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';
            
                        return '\
                        <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete Category" onclick=deleteCategories('+row.id+')>\
                          <i class="la la-trash"></i>\
                        </a>\
                      ';
                      },
                  }]
                });
                $('#m_form_status, #m_form_type, #m_form_hotness,#m_form_list').selectpicker();
                $('#LoadCategoryDatatable').on('click',function(){
                  datatable.reload();
                });
            });
    </script>
		@endsection
			
