<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Lesson Of Day
		</title>
		@extends('layouts.admin')
    	@section('content')
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						<!-- BEGIN: Subheader -->
						<div class="m-subheader ">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title m-subheader__title--separator">
										All Lesson Of Day
									</h3>
									<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							            <li class="m-nav__item m-nav__item--home">
							                <a href="{{url('/dashboard')}}" class="m-nav__link m-nav__link--icon">
							                  <i class="m-nav__link-icon la la-home"></i>
							                </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a href="{{url('/admin/tasks')}}" class="m-nav__link">
							                <span class="m-nav__link-text">
							                  All Lessons
							                </span>
							              </a>
							            </li>
									</ul>
								</div>
								<div>
								</div>
							</div>
						</div>
						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row">
								<div class="col-xl-12">
									<div class="m-portlet m-portlet--mobile col-xl-12">
										<div class="m-portlet__head col-xl-12">
											<div class="m-portlet__head-caption col-xl-4">
												<div class="m-portlet__head-title float-left col-xl-2">
													<h3 class="m-portlet__head-text">Lessons</h3>
													
												</div>
											</div>
											<div class="m-dropdown m-dropdown--inline m-dropdown--arrow col-xl-4" data-dropdown-toggle="click" style="margin-top: 20px;">
												<a href="javascript:;" class="m-dropdown__toggle btn btn-primary dropdown-toggle">
				                                  Add Lesson
				                            	</a>
				                            	<div class="m-dropdown__wrapper">
                              						<span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                              						<div class="m-dropdown__inner">
                                					<div class="m-dropdown__body">
                                  						<div class="m-dropdown__content">
                                    						<ul class="m-nav">
						                                      	<li class="m-nav__section m-nav__section--first">
						                                        <span class="m-nav__section-text">
						                                          Select Question Type
						                                        </span>
						                                      	</li>
						                                      	<li class="m-nav__item">
						                                        <a href="javascript:;" id="FillBlankQuestionType" class="m-nav__link">
						                                          <i class="m-nav__link-icon flaticon-share"></i>
						                                          <span class="m-nav__link-text">
						                                            Fill in the Blank question
						                                          </span>
						                                        </a>
						                                      	</li>
						                                      	<li class="m-nav__item">
						                                        <a href="javascript:;" class="m-nav__link" id="MultiChoiceQuestionType">
						                                          <i class="m-nav__link-icon flaticon-chat-1"></i>
						                                          <span class="m-nav__link-text">
						                                            Multi Choice Question
						                                          </span>
						                                        </a>
						                                      	</li>
						                                      	<li class="m-nav__item">
						                                        <a href="javascript:;" id="ArrangeOrderQuestionType" class="m-nav__link">
						                                          <i class="m-nav__link-icon flaticon-info"></i>
						                                          <span class="m-nav__link-text">
						                                            Arrange Order
						                                          </span>
						                                        </a>
						                                      	</li>
                                    						</ul>
                                  						</div>
                                					</div>
                          							</div>
                            					</div>
                          					</div>
										</div>
										<div class="m-portlet__body col-xl-12">
											<div class="lesson_datatable col-xl-12" id="local_data">
											</div>
										</div>
									</div>
									<!-- <div class="m-portlet m-portlet--mobile col-xl-12">

									</div> -->
								</div>
							</div>
							<!--End::Main Portlet-->   
							<!--Begin::Main Portlet-->
							
							<!--End::Main Portlet-->
						</div>
					</div>
				</div>
			</div>
			 <!-- Modal for short question type -->
	    <div class="modal fade" id="FillBlankTypeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <h5 class="modal-title" id="exampleModalLabel">
	                       Fill in the blank(s).
	                    </h5>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">
	                            &times;
	                        </span>
	                    </button>
	                </div>
	                <form name="fm-student" id="fill-blanks-form" enctype="multipart/form-data">
	                <div id="loader" style="display:none;">
						    <img src="{{url('assets/app/media/loader.gif')}}">
					</div>   
	                <div class="modal-body">
	                        <div class="form-group">
	                            <label for="fillblank-input" class="form-control-label">
	                              Lesson Title *:
	                            </label>
	                            <textarea type="text" name="fill_question_input" id="shortQuestion-input" class="form-control"></textarea>
	                            <input type="hidden" name="fill_lesson_id" id="fill_lesson_id" class="form-control">
	                        </div>
	                        <div class="form-group">
	                            <label for="fillblank-input" class="form-control-label">
	                              Lesson Date *:
	                            </label>
	                            <input type="text" name="fill_question_input_date" id="fill_question_input_date" class="form-control" readonly>
	                        </div>
	                        <div class="form-group">
	                            <label for="fillblank-input" class="form-control-label">
	                              Points *:
	                            </label>
	                            <input type="text" class="form-control form-control-danger" name="fill_lesson_points" placeholder="Points of Question" id="fill_lesson_points">
                    		</div>
                    		<div class="form-group">
	                            <label for="fillblank-input" class="form-control-label">
	                              Attach File :
	                            </label>
	                            <input type="file" class="form-control form-control-danger" name="fill_attached_file" placeholder="Points of Question" id="fill_attached_file">
                    		</div>
                    		<div class="form-group">
	                            <label for="fillblank-input" class="form-control-label">
	                              Share Url :
	                            </label>
	                            <input type="text" class="form-control form-control-danger" name="fill_shared_url" placeholder="Enter link of shared url" id="fill_shared_url">
                    		</div>
                    		<div class="form-group" id="fill_attached_file_show" style="display:none;">
                		    </div>
                    		<div class="form-group">
	                            <label for="fillblank-input" class="form-control-label">
	                              Skipped Time In Minute:
	                            </label>
	                            <select class="form-control" id="fill_skip_minute" name="fill_skip_minute">
								@for($a=0; $a< 6; $a++)
                                    @for($b=0; $b< 10; $b++)
								    <option value="{{$a.$b}}" @php if($a.$b=='00') echo 'selected' @endphp>{{$a.$b}} min</option>
								    @endfor
								@endfor
								</select>
                    		</div>
                    		<div class="form-group">
	                            <label for="fillblank-input" class="form-control-label">
	                              Skipped Time in Second:
	                            </label>
	                            <select class="form-control" id="fill_skip_second" name="fill_skip_second">
								@for($a=0; $a< 6; $a++)
                                    @for($b=0; $b< 10; $b++)
								    <option value="{{$a.$b}}" @php if($a.$b=='00') echo 'selected' @endphp>{{$a.$b}} sec</option>
								    @endfor
								@endfor
								</select>
                    		</div>
                    		<div class="form-group">
	                            <label for="fillblank-input" class="form-control-label">
	                              Lesson Description :
	                            </label>
	                            <textarea type="text" name="fill_question_description" id="fill_question_description" class="form-control"></textarea>
	                        </div>
	                        <input type="hidden" name="answer[]" id="answers">

	                        <div id="FillInTheBlanksAnswers">
	                            <div class="form-group  m-form__group row">
	                                <label for="fillblank-input" class="form-control-label">
	                                 Answer for blank(s) *:
	                               </label>
	                                <div data-repeater-list="" id="FillAnswerResponse" class="col-lg-12 ">
	                                    <div data-repeater-item class="row m--margin-bottom-10 clonedInputsForBlank">
	                                        <div class="col-lg-9">
	                                            <div class="input-group">
	                                            <input type="text" class="form-control form-control-danger ans_input" name="fill_answer_input" placeholder="Answer" id="">
	                                            </div>
	                                        </div>
	                                       
	                                        <div class="col-lg-3">
	                                            <a href="javascript:;" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
	                                                <i class="la la-remove"></i>
	                                            </a>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-lg-3"></div>
	                                <div class="col">
	                                    <div data-repeater-create="" class="btn btn btn-primary m-btn m-btn--icon">
	                                        <span>
	                                            <i class="la la-plus"></i>
	                                            <span>
	                                                Add
	                                            </span>
	                                        </span>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>

	                        
	                    
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
	                        Close
	                    </button>
	                    
	                    <button type="submit" class="btn btn-primary" id="FillBlankTypeBtn">
	                        Submit
	                    </button>
	                </div>
	                </form>
	            </div>
	        </div>
	    </div> 
	   

	<!-- Modal for multi choice question type -->
	    <div class="modal fade" id="MultiChoiceQuestionTypeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <h5 class="modal-title" id="exampleModalLabel">
	                      Multi Choice Question/Answer 
	                    </h5>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">
	                            &times;
	                        </span>
	                    </button>
	                </div>
	                <form name="fm-student" id="multi-choice-form" enctype="multipart/form-data">
	                <div class="modal-body">
	                    <div class="form-group">
	                        <label for="shortQuestion-input" class="form-control-label">
	                          Lesson Title*:
	                        </label>
	                        <textarea name="multiQuestion_input" id="multiQuestion_input" class="form-control"></textarea>
	                        <input type="hidden" name="multi_lesson_id" id="multi_lesson_id" class="form-control">
	                    </div>
	                    
	                    <div class="form-group">
                            <label for="fillblank-input" class="form-control-label">
                              Lesson Date *:
                            </label>
                            <input type="text" name="multi_question_input_date" id="multi_question_input_date" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="fillblank-input" class="form-control-label">
                              Points *:
                            </label>
                            <input type="text" class="form-control form-control-danger" name="multi_lesson_points" placeholder="Points of Question" id="multi_lesson_points">
                		</div>
                		<div class="form-group">
                            <label for="fillblank-input" class="form-control-label">
                              Attach File :
                            </label>
                            <input type="file" class="form-control form-control-danger" name="multi_attached_file" placeholder="Points of Question" id="multi_attached_file">
                		</div>
                		<div class="form-group">
	                            <label for="fillblank-input" class="form-control-label">
	                              Share Url :
	                            </label>
	                            <input type="text" class="form-control form-control-danger" name="multi_shared_url" placeholder="Enter link of shared url" id="multi_shared_url">
                    		</div>
                            <div class="form-group" id="multi_attached_file_show" style="display:none;">
                                </div>
                            <div class="form-group">
                                <label for="multiblank-input" class="form-control-label">
                                Skipped Time In Minute:
                                </label>
                                <select class="form-control" id="multi_skip_minute" name="multi_skip_minute">
                                    @for($a=0; $a< 6; $a++)
                                        @for($b=0; $b< 10; $b++)
    								    <option value="{{$a.$b}}" @php if($a.$b=='00') echo 'selected' @endphp>{{$a.$b}} min</option>
    								    @endfor
								    @endfor
                                </select>
                            </div>
                    		<div class="form-group">
                                <label for="multiblank-input" class="form-control-label">
                                  Skipped Time in Second:
                                </label>
                                <select class="form-control" id="multi_skip_second" name="multi_skip_second">
    							    @for($a=0; $a< 6; $a++)
                                        @for($b=0; $b< 10; $b++)
    								    <option value="{{$a.$b}}" @php if($a.$b=='00') echo 'selected' @endphp>{{$a.$b}} sec</option>
    								    @endfor
							    	@endfor
    							</select>
                    		</div>
                		<div class="form-group">
                            <label for="fillblank-input" class="form-control-label">
                              Lesson Description :
                            </label>
                            <textarea type="text" name="multi_question_description" id="multi_question_description" class="form-control"></textarea>
                        </div>
                        <input type="hidden" name="options[]" id="options">
                        <input type="hidden" name="correct_answer[]" id="correct_answer">

	                        
	                    <div id="MultiChoiceOptionDiv">
	                        <div class="form-group  m-form__group">
	                            <label for="multiQuestion-option" class="form-control-label">
	                              Option(s) *:
	                            </label>
	                            <div data-repeater-list="" id="MultiAnswerResponse">
	                                <div data-repeater-item class="form-group m-form__group row align-items-center clonedOptionsForMultiChoice">
	                                    <div class="col-md-9">
	                                        <div class="input-group m-form__group">
	                                            <span class="input-group-addon">
	                                               
	                                                    <input type="checkbox" class="check_answer" >
	                                                    
	                                               
	                                            </span>
	                                            <input type="text" class="form-control options_value" aria-label="Enter Option">
	                                        </div>
	                                        <div class="d-md-none m--margin-bottom-10"></div>
	                                    </div>
	                                    <div class="col-md-3">
	                                        <div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
	                                            <span>
	                                                <i class="la la-trash-o"></i>
	                                                <span>
	                                                    Delete
	                                                </span>
	                                            </span>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="m-form__group form-group row">
	                            <label class="col-lg-2 col-form-label"></label>
	                            <div class="col-lg-4">
	                                <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
	                                    <span>
	                                        <i class="la la-plus"></i>
	                                        <span>
	                                            Add
	                                        </span>
	                                    </span>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
	                        Close
	                    </button>
	                    <button type="submit" class="btn btn-primary" id="MultiChoiceQuestionTypeBtn">
	                        Submit
	                    </button>
	                </div>
	                </form>
	            </div>
	        </div>
	    </div> 



	<!-- Modal for multi choice question type -->
	    <div class="modal fade" id="ArrangeOrderQuestionTypeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <h5 class="modal-title" id="exampleModalLabel">
	                      Arrange Order Question/Answer 
	                    </h5>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">
	                            &times;
	                        </span>
	                    </button>
	                </div>
	                <form name="fm-student" id="arrange-order-form" enctype="multipart/form-data">
	                <div class="modal-body">
	                       
	                        <div id="ArrangeOrderParts">
	                        <div class="form-group">
	                            <label for="arrangeQuestion-input" class="form-control-label">
	                              Lesson Title * :
	                            </label>
	                            <textarea name="arrangeQuestion_input" id="arrangeQuestion_input" class="form-control"></textarea>
	                            <input type="hidden" name="arrange_lesson_id" id="arrange_lesson_id" class="form-control">
	                        </div>
	                        <div class="form-group">
                            <label for="fillblank-input" class="form-control-label">
                              Lesson Date *:
                            </label>
                            <input type="text" name="arrange_question_input_date" id="arrange_question_input_date" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="fillblank-input" class="form-control-label">
                              Points *:
                            </label>
                            <input type="text" class="form-control form-control-danger" name="arrange_lesson_points" placeholder="Points of Question" id="arrange_lesson_points">
                		</div>
                		<div class="form-group">
                            <label for="fillblank-input" class="form-control-label">
                              Attach File :
                            </label>
                            <input type="file" class="form-control form-control-danger" name="arrange_attached_file" placeholder="Points of Question" id="arrange_attached_file">
                		</div>
                		<div class="form-group">
	                            <label for="fillblank-input" class="form-control-label">
	                              Share Url :
	                            </label>
	                            <input type="text" class="form-control form-control-danger" name="arrange_shared_url" placeholder="Enter link of shared url" id="arrange_shared_url">
                		</div>
                		<div class="form-group" id="arrange_attached_file_show" style="display:none;">
	                            
                		</div>
                		<div class="form-group">
                            <label for="arrangeblank-input" class="form-control-label">
                            Skipped Time In Minute:
                            </label>
                            <select class="form-control" id="arrange_skip_minute" name="arrange_skip_minute">
                                @for($a=0; $a< 6; $a++)
                                    @for($b=0; $b< 10; $b++)
								    <option value="{{$a.$b}}" @php if($a.$b=='00') echo 'selected' @endphp>{{$a.$b}} min</option>
								    @endfor
								@endfor
                            </select>
                        </div>
                		<div class="form-group">
                            <label for="arrangeblank-input" class="form-control-label">
                              Skipped Time in Second:
                            </label>
                            <select class="form-control" id="arrange_skip_second" name="arrange_skip_second">
    							@for($a=0; $a< 6; $a++)
                                    @for($b=0; $b< 10; $b++)
								    <option value="{{$a.$b}}" @php if($a.$b=='00') echo 'selected' @endphp>{{$a.$b}} sec</option>
								    @endfor
								@endfor
							</select>
                		</div>
                		<div class="form-group">
                            <label for="fillblank-input" class="form-control-label">
                              Lesson Description :
                            </label>
                            <textarea type="text" name="arrange_question_description" id="arrange_question_description" class="form-control"></textarea>
                        </div>
                        <input type="hidden" name="correct_order[]" id="correct_order">
                        <input type="hidden" name="incorrect_order[]" id="incorrect_order">
	                       
	                        <div class="form-group  m-form__group row" id="DoClone">
	                            <label  class="col-lg-2 col-form-label">
	                                Parts *:
	                            </label>
	                            <div data-repeater-list=""  class="col-lg-10 " id="ArrangeOrderPartsDiv">
	                                <div data-repeater-item class="m--margin-bottom-10 clone_number" id="clone0">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">
	                                            <i class="la la-check"></i>
	                                        </span>
	                                        <input type="text" class="form-control form-control-danger incorrect_order" placeholder="Enter Question Part">
	                                        <span class="input-group-btn" data-repeater-delete="">
	                                            <a href="javascript:;" class="btn btn-danger m-btn m-btn--icon" >
	                                                <i class="la la-close"></i>
	                                            </a>
	                                        </span>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-lg-3"></div>
	                            <div class="col">
	                                <div data-repeater-create="" class="btn btn btn-warning m-btn m-btn--icon">
	                                    <span>
	                                        <i class="la la-plus"></i>
	                                        <span>
	                                            Add
	                                        </span>
	                                    </span>
	                                </div>
	                            </div>
	                            <div class="col">
	                                <button type="button" class="btn btn-info" id="ArrangeOrderBtn">Arrange Correct Order</button>
	                            </div>

	                        </div>
	                    </div>
	                       <br><br> 
	                    <div class="form-group">
	                        <ul id="ArrangeOrderDiv" type="none">
	                        </ul>

	                    </div>
	                    
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
	                        Close
	                    </button>
	                    <button type="submit" class="btn btn-primary" id="ArrangeOrderQuestionTypeBtn">
	                        Submit
	                    </button>
	                </div>
	                </form>
	            </div>
	        </div>
	    </div>

	<!-- Modal for success response -->
	    <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                
	                <form name="fm-student" id="arrange-order-form1">
	                <div class="modal-body">
	                    <h5 id="ResponseHeading"></h5>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadLessonDatatable">
	                        OK
	                    </button>
	                </div>
	                </form>
	            </div>
	        </div>
	    </div>
		@endsection
		@section('js')
		<script src="{{url('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
		<script type="text/javascript" src="{{url('/assets/admin/js/lesson.js')}}"></script>
		<script src="{{url('/assets/admin/js/question-page.js')}}" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
		<script>
	
		 $("#FillBlankQuestionType").on('click',function(){
		    $("#FillBlankTypeModal").modal('show');
		 });

		 $("#MultiChoiceQuestionType").on('click',function(){
		    $("#MultiChoiceQuestionTypeModal").modal('show');
		 }) 

		 $("#ArrangeOrderQuestionType").on('click',function(){
		     $("#ArrangeOrderQuestionTypeModal")
		    .find("input,textarea")
		       .val('');
		    $("#ArrangeOrderQuestionTypeModal #ArrangeOrderDiv").html('');
		     $(this).data('id','');

		    $("#ArrangeOrderQuestionTypeModal").modal('show');
		 })

		$("#ArrangeOrderQuestionTypeModal #DoSortable").sortable({
		    stop : function(event, ui){
		}
		});

		$("#ArrangeOrderQuestionTypeModal #ArrangeOrderBtn").on('click', function(){
		   var list_number = 1;
		   $("#ArrangeOrderQuestionTypeModal #ArrangeOrderDiv").html('');
		  $("#ArrangeOrderPartsDiv .clone_number").each(function(){
		    var order_text = $(this).find('input').val();
		    var OrderList = "<li id="+list_number+">"+order_text+"</li>";
		    $("#ArrangeOrderQuestionTypeModal #ArrangeOrderDiv").append(OrderList);
		    list_number++;
		   }); 

		   $("#ArrangeOrderDiv").sortable({
		    stop : function(event, ui){
		     }
		   });

		})


		/*========Fill in the blanks===========*/
		$(function()
		{
        $("#FillBlankTypeBtn").on('click',function(event){
            event.preventDefault();
            var answers = [];
            i = 0;
            $('.ans_input').each(function()
            { 
                if($(this).val()!=''){
                    answers[i++] = $(this).val();
                }
                 
            });
            
            $('#answers').val(answers);
            var form = $('#fill-blanks-form')[0];
            var data = new FormData(form);
            var lesson_points = $('#fill_lesson_points').val();
            var lesson_date = $('#fill_question_input_date').val();
            
            if(lesson_date=='')
            {
            	swal('Error','Please Enter Lesson date','error');
            	return false;	
            }
            if(lesson_points=='')
            {
            	swal('Error','Please Enter Lesson Points','error');
            	return false;	
            }
            if($('#fill_attached_file').val()!='' && $('#fill_shared_url').val()!='')
            {
               swal('Error','Please Either attach file or share the url','error');
            	return false; 
            }
            var question = $("#shortQuestion-input").val();
            //var questionid = $('#FillBlankTypeModal').data('id');

            //alert(questionid);
               
                if(question!=''){
                    $.ajax({
                              type: 'POST',
                              enctype: 'multipart/form-data',
                              url: 'store_lesson_fill_blanks',
                               headers: {
                                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                               },
                               data:data,
                               processData: false,
                               contentType: false,
                               cache: false,
                               beforeSend:function(res){
                                   $('#FillBlankTypeBtn').prop('disabled', true);
                                   //$('#loader').show();
		                           //$(body).css('@zindex-modal-background','1040')
                                },
                              success: function(data) {
                                $('#FillBlankTypeBtn').prop('disabled', false);
                                var res = $.parseJSON(data);
                                console.log(res);
                                if(res.status == 'error'){
                                  swal('Error',res.message,'error');
                                }else{
                                    $("#FillBlankTypeModal").modal('hide'); 
                                    $("#ResponseSuccessModal").modal('show');
                                    $("#ResponseSuccessModal #ResponseHeading").text(res.message);
                                } 
                              },
                              error: function(data) {
                                $('#FillBlankTypeBtn').prop('disabled', false);
                                swal('Error',data,'error');
                              }
                            });
                }else{
                    swal('Error','Please fill out input boxes','error');
                   
                }
                
           });
	    });

		/*========Multi Choice==================*/

		$(function(){
        $("#MultiChoiceQuestionTypeBtn").click(function(event){
            event.preventDefault();

            var options = [];
            var correct_answer = [];
            i = 0;
            $('.options_value').each(function()
            { 
                if($(this).val()!=''){
                    correct_answer[i] = $(this).siblings().children().val();
                    options[i++] = $(this).val();
                }
                 
            });
            $('#options').val(options);
            $('#correct_answer').val(correct_answer);
            var form = $('#multi-choice-form')[0];
            var data = new FormData(form);
            var lesson_points = $('#multi_lesson_points').val();
            var lesson_date = $('#multi_question_input_date').val();
            
            if(lesson_date=='')
            {
            	swal('Error','Please Enter Lesson date','error');
            	return false;	
            }
            if(lesson_points=='')
            {
            	swal('Error','Please Enter Lesson Points','error');
            	return false;	
            }
            if($('#multi_attached_file').val()!='' && $('#multi_shared_url').val()!='')
            {
               swal('Error','Please Either attach file or share the url','error');
            	return false; 
            }

            var question = $("#multiQuestion_input").val();
            if(question!=''){
                $.ajax({
                        type: 'POST',
                        enctype: 'multipart/form-data',
                        url: 'store_lesson_multi_choice',
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                           headers: {
                             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                           },
                           beforeSend:function(res){
                                   $('#MultiChoiceQuestionTypeBtn').prop('disabled', true);
                                },
                          success: function(data) {
                              $('#MultiChoiceQuestionTypeBtn').prop('disabled', false);
                            var res = $.parseJSON(data);
                            if(res.status == 'error'){
                              swal('Error',res.message,'error');
                            }else{
                              $("#MultiChoiceQuestionTypeModal").modal('hide'); 
                              $("#ResponseSuccessModal").modal('show');
                              $("#ResponseSuccessModal #ResponseHeading").text(res.message);
                            } 
                          },
                          error: function(data) {
                              $('#MultiChoiceQuestionTypeBtn').prop('disabled', false);
                            swal('Error',data,'error');
                          }
                        });
                }else{
                    swal('Error','Please fill out input boxes','error');
                   
                }
                
           });
	    });


		$(document).on("change", "input[class='check_answer']", function () {
		    if (this.checked) {
		     $(this).val(1);
		     }else{
		    $(this).val(0);
		     }
		});

		/*======Arrange Order==============*/

		$(function(){
		        $("#ArrangeOrderQuestionTypeBtn").click(function(event){
		            event.preventDefault();
		                var incorrect_order = [];
		                var correct_order = [];
		                i = 0;
		                $('.incorrect_order').each(function()
		                { 
		                    if($(this).val()!=''){
		                        incorrect_order[i++] = $(this).val();
		                    }
		                     
		                });
		                 $('#ArrangeOrderDiv li').each(function(index)
		                {
		                        correct_order.push($(this).text());
		                });
		                $('#correct_order').val(correct_order);
                        $('#incorrect_order').val(incorrect_order);
                        var form = $('#arrange-order-form')[0];
                        var data = new FormData(form);
                        var lesson_points = $('#arrange_lesson_points').val();
                        var lesson_date = $('#arrange_question_input_date').val();
                        if(lesson_date=='')
                        {
                        	swal('Error','Please Enter Lesson date','error');
                        	return false;	
                        }
                        if(lesson_points=='')
                        {
                        	swal('Error','Please Enter Lesson Points','error');
                        	return false;	
                        }
                        
                        if($('#arrange_attached_file').val()!='' && $('#arrange_shared_url').val()!='')
                        {
                           swal('Error','Please Either attach file or share the url','error');
                        	return false; 
                        }
		                 
		                var question = $("#arrangeQuestion_input").val();
		                if(question!=''){
		                        $.ajax({
                                        type: 'POST',
                                        enctype: 'multipart/form-data',
                                        url: 'store_lesson_arrange_order',
                                        data: data,
                                        processData: false,
                                        contentType: false,
                                        cache: false,
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        beforeSend:function(res){
                                           $('#ArrangeOrderQuestionTypeBtn').prop('disabled', true);
                                        },
		                              success: function(data) {
		                                  $('#ArrangeOrderQuestionTypeBtn').prop('disabled', false);
		                                var res = $.parseJSON(data);
		                                if(res.status == 'error'){
		                                  swal('Error',res.message,'error');
		                                }else{
		                                  $("#ArrangeOrderQuestionTypeModal").modal('hide'); 
		                                  $("#ResponseSuccessModal").modal('show');
		                                    $("#ResponseSuccessModal #ResponseHeading").text(res.message);
		                                    $("#ArrangeOrderDiv").sortable({
		                                        stop : function(event, ui){
		                                         }
		                                       });
		                                } 
		                              },
		                              error: function(data) {
		                                swal('Error',data,'error');
		                              }
		                            });
		                }else{
		                    swal('Error','Please fill out input boxes','error');
		                   
		                }
		                
		           });
		    });


		$('#FillBlankTypeModal').on('hidden.bs.modal', function (e) {
		  $(this)
		    .find("input,textarea,select")
		       .val('')
		       .end()
		    .find("input[type=checkbox], input[type=radio]")
		       .prop("checked", "")
		       .end();
		        $(this).data('id','');
		})


		$('#MultiChoiceQuestionTypeModal').on('hidden.bs.modal', function (e) {
		  $(this)
		    .find("input,textarea,select")
		       .val('')
		       .end()
		    .find("input[type=checkbox], input[type=radio]")
		       .prop("checked", "")
		       .end();
		        $(this).data('id','');
		})


		$('#ArrangeOrderQuestionTypeModal').on('hidden.bs.modal', function (e) {
		  $(this)
		    .find("input,textarea,select")
		       .val('')
		       .end()
		    .find("input[type=checkbox], input[type=radio]")
		       .prop("checked", "")
		       .end();
		    $("#ArrangeOrderDiv").html('');
		     $(this).data('id','');
		})

        function getLessonDetail(id)
        {
        	// alert(id);
        	// return false;
	        var path = "getLessonDetail";
	        $.ajax({
	          type: "POST",
	          url: path,
	          data: {
	            id: id
	          },
	           headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
	          success: function(result){
	            //console.log(result);
	            var res = $.parseJSON(result);

	            if(res.status == 'error'){

	            }else{
	              var data = $.parseJSON(JSON.stringify(res.message));
	              var answer = $.parseJSON(JSON.stringify(res.answers));
	              var ansHtml = '';
	              var i=0;
	              if(data.question_type==1){
	                $("#FillBlankTypeModal #shortQuestion-input").val(data.lesson_title);
	                $("#FillBlankTypeModal #fill_question_input_date").val(data.lesson_date);
	                $("#FillBlankTypeModal #fill_lesson_points").val(data.earn_points);
	                $("#FillBlankTypeModal #fill_question_description").val(data.lesson_description);
	                $("#FillBlankTypeModal #fill_shared_url").val(data.link_url);
	                $('#fill_lesson_id').val(data.id);
	                var skipped_time = data.skipped_time;
	                var file_skipped_time = skipped_time.split(":");
	                //console.log(file_skipped_time);
	                $('#fill_skip_minute').val(file_skipped_time[1]);
	                $('#fill_skip_second').val(file_skipped_time[2]);
	                var str = data.file_attached;
	                var html_data = '';
	                if(str!='' || str!=null)
	                {
                        var file_ext = str.split(".");
                        if(file_ext[1]=='mp3')
                        {
                            
                            var html_data = '<audio controls class="player" src="../storage/app/lessons/'+data.file_attached+'"></audio>';
                            
                        }
                        else if(file_ext[1]=='mp4')
                        {
                            html_data = '<video width="300" id="video"><source src="../storage/app/lessons/'+data.file_attached+'" type="video/mp4" autoplay></video>';
                        }
                        else if(file_ext[1]=='jpg' || file_ext[1]=='png' || file_ext[1]=='jpeg')
                        {
                          html_data = '<img src="../storage/app/lessons/'+data.file_attached+'" width="300" height="300">';
                        }
	                }
                    $('#fill_attached_file_show').html('<div>'+html_data+'</div>');
                    $('#fill_attached_file_show').css('display','block');
                    $('#fill_shared_url').val(data.link_url);
	                $.each(answer, function(idx,values){
	                  
	                      ansHtml += '<div data-repeater-item="" class="row m--margin-bottom-10 clonedInputsForBlank" style=""><div class="col-lg-9"><div class="input-group"><input type="text" class="form-control form-control-danger ans_input" name="['+i+'][fill_answer_input]" placeholder="Answer" id="" value="'+values.correct_options+'"></div></div><div class="col-lg-3"><a href="javascript:;" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"><i class="la la-remove"></i></a></div></div>';
	                   
	                    i++;
	                });
	                $("#FillBlankTypeModal #FillAnswerResponse").html(ansHtml);
	                $('#FillBlankTypeModal').modal('show');
	                
	              }else if(data.question_type==2){
	                $("#MultiChoiceQuestionTypeModal #multiQuestion_input").val(data.lesson_title);
	                $("#MultiChoiceQuestionTypeModal #multi_question_input_date").val(data.lesson_date);
	                $("#MultiChoiceQuestionTypeModal #multi_lesson_points").val(data.earn_points);
	                $("#MultiChoiceQuestionTypeModal #multi_question_description").val(data.lesson_description);
	                $("#MultiChoiceQuestionTypeModal #multi_shared_url").val(data.link_url);
	                $('#multi_lesson_id').val(data.id);
	                var skipped_time = data.skipped_time;
	                var file_skipped_time = skipped_time.split(":");
	                //console.log(file_skipped_time);
	                $('#multi_skip_minute').val(file_skipped_time[1]);
	                $('#multi_skip_second').val(file_skipped_time[2]);
	                var str = data.file_attached;
                    var file_ext = str.split(".");
                    var html_data = '';
                    if(file_ext[1]=='mp3')
                    {
                        
                        var html_data = '<audio controls class="player" src="../storage/app/lessons/'+data.file_attached+'"></audio>';
                        
                    }
                    else if(file_ext[1]=='mp4')
                    {
                        html_data = '<video width="300" id="video"><source src="../storage/app/lessons/'+data.file_attached+'" type="video/mp4" autoplay></video>';
                    }
                    else if(file_ext[1]=='jpg' || file_ext[1]=='png' || file_ext[1]=='jpeg')
                    {
                      html_data = '<img src="../storage/app/lessons/'+data.file_attached+'">';
                    }
                    $('#multi_attached_file_show').html('<div>'+html_data+'</div>');
                    $('#multi_attached_file_show').css('display','block');
                    $('#multi_shared_url').val(data.link_url);
	                $.each(answer, function(idx,values){
	                        if(values.correct_options==1){
	                            var checked = 'checked';
	                            var chk_val = 1;
	                        }else{
	                            var checked = '';
	                            var chk_val = 0;
	                        }
	                      ansHtml += '<div data-repeater-item="" class="form-group m-form__group row align-items-center clonedOptionsForMultiChoice" style=""><div class="col-md-9"><div class="input-group m-form__group"><span class="input-group-addon"><input type="checkbox" class="check_answer" value="'+chk_val+'" '+checked+'></span><input type="text" class="form-control options_value" aria-label="Enter Option" value="'+values.options+'"></div><div class="d-md-none m--margin-bottom-10"></div></div><div class="col-md-3"><div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill"><span><i class="la la-trash-o"></i><span>Delete</span></span></div></div></div>';
	                   
	                    
	                });
	                $("#MultiChoiceQuestionTypeModal #MultiAnswerResponse").html(ansHtml);

	                $('#MultiChoiceQuestionTypeModal').modal('show');

	              }else{
	                $("#ArrangeOrderQuestionTypeModal #arrangeQuestion_input").val(data.lesson_title);
	                $("#ArrangeOrderQuestionTypeModal #arrange_question_input_date").val(data.lesson_date);
	                $("#ArrangeOrderQuestionTypeModal #arrange_lesson_points").val(data.earn_points);
	                $("#ArrangeOrderQuestionTypeModal #arrange_question_description").val(data.lesson_description);
	                $("#ArrangeOrderQuestionTypeModal #arrange_shared_url").val(data.link_url);
	                $('#arrange_lesson_id').val(data.id);
	                var skipped_time = data.skipped_time;
	                var file_skipped_time = skipped_time.split(":");
	                //console.log(file_skipped_time);
	                $('#arrange_skip_minute').val(file_skipped_time[1]);
	                $('#arrange_skip_second').val(file_skipped_time[2]);
	                var str = data.file_attached;
                    var file_ext = str.split(".");
                    var html_data = '';
                    if(file_ext[1]=='mp3')
                    {
                        
                        var html_data = '<audio controls class="player" src="../storage/app/lessons/'+data.file_attached+'"></audio>';
                        
                    }
                    else if(file_ext[1]=='mp4')
                    {
                        html_data = '<video width="300" id="video"><source src="../storage/app/lessons/'+data.file_attached+'" type="video/mp4" autoplay></video>';
                    }
                    else if(file_ext[1]=='jpg' || file_ext[1]=='png' || file_ext[1]=='jpeg')
                    {
                      html_data = '<img src="../storage/app/lessons/'+data.file_attached+'">';
                    }
                    $('#arrange_attached_file_show').html('<div>'+html_data+'</div>');
                    $('#arrange_attached_file_show').css('display','block');
                    $('#arrange_shared_url').val(data.link_url);
	                var j=1;
	                resultres = '';
	                $.each(answer, function(idx,values){
	                      ansHtml += '<div data-repeater-item="" class="m--margin-bottom-10 clone_number" id="clone'+j+'" style=""><div class="input-group"><span class="input-group-addon"><i class="la la-check"></i></span><input type="text" class="form-control form-control-danger incorrect_order" placeholder="Enter Question Part" value="'+values.options+'"><span class="input-group-btn" data-repeater-delete=""><a href="javascript:;" class="btn btn-danger m-btn m-btn--icon"><i class="la la-close"></i></a></span></div></div>';

	                       resultres += '<li id="'+j+'">'+values.correct_options+'</li>';
	                     
	                   j++;
	                    
	                });
	                $("#ArrangeOrderQuestionTypeModal #ArrangeOrderPartsDiv").html(ansHtml);
	                $("#ArrangeOrderQuestionTypeModal #ArrangeOrderDiv").html(resultres);


	                $('#ArrangeOrderQuestionTypeModal').modal('show');
	                 $("#ArrangeOrderDiv").sortable({
	                                        stop : function(event, ui){
	                                         }
	                                       });

	              }

	              if(data.status==0){
	                $("#ArrangeOrderQuestionTypeBtn").css('display','none');
	                $("#FillBlankTypeBtn").css('display','none');
	                $("#MultiChoiceQuestionTypeBtn").css('display','none');
	              }else{
	                $("#ArrangeOrderQuestionTypeBtn").css('display','block');
	                $("#FillBlankTypeBtn").css('display','block');
	                $("#MultiChoiceQuestionTypeBtn").css('display','block');
	              }

	            }


	          },
	          error: function(){
	            alert("Error");
	          }
	        }); 
	  	} 
		</script>
		<style>
		ul#ArrangeOrderDiv li {
		    background: #8bc34a;
		    padding: 10px 15px;
		    margin: 3px;
		    color: #fff;
		    border-radius: 4px;
		}
		ul#ArrangeOrderDiv li {
		    position: relative;
		    left: -21px;
		}
		#ResponseHeading{
		 color: #4CAF50;
		}
		</style>
		@endsection
			
