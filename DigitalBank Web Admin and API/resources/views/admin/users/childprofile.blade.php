<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Users
		</title>
		@extends('layouts.admin')
    	@section('content')
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						<!-- BEGIN: Subheader -->
						<div class="m-subheader ">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title m-subheader__title--separator">
										Child Profile
									</h3>
									<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							            <li class="m-nav__item m-nav__item--home">
							                <a href="{{url('/dashboard')}}" class="m-nav__link m-nav__link--icon">
							                  <i class="m-nav__link-icon la la-home"></i>
							                </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a href="{{url('/admin/users')}}" class="m-nav__link">
							                <span class="m-nav__link-text">
							                  Users
							                </span>
							              </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a href="" class="m-nav__link">
							                <span class="m-nav__link-text">
							                  {{$data->user_name}}
							                </span>
							              </a>
							            </li>
									</ul>
								</div>
								<div>
								</div>
							</div>
						</div>
						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row">
								<div class="col-xl-12">
									<div class="m-portlet m-portlet--mobile">
										<div class="m-portlet__body">
											<div class="row m-row--no-padding m-row--col-separator-xl">
										<div class="col-xl-4">
											<!--begin:: Widgets/Daily Sales-->
											<!-- <div class="m-widget14">
												<div class="m-widget14__header m--margin-bottom-30">
													<h3 class="m-widget14__title">
														Profile Pic
													</h3>
												</div>
												<div class="m-widget14__chart">
													<img src="{{url('assets/app/media/img/users/user4.jpg')}}" height="200" width="150">
												</div>
											</div> -->
											<div class="card" style="width:400px; height: 465px;">
											@php if($data->avtar!='')
											{ @endphp
											  <img class="card-img-top" src="{{url('storage/app/profile/').'/'.$data->avtar}}" alt="Card image" style="width:400px; height: 400px;">
											@php } else { @endphp
												<img class="card-img-top" src="{{url('storage/app/profile/user.jpg')}}" alt="Card image">
											@php } @endphp
											  <div class="card-body">
											    <h4 class="card-title">{{$data->user_name}}</h4>
											  </div>
											</div>
											<!--end:: Widgets/Daily Sales-->
										</div>
										<div class="col-xl-8">
												<div class="row  align-items-center m-widget14">
													<div class="m-portlet m-portlet--tabs">
													<div class="m-portlet__head">
														<div class="m-portlet__head-tools">
														<ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
															<li class="nav-item m-tabs__item">
																<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_1" role="tab">
																	<i class="la la-cog"></i>
																	Profile
																</a>
															</li>
															<li class="nav-item m-tabs__item">
																<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_2" role="tab">
																	<i class="fa fa-child"></i>
																	Parent Details
																</a>
															</li>
															<li class="nav-item m-tabs__item">
																<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_3" role="tab">
																	<i class="fa fa-child"></i>
																	QR Code
																</a>
															</li>
														</ul>
														</div>
													</div>
													<div class="m-portlet__body">
													<div class="tab-content">
														<div class="tab-pane active col-xl-12" id="m_tabs_6_1" role="tabpanel" style="width: 800px; height: 300px;">
															
																<div class="form-group m-form__group row">
																	<div class="col-lg-4">Email
																	</div>
																	<div class="col-lg-8">{{$data->email}}
																	</div>
																</div>
																<div class="form-group m-form__group row">
																	<div class="col-lg-4">Country Name
																	</div>
																	<div class="col-lg-8">{{$data->country_name}}
																	</div>
																</div>
																<div class="form-group m-form__group row">
																	<div class="col-lg-4">Country Code
																	</div>
																	<div class="col-lg-8">{{$data->country_code}}
																	</div>
																</div>
																<div class="form-group m-form__group row">
																	<div class="col-lg-4">Mobile
																	</div>
																	<div class="col-lg-8">{{$data->mobile}}
																	</div>
																</div>
																<div class="form-group m-form__group row">
																	<div class="col-lg-4">School Name
																	</div>
																	<div class="col-lg-8">{{$data->school_name}}
																	</div>
																</div>
																<div class="form-group m-form__group row">
																	<div class="col-lg-4">Date of Birth
																	</div>
																	<div class="col-lg-8">{{$data->dob}}
																	</div>
																</div>
																<div class="form-group m-form__group row">
																	<div class="col-lg-4">Address
																	</div>
																	<div class="col-lg-8">{{$data->address}}
																	</div>
																</div>
														</div>
														<div class="tab-pane" id="m_tabs_6_3" role="tabpanel" style="width: 800px; height: 300px;">
															<div class="form-group m-form__group row">
																	<div class="col-lg-12"><img height="250" width="250" src="{{url('public/userQrcodes/'.$data->qr_code)}}">											</div>
																</div>
														</div>
													</div>
													</div>
												</div>	
												</div>
											<!-- </div> -->
											<!--end:: Widgets/Profit Share-->
										</div>
										</div>
										</div>
									</div>
								</div>
								<div class="col-xl-12" id="#panel_transaction">
									<div class="panel-group">
									  	<div class="panel panel-default">
										    <div class="panel-heading" id="load_transaction" data-toggle="collapse" href="#panel_transaction_data">
										      <h4 class="panel-title">
										        <a>Transaction Passbook</a>
										      </h4>
										    </div>
										    <div id="panel_transaction_data" class="panel-collapse collapse">
										      <div class="child_transaction_datatable col-xl-12" id="local_data" data-id="{{$data->id}}">
												</div>
										    </div>
									  </div>
									</div>
								</div>
							</div>
							<!--End::Main Portlet-->   
							<!--Begin::Main Portlet-->
							
							<!--End::Main Portlet-->
						</div>
					</div>
				</div>
			</div>
		@endsection
		@section('js')
		<!--<script type="text/javascript" src="{{url('/assets/admin/js/parent.js')}}"></script>-->
		<script>
		    var user_id = $('#local_data').attr('data-id');
		    $('#load_transaction').on('click',function(){
            transaction_datatable = $('.child_transaction_datatable').mDatatable({
                // datasource definition
              data: {
              type: 'remote',
              source: {
                  read: {
                      url: '../../getAllChildTransaction/'+user_id,
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                              searchhotness: ''
                          }
                      },
                      map: function(raw) {
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                               dataSet = raw.data;
                          }
                          return dataSet;
                      },
                  }
              },
              pageSize: 10,
                saveState: {
                    cookie: false,
                    webstorage: false
                },
        
                serverPaging: true,
                serverFiltering: false,
                serverSorting: false
            },
            // layout definition
            layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },
        
            // column sorting
            sortable: false,
        
            pagination: true,
        
            // columns definition
            columns: [
            {
              field: "S_No",
              title: "S.No",
              textAlign: 'center',
              width: 50,
            },
            {
              field: "from_id",
              title: "From Account",
              width: 130      
            },
            {
              field: "to_id",
              title: "To Account",
              width: 130       
            },
            {
              field: "transaction_points",
              title: "Points Transaction",
              width: 220,
              textAlign:'center'       
            },
            {
              field: "transaction_type",
              title: "Transanction Type",
              width: 220,
              textAlign:'center',
              template: function (row) {
                if(row.transaction_type=='1')
                {
                    return '\
                    <div>Cr.</div>\
                    ';
                }
                return '\
                <div >Db.</div>\
                ';
              }  
            },
            {
              field: "remarks",
              title: "Remarks",
              width: 220            
            }]
            });
        });
		</script>
		<style type="text/css">
			.panel-default .panel-heading a {
	          color: #fff;
	          }
	        .panel-default .panel-heading {
	          background-color: #5757f3;
	          border-top-left-radius: 3px;
	          border-top-right-radius: 3px;
	          padding: 10px 15px;
	          margin: 0 0 3px;
	        }
		</style>
		
		@endsection
			
