<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Users
		</title>
		@extends('layouts.admin')
    	@section('content')
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						<!-- BEGIN: Subheader -->
						<div class="m-subheader ">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title m-subheader__title--separator">
										All Users
									</h3>
									<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							            <li class="m-nav__item m-nav__item--home">
							                <a href="{{url('/dashboard')}}" class="m-nav__link m-nav__link--icon">
							                  <i class="m-nav__link-icon la la-home"></i>
							                </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a href="{{url('/admin/users')}}" class="m-nav__link">
							                <span class="m-nav__link-text">
							                  All Users
							                </span>
							              </a>
							            </li>
									</ul>
								</div>
								<div>
								</div>
							</div>
						</div>
						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row">
							    <div class="col-xl-12">
    							    <div class="pull-right">
										<button class="btn btn-primary m-btn m-btn--icon" id="addUser">
											<span>
												<i class="la la-plus"></i>
												<span>
													Add User
												</span>
											</span>
										</button>
    								</div>
    								<div class="pull-right" style="margin-right:20px;">
    									<button class="btn btn-primary m-btn m-btn--icon" id="store_reward">
    										<span>
											<i class="la la-plus"></i>
											<span>
												Add Reward
											</span>
    										</span>
    									</button>
    								</div>
								</div>
								<div class="col-xl-12">
									<div class="m-portlet m-portlet--mobile">
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption">
												<div class="m-portlet__head-title float-left col-xl-2">
													<h3 class="m-portlet__head-text">Users</h3>
												</div>
												<div class="float-left col-xl-3" style="margin-top: 15px;">
													<select class="form-control mx-auto" id="country_id" name="country_id">
                        		                        <option value="">Select Country</option>
                            						    @foreach($countries as $item)
                            							<option value="{{$item->id}}">{{$item->country_name}}</option>
                            							@endforeach
                        							</select>
												</div>
												<div class="float-left col-xl-2">
													<select class="form-control mx-auto" id="userlist" style="margin-top: 15px; ">
												    <option value="">Select User</option>
													<option value="2">Student</option>
													<option value="3">Parents</option>
  													</select>
												</div>
										<div class="float-left col-xl-2">
													<select class="form-control mx-auto" id="user_status" style="margin-top: 15px; ">
												    <option value="">Select Status</option>
													<option value="1">Active</option>
													<option value="2">Inactive</option>
													<option value="3">Archieve</option>
  													</select>
												</div>
												<div class="float-left col-xl-2">
													<div class="m-input-icon m-input-icon--left" style="margin-top: 15px;">
														<input type="text" class="form-control m-input m-input--solid" placeholder="Search User" id="generalSearch">
														<span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
													</div>
												</div>
											</div>
											<!--<div class="pull-right" style="margin-top: 20px;">
												<button class="btn btn-primary m-btn m-btn--icon" id="addUser">
													<span>
														<i class="la la-plus"></i>
														<span>
															Add User
														</span>
													</span>
												</button>
											</div>
											<div class="pull-right" style="margin-top: 20px;">
												<button class="btn btn-primary m-btn m-btn--icon" id="store_reward">
													<span>
														<i class="la la-plus"></i>
														<span>
															Add Reward
														</span>
													</span>
												</button>
											</div>-->
										</div>
										<div class="m-portlet__body">
											<div class="users_datatable" id="local_data">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--End::Main Portlet-->   
							<!--Begin::Main Portlet-->
							
							<!--End::Main Portlet-->
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="AddUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
		    <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title" id="exampleModalLabel">
		               Add User
		            </h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">
		                    &times;
		                </span>
		            </button>
		        </div>
		        <div class="modal-body">
		          {{ csrf_field() }}
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Country Name:
		                    </label>
		                    <select class="form-control mx-auto" id="country_name" name="country_name">
		                        <option value="">Select Country</option>
    						    @foreach($countries as $item)
    							<option value="{{$item->id}}">{{$item->country_name}}</option>
    							@endforeach
							</select>
		                </div>
		                <div class="form-group">
		                    <label for="user_name" class="form-control-label">
		                        Username *:
		                    </label>
		                    <input type="text" class="form-control" id="user_name" name="user_name">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Full Name :
		                    </label>
		                    <input type="text" class="form-control" id="name" name="name">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Email :
		                    </label>
		                    <input type="email" class="form-control" id="email" name="email">
		                </div>
		                <div class="form-group">
		                    <label for="country_code" class="form-control-label">
		                        Country Code:
		                    </label>
		                    <input type="text" class="form-control" id="country_code" name="country_code">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        User Mobile :
		                    </label>
		                    <input type="text" class="form-control" id="mobile" name="mobile">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        User Type *:
		                    </label>
		                    <select class="form-control" id="type">
		                    	<option value="">Please Select User Type</option>
		                    	<option value="2">Student</option>
		                    	<option value="3">Parent</option>
		                    </select>
		                </div>
		                <div class="form-group" id="PasswordInput">
                    
                		</div>
		            
		        </div>
		        <div class="modal-footer">
		            <button type="button" class="btn btn-secondary" data-dismiss="modal">
		                Close
		            </button>
		            <button type="button" class="btn btn-primary" id="formSubmit">
		                Save changes
		            </button>
		        </div>
		    </div>
			</div>
			</div>
			<div class="modal fade" id="ChangePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
		    <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title" id="exampleModalLabel">
		               
		            </h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">
		                    &times;
		                </span>
		            </button>
		        </div>
		        <form name="fm-student" id="change_password_form">
		        <div class="modal-body">
		          {{ csrf_field() }}
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Password:
		                    </label>
		                    <input type="password" class="form-control" id="change_password" name="change_password" accept=".csv">
		                </div>
		            
		        </div>
		        <div class="modal-footer">
		            <button type="button" class="btn btn-secondary" data-dismiss="modal">
		                Close
		            </button>
		            <button type="submit" class="btn btn-primary" id="ChangePasswordBtn">
		                Save changes
		            </button>
		        </div>
		        </form>
		    </div>
		</div>
		</div>
		<div class="modal fade" id="AddRewardPontsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
		    <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title" id="exampleModalLabel">
		               Add Reward Points
		            </h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">
		                    &times;
		                </span>
		            </button>
		        </div>
		        <div class="modal-body">
		          {{ csrf_field() }}
		                
		                <input type="hidden" class="form-control" id="user_id" name="user_id">
		                
		                <div class="form-group">
		                    <label for="reward_points" class="form-control-label">
		                        Reward Points :
		                    </label>
		                    <input type="text" class="form-control" id="reward_points" name="reward_points">
		                </div>		                		                		              
		        </div>
		        <div class="modal-footer">
		            <button type="button" class="btn btn-secondary" data-dismiss="modal">
		                Close
		            </button>
		            <button type="button" class="btn btn-primary" id="add_points">
		                Save changes
		            </button>
		        </div>
		    </div>
			</div>
			</div>
		<div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		        <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                
		                <form name="fm-student" id="arrange-order-form">
		                <div class="modal-body">
		                    <h5 id="ResponseHeading"></h5>
		                </div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadUserDatatable">
		                        OK
		                    </button>
		                </div>
		                </form>
		            </div>
		        </div>
		    </div>
		@endsection
		@section('js')
		<script type="text/javascript" src="{{url('/assets/admin/js/users.js')}}"></script>
		<script type="text/javascript">
		    $("#country_code,#mobile").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
			$('#addUser').click(function(e){
			    e.preventDefault();
			    $('#AddUserModal').modal('show');
			    $('#AddUserModal').data('id','');
			    $('#country_name').val('');
			    $('#user_name').val('');
			    $('#name').val('');
			    $('#email').val('');
			    $('#mobile').val('');
			    $('#type').val('');
			    $('#parent_list').hide();
			    $('#student_list').hide();
			    $('#parent_tag').val('');
			    $('#student_tag').val('');
			    var pwd_input = '<label for="password" class="form-control-label">Password *:</label><input type="password" class="form-control" id="password" name="password">';
				$("#PasswordInput").html(pwd_input);
		  	});
			// $('#type').on('change',function(){
			// 	var value = $(this).val();
			// 	if(value=='')
			// 	{
			// 		$('#parent_list').hide();
			// 		$('#student_list').hide();
			// 	}
			// 	if(value=='3')
			// 	{
			// 		$('#parent_list').hide();
			// 		$('#student_list').show(2000);
			// 	}
			// 	if(value=='2')
			// 	{
			// 		$('#student_list').hide();
			// 		$('#parent_list').show(2000);
			// 	}
			// });
			$('#country_name').on('change',function(){
			    var id = $(this).val();
			    $.ajax({
                method: 'POST',
                url: 'getCountryCode',
                data: {
                  id: id
                },
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                  var res = $.parseJSON(data);
                  if(res.status == 'error'){
                    /*swal('Error',res.message,'error');*/
                    $('#country_code').val('');
                  }else{
                    $('#country_code').val(res.data.country_code);
                  } 
                },
                error: function(data) {
                  swal('Error',data,'error');
                }
              });
			});
			$('#store_reward').click(function(e){ 
			//addUserPoints
			    e.preventDefault();
			    $('#AddRewardPontsModal').modal('show');			    
			    $('#reward_points').val('');			    			    
		  	});
		</script>
		@endsection
			
