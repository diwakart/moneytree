<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DUMO-privacy-policy</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <style>
        .h1,
        .h2,
        .h3,
        .h4,
        .h5,
        .h6,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-weight: 600;
        }

        .background-color {
            background: #ed7d31;
            color: #fff;
            padding: 20px;
        }

        .border_section {
            border: 1px solid #c16a31;
            margin-bottom: -1px;
        }

            .border_section img {
                padding: 5%;
                margin: auto;
            }

            .border_section .col-md-2 {
                border-right: 1px solid #ed7d31;
            }

        .aboutright {
            background: #ed7d31;
            padding: 25px 20px;
            color: #fff;
            text-align: left;
        }

            .aboutright img {
                margin: 10px auto;
            }

        .aboutright_1 {
            background: #ed7d31;
            padding: 25px 20px;
            color: #fff;
            text-align: left;
        }

            .aboutright_1 p {
                display: flex;
                justify-content: left;
                align-items: center;
            }

            .aboutright_1 img {
                width: 40px;
                margin-right: 10px;
                height: 45px;
            }
    </style>
</head>

<body>
    <section class="main-sec">
        <div class="container">
            <div class="main-heading">
                <h1 class="text-center">Dumo Service Terms</h1>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-all paragraph-all">
                        <h2>Introduction</h2>
                        <p>
                            This document (the “Dumo Service Terms”) provides information surrounding your legal rights and obligations in relation to your use of the Dumo service (“Dumo”), including use of the Dumo App.
                        </p>
                        <p>
                            Parents should read this document carefully with their children to ensure they understand how Dumo works and how to use Dumo safely before you submit your order to us. These Dumo Service Terms tell you who we are, information about Dumo, how you and we may change or end our contract with you, how we charge you, what to do if there is a problem and other important information. If you think there is a mistake in these Dumo Service Terms, please contact us to discuss.
                        </p>
                        <p>
                            These Dumo Service Terms should be read in conjunction with the Privacy Policy which set out further terms in relation to how we collect and use your personal information.
                        </p>
                        <h2>Dumo basics</h2>
                        <p>Dumo is a service which helps young people learn to manage money more responsibly.</p>
                        <h3>Who are we?</h3>
                        <p>We are <b>Dumo Limited</b>, a company registered in Singapore. Our company registration number is <b>201804855M.</b> Our address is: <b>128 Prinsep St, 01, Singapore 188655.</b></p>
                        <p>You can contact us in the following ways:</p>
                        <ul>
                            <li><b><u>Support Services</u></b>: Our support team is standing by to help from 9am to 6pm on Monday to Friday. Please contact us using the in-app chat or email us at help@dumohere.com</li>
                            <li><u><b>General Enquiries</b></u>: Please contact us by emailing hello@dumohere.com;</li>
                            <li><u><b>Write to us</b></u>: Dump, 128 Prinsep St, 01, Singapore 188655</li>
                        </ul>
                        <h3>How does Dumo work?</h3>
                        <p>Dumo is a web and mobile app-based service for parents who wish to improve the money management skills of their children aged 8 and upwards. As part of Dumo, parents and young people receive access to a powerful money management mobile app on Android and iOS (the <b>“Dumo App”</b>). A parent adds money his/her account. A young person’s account will earn money when they complete learning tasks in the Dumo app. The money that a young person earns will be deducted from the money in parent’s Dumo account. </p>
                        <h3>How do I get up and running with Dumo?</h3>
                        <p>In order to start using Dumo you need to read and agree to the following documents which set out the terms and conditions on which Dumo and the Dumo App is provided:</p>
                        <ul>
                            <li><u><b>Dumo Service Terms: </b></u>(this document) which sets out the terms and conditions under which we will provide you with access to Dumo via the Dumo App;</li>
                            <li><u><b>Dumo Privacy Policy: </b></u>this sets out the basis on which your personal data that we collect from you or that you provide us will be processed and can be found here<a href="#">Privacy Policy.</a> </li>
                        </ul>
                        <h3>What personal data do you store?</h3>
                        <p>Dumo values your data and your privacy. We need to securely store personal and non-personal information in order to operate Dumo effectively. For a detailed explanation of the personal information that we collect and the purposes for which we use it, please read our <a href="#">Privacy Policy.</a> </p>
                        <h3>What limits does the Dumo service have?</h3>
                        <p>For the safety of Dumo users, Dumo will conduct further fraud checks on the usage of your account. If suspicious activity is identified, unless we are restricted by laws that apply to Dumo, you will be notified, and your account may be blocked for security.</p>
                        <h3>How much does it cost to join Dumo?</h3>
                        <p>
                            Downloading the Dumo mobile app and joining Dumo is free for both parent and young person.
                        <p>
                            Parents will have to pay for the points which are reloaded to their Dumo account. Price of the points will be shown in Dumo before a parent proceeds to purchase and pays for the purchase of the points.

                        </p>
                        <p>
                            Any changes changes to the price of points will be reflected in Dumo and Dumo app.
                        </p>
                        <h3>How do we collect fees for purchase of points?</h3>
                        <p>
                            Parents will be charged for the points they purchase in Dumo once they have confirmed the purchase. All payments will be processed via third party payment service provider, Stripe Inc. Through Stripe, parents are able to make payment using their Stripe account, Credit or Debit Card and other payment methods made available by Stripe.
                        </p>
                        <p>
                            The list of payment methods available are shown here:<a href="https://stripe.com/payments/payment-methods-guide">https://stripe.com/payments/payment-methods-guide</a>
                        </p>
                        <p><i>Stripe Inc is a technology company. Its software allows individuals and businesses to receive payments over the Internet. Stripe provides the technical, fraud prevention, and banking infrastructure required to operate on-line payment systems.</i></p>
                        <p>
                            <i>
                                Website: www.stripe.com<br />
                                E-mail: info@stripe.com
                            </i>
                        </p>
                        <h3>What if we are not able to collect the fee for purchase of points?</h3>
                        <p>If we are unable to collect the payment for the purchase of points, no points will be reloaded into the parent’s Dumo account. We may re-attempt to collect the payment several times to make sure we maximise your chance of being able to continue use the Dumo points to motivate your kids to learn.</p>
                        <h3>How do I cancel my contract with Dumo?</h3>
                        <p>Your use of Dumo and the effectiveness of these Terms will continue until terminated by you or us. You can cancel your use with Dumo at any time. If you wish to cancel your use Dumo, you may simply delete your Dumo account. </p>
                        <h3>What happens to points in my Dumo account when I cancel?</h3>
                        <p>When you delete or cancel your Dumo account, the points in your account will expire. No refunds will be made for the remaining points in your account. </p>
                        <h3>Can Dumo terminate my access to Dumo?</h3>
                        <p>We may terminate your access to Dumo (including access to the Dumo App) immediately by written notice to you (including by email):</p>
                        <ul>
                            <li>
                                if you commit a material or persistent breach of these Dumo Service Terms which you fail to remedy within a reasonable period;
                            </li>
                            <li>
                                if you breach any of the usage restrictions set out in these Dumo Service Terms; or
                            </li>
                            <li>
                                if you breach the terms of the Dumo Cardholder Agreement
                            </li>
                        </ul>
                        <p>On termination by you or us for any reason:</p>
                        <ul>
                            <li>the rights and licenses granted to you under these Dumo Service Terms shall cease;</li>
                            <li>you must immediately cease all activities authorised by these Dumo Service Terms, including your use of any service of Dumo and the Dumo App;</li>
                            <li>you must immediately delete or remove the Dumo App from all devices, and immediately destroy all copies of the Dumo App then in your possession, custody or control and certify to us that you have done so;</li>
                            <li>if necessary, we may invalidate your login details and cease providing you with access to Dumo.</li>

                        </ul>

                        <h2>Dumo App</h2>
                        <p>Once you have signed up to Dumo, you can access the Dumo App to manage your Dumo accounts.</p>
                        <h3>What does the Dumo App do?</h3>
                        <p>
                            The Dumo App allows young people to manage their Dumo Card and parents to give them money and have visibility of their children’s account activity. The Dumo App is slightly different for young people and parents.
                        </p>
                        <p>
                            Young people can use the Dumo App to view transactions, check the balance of their Dumo card, temporarily lock their Dumo Card and perform other activities. Parents can view the accounts of each of their children, load money onto their children’s Dumo Cards, set-up recurring allowances and perform other activities. Features will continue to be added to the Dumo App as Dumo evolves.
                        </p>
                        <h3>Do parents need to use the Dumo App?</h3>
                        <p>
                            We strongly recommend the Dumo App be used by both parents and young people to get the best experience from the Dumo service.
                        </p>
                        <h3>What is the Parent Account? </h3>
                        <p>Each parent who joins Dumo will be given a parent account. A parent in Dumo will have the following functions:</p>
                        <ul>
                            <li> The ability to top-up your parent account will points. These points will be transferred to your Young person’s account when the young person completes a learning task in Dumo.</li>
                            <li>
                                The ability to increase the point value of each task in Dumo app for your Young person.
                            </li>
                            <li>
                                The ability to monitor or receive report on the activity of your Young person in Dumo.
                            </li>
                        </ul>
                        <h3>Loading the Dumo Parent Account</h3>
                        <p>
                            You can view the balance of your Parent Account in the Dumo app. The points in your parent account will be transferred to your Young person’s Dumo account when they complete a learning task on Dumo.
                        </p>
                        <p>
                            If you would like to purchase more points for your Dumo parent account, you can buy points on the Dumo app. Prices will be quoted in the mobile app and payments for the purchase will be processed by our third party payment provider.
                        </p>
                        <h3>My child doesn’t have an iPhone or Android phone, can they still use Dumo?</h3>
                        <p>Yes. Many of the young people using our service do not have an iPhone or Android phone. They manage their accounts using the household iPad, Android tablet, iPod Touch or their parent’s phone. Please visit the <a href="">Dumo FAQs</a> for more information.</p>
                        <h3>Permission to download the Dumo App</h3>
                        <p>The person downloading the Dumo App must obtain permission from the owner of the device to download or stream a copy of the Dumo App onto the device. Please note that the device owner may be charged by their service provider for internet access when using the Dumo App on the device.</p>
                        <h3>How can I keep the Dumo App secure?</h3>
                        <p>Using the Dumo App allows you and your child to manage your Dumo account online. You should keep your phone/tablet and account credentials safe at all times, but there are some other steps you and your child can take to help protect yourselves from theft:</p>
                        <ul>
                            <li>Close the Dumo App when you’re not using it. It will log you out anyway after around 30 seconds of inactivity, but better to be safe than sorry</li>
                            <li>Set a unique password for Dumo that you do not use on other websites or services</li>
                            <li>Let us know if any of your account details change. And remember, we will never contact you asking for any security details</li>
                        </ul>
                        <h3>Licence, usage restrictions and ownership of the Dumo App</h3>
                        <p>
                            You should make sure that you download the Dumo App from legitimate sources as advised in our activation process.
                        </p>
                        <p>
                            In consideration of you agreeing to abide by these Dumo Service Terms, Dumo hereby grants to you a non-exclusive, non-transferable licence to use the Dumo App on the terms set out in these Dumo Service Terms and the Privacy Policy. Dumo reserves all other rights.
                        </p>
                        <p>
                            You are permitted to use the Dumo App for personal purposes only.
                        </p>
                        <p>
                            In relation to all or any part of the Dumo App and any related technology, you must not:
                        </p>
                        <ul>
                            <li>copy or alter it in any way;</li>
                            <li>
                                make it available to anybody else without our consent;

                            </li>
                            <li>use it in any unlawful manner or for any unlawful purpose </li>
                            <li>use it for fraudulent or malicious acts, such as hacking the Dumo App;</li>
                            <li>transmit any material that is defamatory, offensive or otherwise objectionable;

                            <li>
                            <li>
                                derive income from your use of it;
                            </li>
                            <li>
                                use it in a way that damages, disables, impairs or inhibits use for other people;

                            </li>
                            <li>
                                collect information or data from it and our systems to try to decipher transmissions to and from our servers.
                            </li>
                        </ul>
                        <p>
                            If you use the Dumo App and related technology in any way that is prohibited (as set out above), this could result in you being investigated and being suspended from using Dumo.
                        </p>
                        <p>
                            Please note that all intellectual property rights in Dumo, the Dumo App and related technology belong to our licensors or us and you have no rights in, or to it other than the licence to use it in accordance with these Dumo Service Terms.
                        </p>
                        <p>
                            You acknowledge that the Dumo App has not been developed to meet your individual requirements.
                        </p>

                        <h3>Third party sites available through the Dumo App</h3>
                        <p>The Dumo App may contain links to other independent third-party websites <b>(“Third-party Sites”)</b>. Third-party Sites are not under our control, and we are not responsible for and do not endorse their content or their privacy policies (if any). You will need to make your own independent judgment regarding your interaction with any Third-party Sites, including the purchase and use of any products or services accessible through them</p>
                        <h3>Dumo offers</h3>
                        <p>
                            From time-to-time we may run promotional offers, the terms of which will be set out in the relevant communication.
                        </p>
                        <p>
                            Each promotion will automatically close at the end of the specified period, at which point no further participation will be possible. Where no period is specified the relevant promotion will end when it is removed from our webpage or from Dumo App, or when we specifically notify you that the promotion is being terminated.
                        </p>
                        <p>
                            Promotions are open to those who accept and comply with these Dumo Service Terms (as well as any other requirements set out in the promotion communication).

                        </p>
                        <p>
                            We reserve the right to change the rules relating to promotions or withdraw a promotion at any time. We also reserve the right to terminate a promotion or revoke any benefits granted thereunder, if we believe a user is abusing the offer, or not complying with its terms.

                        </p>
                        <p>Where applicable we will load any offer amounts onto Dumo Cards or the parent wallet as soon as reasonably practicable once promotion requirements are met, but in some instances delays may occur. If a delay does occur we will endeavour to load any offer amounts within 20 days.</p>

                        <h2>Help and support</h2>
                        <3>What happens if things go wrong?</3>
                        <p>
                            If there is a technical problem with the Dumo App or the Dumo service, please contact the Dumo Support team:
                        </p>
                        <ul>
                            <li>
                                <b>By emailing: </b>help@dumohere.com ; or

                            </li>
                            <li>
                                <b>Chatting with us in the Dumo app</b>
                            </li>

                        </ul>
                        <h3>Our responsibility for loss or damage suffered by you</h3>
                        <p>
                            If we fail to comply with these Dumo Service Terms, we are responsible for loss or damage you suffer that is a foreseeable result of our breaking this contract or our failing to use reasonable care and skill, but we cannot be responsible where the loss or damage was not our fault or was not caused by our breach.
                        </p>
                        <p>
                            We do not exclude or limit in any way our liability to you where it would be unlawful to do so. This includes liability for death or personal injury caused by our negligence or the negligence of our employees, agents or subcontractors; for fraud or fraudulent misrepresentation; for breach of your legal rights in relation to use of Dumo.
                        </p>
                        <p>
                            We are not liable for any business losses. We only supply the products for domestic and private use. If you use Dumo for any commercial, business or re-sale purpose we will have no liability to you for any loss of profit, loss of business, business interruption, or loss of business opportunity.
                        </p>
                        <p>
                            We will not be liable or responsible for any failure to perform, or delay in performance of any of our obligations under these Dumo Service Terms (including access to the Dumo App) that occurs as a result of your device ceasing to operate properly, a third party error or any other event outside of our reasonable control.

                        </p>
                        <h2>Complaints procedure</h2>
                        <h3>How can I register a complaint with Dumo?</h3>
                        <p>If You are not satisfied with any element of the service You receive, any complaints should be made to Our customer services team using the contact details below. Calls may be monitored or recorded for training purposes.</p>
                        <p><b>By email:</b> You can send us a message to<a href="#">complaints@dumohere.com</a> .</p>
                        <p><b>By post:</b> Send your query to <b>Dumo 128 Prinsep St, 01, Singapore 188655</b></p>
                        <h3>What information will I need to provide if I complain?</h3>
                        <p>The more information you can provide, the more easily we will be able to help you. Please provide details of the following:</p>
                        <ul>
                            <li>
                                your contact details, including a daytime phone number;
                            </li>
                            <li>
                                your Dumo account number.
                            </li>
                            <li>
                                the nature of your complaint;
                            </li>
                            <li>
                                any specific purchases, dates, locations that are relevant to the complaint;
                            </li>
                            <li>
                                value of funds impacted (if relevant);
                            </li>
                            <li>
                                if there is anything that can be done at this stage to resolve the issue.

                            </li>
                        </ul>
                        <h3>What happens behind the scenes after I complain?</h3>
                        <p>
                            1. The complaint is collected by one of our customer service representatives
                        </p>
                        <p>
                            2. A member of the Dumo team will email you within one business day that your complaint is being actioned
                        </p>
                        <p>
                            3. Dumo will try to resolve the issue within one week and if this is not possible, we will be in touch to explain why and what the next steps are
                        </p>
                        <p>
                            4. For more complex disputes, Dumo will continue to keep you updated of progress at least weekly

                            <h2>Other Important Terms</h2>
                        <p>
                            <b>From time to time we may need to make changes to these terms or the Dumo service. </b> This may include changes to the price of points for Dumo or the things we need to charge you for. Where we make material changes to these Dumo Service Terms or the Privacy Policy we’ll notify you – e.g. by email or by displaying a prominent notice on our website or the Dumo App and provide you details of the changes. In most cases, we will notify you in advance, and your continued use of the Dumo service after the changes have been made will constitute your acceptance of the changes. Please therefore ensure that you read our notices. If you no longer wish to use Dumo after our changes to our terms or the Dumo service, then please notify us to cancel your contract with us before the changes take effect.
                        </p>
                        <p><b>We may transfer this agreement to someone else.</b> We may transfer our rights and obligations under these Dumo Service Terms to another organisation. We will contact you to let you know if we plan to do this.</p>
                        <p>You need our consent to transfer your rights to someone else. You may only transfer your rights or your obligations under these Dumo Service Terms to another person if we agree to this in writing.</p>
                        <p><b>If a court finds part of our contract illegal, the rest will continue in force. </b>Each of the paragraphs of these Dumo Service Terms operates separately. If any court or relevant authority decides that any of them are unlawful, the remaining paragraphs will remain in full force and effect.</p>
                        <p><b>Even if we delay in enforcing this contract, we can still enforce it later. </b>If we do not insist immediately that you do anything you are required to do under these Dumo Service Terms, or if we delay in taking steps against you in respect of your breaking this contract, that will not mean that you do not have to do those things and it will not prevent us taking steps against you at a later date. </p>
                        <p><b>We are not responsible for delays outside our control. </b>. If our supply of Dumo is or has been delayed by an event outside our control then we will endeavour to contact you as soon as reasonably possible to let you know. We will also take steps to minimise the effect of the delay. Provided we do this we will not be liable for delays caused by the event. However, if there is a risk of substantial delay in us providing Dumo as a result of the event, you may contact us to end the contract.</p>
                        <p><b>Which laws apply to our contract and where you may bring legal proceedings.</b>These Dumo Service Terms are governed by Singapore law and you can bring legal proceedings in respect of the products in Singapore courts.</p>

                    </div>
                     
                </div>
            </div>
           
        </div>
    </section>
</body>

</html>