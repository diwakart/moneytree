<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Packages
		</title>
		@extends('layouts.admin')
    	@section('content')
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						<!-- BEGIN: Subheader -->
						<div class="m-subheader ">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title m-subheader__title--separator">
										All Packages
									</h3>
									<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							            <li class="m-nav__item m-nav__item--home">
							                <a href="{{url('/dashboard')}}" class="m-nav__link m-nav__link--icon">
							                  <i class="m-nav__link-icon la la-home"></i>
							                </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a href="{{url('/admin/packages')}}" class="m-nav__link">
							                <span class="m-nav__link-text">
							                  All Packages
							                </span>
							              </a>
							            </li>
									</ul>
								</div>
								<div>
								</div>
							</div>
						</div>
						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row">
								<div class="col-xl-12">
									<div class="m-portlet m-portlet--mobile">
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption">
												<div class="m-portlet__head-title float-left col-xl-2">
													<h3 class="m-portlet__head-text">Package</h3>
												</div>
												<div class="float-left col-xl-3" style="margin-top: 15px;">
													<select class="form-control mx-auto" id="country_id" name="country_id">
                        		                        <option value="">Select Country</option>
                            						    @foreach($countries as $item)
                            							<option value="{{$item->id}}">{{$item->country_name}}</option>
                            							@endforeach
                        							</select>
												</div>
												<!--<div class="float-left col-xl-3" style="margin-top: 15px;">
													<select class="form-control mx-auto" id="plan_type" name="plan_type">
                        						    <option value="">Select Plan Type</option>
                        						    <option value="1">Price Plan</option>
                        						    <option value="2">Topup</option>
                        							</select>
												</div>-->
											</div>
											<div class="pull-right" style="margin-top: 20px;">
												<button class="btn btn-primary m-btn m-btn--icon" id="addPackage">
													<span>
														<i class="la la-plus"></i>
														<span>
															Add Package
														</span>
													</span>
												</button>
											</div>
										</div>
										<div class="m-portlet__body">
											<div class="package_datatable" id="local_data">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--End::Main Portlet-->   
							<!--Begin::Main Portlet-->
							
							<!--End::Main Portlet-->
						</div>
					</div>
				</div>
			</div>
		<div class="modal fade" id="addPackageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
		    <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title" id="exampleModalLabel">
		               Add Package
		            </h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">
		                    &times;
		                </span>
		            </button>
		        </div>
		        <div class="modal-body" id="package_form">
		          {{ csrf_field() }}
		                <div class="form-group">
		                    <label for="country_name" class="form-control-label">
		                        Country Name *:
		                    </label>
		                    <select class="form-control mx-auto" id="country_name" name="country_name" multiple="multiple" style="height: 120px;">
    						    @foreach($countries as $item)
    							<option value="{{$item->id}}">{{$item->country_name}}</option>
    							@endforeach
							</select>
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Name *:
		                    </label>
		                    <input type="text" class="form-control" id="package_name" name="package_name" placeholder="Please enter package name">
		                </div>
		                <div class="form-group">
                            <label for="plan_currency" class="form-control-label">
                              Plan Currency *:
                            </label>
                            <select class="form-control" id="plan_currency" name="plan_currency">
							<option value="">Select Currency</option>
							@foreach($currencies as $item)
							<option value="{{$item->currency}}" @if($item->currency=='USD') selected @endif>{{$item->currency}}</option>
							@endforeach
							</select>
                        </div>
		                <!--<div class="form-group">
		                    <label for="plan_type" class="form-control-label">
		                        Package Type *:
		                    </label>
		                    <select class="form-control mx-auto" id="plan_name" name="plan_name">
						    <option value="">Select Plan Type</option>
						    <option value="1">Price Plan</option>
						    <option value="2">Topup</option>
							</select>
		                </div>-->
		                <div class="form-group">
		                    <div class="form-check-inline">
                              <label class="form-check-label">
                                <input type="checkbox" name="plan_type[]" id="add_one_time_plan" class="form-check-input" value="1" checked> One Time
                              </label>
                            </div>
                            <div class="form-check-inline">
                              <label class="form-check-label">
                                <input type="checkbox" name="plan_type[]" id="add_recurring_plan" class="form-check-input" value="2" checked> Recurring
                              </label>
                            </div>
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        One Time Amount:
		                    </label>
		                    <input type="text" class="form-control" id="one_time_amount" name="one_time_amount" placeholder="Please enter Recurring amount">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        One Time Points:
		                    </label>
		                    <input type="text" class="form-control" id="one_time_points" name="one_time_points" placeholder="Please enter Recurring points">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Recurring Amount:
		                    </label>
		                    <input type="text" class="form-control" id="recurring_amount" name="recurring_amount" placeholder="Please enter Recurring amount">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Recurring Points:
		                    </label>
		                    <input type="text" class="form-control" id="recurring_points" name="recurring_points" placeholder="Please enter Recurring points">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Package Status *:
		                    </label>
		                    <select class="form-control mx-auto" id="package_status">
						    <option value="">Select Status</option>
						    <option value="1">Active</option>
						    <option value="2">Inactive</option>
							</select>
		                </div>
		            
		        </div>
		        <div class="modal-footer">
		            <button type="button" class="btn btn-secondary" data-dismiss="modal">
		                Close
		            </button>
		            <button type="button" class="btn btn-primary" id="addPackageBtn">
		                Save changes
		            </button>
		        </div>
		    </div>
		</div>
		</div>
			<!-- Modal for success response -->
	    <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                
	                <form name="fm-student" id="arrange-order-form1">
	                <div class="modal-body">
	                    <h5 id="ResponseHeading"></h5>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadPackageDatatable">
	                        OK
	                    </button>
	                </div>
	                </form>
	            </div>
	        </div>
	    </div>
		@endsection
		@section('js')
		<script type="text/javascript" src="{{url('/assets/admin/js/package.js')}}"></script>
		<script type="text/javascript">
		    $('#add_recurring_plan').on('click', function () {
                if (!$(this).prop('checked')) {
                    $('#recurring_amount').val('');
                    $('#recurring_points').val('');
                }
            });
            $('#add_one_time_plan').on('click', function () {
                if (!$(this).prop('checked')) {
                    $('#one_time_amount').val('');
                    $('#one_time_points').val('');
                }
            });
			$('#addPackage').click(function(e){
			    e.preventDefault();
			    $('#addPackageModal').modal('show');
			    $('#addPackageModal').data('id','');
			    $('#package_name').val('');
			    $('#plan_name').val('');
			    $('#country_name').val('');
			    $('#recurring_amount').val('');
                $('#recurring_points').val('');
			    $('#package_status').val('');
			    $('#one_time_amount').val('');
                $('#one_time_points').val('');
                $('#add_recurring_plan').prop('checked',true);
                $('#add_one_time_plan').prop('checked',true);
		  	});
		</script>
		@endsection