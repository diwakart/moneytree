<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Auction
		</title>
		@extends('layouts.admin')
    	@section('content')
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						<!-- BEGIN: Subheader -->
						<div class="m-subheader">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title m-subheader__title--separator">
										All Auction Categories
									</h3>
									<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							            <li class="m-nav__item m-nav__item--home">
							                <a href="{{url('/dashboard')}}" class="m-nav__link m-nav__link--icon">
							                  <i class="m-nav__link-icon la la-home"></i>
							                </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a href="{{url('/admin/auction')}}" class="m-nav__link">
							                <span class="m-nav__link-text">
							                  All Categories
							                </span>
							              </a>
							            </li>
									</ul>
								</div>
								<div>
								</div>
							</div>
						</div>
						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row">
								<div class="col-xl-12">
									<div class="m-portlet m-portlet--mobile">
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption col-xl-4">
												<div class="m-portlet__head-title float-left col-xl-2">
													<h3 class="m-portlet__head-text">Categories</h3>
												</div>
												<div class="float-left col-xl-3" style="margin-top: 15px;">
													<select class="form-control mx-auto" id="country_id" name="country_id">
                        		                        <option value="">Select Country</option>
                            						    @foreach($countries as $item)
                            							<option value="{{$item->id}}">{{$item->country_name}}</option>
                            							@endforeach
                        							</select>
												</div>
											</div>
											<!-- <div class="m-portlet__head-caption col-xl-4">
												<button class="btn btn-primary m-btn m-btn--icon" id="addPoints" data-toggle="modal" data-target="#AddPointsModal" style="margin-top: 7px;">
													<span>
														<i class="la la-plus"></i>
														<span>
															Add Points
														</span>
													</span>
												</button>
											</div> -->
											<div class="pull-right" style="margin-top: 20px;">
												<button class="btn btn-primary m-btn m-btn--icon" data-toggle="modal" data-target="#AddCategoryModal">
													<span>
														<i class="la la-plus"></i>
														<span>
															Add Category
														</span>
													</span>
												</button>
											</div>
										</div>
										<div class="m-portlet__body">
											<div class="categories_datatable" id="local_data">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--End::Main Portlet-->   
							<!--Begin::Main Portlet-->
							
							<!--End::Main Portlet-->
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="AddPointsModal" tabindex="-1" role="dialog" aria-labelledby="AddPointsModal" aria-hidden="true">
			<div class="modal-dialog" role="document">
		    <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title" id="AddPointsModal">
		               Add Points
		            </h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">
		                    &times;
		                </span>
		            </button>
		        </div>
		        <div class="modal-body">
		          {{ csrf_field() }}
		                <div class="form-group">
		                    <label for="points" class="form-control-label">
		                        Points Value:
		                    </label>
		                    <input type="text" class="form-control" id="category_point" name="category_point">
		                </div>
		            
		        </div>
		        <div class="modal-footer">
		            <button type="button" class="btn btn-secondary" data-dismiss="modal">
		                Close
		            </button>
		            <button type="button" class="btn btn-primary" id="addPointBtn">
		                Save changes
		            </button>
		        </div>
		    </div>
		</div>
		</div>
			<div class="modal fade" id="AddCategoryModal" tabindex="-1" role="dialog" aria-labelledby="AddCategoryModal" aria-hidden="true">
			<div class="modal-dialog" role="document">
		    <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title" id="AddCategoryModal">
		               Add Category
		            </h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">
		                    &times;
		                </span>
		            </button>
		        </div>
		        <div class="modal-body">
		          {{ csrf_field() }}
		                <div class="form-group">
		                    <label for="country_add_name" class="form-control-label">
		                        Country Name *:
		                    </label>
		                    <select class="form-control mx-auto" id="country_add_name" name="country_add_name" multiple="multiple" style="height: 120px;">
    						    @foreach($countries as $item)
    							<option value="{{$item->id}}">{{$item->country_name}}</option>
    							@endforeach
							</select>
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Category Name *:
		                    </label>
		                    <input type="text" class="form-control" id="category_add_name" name="category_add_name">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Base Price *:
		                    </label>
		                    <input type="text" class="form-control" id="category_base_price" name="category_base_price">
		                </div>
		        </div>
		        <div class="modal-footer">
		            <button type="button" class="btn btn-secondary" data-dismiss="modal">
		                Close
		            </button>
		            <button type="button" class="btn btn-primary" id="addBtn">
		                Save changes
		            </button>
		        </div>
		    </div>
		</div>
		</div>
			<div class="modal fade" id="EditCategoryModal" tabindex="-1" role="dialog" aria-labelledby="EditCategoryModal" aria-hidden="true">
			<div class="modal-dialog" role="document">
		    <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title" id="EditCategoryModal">
		               
		            </h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">
		                    &times;
		                </span>
		            </button>
		        </div>
		        <form name="fm-student" id="edit_category_form">
		        <div class="modal-body">
		          {{ csrf_field() }}
		                <div class="form-group">
		                    <label for="country_name" class="form-control-label">
		                        Country Name *:
		                    </label>
		                    <select class="form-control mx-auto" id="country_name" name="country_name" multiple="multiple" style="height: 120px;">
    						    @foreach($countries as $item)
    							<option value="{{$item->id}}">{{$item->country_name}}</option>
    							@endforeach
							</select>
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Category Name *:
		                    </label>
		                    <input type="text" class="form-control" id="category_name" name="category_name">
		                </div>
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Base Price *:
		                    </label>
		                    <input type="text" class="form-control" id="category_price" name="category_base_price">
		                </div>
		            
		        </div>
		        <div class="modal-footer">
		            <button type="button" class="btn btn-secondary" data-dismiss="modal">
		                Close
		            </button>
		            <button type="submit" class="btn btn-primary" id="Updatebtn">
		                Save changes
		            </button>
		        </div>
		        </form>
		    </div>
		</div>
		</div>


		<div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		        <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                
		                <form name="fm-student" id="arrange-order-form">
		                <div class="modal-body">
		                    <h5 id="ResponseHeading"></h5>
		                </div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadCategoryDatatable">
		                        OK
		                    </button>
		                </div>
		                </form>
		            </div>
		        </div>
		    </div>
		@endsection
		@section('js')
		<script type="text/javascript" src="{{url('/assets/admin/js/auction_categories.js')}}"></script>
		<script type="text/javascript">
			function editCategories(id){
		        var path = "auctionCategoryDetails";
		        $.ajax({
		          type: "POST",
		          url: path,
		          data: {
		            id: id
		          },
		           headers: {
		          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		          success: function(result){
		            //console.log(result);
		            var res = $.parseJSON(result);

		            if(res.status == 'error'){

		            }else{
		              var data = $.parseJSON(JSON.stringify(res.message));
		              $('#EditCategoryModal').data('id',data.id);
		              $('#EditCategoryModal').find('.modal-title').html('Update Category ' + data.category_name);
		              var country_id = data.country_ids;
		              //alert(country_id);
		                $.each(country_id.split(","), function(i,e){
                            $("#country_name option[value='" + e + "']").prop("selected", true);
                        });
		              $('#category_name').val(data.category_name);
		              $('#category_price').val(data.base_price);
		              /*$('#country_name').val(data.country_id);*/
		              $('#EditCategoryModal').modal('show');
		              //datatable.reload();
		            }
		          },
		          error: function(){
		            alert("Error");
		          }
		        }); 
		  	}

		</script>
		@endsection
			
