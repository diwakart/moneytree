<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Products
		</title>
		@extends('layouts.admin')
    	@section('content')
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						<!-- BEGIN: Subheader -->
						<div class="m-subheader">
							<div class="d-flex align-items-center">
								<div class="mr-auto">
									<h3 class="m-subheader__title m-subheader__title--separator">
										All Products
									</h3>
									<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							            <li class="m-nav__item m-nav__item--home">
							                <a href="{{url('/dashboard')}}" class="m-nav__link m-nav__link--icon">
							                  <i class="m-nav__link-icon la la-home"></i>
							                </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a href="{{url('/admin/auction')}}" class="m-nav__link">
							                <span class="m-nav__link-text">
							                  All Categories
							                </span>
							              </a>
							            </li>
							            <li class="m-nav__separator">
							              -
							            </li>
							            <li class="m-nav__item">
							              <a class="m-nav__link">
							                <span class="m-nav__link-text">
							                  {{$data->category_name}}
							                </span>
							              </a>
							            </li>
									</ul>
								</div>
								<div>
								</div>
							</div>
						</div>
						<!-- END: Subheader -->
						<div class="m-content">
							<div class="row">
								<div class="col-xl-12">
									<div class="m-portlet m-portlet--mobile">
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption col-xl-4">
												<div class="m-portlet__head-title">
													<h3 class="m-portlet__head-text">Categories</h3>
												</div>
											</div>
											@php if($data->status == '2'){ @endphp
											<div class="pull-right" style="margin-top: 20px;">
												<button class="btn btn-primary m-btn m-btn--icon" data-toggle="modal" data-target="#AddProductModal">
													<span>
														<i class="la la-plus"></i>
														<span id="addProductBtn">
															Add Product
														</span>
													</span>
												</button>
											</div>
											@php } @endphp
										</div>
										<div class="m-portlet__body">
											<div class="products_datatable" id="local_data" data_id="{{$data->id}}">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--End::Main Portlet-->   
							<!--Begin::Main Portlet-->
							
							<!--End::Main Portlet-->
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="AddProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
		    <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title" id="exampleModalLabel">
		               Add Product
		            </h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">
		                    &times;
		                </span>
		            </button>
		        </div>
		        <div class="modal-body">
		        <form method="post" name="add_product" id="add_product" enctype="multipart/form-data">
		          {{ csrf_field() }}
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Product Name *:
		                    </label>
		                    <input type="text" class="form-control" id="product_name" name="product_name">
		                </div>
		                <div class="form-group">
		                    <label for="price" class="form-control-label">
		                        Product Price *:
		                    </label>
		                    <input type="text" class="form-control" id="product_price" name="product_price">
		                </div>
		                <div class="form-group">
		                    <label for="image" class="form-control-label">
		                        Product Image:
		                    </label>
		                    <input type="file" class="form-control" id="image" name="image">
		                </div>
		                <div class="form-group">
		                    <label for="image" class="form-control-label">
		                        Product Description:
		                    </label>
		                    <textarea class="form-control" id="product_description" name="product_description"></textarea>
		                </div>
		                <!-- <div class="form-group">Allow Redemption : 		                    
		                    <input type="checkbox" class="form-control" id="add_allow_redeem" name="add_allow_redeem">
		                </div> -->
		                <!-- <div class="form-group" id="add_rdm_price_div" style="display: none;">
		                    <label for="image"  id="label_redeem" class="form-control-label">
		                        Redeem Price:
		                    </label>
		                   <input type="text" class="form-control" id="rdm_price" name="rdm_price" value="0">
		                </div> -->

		                <label for="image" class="form-control-label">
		                        Allow Redemption :
		                </label>		                  
	                    <div class="input-group">
	                      <span class="input-group-addon"> 
	                        <input type="checkbox" aria-label="..." id="add_allow_redeem" name="add_allow_redeem" style="height: 40px;">
	                      </span>
	                      <input type="text" class="form-control" id="rdm_price" name="rdm_price" value="0" style="display: none;">
	                    </div><!-- /input-group -->
		                    
		                
		        </form>
		        </div>
		        <div class="modal-footer">
		            <button type="button" class="btn btn-secondary" data-dismiss="modal">
		                Close
		            </button>
		            <button type="button" class="btn btn-primary" id="addBtn">
		                Save changes
		            </button>
		        </div>
		    </div>
		</div>
		</div>
		<div class="modal fade" id="EditProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
		    <div class="modal-content">
		        <div class="modal-header">
		            <h5 class="modal-title" id="exampleModalLabel">
		               Edit Product
		            </h5>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                <span aria-hidden="true">
		                    &times;
		                </span>
		            </button>
		        </div>
		        <div class="modal-body">
		        <form method="post" name="edit_product" id="edit_product" enctype="multipart/form-data">
		          {{ csrf_field() }}
		                <div class="form-group">
		                    <label for="name" class="form-control-label">
		                        Product Name *:
		                    </label>
		                    <input type="text" class="form-control" id="name" name="name">
		                    <input type="hidden" class="form-control" id="category_id" name="category_id">
		                </div>
		                <div class="form-group">
		                    <label for="price" class="form-control-label">
		                        Product Price *:
		                    </label>
		                    <input type="text" class="form-control" id="price" name="price">
		                </div>
		                <div class="form-group">
		                    <label for="image" class="form-control-label">
		                        Product Description:
		                    </label>
		                    <textarea class="form-control" id="description" name="description"></textarea>
		                </div>
		                <div class="form-group">
		                    <label for="image" class="form-control-label">
		                        Product Image:
		                    </label>
		                    <input type="file" class="form-control" id="product_image" name="product_image">
		                </div>
		                <div class="form-group" id="image_selected" style="display: none;">
		                
		                </div>
		                <label for="image" class="form-control-label">
		                        Allow Redemption :
		                </label>		                  
	                    <div class="input-group">
	                      <span class="input-group-addon"> 
	                        <input type="checkbox" id="edit_allow_redeem" name="edit_allow_redeem" style="height: 40px;">
	                      </span>
	                      <input type="text" class="form-control" id="edit_rdm_price" name="edit_rdm_price" style="display: none;">
	                    </div><!-- /input-group -->


		        </form>
		        </div>
		        <div class="modal-footer">
		            <button type="button" class="btn btn-secondary" data-dismiss="modal">
		                Close
		            </button>
		            <button type="button" class="btn btn-primary" id="Updatebtn">
		                Save changes
		            </button>
		        </div>
		    </div>
		</div>
		</div>
		<div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		        <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                
		                <form name="fm-student" id="arrange-order-form">
		                <div class="modal-body">
		                    <h5 id="ResponseHeading"></h5>
		                </div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadProductDatatable">
		                        OK
		                    </button>
		                </div>
		                </form>
		            </div>
		        </div>
		    </div>
		@endsection
		@section('js')
		<script type="text/javascript" src="{{url('/assets/admin/js/auction_products.js')}}"></script>
		<script type="text/javascript">
            $('#addProductBtn').on('click',function(){
                $('#product_name').val('');
                $('#product_price').val('');
                $('#product_description').val('');
                $('#rdm_price').val('0');
                $('#image').val('');
                $('#add_allow_redeem').prop("checked", false);
            });
            
			var rdm_price_value = '';
			function editProducts(id){
		        var path = "../product_details";
		        $("#edit_allow_redeem").attr("checked", false);
		        $('#edit_rdm_price').val('0');
		        $.ajax({
		          type: "POST",
		          url: path,
		          data: {
		            id: id
		          },
		           headers: {
		          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		          success: function(result){
		            console.log(result);
		            var res = $.parseJSON(result);

		            if(res.status == 'error'){

		            }else{
		              var data = $.parseJSON(JSON.stringify(res.message));		              		              
		              $('#EditProductModal').data('id',data.id);
		              $('#EditProductModal').find('.modal-title').html('Update Product ' + data.product_name);
		              $('#name').val(data.product_name);
		              $('#price').val(data.product_price);
		              $('#description').val(data.description);
		              $('#category_id').val(data.auction_category_id);
		              
		              $('#product_image').val('');
		              if(data.product_image!='')
		              {
		              	var image_input = '<label for="image_selected" class="form-control-label" style="width:100px;">Image:</label><img src="../../../storage/app/products/'+data.product_image+'" height="100" width="100"></img>';
						$("#image_selected").html(image_input);
						$("#image_selected").show();
		              }

		               if(data.rdm_price > 0)
		               {
		               		$("#edit_allow_redeem").prop("checked", true);
		               		$('#edit_rdm_price').css('display','block');
		               		$('#edit_rdm_price').val(data.rdm_price);
		               } 
		               else {

		              		$('#edit_rdm_price').val('0');
      						$('#edit_rdm_price').hide();
      						$("#edit_allow_redeem").prop("checked", false);
      						$('#edit_rdm_price').css('display','none');
		               }
		              		             
		              $('#EditProductModal').modal('show');		              
		            }
		          },
		          error: function(){
		            alert("Error");
		          }
		        }); 
		  	}
			
		</script>
		@endsection
			
