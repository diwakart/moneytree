<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			DIGITAL BANK | Refer Register
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<!--end::Web font -->
        <!--begin::Base Styles -->
		<link href="../assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="../assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="../assets/demo/default/media/img/logo/favicon.ico" />
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--singin" id="m_login">
				<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
					<div class="m-stack m-stack--hor m-stack--desktop">
						<div class="m-stack__item m-stack__item--fluid">
							<div class="m-login__wrapper" style="padding:0px;">
								<div class="m-login__logo">
									<a href="#">
										<img src="../assets/app/media/img//logos/logo-2.png">
									</a>
								</div>
								<div class="m-login__signin">
									<div class="m-login__head">
										<h3 class="m-login__title">
											Refer Student Register
										</h3>
									</div>
									<form class="m-login__form m-form" method="post">
										@if (Session::has('message'))
									   	<div class="m-alert m-alert--outline alert alert-info alert-dismissible fade show" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
													{{Session::get('message')}}
												</div>
										@endif
										@if (Session::has('admin_message'))
									   	<div class="m-alert m-alert--outline alert alert-info alert-dismissible fade show" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
													{{Session::get('admin_message')}}
												</div>
										@endif
										<div class="form-group m-form__group">
                		                    <select class="form-control" id="country_id" name="country_id" style="padding:0px;">
                		                        <option value="">Select Country</option>
                    						    @foreach($countries as $item)
                    							<option value="{{$item->id}}">{{$item->country_name}}</option>
                    							@endforeach
                							</select>
                		                </div>
										<div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="Full name" name="name" id="name" autocomplete="off">
                                        </div>
                                         <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="User Name" name="user_name" id="user_name" autocomplete="off">
                                        </div> 
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="Email" name="email" id="email" autocomplete="off">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="Country Code" name="country_code" id="country_code" autocomplete="off">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="Mobile Number" name="mobile" id="mobile" autocomplete="off">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" id="password">
                                        </div>
                                         <div class="form-group m-form__group">
                                            <input class="form-control m-input m-login__form-input--last" type="hidden"  name="refer_id" id="refer_id" value="{{$refer_data->id}}">
                                        </div>
                                        
                                        <div class="m-login__form-action">
                                            <button id="m_login_signup_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                               Register
                                            </button>
                                        </div>
									</form>
								</div>
							</div>
						</div>
						<!-- <div class="m-stack__item m-stack__item--center">
							<div class="m-login__account">
								<span class="m-login__account-msg">
									Don't have an account yet ?
								</span>
								&nbsp;&nbsp;
								<a href="javascript:;" id="m_login_signup" class="m-link m-link--focus m-login__account-link">
									Sign Up
								</a>
							</div>
						</div> -->
					</div>
				</div>
				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content" style="background-image: url(../assets/app/media/img//bg/bg-4.jpg)">
					<div class="m-grid__item m-grid__item--middle">
						<h3 class="m-login__welcome">
							Join Our Community
						</h3>
						<p class="m-login__msg">
							Lorem ipsum dolor sit amet, coectetuer adipiscing
							<br>
							elit sed diam nonummy et nibh euismod
						</p>
					</div>
				</div>
			</div>
		</div>
		<!-- end:: Page -->
    	<!--begin::Base Scripts -->
		<script src="../assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="../assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
		<!--end::Base Scripts -->   
		<script>
		    $("#country_code,#mobile").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
    		$('#country_id').on('change',function(){
    			    var id = $(this).val();
    			    $.ajax({
                    method: 'POST',
                    url: 'getCountryCode',
                    data: {
                      id: id
                    },
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                      var res = $.parseJSON(data);
                      if(res.status == 'error'){
                        /*swal('Error',res.message,'error');*/
                        $('#country_code').val('');
                      }else{
                        $('#country_code').val(res.data.country_code);
                      } 
                    },
                    error: function(data) {
                      swal('Error',data,'error');
                    }
                  });
    			});
		    $(function(){
		        $("#m_login_signup_submit").click(function(event){
		            event.preventDefault();
		        var country_id  = $('#country_id').val();
		        var user_name   = $('#user_name').val();
		        var name        = $('#name').val();
		        var email       = $('#email').val();
		        var mobile      = $('#mobile').val();
	            var password    = $('#password').val();
		        var refer_id    = $('#refer_id').val();
		        if(country_id=='')
                {
                	swal('Error','Please select country name','error');
                	return false;	
                }
                else if(user_name=='')
                {
                	swal('Error','Please Enter User Name','error');
                	return false;	
                }
                else if(password=='')
                {
                	swal('Error','Please Enter Password','error');
                	return false;	
                }
                $.ajax({
                        type: 'POST',
                        url: '../refer_child/store',
                        data: {refer_id:refer_id,country_id:country_id,user_name:user_name,name:name,email:email,mobile:mobile,password:password},
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        beforeSend:function(res){
                            $('#m_login_signup_submit').prop('disabled', true);
                        },
                        success: function(data) {
                        $('#m_login_signup_submit').prop('disabled', false);
                        var res = $.parseJSON(data);
                        console.log(res);
                            if(res.status == 'error'){
                                swal('Error',res.message,'error');
                            }
                            else
                            {
                                swal('Success',res.message,'success');
                            } 
                        },
                        error: function(data) {
                            swal('Error',data,'error');
                        }
                    });
                });
		    });
		</script>
	</body>
	<!-- end::Body -->
</html>
