<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Response;
use Illuminate\Support\Facades\Auth;
use DeviceNotification;
use DB;
//use Auth;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('isAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $remaining_users = array();
        $ERROR='';
        $auction_data = DB::table('send_notification')->where('status','2')->first();
        //echo "<pre>";
            //print_r($auction_data); die;
        if(!empty($auction_data))
        {
            /*****************************Auction Notification****************/
            if($auction_data->type=='2')
            {
                $start_row = $auction_data->start_row;
                $devices = DB::table('users_device')->select('users_device.device_secret','users_device.device_type','users_device.token')->limit(100)->offset($auction_data->start_row)->get();
                foreach($devices as $device)
                {
                    $message = [
                    'title' => 'Digital Bank',
                    'body' => "Auction Active please login and participate in that to get that product",
                    'type' =>'2',
                    ];
                    if($device->device_type=='1' || $device->device_type=='2')
                    {
                        try
                        {
                            $result = DeviceNotification::sendNotification($device->token,$message);
                            $start_row++;
                        }
                        catch(\Illuminate\Database\QueryException $ex){ 
                            $error_code = $ex->errorInfo[1];
                            if($error_code == 1062){
                                $result = $ex->getMessage();
                                $remaining_users[] = ucwords($device->device_secret);
                            }
                        }
                    }
                }
                if(count($remaining_users)>0){
                
                $error_names =  array_unique($remaining_users, SORT_REGULAR);
                $students_name = implode(', ', $error_names);
                $ERROR =   'Record Inserted Successfully. '.$students_name.' named users(s) may have some error.';
                
                }
                DB::table('send_notification')
                ->where('id', $auction_data->id)
                ->update(['status' => '3','start_row'=>$start_row,'message'=>$ERROR]); 
            }
            /*****************************Task Notification****************/
            elseif($auction_data->type=='1')
            {
                $start_row = $auction_data->start_row;
                $devices = DB::table('users_device')->select('users_device.device_secret','users_device.device_type','users_device.token')->limit(100)->offset($auction_data->start_row)->get();
                foreach($devices as $device)
                {
                    $message = [
                    'title' => 'Digital Bank',
                    'body' => "New task is Active attempt it to earn more points",
                    'type' =>'1',
                    ];
                    if($device->device_type=='1' || $device->device_type=='2')
                    {
                        try
                        {
                            $result = DeviceNotification::sendNotification($device->token,$message);
                            $start_row++;
                        }
                        catch(\Illuminate\Database\QueryException $ex){ 
                            $error_code = $ex->errorInfo[1];
                            if($error_code == 1062){
                                $result = $ex->getMessage();
                                $remaining_users[] = ucwords($device->device_secret);
                            }
                        }
                    }
                }
                if(count($remaining_users)>0){
                
                $error_names =  array_unique($remaining_users, SORT_REGULAR);
                $students_name = implode(', ', $error_names);
                $ERROR =   'Record Inserted Successfully. '.$students_name.' named users(s) may have some error.';
                
                }
                DB::table('send_notification')
                ->where('id', $auction_data->id)
                ->update(['status' => '3','start_row'=>$start_row,'message'=>$ERROR]); 
            }
            /************************Quiz Round Notification****************/
            else
            {
                //echo "hello"; die;
                $start_row = $auction_data->start_row;
                $devices = DB::table('users_device')->select('users_device.device_secret','users_device.device_type','users_device.token')->limit(100)->offset($auction_data->start_row)->get();
                foreach($devices as $device)
                {
                    $message = [
                    'title' => 'Digital Bank',
                    'body' => "Quiz round active please participate in quiz to earn more points.",
                    'type' =>'2',
                    ];
                    if($device->device_type=='1' || $device->device_type=='2')
                    {
                        try
                        {
                            $result = DeviceNotification::sendNotification($device->token,$message);
                            $start_row++;
                        }
                        catch(\Illuminate\Database\QueryException $ex){ 
                            $error_code = $ex->errorInfo[1];
                            if($error_code == 1062){
                                $result = $ex->getMessage();
                                $remaining_users[] = ucwords($device->device_secret);
                            }
                        }
                    }
                }
                if(count($remaining_users)>0){
                
                $error_names =  array_unique($remaining_users, SORT_REGULAR);
                $students_name = implode(', ', $error_names);
                $ERROR =   'Record Inserted Successfully. '.$students_name.' named users(s) may have some error.';
                
                }
                DB::table('send_notification')
                ->where('id', $auction_data->id)
                ->update(['status' => '3','start_row'=>$start_row,'message'=>$ERROR]);
            }
            
        }
        
        
    }
}
