<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Response;
use DB;
use App\Model\AuctionCategories;
use App\Model\AuctionProducts;
use App\Model\Task;
use Illuminate\Support\Facades\Auth;
use Validator;
//use Auth;

class AuctionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = \DB::table('countries')->select('id','country_name')->get()->toArray();
        return view('admin.auction.index',compact(['countries']));
    }

    public function auctionProducts($id)
    {
        $data = AuctionCategories::find($id);
        return view('admin.auction.products',compact(['data']));
    }

    public function getAllAuctionCategories(Request $request)
    {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $country_id=(isset($request->datatable['query']['country_id']) && ($request->datatable['query']['country_id']!="") ? $request->datatable['query']['country_id'] : '');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        $categories = new AuctionCategories();
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $categories = $categories->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'auction_categories.id as id','auction_categories.category_name','auction_categories.base_price','auction_categories.status',\DB::raw('group_concat(dg_acc.country_id) as country_ids'),\DB::raw('group_concat(dg_co.country_name) as country_name'));
        $total = $categories->count();
        $categories = $categories->join('auction_category_country as acc','acc.auction_category_id','=','auction_categories.id')->join('countries as co','co.id','=','acc.country_id');
        if($country_id !='')
        {
          $categories = $categories->where('acc.country_id',$country_id);
          $total = $categories->count();
        }
        //$total = $categories->count();
        $categories = $categories->groupBy('auction_categories.id')->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=>$categories,'meta'=>$meta));
    }

    public function getAllProducts(Request $request,$id)
    {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        $products = new AuctionProducts();
        //$total = $products->count();
        $total = $products->where('auction_category_id',$id)->count('id');
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $products = $products->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','auction_category_id','product_name','product_price','rdm_price','product_image','status','description')->where('auction_category_id',$id)->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=>$products,'meta'=>$meta));
    }

    public function add(Request $request)
    {
        $rules = array(
          'category_name'        => 'required',
          'category_base_price'  => 'required|numeric',
          'country_id'           => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['category_name']))
            $errorMsg = $failedRules['category_name'][0] . "\n";
            if(isset($failedRules['category_base_price']))
            $errorMsg = $failedRules['category_base_price'][0] . "\n";
            if(isset($failedRules['country_id']))
            $errorMsg = $failedRules['country_id'][0] . "\n";
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
        }
        else
        {
            $status = "success";
            $category = new AuctionCategories();
            try
            {
                $category->category_name = $request->category_name;
                $category->base_price = $request->category_base_price;
                $country_id = $request->country_id;
                if($category->save())
                {
                    foreach($country_id as $country_data)
                    {
                        $data[] = array(
                            'auction_category_id' => $category->id,
                            'country_id' => $country_data
                            );
                    }
                    DB::table('auction_category_country')->insert($data);
                    $result = "Category Inserted Successfully";
                    return (json_encode(array('status'=>$status,'message'=>$result ))); 
                }  
            }
            catch(QueryException $ex){ 
              //dd($ex->getMessage());
              $status = "error";
              $result = $ex->getMessage();
              return (json_encode(array('status'=>$status,'message'=>$result )));
            }
            
        }
    }

    public function categoryDetails(Request $request)
    {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = AuctionCategories::select('auction_categories.id as id','auction_categories.category_name','auction_categories.base_price','auction_categories.status',\DB::raw('group_concat(dg_acc.country_id) as country_ids'))->join('auction_category_country as acc','acc.auction_category_id','=','auction_categories.id')->groupBy('auction_categories.id')->find($request->id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;  
    }

    public function changeStatus(Request $request)
    {
        $status = "success";
        $category_status = ($request->status == '1') ? '2' : '1';
        try {
            AuctionCategories::where('id',$request->category_id)->update(['status'=>$category_status]);
            $result = "Category status updated successfully";
        }
        catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))); 
    }

    public function update(Request $request)
    {
        $rules = array(
          'category_name'        => 'required',
          'category_price'       => 'required|numeric',
          'country_id'           => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['category_name']))
            $errorMsg = $failedRules['category_name'][0] . "\n";
            if(isset($failedRules['category_price']))
            $errorMsg = $failedRules['category_price'][0] . "\n";
            if(isset($failedRules['country_id']))
            $errorMsg = $failedRules['country_id'][0] . "\n";
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
        }
        else
        {
            $status = "success";
            try 
            {
                $request_countries      = $request->country_id;
                $exist_countries_data   = DB::table('auction_category_country')->select(\DB::raw('group_concat(country_id) as country_ids'))->where('auction_category_id',$request->id)->groupby('auction_category_id')->first();
                $exist_countries = explode(",",$exist_countries_data->country_ids);
                $result_countries = array_diff($request_countries,$exist_countries);
                $deleted_countries = array_diff($exist_countries,$request_countries);
                if(!empty($deleted_countries))
                {
                    foreach($deleted_countries as $country_data)
                    {
                        DB::table('auction_category_country')->where(['auction_category_id'=>$request->id,'country_id'=>$country_data])->delete();
                    }
                }
                if(!empty($result_countries))
                {
                    foreach($result_countries as $country_data)
                    {
                        $data[] = array(
                            'auction_category_id' => $request->id,
                            'country_id' => $country_data
                            );
                    }
                    DB::table('auction_category_country')->insert($data);  
                }
                $data = array(
                'category_name' => $request->category_name,
                'base_price'    => $request->category_price
                );
                AuctionCategories::where('id',$request->id)->update($data);
                $result = "Category Updated Successfully";
            }
            catch(QueryException $ex){ 
              dd($ex->getMessage());
              $status = "error";
              $result = $ex->getMessage();
            }
            return (json_encode(array('status'=>$status,'message'=>$result )));
        }
    }

    public function delete(Request $request)
    {
        $status = "success";
        try {
            if(AuctionProducts::where('auction_category_id',$request->id)->exists())
            {
              $product_id = \DB::table('auction_products')->select('id')->where('auction_category_id',$request->id)->get()->toArray();
              foreach ($product_id as $key => $value) {
                \DB::table('auction_products')->where('id',$value->id)->delete();
              }
            }
            AuctionCategories::where('id',$request->id)->delete($request->id);
            $result = "Category Deleted Successfully";
        }
        catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result )));
    }

    public function addProducts(Request $request,$id)
    {
        // print_r($request->all());
        // die;
        $status = "success";
        try {
            $image_name = '';
            if ($request->hasFile('image'))
            {
                $image = $request->image;
                $path = storage_path('app/products');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $type       = $image->getClientOriginalExtension();
                $allowed =  array('jpg','png','jpeg');
                if(!in_array($type,$allowed) ) {
                    return Response::json(array('status'=> 'error','message'=>'Please select only image jpg,png,jpeg format.')) ;
                }
                $image->move($path, $filename);

                $image_name = $filename;

            }
            $data = array(   
            'auction_category_id'=>$id,
            'product_name' => $request->product_name,
            'product_price' => $request->product_price,
            'product_image'=>$image_name,
            'rdm_price'=>$request->rdm_price
            );
            if($request->has('product_description'))
            {
                $data['description'] = $request->product_description;
            }
            AuctionProducts::insert($data);
            $result = "Products Inserted Successfully";
        }
        catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result )));
    }

    public function editProducts(Request $request,$id)
    {
        $status = "success";
        try {
            $image_name = '';
            if ($request->hasFile('product_image'))
            {
                $image = $request->product_image;
                $path = storage_path('app/products');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $type       = $image->getClientOriginalExtension();
                $allowed =  array('jpg','png','jpeg');
                if(!in_array($type,$allowed) ) {
                    return Response::json(array('status'=> 'error','message'=>'Please select only image jpg,png,jpeg format.')) ;
                }
                $image->move($path, $filename);

                $image_name = $filename;

            }
            $data = array(   
            'auction_category_id'=>$request->category_id,
            'product_name' => $request->name,
            'product_price' => $request->price,
            'rdm_price' => $request->edit_rdm_price
            );
            if($image_name!='')
            {
                $data['product_image'] = $image_name;
            }
            if($request->has('description'))
            {
                $data['description'] = $request->description;
            }
            AuctionProducts::where('id',$id)->update($data);
            $result = "Products Updated Successfully";
        }
        catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result )));
    }

    public function productDetails(Request $request)
    {
      $status = "success";
        try { //->update(['name' => $request->name])
          $result = AuctionProducts::find($request->id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;    
    }

    public function deleteProduct(Request $request)
    {
      $status = "success";
        try {
            AuctionProducts::where('id',$request->id)->delete($request->id);
            $result = "Product Deleted Successfully";
        }
        catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result )));
    }
    
    
}
