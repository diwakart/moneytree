<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\User;
use App\Model\Transaction;
use App\Model\Account;
use App\Model\UserNotification;
use DB;
use QrCode;
use Response;
use Validator;

//use Auth;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = \DB::table('countries')->select('id','country_name')->get()->toArray();
        return view('admin.users.index',compact(['countries']));
    }

    public function getAllUsers(Request $request)
    {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $country_id=(isset($request->datatable['query']['country_id']) && ($request->datatable['query']['country_id']!="") ? $request->datatable['query']['country_id'] : '');
        $user_list=(isset($request->datatable['query']['user_list']) && ($request->datatable['query']['user_list']!="") ? $request->datatable['query']['user_list'] : '');
        $search=(isset($request->datatable['query']['search']) && ($request->datatable['query']['search']!="") ? $request->datatable['query']['search'] : '');
         $status=(isset($request->datatable['query']['status']) && ($request->datatable['query']['status']!="") ? $request->datatable['query']['status'] : '');
        $users = new user();
        $user_status = 1;
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        if($user_list!='')
        {
          $users = $users->where('type',$user_list);
        }
        if($search!='')
        {
          $users = $users->where('user_name', 'like', $search.'%');
        }
        if($status!='')
        {
          $user_status = $status;
        }
        if($country_id !='')
        {
          $users = $users->where('users.country_id',$country_id);
        }
        
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $users = $users->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'users.id as id','users.user_name','users.name','users.email','users.mobile','users.type','users.status','co.country_name','co.id as country_id')->join('countries as co','co.id','=','users.country_id')->where('type','!=','1')->where('status','=',$user_status);
        $total = $users->count();
        $users = $users->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=>$users,'meta'=>$meta));
    }
    
    public function sendNotification(Request $request)
    {
        $data = array(
            'type' =>'2',
            'notification_for_id' => $request->auction_id,
            'start_row'=>'0',
            'end_row'=>'100',
        );
        try 
        {
            if(DB::table('send_notification')->where('notification_for_id',$request->auction_id)->exists())
            {
                return (json_encode(array('status'=>'success','message'=>'Sending notification already processed'))) ;
            }
            else
            {
                $result = DB::table('send_notification')->insert($data);
                if($result)
                {
                    return (json_encode(array('status'=>'success','message'=>'Notification send in progress'))) ;
                }
            }
                       
        }catch(\Illuminate\Database\QueryException $ex){ 
            $result = $ex->getMessage();
            return (json_encode(array('status'=>'error','message'=>$result))) ;
        }
    }

    public function add(Request $request)
    {
        
        $rules = array(
          'user_name'        => 'required|min:4',
          'type'        => 'required|numeric',
          'country_name'  => 'required|numeric',
        );
        $validator = Validator::make($request->all(), $rules);
        $level;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['user_name']))
            $errorMsg = $failedRules['user_name'][0] . "\n";
            if(isset($failedRules['type']))
            $errorMsg = $failedRules['type'][0] . "\n";
            if(isset($failedRules['country_name']))
            $errorMsg = $failedRules['country_name'][0] . "\n";
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
        }
        else
        {
            $id=$request->id;
            if($request->user_name!=''){
              $check_user_name=User::where(['user_name'=>$request->user_name])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();
                if($check_user_name)
                {
                    return (json_encode(array('status'=>'error','message'=>"Username already Taken please try another." )));
                }
            }
            $data = array(
                'country_id' => $request->country_name,
                'user_name' => $request->user_name,
                'type' => $request->type,
            );
            if($request->has('email'))
            {
              $data['email'] = $request->email;  
            }
            if($request->has('name'))
            {
              $data['name'] = $request->name;  
            }
            if($request->has('mobile'))
            {
                if(!is_numeric($request->input('mobile')))
                {
                    return (json_encode(array('status'=>'error','message'=>'Mobile number must be numeric')));
                }
                if(!$request->has('country_code'))
                {
                    return (json_encode(array('status'=>'error','message'=>'Country code required for mobile')));
                }
                else if(!DB::table('countries')->where('country_code',$request->input('country_code'))->exists())
                {
                        return (json_encode(array('status'=>'error','message'=>'Please enter correct country code')));
                }
                  $data['mobile'] = $request->mobile;  
                  $data['country_code'] = $request->country_code; 
            }
            
            if($request->id)
            { 
                try 
                {
                    User::where('id',$request->id)->update($data);
                    return (json_encode(array('status'=>'success','message'=>'User '.$request->name.' Updated Successfully')));
                }
                catch(\Illuminate\Database\QueryException $ex)
                { 
                    $error_code = $ex->errorInfo[1];
                    if($error_code == 1062){
                    $result = $ex->getMessage();
                    return (json_encode(array('status'=>'error','message'=>'Email or Contact already exist'))) ;
                    }
                }
            }
            else{
                if($request->password=='')
                {
                   return (json_encode(array('status'=>'error','message'=>'Password Field is mandatory.'))); 
                }
                $data['password'] = Hash::make($request->password);
                try {
                       // User::insert($data);
                        $query = User::insert($data);
                        if($query)
                        {
                            $details = user::select('id','digital_key')->orderby('id','desc')->first();
                           // print_r($details);die("okk");
                            QrCode::format('png')->size(200)->generate($details->digital_key,'./public/userQrcodes/'.$details->digital_key.'.png');
                        }
                        else
                        {
                           return (json_encode(array('status'=>'error','message'=>'User '.$request->name.' not Inserted Successfully'))); 
                        }
                        try
                        {
                         User::where('id',$details->id)->update(['qr_code'=>$details->digital_key.'.png']);
                        }
                        catch(QueryException $ex){ 
                          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                                            'Content-Type' => 'application/json',
                                            'Accept' => 'application/json',
                                         ]);
                        }
                        return (json_encode(array('status'=>'success','message'=>'User '.$request->name.' Inserted Successfully')));
                       
                }catch(\Illuminate\Database\QueryException $ex){ 
                       $error_code = $ex->errorInfo[1];
                       if($error_code == 1062){
                        $result = $ex->getMessage();
                        return (json_encode(array('status'=>'error','message'=>'Email or Contact already exist'))) ;
                  }
                }
          }
        }
    }
    public function userDetails(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = User::find($request->id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }

    public function getUserID(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = User::find($request->id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }

    public function changePassword(Request $request)
    {
      if($request->ajax())
      {
        $user;
        $rules = array(
          'password'     => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['password']))
            $errorMsg = $failedRules['password'][0] . "\n";
          
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        } else if($request->id){
          $user = User::find($request->id);
        }
        else{
          $user = new User();
          
        }

        $user->password = Hash::make($request->password);
        if($user->save()){
          return (json_encode(array('status'=>'success','message'=>sprintf('Passsword for User "%s" successfully saved', $user->name))));
          } else{
             return (json_encode(array('status'=>'error','message'=>'Failed to Change Password'))) ;
          }
      }
    }

    public function delete(Request $request)
    {
      if($request->ajax())
      {
        if($request->id){
          $user = User::find($request->id);
          $update = User::where('id',$user->id)->update(['status'=>'3']);
          if ($update)
            return (json_encode(array('status'=>'success','message'=>"User Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }
    }

    public function childProfile(Request $request,$id)
    {
        $data = DB::table('users as user')->select('user.id as id','user.user_name','user.name','user.country_code','user.mobile','user.dob','user.address','user.email','user.avtar','user.school_name','countries.country_name','user.qr_code')->join('countries', 'countries.id', '=', 'user.country_id')->where('user.id',$id)->first();
        
        return view('admin.users.childprofile',compact(['data']));
    }

    public function parentProfile(Request $request,$id)
    {
      $data = User::where('id',$id)->first();
      return view('admin.users.parentprofile',compact(['data']));
    }

    public function getAllParentChild(Request $request,$id)
    {
      $perpage = $request->datatable['pagination']['perpage'];
      $page = $request->datatable['pagination']['page'];
      $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
      if($page=='1')
      {
        $offset = 0;
      }
      else{
        $offset = ($page-1)*$perpage;
      }
      $users = new user();
      $total = $users->where('parent_id',$id)->count();
      DB::statement(DB::raw('set @rownumber='.$offset.''));
      $users = $users->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','name','email','mobile','address','school_name')->where('parent_id',$id)->offset($offset)->limit($perpage)->get();
      $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
      return Response::json(array('data'=>$users,'meta'=>$meta));
    }

    public function getAllUserTransaction(Request $request,$id)
    {
      $perpage = $request->datatable['pagination']['perpage'];
      $page = $request->datatable['pagination']['page'];
      $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
      if($page=='1')
      {
        $offset = 0;
      }
      else{
        $offset = ($page-1)*$perpage;
      }
      $transaction = new transaction();
      $total = $transaction->where('user_id',$id)->count();
      DB::statement(DB::raw('set @rownumber='.$offset.''));
      $transaction = $transaction->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'transaction.id as id','transaction.from_id as from_name','transaction.to_id as name','transaction.user_id','transaction.remarks','transaction.transaction_points')->where('user_id',$id)->offset($offset)->limit($perpage)->get();
      $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
      return Response::json(array('data'=>$transaction,'meta'=>$meta));
    }
    public function getAllChildTransaction(Request $request,$id)
    {
        //$user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account WHERE dg_account.user_id = '1' AND dg_account.account_type = 1) as admin_account"))->where('user_id','1')->first();
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        if($page=='1')
        {
            $offset = 0;
        }
        else{
            $offset = ($page-1)*$perpage;
        }
        $transaction = new transaction();
        
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $transaction = $transaction->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'transaction.id as id','transaction.from_id','transaction.to_id','transaction.user_id','transaction.remarks','transaction.transaction_type','transaction.transaction_points')->where('user_id',$id);
        $total = $transaction->count();
        $transaction = $transaction->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=>$transaction,'meta'=>$meta));
    }

    public function getAllUserRequest(Request $request,$id)
    {
      $perpage = $request->datatable['pagination']['perpage'];
      $page = $request->datatable['pagination']['page'];
      $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
      if($page=='1')
      {
        $offset = 0;
      }
      else{
        $offset = ($page-1)*$perpage;
      }
      $notification = new UserNotification();
      $total = $notification->where('to_id',$id)->count();
      DB::statement(DB::raw('set @rownumber='.$offset.''));
      $notification = $notification->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'users.name as child_name','user_notification.request_type','user_notification.request_status','user_notification.points')->join('users', 'users.id', '=', 'user_notification.from_id')->where('user_notification.to_id',$id)->offset($offset)->limit($perpage)->get();
      $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
      return Response::json(array('data'=>$notification,'meta'=>$meta));
    }
    public function addRewardPoints(Request $request)
    {
        $admin_data = Account::select(DB::raw("(SELECT account_number FROM dg_account
                  WHERE dg_account.user_id = '1' AND dg_account.account_type = 1
                ) as admin_account"))->where('user_id','1')->first();
        $id = $request->id;
        foreach($id as $value) {
            $data = User::select('id','type','user_name')->where(['id'=>$value])->first();
            $transaction_data = array(
                  'user_id' => '1',
                  'type' => '6',
                  'transaction_for_id' => '',             
                  'transaction_points' => $request->reward_points,
                  'transaction_type' => '1',              
                  'remarks' => "Rewarded Points by Admin to $data->user_name.",                     
                );
            if($data->type == '2') {
              $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account WHERE dg_account.user_id = $value AND dg_account.account_type = 4) as pool_account"))->where('user_id',$value)->first();
              $transaction_data['from_id'] = $admin_data->admin_account;
              $transaction_data['to_id'] = $user_data->pool_account;
              $transaction_data['account_type'] = '4';
              Transaction::insert($transaction_data); 
            }
            if($data->type == '3') {
              $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account WHERE dg_account.user_id = $value AND dg_account.account_type = 1) as current_account"))->where('user_id',$value)->first();
              $transaction_data['from_id'] = $admin_data->admin_account;
              $transaction_data['to_id'] = $user_data->current_account;
              $transaction_data['account_type'] = '1';
              Transaction::insert($transaction_data);
            }
      }
      return (json_encode(array('status'=>'success','message'=>'Points rewarded Successfully')));
    }
}
