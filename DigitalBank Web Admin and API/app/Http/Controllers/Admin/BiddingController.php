<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Auction;
use App\Model\AuctionCategories;
use Response;
use DB;
use Validator;
use Illuminate\Database\QueryException;

class BiddingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = AuctionCategories::where('status','1')->get();
        return view('admin.bidding.index',compact(['categories']));
    }

    public function viewAllBiddingUsers($id)
    {
        $data = Auction::select('auction.id as id','ac.category_name')->join('auction_categories as ac','ac.id','=','auction.auction_category_id')->where('auction.id',$id)->first();
        return view('admin.bidding.auction_users',compact(['data']));
    }
    
    public function getAllBiders($id,Request $request)
    {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $total = DB::table('auction_bid_logs')->where('auction_id',$id)->count();
        $auction = DB::table('auction_bid_logs as abl')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'ac.category_name','users.name','abl.current_bid_value','abl.last_bid_at')->join('users','users.id','=','abl.user_id')->join('auction as auc','auc.id','=','abl.auction_id')->join('auction_categories as ac','ac.id','=','auc.auction_category_id')->where('auction_id',$id)->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=>$auction,'meta'=>$meta));
    }
    
    public function getAllAuctions(Request $request)
    {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $country_id=(isset($request->datatable['query']['country_id']) && ($request->datatable['query']['country_id']!="") ? $request->datatable['query']['country_id'] : '');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        $auction = new Auction();
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $auction = $auction->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'auction.id as id','auction.auction_category_id','auction.auction_price','auction.single_bid_value','auction.incremented_bid_time','auction.start_time','auction.status','auction.end_time','ac.category_name')->join('auction_categories as ac','ac.id','=','auction.auction_category_id');
        if($country_id !='')
        {
          $auction = $auction->where('ac.country_id',$country_id);
        }
        $total = $auction->count();
        $auction = $auction->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=>$auction,'meta'=>$meta));
    }

    public function addAuction(Request $request)
    {
      $status = "success";
        try {
            $category_id = $request->category_id;
            $single_bid_value = $request->single_bid_value;
            $category_data = AuctionCategories::select('id','base_price')->where('id',$category_id)->first();
            $start_time =  date('Y-m-d H:i:s',strtotime($request->start_time));
            $end_time =  date('Y-m-d H:i:s',strtotime($request->end_time));
            $data = array(   
            'auction_category_id'   => $request->category_id,
            'single_bid_value'      => $request->single_bid_value,
            'incremented_bid_time'  => $request->incremented_time,
            'auction_price'         => $category_data->base_price,
            'start_time'            => $start_time,
            'end_time'              => $end_time,
            'current_auction_value' => $category_data->base_price,
            'auction_closed_at'     => $end_time,
            );
            Auction::insert($data);
            $result = "Auction Inserted Successfully";
        }
        catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result )));
    }

    public function auctionDetails(Request $request)
    {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = Auction::find($request->id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;  
    }

    public function update(Request $request)
    {
        $status = "success";
        try {
            $category_id = $request->category_id;
            $single_bid_value = $request->single_bid_value;
            $category_data = AuctionCategories::select('id','base_price')->where('id',$category_id)->first();
            $start_time =  date('Y-m-d H:i:s',strtotime($request->start_time));
            $end_time =  date('Y-m-d H:i:s',strtotime($request->end_time));
            $data = array(   
            'auction_category_id' => $request->category_id,
            'auction_price'       => $category_data->base_price,
            'single_bid_value'    => $request->single_bid_value,
            'incremented_bid_time'=> $request->incremented_time,
            'start_time'          => $start_time,
            'end_time'            => $end_time,
            'auction_closed_at'   => $end_time,
            );
            // print_r($data);
            // die;
            Auction::where('id',$request->id)->update($data);
            $result = "Auction Updated Successfully";
        }
        catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))); 
    }

    public function changeStatus(Request $request)
    {
        $status = "success";
        $category_status = ($request->status == '1') ? '2' : '1';
        try {
            Auction::where('id',$request->auction_id)->update(['status'=>$category_status]);
            $result = "Category status updated successfully";
        }
        catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))); 
    }

    public function delete(Request $request)
    {
        $status = "success";
        try {
            Auction::where('id',$request->id)->delete($request->id);
            $result = "Auction Deleted Successfully";
        }
        catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result )));
    }
}
