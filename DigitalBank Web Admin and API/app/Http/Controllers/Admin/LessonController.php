<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Category;
use App\Model\LessonOfDay;
use App\Model\LessonOptions;
use Response;
use DB;
//use Auth;

class LessonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = new Category();
        $categories = $categories->select('id','category_name')->get();
        return view('admin.lesson.index',compact('categories'));
    }
    
    public function getAllLessons(Request $request)
    {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        $lesson = new LessonOfDay();
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $lesson = $lesson->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','lesson_title','file_attached','lesson_date','lesson_description','question_type','earn_points','status');
        $total = $lesson->count();
        $lesson = $lesson->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=>$lesson,'meta'=>$meta)); 
    }
    
    public function store_lesson_fill_blanks(Request $request)
    {
        if($request->ajax())
        {
            $minute = "$request->fill_skip_minute";
            $second = "$request->fill_skip_second";
            $skipped_time = '00:'.$minute.':'.$second;
            $data = array(
            'lesson_title'=>$request->fill_question_description,
            'lesson_date'=>date("Y-m-d", strtotime($request->fill_question_input_date)),
            'lesson_description'=>$request->fill_question_input,
            'earn_points'=>$request->fill_lesson_points,
            'link_url'=>$request->fill_shared_url,
            'skipped_time'=>$skipped_time,
            'question_type'=>'1',
            );
            $answers = $request->answer;
            if ($request->hasFile('fill_attached_file'))
            {
                $image = $request->fill_attached_file;
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $type       = $image->getClientOriginalExtension();
                $allowed =  array('jpg','png','jpeg','mp3','mp4');
                if(!in_array($type,$allowed) ) {
                    return Response::json(array('status'=> 'error','message'=>'Please select only image jpg,png,jpeg format Audio in mp3 and video only')) ;
                }
                $path = storage_path('app/lessons');
                $image->move($path, $filename);

                $image_name = $filename;
                $data['file_attached']=$image_name;

            }
            
            if($request->fill_lesson_id !='')
            {
                $id = $request->fill_lesson_id;
                $check=LessonOfDay::whereRaw('DATE(lesson_date) = CURDATE()')
                ->count();
                if($check>1)
                {
                    return (json_encode(array('status'=>'error','message'=>'Lesson already added for same date'))) ;
                }
                DB::table('lesson_of_day')->where('id',$id)->update($data);
                $lesson_id = LessonOfDay::where('id', $id)->first();
                $del_ans = LessonOptions::where('lesson_id',$id);
                $del_ans->delete();
                foreach($answers as $fill_ans)
                {
                    $answer = new LessonOptions();
                    if(strlen($fill_ans)>0)
                    {
                        $answer->lesson_id = $lesson_id->id;
                        $answer->correct_options = $fill_ans;
    
    
                        if($answer->save()){
                            return (json_encode(array('status'=>'success','message'=>'Lesson Of the day Updated succesfully')));
        
                        } else{
                        return (json_encode(array('status'=>'error','message'=>'Failed to Add Question'))) ;
                        }
                    }
    
                }
                
                
            }
            else
            {
               $lesson_count = LessonOfDay::whereRaw('DATE(lesson_date) = CURDATE()')->count();
               if($lesson_count!='0')
               {
                    return (json_encode(array('status'=>'error','message'=>'Lesson already added for same date'))) ;
               }
               LessonOfDay::insert($data);
                $lesson_id = LessonOfDay::orderBy('id', 'desc')->limit(1)->first();
                foreach($answers as $fill_ans)
                {
                    $answer = new LessonOptions();
    
                    if(strlen($fill_ans)>0)
                    {
                        $answer->lesson_id = $lesson_id->id;
                        $answer->correct_options = $fill_ans;
    
    
                        if($answer->save()){
        
                        } else{
                        return (json_encode(array('status'=>'error','message'=>'Failed to Add Question'))) ;
                        }
                    }
    
                }
                return (json_encode(array('status'=>'success','message'=>'Lesson Of the day successfully saved'))); 
            }
            
            
        }
    }
    
    public function store_lesson_multi_choice(Request $request)
    {
        //print_r($_POST); 
        //print_r($_FILES); 
        if($request->ajax())
        {
            
            $minute = "$request->multi_skip_minute";
            $second = "$request->multi_skip_second";
            $skipped_time = '00:'.$minute.':'.$second;
            $data = array(
            'lesson_title'=>$request->multi_question_description,
            'lesson_date'=>date("Y-m-d", strtotime($request->multi_question_input_date)),
            'lesson_description'=>$request->multiQuestion_input,
            'earn_points'=>$request->multi_lesson_points,
            'link_url'=>$request->multi_shared_url,
            'skipped_time'=>$skipped_time,
            'question_type'=>'2',
            );
            $options = $request->options[0];
            $correct_answer = $request->correct_answer[0];
            $correct_answer = explode(',',$correct_answer);
            $options = explode(',',$options);
            $image_name = '';
            if ($request->hasFile('multi_attached_file'))
            {
                $image = $request->multi_attached_file;
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $type       = $image->getClientOriginalExtension();
                $allowed =  array('jpg','png','jpeg','mp3','mp4');
                if(!in_array($type,$allowed) ) {
                    return Response::json(array('status'=> 'error','message'=>'Please select only image jpg,png,jpeg format Audio in mp3 and video only')) ;
                }
                $path = storage_path('app/lessons');
                $image->move($path, $filename);

                $image_name = $filename;

            }
            $data['file_attached']=$image_name;
            
            
            if (in_array("1", $correct_answer)){
                if($request->multi_lesson_id !='')
                {
                    $id = $request->multi_lesson_id;
                    $check=LessonOfDay::whereRaw('DATE(lesson_date) = CURDATE()')
                    ->where(function($query) use ($id){
                        if(isset($id)){
                          $query->where('id' , '<>' ,$id);
                        }
                    })->exists();
                    if($check)
                    {
                        return (json_encode(array('status'=>'error','message'=>'Lesson already added for same date'))) ;
                    }
                    $lesson_id = LessonOfDay::where('id', $id)->first();
                    DB::table('lesson_of_day')->where('id',$id)->update($data);
                    $del_ans = LessonOptions::where('lesson_id',$id);
                    $del_ans->delete();
                    foreach( $options as $index => $option ) {
                             $answer = new LessonOptions();
                            
                            $answer->lesson_id = $lesson_id->id;
                            $answer->options = $option;
                           
                            if($correct_answer[$index]==1){
                              $answer->correct_options = $correct_answer[$index];
                            }else{
                              $answer->correct_options = 0; 
                            }
    
                            if($answer->save()){
              
                            } else{
                               return (json_encode(array('status'=>'error','message'=>'Failed to Add Question'))) ;
                            }
                      }
                    
                    
                }
                else
                {
                    $lesson_count = LessonOfDay::whereRaw('DATE(lesson_date) = CURDATE()')->count();
                    if($lesson_count!='0')
                    {
                        return (json_encode(array('status'=>'error','message'=>'Lesson already added for same date'))) ;
                    }
                    LessonOfDay::insert($data);
                    $lesson_id = LessonOfDay::orderBy('id', 'desc')->limit(1)->first();
                    foreach( $options as $index => $option ) {
                             $answer = new LessonOptions();
                            
                            $answer->lesson_id = $lesson_id->id;
                            $answer->options = $option;
                           
                            if($correct_answer[$index]==1){
                              $answer->correct_options = $correct_answer[$index];
                            }else{
                              $answer->correct_options = 0; 
                            }
    
                            if($answer->save()){
              
                            } else{
                               return (json_encode(array('status'=>'error','message'=>'Failed to Add Question'))) ;
                            }
                      }
                }
            }
            else{
              return (json_encode(array('status'=>'error','message'=>'Check correct answer(s)')));
            }
            return (json_encode(array('status'=>'success','message'=>'Lesson Of the day successfully saved')));
            
        }
    }
    
    public function store_lesson_arrange_order(Request $request)
    {
        // print_r($_POST); 
        // print_r($_FILES); die;
        if($request->ajax())
        {
            
            $minute = "$request->arrange_skip_minute";
            $second = "$request->arrange_skip_second";
            $skipped_time = '00:'.$minute.':'.$second;
            $data = array(
            'lesson_title'=>$request->arrange_question_description,
            'lesson_date'=>date("Y-m-d", strtotime($request->arrange_question_input_date)),
            'lesson_description'=>$request->arrangeQuestion_input,
            'earn_points'=>$request->arrange_lesson_points,
            'link_url'=>$request->arrange_shared_url,
            'skipped_time'=>$skipped_time,
            'question_type'=>'3',
            );
            $incorrect_order = $request->incorrect_order[0];
            $correct_order = $request->correct_order[0];
            $incorrect_order = explode(',',$incorrect_order);
            $correct_order = explode(',',$correct_order);
            $image_name = '';
            if ($request->hasFile('arrange_attached_file'))
            {
                $image = $request->arrange_attached_file;
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $type       = $image->getClientOriginalExtension();
                $allowed =  array('jpg','png','jpeg','mp3','mp4');
                if(!in_array($type,$allowed) ) {
                    return Response::json(array('status'=> 'error','message'=>'Please select only image jpg,png,jpeg format Audio in mp3 and video only')) ;
                }
                $path = storage_path('app/lessons');
                $image->move($path, $filename);

                $image_name = $filename;

            }
            $data['file_attached']=$image_name;
            if($request->arrange_lesson_id !='')
                {
                    $id = $request->arrange_lesson_id;
                    $check=LessonOfDay::whereRaw('DATE(lesson_date) = CURDATE()')
                    ->where(function($query) use ($id){
                        if(isset($id)){
                          $query->where('id' , '<>' ,$id);
                        }
                    })->exists();
                    if($check)
                    {
                        return (json_encode(array('status'=>'error','message'=>'Lesson already added for same date'))) ;
                    }
                    $lesson_id = LessonOfDay::where('id', $id)->first();
                    DB::table('lesson_of_day')->where('id',$id)->update($data);
                    $del_ans = LessonOptions::where('lesson_id',$id);
                    $del_ans->delete();
                    foreach( $incorrect_order as $index => $incorrectorder ) {
                        $answer = new LessonOptions();
                                
                        $answer->lesson_id = $lesson_id->id;
                        $answer->options = $incorrectorder;
                        $answer->correct_options = $correct_order[$index];
                        if($answer->save()){
          
                        } else{
                           return (json_encode(array('status'=>'error','message'=>'Failed to Add Question'))) ;
                        }
                    }
                }
                else
                {
                    $lesson_count = LessonOfDay::whereRaw('DATE(lesson_date) = CURDATE()')->count();
                    if($lesson_count!='0')
                    {
                        return (json_encode(array('status'=>'error','message'=>'Lesson already added for same date'))) ;
                    }
                    LessonOfDay::insert($data);
                    $lesson_id = LessonOfDay::orderBy('id', 'desc')->limit(1)->first();
                    foreach( $incorrect_order as $index => $incorrectorder ) {
                        $answer = new LessonOptions();
                                
                        $answer->lesson_id = $lesson_id->id;
                        $answer->options = $incorrectorder;
                        $answer->correct_options = $correct_order[$index];
                        if($answer->save()){
          
                        } else{
                           return (json_encode(array('status'=>'error','message'=>'Failed to Add Question'))) ;
                        }
                    }
                    return (json_encode(array('status'=>'success','message'=>'Lesson Of the day successfully saved')));
                }
            
        }
    }
    
    public function getLessonDetail(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])

          $result = LessonOfDay::where(array('id'=>$request->id))->first();
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }

        if($request->id)
          {
            $allAnswers = LessonOptions::where(array('lesson_id'=>$request->id))
            ->get(array(
              'id',
              'lesson_id',
                'options',
                'correct_options',
              ));

            $corect_ans = LessonOptions::where(array('lesson_id'=>$request->id))
            ->get(array(
              'id',
              'lesson_id',
                'options',
                'correct_options',
              ))->sortBy('correct_answer',false);
            $res = array();
            $answ = array();


            if(!empty($allAnswers) || !empty($corect_ans))
            {
              foreach($allAnswers as $ans)
              {
                $data = array(
                  'id'  => $ans->id,
                  'lesson_id'  => $ans->lesson_id,
                  'options'  => $ans->options,
                  'correct_options'  => $ans->correct_options
                );
                array_push($res,$data);
              }

              foreach($corect_ans as $ans)
              {
                $data = array(
                  'id'  => $ans->id,
                  'lesson_id'  => $ans->lesson_id,
                  'options'  => $ans->options,
                  'correct_options'  => $ans->correct_options,
                );
                array_push($answ,$data);
              }
             return (json_encode(array('status'=>$status,'message'=>$result,'answers'=>$res,'correctOrder'=>$answ))) ;
            }

          }

                 
      }
    }
    
    public function delete(Request $request)
    {
      $status = "success";
        try {
          if(LessonOptions::where('lesson_id',$request->id)->exists())
          {
            LessonOptions::where('lesson_id',$request->id)->delete();
          }
            LessonOfDay::where('id',$request->id)->delete($request->id);
            $result = "Lesson Deleted Successfully";
        }
        catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result )));
    }
}
