<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Model\Category;
use App\Model\Task;
use App\Model\Transaction;
use App\Model\User;
use App\Model\LessonOfDay;
use App\Model\Countries;
use App\Model\Account;
use Validator;
use DB;
Use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
//use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('isAdmin');
    }
    
    public function register(Request $request)
    {
        $url = $request->url();
        $refer_account_number = \Request::segment(2);
        if($refer_account_number!=='' && User::where('digital_key',$refer_account_number)->exists())
        {
            $refer_data=User::where('digital_key',$refer_account_number)->first();
            $countries = \DB::table('countries')->select('id','country_name')->get()->toArray();
            //print_r($refer_data); die;
            return view('register',compact('refer_data','countries'));
        }
        else
        {
           return view('error'); 
        }
    }
    
    public function store(Request $request)
    {
        $rules = array(
        'country_id' => 'required',
        'user_name'     => 'required|unique:users',
        'password'     => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $failedRules = $validator->getMessageBag()->toArray();
            $errorMsg = "";
            if(isset($failedRules['country_id']))
              $errorMsg = $failedRules['country_id'][0] . "\n";
            if(isset($failedRules['user_name']))
              $errorMsg = $failedRules['user_name'][0] . "\n";
              if(isset($failedRules['password']))
              $errorMsg = $failedRules['password'][0] . "\n";
            return (json_encode(array('status'=>'error','message'=>$errorMsg)));
        } else{
        $user= new User();
        if($request->has('mobile'))
        {
            if(!is_numeric($request->input('mobile')))
            {
                return (json_encode(array('status'=>'error','message'=>'Mobile number must be a number..' ))); 
            }
            $user->mobile= $request->input('mobile');
        }
        if($request->has('email'))
        {
            if (!filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
                return (json_encode(array('status'=>'error','message'=>'Enter Valid Email Address.' ))); 
            }
            $user->email= $request->input('email');
        }
        $account_type = $_ENV['ACCOUNT_TYPE'];
        $user->name= $request->name;
        $user->user_name= $request->user_name;
        $user->password= bcrypt($request->input('password'));
        $user->refer_id=$request->refer_id;
        if($user->save())
        {
            if($request->refer_id!='')
            {
                $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = $user->refer_id AND dg_account.account_type = 4
                        ) as pool_account"),DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = $user->refer_id AND dg_account.account_type = 1
                        ) as current_account"))->where('user_id',$user->refer_id)->first();
                $admin_data = Account::select(DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = '1' AND dg_account.account_type = 1
                        ) as admin_account"))->where('user_id','1')->first();
                $data = array(
                    'user_id' => $user->refer_id,
                    'from_id' => $admin_data->admin_account,
                    'to_id' => $user_data->current_account,
                    'type' => '5',
                    'account_type' => '1',
                    'transaction_points' => '50',
                    'transaction_type'  => '1',
                    'remarks' => "'Earn Points After Refering Child' $user->name",
                );
                DB::table('transaction')->insert($data);
                $notify_data = array(
                    'from_id' => '1',
                    'to_id' => $user->refer_id,
                    'message' => "Earn Points After Refering Child $user->user_name",
                );
                DB::table('notifications')->insert($notify_data);
            }
            return (json_encode(array('status'=>'success','message'=>'Registered Sucessfully Now login in App.' ))); 
        }
        else
        {
            return (json_encode(array('status'=>'error','message'=>'Please try again.' ))); 
        }
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }
    
    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        $remember = $request->input('remember');
        if($request->has('remember'))
        {
            Session::put('remember', '1');
        }

        if (Auth::attempt(['email' => $email, 'password' => $password]))
        {
            // Authentication passed...
            return 'success';
        }
        else
        {
            return 'failed';
        }
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

    protected function guard()
    {
        return Auth::guard();
    }
    
    public function getCountryCode(Request $request)
    {
      try
      {
        if($request->ajax())
        {
          if($request->id){
            $data = Countries::where(['id'=>$request->id])->first();
            if(!empty($data))
            {
              return (json_encode(array('status'=>'success','message'=>"Country code details.",'data'=>$data)));
            }
            else
            {
              return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
            }
            
          }
          return (json_encode(array('status'=>'error','message'=>"please select country first.",'data'=>'')));
          
        }
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }

    }


    public function dashboard()
    {
        $user = Auth::user();
        Session::put('name', $user->name);
        Session::put('email', $user->email);
        //Session::put('remember', '1');
        $count_users        = User::where('type','!=','1')->count();
        $count_child        = User::where('type','2')->count();
        $count_parents      = User::where('type','3')->count();
        $count_categories   = Category::count();
        $count_tasks        = Task::count();
        $count_lessons      = LessonOfDay::count();
        return view('dashboard',compact('count_users','count_child','count_parents','count_categories','count_tasks','user','count_lessons')); 
    }

    public function getAdminTransaction()
    {
        $data = User::where('type','1')->first();
        return view('admin.admin_transaction',compact(['data']));
    }
    
    public function getAllCountriesData()
    {
        $country = array();
        $countries = Countries::select('country_name')->get()->toArray();
        foreach($countries as $countries)
        {
            array_push($country,$countries['country_name']);
        };
        return json_encode($country);
    }

    public function getAllAdminTransaction(Request $request)
    {
        $admin_data = Account::select(DB::raw("(SELECT account_number FROM dg_account WHERE dg_account.user_id = '1' AND dg_account.account_type = 1) as admin_account"))->where('user_id','1')->first();
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        $transaction = new Transaction();
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $currentMonth = date('m');
        $transaction = $transaction->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'transaction.id as id','transaction.created_at','transaction.from_id','transaction.to_id','transaction.user_id','transaction.transaction_type','transaction.remarks','transaction.transaction_points')->where('transaction.from_id',$admin_data->admin_account)->orwhere('transaction.to_id',$admin_data->admin_account)->whereRaw('MONTH(dg_transaction.created_at) = ?',[$currentMonth]);
        $total = $transaction->count();
        $transaction = $transaction->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=>$transaction,'meta'=>$meta));
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();
        $data = array(
            'password' => Hash::make($request->password),
        );
        $query = \DB::table('users')->where('id',$user->id)->update($data);
        //\Session::flash('admin_message', "Please Login With New Password");
        return (json_encode(array('status'=>'success','message'=>"Password Updated Successfully")));
    }
    public function policy(Request $request)
    {
        return view('admin.policy');
    }
    public function terms(Request $request)
    {
        return view('admin.terms');
    }
}
