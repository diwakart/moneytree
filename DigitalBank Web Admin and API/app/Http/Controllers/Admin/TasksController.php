<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Task;
use App\Model\Option;
use App\Model\Category;
use Response;
use DB;
//use Auth;

class TasksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = new Category();
        $categories = $categories->select('id','category_name')->get();
        return view('admin.tasks.index',compact('categories'));
    }

    public function getAllTasks(Request $request)
    {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $task_list=(isset($request->datatable['query']['task_list']) && ($request->datatable['query']['task_list']!="") ? $request->datatable['query']['task_list'] : '');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        $tasks = new task();
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $tasks = $tasks->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),DB::raw("'$task_list as task_list'"),'task.id as id','task.task_name','category.category_name','task.question_type','task.file_attached','task.task_points','task.status')->join('category', 'category.id', '=', 'task.category_id');
        if($task_list!='')
        {
          if($page=='1')
          {
            $offset = 0;
          }
          else{
            $offset = ($page-1)*$perpage;
          }
          $tasks = $tasks->where('task.category_id',$task_list);
        }
        $total = $tasks->count();
        $tasks = $tasks->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=>$tasks,'meta'=>$meta)); 
    }

    public function addPoints(Request $request)
    {
      $status = "success";
      try {
        $points = $request->points;
        $task_id = $request->task_id;
        DB::table('task')->whereIn('id',$task_id)->increment('task_points', $points);
        $result = "Point value added to selected Tasks";
      }
      catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result )));
    }

    public function store_question_fill_blanks(Request $request)
    {
        //print_r($_POST); die;
        $user = Auth::user();
        if($request->ajax())
        {
            $minute = "$request->fill_skip_minute";
            $second = "$request->fill_skip_second";
            $skipped_time = '00:'.$minute.':'.$second;
            $question;
            $answer;
            $answers       = $request->answer;
            $attempt_1     = $request->fill_attempt_1;
            $attempt_2     = $request->fill_attempt_2;
            $attempt_3     = $request->fill_attempt_3;
            $attempt_above = $request->fill_attempt_above;
            if($attempt_2>$attempt_1)
            {
              return (json_encode(array('status'=>'error','message'=>'Attempt 2 earn points not greater than Attempt 1')));
            }
            if($attempt_3>$attempt_2)
            {
              return (json_encode(array('status'=>'error','message'=>'Attempt 3 earn points not greater than Attempt 2')));
            }
            if($attempt_above>$attempt_3)
            {
              return (json_encode(array('status'=>'error','message'=>'Attempt 3 above earn points not greater than Attempt 3')));
            }
            if($answers!='')
            {
                if(isset($request->fill_question_id))
                {
                    $del_ans = Option::where('task_id',$request->fill_question_id);
                    $del_ans->delete();
                    $question = Task::find($request->fill_question_id);
                    $question->task_name = htmlspecialchars($request->fill_question_input);
                }
                else
                {
                    $question = new Task();
                }
                
                if ($request->hasFile('fill_attached_file'))
                {
                    $image = $request->fill_attached_file;
                    $filename = time() . '.' . $image->getClientOriginalExtension();
                    $type       = $image->getClientOriginalExtension();
                    $allowed =  array('jpg','png','jpeg','mp3','mp4');
                    if(!in_array($type,$allowed) ) {
                        return Response::json(array('status'=> 'error','message'=>'Please select only image jpg,png,jpeg format Audio in mp3 and video only')) ;
                    }
                    $path = storage_path('app/tasks');
                    $image->move($path, $filename);
    
                    $image_name = $filename;
                    $question->file_attached=$image_name;
    
                }

                $question->category_id    = $request->fill_category_id;
                $question->task_name      = htmlspecialchars($request->fill_question_input);
                $question->question_type  = 1;
                $question->task_points    = $request->fill_task_points;
                $question->attempt_1      = $attempt_1;
                $question->attempt_2      = $attempt_2;
                $question->attempt_3      = $attempt_3;
                $question->above_3        = $attempt_above;
                $question->link_url       = $request->fill_shared_url;
                $question->skipped_time   = $skipped_time;
                if($question->save()){
                $last_insert_id = $question->id;
                $data = array(
                    'type' =>'1',
                    'notification_for_id' => $last_insert_id,
                    'start_row'=>'0',
                    'end_row'=>'100',
                );
                    if(!DB::table('send_notification')->where(['type'=>'1','notification_for_id' => $last_insert_id])->exists())
                    {
                        DB::table('send_notification')->insert($data);
                    }
                }

                /*if(isset($request->answer_id)){
                $answer = Option::find($request->answer_id);


                }else{*/
                foreach($answers as $fill_ans)
                {
                $answer = new Option();

                if(strlen($fill_ans)>0)
                {
                $answer->task_id = $last_insert_id;
                $answer->correct_options = $fill_ans;


                if($answer->save()){

                } else{
                return (json_encode(array('status'=>'error','message'=>'Failed to Add Question'))) ;
                }
                }

                }/*
                }*/

                return (json_encode(array('status'=>'success','message'=>'Question successfully saved')));
            }
            else
            {
                return (json_encode(array('status'=>'error','message'=>'Answer is not filled')));
            }

        }

    }



    public function store_question_multi_choice(Request $request){
        //print_r($_POST); die;
     $user = Auth::user();
     if($request->ajax())
      {
        $question;
        $answer;
        $minute = "$request->multi_skip_minute";
        $second = "$request->multi_skip_second";
        $skipped_time = '00:'.$minute.':'.$second;
        $options = $request->options[0];
        $correct_answer = $request->correct_answer[0];
        $correct_answer = explode(',',$correct_answer);
        $options = explode(',',$options);
        $attempt_1     = $request->multi_attempt_1;
        $attempt_2     = $request->multi_attempt_2;
        $attempt_3     = $request->multi_attempt_3;
        $attempt_above = $request->multi_attempt_above;
        if($attempt_2>$attempt_1)
        {
          return (json_encode(array('status'=>'error','message'=>'Attempt 2 earn points not greater than Attempt 1')));
        }
        if($attempt_3>$attempt_2)
        {
          return (json_encode(array('status'=>'error','message'=>'Attempt 3 earn points not greater than Attempt 2')));
        }
        if($attempt_above>$attempt_3)
        {
          return (json_encode(array('status'=>'error','message'=>'Attempt 3 above earn points not greater than Attempt 3')));
        }

        if($request->multi_question_input!=''){
        if($options!=''){
         
         if (in_array("1", $correct_answer)){
            if(isset($request->multi_question_id)){
               $del_ans = Option::where('task_id',$request->multi_question_id);
               $del_ans->delete();

                $question = Task::find($request->multi_question_id);
                $question->task_name = htmlspecialchars($request->multi_question_input);
            }else{
              $question = new Task();
            }


               if ($request->hasFile('multi_attached_file'))
                {
                    $image = $request->multi_attached_file;
                    $filename = time() . '.' . $image->getClientOriginalExtension();
                    $type       = $image->getClientOriginalExtension();
                    $allowed =  array('jpg','png','jpeg','mp3','mp4');
                    if(!in_array($type,$allowed) ) {
                        return Response::json(array('status'=> 'error','message'=>'Please select only image jpg,png,jpeg format Audio in mp3 and video only')) ;
                    }
                    $path = storage_path('app/tasks');
                    $image->move($path, $filename);
    
                    $image_name = $filename;
                    $question->file_attached=$image_name;
    
                }

                $question->link_url       = $request->multi_shared_url;
                $question->skipped_time   = $skipped_time;
               $question->task_name = htmlspecialchars($request->multi_question_input);
               $question->category_id = $request->multi_category_id;
               $question->question_type= 2;
               $question->task_points= $request->multi_task_points;
               $question->attempt_1      = $attempt_1;
               $question->attempt_2      = $attempt_2;
               $question->attempt_3      = $attempt_3;
               $question->above_3        = $attempt_above;
              if($question->save()){
                  
                $last_insert_id = $question->id;
                $data = array(
                    'type' =>'1',
                    'notification_for_id' => $last_insert_id,
                    'start_row'=>'0',
                    'end_row'=>'100',
                );
                    if(!DB::table('send_notification')->where(['type'=>'1','notification_for_id' => $last_insert_id])->exists())
                    {
                        DB::table('send_notification')->insert($data);
                    }
              }

                
                  foreach( $options as $index => $option ) {
                         $answer = new Option();
                        
                        $answer->task_id = $last_insert_id;
                        $answer->options = $option;
                       
                        if($correct_answer[$index]==1){
                          $answer->correct_options = $correct_answer[$index];
                        }else{
                          $answer->correct_options = 0; 
                        }

                        if($answer->save()){
          
                        } else{
                           return (json_encode(array('status'=>'error','message'=>'Failed to Add Question'))) ;
                        }
                  }

              return (json_encode(array('status'=>'success','message'=>'Question successfully saved')));
            }else{
              return (json_encode(array('status'=>'error','message'=>'Check correct answer(s)')));
            }
          }else{
             return (json_encode(array('status'=>'error','message'=>'Please fill all input box')));
          }
          }else{
             return (json_encode(array('status'=>'error','message'=>'Please fill all input box')));
          }



        }

    }



    public function store_question_arrange_order(Request $request){
     $user = Auth::user();
     if($request->ajax())
      {
       
        $question;
        $answer;
        $minute = "$request->arrange_skip_minute";
        $second = "$request->arrange_skip_second";
        $skipped_time = '00:'.$minute.':'.$second;
        $incorrect_order = $request->incorrect_order[0];
        $correct_order = $request->correct_order[0];
        $incorrect_order = explode(',',$incorrect_order);
        $correct_order = explode(',',$correct_order);
        $attempt_1     = $request->arrange_attempt_1;
        $attempt_2     = $request->arrange_attempt_2;
        $attempt_3     = $request->arrange_attempt_3;
        $attempt_above = $request->arrange_attempt_above;
        if($attempt_2>$attempt_1)
        {
          return (json_encode(array('status'=>'error','message'=>'Attempt 2 earn points not greater than Attempt 1')));
        }
        if($attempt_3>$attempt_2)
        {
          return (json_encode(array('status'=>'error','message'=>'Attempt 3 earn points not greater than Attempt 2')));
        }
        if($attempt_above>$attempt_3)
        {
          return (json_encode(array('status'=>'error','message'=>'Attempt 3 above earn points not greater than Attempt 3')));
        }

        
       if($request->arrange_question_input!=''){
        if($incorrect_order!=''){
         
         if ($correct_order!=''){
            if(isset($request->arrange_question_id)){
               $del_ans = Option::where('task_id',$request->arrange_question_id);
               $del_ans->delete();
               
                $question = Task::find($request->arrange_question_id);
                $question->task_name = htmlspecialchars($request->arrange_question_input);
            }else{
              $question = new Task();
            }

                if ($request->hasFile('multi_attached_file'))
                {
                    $image = $request->multi_attached_file;
                    $filename = time() . '.' . $image->getClientOriginalExtension();
                    $type       = $image->getClientOriginalExtension();
                    $allowed =  array('jpg','png','jpeg','mp3','mp4');
                    if(!in_array($type,$allowed) ) {
                        return Response::json(array('status'=> 'error','message'=>'Please select only image jpg,png,jpeg format Audio in mp3 and video only')) ;
                    }
                    $path = storage_path('app/tasks');
                    $image->move($path, $filename);
    
                    $image_name = $filename;
                    $question->file_attached=$image_name;
    
                }
                $question->link_url       = $request->multi_shared_url;
                $question->skipped_time   = $skipped_time;
                $question->task_name = htmlspecialchars($request->arrange_question_input);
                $question->category_id = $request->arrange_category_id;
                $question->question_type= 3;
                $question->task_points= $request->arrange_task_points;
                $question->attempt_1      = $attempt_1;
                $question->attempt_2      = $attempt_2;
                $question->attempt_3      = $attempt_3;
                $question->above_3        = $attempt_above;
              if($question->save()){
                $last_insert_id = $question->id;
                $data = array(
                    'type' =>'1',
                    'notification_for_id' => $last_insert_id,
                    'start_row'=>'0',
                    'end_row'=>'100',
                );
                    if(!DB::table('send_notification')->where(['type'=>'1','notification_for_id' => $last_insert_id])->exists())
                    {
                        DB::table('send_notification')->insert($data);
                    }
              }
                foreach( $incorrect_order as $index => $incorrectorder ) {
                        $answer = new Option();
                        $answer->task_id = $last_insert_id;
                        $answer->options = $incorrectorder;
                        $answer->correct_options = $correct_order[$index];
                        if($answer->save()){
          
                        } else{
                           return (json_encode(array('status'=>'error','message'=>'Failed to Add Question'))) ;
                        }
                }

              return (json_encode(array('status'=>'success','message'=>'Question successfully saved')));
            }else{
              return (json_encode(array('status'=>'error','message'=>'Arrange correct order')));
            }
          }else{
             return (json_encode(array('status'=>'error','message'=>'Please fill all input box')));
          }

          }else{
             return (json_encode(array('status'=>'error','message'=>'Please fill all input box')));
          }

        }

    }

    public function getquestionDetail(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])

          $result = DB::table('task')->select('task.*')->where(array('task.id'=>$request->id))->first();
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }

        if($request->id)
          {
            $allAnswers = Option::where(array('task_id'=>$request->id))
            ->get(array(
              'id',
              'task_id',
                'options',
                'correct_options',
              ));

            $corect_ans = Option::where(array('task_id'=>$request->id))
            ->get(array(
              'id',
              'task_id',
                'options',
                'correct_options',
              ))->sortBy('correct_answer',false);
            $res = array();
            $answ = array();


            if(!empty($allAnswers) || !empty($corect_ans))
            {
              foreach($allAnswers as $ans)
              {
                $data = array(
                  'id'  => $ans->id,
                  'task_id'  => $ans->task_id,
                  'options'  => $ans->options,
                  'correct_options'  => $ans->correct_options
                );
                array_push($res,$data);
              }

              foreach($corect_ans as $ans)
              {
                $data = array(
                  'id'  => $ans->id,
                  'task_id'  => $ans->task_id,
                  'options'  => $ans->options,
                  'correct_options'  => $ans->correct_options,
                );
                array_push($answ,$data);
              }
             return (json_encode(array('status'=>$status,'message'=>$result,'answers'=>$res,'correctOrder'=>$answ))) ;
            }

          }

                 
      }
    }

    public function delete(Request $request)
    {
      $status = "success";
        try {
          if(Option::where('task_id',$request->id)->exists())
          {
            \DB::table('option')->where('task_id',$request->id)->delete();
          }
            Task::where('id',$request->id)->delete($request->id);
            $result = "Task Deleted Successfully";
        }
        catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result )));
    }
}
