<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Countries;
use Response;
use DB;
use Validator;

class CountriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('admin.countries.index');
    }

    /*public function add(Request $request)
    {
    	//print_r($request->all());die;

        $rules = array(
          'country_name'     => 'required',
          'short_code'       => 'required|alpha',
          'country_code'     => 'required|numeric', 
        );


        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['country_name']))
            $errorMsg = $failedRules['country_name'][0] . "\n";
          if(isset($failedRules['short_code']))
            $errorMsg = $failedRules['short_code'][0] . "\n";
        if(isset($failedRules['country_code']))
            $errorMsg = $failedRules['country_code'][0] . "\n";
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else
        {
          if($request->id)
          {
            // if($request->status=='1')
            // {
            //   return (json_encode(array('status'=>'error','message'=>'Please Inactive package to edit any details')));
            // }
            $data = array(
              'name' => $request->country_name,
              'code' => $request->short_code,
              'country_code' => $request->country_code,
              
            );
            try 
            {
              Countries::where('id',$request->id)->update($data);
              return (json_encode(array('status'=>'success','message'=>'Country '.$request->name.' Updated Successfully')));
               
            }
            catch(\Illuminate\Database\QueryException $ex)
            { 
              $result = $ex->getMessage();
              return (json_encode(array('status'=>'error','message'=>$result))) ;
            }
          }
            else
            {
              $data = array(
              'name' => $request->country_name,
              'code' => $request->short_code,
              'country_code' => $request->country_code,
              );

              $data['country_code'] = '+'.$data['country_code'];
              
              $check_name = DB::table('countries')->where('name',$request->country_name)->exists();
              if($check_name)
              {
                return (json_encode(array('status'=>'error','message'=>'Country Name already exist.'))) ;
              }

              $check_code = DB::table('countries')->where('code',$request->short_code)->exists();
              if($check_code)
              {
                return (json_encode(array('status'=>'error','message'=>'Code already exist.'))) ;
              }

              $check_country_code = DB::table('countries')->where('country_code',$request->country_code)->exists();
              if($check_country_code)
              {
                return (json_encode(array('status'=>'error','message'=>'Country Code already exist.'))) ;
              }
              try 
              {

                Countries::insert($data);
                return (json_encode(array('status'=>'success','message'=>'countries '.$request->name.' Inserted Successfully')));
                 
              }
              catch(\Illuminate\Database\QueryException $ex)
              { 
                $result = $ex->getMessage();
                return (json_encode(array('status'=>'error','message'=>$result))) ;
              }
            }
        }
    }*/
    
    public function add(Request $request)
    {
        try 
        {
          if($request->id){
            $countries = Countries::find($request->id);
            $id = $request->id;
            if($request->country_name!='')
            {
              $check_name=Countries::where(['country_name'=>$request->country_name])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();
              if($check_name)
              {
                return (json_encode(array('status'=>'error','message'=>'Country name already exist'))) ;
              }
              $countries->country_name = $request->country_name;
            }
            if($request->code!='')
            {
              $check_code=Countries::where(['code'=>$request->code])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();
              if($check_code)
              {
                return (json_encode(array('status'=>'error','message'=>'Country short code already exist'))) ;
              }
              $countries->code = $request->code;
            }
            if($request->country_code!='')
            {
              $country_code=Countries::where(['country_code'=>$request->country_code])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();
              if($country_code)
              {
                return (json_encode(array('status'=>'error','message'=>'Country Code already exist'))) ;
              }
              $countries->country_code = $request->country_code;
            }
            try {
              $countries->save();
              return (json_encode(array('status'=>'success','message'=>sprintf('Country successfully updated', $countries->name))));
            }catch(\Illuminate\Database\QueryException $ex){
              $error_code = $ex->errorInfo[1];
              if($error_code == 1062){
                $result = $ex->getMessage();
                return (json_encode(array('status'=>'error','message'=>'Country already exist'))) ;
              }
            }
    
          }
          else
          {
            $rules = array(
              'country_name'    => 'required|unique:countries',
              'code'            =>  'required|alpha|unique:countries',
              'country_code'    =>  'required|unique:countries',
            );
            $validator = Validator::make($request->all(), $rules);
    
            if ($validator->fails()) 
            {
              $failedRules = $validator->getMessageBag()->toArray();
              $errorMsg = "";
              if(isset($failedRules['country_name']))
                $errorMsg = $failedRules['country_name'][0] . "\n";
              if(isset($failedRules['code']))
                $errorMsg = $failedRules['code'][0] . "\n";
              if(isset($failedRules['country_code']))
                $errorMsg = $failedRules['country_code'][0] . "\n";
            
              return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
            }
            else
            {
              $countries = new Countries();
              $countries->country_name = $request->country_name;
              $countries->code = $request->code;
              $countries->country_code = $request->country_code;
              try 
              {
                $countries->save();
                return (json_encode(array('status'=>'success','message'=>sprintf('Successfully saved', $countries->name))));
              }
              catch(\Illuminate\Database\QueryException $ex)
              {
                $error_code = $ex->errorInfo[1];
                if($error_code == 1062)
                {
                  $result = $ex->getMessage();
                  return (json_encode(array('status'=>'error','message'=>'Country already exist'))) ;
                }
              }
            }
          }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
          return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
        }
    }

    public function getAllCountries(Request $request)
    {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $country_name=(isset($request->datatable['query']['country_name']) && ($request->datatable['query']['country_name']!="") ? $request->datatable['query']['country_name'] : '');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        $countries = new Countries();
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $countries = $countries->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','country_name','code','country_code');
        if($country_name!='')
        {
            $countries = $countries->where('country_name','like',$country_name.'%');
        }
        $total = $countries->count();
        $countries = $countries->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];        
        return Response::json(array('data'=>$countries,'meta'=>$meta)); 
    }

    public function getCountryDetails(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = Countries::find($request->id);
          //print_r($result);die;
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }

    public function delete(Request $request)
    {
      if($request->ajax())
      {
        if($request->id){
          $result = Countries::find($request->id);
          if ($result->delete())
            return (json_encode(array('status'=>'success','message'=>"Country Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }
    }   

}
