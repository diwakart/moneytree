<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\UserPackages;
use Response;
use DB;
use Validator;
use \Stripe\Plan;
//use Auth;

class PackagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = \DB::table('countries')->select('id','country_name')->get()->toArray();
        $currencies = \DB::table('countries')->select('currency')->distinct('currency')->get();
        return view('admin.package.index',compact(['countries','currencies']));
    }

    public function getAllPackages(Request $request)
    {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $country_id=(isset($request->datatable['query']['country_id']) && ($request->datatable['query']['country_id']!="") ? $request->datatable['query']['country_id'] : '');
        $plan_type=(isset($request->datatable['query']['plan_type']) && ($request->datatable['query']['plan_type']!="") ? $request->datatable['query']['plan_type'] : '');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        $packages = new UserPackages();
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $packages = $packages->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'user_packages.id as id','user_packages.name','user_packages.currency','user_packages.recurring_amount','user_packages.one_time_amount','user_packages.one_time_points','user_packages.status','user_packages.recurring_points',\DB::raw('group_concat(dg_pc.country_id) as country_ids'),\DB::raw('group_concat(dg_co.country_name) as country_name'));
        $total = $packages->count();
        $packages = $packages->join('package_country as pc','pc.package_id','=','user_packages.id')->join('countries as co','co.id','=','pc.country_id');
        if($country_id !='')
        {
            $packages = $packages->where('pc.country_id',$country_id);
            $total = $packages->count();
        }
        /*if($plan_type !='')
        {
            $packages = $packages->where('user_packages.plan_type',$plan_type);
            $total = $packages->count();
        }*/
        $packages = $packages->groupBy('user_packages.id')->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=>$packages,'meta'=>$meta)); 
    }   

    public function add(Request $request)
    {
        $rules = array(
          'country_id' =>  'required',
          'name'          => 'required',
          'status'        => 'required',
          'plan_currency' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        $level;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
            if(isset($failedRules['country_id']))
            $errorMsg = $failedRules['country_id'][0] . "\n";
            if(isset($failedRules['name']))
            $errorMsg = $failedRules['name'][0] . "\n";
            if(isset($failedRules['status']))
            $errorMsg = $failedRules['status'][0] . "\n";
            if(isset($failedRules['plan_currency']))
            $errorMsg = $failedRules['plan_currency'][0] . "\n";
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else
        {
            \Stripe\Stripe::setApiKey("sk_test_SIc5YNIBhQ3hIfKW5e5XrD7X");
            if($request->id)
            {
                try 
                {
                    $plan_data = UserPackages::where('id',$request->id)->first();
                    if($plan_data->recurring_amount!=$request->recurring_amount)
                    {
                       return (json_encode(array('status'=>'error','message'=>'Amout should be fixed as previous for recurring plan.'))) ; 
                    }
                    else if($request->recurring_amount!='')
                    {
                        if($plan_data->plan_currency!=$request->plan_currency)
                        {
                           return (json_encode(array('status'=>'error','message'=>'Currency should be fixed as previous for recurring plan.'))) ; 
                        }
                    }
                    
                    $request_countries      = $request->country_id;
                    $exist_countries_data   = DB::table('package_country')->select(\DB::raw('group_concat(country_id) as country_ids'))->where('package_id',$request->id)->groupby('package_id')->first();
                    $exist_countries = explode(",",$exist_countries_data->country_ids);
                    $result_countries = array_diff($request_countries,$exist_countries);
                    $deleted_countries = array_diff($exist_countries,$request_countries);
                    if(!empty($deleted_countries))
                    {
                        foreach($deleted_countries as $country_data)
                        {
                            DB::table('package_country')->where(['package_id'=>$request->id,'country_id'=>$country_data])->delete();
                        }
                    }
                    if(!empty($result_countries))
                    {
                        foreach($result_countries as $country_data)
                        {
                            $data[] = array(
                                'package_id' => $request->id,
                                'country_id' => $country_data
                                );
                        }
                        DB::table('package_country')->insert($data);  
                    }
                    $data = array(
                      'name'                => $request->name,
                      'status'              => $request->status,
                      'currency'            => $request->plan_currency,
                      'recurring_amount'    => $request->recurring_amount,
                      'recurring_points'    => $request->recurring_points,
                      'one_time_amount'     => $request->one_time_amount,
                      'one_time_points'    => $request->one_time_points,
                    );
                    if($request->recurring_amount!='')
                    {
                        $p = \Stripe\Plan::retrieve($plan_data->stripe_plan_id);
                        $p->nickname = $request->name;
                        //$p->amount = $request->amount.'00';
                        $p->save();
                    }
                    UserPackages::where('id',$request->id)->update($data);
                    return (json_encode(array('status'=>'success','message'=>'Package '.$request->name.' Updated Successfully')));
                
                }
                catch(\Illuminate\Database\QueryException $ex)
                { 
                $result = $ex->getMessage();
                return (json_encode(array('status'=>'error','message'=>$result))) ;
                }
            }
            else
            {
                try 
                {
                    $package            = new UserPackages();
                    $package->name      = $request->name;
                    $package->status    = $request->status;
                    $country_id         = $request->country_id;
                    $recurring          = '0';
                    if($request->recurring_amount=='' && $request->one_time_amount=="")
                    {
                        return (json_encode(array('status'=>'error','message'=>'Please select at least one plan type.'))) ;  
                    }
                    $package->recurring_amount  = $request->recurring_amount;
                    $package->recurring_points  = $request->recurring_points;
                    $package->one_time_amount   = $request->one_time_amount;
                    $package->one_time_points   = $request->one_time_points;
                    $package->currency          = $request->plan_currency;
                    /*print_r($package); die;*/
                    if($request->recurring_amount!='')
                    {
                       $plan = \Stripe\Plan::create([
                            'currency' => $request->plan_currency,
                            'interval' => 'month',
                            'product' => 'prod_DfYL6wCkPC7f1p',
                            'nickname' => $request->name,
                            'amount' => $request->recurring_amount.'00',
                        ]); 
                        if(!$plan)
                        {
                            return (json_encode(array('status'=>'error','message'=>'Recurring Plan not created please try again.'))) ;  
                        }
                        $recurring = "1";
                    }
                    
                    if($package->save())
                    {
                        foreach($country_id as $country_data)
                        {
                            $package_data[] = array(
                                'package_id' => $package->id,
                                'country_id' => $country_data
                                );
                        }
                        DB::table('package_country')->insert($package_data);
                        if($recurring=='1')
                        {
                            $plan_data = array(
                              "stripe_plan_id" => $plan['id']
                            );
                            UserPackages::where('id',$package->id)->update($plan_data);
                        }
                        return (json_encode(array('status'=>'success','message'=>'Package '.$request->name.' Inserted Successfully')));
                    }
                }
              catch(\Illuminate\Database\QueryException $ex)
              { 
                $result = $ex->getMessage();
                return (json_encode(array('status'=>'error','message'=>$result))) ;
              }
            }
        }
    }

    public function getPackageDetails(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = UserPackages::select('user_packages.id as id','user_packages.name','user_packages.currency','user_packages.recurring_amount','user_packages.one_time_amount','user_packages.one_time_points','user_packages.status','user_packages.recurring_points',\DB::raw('group_concat(dg_pc.country_id) as country_ids'))->join('package_country as pc','pc.package_id','=','user_packages.id')->groupBy('user_packages.id')->find($request->id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }

    public function delete(Request $request)
    {
      if($request->ajax())
      {
        if($request->id){
          \Stripe\Stripe::setApiKey("sk_test_SIc5YNIBhQ3hIfKW5e5XrD7X");
          $result = UserPackages::find($request->id);
          $plan = \Stripe\Plan::retrieve($result->stripe_plan_id);
          if ($result->delete())
          {
              $plan->delete();
          }
            return (json_encode(array('status'=>'success','message'=>"User Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }
    }
}
