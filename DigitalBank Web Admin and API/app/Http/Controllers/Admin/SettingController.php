<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\UserPackages;
use App\Model\ApiRoutes;
use Response;
use DB;
use Validator;
//use Auth;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $setting = DB::table('settings')->where('user_id',$user->id)->first();
        $default_setting = array();
        if(!empty($setting))
        {
            $default_setting = json_decode($setting->settings_json,true);
            //print_r($default_setting); die;
        }
        return view('admin.setting.index',compact(['default_setting']));
    }
    
    public function systemLogPage()
    {
        return view('admin.setting.log_event');
    }
    
    public function getAllEvents(Request $request)
    {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        $routes = new ApiRoutes();
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $routes = $routes->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'route_name','reward_points','event_logged');
        $total = $routes->count();
        $routes = $routes->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=>$routes,'meta'=>$meta));
    }
    
    public function saveSettings(Request $request)
    {
        try
        {
            $user = Auth::user();
            $data = array();
            $data['kid_setting']['refer_points'] = $request->refer_point_reward;
            $data['kid_setting']['limit_task'] = $request->limit_task;
            $data['kid_setting']['auto_assigned_pool_points'] = $request->auto_assigned_pool_points;
            $data['parent_setting']['min_account_balance'] = $request->min_account_balance;
            $result_data = json_encode($data);
            $setting_data = array(
                    'user_id' => $user->id,
                    'settings_json' => $result_data,
                    'last_updated_by' => $user->id
            );
            if(!DB::table('settings')->where('user_id',$user->id)->exists())
            {
                DB::table('settings')->insert($setting_data);
            }
            else
            {
                DB::table('settings')->where('user_id',$user->id)->update($setting_data);
            }
            return (json_encode(array('status'=>'success','message'=>'Setting succesfully saved'))) ;
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
        }

    }

}
