<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Response;
use DB;
use App\Model\Category;
use App\Model\Task;
use Illuminate\Support\Facades\Auth;
use Validator;
//use Auth;

class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = \DB::table('countries')->select('id','country_name')->get()->toArray();
        return view('admin.categories.index',compact(['countries']));
    }

    public function getAllCategories(Request $request)
    {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $country_id=(isset($request->datatable['query']['country_id']) && ($request->datatable['query']['country_id']!="") ? $request->datatable['query']['country_id'] : '');
        $type=(isset($request->datatable['query']['type']) && ($request->datatable['query']['type']!="") ? $request->datatable['query']['type'] : '');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        $categories = new category();
        
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $categories = $categories->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'category.id as id','category.category_name','category.status','category.type','category.no_of_tasks',\DB::raw('group_concat(dg_cc.country_id) as country_ids'),\DB::raw('group_concat(dg_co.country_name) as country_name'));
        $total = $categories->count();
        $categories = $categories->join('country_category as cc','cc.category_id','=','category.id')->join('countries as co','co.id','=','cc.country_id');
        if($country_id !='')
        {
          $categories = $categories->where('cc.country_id',$country_id);
          $total = $categories->count();
        }
        if($type !='')
        {
          $categories = $categories->where('category.type',$type);
          $total = $categories->count();
        }
        $categories = $categories->groupBy('category.id')->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=>$categories,'meta'=>$meta));
    }

    public function getCategoryTasks($id)
    {
        $data = Category::select('id','category_name','status')->where('id',$id)->first();
        return view('admin.categories.tasks',compact('data'));  
    }

    public function addPoints(Request $request)
    {
      $status = "success";
      try {
        $points = $request->points;
        $category_id = $request->category_id;
        //print_r($category_id);
        DB::table('task')->whereIn('category_id',$category_id)->increment('task_points', $points);
        $result = "Point value added to All Tasks of selected Categories";
      }
      catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result )));
    }

    public function getAllCategoriesTasks(Request $request,$id)
    {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        $tasks = new task();
        $total = $tasks->where('task.category_id',$id)->count();
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $tasks = $tasks->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'task.id as id','task.task_name','category.category_name','task.question_type','task.task_points','task.file_attached','task.status')->join('category', 'category.id', '=', 'task.category_id')->where('task.category_id',$id)->offset($offset)->limit($perpage)->get();
        $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=>$tasks,'meta'=>$meta)); 
    }

    public function categoryDetails(Request $request)
    {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = Category::select('category.id as id','category.category_name','category.status','category.type','category.no_of_tasks',\DB::raw('group_concat(dg_cc.country_id) as country_ids'))->join('country_category as cc','cc.category_id','=','category.id')->groupBy('category.id')->find($request->id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;  
    }
    public function update(Request $request)
    {
        $rules = array(
          'category_name'        => 'required',
          'type'                 => 'required|numeric',
          'country_id'           => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        $level;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['category_name']))
            $errorMsg = $failedRules['category_name'][0] . "\n";
            if(isset($failedRules['type']))
            $errorMsg = $failedRules['type'][0] . "\n";
            if(isset($failedRules['country_id']))
            $errorMsg = $failedRules['country_id'][0] . "\n";
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
        }
        else
        {
            $status = "success";
            try {
                $request_countries      = $request->country_id;
                $exist_countries_data   = DB::table('country_category')->select(\DB::raw('group_concat(country_id) as country_ids'))->where('category_id',$request->id)->groupby('category_id')->first();
                $exist_countries = explode(",",$exist_countries_data->country_ids);
                $result_countries = array_diff($request_countries,$exist_countries);
                $deleted_countries = array_diff($exist_countries,$request_countries);
                if(!empty($deleted_countries))
                {
                    foreach($deleted_countries as $country_data)
                    {
                        DB::table('country_category')->where(['category_id'=>$request->id,'country_id'=>$country_data])->delete();
                    }
                }
                if(!empty($result_countries))
                {
                    foreach($result_countries as $country_data)
                    {
                        $data[] = array(
                            'category_id' => $request->id,
                            'country_id' => $country_data
                            );
                    }
                    DB::table('country_category')->insert($data);  
                }
                $data = array(
                'id' => $request->id,
                'category_name' => $request->category_name,
                'type' => $request->type,
                'no_of_tasks'=>$request->no_of_tasks,
                );
                Category::where('id',$request->id)->update($data);
                $result = "Category Updated Successfully";
            }
            catch(QueryException $ex){ 
              dd($ex->getMessage());
              $status = "error";
              $result = $ex->getMessage();
            }
            return (json_encode(array('status'=>$status,'message'=>$result )));
        }
    }

    public function changeStatus(Request $request)
    {
        $status = "success";
        $category_status = ($request->status == '1') ? '2' : '1';
        try {
            Category::where('id',$request->category_id)->update(['status'=>$category_status]);
            $result = "Category status updated successfully";
        }
        catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))); 
    }

    public function add(Request $request)
    {
        $rules = array(
          'category_name'        => 'required',
          'type'                 => 'required|numeric',
          'country_id'           => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        $level;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['category_name']))
            $errorMsg = $failedRules['category_name'][0] . "\n";
            if(isset($failedRules['type']))
            $errorMsg = $failedRules['type'][0] . "\n";
            if(isset($failedRules['country_id']))
            $errorMsg = $failedRules['country_id'][0] . "\n";
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
        }
        else
        {
            //print_r($request->all()); die;
            $status = "success";
            $category = new Category();
            try {
                //$category->category_name = $request->category_name;
                $category->category_name = $request->category_name;
                $category->type = $request->type;
                $category->no_of_tasks = $request->no_of_tasks;
                $country_id = $request->country_id;
                if($category->save())
                {
                    foreach($country_id as $country_data)
                    {
                        $data[] = array(
                            'category_id' => $category->id,
                            'country_id' => $country_data
                            );
                    }
                    DB::table('country_category')->insert($data);
                    $result = "Category Inserted Successfully";
                    return (json_encode(array('status'=>$status,'message'=>$result ))); 
                }
                
            }
            catch(QueryException $ex){ 
              //dd($ex->getMessage());
              $status = "error";
              $result = $ex->getMessage();
              return (json_encode(array('status'=>$status,'message'=>$result )));
            }
            
        }
    }
    public function delete(Request $request)
    {
      $status = "success";
        try {
            if(Task::where('category_id',$request->id)->exists())
            {
              $tasks_id = \DB::table('task')->select('id')->where('category_id',$request->id)->get()->toArray();
              foreach ($tasks_id as $key => $value) {
                \DB::table('option')->where('task_id',$value->id)->delete();
              }
              \DB::table('task')->where('category_id',$request->id)->delete();
            }
            Category::where('id',$request->id)->delete($request->id);
            $result = "Category Deleted Successfully";
        }
        catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result )));
    }
}
