<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Task;
use App\Model\Category;
use App\Model\User;
use App\Model\Option;
use App\Model\Account;
use App\Model\bufferLog;
use App\Model\UserNotification;
use App\Model\UserTaskResult;
use App\Model\UserTaskData;
use Validator;
use DB;
use Response;
use Illuminate\Database\QueryException;


class TaskController extends Controller
{

    public function __construct()
    {
        //$this->middleware('guest');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }      
            if($current_page=='1')
            {
              $offset = 0;
            }
            else
            {
              $offset = ($current_page-1)*$perpage;
            }
            $user= Auth::user();
            $child_id = "";
            $task = new task();
            $task = $task->select('task.id as id','task.task_name as question','category_name','question_type',\DB::raw('group_concat(dg_option.id) as options_id'),\DB::raw('group_concat(options) as options_value'))
                    ->join('category', 'category.id', '=', 'task.category_id')
                    ->join('country_category as cc','cc.category_id','=','category.id')->join('countries as co','co.id','=','cc.country_id')
                    ->join('option', 'option.task_id', '=', 'task.id');
            if($user->type=='3')
            {
                if($request->child_id=='')
                {
                   return Response::json(array('status'=> 'error','message'=>'Child id required.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                else if(!User::where(['id'=>$request->child_id,'type'=>'2'])->exists())
                {
                    return Response::json(array('status'=> 'error','message'=>'Child Id not exist.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                else if(!DB::table('tag_parent')->where(['user_id'=>$request->child_id,'parent_id'=>$user->id])->exists())
                {
                    return Response::json(array('status'=> 'error','message'=>'Child not tagged to you.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                $child_id = $request->child_id;
                $country_id = User::select('country_id')->where('id',$child_id)->first();
                $task = $task->where('cc.country_id',$country_id->country_id);
                //$total = $task->count();
            }
            else
            {
                $task = $task->where('cc.country_id',$user->country_id);
                //$total = $task->count();
            }
            $task = $task->groupBy('option.task_id')->groupBy('category.id');
            $task = $task->offset($offset)->limit($perpage)->get();
            $total = count($task);
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            $task = $task->map(function ($value) use($user,$child_id) {
                if($value->options_id!='')
                {$id = explode(',', $value->options_id);}
                $data['id'] = $value->id;
                $data['category_name'] = $value->category_name;
                $data['question'] = $value->question;
                $data['question_type'] = $value->question_type;
                $data['task_boosted'] = '0';
                $data['boosted_value'] = '0';
                if($user->type=='3')
                {
                   if(\DB::table('user_task_data')->where('bonus_point','!=','0')->where(['task_id'=>$value->id,'parent_id'=>$user->id,'user_id'=>$child_id])->exists())
                    {
                        $boost = \DB::table('user_task_data')->where('bonus_point','!=','0')->where(['task_id'=>$value->id,'parent_id'=>$user->id,'user_id'=>$child_id])->first();
                        $data['boosted_value'] = $boost->bonus_point;
                        $data['task_boosted'] = '1';
                    }
                }
                if($value->options_value!=''){
                $value = explode(',', $value->options_value);
                $data['options'] = [array_combine($id, $value)];
                }
                else
                {
                    $data['options'] = [];
                }
                return $data; 
            });
            $task->all();
            if($total>0)
            {
                
                return Response::json(array('status'=> 'success','message'=>'Task Details','total_task'=>$total,'meta'=>$meta,'data'=>$task ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No Data Found','total_task'=>$total,'meta'=>$meta,'data'=>$task ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
           
    }

    public function getTaskDetails($id)
    {
        
        try
        {
            $account_type = $_ENV['ACCOUNT_TYPE'];
            $user = Auth::user();
            if(\DB::table('task')->where('id',$id)->count()<1)
            {
              return Response::json(array('status'=> 'error','message'=>'Task not exist'))->withHeaders([
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
             ]);  
            }
            $task = \DB::table('task')->select('task.id as id','task.task_name as question','category.category_name','task.category_id','task.file_attached','task.link_url','task.skipped_time','task.task_points','task.attempt_1','task.attempt_2','task.attempt_3','task.above_3','task.question_type',\DB::raw('group_concat(dg_option.id) as options_id'),\DB::raw('group_concat(options) as options_value'),\DB::raw('group_concat(correct_options) as correct_answer'))->join('category', 'category.id', '=', 'task.category_id')->join('country_category as cc','cc.category_id','=','category.id')->join('option', 'option.task_id', '=', 'task.id')->where('category.status','1')->where('task.id',$id)->where('cc.country_id',$user->country_id)->groupBy('option.task_id')->get();
            if(count($task)<1)
            {
              return Response::json(array('status'=> 'error','message'=>'No data found.'))->withHeaders([
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
             ]);  
            }
            if($user->type==2)
            {
                $category_data = Category::select('type')->where('id',$task[0]->category_id)->first();
                if($category_data->type=='1')
                {
                    $required_category = Category::select('id')->where('type','2')->get()->toArray();
                    if(!empty($required_category))
                    {
                        $category_ids = array_column($required_category,'id');
                    }
                    $count = count($required_category);
                    if(!empty($category_ids))
                    {
                        $check = \DB::table('category_flag')->whereIn('category_id',$category_ids)->where('user_id',$user->id)->count();
                        if($check!=$count)
                        {
                           $elective_category = '2'; 
                        }
                        else
                        {
                           $flag_check = \DB::table('category_flag')->whereIn('category_id',$category_ids)->where(['user_id'=>$user->id,'min_threshold_acheived'=>'2'])->count();
                           if($flag_check=='0')
                           {
                               $elective_category = '1'; 
                           }
                           else
                           {
                              $elective_category = '2';  
                           }
                        }
                    }
                    if($elective_category!='1')
                    {
                        return Response::json(array('status'=> 'error','message'=>'Please complete mandatory category first then proceed further.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                    }
                }
                if(\DB::table('user_task_data')->where(['task_id'=>$id,'user_id'=>$user->id,'parent_id'=>$user->parent_id])->where('is_final_submitted','2')->exists())
                {
                    return Response::json(array('status'=> 'error','message'=>'Already answered correctly'))->withHeaders([
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                 ]);  
                }
                $total_attempt = '0';
                if(UserTaskData::where(['task_id'=>$id,'user_id'=>$user->id,'parent_id'=>$user->parent_id])->exists())
                {
                    $bonus_data = UserTaskData::select('bonus_point','total_attempt')->where(['task_id'=>$id,'user_id'=>$user->id,'parent_id'=>$user->parent_id])->first();
                    $bonus_point = (!empty($bonus_data) ? $bonus_data->bonus_point : 0);
                }
                else
                {
                    $bonus_data = DB::table('category_boosts')->select('boosted_by')->where(['category_id'=>$task[0]->category_id,'parent_id'=>$user->parent_id,'child_id'=>$user->id])->first();
                    $bonus_point = 0;
                    if(!empty($bonus_data))
                    {
                        $bonus_point = $task[0]->task_points*$bonus_data->boosted_by - $task[0]->task_points;
                    }
                }
                
                $total_points = $task[0]->task_points + $bonus_point;
                if(!empty($bonus_data->total_attempt)==1)
                {
                    $total_attempt = $bonus_data->total_attempt;
                    $total_points = $task[0]->attempt_2 + $bonus_point;
                }
                if(!empty($bonus_data->total_attempt)==2)
                {
                    $total_attempt = $bonus_data->total_attempt;
                    $total_points = $task[0]->attempt_3 + $bonus_point;
                }
                elseif(!empty($bonus_data->total_attempt)==3)
                {
                    $total_attempt = $bonus_data->total_attempt;
                    $total_points = $task[0]->above_3 + $bonus_point;
                }
                elseif(!empty($bonus_data->total_attempt)>3)
                {
                    $total_attempt = $bonus_data->total_attempt;
                    $total_points = $task[0]->above_3 + $bonus_point;
                }
                //return $total_points;
                //$buffer_points = DB::table('buffer_logs')->select(DB::raw('SUM(transaction_points) as total_buffer_points'))->where('from_id',$user->parent_id)->first();
                //$total_buffer_points = (!empty($buffer_points) ? $buffer_points->total_buffer_points : 0);
               
               //select(DB::raw('count(*) as user_count, status'))
               /************************Changes for Pool Balance now all balance deducted from Kid pool balance*****************/
                /*$current_points = DB::select(DB::raw("Select get_balance(".$user->parent_id.",1 ) AS bal"));*/
                $current_points = DB::select(DB::raw("Select get_balance(".$user->id.",4) AS bal"));
                $current_point=$current_points[0]->bal;
                //return $current_point;
                if($current_point >= $total_points)
                {
                    $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = $user->id AND dg_account.account_type = 4
                        ) as pool_account"),DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = $user->id AND dg_account.account_type = 1
                        ) as current_account"))->where('user_id',$user->id)->first();
                        //return $user_data;
                    DB::table('buffer_logs')->where(['to_id'=>$user_data->current_account,'from_id'=>$user_data->pool_account])->where('task_id','!=','0')->delete();
                    if(!bufferLog::where(['from_id'=>$user_data->pool_account,'to_id'=>$user_data->current_account,'task_id'=>$id])->exists())
                    {
                        $data = array(
                            'from_id'            => $user_data->pool_account,
                            'to_id'              => $user_data->current_account,
                            'task_id'            => $id,
                            'transaction_points' => $total_points,
                            'remarks'            => 'Child '.$user->name.' attempt question',
                            'transaction_type'   => '1',
                        );
                        //return $data;
                        DB::table('buffer_logs')->insert($data);
                    }
                    $task = $task->map(function ($value) use ($total_points,$total_attempt) 
                    {
                        if($value->options_id!='')
                        {$id = explode(',', $value->options_id);}
                        $data['id'] = $value->id;
                        $data['category_name'] = $value->category_name;
                        $data['question'] = $value->question;
                        $data['correct_answer'] = $value->correct_answer;
                        $data['question_type'] = $value->question_type;
                        $data['file_attached'] = '';
                        if($value->file_attached!=null)
                        {
                            $data['file_attached'] = $value->file_attached;
                        }
                        $data['link_url'] = '';
                        if($value->link_url!=null)
                        {
                            $data['link_url'] = $value->link_url;
                            $data['file_attached'] = '';
                        }
                        $data['base_url'] = "https://quizme.com.my/digital_bank/storage/app/tasks/";
                        $data['skipped_time'] = $value->skipped_time;
                        if($value->question_type=='2')
                        {
                            $task = \DB::table('option')->select(\DB::raw('group_concat(dg_option.options) as correct_options'))->where('option.task_id',$value->id)->where('correct_options','1')->groupBy('option.task_id')->get()->toArray();
                            $data['correct_answer'] = $task[0]->correct_options;
                        }
                        $data['total_attempt'] = $total_attempt;
                        $data['task_points'] = $value->task_points;
                        $data['earn_points'] = $total_points;
                        if($value->options_value!=''){
                        $value = explode(',', $value->options_value);
                            $data['options'] = array_map(function($key,$lable){
                                return array("label"=>$lable,"value"=>$key);
                            },$id, $value);
                        }
                        else
                        {
                            $data['options'] = [];
                        }
                        return $data; 
                    });
                    $task->all();
                    if(!empty($task))
                    {
                        return Response::json(array('status'=> 'success','message'=>'Task Data','data'=>$task))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                    }
                    else
                    {
                        return Response::json(array('status'=> 'success','message'=>'No Data Found','data'=>$task))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                    } 
                }
                else
                {
                    return Response::json(array('status'=> 'error','message'=>'You dont have enough amount to attempt task.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
            }
            else
            {
                $task = \DB::table('task')->select('task.id as id','task.task_name as question','category_name','task.task_points','question_type')->join('category', 'category.id', '=', 'task.category_id')->where('task.status','1')->where('task.id',$id)->get();
                if(count($task)>0)
                {
                    return Response::json(array('status'=> 'success','message'=>'Task Data','data'=>$task))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                else
                {
                    return Response::json(array('status'=> 'success','message'=>'No Data Found','data'=>$task))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                } 
            } 

        }
        catch(QueryException $ex){
        if($ex->errorInfo[0] == "45000" && $ex->errorInfo[1] == "1644") {
                return Response::json(array('status'=> 'error','message'=>$ex->errorInfo[2]))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            } 
            return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function searchTask(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'task_name'      => 'required',
        'child_id'       => 'required|numeric'
        ]);
        if ($validator->fails()) {
            $errorMsg = $validator->getMessageBag()->toArray();
            return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        else
        { 
            try
            { 
                $user = Auth::user();
                if(!User::where(['id'=>$request->child_id,'type'=>'2','parent_id'=>$user->id])->exists())
                {
                  return Response::json(array('status'=>'error','message'=>'Child not tagged with you.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                }
                $child_id = $request->child_id;
                $country_id = User::select('country_id')->where('id',$child_id)->first();
                $task = Task::select('task.id','task.task_name')->join('category', 'category.id', '=', 'task.category_id')->join('country_category as cc','cc.category_id','=','category.id')->where('task.task_name', 'like', $request->task_name.'%')->where('task.status','1')->where('cc.country_id',$country_id->country_id)->get();
                $task = $task->map(function ($value) use ($user,$child_id) {
                    $data['id'] = $value->id;
                    $data['task_name'] = $value->task_name;
                    $data['task_boosted'] = '0';
                    $data['boosted_value'] = '0';
                    if(\DB::table('user_task_data')->where('bonus_point','!=','0')->where(['task_id'=>$value->id,'parent_id'=>$user->id,'user_id'=>$child_id])->exists())
                    {
                        $boost = \DB::table('user_task_data')->where('bonus_point','!=','0')->where(['task_id'=>$value->id,'parent_id'=>$user->id,'user_id'=>$child_id])->first();
                        $data['boosted_value'] = $boost->bonus_point;
                        $data['task_boosted'] = '1';
                    }
                return $data;
                });
                $task->all();
                if(count($task)>0)
                {
                  return Response::json(array('status'=>'success','message'=>'Task Details','data'=>$task))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);   
                }
                return Response::json(array('status'=>'error','message'=>'No Task found','data'=>$task))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            } 
            catch(QueryException $ex){ 
              return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
        }
    }

    public function submit_task(Request $request,$id)
    {
        try
        {
            $user = Auth::user();
            if(!DB::table('task')->where('id',$id)->exists())
            {
                return Response::json(array('status'=> 'error','message'=>'Incorrect task id please try again')) ; 
            }
            if(UserTaskData::where(['user_id'=>$user->id,'task_id'=>$id,'is_final_submitted'=>'2'])->exists())
            {
               return Response::json(array('status'=> 'error','message'=>'You already attempt this task'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }

            
                $task_data = Task::select('task.question_type','task.category_id','task.task_points','task.attempt_1','task.attempt_2','task.attempt_3','task.above_3','category.id as category_id','category.type','category.no_of_tasks')->join('category','category.id','=','task.category_id')->where('task.id',$id)->first();
                if($task_data->type=='1')
                {
                    $required_category = Category::select('id')->where('type','2')->get()->toArray();
                    if(!empty($required_category))
                    {
                        $category_ids = array_column($required_category,'id');
                    }
                    $count = count($required_category);
                    if(!empty($category_ids))
                    {
                        $check = \DB::table('category_flag')->whereIn('category_id',$category_ids)->where('user_id',$user->id)->count();
                        if($check!=$count)
                        {
                           $elective_category = '2'; 
                        }
                        else
                        {
                           $flag_check = \DB::table('category_flag')->whereIn('category_id',$category_ids)->where(['user_id'=>$user->id,'min_threshold_acheived'=>'2'])->count();
                           if($flag_check=='0')
                           {
                               $elective_category = '1'; 
                           }
                           else
                           {
                              $elective_category = '2';  
                           }
                        }
                    }
                    if($elective_category!='1')
                    {
                        return Response::json(array('status'=> 'error','message'=>'Please attempt mandatory category first then proceed further.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                    }
                }
                //return $task_data;
                $points = Account::select('current_point')->where('user_id',$user->parent_id)->where('account_type','1')->first();
                $attempt = 0;
                if(UserTaskData::where(['task_id'=>$id,'user_id'=>$user->id,'parent_id'=>$user->parent_id])->exists())
                {
                    $bonus_data = UserTaskData::select('bonus_point','total_attempt')->where(['task_id'=>$id,'user_id'=>$user->id,'parent_id'=>$user->parent_id])->first();
                    $bonus_point = (!empty($bonus_data) ? $bonus_data->bonus_point : 0);
                    $attempt = (!empty($bonus_data) ? $bonus_data->total_attempt : 0);
                }
                else
                {
                    $bonus_data = DB::table('category_boosts')->select('boosted_by')->where(['category_id'=>$task_data->category_id,'parent_id'=>$user->parent_id,'child_id'=>$user->id])->first();
                    $bonus_point = 0;
                    if(!empty($bonus_data))
                    {
                        $bonus_point = $task_data->task_points*$bonus_data->boosted_by - $task_data->task_points;
                    }
                }
                
                $total_points = $task_data->task_points + $bonus_point;
                if(!empty($bonus_data->total_attempt) && $bonus_data->total_attempt==2)
                {
                    $total_points = $task_data->attempt_2 + $bonus_point;
                }
                elseif(!empty($bonus_data->total_attempt) && $bonus_data->total_attempt ==3)
                {
                    $total_points = $task_data->attempt_3 + $bonus_point;
                }
                elseif(!empty($bonus_data->total_attempt) && $bonus_data->total_attempt>3)
                {
                    $total_points = $task_data->above_3 + $bonus_point;
                }
                //return $total_points;
                //die;
                // $buffer_points = DB::table('buffer_logs')->select(DB::raw('SUM(transaction_points) as total_buffer_points'))->where('from_id',$user->parent_id)->first();
                // $total_buffer_points = (!empty($buffer_points) ? $buffer_points->total_buffer_points : 0);
                //$current_point = $points->current_point - $total_buffer_points;
                $current_points = DB::select(DB::raw("Select get_balance(".$user->id.",4) AS bal"));
                $current_point=$current_points[0]->bal;
                $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = $user->id AND dg_account.account_type = 4
                        ) as pool_account"),DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = $user->id AND dg_account.account_type = 1
                        ) as current_account"))->where('user_id',$user->id)->first();
                if($current_point >= $total_points)
                {
                    if(!bufferLog::where(['from_id'=>$user_data->pool_account,'to_id'=>$user_data->current_account,'task_id'=>$id])->exists())
                    {
                        $data = array(
                            'from_id'            => $user_data->pool_account,
                            'to_id'              => $user_data->current_account,
                            'task_id'            => $id,
                            'transaction_points' => $total_points,
                            'remarks'            => 'Child '.$user->name.' attempt question',
                            'transaction_type'   => '1',
                        );
                        //return $data;
                        DB::table('buffer_logs')->insert($data);
                    }  
                }
                else
                {
                   return Response::json(array('status'=> 'error','message'=>'Your parent dont have enough balance to proceed further.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                $account_type = $_ENV['ACCOUNT_TYPE'];
                //$question = Task::select('question_type')->where('id',$id)->first();
                $question_type = $task_data->question_type;
                $task_id = $id;
                if($question_type==1)
                {
                    $answer = explode(',', $request->answer);
                    $answ = array();
                    $correct_answer = \DB::table('option')->select('correct_options')->where('task_id',$task_id)->get();
                    foreach ($correct_answer as $ans) {
                        array_push($answ,$ans->correct_options); 
                    }
                    $answ = array_map('strtolower', $answ);
                    $answer = array_map('strtolower', $answer);

                    //array_map('strtolower', $arr1)
                    if($answer == $answ){
                        $check_ans = 2;
                        $attempt+=1;
                    }else{
                        $check_ans = 1;
                        $attempt+=1;
                    }
                    //return $check_ans;
                    $result                     = new UserTaskData;
                    $result->user_id            = $user->id;
                    $result->task_id            = $task_id;
                    $result->parent_id          = $user->parent_id;
                    $result->bonus_point         = $bonus_point;
                    $result->is_final_submitted = $check_ans;
                    $result->total_attempt     = $attempt;
                    $check=UserTaskData::where(['user_id'=>$user->id,'task_id'=>$task_id])->exists();
                    if(!$check){
                        $result->save();
                    }else{
                        $data = array(
                            'is_final_submitted' => $check_ans,
                            'total_attempt'     => $attempt,
                        );
                        UserTaskData::where(['user_id'=>$user->id,'task_id'=>$task_id])->update($data);
                    }
                }
                if($question_type==2)
                {
                    $given_answer = '';
                    if($request->has('answer'))
                    {
                        $given_answer = $request->answer;
                    }
                    if($given_answer!='')
                    {
                        $given_answer = array_filter(explode(',', $given_answer),function($arr){
                                    return is_numeric($arr);
                            });
                        if(empty($given_answer))
                        {
                            return Response::json(array('status'=> 'error','message'=>'Please answer correct as per doc'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                        }
                        $correct_answ = array();
                        $given_answ = array();
                        $correct_answer = \DB::table('option')->select('correct_options','options')->where(array('correct_options'=>1,'task_id'=>$task_id))->get();
                        $answer = \DB::table('option')->select('options')->whereIn('id',$given_answer)->get();
                        foreach ($correct_answer as $ans) {
                            array_push($correct_answ,$ans->options); 
                        }
                        foreach ($answer as $ans) {
                            array_push($given_answ,$ans->options); 
                        }
                        if(array_udiff($given_answ, $correct_answ, 'strcasecmp')){
                            $check_ans = 1;
                            $attempt+=1;
                        }else{
                            $check_ans = 2;
                            $attempt+=1;
                        }
                    }
                    else{
                        $check_ans = 1;
                        $attempt+=1;  
                    }
                    //return $given_answ;
                    //$given_answer = implode(',', $given_answ);
                    $result = new UserTaskData;
                    $result->user_id            = $user->id;
                    $result->task_id            = $task_id;
                    $result->parent_id          = $user->parent_id;
                    $result->bonus_point         = $bonus_point;
                    $result->is_final_submitted = $check_ans;
                    $result->total_attempt     = $attempt;

                    $check=UserTaskData::where(['user_id'=>$user->id,'task_id'=>$task_id])->exists();
                    if(!$check){
                        $result->save();
                    }else{
                        $data = array(
                            'is_final_submitted' => $check_ans,
                            'total_attempt'     => $attempt,
                        );
                        UserTaskData::where(['user_id'=>$user->id,'task_id'=>$task_id])->update($data);
                    }
                }
                if($question_type==3)
                {
                    $given_answer = '';
                    if($request->has('answer'))
                    {
                        $given_answer = $request->answer;
                    }
                    if($given_answer!='')
                    {
                        $answer = array_filter(explode(',', $request->answer),function($arr){
                                    return is_numeric($arr);
                            });
                        if(empty($answer))
                        {
                            return Response::json(array('status'=> 'error','message'=>'Please answer correct as per doc'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                        }
                        $options = new option();
                        $answ = array();
                        $given_answ = array();
                        $correct_answer = \DB::table('option')->select('id','correct_options')->where('task_id',$task_id)->get();
                        foreach ($correct_answer as $ans=>$value)
                        {
                            array_push($answ,$value->correct_options);
                        }
                        foreach ($answer as $ans=>$value)
                        {
                            $given_answer = \DB::table('option')->select('options')->where('id',$value)->first();
                            array_push($given_answ,$given_answer->options);
                        }
                        $answ = array_map('strtolower', $answ);
                        $given_answ = array_map('strtolower', $given_answ);
                        //return $given_answ;
                        if($given_answ == $answ){
                            $check_ans = 2;
                            $attempt+=1;
                        }else{
                            $check_ans = 1;
                            $attempt+=1;
                        }
                    }
                    else
                    {
                        $check_ans = 1;
                        $attempt+=1;  
                    }
                    //return $check_ans;
                    //$given_answer = implode(',', $answer);
                    $result = new UserTaskData;
                    $result->user_id            = $user->id;
                    $result->task_id            = $task_id;
                    $result->parent_id          = $user->parent_id;
                    $result->bonus_point         = $bonus_point;
                    $result->is_final_submitted = $check_ans;
                    $result->total_attempt     = $attempt;

                    $check=UserTaskData::where(['user_id'=>$user->id,'task_id'=>$task_id])->exists();
                    if(!$check){
                        $result->save();
                    }else{
                        $data = array(
                            'is_final_submitted' => $check_ans,
                            'total_attempt'     => $attempt,
                        );
                        UserTaskData::where(['user_id'=>$user->id,'task_id'=>$task_id])->update($data);
                    }
                }
                if($check_ans=='2')
                {
                    $buffer_data = new bufferLog();
                    $delete_buffer_log = $buffer_data->where(['task_id'=>$task_id,'from_id'=>$user_data->pool_account,'to_id'=>$user_data->current_account])->delete();
                    $earn_points = $task_data->task_points;
                    if($attempt==2)
                    {
                        $earn_points = $task_data->attempt_2;
                    }
                    elseif($attempt==3)
                    {
                        $earn_points = $task_data->attempt_3;
                    }
                    elseif($attempt>2)
                    {
                        $earn_points = $task_data->above_3;
                    }
                    if(UserTaskData::where(['task_id'=>$id,'user_id'=>$user->id,'parent_id'=>$user->parent_id])->exists())
                    {
                        $bonus_data = UserTaskData::select('bonus_point')->where(['task_id'=>$id,'user_id'=>$user->id,'parent_id'=>$user->parent_id])->first();
                        $bonus_point = (!empty($bonus_data) ? $bonus_data->bonus_point : 0);
                    }
                    else
                    {
                        $bonus_data = DB::table('category_boosts')->select('boosted_by')->where(['category_id'=>$task_data->category_id,'parent_id'=>$user->parent_id,'child_id'=>$user->id])->first();
                        $bonus_point = 0;
                        if(!empty($bonus_data))
                        {
                            $bonus_point = $task_data->task_points*$bonus_data->boosted_by - $task_data->task_points;
                        }
                    }
                    //$bonus_data = UserTaskData::select('bonus_point','total_attempt')->where(['task_id'=>$id,'user_id'=>$user->id,'parent_id'=>$user->parent_id])->first();
                    //$bonus_point = (!empty($bonus_data) ? $bonus_data->bonus_point : 0);
                    $earn_points +=$bonus_point;
                    $data = array(
                        'user_id' => $user->id,
                        'from_id' => $user_data->pool_account,
                        'to_id'   => $user_data->current_account,
                        'type'=>'1',
                        'transaction_for_id'=>$id,
                        'account_type' => $account_type,
                        'transaction_points' => $earn_points,
                        'transaction_type'  => '1',
                        'remarks' => 'Earn Points After attempting task',
                    );
                    DB::table('transaction')->insert($data);
                    $data = array(
                        'user_id' => $user->id,
                        'from_id' => $user_data->pool_account,
                        'to_id'   => $user_data->current_account,
                        'type'=>'1',
                        'transaction_for_id'=>$id,
                        'account_type' => '4',
                        'transaction_points' => $earn_points,
                        'transaction_type'  => '2',
                        'remarks' => "Task Amount deduction from pool by $user->user_name",
                    );
                    DB::table('transaction')->insert($data);
                    if($task_data->type=='2')
                    {
                        $check = \DB::table('category_flag')->where(['user_id'=>$user->id,'category_id'=>$task_data->category_id])->get()->toArray();
                        if(empty($check))
                        {
                           $data = array(
                               'user_id' => $user->id,
                               'category_id' => $task_data->category_id,
                               'total_task_completed' => '1',
                            );
                            if($task_data->no_of_tasks=='1')
                            {
                               $data['min_threshold_acheived'] = '1'; 
                            }
                            else
                            {
                                $data['min_threshold_acheived'] = '2';
                            }
                            \DB::table('category_flag')->insert($data);
                        }
                        else
                        {
                            $total = $check[0]->total_task_completed+1;
                            if($task_data->no_of_tasks==$total)
                            {
                               $min_threshold_acheived = '1'; 
                            }
                            else
                            {
                                $min_threshold_acheived = '2';
                            }
                            $data = array(
                               'min_threshold_acheived' => $min_threshold_acheived,
                               'total_task_completed' => $total,
                            );
                            \DB::table('category_flag')->where(['user_id'=>$user->id,'category_id'=>$task_data->category_id])->update($data);
                            
                        }
                    }
                    // $account = Account::find($user->parent_id);
                    // $account->decrement('current_point', $earn_points);
                    return Response::json(array('status'=> 'success','message'=>'Correct Answer','earn_points'=>$earn_points))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                else
                {
                    $buffer_data = new bufferLog();
                    // $buffer_points = $buffer_data->select('transaction_points')->where(['task_id'=>$task_id,'from_id'=>$user->parent_id,'to_id'=>$user->id])->first();
                    // $account = Account::find($user->parent_id);
                    // $account->decrement('buffer_point', $buffer_points->transaction_points);
                    $delete_buffer_log = $buffer_data->where(['task_id'=>$task_id,'from_id'=>$user_data->pool_account,'to_id'=>$user_data->current_account])->delete();
                    if($attempt==1)
                    {
                       $updated_task_points = $task_data->attempt_2; 
                    }
                    if($attempt==2)
                    {
                        $updated_task_points = $task_data->attempt_3;
                    }
                    elseif($attempt==3)
                    {
                        $updated_task_points = $task_data->above_3;
                    }
                    elseif($attempt>3)
                    {
                        $updated_task_points = $task_data->above_3;
                    }
                    $data = array(
                        'updated_task_points' => $updated_task_points,
                        'total_attempt' => $attempt,
                        'earn_points' => '0',
                        );
                    return Response::json(array('status'=> 'error','message'=>'Incorrect Answer','data'=>[$data]))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
        }
        catch(QueryException $ex){
        if($ex->errorInfo[0] == "45000" && $ex->errorInfo[1] == "1644") {
                return Response::json(array('status'=> 'error','message'=>$ex->errorInfo[2]))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            } 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function addAppreciation($id,Request $request)
    {
        try
        {
            $add_value = '0';
            $user = Auth::user();
            if($user->type=='3')
            {
                $validator = Validator::make($request->all(), [
                'student_id' => 'required',
                ]);
                if ($validator->fails()) {
                    $errorMsg = $validator->getMessageBag()->toArray();
                    return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                else
                {
                    $check_parent = \DB::table('users')->where('parent_id',$user->id)->where('id',$request->student_id)->exists();
                    if(!$check_parent)
                    {
                      return Response::json(array('status'=> 'error','message'=>'You are not parent of this user.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                    }
                    $check = \DB::table('users')->where('id',$request->student_id)->exists();
                    if(!$check)
                    {
                      return Response::json(array('status'=> 'error','message'=>'child not exist please try again.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                    }
                    if(\DB::table('task')->where('id',$id)->count()<1)
                    {
                      return Response::json(array('status'=> 'error','message'=>'Task not exist'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                    }
                    $task_points = \DB::table('task')->select('task_points')->where('id',$id)->first();
                    if($request->boost_points)
                    {
                        if($request->boost_points>2 || $request->boost_points<1)
                        {
                          return Response::json(array('status'=> 'error','message'=>'Please enter boost by value as per mentioned in DOC'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);   
                        }
                        if(\DB::table('buffer_logs')->where(['task_id'=>$id,'to_id'=>$request->student_id,'from_id'=>$user->id])->exists())
                        {
                            return Response::json(array('status'=> 'error','message'=>'Child attempt that task right now so task not boosted.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            ]);   
                        }
                        if(UserTaskData::where(['task_id'=>$id,'user_id'=>$request->student_id,'parent_id'=>$user->id,'is_final_submitted'=>'2'])->exists())
                        {
                            return Response::json(array('status'=> 'error','message'=>'Child already answered correctly so task not boosted.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            ]);   
                        }

                            $boost_points = $request->boost_points*$task_points->task_points - $task_points->task_points;
                            $check = \DB::table('user_task_data')->where(['task_id'=>$id,'user_id'=>$request->student_id,'parent_id'=>$user->id])->exists();
                            if($check)
                            {
                                if($request->boost_points == '1')
                                {
                                   \DB::table('user_task_data')->where(['task_id'=>$id,'user_id'=>$request->student_id,'parent_id'=>$user->id])->update(['bonus_point'=>'0']);
                                   return Response::json(array('status'=> 'success','message'=>'Category unboosted successfully '))->withHeaders([
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                    ]);
                                }
                                \DB::table('user_task_data')->where(['task_id'=>$id,'user_id'=>$request->student_id,'parent_id'=>$user->id])->update(['bonus_point'=>$boost_points]);
                               return Response::json(array('status'=> 'success','message'=>'Bonus point updated on task'))->withHeaders([
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                ]); 
                            }
                            else
                            {
                                if($request->boost_points == '1')
                                {
                                  return Response::json(array('status'=> 'success','message'=>'Task boosted value is restored to '.$request->boost_points.'x .'))->withHeaders([
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                    ]); 
                                }
                                $user_task_data = array(
                                    'user_id' => $request->student_id,
                                    'parent_id' => $user->id,
                                    'task_id' => $id,
                                    'bonus_point' => $boost_points,
                                    'type' => '1',
                                );
                                $query = \DB::table('user_task_data')->insert($user_task_data);
                            
                                return Response::json(array('status'=> 'success','message'=>'Bonus point added on task'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                ]); 
                            }
                        }
                        // else
                        // {
                        //     return Response::json(array('status'=> 'error','message'=>'Not enough points in your accounts to add Bonus Points'));  
                        // }
                    else
                    {
                      return Response::json(array('status'=> 'error','message'=>'Please select Boost Value'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                    }
                }
            }
            else
            {
                return Response::json(array('status'=> 'error','message'=>'Only parent add Bonus point'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }
        //
    /**
     * Store a newly created resource in storage.
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
