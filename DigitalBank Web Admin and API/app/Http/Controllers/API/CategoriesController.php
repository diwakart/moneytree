<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Category;
use App\Model\Task;
use App\Model\Account;
use App\Model\User;
use App\Model\UserTaskData;
use Response;
use DB;
use Validator;
use Illuminate\Database\QueryException;

class CategoriesController extends Controller
{

    public function __construct()
    {
        //$this->middleware('guest');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }
            //return $offset;
            $user= Auth::user();
            $child_id = "";
            $category = new category();
            $category_ids = array();
            $elective_category = "1";
            $category = $category->select('category.id','category.category_name','category.type','category.no_of_tasks')->where('category.status','1')->join('country_category as cc','cc.category_id','=','category.id')->join('countries as co','co.id','=','cc.country_id');
            if($user->type=='3')
            {
                if($request->child_id=='')
                {
                   return Response::json(array('status'=> 'error','message'=>'Child id required.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                else if(!User::where(['id'=>$request->child_id,'type'=>'2'])->exists())
                {
                    return Response::json(array('status'=> 'error','message'=>'Child Id not exist.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                else if(!DB::table('tag_parent')->where(['user_id'=>$request->child_id,'parent_id'=>$user->id])->exists())
                {
                    return Response::json(array('status'=> 'error','message'=>'Child not tagged to you.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                $child_id = $request->child_id;
                $country_id = User::select('country_id')->where('id',$child_id)->first();
                $category = $category->where('cc.country_id',$country_id->country_id);
                $total = $category->count();
            }
            else
            {
                $required_category = Category::select('id')->where('type','2')->get()->toArray();
                if(!empty($required_category))
                {
                    $category_ids = array_column($required_category,'id');
                }
                $count = count($required_category);
                if(!empty($category_ids))
                {
                    $check = \DB::table('category_flag')->whereIn('category_id',$category_ids)->where('user_id',$user->id)->count();
                    if($check!=$count)
                    {
                       $elective_category = '2'; 
                    }
                    else
                    {
                       $flag_check = \DB::table('category_flag')->whereIn('category_id',$category_ids)->where(['user_id'=>$user->id,'min_threshold_acheived'=>'2'])->count();
                       if($flag_check=='0')
                       {
                           $elective_category = '1'; 
                       }
                       else
                       {
                          $elective_category = '2';  
                       }
                    }
                }
                $category = $category->where('cc.country_id',$user->country_id);
                $total = $category->count();
            }
            $category = $category->orderBy('type', 'desc')->groupBy('category.id')->offset($offset)->limit($perpage)->get();
            //return $elective_category;
            $category = $category->map(function ($value) use($user,$child_id,$elective_category) {
                $data['id'] = $value->id;
                $data['category_name'] = $value->category_name;
                $data['category_boosted'] = '0';
                $data['boosted_value'] = '0';
                $data['category_type'] = $value->type;
                $data['minimum_task_required'] = $value->no_of_tasks;
                $data['elective_category'] = 'not_allowed';
                if($elective_category=='1')
                {
                    $data['elective_category'] = 'allowed';
                }
                if($user->type=='3')
                {
                   if(\DB::table('category_boosts')->where('category_id',$value->id)->where(['parent_id'=>$user->id,'child_id'=>$child_id])->exists())
                    {
                        $data['category_boosted'] = '1';
                        $result = DB::table('category_boosts')->where('category_id',$value->id)->where(['parent_id'=>$user->id,'child_id'=>$child_id])->first();
                        $data['boosted_value'] = $result->boosted_by;
                    }
                }
                if($user->type=='2' && $value->type=='2')
                {
                   $check = \DB::table('category_flag')->where(['user_id'=>$user->id,'category_id'=>$value->id])->first();
                   if(!empty($check))
                    {
                       $data['min_threshold_acheived'] = $check->min_threshold_acheived;
                    }
                }
                
            return $data;
            });
            $category->all();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            //return $meta;
            if($total>0)
            {
                return Response::json(array('status'=> 'success','message'=>'All Categories','meta'=>$meta,'data'=>$category))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No Data Found','meta'=>$meta,'data'=>$category))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function getCategoryTaskDetails(Request $request,$id)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }
            $user = Auth::user();
            $child_id = "";
            $task_id = array();
            $category = new category();
            $category = $category->select('category.id as id','category.category_name','task.id as task_id','task.task_name as question','task.question_type','task.task_points','task.attempt_1','task.attempt_2','task.attempt_3','task.above_3')->join('country_category as cc','cc.category_id','=','category.id')->join('countries as co','co.id','=','cc.country_id')->join('task', 'task.category_id', '=', 'category.id')->where('category.id',$id)->where('category.status','1');
            $correct_task_id = new UserTaskData();
            $correct_task_id = $correct_task_id->select('task_id');
            if($user->type=='3')
            {
                if($request->child_id=='')
                {
                   return Response::json(array('status'=> 'error','message'=>'Child id required.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                else if(!User::where(['id'=>$request->child_id,'type'=>'2'])->exists())
                {
                    return Response::json(array('status'=> 'error','message'=>'Child Id not exist.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                else if(!DB::table('tag_parent')->where(['user_id'=>$request->child_id,'parent_id'=>$user->id])->exists())
                {
                    return Response::json(array('status'=> 'error','message'=>'Child not tagged to you.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                $child_id = $request->child_id;
                $correct_task_id = $correct_task_id->where('user_id',$child_id);
                $category->where('cc.country_id',$country_id->country_id);
                //$total = $category->count();
            }
            else
            {
                $category_data = Category::select('type')->where('id',$id)->first();
                if($category_data->type=='1')
                {
                    $required_category = Category::select('id')->where('type','2')->get()->toArray();
                    if(!empty($required_category))
                    {
                        $category_ids = array_column($required_category,'id');
                    }
                    $count = count($required_category);
                    if(!empty($category_ids))
                    {
                        $check = \DB::table('category_flag')->whereIn('category_id',$category_ids)->where('user_id',$user->id)->count();
                        if($check!=$count)
                        {
                           $elective_category = '2'; 
                        }
                        else
                        {
                           $flag_check = \DB::table('category_flag')->whereIn('category_id',$category_ids)->where(['user_id'=>$user->id,'min_threshold_acheived'=>'2'])->count();
                           if($flag_check=='0')
                           {
                               $elective_category = '1'; 
                           }
                           else
                           {
                              $elective_category = '2';  
                           }
                        }
                    }
                    if($elective_category!='1')
                    {
                        return Response::json(array('status'=> 'error','message'=>'Please attempt mandatory category first then proceed further.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                    }
                }
                $correct_task_id = $correct_task_id->where('user_id',$user->id);
                $category = $category->where('cc.country_id',$user->country_id);
                //$total = $category->count();
            }
            $correct_task_id = $correct_task_id->where('is_final_submitted','2')->get()->toArray();
            //return $correct_task_id;
            
            if(!empty($correct_task_id))
            {
                foreach($correct_task_id as $correct_task_id)
                {
                    array_push($task_id,$correct_task_id['task_id']);
                }
                $category = $category->whereNotIn('task.id', $task_id);
            }
            $total = $category->count();
            $category = $category->offset($offset)->limit($perpage)->get();
           
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            $category = $category->map(function ($value)  use($user,$child_id) {
                $data['task_id'] = $value->task_id;
                $data['category_name'] = $value->category_name;
                $data['question'] = $value->question;
                $data['question_type'] = $value->question_type;
                $data['task_points'] = $value->task_points;
                $data['attemp1_points'] = $value->attempt_1;
                $data['attemp2_points'] = $value->attempt_2;
                $data['attemp3_points'] = $value->attempt_3;
                $data['above3_points'] = $value->above_3;
                $data['category_boosted'] = '0';
                $data['task_boosted'] = '0';
                $data['boosted_value'] = '0';
                if($user->type=='3')
                {
                    if(\DB::table('category_boosts')->where('category_id',$value->id)->where(['parent_id'=>$user->id,'child_id'=>$child_id])->exists())
                    {
                        $data['category_boosted'] = '1';
                        $result = DB::table('category_boosts')->where('category_id',$value->id)->where(['parent_id'=>$user->id,'child_id'=>$child_id])->first();
                        $data['boosted_value'] = $result->boosted_by;
                    }
                    if(\DB::table('user_task_data')->where('bonus_point','!=','0')->where(['task_id'=>$value->task_id,'parent_id'=>$user->id,'user_id'=>$child_id])->exists())
                    {
                        $boost = \DB::table('user_task_data')->where('bonus_point','!=','0')->where(['task_id'=>$value->task_id,'parent_id'=>$user->id,'user_id'=>$child_id])->first();
                        /*$data['boosted_value'] = '0';*/
                        if(!empty($boost))
                        {
                            $data['boosted_value'] = $boost->bonus_point;
                        }
                        $data['task_boosted'] = '1';
                    }
                }
                $check = \DB::table('user_task_data')->where('user_id',$value->user_id)->where('task_id',$value->task_id)->exists();
                $data['bonus_point'] = '0';
                $data['total_attempts'] = '0';
                $data['attempt_status'] = '1';
                if(\DB::table('user_task_data')->where('user_id',$value->user_id)->where('task_id',$value->task_id)->where('is_final_submitted','2')->exists())
                {
                    $data['attempt_status'] = '2';
                }
                if($check)
                {
                    $bonus_data = \DB::table('user_task_data')->select('bonus_point','total_attempt')->where('user_id',$value->user_id)->where('task_id',$value->task_id)->first();
                    $data['bonus_point'] = $bonus_data->bonus_point;
                    $data['total_attempts'] = $bonus_data->total_attempt;
                }
                /*if($value->options_value!='' && $value->user_id=='2'){
                $id = explode(',', $value->options_id);
                $value = explode(',', $value->options_value);
                    $data['options'] = array_map(function($key,$lable){
                        return array("label"=>$lable,"value"=>$key);
                    },$id, $value);
                }
                else
                {
                    $data['options'] = [];
                }*/
                $data['options'] = [];
                return $data;
                

            });
            $category->all();
            if(count($category)>0)
            {
                return Response::json(array('status'=> 'success','message'=>'Category Task Details','meta'=>$meta,'data' => $category))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
            else
            {
                return Response::json(array('status'=> 'success','message'=>'No Task Found','meta'=>$meta,'data' => $category))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function getCategoryDetails($id)
    {
        try
        {
            $category = new category();
            $category = $category->select('category.id as id','category.category_name')->where('category.status','1')->where('category.id',$id)->get();
            if(count($category)>0)
            {
                return Response::json(array('status'=> 'success','message'=>'Category Data','data'=>$category))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
            else
            {
                return Response::json(array('status'=> 'success','message'=>'No Data Found For This Category','data'=>$category))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function addAppreciation($id,Request $request)
    {
       try
        {
            $user = Auth::user();
            if($user->type=='3')
            { 
                $validator = Validator::make($request->all(), [
                    'student_id' => 'required',
                ]);
                if ($validator->fails()) {
                    $errorMsg = $validator->getMessageBag()->toArray();
                    return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                else
                {
                    $check = \DB::table('users')->where('id',$request->student_id)->where('type','2')->exists();
                    if(!$check)
                    {
                      return Response::json(array('status'=> 'error','message'=>'child not exist please try again.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                    }
                    $check_parent = \DB::table('users')->where('parent_id',$user->id)->where('id',$request->student_id)->exists();
                    if(!$check_parent)
                    {
                      return Response::json(array('status'=> 'error','message'=>'You are not parent of this user.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                    }
                    if($request->boost_points)
                    {
                        if($request->boost_points>2 || $request->boost_points<1)
                        {
                          return Response::json(array('status'=> 'error','message'=>'Please enter boost by value as per mentioned in DOC'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);   
                        }
                        if(\DB::table('category')->where('id',$id)->count()<1)
                        {
                          return Response::json(array('status'=> 'error','message'=>'Category not exist'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                        }
                        $check = \DB::table('category_boosts')->where(['parent_id'=>$user->id,'child_id'=>$request->student_id,
                                'category_id'=>$id])->exists();
                        if($check)
                        {
                            if($request->boost_points == '1')
                            {
                               \DB::table('category_boosts')->where(['parent_id'=>$user->id,'child_id'=>$request->student_id,
                                    'category_id'=>$id])->delete();
                               return Response::json(array('status'=> 'success','message'=>'Category unboosted successfully '))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                            ]);
                            }
                            \DB::table('category_boosts')->where(['parent_id'=>$user->id,'child_id'=>$request->student_id,'category_id'=>$id])->update(['boosted_by'=>$request->boost_points]);
                            return Response::json(array('status'=> 'success','message'=>'Bonus point updated on category'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            ]);  
                        }
                        else
                        {
                            if($request->boost_points == '1')
                            {
                              return Response::json(array('status'=> 'success','message'=>'Category boosted value is restored to '.$request->boost_points.'x .'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                ]); 
                            }
                            $data = array(
                                'parent_id'=>$user->id,
                                'child_id'=>$request->student_id,
                                'category_id'=>$id,
                                'boosted_by'=>$request->boost_points
                                );
                            $query = \DB::table('category_boosts')->insert($data);
                            return Response::json(array('status'=> 'success','message'=>'Bonus point added on category'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                        }                            
                    }
                    else
                    {
                      return Response::json(array('status'=> 'error','message'=>'Please enter bonus point value'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                    }
                    } 
            }  
                else
                {
                    return Response::json(array('status'=> 'error','message'=>'Please login as a parent to add Bonus point'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);  
                }
            }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        //return $ids;
    }

    public function searchCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'category_name'      => 'required',
        'child_id'           => 'required|numeric'
        ]);
        if ($validator->fails()) {
            $errorMsg = $validator->getMessageBag()->toArray();
            return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        else
        { 
            try
            { 
                $user = Auth::user();
                if(!User::where(['id'=>$request->child_id,'type'=>'2','parent_id'=>$user->id])->exists())
                {
                  return Response::json(array('status'=>'error','message'=>'Child not tagged with you.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                }
                $child_id = $request->child_id;
                $country_id = User::select('country_id')->where('id',$child_id)->first();
                //return $country_id;
                $category = Category::select('category.id','category.category_name')->join('country_category as cc','cc.category_id','=','category.id')->where('category.category_name', 'like', $request->category_name.'%')->where('category.status','1')->where('cc.country_id',$country_id->country_id)->get();
                $category = $category->map(function ($value) use ($user,$child_id) {
                    $data['id'] = $value->id;
                    $data['category_name'] = $value->category_name;
                    $data['category_boosted'] = '0';
                    $data['boosted_value'] = '0';
                    if(\DB::table('category_boosts')->where('category_id',$value->id)->where(['parent_id'=>$user->id,'child_id'=>$child_id])->exists())
                    {
                        $result = DB::table('category_boosts')->where('category_id',$value->id)->where(['parent_id'=>$user->id,'child_id'=>$child_id])->first();
                        $data['boosted_value'] = $result->boosted_by;
                        $data['category_boosted'] = '1';
                    }
                return $data;
                });
                $category->all();
                if(count($category)>0)
                {
                  return Response::json(array('status'=>'success','message'=>'Category Details','data'=>$category))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);   
                }
                else
                {
                    return Response::json(array('status'=>'error','message'=>'No Category Found','data'=>$category))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                
                }
            } 
            catch(QueryException $ex){ 
              return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
        }
    }
    
    /**
     * @return \Illuminate\Http\Response
     * Show the form for creating a new resource.
     *
     */
    public function create(Request $request)
    {
        
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }
        //
    /**
     * Store a newly created resource in storage.
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
