<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\LessonOfDay;
use App\Model\LessonOptions;
use App\Model\UserLessonData;
use Validator;
use DB;
use Response;
use Illuminate\Database\QueryException;


class LessonController extends Controller
{

    public function __construct()
    {
        //$this->middleware('guest');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }      
            if($current_page=='1')
            {
              $offset = 0;
            }
            else
            {
              $offset = ($current_page-1)*$perpage;
            }  
            $lessons = new LessonOfDay();
            $lessons = $lessons->select('lesson_of_day.lesson_title','lesson_of_day.skipped_time','lesson_of_day.file_attached','lesson_of_day.link_url','lesson_of_day.lesson_date','lesson_of_day.lesson_description','lesson_of_day.earn_points','lesson_of_day.question_type',\DB::raw('group_concat(dg_lesson_options.id) as options_id'),\DB::raw('group_concat(options) as options_value'),\DB::raw('group_concat(correct_options) as correct_answer'))
                    ->join('lesson_options', 'lesson_options.lesson_id', '=', 'lesson_of_day.id')
                    ->groupBy('lesson_options.lesson_id')
                    ->offset($offset)
                    ->limit($perpage)
                    ->get();
            $total = LessonOfDay::count();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            //return $task;
            //$task = array_map("myfunction",$task);
            $lessons = $lessons->map(function ($value) {
                if($value->options_id!='')
                {$id = explode(',', $value->options_id);}
                $data['lesson_id'] = $value->lesson_id;
                $data['lesson_title'] = $value->lesson_title;
                $data['correct_answer'] = $value->correct_answer;
                if($value->question_type=='2')
                {
                    $answer = \DB::table('lesson_options')->select(\DB::raw('group_concat(dg_lesson_options.options) as correct_options'))->where('lesson_options.lesson_id',$value->lesson_id)->where('correct_options','1')->groupBy('lesson_options.lesson_id')->get()->toArray();
                    $data['correct_answer'] = $answer[0]->correct_options;
                }
                $data['file_attached'] = $value->file_attached;
                $data['link_url'] = $value->link_url;
                $data['base_url'] = "https://quizme.com.my/digital_bank/storage/app/lessons/";
                $data['skipped_time'] = $value->skipped_time;
                $data['lesson_date'] = $value->lesson_date;
                $data['lesson_description'] = $value->lesson_description;
                $data['lesson_points'] = $value->earn_points;
                $data['question_type'] = $value->question_type;
                //$data['options'] = [[$value->options_id => $value->options_value]];
                if($value->options_value!=''){
                $value = explode(',', $value->options_value);
                    $data['options'] = array_map(function($key,$lable){
                        return array("label"=>$lable,"value"=>$key);
                    },$id, $value);
                }
                else
                {
                    $data['options'] = [];
                }
                //
                return $data; 
            });
            $lessons->all();
            if($total>0)
            {
                
                return Response::json(array('status'=> 'success','message'=>'Get All Lesson Of day','total_lessons'=>$total,'meta'=>$meta,'data'=>$lessons ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No Data Found','total_task'=>$total,'meta'=>$meta,'data'=>$lessons ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
           
    }
    
    public function getCurrentDayLesson()
    {
        try
        {
            $user = Auth::user();
            $currentDate = date('d');
            $lessons = new LessonOfDay();
            $lessons = $lessons->select(DB::raw("$user->id as user_id"),'lesson_of_day.lesson_title','lesson_of_day.skipped_time','lesson_of_day.id as lesson_id','lesson_of_day.file_attached','lesson_of_day.link_url','lesson_of_day.lesson_date','lesson_of_day.lesson_description','lesson_of_day.earn_points','lesson_of_day.question_type',\DB::raw('group_concat(dg_lesson_options.id) as options_id'),\DB::raw('group_concat(options) as options_value'),\DB::raw('group_concat(correct_options) as correct_answer'))
                    ->join('lesson_options', 'lesson_options.lesson_id', '=', 'lesson_of_day.id')
                    ->groupBy('lesson_options.lesson_id')
                    ->whereRaw('DATE(lesson_date) = CURDATE()')
                    ->get();
            $total = $lessons->count();
            $lessons = $lessons->map(function ($value) {
                if($value->options_id!='')
                {$id = explode(',', $value->options_id);}
                $data['lesson_id'] = $value->lesson_id;
                $data['lesson_title'] = $value->lesson_title;
                $data['correct_answer'] = $value->correct_answer;
                if($value->question_type=='2')
                {
                    $answer = \DB::table('lesson_options')->select(\DB::raw('group_concat(dg_lesson_options.options) as correct_options'))->where('lesson_options.lesson_id',$value->lesson_id)->where('correct_options','1')->groupBy('lesson_options.lesson_id')->get()->toArray();
                    $data['correct_answer'] = $answer[0]->correct_options;
                }
                $data['file_attached'] = '';
                if($value->file_attached!='')
                {
                    $data['file_attached'] = $value->file_attached;
                }
                $data['link_url'] = '';
                if($value->link_url!=null)
                {
                    $data['link_url'] = $value->link_url;
                    $data['file_attached'] = '';
                }
                $data['base_url'] = "https://quizme.com.my/digital_bank/storage/app/lessons/";
                $data['skipped_time'] = $value->skipped_time;
                $data['lesson_date'] = $value->lesson_date;
                 $data['lesson_points'] = $value->earn_points;
                $data['question_type'] = $value->question_type;
                $data['lesson_description'] = "";
                if($value->lesson_description!=null)
                {
                    $data['lesson_description'] = $value->lesson_description;
                }
                if(UserLessonData::where(['user_id'=>$value->user_id,'lesson_id'=>$value->lesson_id])->exists())
                {
                   $data['already_attempted'] = '1';
                   $data['options'] = [];
                }
                else
                {
                    $data['already_attempted'] = '0';
                    if($value->options_value!=''){
                    $value = explode(',', $value->options_value);
                        $data['options'] = array_map(function($key,$lable){
                            return array("label"=>$lable,"value"=>$key);
                        },$id, $value);
                    }
                    else
                    {
                        $data['options'] = [];
                    }
                }
               
                //$data['options'] = [[$value->options_id => $value->options_value]];
                
                return $data; 
            });
            $lessons->all();
            if($total>0)
            {
                
                return Response::json(array('status'=> 'success','message'=>'Today lesson of the day','data'=>$lessons ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No Lesson data found','data'=>$lessons ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function submit_lesson(Request $request,$id)
    {
        try
        {
            $user = Auth::user();
            if($user->type!='2')
            {
                return Response::json(array('status'=> 'error','message'=>'Please login as a child')) ;
            }
            $validator = Validator::make($request->all(), [
                'answer' => 'required',
                ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                     ]);
            }
            else
            {
                $lesson_data = LessonOfDay::select('question_type','earn_points')->where('id',$id)->whereRaw('DATE(lesson_date) = CURDATE()')->first();
                if(empty($lesson_data))
                {
                    return Response::json(array('status'=> 'error','message'=>'Lesson is not available for submission.')) ; 
                }
                if(UserLessonData::where(['user_id'=>$user->id,'lesson_id'=>$id])->exists())
                {
                   return Response::json(array('status'=> 'error','message'=>'You already attempt this Lesson'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                }
    
                
                    $lesson_data = LessonOfDay::select('question_type','earn_points')->where('id',$id)->first();
                    //print_r($lesson_data); die;
                    $attempt = 0;
                    $lesson_id = $id;
                    $current_points = DB::select(DB::raw("Select get_balance(".$user->id.",4) AS bal"));
                    $current_point=$current_points[0]->bal;
                    $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = $user->id AND dg_account.account_type = 4
                        ) as pool_account"),DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = $user->id AND dg_account.account_type = 1
                        ) as current_account"))->where('user_id',$user->id)->first();
                    if($current_point >= $lesson_data->earn_points)
                    {
                        $account_type = $_ENV['ACCOUNT_TYPE'];
                        //$question = Task::select('question_type')->where('id',$id)->first();
                        $question_type = $lesson_data->question_type;
                        $task_id = $id;
                        if($question_type==1)
                        {
                            $answer = explode(',', $request->answer);
                            $answ = array();
                            $correct_answer = LessonOptions::select('correct_options')->where('lesson_id',$lesson_id)->get();
                            foreach ($correct_answer as $ans) {
                                array_push($answ,$ans->correct_options); 
                            }
                            $answ = array_map('strtolower', $answ);
                            $answer = array_map('strtolower', $answer);
                            
                            //array_map('strtolower', $arr1)
                            if($answer == $answ){
                                $check_ans = 2;
                                $attempt+=1;
                            }
                        
                            $result = new UserLessonData;
                            $result->user_id            = $user->id;
                            $result->lesson_id            = $lesson_id;
                            $result->is_final_submitted = $check_ans;
                            $result->total_attempt     = $attempt;
                            }else{
                                $check_ans = 1;
                                $attempt+=1;
                            
                            $check=UserLessonData::where(['user_id'=>$user->id,'lesson_id'=>$lesson_id])->exists();
                            if(!$check){
                                $result->save();
                            }else{
                                $data = array(
                                'is_final_submitted' => $check_ans,
                                'total_attempt'     => $attempt,
                                );
                                UserLessonData::where(['user_id'=>$user->id,'lesson_id'=>$lesson_id])->update($data);
                            }
                        }
                        if($question_type==2)
                        {
                            $given_answer = $request->answer;
                            if($given_answer!='')
                            {
                                $given_answer = array_filter(explode(',', $given_answer),function($arr){
                                return is_numeric($arr);
                                });
                            if(empty($given_answer))
                            {
                                return Response::json(array('status'=> 'error','message'=>'Please answer correct as per doc'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                ]); 
                            }
                            $correct_answ = array();
                            $given_answ = array();
                            $correct_answer = LessonOptions::select('correct_options','options')->where(array('correct_options'=>1,'lesson_id'=>$lesson_id))->get();
                            $answer = LessonOptions::select('options')->whereIn('id',$given_answer)->get();
                            foreach ($correct_answer as $ans) {
                                array_push($correct_answ,$ans->options); 
                            }
                            foreach ($answer as $ans) {
                                array_push($given_answ,$ans->options); 
                            }
                            if(array_udiff($given_answ, $correct_answ, 'strcasecmp')){
                                $check_ans = 1;
                                $attempt+=1;
                            }else{
                                $check_ans = 2;
                                $attempt+=1;
                            }
                                
                            }
                            else{
                                $check_ans = 1;
                                $attempt+=1;  
                            }
                            
                            //$given_answer = implode(',', $given_answ);
                            $result = new UserLessonData;
                            $result->user_id            = $user->id;
                            $result->lesson_id            = $lesson_id;
                            $result->is_final_submitted = $check_ans;
                            $result->total_attempt     = $attempt;
                            
                            $check=UserLessonData::where(['user_id'=>$user->id,'lesson_id'=>$lesson_id])->exists();
                            if(!$check){
                                $result->save();
                            }else{
                                $data = array(
                                'is_final_submitted' => $check_ans,
                                'total_attempt'     => $attempt,
                                );
                                UserLessonData::where(['user_id'=>$user->id,'lesson_id'=>$lesson_id])->update($data);
                            }
                        }
                        if($question_type==3)
                        {
                            $given_answer = '';
                            if($request->has('answer'))
                            {
                                $given_answer = $request->answer;
                            }
                            if($given_answer!='')
                            {
                            $answer = array_filter(explode(',', $request->answer),function($arr){
                            return is_numeric($arr);
                            });
                            if(empty($answer))
                            {
                            return Response::json(array('status'=> 'error','message'=>'Please answer correct as per doc'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            ]);
                            }
                            $options = new LessonOptions();
                            $answ = array();
                            $given_answ = array();
                            $correct_answer = LessonOptions::select('id','correct_options')->where('task_id',$task_id)->get();
                            foreach ($correct_answer as $ans=>$value)
                            {
                            array_push($answ,$value->correct_options);
                            }
                            foreach ($answer as $ans=>$value)
                            {
                            $given_answer = LessonOptions::select('options')->where('id',$value)->first();
                            array_push($given_answ,$given_answer->options);
                            }
                            $answ = array_map('strtolower', $answ);
                            $given_answ = array_map('strtolower', $given_answ);
                            //return $given_answ;
                            if($given_answ == $answ){
                                $check_ans = 2;
                                $attempt+=1;
                            }else{
                                $check_ans = 1;
                                $attempt+=1;
                            }
                            }
                            else
                            {
                                $check_ans = 1;
                                $attempt+=1;  
                            }
                            $result = new UserLessonData;
                            $result->user_id            = $user->id;
                            $result->lesson_id            = $lesson_id;
                            $result->is_final_submitted = $check_ans;
                            $result->total_attempt     = $attempt;
                            
                            $check=UserLessonData::where(['user_id'=>$user->id,'lesson_id'=>$lesson_id])->exists();
                            if(!$check){
                                $result->save();
                            }else{
                                $data = array(
                                'is_final_submitted' => $check_ans,
                                'total_attempt'     => $attempt,
                                );
                                UserLessonData::where(['user_id'=>$user->id,'lesson_id'=>$lesson_id])->update($data);
                            }
                        }
                            if($check_ans=='2')
                            {
                                $data = array(
                                'user_id' => $user->id,
                                'from_id' => $user_data->pool_account,
                                'to_id'   => $user_data->current_account,
                                'type'=>'1',
                                'transaction_for_id'=>'1',
                                'account_type' => $account_type,
                                'transaction_points' => $lesson_data->earn_points,
                                'transaction_type'  => '1',
                                'remarks' => 'Earn Points After attempting Lesson of Day',
                                );
                                DB::table('transaction')->insert($data);
                                $data = array(
                                'user_id' => $user->id,
                                'from_id' => $user_data->pool_account,
                                'to_id'   => $user_data->current_account,
                                'type'=>'1',
                                'transaction_for_id'=>'1',
                                'account_type' => $account_type,
                                'transaction_points' => $lesson_data->earn_points,
                                'transaction_type'  => '2',
                                'remarks' => 'Earn Points By your child '.$user->name.' After attempting Lesson of Day',
                                );
                                DB::table('transaction')->insert($data);
                                return Response::json(array('status'=> 'success','message'=>'Correct Answer','earn_points'=>$lesson_data->earn_points))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                ]); 
                        }
                        else
                        {
                       
                            return Response::json(array('status'=> 'error','message'=>'Incorrect Answer','earn_points'=>'0'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            ]);
                        }
                    }
                    else
                    {
                       return Response::json(array('status'=> 'error','message'=>'Your parent dont have enough balance. Request sent to your parent to add points.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                    }
                    
            }
        }
        catch(QueryException $ex){
        if($ex->errorInfo[0] == "45000" && $ex->errorInfo[1] == "1644") {
                return Response::json(array('status'=> 'error','message'=>$ex->errorInfo[2]))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            } 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function lessonHistory(Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }      
            if($current_page=='1')
            {
              $offset = 0;
            }
            else
            {
              $offset = ($current_page-1)*$perpage;
            }  
            $user = Auth::user();
            $lessons = new UserLessonData();
            $lessons = $lessons->select('user_lesson_data.total_attempt','user_lesson_data.is_final_submitted','lesson_of_day.lesson_title','lesson_of_day.lesson_date','lesson_of_day.lesson_description','lesson_of_day.earn_points')
                    ->join('lesson_of_day', 'lesson_of_day.id', '=', 'user_lesson_data.lesson_id')
                    ->where('user_lesson_data.user_id',$user->id)
                    ->offset($offset)
                    ->limit($perpage)
                    ->get();
            $lessons = $lessons->map(function ($value) {
                $data['lesson_title'] = $value->lesson_title;
                $data['answer_given'] = "Incorrectly";
                $data['total_attempt'] = $value->total_attempt;
                $data['lesson_points'] = $value->earn_points;
                $data['earn_points'] = "0";
                if($value->is_final_submitted=='2')
                {
                    $data['answer_given'] = "Correctly";
                    $data['earn_points'] = $value->earn_points;
                }
                $data['lesson_date'] = $value->lesson_date;
                $data['lesson_description'] = "";
                if($value->lesson_description!=null)
                {
                    $data['lesson_description'] = $value->lesson_description;
                }
                return $data; 
            });
            $lessons->all();
            $total = UserLessonData::count();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            if($total>0)
            {
                
                return Response::json(array('status'=> 'success','message'=>'User Lesson History','total_attempted_lesson'=>$total,'meta'=>$meta,'data'=>$lessons ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No Data Found','total_task'=>$total,'meta'=>$meta,'data'=>$lessons ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
           
    }
}
