<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Account;
use App\Model\Auction;
use App\Model\User;
use App\Model\Task;
use App\Model\AuctionCategories;
use App\Model\AuctionProducts;
use App\Model\UserWishlist;
use Response;
use Validator;
use DB;
use Illuminate\Database\QueryException;
use ResponseHeader;

class BiddingController extends Controller
{

    public function __construct()
    {
        //$this->middleware('guest');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function userParticipate($id,Request $request)
    {
        try
        {
            $user = Auth::user();
            if($user->type==2)
            {
                // check auction existence
                if(!Auction::where('id',$id)->exists())
                {
                    $message = "Auction not exist";
                    $data = [];
                    return ResponseHeader::sendErrorResponse($message,$data);
                }
                //check auction Active or not
                elseif(Auction::where(['id'=>$id,'status'=>'2'])->exists())
                {
                    $message = "Auction not active for participate.";
                    $data = [];
                    return ResponseHeader::sendErrorResponse($message,$data);  
                }
                //check auction Active or not
                elseif(Auction::where(['id'=>$id,'status'=>'4'])->exists())
                {
                    $message = "Auction is closed cannot participate now.";
                    $data = [];
                    return ResponseHeader::sendErrorResponse($message,$data);  
                }
                // check user have parent or not
                elseif(DB::table('users')->where('parent_id','0')->where('id',$user->id)->exists())
                {
                    $message = "Please tag parent first for participate in auction.";
                    $data = [];
                    return ResponseHeader::sendErrorResponse($message,$data); 
                }
                $auction_data = Auction::select('id','auction_price','auction_category_id')->where('id',$id)->first();
                $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = $user->id AND dg_account.account_type = 4
                        ) as pool_account"),DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = $user->id AND dg_account.account_type = 1
                        ) as current_account"))->where('user_id',$user->id)->first();
                $admin_data = Account::select(DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = '1' AND dg_account.account_type = 1
                        ) as admin_account"))->where('user_id','1')->first();
                // check user already participated or not
                if(!DB::table('buffer_logs')->where(['auction_id'=>$id,'from_id'=>$user->id])->exists())
                {
                    // $priority_product = UserWishlist::where(['user_wishlist.user_id'=>$user->id,'user_wishlist.product_priority'=>'1'])->count();
                    if(DB::table('user_auction_products')->where(['auction_id'=>$id,'user_id'=>$user->id])->exists())
                    {
                        $wishlist_data = DB::table('user_auction_products as uap')->select('auction.id as auction_id','auction.auction_price','auction.single_bid_value','auction.incremented_bid_time','auction.current_auction_value','auction.start_time','auction.end_time','abh.bid_hash','pd.product_name','pd.product_image','users.name as last_bidder')->join('auction_products as pd','pd.id','=','uap.product_id')->join('auction','auction.id','=','uap.auction_id')->leftjoin('users','users.id','=','auction.last_bid_user')->join('auction_bid_hash as abh','abh.auction_id','=','auction.id')->where(['uap.auction_id'=>$id,'uap.user_id'=>$user->id])->get();
                    $wishlist_data = $wishlist_data->map(function ($value) {
                    
                    $data['auction_id'] = $value->auction_id;
                    $data['auction_price'] = $value->auction_price;
                    $data['single_bid_value'] = $value->single_bid_value;
                    $data['incremented_bid_time'] = $value->incremented_bid_time;
                    $data['current_auction_value'] = $value->current_auction_value;
                    $data['start_time'] = $value->start_time;
                    $data['end_time'] = $value->end_time;
                    $data['bid_hash'] = $value->bid_hash;
                    $data['product_name'] = $value->product_name;
                    $data['product_image'] = $value->product_image;
                    $data['last_bidder'] = '';
                    if(!$value->last_bidder==null)
                    {
                       $data['last_bidder'] = $value->last_bidder;
                    }
                    return $data; 
                    });
                    $wishlist_data->all();
                       $data = array(
                        'from_id'            => $user_data->current_account,
                        'to_id'              => $admin_data->admin_account,
                        'auction_id'         => $id,
                        'transaction_points' => $auction_data->auction_price,
                        'remarks'            => 'Child '.$user->name.' Participate in auction',
                        'transaction_type'   => '2',
                        );
                        //return $data;
                        DB::table('buffer_logs')->insert($data);
                        $message = "Participated successfully.";
                        $data = $wishlist_data;
                        return ResponseHeader::sendSuccessResponse($message,$data);
                    }
                    $wishlist_data = UserWishlist::select('user_wishlist.id as id','user_wishlist.product_id as product_id','user_wishlist.product_name','user_wishlist.product_image')->join('auction_products as pd','pd.id','=','user_wishlist.product_id')->where(['pd.auction_category_id'=>$auction_data->auction_category_id,'user_wishlist.user_id'=>$user->id])->get();
                    if(count($wishlist_data)<=0)
                    {
                        $auction_data = Auction::select('id','auction_price','auction_category_id')->where('id',$id)->get();
                        $message = "Please add product in wishlist belong to that auction.";
                        $data = $auction_data;
                        return ResponseHeader::sendErrorResponse($message,$data); 
                    }
                    if(count($wishlist_data)==1)
                    {
                        $product_id = $wishlist_data[0]->product_id;
                        $data = array(
                            'user_id' => $user->id,
                            'product_id' => $product_id,
                            'auction_id' => $id 
                        );
                        DB::table('user_auction_products')->insert($data);
                        //UserWishlist::where(['user_id'=>$user->id,'product_id'=>$product_id])->update(['product_priority'=>'1']);
                    }
                    if(count($wishlist_data)>1)
                    {
                        $message = "Please select highest priority of product first to participate in auction.";
                        $wishlist_data = UserWishlist::select(DB::raw("$id as auction_id"),'user_wishlist.id as id','user_wishlist.product_id as product_id','user_wishlist.product_name','user_wishlist.product_image')->join('auction_products as pd','pd.id','=','user_wishlist.product_id')->where(['pd.auction_category_id'=>$auction_data->auction_category_id,'user_wishlist.user_id'=>$user->id])->get();
                        $wishlist_data = $wishlist_data->map(function ($value) {
                        $data['id'] = $value->id;
                        $data['auction_id'] = $value->auction_id;
                        $data['product_id'] = $value->product_id;
                        $data['product_name'] = $value->product_name;
                        $data['product_image'] = $value->product_image;
                        return $data; 
                        });
                        $wishlist_data->all();
                        $data = $wishlist_data;
                        return ResponseHeader::sendSuccessResponse($message,$data);
                    }
                    // return Response::json(array('status'=> 'success','message'=>'Product list in wishlist','data'=>$wishlist_data))->withHeaders([
                    //         'Content-Type' => 'application/json',
                    //         'Accept' => 'application/json',
                    //      ]);
                    $wishlist_data = DB::table('user_auction_products as uap')->select('auction.id as auction_id','auction.auction_price','auction.single_bid_value','auction.incremented_bid_time','auction.current_auction_value','auction.start_time','auction.end_time','abh.bid_hash','pd.product_name','pd.product_image','users.name as last_bidder')->join('auction_products as pd','pd.id','=','uap.product_id')->join('auction','auction.id','=','uap.auction_id')->leftjoin('users','users.id','=','auction.last_bid_user')->join('auction_bid_hash as abh','abh.auction_id','=','auction.id')->where(['uap.auction_id'=>$id,'uap.user_id'=>$user->id])->get();
                    $wishlist_data = $wishlist_data->map(function ($value) {
                    
                    $data['auction_id'] = $value->auction_id;
                    $data['auction_price'] = $value->auction_price;
                    $data['single_bid_value'] = $value->single_bid_value;
                    $data['incremented_bid_time'] = $value->incremented_bid_time;
                    $data['current_auction_value'] = $value->current_auction_value;
                    $data['start_time'] = $value->start_time;
                    $data['end_time'] = $value->end_time;
                    $data['bid_hash'] = $value->bid_hash;
                    $data['product_name'] = $value->product_name;
                    $data['product_image'] = $value->product_image;
                    $data['last_bidder'] = '';
                    if(!$value->last_bidder==null)
                    {
                       $data['last_bidder'] = $value->last_bidder;
                    }
                    return $data; 
                    });
                    $wishlist_data->all();
                    /*$data = array(
                        'from_id'            => $user->id,
                        'to_id'              => '1',
                        'auction_id'         => $id,
                        'transaction_points' => $auction_data->auction_price,
                        'remarks'            => 'Child '.$user->name.' Participate in auction',
                        'transaction_type'   => '2',
                    );*/
                    $data = array(
                        'from_id'            => $user_data->current_account,
                        'to_id'              => $admin_data->admin_account,
                        'auction_id'         => $id,
                        'transaction_points' => $auction_data->auction_price,
                        'remarks'            => 'Child '.$user->name.' Participate in auction',
                        'transaction_type'   => '2',
                        );
                    DB::table('buffer_logs')->insert($data);
                    $message = "Participated successfully.";
                    $data = $wishlist_data;
                    return ResponseHeader::sendSuccessResponse($message,$data); 
                }
                else
                {
                   $wishlist_data = DB::table('user_auction_products as uap')->select('auction.id as auction_id','auction.auction_price','auction.single_bid_value','auction.incremented_bid_time','auction.current_auction_value','auction.start_time','auction.end_time','abh.bid_hash','pd.product_name','pd.product_image','users.name as last_bidder')->join('auction_products as pd','pd.id','=','uap.product_id')->join('auction','auction.id','=','uap.auction_id')->leftjoin('users','users.id','=','auction.last_bid_user')->join('auction_bid_hash as abh','abh.auction_id','=','auction.id')->where(['uap.auction_id'=>$id,'uap.user_id'=>$user->id])->get();
                    $wishlist_data = $wishlist_data->map(function ($value) {
                    
                    $data['auction_id'] = $value->auction_id;
                    $data['auction_price'] = $value->auction_price;
                    $data['single_bid_value'] = $value->single_bid_value;
                    $data['incremented_bid_time'] = $value->incremented_bid_time;
                    $data['current_auction_value'] = $value->current_auction_value;
                    $data['start_time'] = $value->start_time;
                    $data['end_time'] = $value->end_time;
                    $data['bid_hash'] = $value->bid_hash;
                    $data['product_name'] = $value->product_name;
                    $data['product_image'] = $value->product_image;
                    $data['last_bidder'] = '';
                    if(!$value->last_bidder==null)
                    {
                       $data['last_bidder'] = $value->last_bidder;
                    }
                    return $data; 
                    });
                    $wishlist_data->all();
                    $message = "You already participtated in auction.";
                    $data = $wishlist_data;
                    return ResponseHeader::sendSuccessResponse($message,$data);   
                }
                
            }
            else
            {
                $message = "Please login as a child to participate in auction.";
                $data = [];
                return ResponseHeader::sendErrorResponse($message,$data);
            }
        }
        catch(QueryException $ex){
            if($ex->errorInfo[0] == "45000" && $ex->errorInfo[1] == "1644") {
                $message = $ex->errorInfo[2];
                $data = [];
                return ResponseHeader::sendErrorResponse($message,$data);
            }
            $message = $ex->getMessage();
            $data = [];
            return ResponseHeader::sendErrorResponse($message,$data);
        }
    }

    // public function checkWishlistProducts($id,Request $request)
    // {
    //     try
    //     {
    //         $user = Auth::user();
    //         if($user->type==2)
    //         {
    //             if(!Auction::where('id',$id)->exists())
    //             {
    //               return Response::json(array('status'=> 'error','message'=>'Auction not exist.'))->withHeaders([
    //                         'Content-Type' => 'application/json',
    //                         'Accept' => 'application/json',
    //                      ]);   
    //             }
    //             if(DB::table('users')->where('parent_id','0')->where('id',$user->id)->exists())
    //             {
    //                return Response::json(array('status'=> 'error','message'=>'Please tag parent first for participate in auction'))->withHeaders([
    //                         'Content-Type' => 'application/json',
    //                         'Accept' => 'application/json',
    //                      ]); 
    //             }
    //             $auction_data = Auction::select('auction_category_id')->where('id',$id)->first();
    //             $wishlist_data = UserWishlist::select('user_wishlist.id as id','user_wishlist.product_id as product_id','user_wishlist.product_name','user_wishlist.product_image')->join('auction_products as pd','pd.id','=','user_wishlist.product_id')->where(['pd.auction_category_id'=>$auction_data->auction_category_id,'user_wishlist.user_id'=>$user->id])->get();
    //             if(count($wishlist_data)<=0)
    //             {
    //                 return Response::json(array('status'=> 'error','message'=>'Please add product in wishlist belong to that auction'))->withHeaders([
    //                         'Content-Type' => 'application/json',
    //                         'Accept' => 'application/json',
    //                      ]); 
    //             }
    //             return Response::json(array('status'=> 'success','message'=>'Product list in wishlist','data'=>$wishlist_data))->withHeaders([
    //                         'Content-Type' => 'application/json',
    //                         'Accept' => 'application/json',
    //                      ]);  
    //         }
    //         else
    //         {
    //             return Response::json(array('status'=> 'error','message'=>'Please login as a child to participate in auction'))->withHeaders([
    //                         'Content-Type' => 'application/json',
    //                         'Accept' => 'application/json',
    //                      ]); 
    //         }
    //     }
    //     catch(QueryException $ex){ 
    //       return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
    //                         'Content-Type' => 'application/json',
    //                         'Accept' => 'application/json',
    //                      ]); 
    //     }
    // }

    public function setPriorityOfProducts(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                    'product_id' => 'required',
                    'auction_id' => 'required',
                ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
            }
            else
            {
                $user = Auth::user();
                $id = $request->product_id;
                $auction_id = $request->auction_id;
                if(!UserWishlist::where(['user_id'=>$user->id,'product_id'=>$id])->exists())
                {
                  return Response::json(array('status'=> 'error','message'=>'Product not exist in your wishlist.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);     
                }
                
                if($user->type==2)
                {
                    
                    if(DB::table('user_auction_products')->where(['user_id'=>$user->id,'product_id'=>$id,'auction_id'=>$auction_id])->count()>1)
                    {
                       return Response::json(array('status'=> 'error','message'=>'Product already exist which has high priority in that auction.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);  
                    }
                    else
                    {
                        $data = array(
                            'user_id' => $user->id,
                            'product_id' => $id,
                            'auction_id' => $auction_id 
                        );
                        DB::table('user_auction_products')->insert($data);
                        return Response::json(array('status'=> 'success','message'=>'Product priority added'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);   
                    }
                }
                else
                {
                    return Response::json(array('status'=> 'error','message'=>'Please login as a child to change priority of products'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                }  
            }
        }
        catch(QueryException $ex){ 
            $message = $ex->getMessage();
            $data = [];
            return ResponseHeader::sendErrorResponse($message,$data); 
        }
    }

    public function userRequestBid(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                    'auction_id' => 'required',
                ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
            }
            else
            {
                $auction_id = $request->auction_id;
                $user = Auth::user();
                if(!Auction::where('id',$auction_id)->exists())
                {
                    $message = "Auction not exist";
                    $data = [];
                    return ResponseHeader::sendErrorResponse($message,$data);
                }
                //check auction Active or not
                //check user is child only
                if($user->type==2)
                {
                    $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = $user->id AND dg_account.account_type = 4
                        ) as pool_account"),DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = $user->id AND dg_account.account_type = 1
                        ) as current_account"))->where('user_id',$user->id)->first();
                    $auction_data = Auction::select('current_auction_value')->where('auction.id',$auction_id)->first();
                    if(!DB::table('buffer_logs')->where(['from_id'=>$user_data->current_account,'auction_id'=>$auction_id,'transaction_type'=>'2'])->exists())
                    {
                        /*$data = array(
                        'from_id'            => $user->id,
                        'to_id'              => '1',
                        'auction_id'         => $auction_id,
                        'transaction_points' => $auction_data->current_auction_value,
                        'remarks'            => 'Child '.$user->name.' Participate in auction',
                        'transaction_type'   => '2',
                        );*/
                        $data = array(
                        'from_id'            => $user_data->current_account,
                        'to_id'              => $admin_data->admin_account,
                        'auction_id'         => $id,
                        'transaction_points' => $auction_data->auction_price,
                        'remarks'            => 'Child '.$user->name.' Participate in auction',
                        'transaction_type'   => '2',
                        );
                        DB::table('buffer_logs')->insert($data);
                    }
                    
                    $last_bid_user = DB::table('auction_bids')->select('user_id')->where(['auction_id'=>$auction_id])->orderBy('id', 'desc')->limit(1)->first();
                    if(!empty($last_bid_user))
                    {
                        if($user->id==$last_bid_user->user_id)
                        {
                           return Response::json(array('status'=> 'error','message'=>"You have already placed a bid on this auction. Please wait for your next term."))->withHeaders([
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                 ]);  
                        }
                    }
                    
                   
                        $data = array(
                            'user_id'            => $user->id,
                            'auction_id'         => $auction_id,
                        );
                        //return $data;
                        DB::table('auction_bids')->insert($data);
                        
                        $bidding_details = Auction::select('auction.current_auction_value','users.name',DB::raw("get_balance($user->id,1) AS current_bal"))->join('users','users.id','=','auction.last_bid_user')->where(['users.id'=>$user->id,'auction.id'=>$auction_id])->first();
                    return Response::json(array('status'=> 'success','message'=>'Bid successfully','user_details'=>$bidding_details))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                   
                }
                else
                {
                    return Response::json(array('status'=> 'error','message'=>'Please login as a child to bid in auction'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                }
            }
        }
        catch(QueryException $ex){
            if(($ex->errorInfo[0] == "45000") && $ex->errorInfo[1] == "1644") {
                $message = $ex->errorInfo[2];
                $data = [];
                return ResponseHeader::sendErrorResponse($message,$data);
            }
            $message = $ex->getMessage();
            $data = [];
            return ResponseHeader::sendErrorResponse($message,$data); 
        }
    }
    
    public function auctionBidDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'auction_id' => 'required',
                ]);
        if ($validator->fails()) {
            $errorMsg = $validator->getMessageBag()->toArray();
            return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
        }
        else
        {
            $auction_id = $request->auction_id;
            if(!Auction::where('id',$auction_id)->exists())
            {
                return Response::json(array('status'=>'error','message'=>'Auction not exist'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            //check auction Started or not
            if(!Auction::where(['id'=>$auction_id,'status'=>'3'])->exists())
            {
                return Response::json(array('status'=>'error','message'=>'Auction not started yet'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            if(!$request->has('bid_hash'))
            {
              return Response::json(array('status'=>'error','message'=>'There is no change in auction'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
            }
            $bid_hash = $request->bid_hash;
            if(DB::table('auction_bid_hash')->where(['auction_id'=>$auction_id,'bid_hash'=>$bid_hash])->exists())
            {
                return Response::json(array('status'=>'error','message'=>'No bidding occured'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);   
            }
            // $my_current_value = DB::table('buffer_log')->select('transaction_points as tp')->where(['from_id'=>$user->id,'auction_id'=>$auction_id])->first();
            // $my_auction_buffer=0;
            // if(!empty($my_current_value))
            // {
            //     $my_auction_buffer = $my_current_value->tp;
            // }
            
            // $auction_data = Auction::select('auction.id','auction.start_time','auction.end_time',DB::raw("((current_auction_value - $my_auction_buffer)+single_bid_value) as single_bid_value"),'auction.total_bids','auction.current_auction_value','auction.auction_closed_at','auction.incremented_bid_time','users.name as last_bidder','abh.bid_hash')->join('auction_bid_hash as abh','abh.auction_id','=','auction.id')->join('users','users.id','=','auction.last_bid_user')->where(['auction.id'=>$auction_id,'auction.status'=>'3'])->get();
            $auction_data = Auction::select('auction.id','auction.start_time','auction.end_time','auction.single_bid_value','auction.total_bids','auction.current_auction_value','auction.auction_closed_at','auction.incremented_bid_time','users.name as last_bidder','abh.bid_hash')->join('auction_bid_hash as abh','abh.auction_id','=','auction.id')->join('users','users.id','=','auction.last_bid_user')->where(['auction.id'=>$auction_id,'auction.status'=>'3'])->get();
                return Response::json(array('status'=>'success','message'=>'Auction bid details','data'=>$auction_data))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);

        }
    }
    
    public function getUserBidDetails($id,Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }
            //return $offset;
            $user = Auth::user();
            $auction_id = $id;
            if($user->type!='2')
            {
                return Response::json(array('status'=> 'success','message'=>'Invalid user type for that request'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            if(!Auction::where('id',$id)->exists())
            {
                $message = "Auction not exist";
                $data = [];
                return ResponseHeader::sendErrorResponse($message,$data);
            }
            //check auction Active or not
            elseif(Auction::where(['id'=>$id,'status'=>'2'])->exists())
            {
                $message = "Auction not active.";
                $data = [];
                return ResponseHeader::sendErrorResponse($message,$data);  
            }
            //check auction Active or not
            elseif(Auction::where(['id'=>$id,'status'=>'3'])->exists())
            {
                $message = "Auction is in progress so please wait for complete the auction.";
                $data = [];
                return ResponseHeader::sendErrorResponse($message,$data);  
            }
            elseif(!\DB::table('auction_bid_logs')->where(['user_id'=>$user->id,'auction_id'=>$auction_id])->exists())
            {
                $message = "Not participated in autcion.";
                $data = [];
                return ResponseHeader::sendErrorResponse($message,$data);
            }
            $auction_data = \DB::table('auction_bid_logs as abl')->select('abl.user_id','abl.auction_id','acs.category_name as auction_name','abl.current_bid_value as bid_value','abl.last_bid_at as bid_time')->join('auction as ac','ac.id','=','abl.auction_id')->join('auction_categories as acs','acs.id','=','ac.auction_category_id')->where(['abl.user_id'=>$user->id,'abl.auction_id'=>$auction_id])->offset($offset)->limit($perpage)->get();
            $total = $auction_data->count();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            //return $meta;
            if($total>0)
            {
                return Response::json(array('status'=> 'success','message'=>'Bid Details','meta'=>$meta,'bid_data'=>$auction_data))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No bid found in that Aauction','meta'=>$meta,'bid_data'=>$auction_data))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
   
}
