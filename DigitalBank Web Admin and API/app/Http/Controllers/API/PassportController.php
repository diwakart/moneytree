<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;


class PassportController extends Controller
{

    public $successStatus = 200;

    public function login(Request $request) {
        // http://127.0.0.1:8000/api/login?mobile=8960850272&password=admin@123
        try
        {
            $validator = Validator::make($request->all(), [
                'user_name' => 'required',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                //if(Auth::attempt(['mobile' => request('user_name'), 'password' => request('password')]) || Auth::attempt(['email' => request('user_name'), 'password' => request('password')]))
                if(Auth::attempt(['user_name' => request('user_name'), 'password' => request('password')]))
                {
                    $user = Auth::user();
                    Auth::user()->AauthAcessToken()->delete();
                    $success['token'] =  $user->createToken('MyApp')->accessToken;
                    $success['message'] = 'Logged in successfully';
                    $share_url = "";
                    if($user->type=='3')
                    {
                        $share_url = "'https://quizme.com.my/digital_bank_v2/parent_refer/$user->digital_key'";
                        $details = User::select('users.id as user_id','users.user_name','users.gender','users.avtar','account.account_type','account.current_point','account.buffer_point','users.type',\DB::raw("$share_url as share_url"))->selectRaw('IFNULL(gender,"") as gender')->selectRaw('IFNULL(name,"") as name')->selectRaw('IFNULL(email,"") as email')->selectRaw('IFNULL(mobile,"") as mobile')->join('account', 'account.user_id', '=', 'users.id')->where('users.id',$user->id)->where('account.account_type','1')->get();
                    }
                    if($user->type=='2')
                    {
                        $wishlist_data = \DB::table('user_wishlist')->where('user_id',$user->id)->count();
                        $product_points = \DB::table('user_wishlist as uw')->select(\DB::raw("SUM(base_price) as required_points"))->join('auction_products as ap','ap.id','=','uw.product_id')->join('auction_categories as ac','ac.id','=','ap.auction_category_id')->where('user_id',$user->id)->first();
                        $product_points = ($product_points->required_points !=null) ? $product_points->required_points : 0;
                        $details = User::select('users.id as user_id','users.user_name','users.quiz_key as digital_key','users.avtar','account.account_type','account.current_point','account.buffer_point','users.type',\DB::raw("$wishlist_data as total_wishlist_products"),\DB::raw("$product_points as total_wishlist_points"))->selectRaw('IFNULL(gender,"") as gender')->selectRaw('IFNULL(name,"") as name')->selectRaw('IFNULL(email,"") as email')->selectRaw('IFNULL(mobile,"") as mobile')->join('account', 'account.user_id', '=', 'users.id')->where('users.id',$user->id)->where('account.account_type','1')->get();
                    }
                    return Response::json(['success' => $success,'data'=>$details], $this->successStatus)->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                else{
                    return Response::json(['error'=>'Unauthorized','message'=>'Please check credentials'], 401)->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function test() {
        echo 'Its Works!';
    }
}
