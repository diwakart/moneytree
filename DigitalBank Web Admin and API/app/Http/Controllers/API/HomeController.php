<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Response;
use App\Model\Countries;
use App\Model\User;
use Validator;

class HomeController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        //$this->middleware('guest');
    }


    /*
     * Attempt for Login
     *
     * @return \Illuminate\Http\Response
    */

    // public function login(Request $request)
    // {
    //     $contact = $request->input('contact');
    //     $password = $request->input('password');
    //     if (Auth::attempt(['mobile' => $contact, 'password' => $password]))
    //     {
    //         // Authentication passed...
    //         return Response::json(array('success'=>'Login Sucessfully'));
    //     }
    //     else
    //     {
    //         return Response::json(array('error'=>'Credential Not matched Please Try Again')) ;
    //     }
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::view('admin.exam')->header('Content-Type', 'text/html');
    }
    
    /**
    {
        //
    }
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }
        //
    /**
     * Store a newly created resource in storage.
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function getAllCountryDetails(Request $request)
    {
        try
        {
            
            $countries = new Countries();
            $countries = $countries->select('id','country_name','country_code');
            if($request->has('country_name'))
            {
                $country_name = $request->input('country_name');
                $countries = $countries->where('country_name', 'like', $country_name.'%');
            }
            $total = $countries->count();
            $countries = $countries->get();
            if(count($countries)>0)
            {
              return Response::json(array('status'=>'success','message'=>'Country Details','total'=>$total,'data'=>$countries))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                     ]);   
            }
            else
            {
                return Response::json(array('status'=>'error','message'=>'No Country Found','total'=>$total,'data'=>$countries))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                     ]); 
            
            }
        }
        //catch all exceptions
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function getAllAvtars(Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }
            $base_url = "https://quizme.com.my/digital_bank_v2/storage/app/avtars/";
            $avtars = \DB::table('avtars')->select('id','avtar_name',\DB::raw("'$base_url' as base_url"))->offset($offset)->limit($perpage)->get();
            $total = $avtars->count();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            //return $meta;
            if($total>0)
            {
                return Response::json(array('status'=> 'success','message'=>'All Avtars','meta'=>$meta,'data'=>$avtars))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No Data Found','meta'=>$meta,'data'=>$avtars))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function save()
    {
        \DB::table('api_routes')->insert(array (
            11 => 
            array (
                'route_name' => 'updateProfile',
                'route_path' => '/user/updateProfile.json'
            ),
            12 => 
            array (
                'route_name' => 'updateProfilePic',
                'route_path' => '/user/profilePicUpdate.json'
            ),
            13 => 
            array (
                'route_name' => 'childList',
                'route_path' => '/user/childlist.json'
            ),
            14 => 
            array (
                'route_name' => 'getAllParents',
                'route_path' => '/parents.json'
            ),
            15 => 
            array (
                'route_name' => 'getAllChild',
                'route_path' => '/child.json'
            ),
            16 => 
            array (
                'route_name' => 'childDashboardDeatails',
                'route_path' => '/child/dashboard.json'
            ),
            17 => 
            array (
                'route_name' => 'addRequest',
                'route_path' => '/user/request.json'
            ),
            18 => 
            array (
                'route_name' => 'userResponse',
                'route_path' => '/user/response.json'
            ),
            19 => 
            array (
                'route_name' => 'addChild',
                'route_path' => '/child/tag.json'
            ),
            20 => 
            array (
                'route_name' => 'searchUser',
                'route_path' => '/user/search.json'
            ),
            21 => 
            array (
                'route_name' => 'getAllRequest',
                'route_path' => '/user/notification.json'
            ),
            22 => 
            array (
                'route_name' => 'getAllNotifications',
                'route_path' => '/user_notification.json'
            ),
            23 => 
            array (
                'route_name' => 'stripePayment',
                'route_path' => '/user/payment.json'
            ),
            24 => 
            array (
                'route_name' => 'getAllPackages',
                'route_path' => '/user/packages.json'
            ),
            25 => 
            array (
                'route_name' => 'getAllTransaction',
                'route_path' => '/user/passbook.json'
            ),
            26 => 
            array (
                'route_name' => 'getAllCreditTransaction',
                'route_path' => '/user/earn_list.json'
            ),
            27 => 
            array (
                'route_name' => 'getAllDebitTransaction',
                'route_path' => '/user/spent_list.json'
            ),
            28 => 
            array (
                'route_name' => 'getTransactionDetails',
                'route_path' => '/user/transaction_details.json'
            ),
            29 => 
            array (
                'route_name' => 'referFriendRequest',
                'route_path' => '/user/refer_friend.json'
            ),
            30 => 
            array (
                'route_name' => 'userLogout',
                'route_path' => '/user/logout.json'
            ),
            31 => 
            array (
                'route_name' => 'getUserAccountDetails',
                'route_path' => '/user/account_details.json'
            ),
            32 => 
            array (
                'route_name' => 'pointDisbursalToChild',
                'route_path' => '/user/point_disbursal.json'
            ),
            33 => 
            array (
                'route_name' => 'setKidDefaultSetting',
                'route_path' => '/parent/kid_setting.json'
            ),
            34 => 
            array (
                'route_name' => 'deviceRegisterFcm',
                'route_path' => '/user/device_register.json'
            ),
            35 => 
            array (
                'route_name' => 'userProductRedemption',
                'route_path' => '/user/product_redemption.json'
            ),
            36 => 
            array (
                'route_name' => 'getAllTasks',
                'route_path' => '/tasks.json'
            ),
            37 => 
            array (
                'route_name' => 'getSingleTask',
                'route_path' => '/tasks/{id}.json'
            ),
            38 => 
            array (
                'route_name' => 'searchTask',
                'route_path' => '/task/search.json'
            ),
            39 => 
            array (
                'route_name' => 'SubmitTask',
                'route_path' => '/tasks/{id}/answer.json'
            ),
            40 => 
            array (
                'route_name' => 'addAppreciationOnTask',
                'route_path' => 'tasks/{id}/addBonusPoint.json'
            ),
            41 => 
            array (
                'route_name' => 'getAllLessons',
                'route_path' => '/lessons.json'
            ),
            42 => 
            array (
                'route_name' => 'SubmitLesson',
                'route_path' => '/lesson/{id}/answer.json'
            ),
            43 => 
            array (
                'route_name' => 'getCurrentDayLesson',
                'route_path' => '/lessons_of_day.json'
            ),
            44 => 
            array (
                'route_name' => 'lessonHistory',
                'route_path' => '/user/lesson_history.json'
            ),
            45 => 
            array (
                'route_name' => 'getAllCategories',
                'route_path' => '/categories.json'
            ),
            46 => 
            array (
                'route_name' => 'searchCategory',
                'route_path' => '/category/search.json'
            ),
            47 => 
            array (
                'route_name' => 'getSingleCategories',
                'route_path' => '/categories/{id}.json'
            ),
            48 => 
            array (
                'route_name' => 'getCategoriesTasks',
                'route_path' => '/categories/{id}/tasks.json'
            ),
            49 => 
            array (
                'route_name' => 'addAppreciationOnCategories',
                'route_path' => 'categories/{id}/addBonusPoint.json'
            ),
            50 => 
            array (
                'route_name' => 'getAllAuction',
                'route_path' => '/auction/categories.json'
            ),
            51 => 
            array (
                'route_name' => 'getAllProducts',
                'route_path' => '/auction/products.json'
            ),
            52 => 
            array (
                'route_name' => 'getAllCategoryProducts',
                'route_path' => '/category/{id}/products.json'
            ),
            53 => 
            array (
                'route_name' => 'addWishlistProduct',
                'route_path' => '/user/addwishlist.json'
            ),
            54 => 
            array (
                'route_name' => 'deleteWishlistProduct',
                'route_path' => '/user/removewishlist.json'
            ),
            55 => 
            array (
                'route_name' => 'getAllWishlistProducts',
                'route_path' => '/user/wishlistproducts.json'
            ),
            56 => 
            array (
                'route_name' => 'getAllChildDetails',
                'route_path' => '/child/{id}/details.json'
            ),
            57 => 
            array (
                'route_name' => 'getChildWishlist',
                'route_path' => '/child/{id}/wishlist.json'
            ),
            58 => 
            array (
                'route_name' => 'getChildSpentlist',
                'route_path' => '/child/{id}/spentlist.json'
            ),
            59 => 
            array (
                'route_name' => 'getChildEarnlist',
                'route_path' => '/child/{id}/earnlist.json'
            ),
            60 => 
            array (
                'route_name' => 'getAllAuctions',
                'route_path' => '/auctions.json'
            ),
            61 => 
            array (
                'route_name' => 'auctionHistory',
                'route_path' => '/user/auction_history.json'
            ),
            62 => 
            array (
                'route_name' => 'userParticipateInAuction',
                'route_path' => '/auction/{id}/participate.json'
            ),
            63 => 
            array (
                'route_name' => 'checkWishlistProducts',
                'route_path' => '/auction/{id}/check_wishlist.json'
            ),
            64 => 
            array (
                'route_name' => 'setPriorityOfProducts',
                'route_path' => '/product/set_priority.json'
            ),
            65 => 
            array (
                'route_name' => 'userRequestBid',
                'route_path' => '/user/bid.json'
            ),
            66 => 
            array (
                'route_name' => 'getUserBidDetails',
                'route_path' => '/user/{id}/bid_details.json'
            ),
            67 => 
            array (
                'route_name' => 'auctionBidDetails',
                'route_path' => '/auction/bid_details.json'
            ))
        );
    }
}
