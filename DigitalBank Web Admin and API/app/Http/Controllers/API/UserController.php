<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\UserPackages;
use App\Model\Transaction;
use App\Model\AuctionProducts;
use App\Model\UserWishlist;
use App\Model\Account;
use App\Model\UserNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use Response;
use Twilio\Rest\Client;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;
use DB;
use DeviceNotification;
use Twilio\Exceptions\RestException;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReferFriend;
use Twilio\Exceptions\TwilioException;
use QrCode;




class UserController extends Controller
{

    use SendsPasswordResetEmails;

    public function __construct()
    {
        //$this->middleware('guest');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'user_name' => 'required|min:4|unique:users',
                'type' => 'required|numeric|max:3',
                'password' => 'required',
                'country_id' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                if(!DB::table('countries')->where('id',$request->input('country_id'))->exists())
                {
                    return Response::json(array('status'=> 'error','message'=>'Country id not exist please check.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                if($request->input('type')=='0')
                {
                    return Response::json(array('status'=> 'error','message'=>'Not allowed type of user for register accept only 2 & 3'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                if($request->input('type')!='1')
                {
                    if($request->input('type')=='3')
                    {
                        if(!$request->has('mobile'))
                        {
                           return Response::json(array('status'=> 'error','message'=>'Mobile number manadatory for parents.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                            ]); 
                        }
                    }
                    if($request->has('mobile'))
                    {
                        if(!is_numeric($request->input('mobile')))
                        {
                           return Response::json(array('status'=> 'error','message'=>'Mobile number must be a number.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            ]); 
                        }
                        if(!$request->has('country_code'))
                        {
                           return Response::json(array('status'=> 'error','message'=>'Country code required for mobile.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                            ]); 
                        }
                        else if(!DB::table('countries')->where('country_code',$request->input('country_code'))->exists())
                        {
                            return Response::json(array('status'=> 'error','message'=>'Country code not exist please check.'))->withHeaders([
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                 ]);
                        }
                        $data['country_code'] = $request->input('country_code');
                        
                    }
                    $data = array(
                        'country_id' => $request->input('country_id'),
                        'user_name' => $request->input('user_name'),
                        'name' => $request->input('name'),
                        'country_code' => $request->input('country_code'),
                        'mobile' => $request->input('mobile'),
                        'avtar'=>'photo.png',
                        'type' => $request->input('type'),
                        'password' => bcrypt($request->input('password')),
                     );
                    if($request->has('email'))
                    {
                        if (!filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
                            return Response::json(array('status'=> 'error','message'=>'Enter proper email address.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            ]);
                        }
                        $data['email'] = $request->input('email');
                    }
                    //return $data;
                    $query = DB::table('users')->insert($data);
                    $details = User::select('id','user_name','name','digital_key')->orderBy('id', 'desc')->first();
                        QrCode::format('png')->size(200)->generate($details->digital_key,'./public/userQrcodes/'.$details->digital_key.'.png');
                        try
                        {
                                User::where('id',$details->id)->update(['qr_code'=>$details->digital_key.'.png']);
                        }
                        catch(QueryException $ex){ 
                          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                                            'Content-Type' => 'application/json',
                                            'Accept' => 'application/json',
                                         ]);
                        }
                    if($query)
                    {
                     return Response::json(array('status'=> 'success','message'=>'Registered Successfully','data'=>$details ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                    }
                    return Response::json(array('status'=> 'error','message'=>'Not Inserted'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                else
                {
                    return Response::json(array('status'=> 'error','message'=>'Not allowed type of user for register accept only 2 & 3'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function getAllChildData(Request $request)
    {
        try
        {
            $user = Auth::user();
            $child_data = array();
            if($user->type!='3')
            {
                return Response::json(array('status'=>'error','message'=>'Please login as parent to get all details.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);
            }
            $parent_data = \DB::table('tag_parent')->select(\DB::raw('group_concat(user_id) as user_ids'))->where('parent_id',$user->id)->get();
            if(!empty($parent_data))
            {
                $parent_ids = $parent_data[0]->user_ids;
                $user_ids = explode(',',$parent_ids); 
                $child_data = User::select('users.id as id','users.user_name','users.name','users.mobile','users.email','users.avtar')->whereIn('id',$user_ids)->get();
                $child_data = $child_data->map(function ($value) {
                    $data['child_id'] = $value->id;
                    $data['child_user_name'] = $value->user_name;
                    $data['avtar'] = $value->avtar;
                    return $data;
                });
                $child_data->all();
            }
            
            //return $child_data;
            $user_data = Account::select(DB::raw("get_balance($user->id,1) AS current_bal"))->where('user_id',$user->id)->first();
            $total_child = count($child_data);
            return Response::json(array('status'=> 'success','message'=>"$user->user_name Child details",'total_kids'=>$total_child,'remaining_balance'=>$user_data->current_bal,'child_data'=>$child_data))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function pointDisbursalToChild(Request $request)
    {
        try
        {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                    'disbured_amount' => 'required',
                    'child_data'  => 'required',
            ]);
            if ($validator->fails()) {
            $errorMsg = $validator->getMessageBag()->toArray();
            return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                //return $user;
                $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account WHERE dg_account.user_id = $user->id AND dg_account.account_type = 1) as current_account"),DB::raw("get_balance($user->id,1) AS current_bal"))->where('user_id',$user->id)->first();
                if($request->disbured_amount>$user_data->current_bal)
                {
                    return Response::json(array('status'=>'error','message'=>'disbursed amount not be greater than user balance'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                $child_data = "$request->child_data";
                $child_json  = json_decode($child_data, true);
                switch (json_last_error()) {
                    case JSON_ERROR_NONE:
                        $json_message = "No errors";
                    break;
                    case JSON_ERROR_DEPTH:
                        $json_message = "Maximum stack depth exceeded";
                    break;
                    case JSON_ERROR_STATE_MISMATCH:
                        $json_message = "Underflow or the modes mismatch";
                    break;
                    case JSON_ERROR_CTRL_CHAR:
                        $json_message = "Unexpected control character found";
                    break;
                    case JSON_ERROR_SYNTAX:
                        $json_message = "No errors";
                        echo ' - ';
                    break;
                    case JSON_ERROR_UTF8:
                        $json_message = "Malformed UTF-8 characters, possibly incorrectly encoded";
                    break;
                    default:
                        $json_message = "Unkonwn errors";
                    break;
                }
                if($json_message=='No errors')
                {
                    if (array_key_exists("id",$child_json[0]) && array_key_exists("points",$child_json[0]))
                    {
                        $sum = '0';
                        $kid_data = $child_json;
                        //return $kid_data; die;
                        $disbursed_amount = array_column($kid_data, 'points');
                        foreach($disbursed_amount as $disbursed_amount)
                        {
                            if (is_numeric($disbursed_amount)) {
                               $sum +=  $disbursed_amount;
                            }
                            else
                            {
                                return Response::json(array('status'=> 'error','message'=>'Points data not given properly.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                ]); 
                            }
                        }
                        if($sum>$request->disbured_amount)
                        {
                           return Response::json(array('status'=> 'error','message'=>'Point provided in json is greater than disbursed amount.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                ]); 
                        }
                        else
                        {
                           foreach($kid_data as $kid_data)
                            {
                                $id = $kid_data['id'];
                                $points = $kid_data['points'];
                                $child_account = Account::select(DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id =$id  AND dg_account.account_type = 4
                        ) as pool_account"))->where('user_id',$id)->first();
                                $data = array(
                                    'user_id' => $user->id,
                                    'from_id' => $user_data->current_account,
                                    'to_id' => $child_account->pool_account,
                                    'type'=>'8',
                                    'account_type' => '1',
                                    'transaction_points' => $points,
                                    'transaction_type'  => '2',
                                    'remarks' => "Disbursed $points point added to kid acount",
                                );
                                DB::table('transaction')->insert($data);
                                $data = array(
                                    'user_id' => $user->id,
                                    'from_id' => $user_data->current_account,
                                    'to_id' => $child_account->pool_account,
                                    'type'=>'8',
                                    'account_type' => '4',
                                    'transaction_points' => $points,
                                    'transaction_type'  => '1',
                                    'remarks' => "Disbursed $points point added to kid acount",
                                );
                                DB::table('transaction')->insert($data);
                            } 
                            return Response::json(array('status'=> 'success','message'=>'Point disbursed sucessfully.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                ]);
                        }
                       
                    }
                    else
                    {
                        return Response::json(array('status'=> 'error','message'=>'Please provide child data in proper json format as per doc.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                    }
                }
                else
                {
                    return Response::json(array('status'=> 'error','message'=>$json_message))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function setKidDefaultSetting(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'child_id'                  => 'required|numeric',
                'task_limit'                => 'required|numeric',
                'minimum_pool_threshold'    => 'required|numeric'
            ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                $user = Auth::user();
                if($user->type=='3')
                {
                   if(!User::where(['id'=>$request->child_id,'type'=>'2'])->exists())
                    {
                        return Response::json(array('status'=> 'error','message'=>'Child not exist please try with correct id of child.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                    }
                    else if(!DB::table('tag_parent')->where(['user_id'=>$request->child_id,'parent_id'=>$user->id])->exists())
                    {
                        return Response::json(array('status'=> 'error','message'=>'Child not tagged to you.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                    }
                    $data = array();
                    if(!is_numeric ($request->task_limit) || !is_numeric ($request->minimum_pool_threshold))
                            {
                                return Response::json(array('status'=> 'error','message'=>'Please provide task Limit or minimum data in proper json format as per doc.'))->withHeaders([
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                 ]);
                            }
                    elseif($request->minimum_pool_threshold>100)
                    {
                        return Response::json(array('status'=> 'error','message'=>'Please provide Pool assigned value less than 100.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                    }
                    $data['kid_setting']['limit_task'] = $request->task_limit;
                    $data['kid_setting']['auto_assigned_pool_points'] = $request->minimum_pool_threshold;
                    $result_data = json_encode($data);
                    $setting_data = array(
                            'user_id' => $request->child_id,
                            'settings_json' => $result_data,
                            'last_updated_by' => $user->id
                    );
                    if(!DB::table('settings')->where('user_id',$request->child_id)->exists())
                    {
                        DB::table('settings')->insert($setting_data);
                    }
                    else
                    {
                        DB::table('settings')->where('user_id',$request->child_id)->update($setting_data);
                    }
                        return Response::json(array('status'=> 'success','message'=>'Setting for kid saved succesfully.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        ]);
                    }
                        else
                        {
                            return Response::json(array('status'=> 'error','message'=>'Please provide child data in proper json format as per doc.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                        }
                }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    

    
    public function referFriendRequest(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:3',
            ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                $user = Auth::user();
                if($request->mobile=='' &&  $request->email=='')
                {
                   return Response::json(array('status'=> 'error','message'=>'Please provide either mobile number or email for refer a friend.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        ]); 
                }
                $sid = 'ACbfcf0a5d3a4790cc95d0ad12b2e7dd19';
                $token = '5930ab90756eade8563f0acc74ba5f69';
                $url = "https://quizme.com.my/digital_bank_v2/register/$user->digital_key";
                $client = new Client($sid, $token);
                if($request->has('mobile'))
                {
                    if(!is_numeric($request->input('mobile')))
                    {
                       return Response::json(array('status'=> 'error','message'=>'Mobile number must be a number.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        ]); 
                    }
                    if(!$request->has('country_code'))
                    {
                       return Response::json(array('status'=> 'error','message'=>'Country code required for mobile to send a sms to your friend.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                        ]); 
                    }
                    else if(!DB::table('countries')->where('country_code',$request->input('country_code'))->exists())
                    {
                        return Response::json(array('status'=> 'error','message'=>'Country code not exist please check.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);
                    }
                    $country_code = $request->input('country_code');
                    try {
                    $message = $client->messages->create(
                        // the number you'd like to send the message to
                        '+'.$country_code.$request->input('mobile'),
                        array(
                            // A Twilio phone number you purchased at twilio.com/console
                            'from' => '+15416159055',
                            // the body of the text message you'd like to send
                            'body' => "Hello $request->name Please Register on this Given link For digital bank. $url",
                            // 'mediaUrl' => 'http://big5kayakchallenge.com/wp-content/uploads/2018/01/inspirational-wallpaper-for-mobile-samsung-hii-wallpapers-to-your-cell-phone-hello-hi-wallpaper-for-mobile-samsung.jpg'
                        )
                    );
                    } catch (TwilioException $e) {
                        return Response::json(array('status'=> 'error','message'=>'Incorrect mobile number or country code please check.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        ]);
                    }
                    /*if(!$message)
                    {
                        return Response::json(array('status'=> 'success','message'=>'Message not sent please check mobile number.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        ]); 
                    }*/
                }
                if($request->has('email'))
                {
                    if (!filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
                        return Response::json(array('status'=> 'error','message'=>'Enter proper email address.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        ]);
                    }
                    $child_mail_data = array(
                            'from_name' => $user->name,
                            'to_name' => $request->name,
                            'refer_url'=> "$url",
                        );
                    Mail::to($request->email)->send(new ReferFriend($child_mail_data));
                }
                return Response::json(array('status'=> 'success','message'=>'Invitation Send succesfully.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        ]); 
                    
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function getAllParentsData(Request $request)
    {
        try 
        {
            $user = Auth::user();
            $user_type = User::select('type')->where('users.id',$user->id)->first();
            if($user_type->type=='2')
            {
                $parent_data = \DB::table('tag_parent')->select(\DB::raw('group_concat(parent_id) as parent_ids'))->where('user_id',$user->id)->get();
                $user_data = array();
                //return $parent_data;
                if(!empty($parent_data))
                {
                    $parent_ids = $parent_data[0]->parent_ids;
                    $user_ids = explode(',',$parent_ids); 
                    $user_data = User::select('users.id as id','users.user_name','users.name','users.mobile','users.email','users.avtar')->whereIn('id',$user_ids)->get();
                }
                return Response::json(array('status'=> 'success','message'=>'Child Parent Details','parent_data'=>$user_data))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                return Response::json(array('status'=> 'error','message'=>'Please login as Child for get all parent details'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
        } catch(QueryException $ex){
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function userDetails(Request $request)
    {
        try 
        {
            $user = Auth::user();
            $user_type = User::select('type')->where('users.id',$user->id)->first();
            if($user_type->type=='2')
            {
                /*$parent_data = \DB::table('tag_parent')->select(\DB::raw('group_concat(parent_id) as parent_ids'))->where('user_id',$user->id)->get();
                $user_data = array();
                //return $parent_data;
                if(!empty($parent_data))
                {
                    $parent_ids = $parent_data[0]->parent_ids;
                    $user_ids = explode(',',$parent_ids); 
                    $user_data = User::select('users.id as id','users.user_name','users.name','users.mobile','users.email','users.avtar')->whereIn('id',$user_ids)->get();
                }*/
                $details = User::select('users.id as id','users.country_id','users.user_name','users.name','countries.country_name','users.mobile','users.email','users.type','users.avtar','users.gender','users.school_name','account.account_type','account.current_point','account.buffer_point','users.dob','users.address')->Join('countries', 'countries.id', '=', 'users.country_id')->join('account', 'account.user_id', '=', 'users.id')->where('users.id',$user->id)->where('account.account_type','1')->get();
                return Response::json(array('status'=> 'success','message'=>'Child Details','data'=>$details))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            if($user_type->type=='3')
            {
                $total_students = \DB::table('tag_parent')->where('parent_id',$user->id)->count();
                $details = User::select('users.id as id','users.name','users.country_id','users.user_name','countries.country_name','users.mobile','users.email','users.type','users.avtar','users.gender','account.current_point','account.buffer_point','users.dob','users.address')->Join('countries', 'countries.id', '=', 'users.country_id')->join('account', 'account.user_id', '=', 'users.id')->where('users.id',$user->id)->where('account.account_type','1')->get();
                return Response::json(array('status'=> 'success','message'=>'Parent Details','data'=>$details,'total_child'=>$total_students))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
        } catch(QueryException $ex){
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function checkAvailability(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'user_name' => 'required|min:4'
            ]);
            // if ($validator->fails()) {
            //     $errorMsg = $validator->getMessageBag()->toArray();
            //     return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
            //                 'Content-Type' => 'application/json',
            //                 'Accept' => 'application/json',
            //              ]);
            // }
            
            if ($validator->fails()) {
                  $failedRules = $validator->getMessageBag()->toArray();
                  $errorMsg = "";
                  if(isset($failedRules['user_name']))
                    $errorMsg = $failedRules['user_name'][0];
                  return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                $check = User::where('user_name',$request->user_name)->exists();
                if(!$check)
                {
                    return Response::json(array('status'=> 'success','message'=>'User name available'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                            ]); 
                }
                else
                {
                    return Response::json(array('status'=> 'success','message'=>'That username is taken. Try another.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                        ]); 
                }
                
            }
        }
        catch(QueryException $ex){
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function childDashboardDeatails(){
        
    try{
        $user = Auth::user();
        $user_type = User::select('type')->where('users.id',$user->id)->first();
        if($user_type->type=='2')
        {
            $current_week  = date("W", strtotime('now'));
            $previous_week  = date("W", strtotime('-1 week'));
            $last_week  = date("W", strtotime('-2 week'));
            $wishlist_data = \DB::table('user_wishlist')->select(DB::raw("get_balance($user->id,1) AS bal"),DB::raw('count(*) as wishlist_items'),DB::raw('SUM(product_price) as total_price'))->where('user_id',$user->id)->get()->toArray();
            $product_details = \DB::table('user_wishlist')->select('product_name','product_image')->where('user_id',$user->id)->get()->toArray();
            $buffer_point = \DB::table('account')->select('buffer_point')->where('user_id',$user->id)->first();
            if(!empty($wishlist_data))
            {
                if($wishlist_data[0]->total_price!=null)
                {
                    $total_price = $wishlist_data[0]->total_price;
                }
                else
                {
                    $total_price = 0;
                }
            }
            $data = array(
                'user_name' => $user->user_name,
                'mobile'=>(is_null($user->mobile))?'':$user->mobile,
                'digital_key'=>$user->quiz_key,
                'current_balance'=>$wishlist_data[0]->bal,
                'buffer_balance'=>$buffer_point->buffer_point,
                'wishlist_items'=>$wishlist_data[0]->wishlist_items,
                'wishlist_products'=>$product_details,
                'total_wishlist_price'=>$total_price,
                'current_week'=>array('total_credit_points'=>0,'total_debit_points'=>0),
                'previous_week'=>array('total_credit_points'=>0,'total_debit_points'=>0),
                'last_week'=>array('total_credit_points'=>0,'total_debit_points'=>0),
                );
            $transaction_data = \DB::select("SELECT * FROM `get_user_last_trans_data` WHERE user_id=$user->id");
           foreach($transaction_data as $transaction_data)
           {
               if($transaction_data->week_created_at==$current_week)
               {
                   
                    if($transaction_data->transaction_type==1)
                    {
                       $data['current_week']['total_credit_points']=$transaction_data->total;
                    }
                    elseif($transaction_data->transaction_type==2)
                    {
                        $data['current_week']['total_debit_points']=$transaction_data->total;
                    }
                   
               }
               if($transaction_data->week_created_at==$previous_week)
               {
                   
                   if($transaction_data->transaction_type==1)
                    {
                       $data['previous_week']['total_credit_points']=$transaction_data->total;
                    }
                    elseif($transaction_data->transaction_type==2)
                    {
                        $data['previous_week']['total_debit_points']=$transaction_data->total;
                    }
               }
               if($transaction_data->week_created_at==$last_week)
               {
                   
                    if($transaction_data->transaction_type==1)
                    {
                       $data['last_week']['total_credit_points']=$transaction_data->total;
                    }
                    elseif($transaction_data->transaction_type==2)
                    {
                        $data['last_week']['total_debit_points']=$transaction_data->total;
                    }
               }
           }
            return Response::json(array('status'=> 'success','message'=>'Child details','data'=>[$data]))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }else{
            return Response::json(array('status'=> 'error','message'=>'Not a valid user type for this request.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    } catch(QueryException $ex){
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function deviceRegisterFcm(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'token' => 'required',
                'device_secret' => 'required',
                'device_type' => 'required|numeric|max:2'
            ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                if(!DB::table('users_device')->where(['device_type' => $request->input('device_type'),'device_secret' => $request->input('device_secret')])->exists())
                {
                    $data = array(
                        'token' => $request->input('token'),
                        'device_secret' => $request->input('device_secret'),
                        'device_type' => $request->input('device_type')
                    );
                    $result = DB::table('users_device')->insert($data);
                    if($result)
                    {
                        return Response::json(array('status'=> 'success','message'=>'Device registred suucessfully'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);
                    }
                    else
                    {
                       return Response::json(array('status'=> 'error','message'=>'Something error please try again'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                    }
                }
                else
                {
                    DB::table('users_device')->where(['device_type' => $request->input('device_type'),'device_secret' => $request->input('device_secret')])->update(['token'=>$request->token]);
                    return Response::json(array('status'=> 'success','message'=>'Device token updated successfully'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);   
                }
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        
    }
    
    public function sendUserNotification(Request $request)
    {
        $message = [
        'title' => 'welcome4',
        'body' => 'abc'
        ];

        $device_id = 'dcbGvfzXTOs:APA91bE5QQRl__klK-DaBooMGLmYd1V0NuqGbhQF6d5IUF9EQTliR9_M95ymS3ozxp_ROmrnsyaX5dslzXiNdx5bNuXh3mRULAIle59m9owwgkzC3PdkCbNqKWvg_BqvfeqemDRNHnqPiuYLR9hrsmlAHFCSpMvKmQ';
        $url = 'https://fcm.googleapis.com/fcm/send';
	    $msg = array(
            'to'  			=>  $device_id,
            'notification'  =>  $message,
            'data' 			=>  $message
        );
        $headers = array(
            'Authorization: key=AIzaSyDm5Q08LbJsFitZRdWI-a5gqn5kUXGwVII',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($msg));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        //echo '<pre>'; print_r($result);die;
        return  $result;
    }

    public function childList(Request $request)
    {
       try
        {
            $user = Auth::user();
            $child_data = \DB::table('user_notification as un')->select('un.id as id','un.request_type','un.request_status','un.points','users.id as student_id','users.name','users.dob','users.mobile','users.email','users.avtar','account.current_point','account.buffer_point','account.account_type','un.created_at as requested_time')->join('users', 'users.id', '=', 'un.from_id')->join('account', 'account.user_id', '=', 'users.id')->where('account.account_type','1')->where('un.to_id',$user->id)->where('un.request_type','1')->get()->toArray();
            
            $details = \DB::table('user_notification as un')->select('un.id as id','un.request_type','un.request_status','un.points','users.id as student_id','users.name','users.dob','users.mobile','users.email','users.avtar','account.current_point','account.buffer_point','account.account_type','un.created_at as requested_time')->join('users', 'users.id', '=', 'un.to_id')->join('account', 'account.user_id', '=', 'users.id')->where('account.account_type','1')->where('un.from_id',$user->id)->where('un.request_type','1')->get()->toArray();
            $data = array_merge($details,$child_data);
            return Response::json(array('status'=> 'success','message'=>'All Child Details','data'=>$data))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        } 
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    public function getAllParents(Request $request)
    {
       try
        {
            $parent_list = \DB::table('users')->select('id as parent_id','name')->where('type','3')->get();
            return Response::json(array('status'=> 'success','message'=>'All Parents','data'=>$parent_list))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        } 
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    public function getAllChild(Request $request)
    {
       try
        {
            $student_list = \DB::table('users')->select('id as child_id','name','email','mobile')->where('type','2')->where('parent_id','0')->get();
            return Response::json(array('status'=> 'success','message'=>'All Childs','data'=>$student_list))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        } 
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function tagUser(Request $request)
    {
       try
        {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                $check=User::where('id',$request->user_id)->exists();
                if(!$check)
                {
                   return Response::json(array('status'=> 'error','message'=>'User not exist Please try again'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
               }
                $user = Auth::user();
                if($user->type=='3')
                {
                    $student_id = $request->user_id;
                    $check_user=User::where(['id'=>$student_id,'type' => '2'])->exists();
                    if(!$check_user)
                    {
                        return Response::json(array('status'=> 'error','message'=>'This user is not child please enter correct id.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                    }
                    if($request->has('resend'))
                    {
                        
                       UserNotification::where(['from_id'=>$user->id,'to_id' => $student_id,'request_type' => '1'])->update(['request_status'=>'1']);
                       return Response::json(array('status'=> 'success','message'=>'Invitation resend successfully'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                    }
                    /*$check=User::where('parent_id','!=','0')->where('id',$student_id)->exists();*/
                    if(UserNotification::where(['from_id'=>$user->id,'to_id' => $student_id,'request_type' => '1'])->exists())
                    {
                        return Response::json(array('status'=> 'error','message'=>'Child already request for tag'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                    }
                    /*if($check)
                    {
                       return Response::json(array('status'=> 'error','message'=>'Child already added to another parent'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                    }*/
                    $check_user=UserNotification::where(['to_id'=>$user->id,'from_id' => $student_id,'request_type' => '1'])->exists();
                    if($check_user)
                    {
                        return Response::json(array('status'=> 'error','message'=>'Child already requested for tagging.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                    }
                        $data = array(
                            'from_id' => $user->id,
                            'to_id' => $student_id,
                            'request_type' => '1',
                        );
                        UserNotification::insert($data);
                        
                        $notification_data = array(
                            'from_id' => $user->id,
                            'to_id' => $student_id,
                            'message' => "$user->user_name invited you to tag as a child",
                        );
                        \DB::table('notifications')->insert($notification_data);
                        
                        return Response::json(array('status'=> 'success','message'=>'Child invitation sent successfully.')) ->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                if($user->type=='2')
                {
                    /*if(User::where('id',$user->id)->where('parent_id','!=','0')->exists())
                    {
                        return Response::json(array('status'=> 'error','message'=>'You already tagged with another parent'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                    }*/
                    $parent_id = $request->user_id;
                    $check_user=User::where(['id'=>$parent_id,'type' => '3'])->exists();
                    if(!$check_user)
                    {
                        return Response::json(array('status'=> 'error','message'=>'This user is not parent please enter correct id.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                    }
                    if($request->has('resend'))
                    {
                       UserNotification::where(['from_id'=>$user->id,'to_id' => $parent_id,'request_type' => '1'])->update(['request_status'=>'1']);
                       return Response::json(array('status'=> 'success','message'=>'Invitation resend successfully'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                    }
                    $check_user=UserNotification::where(['from_id'=>$user->id,'to_id' => $parent_id,'request_type' => '1'])->exists();
                    if($check_user)
                    {
                     return Response::json(array('status'=> 'error','message'=>'You already request parent for Tag.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);   
                    }
                    $check_user=UserNotification::where(['to_id'=>$user->id,'from_id' => $parent_id,'request_type' => '1'])->exists();
                    if($check_user)
                    {
                        return Response::json(array('status'=> 'error','message'=>'Parent already requested for tagging.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                    }
                    $data = array(
                        'from_id' => $user->id,
                        'to_id' => $parent_id,
                        'request_type' => '1',
                    );
                    UserNotification::insert($data);
                    
                    $notification_data = array(
                            'from_id' => $user->id,
                            'to_id' => $parent_id,
                            'message' => "$user->user_name invited you to tag as a parent",
                        );
                    \DB::table('notifications')->insert($notification_data);
                    return Response::json(array('status'=> 'success','message'=>'Parent invitation sent successfully.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                
            }
            
        } 
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function searchUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'mobile'      => 'required|numeric',
        'type'      => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $errorMsg = $validator->getMessageBag()->toArray();
            return Response::json(array('status'=>'error','message'=>$errorMsg)) ->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        else
        { 
            try
            { 
                if($request->type=='2')
                {
                     $data = User::select('id','user_name','name','mobile','avtar','email','dob','type')->where('mobile',$request->mobile)->where('type','2')->first();
                }
                if($request->type=='3')
                {
                    $data = User::select('id','name','user_name','mobile','avtar','email','dob','type')->where('mobile',$request->mobile)->where('type','3')->first();
                }
                if(count($data)>0)
                {
                  return Response::json(array('status'=>'success','message'=>'User Details','data'=>$data))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);   
                }
                return Response::json(array('status'=>'error','message'=>'No data Found'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            } 
            catch(QueryException $ex){ 
              return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
        }
    }

    public function addRequest(Request $request)
    {
       try
        {
            $user = Auth::user();
            $parent_id = $request->parent_id;
            $validator = Validator::make($request->all(), [
            'request_type'      => 'required|numeric',
            'parent_id'         => 'required|numeric',
            ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                $check=User::where('id',$request->parent_id)->exists();
                if(!$check)
                {
                   return Response::json(array('status'=> 'error','message'=>'Parent User not exist'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                if($request->request_type == '1')
                {
                    $check=UserNotification::where(['from_id'=>$user->id,'request_type' => '1'])->exists();
                    if($check)
                    {
                     return Response::json(array('status'=> 'error','message'=>'You already request parent for Tag.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);   
                    }
                    else
                    {
                        $data = array(
                            'from_id'       => $user->id,
                            'to_id'         => $parent_id,
                            'request_type'  => $request->request_type,
                        );
                        UserNotification::insert($data);
                        return Response::json(array('status'=> 'success','message'=>'Request sent to parent'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                    }
                }
                if($request->request_type == '2')
                {
                    $validator = Validator::make($request->all(), [
                    'points'      => 'required|numeric',
                    ]);
                    if ($validator->fails()) {
                        $errorMsg = $validator->getMessageBag()->toArray();
                        return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                    }
                    else
                    {
                        $data = array(
                            'from_id'       => $user->id,
                            'to_id'         => $parent_id,
                            'request_type'  => $request->request_type,
                            'points'  => $request->points,
                        ); 
                        UserNotification::insert($data);
                        return Response::json(array('status'=> 'success','message'=>'Request sent to parent'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                    }
                }
                return Response::json(array('status'=> 'error','message'=>'Unknow request'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            
        } 
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function userResponse(Request $request)
    {
        try
        {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'user_id'           => 'required',
                'request_type'      => 'required',
                'request_status'    => 'required',
            ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                $user_id = $request->user_id;
                $check_user=User::where(['id'=>$user_id])->exists();
                if(!$check_user)
                {
                    return Response::json(array('status'=> 'error','message'=>'This user not exist.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                if($user->type=='3')
                {
                    if(!UserNotification::where([['to_id', $user->id],['from_id',$request->user_id],['request_type',$request->request_type]])->exists())
                    {
                      return Response::json(array('status'=> 'error','message'=>'There is no such type of request to this user.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                    }
                    if($request->request_type=='1')
                    {
                        if($request->request_status=='2')
                        {
                            
                            UserNotification::where([['to_id', $user->id],['from_id',$request->user_id],['request_type',$request->request_type]])->update(['request_status'=>$request->request_status]);
                            UserNotification::where([['from_id', $user->id],['to_id',$request->user_id],['request_type',$request->request_type]])->update(['request_status'=>$request->request_status]);
                            //User::where('id',$request->user_id)->update(['parent_id'=>$user->id]);
                            $parent_data = array(
                                'user_id' => $request->user_id,
                                'parent_id' => $user->id
                            );
                            \DB::table('tag_parent')->insert($parent_data);
                            $notification_data = array(
                                'from_id' => $user->id,
                                'to_id' => $request->user_id,
                                'message' => "$user->user_name Accepted your tagging request ",
                            );
                            \DB::table('notifications')->insert($notification_data);
                            return Response::json(array('status'=>'success','message'=>'Request accepted'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                        }
                        if($request->request_status=='3')
                        {
                          UserNotification::where([['to_id', $user->id],['from_id',$request->user_id],['request_type',$request->request_type]])->update(['request_status'=>$request->request_status]);
                          UserNotification::where([['from_id', $user->id],['to_id',$request->user_id],['request_type',$request->request_type]])->update(['request_status'=>$request->request_status]);
                          $notification_data = array(
                                'from_id' => $user->id,
                                'to_id' => $request->user_id,
                                'message' => "$user->user_name Declined your tagging request ",
                            );
                            \DB::table('notifications')->insert($notification_data);
                          return Response::json(array('status'=>'success','message'=>'Request Declined'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                        }
                    }
                    if($request->request_type=='2')
                    {
                        if($request->request_status=='2')
                        {
                            
                            // UserNotification::->where([['from_id', $user->id],['to_id',$request->user_id],['request_type',$request_type]])->update($update_data);
                            // User::where('id',$request->user_id)->update(['parent_id', $user->id]);
                            // return Response::json(array('status'=>'success','message'=>'Request accepted')) ; 
                        }
                        if($request->request_status=='3')
                        {
                          UserNotification::where([['to_id', $user->id],['from_id',$request->user_id],['request_type',$request->request_type]])->update(['request_status'=>$request->request_status]);
                          return Response::json(array('status'=>'success','message'=>'Request Declined'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                        }  
                    }
                }
                if($user->type=='2')
                {
                    if(!UserNotification::where([['to_id', $user->id],['from_id',$request->user_id],['request_type',$request->request_type]])->exists())
                    {
                      return Response::json(array('status'=> 'error','message'=>'There is no such type of request to this user.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                    }
                    if($request->request_type=='1')
                    {
                        /*$check=User::where('parent_id','!=','0')->where('id',$user->id)->exists();
                        if($check)
                        {
                           return Response::json(array('status'=>'error','message'=>'You already tagged with another parent'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                        }*/
                        if($request->request_status=='2')
                        {
                            UserNotification::where([['to_id', $user->id],['from_id',$request->user_id],['request_type',$request->request_type]])->update(['request_status'=>$request->request_status]);
                            UserNotification::where([['from_id', $user->id],['to_id',$request->user_id],['request_type',$request->request_type]])->update(['request_status'=>$request->request_status]);
                            $parent_data = array(
                                'user_id' => $user->id,
                                'parent_id' => $request->user_id
                            );
                            \DB::table('tag_parent')->insert($parent_data);
                            /*$parent_id = array(
                                'parent_id' => $request->user_id
                            );
                            User::where('id',$user->id)->update($parent_id);*/
                            $notification_data = array(
                                'from_id' => $user->id,
                                'to_id' => $request->user_id,
                                'message' => "$user->user_name Accepted your tagging request ",
                            );
                            \DB::table('notifications')->insert($notification_data);
                            return Response::json(array('status'=>'success','message'=>'Request accepted'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                        }
                        if($request->request_status=='3')
                        {
                          UserNotification::where([['to_id', $user->id],['from_id',$request->user_id],['request_type',$request->request_type]])->update(['request_status'=>$request->request_status]);
                          UserNotification::where([['from_id', $user->id],['to_id',$request->user_id],['request_type',$request->request_type]])->update(['request_status'=>$request->request_status]);
                          $notification_data = array(
                                'from_id' => $user->id,
                                'to_id' => $request->user_id,
                                'message' => "$user->user_name Declined your tagging request ",
                            );
                            \DB::table('notifications')->insert($notification_data);
                          return Response::json(array('status'=>'success','message'=>'Request Declined'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                        }
                    }
                }
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        } 
    }

    public function getAllRequest(Request $request)
    {
       try
        {
            $user = Auth::user();
                if($user->type == '3')
                {
                   $details = \DB::table('user_notification as un')->select('un.id as id','un.request_type','un.request_status','un.points','users.id as student_id','users.name')->join('users', 'users.id', '=', 'un.from_id')->where('un.to_id',$user->id)->get();
                    return Response::json(array('status'=> 'success','message'=>'All Request','data'=>$details))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                if($user->type == '2')
                {
                   $details = \DB::table('user_notification as un')->select('un.id as id','un.request_type','un.request_status','un.points','users.id as parent_id','users.name as parent_name')->join('users', 'users.id', '=', 'un.from_id')->where('un.to_id',$user->id)->get();
                    return Response::json(array('status'=> 'success','message'=>'All Request','data'=>$details))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                return Response::json(array('status'=> 'error','message'=>'Unknow user type'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            
        } 
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
     public function getAllNotifications(Request $request)
    {
       try
        {
            $user = Auth::user();
            $details = \DB::table('notifications as un')->select('un.id as id','users.name as from','user2.name as To','un.message')->join('users', 'users.id', '=', 'un.from_id')->join('users as user2', 'user2.id', '=', 'un.to_id')->where('un.to_id',$user->id)->get();
            //print_r($details); die;
            return Response::json(array('status'=> 'success','message'=>'All notifications','data'=>$details))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                     ]);
        } 
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function changePassword(Request $request)
    {
        try
        {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'new_password'      => 'required',
                're_password'      => 'required',
            ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                if($request->new_password != $request->re_password)
                {
                    return Response::json(array('status'=> 'error','message'=>'Password and confirm password not matched.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                else
                {
                    $user = User::find($user->id);
                    //return $user;
                    $user->password =  Hash::make($request->new_password);
                    if($user->save())
                    {
                        return Response::json(array('status'=> 'success','message'=>'Password updated successfully.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                    } 
                    else{
                        return Response::json(array('status'=> 'error','message'=>'Failed to update password.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                    }
               }
                //return $current_password;
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function updateProfilePic(Request $request)
    {
        try
        {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'picture'      => 'required|image| max:1000',
                //'re_password'      => 'required',
            ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                $user = Auth::user();
                $user = User::find($user->id);
                if ($request->hasFile('picture'))
                {
                    $image = $request->file('picture');
                    $path = storage_path('app/profile');
                    $filename = time() . '.' . $image->getClientOriginalExtension();
                    $type       = $image->getClientOriginalExtension();
                    $allowed =  array('jpg','png','jpeg');
                    if(!in_array($type,$allowed) ) {
                        return Response::json(array('status'=> 'error','message'=>'Please select only image jpg,png,jpeg format.')) ->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                    }
                    $image->move($path, $filename);

                    $user->avtar = $filename;

                }
                if($user->save())
                {
                    $data = User::select('avtar')->where('id',$user->id)->first();
                    return Response::json(array('status'=> 'success','message'=>'Profile Pic Updated Successfully.','data'=>$data))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                } 
                else{
                    return Response::json(array('status'=> 'error','message'=>'Failed to update.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }

    }

    // public function checkUserExist(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'mobile'      => 'required|numeric',
    //     ]);
    //     if ($validator->fails()) {
    //         $errorMsg = $validator->getMessageBag()->toArray();
    //         return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
    //                         'Content-Type' => 'application/json',
    //                         'Accept' => 'application/json',
    //                      ]);
    //     }
    //     try
    //     {
                
    //         $check=User::where(['mobile'=>$request->input('mobile')])->exists();
    //         if($check)
    //         {
                
    //             return Response::json(array('status'=> 'success','message'=>'We have sent an OTP please check!'))->withHeaders([
    //                         'Content-Type' => 'application/json',
    //                         'Accept' => 'application/json',
    //                      ]);
    //         }
    //         else
    //         {
    //             return Response::json(array('status'=> 'error','message'=>'Contact not exist please try again with correct number.'))->withHeaders([
    //                         'Content-Type' => 'application/json',
    //                         'Accept' => 'application/json',
    //                      ]);
    //         }
    //     }
    //     catch(QueryException $ex){ 
    //       return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
    //                         'Content-Type' => 'application/json',
    //                         'Accept' => 'application/json',
    //                      ]);
    //     }
    // }
    
    public function checkUserExist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_name'      => 'required',
        ]);
        if ($validator->fails()) {
            $errorMsg = $validator->getMessageBag()->toArray();
            return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        try
        {
                
            $check=User::select('mobile','country_code')->where(['user_name'=>$request->input('user_name')])->first();
            if(!empty($check))
            {
                if($check->mobile!='')
                {
                    $data = array(
                        'country_code'=>$check->country_code,
                        'mobile' => $check->mobile,
                        );
                    return Response::json(array('status'=> 'success','message'=>'','data'=>$data))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                else
                {
                    return Response::json(array('status'=> 'error','message'=>'Mobile number not updated for this username contact admin to reset your password.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                
            }
            else
            {
                return Response::json(array('status'=> 'error','message'=>'User name not exist please try with registered username.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'mobile'      => 'required|numeric',
                'otp'         => 'required',
        ]);
        if ($validator->fails()) {
            $errorMsg = $validator->getMessageBag()->toArray();
            return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        else
        {
            $check=User::where(['mobile'=>$request->input('mobile')])->exists();
            if($check)
            {
                $data = User::select('id')->where('mobile',$request->mobile)->first();
                $user = User::find($data->id);
                $user->password =  Hash::make($request->otp);
                if($user->save())
                {
                    return Response::json(array('status'=> 'success','message'=>'Password Changed successfully.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                } 
                else{
                    return Response::json(array('status'=> 'error','message'=>'Failed to change password. Please try again !.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }

            } 
            return Response::json(array('status'=> 'error','message'=>'User not exist please try again with registered number'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
        }
    }

    // public function forgotPassword(Request $request)
    // {
    //     try
    //     {
    //         $user = Auth::user();
    //         $validator = Validator::make($request->all(), [
    //             //'contact'      => 'required|numeric',
    //             'user_name'      => 'required',
    //         ]);
    //         if ($validator->fails()) {
    //             $errorMsg = $validator->getMessageBag()->toArray();
    //             return Response::json(array('status'=>'error','message'=>$errorMsg)) ;
    //         }
    //         else
    //         {
    //             $type = filter_var($request->input('user_name'), FILTER_VALIDATE_EMAIL)
    //             ? 'email'
    //             : 'mobile';

    //             $request->merge([
    //             $type => $request->input('user_name')
    //             ]);
    //             if($type=='email')
    //             {
    //                 $validator = Validator::make($request->all(), [
    //                     //'contact'      => 'required|numeric',
    //                     'user_name'      => 'email',
    //                 ]);
    //                 if ($validator->fails()) {
    //                     $errorMsg = $validator->getMessageBag()->toArray();
    //                     return Response::json(array('status'=>'error','message'=>$errorMsg)) ;
    //                 }
    //                 else
    //                 {
    //                     $check=User::where(['email'=>$request->input('user_name')])->exists();
    //                     if($check)
    //                     {
    //                         $email_sent = $this->sendResetLinkEmail($request);
    //                         if($email_sent)
    //                         {
    //                           return Response::json(array('status'=> 'success','message'=>'We have e-mailed your password reset link!')) ;  
    //                         }
    //                     }
    //                     else
    //                     {
    //                         return Response::json(array('status'=> 'error','message'=>'Email not registered please try again with correct Email-Id.')) ;
    //                     }
    //                 }
    //             }
    //             $validator = Validator::make($request->all(), [
    //                     //'contact'      => 'required|numeric',
    //                     'user_name'      => 'numeric',
    //                 ]);
    //             if ($validator->fails()) {
    //                 $errorMsg = $validator->getMessageBag()->toArray();
    //                 return Response::json(array('status'=>'error','message'=>$errorMsg)) ;
    //             }
    //             else
    //             {
    //                 $check=User::where(['mobile'=>$request->input('user_name')])->exists();
    //                 if($check)
    //                 {
    //                     // $email_sent = $this->sendResetLinkEmail($request);
    //                     // if($email_sent)
    //                     // {
    //                       return Response::json(array('status'=> 'success','message'=>'We have sent an OTP please check!')) ;  
    //                     // }
    //                 }
    //                 else
    //                 {
    //                     return Response::json(array('status'=> 'error','message'=>'Contact not exist please try again with correct number.')) ;
    //                 }
    //             }
                
    //         }
    //     }
    //     catch(QueryException $ex){ 
    //       return Response::json(array('status'=> 'error','message'=>$ex->getMessage())) ;
    //     }
    // }
    public function payment_sub(Request $request)
    {
        $admin_id = $_ENV['ADMIN_ID'];
        $account_type = $_ENV['ACCOUNT_TYPE'];
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
                'card_no' => 'required',
                'ccExpiryMonth' => 'required',
                'ccExpiryYear' => 'required',
                'cvvNumber' => 'required',
                'package_id' => 'required',
        ]);
        if ($validator->fails()) {
            $errorMsg = $validator->getMessageBag()->toArray();
            return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        else
        {
            $stripe = Stripe::make('sk_test_SIc5YNIBhQ3hIfKW5e5XrD7X');
            $token = $stripe->tokens()->create([
                    'card' => [
                        'number'    => $request->get('card_no'),
                        'exp_month' => $request->get('ccExpiryMonth'),
                        'exp_year'  => $request->get('ccExpiryYear'),
                        'cvc'       => $request->get('cvvNumber'),
                    ],
                ]);
            //return $token;
            if (!isset($token['id'])) {
                return Response::json(array('status'=>'error','message'=>'The Stripe token was not generated correctly. Please retry.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                     ]);
            }
            $user = Auth::user();
            //return $user;
            $plan_data = array('gold' => 'Gold');
            /*$data = array(
                'plan' => $plan_data
            );*/
            try {
                //$user->subscription($data)->create($token,[
                        //'email' => $user->email
                    //]);
                $user->newSubscription('main', 'plan_DfEXfnZm39pngU')->create($token['id']);
                return 'success';
            } catch (Exception $e) {
                return back()->with('success',$e->getMessage());
            }
            
            //return back()->with('success','Subscription is completed.');
            //$user->newSubscription('main', 'Gold')->create($token['id'],[]);
        }
    }


    public function stripePayment(Request $request)
    {
        $admin_id = $_ENV['ADMIN_ID'];
        $account_type = $_ENV['ACCOUNT_TYPE'];
        $user = Auth::user();
        if($user->type!='3')
        {
            return Response::json(array('status'=>'error','message'=>'Please login as parent for payment.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        $validator = Validator::make($request->all(), [
                'card_no'       => 'required',
                'ccExpiryMonth' => 'required',
                'ccExpiryYear'  => 'required',
                'cvvNumber'     => 'required',
                'package_id'    => 'required',
                'plan_type'     =>'required',
        ]);
        if ($validator->fails()) {
            $errorMsg = $validator->getMessageBag()->toArray();
            return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        else
        {
            $check = \DB::table('user_packages')->where('id',$request->package_id)->exists();
            if(!$check)
            {
             return Response::json(array('status'=>'error','message'=>'Plan not exist try again'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);   
            }
            if($request->plan_type<'1' || $request->plan_type>'2')
            {
             return Response::json(array('status'=>'error','message'=>'Plan Type only allowed 1 or 2.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);   
            }
            $input = $request->all();
            $stripe = Stripe::make('sk_test_SIc5YNIBhQ3hIfKW5e5XrD7X');
            $plan_data = \DB::table('user_packages')->where('id',$request->package_id)->first();
            /*$child_data = DB::table('users')->whereRaw("FIND_IN_SET($user->id,parent_id)")->get();
            $child_data = $child_data->map(function ($value) {
                $data['child_id'] = $value->id;
                $data['user_name'] = $value->user_name;
                $data['avtar'] = $value->avtar;
                return $data;
            });
            $child_data->all();*/
            $parent_data = \DB::table('tag_parent')->select(\DB::raw('group_concat(user_id) as user_ids'))->where('parent_id',$user->id)->get();
            $child_data = array('');
            if(!empty($parent_data))
            {
                $parent_ids = $parent_data[0]->user_ids;
                $user_ids = explode(',',$parent_ids); 
                $child_data = User::select('users.id as id','users.user_name','users.name','users.mobile','users.email','users.avtar')->whereIn('id',$user_ids)->get();
                $child_data = $child_data->map(function ($value) {
                    $data['child_id'] = $value->id;
                    $data['child_user_name'] = $value->user_name;
                    $data['avtar'] = $value->avtar;
                    return $data;
                });
                $child_data->all();
            }
            $total_child = count($child_data);
            //print_r($child_data); die;
            $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account WHERE dg_account.user_id = $user->id AND dg_account.account_type = 1) as current_account"))->where('user_id',$user->id)->first();
            $admin_data = Account::select('account_number')->where('user_id','1')->first();
            try 
            {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number'    => $request->get('card_no'),
                        'exp_month' => $request->get('ccExpiryMonth'),
                        'exp_year'  => $request->get('ccExpiryYear'),
                        'cvc'       => $request->get('cvvNumber'),
                    ],
                ]);
                if (!isset($token['id'])) {
                    return Response::json(array('status'=>'error','message'=>'The Stripe token was not generated correctly. Please retry.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                if($request->plan_type=='1')
                {
                    if($plan_data->stripe_plan_id!='')
                    {
                        $user = Auth::user();
                        if(DB::table('subscriptions')->where('user_id',$user->id)->exists())
                        {
                            
                            return Response::json(array('status'=>'error','message'=>'Already subscribed for another plan '))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                        }
                        //return 'asdasd';
                        try {
                            $status = $user->newSubscription('main', $plan_data->stripe_plan_id)->create($token['id']);
                            if(!empty($status))
                            {
                                $data = array(
                                    'user_id' => $user->id,
                                    'from_id' => $admin_data->account_number,
                                    'to_id' => $user_data->current_account,
                                    'type'=>'4',
                                    'transaction_for_id'=>$plan_data->id,
                                    'account_type' => $account_type,
                                    'transaction_points' => $plan_data->recurring_points,
                                    'transaction_type'  => '1',
                                    'remarks' => 'Payed '.$plan_data->recurring_amount.' for '.$plan_data->name.' plan. '.$plan_data->recurring_points.' points added to wallet.',
                                );
                                DB::table('transaction')->insert($data);
                                return Response::json(array('status'=>'success','message'=>'Points added successfully in wallet and subscribed for this plan','total_child'=>$total_child,'child_data'=>$child_data))->withHeaders([
                                        'Content-Type' => 'application/json',
                                        'Accept' => 'application/json',
                                     ]);
                            }
                        } catch (Exception $e) {
                            return Response::json(array('status'=>'error','message'=>$e->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                        }
                    }
                    else
                    {
                       return Response::json(array('status'=>'error','message'=>'The Package plan not allowed for recurring please check.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                    }
                }
                else
                {
                    if($plan_data->one_time_amount!='')
                    {
                        $charge = $stripe->charges()->create([
                            'card' => $token['id'],
                            'currency' => $plan_data->currency,
                            'amount'   => $plan_data->one_time_amount,
                            'description' => 'Add in wallet',
                        ]);
                        // print_r($charge);
                        // die;
                        if($charge['status'] == 'succeeded') {
                            $data = array(
                                'user_id' => $user->id,
                                'from_id' => $admin_data->account_number,
                                'to_id' => $user_data->current_account,
                                'type'=>'4',
                                'transaction_for_id'=>$plan_data->id,
                                'account_type' => $account_type,
                                'transaction_points' => $plan_data->one_time_points,
                                'transaction_type'  => '1',
                                'remarks' => 'Payed '.$plan_data->one_time_amount.' for '.$plan_data->name.' plan. '.$plan_data->one_time_points.' points added to wallet.',
                            );
                            DB::table('transaction')->insert($data);
                            return Response::json(array('status'=>'success','message'=>'Points added successfully in wallet','total_child'=>$total_child,'child_data'=>$child_data))->withHeaders([
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                 ]);
                        } else {
                            return Response::json(array('status'=>'error','message'=>'Money not added in wallet. Please try again'))->withHeaders([
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                 ]);
                        }
                    }
                    else
                    {
                      return Response::json(array('status'=>'error','message'=>'This Plan not for topup please try again'))->withHeaders([
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                 ]);  
                    }
                }
            }
            catch (Exception $e) {
                return Response::json(array('status'=>'error','message'=>$e->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
                return Response::json(array('status'=>'error','message'=>$e->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                return Response::json(array('status'=>'error','message'=>$e->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            catch(\Cartalyst\Stripe\Exception\UnauthorizedException $e) {
                return Response::json(array('status'=>'error','message'=>$e->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }

        }
    }
    
    public function userProductRedemption(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'product_id' => 'required|numeric',
            ]);
        if ($validator->fails()) {
            $errorMsg = $validator->getMessageBag()->toArray();
            return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        else
        {
            $user = Auth::user();
            if(!AuctionProducts::join('auction_categories as ac', 'ac.id', '=', 'auction_products.auction_category_id')->join('auction_category_country as acc','acc.auction_category_id','=','ac.id')->where(['auction_products.id'=>$request->product_id,'ac.status'=>'1','acc.country_id'=>$user->country_id])->exists())
            {
                return Response::json(array('status'=> 'error','message'=>'Product not Active to add in wishlist.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                     ]);  
            }
            $check=UserWishlist::where(['user_id'=>$user->id,'product_id'=>$request->product_id])->exists();
            if($check)
            {
               $current_points = DB::select(DB::raw("Select get_balance(".$user->id.",1 ) AS bal"));
                $current_point=$current_points[0]->bal; 
                $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = $user->id AND dg_account.account_type = 1
                        ) as current_account"))->where('user_id',$user->id)->first();
                $admin_data = Account::select(DB::raw("(SELECT account_number FROM dg_account
                          WHERE dg_account.user_id = '1' AND dg_account.account_type = 1
                        ) as admin_account"))->where('user_id','1')->first();
                $product_data = UserWishlist::join('auction_products', 'auction_products.id', '=', 'user_wishlist.product_id')->select('auction_products.rdm_price as base_price')->where(['user_wishlist.product_id'=>$request->product_id,'user_id'=>$user->id])->first();
                if(!empty($product_data))
                {
                    if($product_data->base_price=='0')
                    {
                        return Response::json(array('status'=> 'error','message'=>'Not allowed for redemption.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        ]);
                    }
                    if($product_data->base_price<=$current_point)
                    {
                        $data = array(
                            'user_id' => $user->id,
                            'from_id' => $user_data->current_account,
                            'to_id' => $admin_data->admin_account,
                            'type'=>'7',
                            'transaction_for_id'=>$request->product_id,
                            'account_type' =>'1' ,
                            'transaction_points' =>$product_data->base_price,
                            'transaction_type'  => '2',
                            'remarks' => "Product redemption Amount deduction by $user->name",
                        );
                        DB::table('transaction')->insert($data);
                        return Response::json(array('status'=> 'success','message'=>'Congratulations you have successfully reedem this product.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        ]);
                    }
                    else
                    {
                        return Response::json(array('status'=> 'error','message'=>'you do not have enough current points to reedem this product.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        ]);
                    }
                }
                else
                {
                   return Response::json(array('status'=> 'error','message'=>'Please try again there is some issue.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                     ]); 
                }
            }
            else
            {
                return Response::json(array('status'=> 'error','message'=>'Product not added in your wishlist please add first.'))->withHeaders([
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                     ]); 
            }
        }
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }
        //
    /**
     * Store a newly created resource in storage.
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        try
        {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'name'          => 'min:3',
                'dob'           => 'date',
            ]);
            if ($validator->fails()) {
                $errorMsg = $validator->getMessageBag()->toArray();
                return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
                $user = Auth::user();
                $id = $user->id;
                    $user = User::find($id);
                    //return $user;

                    $user->name = $request->name;
                    if($request->has('email'))
                    {
                        if (!filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
                            return Response::json(array('status'=> 'error','message'=>'Enter proper email address.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            ]);
                        }
                       $user->email = $request->email;
                    }
                    if($request->has('mobile'))
                    {
                        if(!is_numeric($request->input('mobile')))
                        {
                           return Response::json(array('status'=> 'error','message'=>'Mobile number must be a number.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            ]); 
                        }
                       $user->mobile = $request->mobile; 
                    }
                    if($request->has('school_name'))
                    {
                       $user->school_name = $request->school_name; 
                    }
                    if($request->has('gender'))
                    {
                       $user->gender = $request->gender;
                       if($request->gender=='1')
                       {
                           $user->avtar = 'male.png';
                       }
                       elseif($request->gender=='2')
                       {
                           $user->avtar = 'female.png';
                       }
                       else
                       {
                           return Response::json(array('status'=> 'error','message'=>'Enter only value 1 and 2.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            ]);
                       }
                    }
                    if($request->has('dob'))
                    {
                        //echo $request->dob;die;
                        $dob = date('Y-m-d',strtotime($request->dob));
                        $user->dob = $dob; 
                    }
                    if($request->has('address'))
                    {
                       $user->address = $request->address; 
                    }
                    //$user->contact =$_POST['contact'];
                
                    if ($request->hasFile('picture'))
                    {
                        $image = $request->file('picture');
                        $path = storage_path('app/profile');
                        $filename = time() . '.' . $image->getClientOriginalExtension();
                        $image->move($path, $filename);
                        
                        $user->avtar = $filename;

                    }
                 
                    if($user->save())
                    {
                        return Response::json(array('status'=> 'success','message'=>'Profile Updated Successfully.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                    } 
                    else{
                        return Response::json(array('status'=> 'error','message'=>'Failed to update.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                    }
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAllPackages(Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }      
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }  
            
            $user = Auth::user();
            if($user->type=='3')
            {
                $packages = new UserPackages();
                DB::statement(DB::raw('set @rownumber='.$offset.''));
                $packages = $packages->select('user_packages.id','user_packages.name','user_packages.currency','user_packages.recurring_amount','user_packages.recurring_points','user_packages.one_time_amount','user_packages.one_time_points','user_packages.stripe_plan_id as plan_id')->join('package_country as pc','pc.package_id','=','user_packages.id');
                /*if ($request->has('plan_type')) {
                    if($request->input('plan_type')<1 || $request->input('plan_type')>2)
                    {
                        return Response::json(array('status'=> 'error','message'=>'Not allowed plan type allow only 1 & 2'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);
                    }
                    $packages = $packages->where('plan_type',$request->input('plan_type'));
                }*/
                $packages = $packages->where('status','1')->where('pc.country_id',$user->country_id);
                $total = $packages->count();
                $packages = $packages->offset($offset)->limit($perpage)->get();
                $packages = $packages->map(function ($value) {
                
                    $data['id'] = $value->id;
                    $data['name'] = $value->name;
                    $data['recurring_amount'] = "";
                    $data['recurring_points'] = "";
                    $data['one_time_amount'] = "";
                    $data['one_time_points'] = "";
                    $data['plan_id'] = "";
                    $data['currency'] = $value->currency;
                    if($value->recurring_amount!=null)
                    {
                        $data['recurring_amount'] = $value->recurring_amount;
                    }
                    if($value->recurring_points!=null)
                    {
                        $data['recurring_points'] = $value->recurring_points;
                    }
                    if($value->one_time_amount!=null)
                    {
                        $data['one_time_amount'] = $value->one_time_amount;
                    }
                    if($value->one_time_points!=null)
                    {
                        $data['one_time_points'] = $value->one_time_points;
                    }
                    if($value->plan_id!=null)
                    {
                        $data['plan_id'] = $value->plan_id;
                    }
                    return $data; 
                });
                $packages->all();
                $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
                if($total>0)
                {
                    return Response::json(array('status'=> 'success','message'=>'All package details','total_packages'=>$total,'meta'=>$meta,'data'=>$packages ))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);
                }
                else
                {
                   return Response::json(array('status'=> 'success','message'=>'No Data Found','total_packages'=>$total,'meta'=>$meta,'data'=>$packages ))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                }
            }
            else
            {
                return Response::json(array('status'=> 'error','message'=>'No Data Found'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function getAllTransaction(Request $request)
    {
        try
        {
            $user = Auth::user();
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }      
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }  
            $currentMonth = date('m');
            $transaction = new Transaction();
            $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account  WHERE dg_account.user_id = $user->id AND dg_account.account_type = 1) as current_account"))->where('user_id',$user->id)->first();
            DB::statement(DB::raw('set @rownumber='.$offset.''));
            $transaction = $transaction->select('transaction.id as id','transaction.transaction_points','transaction.transaction_type','transaction.remarks','transaction.created_at as transaction_time')->where('transaction.from_id',$user_data->current_account)->orwhere('transaction.to_id',$user_data->current_account)->whereRaw('MONTH(dg_transaction.created_at) = ?',[$currentMonth]);
            $transaction = $transaction->offset($offset)->limit($perpage)->get();
            $total = $transaction->count();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            if($total>0)
            {
                return Response::json(array('status'=> 'success','message'=>'Account Statement','total_transactions'=>$total,'meta'=>$meta,'data'=>$transaction ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No Data Found','total_transactions'=>$total,'meta'=>$meta,'data'=>$transaction ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function getAllCreditTransaction(Request $request)
    {
        try
        {
            $user = Auth::user();
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }      
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }  
            $currentMonth = date('m');
            $transaction = new Transaction();
            $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account  WHERE dg_account.user_id = $user->id AND dg_account.account_type = 1) as current_account"))->where('user_id',$user->id)->first();
            DB::statement(DB::raw('set @rownumber='.$offset.''));
            $transaction = $transaction->select('transaction.id as transaction_id','transaction.type','transaction.transaction_points','transaction.remarks','transaction.created_at as transaction_time')->where('transaction.to_id',$user_data->current_account)->where('transaction.transaction_type','1');
            //whereRaw('MONTH(dg_transaction.created_at) = ?',[$currentMonth]);
            $transaction = $transaction->offset($offset)->limit($perpage)->get();
            $total = $transaction->count();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            if($total>0)
            {
                return Response::json(array('status'=> 'success','message'=>'User Earn list details','total_transactions'=>$total,'meta'=>$meta,'data'=>$transaction ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No Data Found','total_transactions'=>$total,'meta'=>$meta,'data'=>$transaction ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function getAllDebitTransaction(Request $request)
    {
        try
        {
            $user = Auth::user();
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }      
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }  
            $currentMonth = date('m');
            $transaction = new Transaction();
            $user_data = Account::select(DB::raw("(SELECT account_number FROM dg_account  WHERE dg_account.user_id = $user->id AND dg_account.account_type = 1) as current_account"))->where('user_id',$user->id)->first();
            DB::statement(DB::raw('set @rownumber='.$offset.''));
            $transaction = $transaction->select('transaction.id as transaction_id','transaction.type','transaction.transaction_points','transaction.remarks','transaction.created_at as transaction_time')->where('transaction.from_id',$user_data->current_account)->where('transaction.transaction_type','1');
            //whereRaw('MONTH(dg_transaction.created_at) = ?',[$currentMonth]);
            $transaction = $transaction->offset($offset)->limit($perpage)->get();
            $total = $transaction->count();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            if($total>0)
            {
                return Response::json(array('status'=> 'success','message'=>'User Spent list details','total_transactions'=>$total,'meta'=>$meta,'data'=>$transaction ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No Data Found','total_transactions'=>$total,'meta'=>$meta,'data'=>$transaction ))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }
    
    public function getTransactionDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'transaction_id' => 'required',
            ]);
        if ($validator->fails()) {
            $errorMsg = $validator->getMessageBag()->toArray();
            return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        else
        {
            try
            {
                
                $user = Auth::user();
                $transaction_id = $request->transaction_id;
                $transaction = new Transaction();
                $transaction = $transaction->where('transaction.id',$transaction_id)->first();
                //print_r($transaction); die;
                if(empty($transaction))
                {
                   return Response::json(array('status'=> 'error','message'=>'No Transaction exist please check id'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                }
                else
                {
                    switch($transaction->type)
                    {
                        case '1' :
                         $transaction_details = Transaction::select('transaction.transaction_points','transaction.type','transaction.remarks','transaction.created_at','utd.total_attempt','category.category_name','task.task_name')->join('user_task_data as utd', function($join)
                            {
                                $join->on('utd.task_id', '=', 'transaction.transaction_for_id');
                                $join->on('utd.user_id','=', 'transaction.user_id');
                            })->join('task','task.id','=','transaction.transaction_for_id')->join('category','category.id','=','task.category_id')->where('transaction.id',$transaction_id)->get();
                            break;
                        case '2' :
                            $transaction_details = Transaction::select('transaction.transaction_points','transaction.type','transaction.remarks','transaction.created_at','auction.single_bid_value','auction.start_time','auction.auction_closed_at','ac.category_name')->join('auction','auction.id','=','transaction.transaction_for_id')->join('auction_categories as ac','ac.id','=','auction.auction_category_id')->where('transaction.id',$transaction_id)->get();
                            break;
                        case '3' :
                            $transaction_details = Transaction::select('transaction.transaction_points','transaction.type','transaction.remarks','transaction.created_at','uld.total_attempt','lod.lesson_title','lod.file_attached','lod.link_url','lod.lesson_date','lod.skipped_time')->join('user_lesson_data as uld', function($join)
                            {
                                $join->on('uld.lesson_id', '=', 'transaction.transaction_for_id');
                                $join->on('uld.user_id','=', 'transaction.user_id');
                            })->join('lesson_of_day as lod','lod.id','=','transaction.transaction_for_id')->where('transaction.id',$transaction_id)->get();
                            break;
                        case '6' :
                            $transaction_details = Transaction::select('transaction.transaction_points','transaction.type','transaction.remarks','transaction.created_at')->where('transaction.id',$transaction_id)->get();
                            break;
                        case '7' :
                            $transaction_details = Transaction::select('transaction.transaction_points','transaction.type','transaction.remarks','transaction.created_at','ap.product_name','ap.product_price','ap.rdm_price')->join('auction_products as ap','ap.id','=','transaction.transaction_for_id')->join('auction_categories as ac','ac.id','=','ap.auction_category_id')->where('transaction.id',$transaction_id)->get();
                            break;
                    }
                    if(!empty($transaction_details))
                    {
                        return Response::json(array('status'=> 'success','message'=>'User transaction details','data'=>$transaction_details ))->withHeaders([
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                 ]);
                    }
                    else
                    {
                        $transaction_details = array();
                       return Response::json(array('status'=> 'success','message'=>'No Data Found','data'=>$transaction_details ))->withHeaders([
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                 ]);
                    }
                }
                
                
            }
            catch(QueryException $ex){ 
              return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);
            }
        }
    }
    
    public function userLogout(Request $request)
    {
        $user = Auth::user();
        if(Auth::check()) 
        {
            User::where('id', $user->id)
            ->update(['quiz_key' => '']);
            Auth::user()->AauthAcessToken()->delete();
            return Response::json(array('status'=> 'success','message'=>'Auth logout succesfully.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);
        }
        else
        {
            return Response::json(array('status'=> 'error','message'=>'Not Auth user check credentials.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);
        }
        
    }
    
    public function accountDetails()
    {
        try
        {
            $user = Auth::user();
            $pool_balance = '80';
            $account_details = Account::select('current_point','buffer_point',DB::raw("$pool_balance as pool_balance"))->where(['account_type'=>'1','user_id'=>$user->id])->get();
            return Response::json(array('status'=> 'success','message'=>"Account details of $user->name",'data'=>$account_details))->withHeaders([
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ]);

        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }

    }
}
