<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Account;
use App\Model\Auction;
use App\Model\User;
use App\Model\Task;
use App\Model\AuctionCategories;
use App\Model\AuctionProducts;
use App\Model\UserWishlist;
use Response;
use Validator;
use Illuminate\Database\QueryException;

class AuctionController extends Controller
{

    public function __construct()
    {
        //$this->middleware('guest');
    }
    /**
     * Display a listing of the Auction.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            //Check request has per page or not
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            //Check request has current page or not
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }
            //return $offset;
            $category = new AuctionCategories();
            $category = $category->select('id','category_name','base_price')->where('status','1');
            $total = $category->count();
            $category = $category->offset($offset)->limit($perpage)->get();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            //return $meta;
            //check data found or not
            if($total>0)
            {
                return Response::json(array('status'=> 'success','message'=>'Auction categories','meta'=>$meta,'data'=>$category))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No data found in auction','meta'=>$meta,'data'=>$category))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        //catch all exceptions
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function getAllAuctions(Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }
            //return $offset;
            $user = Auth::user();
            if($user->type==2)
            {
                //return $user->country_id;
                $auctions = new Auction();
                $user_id = $user->id;
                $auctions = $auctions->select(\DB::raw("$user_id as user_id"),'auction.id as auction_id','auction.auction_category_id','ac.category_name','auction.auction_price','auction.single_bid_value','auction.incremented_bid_time','auction.current_auction_value','auction.start_time','auction.end_time','abh.bid_hash','auction.last_bid_user')->join('auction_categories as ac','ac.id','=','auction.auction_category_id')->join('auction_category_country as acc','acc.auction_category_id','=','ac.id')->join('auction_bid_hash as abh','abh.auction_id','=','auction.id')->where(function ($query) {
                        $query->where('auction.status', '=', 1)
                              ->orWhere('auction.status', '=', 3);
                            })->where('acc.country_id',$user->country_id);
                            $total = $auctions->count();
                $auctions = $auctions->offset($offset)->limit($perpage)->get();
                //return $auctions;
                $auctions = $auctions->map(function ($value) use($user){
                    
                    $data['auction_id'] = $value->auction_id;
                    //$data['country_id'] = $value->country_id;
                    $data['category_name'] = $value->category_name;
                    $data['auction_price'] = $value->auction_price;
                    $data['single_bid_value'] = $value->single_bid_value;
                    $data['incremented_bid_time'] = $value->incremented_bid_time;
                    $data['current_auction_value'] = $value->current_auction_value;
                    /*$data['reedem_option_allow'] = '0';
                    $current_points = \DB::select(\DB::raw("Select get_balance(".$user->id.",1 ) AS bal"));
                    $current_point=$current_points[0]->bal;
                    $data['current_points'] = $current_point;
                    if($value->auction_price<=$current_point)
                    {
                        $data['reedem_option_allow'] = '1';
                    }*/
                    $data['start_time'] = $value->start_time;
                    $data['end_time'] = $value->end_time;
                    $data['bid_hash'] = $value->bid_hash;
                    $data['user_participated'] = '0';
                    $data['product_name'] = '';
                    $data['product_image'] = 'auction.png';
                    $wishlist_data = UserWishlist::select('user_wishlist.id as id','user_wishlist.product_id as product_id','user_wishlist.product_name','user_wishlist.product_image')->join('auction_products as pd','pd.id','=','user_wishlist.product_id')->where(['pd.auction_category_id'=>$value->auction_category_id,'user_wishlist.user_id'=>$value->user_id])->first();
                    if(count($wishlist_data)>0)
                    {
                       $data['product_name'] = $wishlist_data->product_name;
                        $data['product_image'] = $wishlist_data->product_image; 
                    }
                    $product_data = \DB::table('user_auction_products as uap')->select('ap.product_name','ap.product_image')->join('auction_products as ap','ap.id','=','uap.product_id')->where(['auction_id'=>$value->auction_id,'user_id'=>$value->user_id])->first();
                    $check = \DB::table('buffer_logs')->where(['auction_id'=>$value->auction_id,'from_id'=>$value->user_id,'to_id'=>'1'])->exists();
                    if(count($product_data)>0 && $check)
                    {
                        $data['user_participated'] = '1';
                        $data['product_name'] = $product_data->product_name;
                        $data['product_image'] = $product_data->product_image;
                    }
                    $data['last_bidder'] = '';
                    if(!$value->last_bid_user==null)
                    {
                        $user_id = $value->last_bid_user;
                        $user_name = User::select('user_name')->where('id',$user_id)->first();
                       $data['last_bidder'] = $user_name->user_name;
                    }
                    return $data; 
                    });
                $auctions->all();
                $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
                //return $meta;
                if($total>0)
                {
                    return Response::json(array('status'=> 'success','message'=>'Auction details','meta'=>$meta,'data'=>$auctions))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);
                }
                else
                {
                   return Response::json(array('status'=> 'success','message'=>'No data found','meta'=>$meta,'data'=>$auctions))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                }
            }
            else
            {
                return Response::json(array('status'=> 'error','message'=>'Please login as a child to participate in auction.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function getAllProducts(Request $request)
    {
        try
        {
            $user = Auth::user();
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }
            //return $offset;
            $products = new AuctionProducts();
            $products = $products->select('auction_products.id as id','auction_products.product_name','auction_products.product_image','auction_products.auction_category_id as category_id','ac.category_name','ac.base_price')->join('auction_categories as ac', 'ac.id', '=', 'auction_products.auction_category_id')->join('auction_category_country as acc','acc.auction_category_id','=','ac.id');
            if($user->type=='3')
            {
                if($request->child_id=='')
                {
                   return Response::json(array('status'=> 'error','message'=>'Child id required.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                else if(!User::where(['id'=>$request->child_id,'type'=>'2'])->exists())
                {
                    return Response::json(array('status'=> 'error','message'=>'Child Id not exist.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                else if(!DB::table('tag_parent')->where(['user_id'=>$request->child_id,'parent_id'=>$user->id])->exists())
                {
                    return Response::json(array('status'=> 'error','message'=>'Child not tagged to you.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
                }
                $child_id = $request->child_id;
                $country_id = User::select('country_id')->where('id',$child_id)->first();
                $products = $products->where('acc.country_id',$country_id->country_id);
                $total = $products->count();
            }
            else
            {
                $products = $products->where('acc.country_id',$user->country_id);
                $total = $products->count();
            }
            $products = $products->where('auction_products.status','1')->where('ac.status','1');
            $products = $products->offset($offset)->limit($perpage)->get();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            //return $meta;
            if($total>0)
            {
                return Response::json(array('status'=> 'success','message'=>'Auction products','meta'=>$meta,'data'=>$products))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No data found in auction products','meta'=>$meta,'data'=>$products))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function getAllCategoryProducts($id,Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }
            //return $offset;
            $products = new AuctionProducts();
            
            $products = $products->select('auction_products.id as id','auction_products.product_name','auction_products.product_image','auction_products.auction_category_id as category_id','ac.category_name','ac.base_price')->join('auction_categories as ac', 'ac.id', '=', 'auction_products.auction_category_id')->where('auction_products.status','1')->where('ac.id',$id)->where('ac.status','1');
            $total = $products->count();
            $products = $products->offset($offset)->limit($perpage)->get();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            //return $meta;
            if($total>0)
            {
                return Response::json(array('status'=> 'success','message'=>'Category products details','meta'=>$meta,'data'=>$products))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No product found in ategory','meta'=>$meta,'data'=>$products))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function getAllWishlistProducts(Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }
            $user= Auth::user();
            //return $offset;
            $products = new UserWishlist();

            if($user->type=='2')
            {
                $products = $products->select('user_wishlist.id as id','user_wishlist.product_id','user_wishlist.product_name','user_wishlist.product_price','user_wishlist.product_image','ap.rdm_price','user_wishlist.product_price as auction_price')->join('auction_products as ap','ap.id','=','user_wishlist.product_id')->where('user_id',$user->id);
            }
            if($user->type=='3')
            {
                $parent_data = \DB::table('tag_parent')->select(\DB::raw('group_concat(user_id) as user_ids'))->where('parent_id',$user->id)->get();
                $child_id = [];
                if(!empty($parent_data))
                {
                    $parent_ids = $parent_data[0]->user_ids;
                    $child_id = explode(',',$parent_ids);
                }
                $products = $products->select('user_wishlist.id as id','users.name','user_wishlist.product_id','user_wishlist.product_name','user_wishlist.product_price','user_wishlist.product_image')->join('users','users.id','=','user_wishlist.user_id')->whereIn('user_wishlist.user_id',$child_id);
            }
            $total = $products->count();
            $products = $products->offset($offset)->limit($perpage)->get();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            //return $meta;
            if($total>0)
            {
                return Response::json(array('status'=> 'success','message'=>'Wishlist products','meta'=>$meta,'data'=>$products))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No data found in wishlist','meta'=>$meta,'data'=>$products))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function addWishlistProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'product_id' => 'required|numeric',
            ]);
        if ($validator->fails()) {
            $errorMsg = $validator->getMessageBag()->toArray();
            return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        else
        {
            try
            {
                $user = Auth::user();
                if(!AuctionProducts::where('id',$request->product_id)->exists())
                {
                  return Response::json(array('status'=> 'error','message'=>'Product not exist.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                }
                if(!AuctionProducts::join('auction_categories as ac', 'ac.id', '=', 'auction_products.auction_category_id')->join('auction_category_country as acc', 'acc.auction_category_id', '=', 'ac.id')->where(['auction_products.id'=>$request->product_id,'ac.status'=>'1','acc.country_id'=>$user->country_id])->exists())
                {
                    return Response::json(array('status'=> 'error','message'=>'Product not Active to add in wishlist.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                }
                
                
                $check=UserWishlist::where(['user_id'=>$user->id,'product_id'=>$request->product_id])->exists();
                if(!$check){
                $product_data = AuctionProducts::select('auction_products.id as product_id','auction_products.product_name','ac.base_price as product_price','auction_products.product_image')->join('auction_categories as ac', 'ac.id', '=', 'auction_products.auction_category_id')->where('auction_products.id',$request->product_id)->first();
                $data = array(
                    'user_id'       => $user->id,
                    'product_id'    => $product_data->product_id,
                    'product_name'  => $product_data->product_name,
                    'product_price' => $product_data->product_price,
                    'product_image' => $product_data->product_image,
                );
                $query = UserWishlist::insert($data);
                return Response::json(array('status'=>'success','message'=>'Product added to wishlist successfully'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                else
                {
                  return Response::json(array('status'=>'error','message'=>'Product already added to your wishlist'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                }
            }
            catch(QueryException $ex){ 
                return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
        }
    }

    public function deleteWishlistProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'product_id' => 'required',
            ]);
        if ($validator->fails()) {
            $errorMsg = $validator->getMessageBag()->toArray();
            return Response::json(array('status'=>'error','message'=>$errorMsg))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        else
        {
            try
            {
                $user = Auth::user();
                $check=UserWishlist::where(['user_id'=>$user->id,'product_id'=>$request->product_id])->exists();
                if($check){
                    $query = UserWishlist::where(['user_id'=>$user->id,'product_id'=>$request->product_id])->delete();
                    return Response::json(array('status'=>'success','message'=>'Product removed from wishlist successfully'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
                }
                else
                {
                  return Response::json(array('status'=>'error','message'=>'Product not available in your wishlist to remove.'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
                }
            }
            catch(QueryException $ex){ 
                return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
        }
    }

    public function getAllChildDetails(Request $request,$id)
    {
        try
        {
            $auction = new Auction();
             $auction =  $auction->join('user_auction_products as aup', function($join)
            {
                $join->on('aup.auction_id', '=', 'auction.id');
                $join->on('aup.user_id','=', 'auction.last_bid_user');
            })
            ->join('auction_products as ap','ap.id','=','aup.product_id')
            ->join('auction_categories as ac','ac.id','=','ap.auction_category_id')
            ->select(\DB::raw('SUM(current_auction_value) as spent_amount'))
            ->where(['auction.last_bid_user'=>$id,'auction.status'=>'4'])->first();
            $spent_points = ($auction->spent_amount !=null) ? $auction->spent_amount : 0;
            $earn_points    = \DB::table('user_task_data')->select('task_id','bonus_point','total_attempt')->where(['user_id'=>$id,'is_final_submitted'=>'2'])->get();
            $total_earn_points = "0";
            foreach ($earn_points as $key => $value) {
                $task_data = Task::select('question_type','task_points','attempt_1','attempt_2','attempt_3','above_3')->where('id',$value->task_id)->first();
                $bonus_point = $value->bonus_point;
                if($value->total_attempt==1)
                {
                    $total_earn_points += $task_data->attempt_1 + $bonus_point;
                }
                elseif($value->total_attempt==2)
                {
                    $total_earn_points += $task_data->attempt_2 + $bonus_point;
                }
                elseif($value->total_attempt==3)
                {
                    $total_earn_points += $task_data->attempt_3 + $bonus_point;
                }
                elseif($value->total_attempt>3)
                {
                    $total_earn_points += $task_data->above_3 + $bonus_point;
                }
            }
            //return $total_earn_points;
            $total_points   = $total_earn_points - $spent_points;
            $total_packages = \DB::table('user_wishlist')->where('user_id',$id)->count();
            $data = \DB::table('users')->select(\DB::raw("$spent_points as spent_points"),\DB::raw("$total_earn_points as earn_points"),\DB::raw("$total_points as total_points"),\DB::raw("$total_packages as total_packages"),'name','avtar')->where('id',$id)->get();
            return Response::json(array('status'=> 'success','message'=>'Child Details','data'=>$data))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        } 
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function getChildWishlist($id,Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }
            //return $offset;
            $products = new UserWishlist();

            $products = $products->select('id as id','product_id','product_name','product_price','product_image')->where('user_id',$id)->offset($offset)->limit($perpage)->get();
            $child_data = User::select('name','avtar')->where('id',$id)->get();
            $total = $products->count();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            //return $meta;
            if($total>0)
            {
                return Response::json(array('status'=> 'success','message'=>'Wishlist products','meta'=>$meta,'child_data'=>$child_data,'wishlist_data'=>$products))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No data found in Wishlist','meta'=>$meta,'child_data'=>$child_data,'wishlist_data'=>$products))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function getChildEarnlist($id,Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }
            //return $offset;
            $earn_data = \DB::table('user_task_data')->select('task_id','bonus_point','total_attempt','updated_at')->where(['user_id'=>$id,'is_final_submitted'=>'2'])->orderBy('updated_at', 'desc')->offset($offset)->limit($perpage)->get();
            $total = $earn_data->count();
                $earn_data = $earn_data->map(function ($value) {
                $task_data = Task::select('task.task_name','task.question_type','task.task_points','task.attempt_1','task.attempt_2','task.attempt_3','task.above_3','category.category_name')->join('category', 'category.id', '=', 'task.category_id')->where('task.id',$value->task_id)->first();
                $bonus_point = $value->bonus_point;
                $total_earn_points = $task_data->task_points + $bonus_point;
                if($value->total_attempt==2)
                {
                    $total_earn_points = $task_data->attempt_2 + $bonus_point;
                }
                if($value->total_attempt==3)
                {
                    $total_earn_points = $task_data->attempt_3 + $bonus_point;
                }
                if($value->total_attempt>3)
                {
                    $total_earn_points = $task_data->above_3 + $bonus_point;
                }
                $data['category_name'] = $task_data->category_name;
                $data['question'] = $task_data->task_name;
                $data['earn_points'] = $total_earn_points;
                $data['earn_point_time'] = $value->updated_at;
                return $data; 
                //$data['question_type'] = $value->question_type;
            });
            $earn_data->all();
            $child_data = User::select('name','avtar')->where('id',$id)->get();
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            //return $meta;
            if($total>0)
            {
                return Response::json(array('status'=> 'success','message'=>'Earn Details','meta'=>$meta,'child_data'=>$child_data,'earn_data'=>$earn_data))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No data found in earn list','meta'=>$meta,'child_data'=>$child_data,'earn_data'=>$earn_data))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
    }

    public function getChildSpentlist($id,Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }
            //return $offset;
            $auction = new Auction();
            $auction = $auction->join('user_auction_products as aup', function($join)
            {
                $join->on('aup.auction_id', '=', 'auction.id');
                $join->on('aup.user_id','=', 'auction.last_bid_user');
            })
            ->join('auction_products as ap','ap.id','=','aup.product_id')
            ->join('auction_categories as ac','ac.id','=','ap.auction_category_id')
            ->select('auction.id as auction_id','ac.category_name','auction.current_auction_value as spent_points','ap.product_name','ap.product_image','auction.updated_at as spent_time')
            ->where(['auction.last_bid_user'=>$id,'auction.status'=>'4'])->orderBy('auction.updated_at', 'desc')->offset($offset)->limit($perpage)->get();
            $child_data = User::select('name','avtar')->where('id',$id)->get();
            $total = count($auction);
            $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
            //return $meta;
            if($total>0)
            {
                return Response::json(array('status'=> 'success','message'=>'Spent list details','meta'=>$meta,'child_data'=>$child_data,'spent_data'=>$auction))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
            }
            else
            {
               return Response::json(array('status'=> 'success','message'=>'No data found in spent list','meta'=>$meta,'child_data'=>$child_data,'spent_data'=>$auction))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);  
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]); 
        }
    }
    
    public function auctionHistory(Request $request)
    {
        try
        {
            $perpage = '100';
            $current_page = '1';
            if($request->has('perpage'))
            {
                $perpage = $request->input('perpage');
            }
            if($request->has('current_page'))
            {
                $current_page = $request->input('current_page');
            }
            if($current_page=='1')
            {
              $offset = 0;
            }
            else{
              $offset = ($current_page-1)*$perpage;
            }
            $user = Auth::user();
            if($user->type==2)
            {
                $auctions = new Auction();
                $user_id = $user->id;
                //return $user_id;
                $auction_id = \DB::table('auction_bid_logs')->where('user_id',$user_id)->distinct()->get(['auction_id']);
                $auction_ids = array();
                foreach ($auction_id as $auction_id) {
                    array_push($auction_ids,$auction_id->auction_id); 
                }
                //return $auction_ids;
                $auctions = $auctions->select(\DB::raw("$user_id as user_id"),'auction.id as auction_id','ac.category_name','auction.auction_price','auction.single_bid_value','auction.incremented_bid_time','auction.current_auction_value','auction.start_time','auction.end_time','abh.bid_hash','users.name as auction_winner')->join('auction_categories as ac','ac.id','=','auction.auction_category_id')->join('users','users.id','=','auction.last_bid_user')->join('auction_bid_hash as abh','abh.auction_id','=','auction.id')->where('auction.status','4')->whereIn('auction.id',$auction_ids)->offset($offset)->limit($perpage)->get();
                $auctions = $auctions->map(function ($value) {
                    $data['auction_id'] = $value->auction_id;
                    $data['category_name'] = $value->category_name;
                    $data['auction_price'] = $value->auction_price;
                    $data['single_bid_value'] = $value->single_bid_value;
                    $data['incremented_bid_time'] = $value->incremented_bid_time;
                    $data['current_auction_value'] = $value->current_auction_value;
                    $data['start_time'] = $value->start_time;
                    $data['end_time'] = $value->end_time;
                    $data['bid_hash'] = $value->bid_hash;
                    $data['auction_winner'] = $value->auction_winner;
                    $product_data = \DB::table('user_auction_products as uap')->select('ap.product_name','ap.product_image')->join('auction_products as ap','ap.id','=','uap.product_id')->where(['auction_id'=>$value->auction_id,'user_id'=>$value->user_id])->first();
                    if(count($product_data)>0)
                    {
                        $data['product_name'] = $product_data->product_name;
                        $data['product_image'] = $product_data->product_image;
                    }
                    return $data; 
                    });
                $auctions->all();
                $total = $auctions->count();
                $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$current_page];
                //return $meta;
                if($total>0)
                {
                    return Response::json(array('status'=> 'success','message'=>'Auction History details','meta'=>$meta,'data'=>$auctions))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]);
                }
                else
                {
                   return Response::json(array('status'=> 'success','message'=>'No Auction history found','meta'=>$meta,'data'=>$auctions))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
                }
            }
                else
            {
                return Response::json(array('status'=> 'error','message'=>'Please login as a child to participate in auction.'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                             ]); 
            }
        }
        catch(QueryException $ex){ 
          return Response::json(array('status'=> 'error','message'=>$ex->getMessage()))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        
    }
    
}
