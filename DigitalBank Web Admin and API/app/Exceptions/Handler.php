<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($exception instanceof ErrorException)
        {
           return \Response::make(['status'=>'error','message' => 'Unauthenticated.'], 401); 
        }
        if ($exception instanceof NotFoundHttpException)
        {
            $data = [];
             return \Response::make(['status'=>'error','message'=>'Please refer to API doc for correct URI and parameters','data'=>$data,'error_code'=>'404'], 404);
        }
        if ($exception instanceof MethodNotAllowedHttpException)
        {
            $data = [];
             return \Response::make(['status'=>'error','message'=>'Please refer to API doc for correct URI Type and parameters','data'=>$data,'error_code'=>'405'], 405);
        }
        else
        {
            $data = [];
             return \Response::make(['status'=>'error','message'=>$exception->getMessage(),'data'=>$data,'error_code'=>'401'], 401);
        }
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['status'=>'unauthenticated','message' => 'Unauthenticated.'], 401);
        }
        else
        {
            return response()->json(array('status'=> 'unauthenticated','message'=>'Unauthenticated'),401)->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
        }
        //return redirect()->guest(route('login'));
    }
}
