<?php
namespace App\Helper;

use Response;
 

class ResponseHeaderHelper {

    public static function sendSuccessResponse($message,$data) {

        return Response::json(array('status'=> 'success','message'=>$message,'data'=>$data))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
    }

    public static function sendErrorResponse($message,$data) {

        return Response::json(array('status'=> 'error','message'=>$message,'data'=>$data))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                         ]);
    }

}
