<?php
namespace App\Helper;

use Response;
 

class DeviceNotificationHelper {
    
    public static function sendNotification($device_id, $message) { 
        $data = array(
            'title'  			=>  $message['title'],
            'body'  			=>  $message['body'],
            'type'  			=>  $message['type']
        );
        $data =  Response::json(array('data'=>$data));
        $url = 'https://fcm.googleapis.com/fcm/send';
        $msg = array(
            'to'  			=>  $device_id,
            'notification'  =>  $message,
            'data' 			=>  $data,
        );
        // echo "<pre>";
        // print_r($msg);
        // die;
        $headers = array(
            'Authorization: key=AIzaSyDm5Q08LbJsFitZRdWI-a5gqn5kUXGwVII',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($msg));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        //echo '<pre>'; print_r($result);die;
        return  $result;
    }

}
