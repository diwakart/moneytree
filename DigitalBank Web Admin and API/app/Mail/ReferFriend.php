<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReferFriend extends Mailable
{
    use Queueable, SerializesModels;
    
    protected $child_data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($child_mail_data)
    {
        $this->child_data = $child_mail_data;
        //print_r($this->child_data); die;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('anup.singh2494@gmail.com')->markdown('mail.refer_friend')
                    ->with([
                        'from_name' => $this->child_data['from_name'],
                        'to_name' => $this->child_data['to_name'],
                        'refer_url' => $this->child_data['refer_url']
                    ]);
    }
}
