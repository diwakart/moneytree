<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserTaskResult extends Model
{
    public $table = "users_task_result";
}
