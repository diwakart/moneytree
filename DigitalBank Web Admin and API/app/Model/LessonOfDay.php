<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LessonOfDay extends Model
{
    public $table = "lesson_of_day";
}
