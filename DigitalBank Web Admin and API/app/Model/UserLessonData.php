<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserLessonData extends Model
{
    public $table = "user_lesson_data";
}
