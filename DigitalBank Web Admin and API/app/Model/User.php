<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    public $table = "users";
    
    public function AauthAcessToken()
    {
        return $this->hasMany('\App\OauthAccessToken');
    }
}
