<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LessonOptions extends Model
{
    public $table = "lesson_options";
}
