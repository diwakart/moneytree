<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserTaskData extends Model
{
    public $table = "user_task_data";
}
