<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AuctionCategories extends Model
{
    public $table = "auction_categories";
}
