<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ApiRoutes extends Model
{
    public $table = "api_routes";
}
