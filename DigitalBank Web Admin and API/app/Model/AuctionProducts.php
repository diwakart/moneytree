<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AuctionProducts extends Model
{
    public $table = "auction_products";
}
