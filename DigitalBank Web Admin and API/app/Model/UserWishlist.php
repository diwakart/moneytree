<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserWishlist extends Model
{
    public $table = "user_wishlist";
}
