<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserPackages extends Model
{
    public $table = "user_packages";
}
