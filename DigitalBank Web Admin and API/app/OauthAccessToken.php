<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OauthAccessToken extends Model {

    public $table = "oauth_access_tokens";

}
