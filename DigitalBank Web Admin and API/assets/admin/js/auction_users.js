$(document).ready(function() {
passbook_datatable = $('.users_datatable').mDatatable({
        // datasource definition
      data: {
      type: 'remote',
      source: {
          read: {
              url: 'getAllBiders',
              method: 'GET',
              // custom headers
              headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              params: {
                  // custom query params
                  query: {
                      search: ''
                  }
              },
              map: function(raw) {
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                       dataSet = raw.data;
                  }
                  return dataSet;
              },
          }
      },
      pageSize: 10,
        saveState: {
            cookie: false,
            webstorage: false
        },

        serverPaging: true,
        serverFiltering: false,
        serverSorting: false
    },
    // layout definition
    layout: {
      theme: 'default', // datatable theme
      class: '', // custom wrapper class
      scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: false,

    pagination: true,

    // columns definition
    columns: [
    {
      field: "S_No",
      title: "S.No",
      textAlign: 'center',
      width: 50,
    },
    {
      field: "name",
      title: "Bidder Name",
      width: 120      
    },
    {
      field: "current_bid_value",
      title: "Current Value",
      width: 120       
    },
    {
      field: "last_bid_at",
      title: "Last Time Bid",
      width: 220,
      textAlign:'center'       
    }]
    });
  });