$(document).ready(function() {
    var datatable='';
    var categoryData;
    var id = $('#local_data').attr('data_id');
      datatable = $('.products_datatable').mDatatable({
        // datasource definition
      data: {
      type: 'remote',
      source: {
          read: {
              url: '../getAllProducts/'+id,
              method: 'GET',
              // custom headers
              headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              params: {
                  // custom query params
                  query: {
                      generalSearch: ''
                  }
              },
              map: function(raw) {
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                       dataSet = raw.data;
                  }
                  return dataSet;
              },
          }
      },
      pageSize: 10,
        saveState: {
            cookie: false,
            webstorage: false
        },

        serverPaging: true,
        serverFiltering: false,
        serverSorting: false
    },
    // layout definition
    layout: {
      theme: 'default', // datatable theme
      class: '', // custom wrapper class
      scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: false,

    pagination: true,

    // search: {
    //   input: $('#generalSearch')
    // },

    // inline and bactch editing(cooming soon)
    // editable: false,

    // columns definition
    columns: [
    // {
    //   field: "id",
    //   title: "#",
    //   width: 50,
    //   selector: {class: 'm-checkbox--solid m-checkbox--brand'}
    // },
    {
      field: "S_No",
      title: "S.No",
      textAlign: 'center',
      width: 50,
      textAlign: 'center',
    },
    {
      field: "product_name",
      title: "Name"      
    },
    {
      field: "product_price",
      title: "Product Price"      
    },
    {
      field: "rdm_price",
      title: "Redemption Price"      
    },
    {
      field: "product_image",
      title: "Product Image",
      template: function (row) {
        if(row.product_image=='')
        {
            return '\
            <div></div>\
            ';
        }
        return '\
        <div ><img src="../../../storage/app/products/'+row.product_image+'" height="70" width="70"></div>\
        ';
      }       
    },
    {
      field: "description",
      title: "Product Description",      
    },
    {
      field: "status",
      title: "Status",
      template: function (row) {
        if(row.status=='1')
        {
            return '\
            <div>Active</div>\
            ';
        }
        return '\
        <div >Inactive</div>\
        ';
      }     
    },
    {
          field: 'Actions',
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

            return '\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete Product" onclick=deleteProducts('+row.id+')>\
              <i class="la la-trash"></i>\
            </a>\
            \
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Product" onclick=editProducts('+row.id+')>\
              <i class="la la-edit"></i>\
            </a>\
          ';
          },
      }]
    });

    $('#addBtn').click(function(){
      var product_name = $('#product_name').val();
      var product_price = $('#product_price').val();
      var path = '../add_product/'+id;
      var form = $('#add_product')[0];
      var data = new FormData(form);
      if(product_name=='' || product_price=='')
      {
        swal('Error','Mandatory fields Required','error');
        return false;
      }
      $.ajax({
          type: 'POST',
          enctype: 'multipart/form-data',
          url: path,
          data:data,
          processData: false,
          contentType: false,
          cache: false,
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              $('#AddProductModal').modal('toggle');
              swal('Error',res.message,'error');
              $('#product_name').val('');
              $('#product_price').val('');
              $('#image').val('');
              datatable.reload();
            }else{
              $('#AddProductModal').modal('toggle');
              $('#product_name').val('');
              $('#product_price').val('');
              $('#product_description').val('');
              $('#rdm_price').val('0');
              $('#image').val('');
              swal('Success',res.message,'success');
              datatable.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });

    });
    $('#Updatebtn').click(function(e){
        e.preventDefault();
        var pro_id = $('#EditProductModal').data('id');
        var product_name = $('#name').val();
        var product_price = $('#price').val();
        var form = $('#edit_product')[0];
        var data = new FormData(form);
        var path = '../edit_product/'+pro_id;
        if(product_name=='' || product_price=='')
        {
          swal('Error','Mandatory fields Required','error');
          return false;
        }
        $.ajax({
          type: 'POST',
          enctype: 'multipart/form-data',
          url: path,
          data:data,
          processData: false,
          contentType: false,
          cache: false,
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
              datatable.reload();
            }else{
              $('#EditProductModal').modal('toggle');
              $('#product_name').val('');
              $('#product_price').val('');
              $('#EditProductModal').data('id','');
              $("#image_selected").html('');
              $('#image').val('');
              $("#image_selected").hide();
             $("#ResponseSuccessModal").modal('show');
             $("#ResponseSuccessModal #ResponseHeading").text(res.message);
             datatable.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      });
});

$("#product_price,#category_base_price,#add_rdm_price,#edit_rdm_price").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

function deleteProducts(id)
{
  var path = "../delete_products";
    var _this = $(this);
    swal({
      title: "Are you sure to delete this Product?",
      text: "Your will lost all records of this Product",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        var data = id;
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: data,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               $('.sweet-overlay').remove();
               $('.showSweetAlert ').remove();
               swal('Success',res.message,'success');
                location.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
}
/**********************Redemption change***************************/
$('#add_allow_redeem').on('click', function () {
    if ($(this).prop('checked')) {
      $('#rdm_price').show();
    }
    else
    {
      $('#rdm_price').val('0');
      $('#rdm_price').hide();
    }
  });

  $('#edit_allow_redeem').on('click', function () {
    if ($(this).prop('checked')) {
      $('#edit_rdm_price').css('display','block');
    }
    else
    {
      $('#edit_rdm_price').val('0');
      $('#edit_rdm_price').css('display','none');
    }
  });
