$(document).ready(function() {
    var datatable;
      datatable = $('.country_datatable').mDatatable({
        // datasource definition
      data: {
      type: 'remote',
      source: {
          read: {
              url: 'getAllCountries',
              method: 'GET',
              // custom headers
              headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              params: {
                  // custom query params
                  query: {
                    generalSearch: '',
                    country_name: '',
                 }
              },
              map: function(raw) {
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                       dataSet = raw.data;
                  }
                  return dataSet;
              },
          }
      },
      pageSize: 10,
        saveState: {
            cookie: false,
            webstorage: false
        },

        serverPaging: true,
        serverFiltering: false,
        serverSorting: false
    },
    // layout definition
    layout: {
      theme: 'default', // datatable theme
      class: '', // custom wrapper class
      scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: false,

    pagination: true,

    search: {
      input: $('#generalSearch')
    },

    // inline and bactch editing(cooming soon)
    // editable: false,

    // columns definition
    columns: [
    // {
    //   field: "action",
    //   title: "#",
    //   textAlign: 'center',
    //   width: 50,
    // },
    {
      field: "S_No",
      title: "S.No",
      textAlign: 'center',
      width: 50
    },
    {
      field: "country_name",
      title: "Country Name",
      width: 150      
    },
    {
      field: "code",
      title: "Short Code",
      width: 220 ,
      textAlign:'center'       
    },
    {
      field: "country_code",
      title: "Country Code",
      width: 220 ,
      textAlign:'center'       
    },
    // {
    //   field: "status",
    //   title: "Package Status",
    //   template: function (row) {
    //     if(row.status=='2')
    //     {
    //         return '\
    //         <div>Inactive</div>\
    //         ';
    //     }
    //     return '\
    //     <div >Active</div>\
    //     ';
    //   }        
    // },
    {
          width: 150,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          textAlign:'center',
          field: 'Actions',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';
            var url =  '\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit" onclick=getCountryDetail('+row.id+')></i>\
            </a>\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deletePacakge('+row.id+')>\
              <i class="la la-trash"></i>\
            </a>\
          ';
          
          return url;
          },
        }]
    });
    $('#m_form_status, #m_form_type, #m_form_hotness,#m_form_list').selectpicker();
    $('#LoadCountryDatatable').on('click',function(){
      datatable.reload();
    });
    
    $('#generalSearch').on('change',function(){
        var value = $(this).val();
        if(value!='')
        {
          datatable.setDataSourceQuery({country_name:value});
          datatable.reload();
        }
        else
        {
          datatable.setDataSourceQuery({country_name:''});
          datatable.reload();
        }

    });
    
      $('#addCountryBtn').on('click',function(){
      var id = $('#addCountryModal').data('id');
      var country_name = $('#country_name').val();      
      var code = $('#short_code').val();      
      var country_code = $('#country_code').val();      
      
      if(country_name=='' || code=='' || country_code=='')
      {
        swal('Error','Mandatory Fields Required','error');
        return false;
      }     
      path = "add_country"
      $.ajax({
          type: 'POST',
          url: path,
          data: {

            id:id,

            country_name: country_name,

            code:code,

            country_code:country_code
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            //console.log("in");
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
              swal('success',res.message,'success');
              $('#addCountryModal').modal('hide'); 
              datatable.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
    });
});

// $("#package_amount,#package_points").keydown(function (e) {
//         // Allow: backspace, delete, tab, escape, enter and .
//     if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
//          // Allow: Ctrl+A, Command+A
//         (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
//          // Allow: home, end, left, right, down, up
//         (e.keyCode >= 35 && e.keyCode <= 40)) {
//              // let it happen, don't do anything
//              return;
//     }
//     // Ensure that it is a number and stop the keypress
//     if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
//         e.preventDefault();
//     }
// });

function getCountryDetail(id){
    var path = "country_detail";
    $.ajax({
      type: "POST",
      url: path,
      data: {
        id: id
      },
       headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
      success: function(result){
        //console.log(result);
        var res = $.parseJSON(result);

        if(res.status == 'error'){

        }else{
          var data = $.parseJSON(JSON.stringify(res.message));
          console.log(data);
          
          $('#addCountryModal').data('id',data.id);
          $('#addCountryModal').find('.modal-title').html('Update Country ' + data.country_name);
          $('#country_name').val(data.country_name);
          $('#short_code').val(data.code);
          $('#country_code').val(data.country_code);
          // $('#package_status').val(data.status);
          $('#addCountryModal').modal('show');
        }
      },
      error: function(){
        alert("Error");
      }
    }); 
  }
  $("#country_code").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
  function deletePacakge(id){
    var path = "country_delete";
    var _this = $(this);
    swal({
      title: "Are you sure to delete this Country?",
      text: "Your will lost all records of this Country",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        var data = id;
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: data,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
	
              $('.sweet-overlay').remove();
              $('.showSweetAlert ').remove();
              //swal('Success',res.message,'success');
              $("#ResponseSuccessModal").modal('show');
              $("#ResponseSuccessModal #ResponseHeading").text(res.message);
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
  }
