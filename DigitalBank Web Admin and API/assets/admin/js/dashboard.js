$(document).ready(function() {
    $('#ChangePwd').click(function(e){
    e.preventDefault();
    var password  = $('#change_pwd').val();
    if(password=='')
    {
      swal('Error','Please Enter Password','error');
      return false;
    }
    if(password.length < 4)
    {
      swal('Error','Please Enter Password more than 4 character','error');
      return false;
    }
    var path = 'admin/change_pwd';
    $.ajax({
      type: 'POST',
      url: path,
      data: {
        password: password
      },
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
      success: function(data) {
        var res = $.parseJSON(data);
        if(res.status == 'error'){
          swal('Error',res.message,'error');
        }else{
          $.ajax
          ({
              type: 'POST',
              url: 'logout',
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              success: function()
              {
                  location.reload();
              }
          });
        } 
      },
      error: function(data) {
        swal('Error',data,'error');
      }
    });
  });
});