$(document).ready(function() {
    var datatable='';
    var categoryData;
      datatable = $('.auction_datatable').mDatatable({
        // datasource definition
      data: {
      type: 'remote',
      source: {
          read: {
              url: 'getAllAuctions',
              method: 'GET',
              // custom headers
              headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              params: {
                  // custom query params
                  query: {
                      generalSearch: ''
                  }
              },
              map: function(raw) {
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                       dataSet = raw.data;
                  }
                  return dataSet;
              },
          }
      },
      pageSize: 10,
        saveState: {
            cookie: false,
            webstorage: false
        },

        serverPaging: true,
        serverFiltering: false,
        serverSorting: false
    },
    // layout definition
    layout: {
      theme: 'default', // datatable theme
      class: '', // custom wrapper class
      scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: false,

    pagination: true,

    // search: {
    //   input: $('#generalSearch')
    // },

    // inline and bactch editing(cooming soon)
    // editable: false,

    // columns definition
    columns: [
    // {
    //   field: "id",
    //   title: "#",
    //   width: 50,
    //   selector: {class: 'm-checkbox--solid m-checkbox--brand'}
    // },
    {
      field: "S_No",
      title: "S.No",
      textAlign: 'center',
      width: 50,
      textAlign: 'center',
    },
    {
      field: "category_name",
      title: "Auction name"      
    },
    {
      field: "auction_price",
      title: "Auction value",
      textAlign:'center',     
    },
    {
      field: "single_bid_value",
      title: "Single bid value",
      textAlign:'center',      
    },
    {
      field: "incremented_bid_time",
      title: "Incremented bid time",
      textAlign:'center',
      width : 170,     
    },
    {
      field: "start_time",
      title: "Start time",
      width: 140,     
    },
    {
      field: "end_time",
      title: "Expiration time",
      width: 140,      
    },
    {
      field: "status",
      title: "Status",
      template: function (row) {
        if(row.status=='1')
        {
            return '\
            <button type="button" class="btn btn-success" onclick=changeStatus('+row.id+','+row.status+')>Active\
            </button>\
            ';
        }
        if(row.status=='3')
        {
            return '\
            <button type="button" class="btn btn-success">In progress\
            </button>\
            ';
        }
        if(row.status=='4')
        {
            return '\
            <button type="button" class="btn btn-success">Closed\
            </button>\
            ';
        }
        return '\
          <button type="button" class="btn btn-warning" onclick=changeStatus('+row.id+','+row.status+')>Inactive\
          </button>\
          ';
      }     
    },
    {
          field: 'Actions',
          width: 160,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';
            if(row.status=='3' || row.status=='4')
            {
              return '\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete Auction" onclick=deleteAuction('+row.id+')>\
              <i class="la la-trash"></i>\
            </a>\
            \
            <a href="auction/'+row.id+'/users" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="View Auction Details">\
              <i class="la la-list"></i>\
            </a>\
          ';  
            }

            var actions = '\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete Auction" onclick=deleteAuction('+row.id+')>\
              <i class="la la-trash"></i>\
            </a>\
            \
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Auction" onclick=editAuction('+row.id+')>\
              <i class="la la-edit"></i>\
            </a>\
            \
            <a href="auction/'+row.id+'/users" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="View Auction Details">\
              <i class="la la-list"></i>\
            </a>';
            if(row.status=='1')
            {
            actions +=  '\
            <button type="button" class="btn btn-info" onclick=sendNotification('+row.id+')>Send Notification</button>\
            ';
            }
          return actions;
          },
      }]
    });
    
    $('#country_id').on('change',function(){
      var value = $(this).val();
      datatable.setDataSourceQuery({country_id:value});
      datatable.reload();
    });

    $('#LoadAuctionDatatable').on('click',function(){
      datatable.reload();
    });

    $('#addBtn').click(function(){
      var category_id = $('#fill_category_id').val();
      var single_bid_value = $('#single_bid_value').val();
      var incremented_time = $('#incremented_time').val();
      var start_time = $('#start_time').val();
      var end_time = $('#end_time').val();
      var path = 'add_auction';
      if(category_id=='' || start_time=='' || end_time=='' || single_bid_value=='' || incremented_time =='')
      {
        swal('Error','Mandatory fields Required','error');
        return false;
      }
      $.ajax({
          type: 'POST',
          url: path,
          data: {
            category_id: category_id,
            single_bid_value: single_bid_value,
            incremented_time: incremented_time,
            start_time:start_time,
            end_time:end_time
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              $('#AddAuctionModal').modal('toggle');
              swal('Error',res.message,'error');
              $('#fill_category_id').val('');
              $('#start_time').val('');
              $('#end_time').val('');
              $('#single_bid_value').val('');
              $('#incremented_time').val('');
              datatable.reload();
            }else{
              $('#AddAuctionModal').modal('toggle');
              $('#fill_category_id').val('');
              $('#start_time').val('');
              $('#end_time').val('');
              $('#single_bid_value').val('');
              $('#incremented_time').val('');
              swal('Success',res.message,'success');
              datatable.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });

    });
    $('#Updatebtn').click(function(e){
        e.preventDefault();
        var id = $('#EditAuctionModal').data('id');
        var category_id = $('#fill_category_id_edit').val();
        var single_bid_value = $('#single_bid_value_edit').val();
        var incremented_time = $('#incremented_time_edit').val();
        var start_time = $('#start_time_edit').val();
        var end_time = $('#end_time_edit').val();
        var path = 'update_auction';
        if(category_id=='' || start_time=='' || end_time=='')
        {
          swal('Error','Mandatory fields Required','error');
          return false;
        }
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: id,
            category_id: category_id,
            single_bid_value: single_bid_value,
            incremented_time: incremented_time,
            start_time:start_time,
            end_time:end_time
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
              datatable.reload();
            }else{
              $('#EditAuctionModal').modal('toggle');
              $('#fill_category_id').val('');
              $('#start_time_edit').val('');
              $('#end_time_edit').val('');
              $('#single_bid_value_edit').val('');
              $('#incremented_time_edit').val('');
              $('#EditAuctionModal').data('id','');
             $("#ResponseSuccessModal").modal('show');
             $("#ResponseSuccessModal #ResponseHeading").text(res.message);
             datatable.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      });
});

$('#auction_range').daterangepicker({
    buttonClasses: 'm-btn btn',
    applyClass: 'btn-primary',
    cancelClass: 'btn-secondary',
    position: 'relative',

    timePicker: true,
    timePickerIncrement: 30,
    minDate:'0',
    locale: {
        format: 'MM/DD/YYYY h:mm A'
    }
  }, function(start, end, label) {
    $('#start_date .form-control').val(start.format('MM/DD/YYYY h:mm A'));
    $('#end_date .form-control').val(end.format('MM/DD/YYYY h:mm A'));
  });
$('#auction_range_edit').daterangepicker({
    buttonClasses: 'm-btn btn',
    applyClass: 'btn-primary',
    cancelClass: 'btn-secondary',

    timePicker: true,
    timePickerIncrement: 30,
    locale: {
        format: 'MM/DD/YYYY h:mm A'
    }
  }, function(start, end, label) {
    $('#start_date_edit .form-control').val(start.format('MM/DD/YYYY h:mm A'));
    $('#end_date_edit .form-control').val(end.format('MM/DD/YYYY h:mm A'));
  });
$("#single_bid_value,#single_bid_value_edit").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
function deleteAuction(id)
{
  var path = "delete_auction";
    var _this = $(this);
    swal({
      title: "Are you sure to delete this Auction?",
      text: "Your will lost all records of this Auction",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        var data = id;
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: data,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               $('.sweet-overlay').remove();
               $('.showSweetAlert ').remove();
               swal('Success',res.message,'success');
              location.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
}

function changeStatus(auction_id,status)
{
    var path = "auction/change_status"
    var auction_id = auction_id;
    var status = status;
    var _this = $(this);
    swal({
      title: "Are you sure to change status of auction?",
      //text: "Your will not able to this Category",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, Change it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: 'GET',
          url: path,
          data: {
            auction_id: auction_id,
            status: status,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               $('.sweet-overlay').remove();
               $('.showSweetAlert ').remove();
                //swal('Success',res.message,'success');
                $("#ResponseSuccessModal").modal('show');
                $("#ResponseSuccessModal #ResponseHeading").text(res.message);
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
}

function sendNotification (auction_id)
{
    var path = "auction/send_notificattion"
    var auction_id = auction_id;
   
        $.ajax({
          type: 'GET',
          url: path,
          data: {
            auction_id: auction_id,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               $('.sweet-overlay').remove();
               $('.showSweetAlert ').remove();
                //swal('Success',res.message,'success');
                $("#ResponseSuccessModal").modal('show');
                $("#ResponseSuccessModal #ResponseHeading").text(res.message);
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
    }