$(document).ready(function() {
    var datatable;
    var transaction_datatable;
    var request_datatable;
    var parent_id = $('#local_data').attr('data-id');
    $('#load_datatable').click(function(){
      datatable = $('.child_datatable').mDatatable({
        // datasource definition
      data: {
      type: 'remote',
      source: {
          read: {
              url: '../../getAllParentChild/'+parent_id,
              method: 'GET',
              // custom headers
              headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              params: {
                  // custom query params
                  query: {
                      generalSearch: ''
                  }
              },
              map: function(raw) {
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                       dataSet = raw.data;
                  }
                  return dataSet;
              },
          }
      },
      pageSize: 10,
        saveState: {
            cookie: false,
            webstorage: false
        },

        serverPaging: true,
        serverFiltering: false,
        serverSorting: false
    },
    // layout definition
    layout: {
      theme: 'default', // datatable theme
      class: '', // custom wrapper class
      scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: false,

    pagination: true,

    // columns definition
    columns: [
    // {
    //   field: "action",
    //   title: "#",
    //   textAlign: 'center',
    //   width: 50,
    // },
    {
      field: "S_No",
      title: "S.No",
      textAlign: 'center',
      width: 50,
      textAlign: 'center',
    },
    {
      field: "name",
      title: "Child Name",
      width: 150,
      template: function (row) {
        return '\
        <div >\
          <a title="View Child Profile" href="../../child/profile/'+row.id+'">'+row.name+'</a>\
        </div>\
        ';
      }       
    },
    {
      field: "email",
      title: "Child Email",
      width: 220        
    },
    {
      field: "mobile",
      title: "Child Contact"      
    },
    {
      field: "school_name",
      title: "Child School"      
    }]
    });
  });
  $('#load_transaction').on('click',function(){
  transaction_datatable = $('.transaction_datatable').mDatatable({
        // datasource definition
      data: {
      type: 'remote',
      source: {
          read: {
              url: '../../getAllUserTransaction/'+parent_id,
              method: 'GET',
              // custom headers
              headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              params: {
                  // custom query params
                  query: {
                      searchhotness: ''
                  }
              },
              map: function(raw) {
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                       dataSet = raw.data;
                  }
                  return dataSet;
              },
          }
      },
      pageSize: 10,
        saveState: {
            cookie: false,
            webstorage: false
        },

        serverPaging: true,
        serverFiltering: false,
        serverSorting: false
    },
    // layout definition
    layout: {
      theme: 'default', // datatable theme
      class: '', // custom wrapper class
      scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: false,

    pagination: true,

    // columns definition
    columns: [
    {
      field: "S_No",
      title: "S.No",
      textAlign: 'center',
      width: 50,
    },
    {
      field: "from_name",
      title: "From",
      width: 120      
    },
    {
      field: "name",
      title: "To",
      width: 120       
    },
    {
      field: "transaction_points",
      title: "Points Transaction",
      width: 220,
      textAlign:'center'       
    },
    {
      field: "remarks",
      title: "Remarks",
      width: 220            
    }]
    });
  });
    $('#request_data').on('click',function(){
    request_datatable = $('.request_datatable').mDatatable({
        // datasource definition
      data: {
      type: 'remote',
      source: {
          read: {
              url: '../../getAllUserRequest/'+parent_id,
              method: 'GET',
              // custom headers
              headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              params: {
                  // custom query params
                  query: {
                      searchhotness: ''
                  }
              },
              map: function(raw) {
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                       dataSet = raw.data;
                  }
                  return dataSet;
              },
          }
      },
      pageSize: 10,
        saveState: {
            cookie: false,
            webstorage: false
        },

        serverPaging: true,
        serverFiltering: false,
        serverSorting: false
    },
    // layout definition
    layout: {
      theme: 'default', // datatable theme
      class: '', // custom wrapper class
      scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: false,

    pagination: true,

    // columns definition
    columns: [
    {
      field: "S_No",
      title: "S.No",
      textAlign: 'center',
      width: 50,
    },
    {
      field: "child_name",
      title: "Child Name",
      width: 100,
    },
    {
      field: "request_type",
      title: "Request Type",
      width: 100,
      template: function (row) {
        if(row.request_type=='1')
        {
          return '\
          <div>Tag</div>\
          ';
        }
        if(row.request_type=='2')
        {
          return '\
          <div>Points</div>\
          ';
        }
      }        
    },
    {
      field: "points",
      title: "Request Points",
      width: 140,
      textAlign:'center'       
    },
    {
      field: "request_status",
      title: "Status",
      width: 120,
      template: function (row) {
        if(row.request_status=='1')
        {
          return '\
          <div>Waiting</div>\
          ';
        }
        if(row.request_status=='2')
        {
          return '\
          <div>Accepted</div>\
          ';
        }
        return '\
          <div>Rejected</div>\
          ';
      }              
    }]
    });
  });
});
