$(document).ready(function() {
    var datatable='';
    var categoryData;
      datatable = $('.categories_datatable').mDatatable({
        // datasource definition
      data: {
      type: 'remote',
      source: {
          read: {
              url: 'getAllAuctionCategories',
              method: 'GET',
              // custom headers
              headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              params: {
                  // custom query params
                  query: {
                      generalSearch: ''
                  }
              },
              map: function(raw) {
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                       dataSet = raw.data;
                  }
                  return dataSet;
              },
          }
      },
      pageSize: 10,
        saveState: {
            cookie: false,
            webstorage: false
        },

        serverPaging: true,
        serverFiltering: false,
        serverSorting: false
    },
    // layout definition
    layout: {
      theme: 'default', // datatable theme
      class: '', // custom wrapper class
      scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: false,

    pagination: true,

    // search: {
    //   input: $('#generalSearch')
    // },

    // inline and bactch editing(cooming soon)
    // editable: false,

    // columns definition
    columns: [
    // {
    //   field: "id",
    //   title: "#",
    //   width: 50,
    //   selector: {class: 'm-checkbox--solid m-checkbox--brand'}
    // },
    {
      field: "S_No",
      title: "S.No",
      textAlign: 'center',
      width: 50,
      textAlign: 'center',
    },
    {
      field: "category_name",
      title: "Name"      
    },
    {
      field: "country_name",
      title: "Country Name"      
    },
    {
      field: "base_price",
      title: "Base Price"      
    },
    {
      field: "status",
      title: "Status",
      template: function (row) {
        if(row.status=='1')
        {
            return '\
            <button type="button" class="btn btn-success" onclick=changeStatus('+row.id+','+row.status+')>Active\
            </button>\
            ';
        }
        return '\
          <button type="button" class="btn btn-warning" onclick=changeStatus('+row.id+','+row.status+')>Inactive\
          </button>\
          ';
      }     
    },
    {
          field: 'Actions',
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

            return '\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete Category" onclick=deleteCategories('+row.id+')>\
              <i class="la la-trash"></i>\
            </a>\
            \
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Category" onclick=editCategories('+row.id+')>\
              <i class="la la-edit"></i>\
            </a>\
            \
            <a href="auction/'+row.id+'/products" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="View Category Products">\
              <i class="la la-list"></i>\
            </a>\
          ';
          },
      }]
    });

    $('#LoadCategoryDatatable').on('click',function(){
      datatable.reload();
    });
    $('#country_id').on('change',function(){
      var value = $(this).val();
      datatable.setDataSourceQuery({country_id:value});
      datatable.reload();
    });

    $('#addBtn').click(function(){
      var country_id   = $('#country_add_name').val();
      var category_name = $('#category_add_name').val();
      var category_base_price = $('#category_base_price').val();
      var path = 'add_auction_category';
      if(country_id=='' || category_name=='' || category_base_price=='')
      {
        swal('Error','Mandatory fields Required','error');
        return false;
      }
      $.ajax({
          type: 'POST',
          url: path,
          data: {
            country_id:country_id,
            category_name: category_name,
            category_base_price:category_base_price
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              $('#AddCategoryModal').modal('toggle');
              swal('Error',res.message,'error');
              $('#category_add_name').val('');
              $('#category_base_price').val('');
              datatable.reload();
            }else{
              $('#AddCategoryModal').modal('toggle');
              $('#category_add_name').val('');
              $('#country_add_name').val('');
              $('#category_base_price').val('');
              swal('Success',res.message,'success');
              datatable.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });

    });
    $('#Updatebtn').click(function(e){
        e.preventDefault();
        var id = $('#EditCategoryModal').data('id');
        var country_id   = $('#country_name').val();
        var category_name  = $('#category_name').val();
        var category_price  = $('#category_price').val();
        var path = 'update_auctionCategory';
        if(country_id=='')
        {
            swal('Error','Country Name Required','error');
            return false;
        }
        else if(category_name=='')
        {
          swal('Error','Category Name Required','error');
          return false;
        }
        else if(category_price=='')
        {
          swal('Error','Auction category base price Required','error');
          return false;
        }
        
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: id,
            country_id:country_id,
            category_name: category_name,
            category_price:category_price
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
              datatable.reload();
            }else{
              $('#EditCategoryModal').modal('toggle');
              $('#category_name').val('');
              $('#category_price').val('');
              $('#country_edit_name').val('');
              $('#EditCategoryModal').data('id','');
             $("#ResponseSuccessModal").modal('show');
             $("#ResponseSuccessModal #ResponseHeading").text(res.message);
             datatable.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      });
});

$("#category_price,#category_base_price").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

function deleteCategories(id)
{
  var path = "delete_category";
    var _this = $(this);
    swal({
      title: "Are you sure to delete this category_name?",
      text: "Your will lost all records of this Category",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        var data = id;
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: data,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               $('.sweet-overlay').remove();
               $('.showSweetAlert ').remove();
               swal('Success',res.message,'success');
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
}

function changeStatus(category_id,status)
{
    var path = "auction_category/change_status"
    var category_id = category_id;
    var status = status;
    var _this = $(this);
    swal({
      title: "Are you sure to Change status of Category?",
      //text: "Your will not able to this Category",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, Change it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: 'GET',
          url: path,
          data: {
            category_id: category_id,
            status: status,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               $('.sweet-overlay').remove();
               $('.showSweetAlert ').remove();
                //swal('Success',res.message,'success');
                $("#ResponseSuccessModal").modal('show');
                $("#ResponseSuccessModal #ResponseHeading").text(res.message);
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
}
