$(document).ready(function() {
    var datatable;
      datatable = $('.package_datatable').mDatatable({
        // datasource definition
      data: {
      type: 'remote',
      source: {
          read: {
              url: 'getAllPackages',
              method: 'GET',
              // custom headers
              headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              params: {
                  // custom query params
                  query: {
                      country_name: '',
                      plan_type:''
                  }
              },
              map: function(raw) {
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                       dataSet = raw.data;
                  }
                  return dataSet;
              },
          }
      },
      pageSize: 10,
        saveState: {
            cookie: false,
            webstorage: false
        },

        serverPaging: true,
        serverFiltering: false,
        serverSorting: false
    },
    // layout definition
    layout: {
      theme: 'default', // datatable theme
      class: '', // custom wrapper class
      scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: false,

    pagination: true,

    // search: {
    //   input: $('#generalSearch')
    // },

    // inline and bactch editing(cooming soon)
    // editable: false,

    // columns definition
    columns: [
    // {
    //   field: "action",
    //   title: "#",
    //   textAlign: 'center',
    //   width: 50,
    // },
    {
      field: "S_No",
      title: "S.No",
      textAlign: 'center',
      width: 50,
      textAlign: 'center',
    },
    {
      field: "name",
      title: "Package Name",
      width: 150      
    },
    {
      field: "currency",
      title: "Currency",
      width: 80
    },
    {
      field: "country_name",
      title: "Package Country",
      width: 140      
    },
    {
      field: "recurring_amount",
      title: "Recurring Amount",
      width: 180 ,
      textAlign:'center'       
    },
    {
      field: "recurring_points",
      title: "Recurring Points",
      width: 120 ,
      textAlign:'center'       
    },
    {
      field: "one_time_amount",
      title: "Topup Amount",
      width: 180 ,
      textAlign:'center'       
    },
    {
      field: "one_time_points",
      title: "Topup Points",
      width: 120 ,
      textAlign:'center'       
    },
    {
      field: "status",
      title: "Package Status",
      template: function (row) {
        if(row.status=='2')
        {
            return '\
            <div>Inactive</div>\
            ';
        }
        return '\
        <div >Active</div>\
        ';
      }        
    },
    {
          width: 150,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          textAlign:'center',
          field: 'Actions',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';
            var url =  '\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit" onclick=getPacakgeDetail('+row.id+')></i>\
            </a>\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deletePacakge('+row.id+')>\
              <i class="la la-trash"></i>\
            </a>\
          ';
          
          return url;
          },
        }]
    });
    $('#m_form_status, #m_form_type, #m_form_hotness,#m_form_list').selectpicker();
    $('#LoadPackageDatatable').on('click',function(){
      datatable.reload();
    });
    $('#country_id').on('change',function(){
      var value     = $(this).val();
      var plan_type = $('#plan_type').val()
      datatable.setDataSourceQuery({country_id:value,plan_type:plan_type});
      datatable.reload();
    });
    $('#plan_type').on('change',function(){
      var value      = $(this).val();
      var country_id = $('#country_id').val();
      datatable.setDataSourceQuery({country_id:country_id,plan_type:value});
      datatable.reload();
    });
      $('#addPackageBtn').on('click',function(){
        var id                = $('#addPackageModal').data('id');
        var package_name      = $('#package_name').val();
        var one_time_amount   = $('#one_time_amount').val();
        var package_status    = $('#package_status').val();
        var one_time_points   = $('#one_time_points').val();
        var recurring_amount  = $('#recurring_amount').val();
        var recurring_points  = $('#recurring_points').val();
        var plan_currency     = $('#plan_currency').val();
        //var plan_type         = $('#plan_name').val();
        var country_id        = $('#country_name').val();
        if(country_id=='')
        {
            swal('Error','Country name required','error');
            return false;
        }
        else if(package_name=='')
        {
            swal('Error','Package name required','error');
            return false;  
        }
        else if(plan_currency=='')
        {
            swal('Error','Currency required for package','error');
            return false;  
        }
        var checked = $("#package_form input:checked").length > 0;
        if (!checked){
            swal('Error','Please check at least one type of plan','error');
            return false;
        }
       if($("#add_recurring_plan").is(':checked'))
       {
            if(recurring_amount=='' || recurring_points=="")
            {
                swal('Error','Recuuring Amount and Points required if plan is recurring type','error');
                return false;
            }
       }
       if($("#add_one_time_plan").is(':checked'))
       {
            if(one_time_amount=='' || one_time_points=="")
            {
                swal('Error','One Time Amount and Points required if plan is Topup type','error');
                return false;
            }
       }
        if(package_status=='')
        {
            swal('Error','Package status required','error');
            return false;  
        }
      
      path = "add_package"
      $.ajax({
          type: 'POST',
          url: path,
          data: {
            id:id,country_id:country_id,name: package_name,plan_currency:plan_currency,one_time_amount:one_time_amount,one_time_points:one_time_points,recurring_amount:recurring_amount,recurring_points:recurring_points,status:package_status
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
              swal('success',res.message,'success');
              $('#addPackageModal').modal('hide'); 
              datatable.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
    });
});

$("#recurring_amount,#recurring_points,#one_time_amount,#one_time_points").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

function getPacakgeDetail(id){
    var path = "package_detail";
    $.ajax({
      type: "POST",
      url: path,
      data: {
        id: id
      },
       headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
      success: function(result){
        //console.log(result);
        var res = $.parseJSON(result);

        if(res.status == 'error'){

        }else{
          var data = res.message;
          console.log(data.id);
          $('#addPackageModal').data('id',data.id);
          $('#addPackageModal').find('.modal-title').html('Update Pacakge ' + data.name);
          $('#plan_name').val(data.plan_type);
          $('#plan_currency').val(data.currency);
          var country_id = data.country_ids;
          //alert(country_id);
            $.each(country_id.split(","), function(i,e){
                $("#country_name option[value='" + e + "']").prop("selected", true);
            });
		              /*$('#country_name').val(data.country_id);*/
	      /*$('#country_name').val(data.country_id);*/
          $('#package_name').val(data.name);
          $('#package_status').val(data.status);
          if(data.recurring_amount=='' || data.recurring_amount==null)
          {
            $('#add_recurring_plan').prop('checked',false);
            $('#recurring_amount').val('');
            $('#recurring_points').val('');
          }
          else
          {
            $('#add_recurring_plan').prop('checked',true);
            $('#recurring_amount').val(data.recurring_amount);
            $('#recurring_points').val(data.recurring_points);
          }
          if(data.one_time_amount=='' || data.one_time_amount==null)
          {
            $('#add_one_time_plan').prop('checked',false);  
            $('#one_time_amount').val('');
            $('#one_time_points').val('');
          }
          else
          {
            $('#add_one_time_plan').prop('checked',true);
            $('#one_time_amount').val(data.one_time_amount);
            $('#one_time_points').val(data.one_time_points);
          }
          $('#addPackageModal').modal('show');
        }
      },
      error: function(){
        alert("Error");
      }
    }); 
  }
  function deletePacakge(id){
    var path = "package_delete";
    var _this = $(this);
    swal({
      title: "Are you sure to delete this Package?",
      text: "Your will lost all records of this Package",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        var data = id;
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: data,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
              $('.sweet-overlay').remove();
              $('.showSweetAlert ').remove();
              //swal('Success',res.message,'success');
              $("#ResponseSuccessModal").modal('show');
              $("#ResponseSuccessModal #ResponseHeading").text(res.message);
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
  }
