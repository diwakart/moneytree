$(document).ready(function() {
    var datatable;
      datatable = $('.users_datatable').mDatatable({
        // datasource definition
      data: {
      type: 'remote',
      source: {
          read: {
              url: 'getAllUsers',
              method: 'GET',
              // custom headers
              headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              params: {
                  // custom query params
                  query: {
                      search: '',
                      userlist:''
                  }
              },
              map: function(raw) {
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                       dataSet = raw.data;
                  }
                  return dataSet;
              },
          }
      },
      pageSize: 10,
        saveState: {
            cookie: false,
            webstorage: false
        },

        serverPaging: true,
        serverFiltering: false,
        serverSorting: false
    },
    // layout definition
    layout: {
      theme: 'default', // datatable theme
      class: '', // custom wrapper class
      scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: false,

    pagination: true,
    // columns definition
    columns: [
    // {
    //   field: "action",
    //   title: "#",
    //   textAlign: 'center',
    //   width: 50,
    // },
    {
      field: "id",
      title: "Points",
      selector: {class:'m-checkbox--solid m-checkbox--brand send_mail'},
      width: 40,
    },
    /*{
      field: "S_No",
      title: "S.No",
      textAlign: 'center',
      width: 50,
    },*/
    {
      field: "country_name",
      title: "Country Name",
      width: 150      
    },
    {
      field: "user_name",
      title: "User Name",
      width: 150      
    },
    {
      field: "name",
      title: "Name",
      width: 150      
    },
    {
      field: "email",
      title: "User Email",
      width: 220        
    },
    {
      field: "mobile",
      title: "User Contact"      
    },
    {
      field: "type",
      title: "User Type",
      template: function (row) {
        if(row.type=='2')
        {
            return '\
            <div>Student</div>\
            ';
        }
        return '\
        <div >Parent</div>\
        ';
      }        
    },
    {
          width: 150,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          textAlign:'center',
          field: 'Actions',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';
            if(row.type=='2')
            {
             var url_view = '\
              <a href="child/profile/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                <i class="fa fa-eye"></i>\
              </a>\
              ';
            }
            if(row.type=='3')
            {
             var url_view = '\
              <a href="parent/profile/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                <i class="fa fa-eye"></i>\
              </a>\
              ';
            }
            var url =  '\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit" onclick=getUserDetail('+row.id+')></i>\
            </a>\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Change Password">\
              <i class="la la-key" onclick=getUserID('+row.id+')></i>\
            </a>\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteUser('+row.id+')>\
              <i class="la la-trash"></i>\
            </a>\
          ' + url_view;
          
          return url;
          },
        }]
    });
    //var selected_id = new Array();
    $('.users_datatable').on('m-datatable--on-check', function (e, args) 
    {
        var count = datatable.getSelectedRecords().length;

        /** WebQuiz changes for Digital Bank merger **/

        selected_id = $.map(datatable.getSelectedRecords(), function (item) {
        return $(item).find("td").eq(0).find("input").val();
        });
        $('#m_datatable_selected_number').html(count);
        if (count > 0) {
        $('#store_reward').show();
        }
    })
      .on('m-datatable--on-uncheck m-datatable--on-layout-updated', function (e, args) {
        var count = datatable.getSelectedRecords().length;
        $('#m_datatable_selected_number').html(count);
        if (count === 0) {
          $('#store_reward').hide();
        }
      });
      
    $('#add_points').on('click',function(){  
      var user_id = JSON.parse(JSON.stringify(selected_id));
      var reward_points = $('#reward_points').val();
      if(reward_points<=0)
      {
          swal('Error','Please Enter Point more than 0.','error');
          return false;
      }
      
      path = "add_reward_points"
      $.ajax({
          type: 'POST',
          url: path,
          data: {
            id:user_id,reward_points: reward_points
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
              swal('success',res.message,'success');
              $('#AddRewardPontsModal').modal('hide'); 
              datatable.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
    });
    $('#country_id').on('change',function(){
      var value = $(this).val();
      var user_list = $('#userlist').val();
      var search = $('#generalSearch').val();
      var status = $('#user_status').val();
      datatable.setDataSourceQuery({country_id:value,user_list:user_list,search:search,status:status});
      datatable.reload();
    });
    $('#userlist').on('change',function(){
      var value = $(this).val();
      var country_id = $('#country_id').val();
      var search = $('#generalSearch').val();
      var status = $('#user_status').val();
      datatable.setDataSourceQuery({country_id:country_id,user_list:value,search:search,status:status});
      datatable.reload();
    });
    $('#user_status').on('change',function(){
      var value = $(this).val();
      var country_id = $('#country_id').val();
      var user_list = $('#userlist').val();
      var search = $('#generalSearch').val();
      datatable.setDataSourceQuery({country_id:country_id,user_list:user_list,status:value,search:search});
      datatable.reload();
    });
    $('#generalSearch').on('blur',function(){
      var value = $(this).val();
      var country_id = $('#country_id').val();
      var user_list = $('#userlist').val();
      var status = $('#user_status').val();
      datatable.setDataSourceQuery({country_id:country_id,search:value,user_list:user_list,status:status});
      datatable.reload();
    });
    $('#formSubmit').on('click',function(){
      var id = $('#AddUserModal').data('id');
       var country_name = $('#country_name').val();
      var user_name = $('#user_name').val();
      var country_code = $('#country_code').val();
      var name = $('#name').val();
      var user_email = $('#email').val();
      var user_mobile = $('#mobile').val();
      var user_type = $('#type').val();
      var user_password = $('#password').val();
      var student_list = '';
      var parent_list = '';
      if(country_name=='')
      {
        swal('Error','Country name required','error');
        return false;
      }
      if(user_name=='' || user_type=='')
      {
        swal('Error','Mandatory Fields Required','error');
        return false;
      }
      path = "add_user"
      $.ajax({
          type: 'POST',
          url: path,
          data: {
            id:id,country_name:country_name,user_name:user_name,name: name,email:user_email,type:user_type,country_code:country_code,mobile:user_mobile,student_list:student_list,parent_list:parent_list,password:user_password
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
              swal('success',res.message,'success');
              $('#AddUserModal').modal('hide'); 
              datatable.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
    });
    $('#LoadUserDatatable').click(function(){
      datatable.reload();
    });
    $('#ChangePasswordBtn').click(function(e){
    e.preventDefault();
    
    var id = $('#ChangePasswordModal').data('id');
    var password  = $('#change_password').val();
    var path = 'changePassword';
    $.ajax({
      type: 'POST',
      url: path,
      data: {
        id: id,
        password: password
      },
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
      success: function(data) {
        var res = $.parseJSON(data);
        if(res.status == 'error'){
          swal('Error',res.message,'error');
        }else{
          $('#ChangePasswordModal').modal('toggle');
          $('#change_password').val('');
          $('#ChangePasswordModal').data('id','');
          swal('Success',res.message,'success');
          datatable.reload();
        } 
      },
      error: function(data) {
        swal('Error',data,'error');
      }
    });
  });
});
function getUserDetail(id){
    var path = "user_detail";
    $.ajax({
      type: "POST",
      url: path,
      data: {
        id: id
      },
       headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
      success: function(result){
        //console.log(result);
        var res = $.parseJSON(result);

        if(res.status == 'error'){

        }else{
          var data = $.parseJSON(JSON.stringify(res.message));
          $('#AddUserModal').data('id',data.id);
          $('#AddUserModal').find('.modal-title').html('Update student ' + data.name);
          $('#user_name').val(data.user_name);
          $('#country_name').val(data.country_id);
          $('#name').val(data.name);
          $('#email').val(data.email);
          $('#mobile').val(data.mobile);
          $("#PasswordInput").html('');
          $('#type').val(data.type);
          $('#parent_list').hide();
          $('#student_list').hide();
          // if(data.type=='2')
          // {
          //   $('#type').val(data.type);
          //   $('#parent_tag').val(data.parent_id)
          //   $('#parent_list').show();
          // }
          // if(data.type=='3')
          // {
          //   $('#type').val(data.type);
          //   $('student_list').css('display','block');
          //   //$('#student_tag').val(data.parent_id).prop('selected', true);
          // }
          $('#AddUserModal').modal('show');
        }
      },
      error: function(){
        alert("Error");
      }
    }); 
  }
  function getUserID(id){
    var path = "getUserID";
    $.ajax({
      type: "POST",
      url: path,
      data: {
        id: id
      },
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
      success: function(result){
        //console.log(result);
        var res = $.parseJSON(result);

        if(res.status == 'error'){

        }else{
          var data = $.parseJSON(JSON.stringify(res.message));
          $('#ChangePasswordModal').data('id',data.id);
          $('#ChangePasswordModal').find('.modal-title').text('Change Password of Student  ' + data.name);
          $('#ChangePasswordModal').modal('show');
        }
      },
      error: function(){
        alert("Error");
      }
    }); 
  }
  function deleteUser(id){
    var path = "user_delete";
    var _this = $(this);
    swal({
      title: "Are you sure to delete this User?",
      text: "Your will lost all records of this User",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        var data = id;
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: data,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
              $('.sweet-overlay').remove();
              $('.showSweetAlert ').remove();
              //swal('Success',res.message,'success');
              $("#ResponseSuccessModal").modal('show');
              $("#ResponseSuccessModal #ResponseHeading").text(res.message);
              //location.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
  }
  $("#reward_points").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});