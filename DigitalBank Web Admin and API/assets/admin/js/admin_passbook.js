$(document).ready(function() {
passbook_datatable = $('.passbook_datatable').mDatatable({
        // datasource definition
      data: {
      type: 'remote',
      source: {
          read: {
              url: 'getAllAdminTransaction',
              method: 'GET',
              // custom headers
              headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              params: {
                  // custom query params
                  //query: {
                  //    searchhotness: ''
                  //}
              },
              map: function(raw) {
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                       dataSet = raw.data;
                  }
                  return dataSet;
              },
          }
      },
      pageSize: 10,
        saveState: {
            cookie: false,
            webstorage: false
        },

        serverPaging: true,
        serverFiltering: false,
        serverSorting: false
    },
    // layout definition
    layout: {
      theme: 'default', // datatable theme
      class: '', // custom wrapper class
      scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: false,

    pagination: true,

    // columns definition
    columns: [
    {
      field: "S_No",
      title: "S.No",
      textAlign: 'center',
      width: 50,
    },
    {
      field: "created_at",
      title: "Logged At",
      textAlign: 'center',
      width: 150, 
    },
    {
      field: "from_id",
      title: "From Account",
      width: 120      
    },
    {
      field: "to_id",
      title: "To Account",
      width: 140       
    },
    {
      field: "transaction_points",
      title: "Points Transaction",
      width: 220,
      textAlign:'center'       
    },
    {
      field: "transaction_type",
      title: "Transaction Type",
      width: 220,
      textAlign:'center',
      template: function (row) {
      if(row.transaction_type=='1')
      {
          return '<div>Debit</div>';
      }
      else
      {
         return '<div>Credit</div>'; 
      }
      }
    },
    {
      field: "remarks",
      title: "Remarks",
      width: 220            
    }]
    });
  });