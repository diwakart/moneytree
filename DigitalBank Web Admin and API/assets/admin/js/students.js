$(document).ready(function() {
    var datatable;
      $('.loader_msg').css('display','none');
      var accountsdata;
      var edit_url = 'accounts/edit';
      var delete_url = 'accounts/delete';
      var view_url = 'accounts/view';
      datatable = $('.m_datatable').mDatatable({
        // datasource definition
      data: {
      type: 'remote',
      source: {
          read: {
              url: 'getAllUsers',
              method: 'GET',
              // custom headers
              headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              params: {
                  // custom query params
                  query: {
                      generalSearch: ''
                  }
              },
              map: function(raw) {
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                       dataSet = raw.data;
                  }
                  return dataSet;
              },
          }
      },
      pageSize: 10,
        saveState: {
            cookie: false,
            webstorage: false
        },

        serverPaging: true,
        serverFiltering: false,
        serverSorting: false
    },
    // layout definition
    layout: {
      theme: 'default', // datatable theme
      class: '', // custom wrapper class
      scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: false,

    pagination: true,

    // search: {
    //   input: $('#generalSearch')
    // },

    // inline and bactch editing(cooming soon)
    // editable: false,

    // columns definition
    columns: [
    // {
    //   field: "action",
    //   title: "#",
    //   textAlign: 'center',
    //   width: 50,
    // },
    {
      field: "S.No",
      title: "S.No",
      textAlign: 'center',
      width: 50,
      textAlign: 'center',
    },
    {
      field: "category_name",
      title: "Category Name"      
    },
    {
      field: "status",
      title: "Status",
      template: function (row) {
        if(row.status=='1')
        {
            return '\
            <div>Active</div>\
            ';
        }
        return '\
        <div >Inactive</div>\
        ';
      }     
    },
    {
      field: "Status",
      title: "Action"      
    }]
    });
    $('#m_form_status, #m_form_type, #m_form_hotness,#m_form_list').selectpicker();
});