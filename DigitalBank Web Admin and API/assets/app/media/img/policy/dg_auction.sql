-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 30, 2018 at 10:08 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digital_bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `dg_auction`
--

CREATE TABLE `dg_auction` (
  `id` int(10) UNSIGNED NOT NULL,
  `auction_category_id` int(10) UNSIGNED NOT NULL,
  `auction_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `single_bid_value` int(11) NOT NULL,
  `total_bids` int(11) NOT NULL DEFAULT '0',
  `current_auction_value` int(11) NOT NULL,
  `auction_closed_at` datetime(6) DEFAULT NULL,
  `status` enum('1','2','3','4') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2' COMMENT '1 => Active, 2 => Inactive,3=>In progress,4=>Complete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dg_auction`
--

INSERT INTO `dg_auction` (`id`, `auction_category_id`, `auction_price`, `start_time`, `end_time`, `single_bid_value`, `total_bids`, `current_auction_value`, `auction_closed_at`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, '500', '2018-07-01 00:00:00', '2018-07-04 23:00:00', 10, 0, 500, '2018-07-04 23:00:00.000000', '1', '2018-06-29 07:16:37', '2018-06-29 07:24:59');

--
-- Triggers `dg_auction`
--
DELIMITER $$
CREATE TRIGGER `auction_bid_log` AFTER UPDATE ON `dg_auction` FOR EACH ROW BEGIN
   IF(NEW.status = 4) THEN 
   INSERT INTO dg_auction_bid_logs (SELECT * FROM dg_auction_bids WHERE dg_auction_bids.auction_id = id); 
   DELETE FROM dg_auction_bids WHERE auction_id = id;
END IF;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dg_auction`
--
ALTER TABLE `dg_auction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auction_auction_category_id_foreign` (`auction_category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dg_auction`
--
ALTER TABLE `dg_auction`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `dg_auction`
--
ALTER TABLE `dg_auction`
  ADD CONSTRAINT `auction_auction_category_id_foreign` FOREIGN KEY (`auction_category_id`) REFERENCES `dg_auction_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
