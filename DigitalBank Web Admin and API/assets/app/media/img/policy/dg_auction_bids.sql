-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2018 at 12:04 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digital_bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `dg_auction_bids`
--

CREATE TABLE `dg_auction_bids` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `auction_id` int(10) UNSIGNED NOT NULL,
  `current_bid_value` int(11) DEFAULT NULL,
  `last_bid_at` datetime(6) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Triggers `dg_auction_bids`
--
DELIMITER $$
CREATE TRIGGER `check_balance_before_adding_bid` BEFORE INSERT ON `dg_auction_bids` FOR EACH ROW BEGIN
                    IF (get_balance(NEW.user_id,1)<NEW.current_bid_value) THEN SIGNAL   SQLSTATE "45000" SET MESSAGE_TEXT = "Buffer points are more than user current avialable balance";
                    END IF; 
                END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_auction_details` AFTER INSERT ON `dg_auction_bids` FOR EACH ROW BEGIN
                    UPDATE dg_auction SET total_bids = (total_bids + 1),current_auction_value = (current_auction_value+NEW.current_bid_value) WHERE id = NEW.auction_id;
                END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dg_auction_bids`
--
ALTER TABLE `dg_auction_bids`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auction_bids_user_id_foreign` (`user_id`),
  ADD KEY `auction_bids_auction_id_foreign` (`auction_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dg_auction_bids`
--
ALTER TABLE `dg_auction_bids`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `dg_auction_bids`
--
ALTER TABLE `dg_auction_bids`
  ADD CONSTRAINT `auction_bids_auction_id_foreign` FOREIGN KEY (`auction_id`) REFERENCES `dg_auction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auction_bids_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `dg_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
