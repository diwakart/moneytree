<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('countries')->truncate();
        
        \DB::table('countries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'AF',
                'country_name' => 'Afghanistan',
                'country_code' => '93',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'AX',
                'country_name' => 'Åland Islands',
                'country_code' => '358',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'AL',
                'country_name' => 'Albania',
                'country_code' => '355',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'DZ',
                'country_name' => 'Algeria',
                'country_code' => '213',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'AS',
                'country_name' => 'American Samoa',
                'country_code' => '1-684',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'AD',
                'country_name' => 'Andorra',
                'country_code' => '376',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'AO',
                'country_name' => 'Angola',
                'country_code' => '244',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'code' => 'AI',
                'country_name' => 'Anguilla',
                'country_code' => '1-264',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'code' => 'AQ',
                'country_name' => 'Antarctica',
                'country_code' => '672',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'code' => 'AG',
                'country_name' => 'Antigua and Barbuda',
                'country_code' => '1-268',
                'default_country' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-06-28 06:28:17',
            ),
            10 => 
            array (
                'id' => 11,
                'code' => 'AR',
                'country_name' => 'Argentina',
                'country_code' => '54',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'code' => 'AM',
                'country_name' => 'Armenia',
                'country_code' => '374',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'code' => 'AW',
                'country_name' => 'Aruba',
                'country_code' => '297',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'code' => 'AU',
                'country_name' => 'Australia',
                'country_code' => '61',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'code' => 'AT',
                'country_name' => 'Austria',
                'country_code' => '43',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'code' => 'AZ',
                'country_name' => 'Azerbaijan',
                'country_code' => '994',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'code' => 'BS',
                'country_name' => 'Bahamas',
                'country_code' => '1-242',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'code' => 'BH',
                'country_name' => 'Bahrain',
                'country_code' => '973',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'code' => 'BD',
                'country_name' => 'Bangladesh',
                'country_code' => '880',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'code' => 'BB',
                'country_name' => 'Barbados',
                'country_code' => '1-246',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'code' => 'BY',
                'country_name' => 'Belarus',
                'country_code' => '375',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'code' => 'BE',
                'country_name' => 'Belgium',
                'country_code' => '32',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'code' => 'BZ',
                'country_name' => 'Belize',
                'country_code' => '501',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'code' => 'BJ',
                'country_name' => 'Benin',
                'country_code' => '229',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'code' => 'BM',
                'country_name' => 'Bermuda',
                'country_code' => '1-441',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'code' => 'BT',
                'country_name' => 'Bhutan',
                'country_code' => '975',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'code' => 'BO',
                'country_name' => 'Bolivia, Plurinational State of',
                'country_code' => '591',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'code' => 'BQ',
                'country_name' => 'Bonaire, Sint Eustatius and Saba',
                'country_code' => '599',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'code' => 'BA',
                'country_name' => 'Bosnia and Herzegovina',
                'country_code' => '387',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'code' => 'BW',
                'country_name' => 'Botswana',
                'country_code' => '267',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'code' => 'BR',
                'country_name' => 'Brazil',
                'country_code' => '55',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'code' => 'IO',
                'country_name' => 'British Indian Ocean Territory',
                'country_code' => '246',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'code' => 'BN',
                'country_name' => 'Brunei Darussalam',
                'country_code' => '673',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'code' => 'BG',
                'country_name' => 'Bulgaria',
                'country_code' => '359',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'code' => 'BF',
                'country_name' => 'Burkina Faso',
                'country_code' => '226',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'code' => 'BI',
                'country_name' => 'Burundi',
                'country_code' => '257',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'code' => 'KH',
                'country_name' => 'Cambodia',
                'country_code' => '855',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'code' => 'CM',
                'country_name' => 'Cameroon',
                'country_code' => '237',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'code' => 'CA',
                'country_name' => 'Canada',
                'country_code' => '1',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'code' => 'CV',
                'country_name' => 'Cape Verde',
                'country_code' => '238',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'code' => 'KY',
                'country_name' => 'Cayman Islands',
                'country_code' => '1-345',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'code' => 'CF',
                'country_name' => 'Central African Republic',
                'country_code' => '236',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'code' => 'TD',
                'country_name' => 'Chad',
                'country_code' => '235',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'code' => 'CL',
                'country_name' => 'Chile',
                'country_code' => '56',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'code' => 'CN',
                'country_name' => 'China',
                'country_code' => '86',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'code' => 'CX',
                'country_name' => 'Christmas Island',
                'country_code' => '61',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'code' => 'CC',
                'country_name' => 'Cocos (Keeling) Islands',
                'country_code' => '61',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'code' => 'CO',
                'country_name' => 'Colombia',
                'country_code' => '57',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'code' => 'KM',
                'country_name' => 'Comoros',
                'country_code' => '269',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'code' => 'CG',
                'country_name' => 'Congo',
                'country_code' => '242',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'code' => 'CD',
                'country_name' => 'Congo, the Democratic Republic of the',
                'country_code' => '243',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'code' => 'CK',
                'country_name' => 'Cook Islands',
                'country_code' => '682',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'code' => 'CR',
                'country_name' => 'Costa Rica',
                'country_code' => '506',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'code' => 'CI',
                'country_name' => 'Côte d\'Ivoire',
                'country_code' => '225',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'code' => 'HR',
                'country_name' => 'Croatia',
                'country_code' => '385',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'code' => 'CU',
                'country_name' => 'Cuba',
                'country_code' => '53',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'code' => 'CW',
                'country_name' => 'Curaçao',
                'country_code' => '599',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'code' => 'CY',
                'country_name' => 'Cyprus',
                'country_code' => '357',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'code' => 'CZ',
                'country_name' => 'Czech Republic',
                'country_code' => '420',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'code' => 'DK',
                'country_name' => 'Denmark',
                'country_code' => '45',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'code' => 'DJ',
                'country_name' => 'Djibouti',
                'country_code' => '253',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'code' => 'DM',
                'country_name' => 'Dominica',
                'country_code' => '1-767',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'code' => 'EC',
                'country_name' => 'Ecuador',
                'country_code' => '593',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'code' => 'EG',
                'country_name' => 'Egypt',
                'country_code' => '20',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'code' => 'SV',
                'country_name' => 'El Salvador',
                'country_code' => '503',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'code' => 'GQ',
                'country_name' => 'Equatorial Guinea',
                'country_code' => '240',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'code' => 'ER',
                'country_name' => 'Eritrea',
                'country_code' => '291',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'code' => 'EE',
                'country_name' => 'Estonia',
                'country_code' => '372',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'code' => 'ET',
                'country_name' => 'Ethiopia',
                'country_code' => '251',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'code' => 'FK',
            'country_name' => 'Falkland Islands (Malvinas)',
                'country_code' => '500',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'code' => 'FO',
                'country_name' => 'Faroe Islands',
                'country_code' => '298',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'code' => 'FJ',
                'country_name' => 'Fiji',
                'country_code' => '679   ',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'code' => 'FI',
                'country_name' => 'Finland',
                'country_code' => '358',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'code' => 'FR',
                'country_name' => 'France',
                'country_code' => '33',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'code' => 'GF',
                'country_name' => 'French Guiana',
                'country_code' => '689',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'code' => 'PF',
                'country_name' => 'French Polynesia',
                'country_code' => '689',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'code' => 'TF',
                'country_name' => 'French Southern Territories',
                'country_code' => '262',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'code' => 'GA',
                'country_name' => 'Gabon',
                'country_code' => '241',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'code' => 'GM',
                'country_name' => 'Gambia',
                'country_code' => '220',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'code' => 'GE',
                'country_name' => 'Georgia',
                'country_code' => '995',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'code' => 'DE',
                'country_name' => 'Germany',
                'country_code' => '49',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'code' => 'GH',
                'country_name' => 'Ghana',
                'country_code' => '233',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'code' => 'GI',
                'country_name' => 'Gibraltar',
                'country_code' => '350',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'code' => 'GR',
                'country_name' => 'Greece',
                'country_code' => '30',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'code' => 'GL',
                'country_name' => 'Greenland',
                'country_code' => '299',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'code' => 'GD',
                'country_name' => 'Grenada',
                'country_code' => '1-473',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'code' => 'GP',
                'country_name' => 'Guadeloupe',
                'country_code' => '502',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'code' => 'GU',
                'country_name' => 'Guam',
                'country_code' => '1-671',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'code' => 'GT',
                'country_name' => 'Guatemala',
                'country_code' => '502',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'code' => 'GG',
                'country_name' => 'Guernsey',
                'country_code' => '44-1481',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'code' => 'GN',
                'country_name' => 'Guinea',
                'country_code' => '224',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'code' => 'GW',
                'country_name' => 'Guinea-Bissau',
                'country_code' => '245',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'code' => 'GY',
                'country_name' => 'Guyana',
                'country_code' => '592',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'code' => 'HT',
                'country_name' => 'Haiti',
                'country_code' => '509',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'code' => 'VA',
                'country_name' => 'Holy See (Vatican City State)',
                'country_code' => '379',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'code' => 'HN',
                'country_name' => 'Honduras',
                'country_code' => '504',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'code' => 'HK',
                'country_name' => 'Hong Kong',
                'country_code' => '852',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'code' => 'HU',
                'country_name' => 'Hungary',
                'country_code' => '36',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'code' => 'IS',
                'country_name' => 'Iceland',
                'country_code' => '354',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'code' => 'IN',
                'country_name' => 'India',
                'country_code' => '91',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'code' => 'ID',
                'country_name' => 'Indonesia',
                'country_code' => '62',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'code' => 'IR',
                'country_name' => 'Iran, Islamic Republic of',
                'country_code' => '98',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'code' => 'IQ',
                'country_name' => 'Iraq',
                'country_code' => '964',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'code' => 'IE',
                'country_name' => 'Ireland',
                'country_code' => '353',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'code' => 'IM',
                'country_name' => 'Isle of Man',
                'country_code' => '44-1624',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'code' => 'IL',
                'country_name' => 'Israel',
                'country_code' => '972',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'code' => 'IT',
                'country_name' => 'Italy',
                'country_code' => '39',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'code' => 'JM',
                'country_name' => 'Jamaica',
                'country_code' => '1-876',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'code' => 'JP',
                'country_name' => 'Japan',
                'country_code' => '81',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'code' => 'JE',
                'country_name' => 'Jersey',
                'country_code' => '44-1534',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'code' => 'JO',
                'country_name' => 'Jordan',
                'country_code' => '962',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'code' => 'KZ',
                'country_name' => 'Kazakhstan',
                'country_code' => '7',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'code' => 'KE',
                'country_name' => 'Kenya',
                'country_code' => '254',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'code' => 'KI',
                'country_name' => 'Kiribati',
                'country_code' => '686',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'code' => 'KP',
                'country_name' => 'Korea, Democratic People\'s Republic of',
                'country_code' => '850',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'code' => 'KR',
                'country_name' => 'Korea, Republic of',
                'country_code' => '850',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => 120,
                'code' => 'KW',
                'country_name' => 'Kuwait',
                'country_code' => '965',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => 121,
                'code' => 'KG',
                'country_name' => 'Kyrgyzstan',
                'country_code' => '996',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => 122,
                'code' => 'LA',
                'country_name' => 'Lao People\'s Democratic Republic',
                'country_code' => '856',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => 123,
                'code' => 'LV',
                'country_name' => 'Latvia',
                'country_code' => '371',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => 124,
                'code' => 'LB',
                'country_name' => 'Lebanon',
                'country_code' => '961',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => 125,
                'code' => 'LS',
                'country_name' => 'Lesotho',
                'country_code' => '266',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => 126,
                'code' => 'LR',
                'country_name' => 'Liberia',
                'country_code' => '231',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => 127,
                'code' => 'LY',
                'country_name' => 'Libya',
                'country_code' => '218',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => 128,
                'code' => 'LI',
                'country_name' => 'Liechtenstein',
                'country_code' => '423',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => 129,
                'code' => 'LT',
                'country_name' => 'Lithuania',
                'country_code' => '370',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => 130,
                'code' => 'LU',
                'country_name' => 'Luxembourg',
                'country_code' => '352',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => 131,
                'code' => 'MO',
                'country_name' => 'Macao',
                'country_code' => '853',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => 132,
                'code' => 'MK',
                'country_name' => 'Macedonia, the Former Yugoslav Republic of',
                'country_code' => '389',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => 133,
                'code' => 'MG',
                'country_name' => 'Madagascar',
                'country_code' => '261',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => 134,
                'code' => 'MW',
                'country_name' => 'Malawi',
                'country_code' => '265',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => 135,
                'code' => 'MY',
                'country_name' => 'Malaysia',
                'country_code' => '60',
                'default_country' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-06-28 23:37:53',
            ),
            135 => 
            array (
                'id' => 136,
                'code' => 'MV',
                'country_name' => 'Maldives',
                'country_code' => '960',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => 137,
                'code' => 'ML',
                'country_name' => 'Mali',
                'country_code' => '223',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => 138,
                'code' => 'MT',
                'country_name' => 'Malta',
                'country_code' => '356',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => 139,
                'code' => 'MH',
                'country_name' => 'Marshall Islands',
                'country_code' => '692',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => 140,
                'code' => 'MQ',
                'country_name' => 'Martinique',
                'country_code' => '596',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => 141,
                'code' => 'MR',
                'country_name' => 'Mauritania',
                'country_code' => '222',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => 142,
                'code' => 'MU',
                'country_name' => 'Mauritius',
                'country_code' => '230',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => 143,
                'code' => 'YT',
                'country_name' => 'Mayotte',
                'country_code' => '262',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => 144,
                'code' => 'MX',
                'country_name' => 'Mexico',
                'country_code' => '52',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => 145,
                'code' => 'FM',
                'country_name' => 'Micronesia, Federated States of',
                'country_code' => '691',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => 146,
                'code' => 'MD',
                'country_name' => 'Moldova, Republic of',
                'country_code' => '373',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => 147,
                'code' => 'MC',
                'country_name' => 'Monaco',
                'country_code' => '377',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => 148,
                'code' => 'MN',
                'country_name' => 'Mongolia',
                'country_code' => '976',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => 149,
                'code' => 'ME',
                'country_name' => 'Montenegro',
                'country_code' => '382',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => 150,
                'code' => 'MS',
                'country_name' => 'Montserrat',
                'country_code' => '1-664',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => 151,
                'code' => 'MA',
                'country_name' => 'Morocco',
                'country_code' => '212',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => 152,
                'code' => 'MZ',
                'country_name' => 'Mozambique',
                'country_code' => '258',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => 153,
                'code' => 'MM',
                'country_name' => 'Myanmar',
                'country_code' => '95',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => 154,
                'code' => 'NA',
                'country_name' => 'Namibia',
                'country_code' => '264',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => 155,
                'code' => 'NR',
                'country_name' => 'Nauru',
                'country_code' => '674',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => 156,
                'code' => 'NP',
                'country_name' => 'Nepal',
                'country_code' => '977',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => 157,
                'code' => 'NL',
                'country_name' => 'Netherlands',
                'country_code' => '31',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => 158,
                'code' => 'NC',
                'country_name' => 'New Caledonia',
                'country_code' => '687',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => 159,
                'code' => 'NZ',
                'country_name' => 'New Zealand',
                'country_code' => '64',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => 160,
                'code' => 'NI',
                'country_name' => 'Nicaragua',
                'country_code' => '505',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => 161,
                'code' => 'NE',
                'country_name' => 'Niger',
                'country_code' => '227',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => 162,
                'code' => 'NG',
                'country_name' => 'Nigeria',
                'country_code' => '234',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => 163,
                'code' => 'NU',
                'country_name' => 'Niue',
                'country_code' => '683',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => 164,
                'code' => 'NF',
                'country_name' => 'Norfolk Island',
                'country_code' => '672',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => 165,
                'code' => 'MP',
                'country_name' => 'Northern Mariana Islands',
                'country_code' => '1-670',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => 166,
                'code' => 'NO',
                'country_name' => 'Norway',
                'country_code' => '47',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => 167,
                'code' => 'OM',
                'country_name' => 'Oman',
                'country_code' => '968',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => 168,
                'code' => 'PK',
                'country_name' => 'Pakistan',
                'country_code' => '92',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => 169,
                'code' => 'PW',
                'country_name' => 'Palau',
                'country_code' => '680',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => 170,
                'code' => 'PS',
                'country_name' => 'Palestine, State of',
                'country_code' => '970',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => 171,
                'code' => 'PA',
                'country_name' => 'Panama',
                'country_code' => '507',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => 172,
                'code' => 'PG',
                'country_name' => 'Papua New Guinea',
                'country_code' => '675',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => 173,
                'code' => 'PY',
                'country_name' => 'Paraguay',
                'country_code' => '595',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => 174,
                'code' => 'PE',
                'country_name' => 'Peru',
                'country_code' => '51',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => 175,
                'code' => 'PH',
                'country_name' => 'Philippines',
                'country_code' => '63',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => 176,
                'code' => 'PN',
                'country_name' => 'Pitcairn',
                'country_code' => '      64',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => 177,
                'code' => 'PL',
                'country_name' => 'Poland',
                'country_code' => '48',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => 178,
                'code' => 'PT',
                'country_name' => 'Portugal',
                'country_code' => '351',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => 179,
                'code' => 'PR',
                'country_name' => 'Puerto Rico',
                'country_code' => '1-787',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => 180,
                'code' => 'QA',
                'country_name' => 'Qatar',
                'country_code' => '974',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => 181,
                'code' => 'RE',
                'country_name' => 'Réunion',
                'country_code' => '262',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => 182,
                'code' => 'RO',
                'country_name' => 'Romania',
                'country_code' => '40',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => 183,
                'code' => 'RU',
                'country_name' => 'Russian Federation',
                'country_code' => '7',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => 184,
                'code' => 'RW',
                'country_name' => 'Rwanda',
                'country_code' => '250',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => 185,
                'code' => 'BL',
                'country_name' => 'Saint Barthélemy',
                'country_code' => '590',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => 186,
                'code' => 'SH',
                'country_name' => 'Saint Helena, Ascension and Tristan da Cunha',
                'country_code' => '290',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => 187,
                'code' => 'KN',
                'country_name' => 'Saint Kitts and Nevis',
                'country_code' => '1-869',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => 188,
                'code' => 'LC',
                'country_name' => 'Saint Lucia',
                'country_code' => '1-758',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => 189,
                'code' => 'MF',
            'country_name' => 'Saint Martin (French part)',
                'country_code' => '590',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => 190,
                'code' => 'PM',
                'country_name' => 'Saint Pierre and Miquelon',
                'country_code' => '508',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => 191,
                'code' => 'VC',
                'country_name' => 'Saint Vincent and the Grenadines',
                'country_code' => '1-784',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => 192,
                'code' => 'WS',
                'country_name' => 'Samoa',
                'country_code' => '685',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => 193,
                'code' => 'SM',
                'country_name' => 'San Marino',
                'country_code' => '378',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => 194,
                'code' => 'ST',
                'country_name' => 'Sao Tome and Principe',
                'country_code' => '239',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => 195,
                'code' => 'SA',
                'country_name' => 'Saudi Arabia',
                'country_code' => '966',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => 196,
                'code' => 'SN',
                'country_name' => 'Senegal',
                'country_code' => '221',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => 197,
                'code' => 'RS',
                'country_name' => 'Serbia',
                'country_code' => '381',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => 198,
                'code' => 'SC',
                'country_name' => 'Seychelles',
                'country_code' => '248',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => 199,
                'code' => 'SL',
                'country_name' => 'Sierra Leone',
                'country_code' => '232',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => 200,
                'code' => 'SG',
                'country_name' => 'Singapore',
                'country_code' => '65',
                'default_country' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => 201,
                'code' => 'SX',
            'country_name' => 'Sint Maarten (Dutch part)',
                'country_code' => '1-721',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => 202,
                'code' => 'SK',
                'country_name' => 'Slovakia',
                'country_code' => '421',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => 203,
                'code' => 'SI',
                'country_name' => 'Slovenia',
                'country_code' => '386',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => 204,
                'code' => 'SB',
                'country_name' => 'Solomon Islands',
                'country_code' => '677',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => 205,
                'code' => 'SO',
                'country_name' => 'Somalia',
                'country_code' => '252',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => 206,
                'code' => 'ZA',
                'country_name' => 'South Africa',
                'country_code' => '27',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => 207,
                'code' => 'GS',
                'country_name' => 'South Georgia and the South Sandwich Islands',
                'country_code' => '500',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => 208,
                'code' => 'SS',
                'country_name' => 'South Sudan',
                'country_code' => '211',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => 209,
                'code' => 'ES',
                'country_name' => 'Spain',
                'country_code' => '34',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => 210,
                'code' => 'LK',
                'country_name' => 'Sri Lanka',
                'country_code' => '94',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => 211,
                'code' => 'SD',
                'country_name' => 'Sudan',
                'country_code' => '249',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => 212,
                'code' => 'SR',
                'country_name' => 'Suricountry_name',
                'country_code' => '597',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => 213,
                'code' => 'SJ',
                'country_name' => 'Svalbard and Jan Mayen',
                'country_code' => '47',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => 214,
                'code' => 'SZ',
                'country_name' => 'Swaziland',
                'country_code' => '268',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => 215,
                'code' => 'SE',
                'country_name' => 'Sweden',
                'country_code' => '46',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => 216,
                'code' => 'CH',
                'country_name' => 'Switzerland',
                'country_code' => '41',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => 217,
                'code' => 'SY',
                'country_name' => 'Syrian Arab Republic',
                'country_code' => '963',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => 218,
                'code' => 'TW',
                'country_name' => 'Taiwan',
                'country_code' => '886',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => 219,
                'code' => 'TJ',
                'country_name' => 'Tajikistan',
                'country_code' => '992',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => 220,
                'code' => 'TZ',
                'country_name' => 'Tanzania, United Republic of',
                'country_code' => '255',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => 221,
                'code' => 'TH',
                'country_name' => 'Thailand',
                'country_code' => '66',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => 222,
                'code' => 'TL',
                'country_name' => 'Timor-Leste',
                'country_code' => '670',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => 223,
                'code' => 'TG',
                'country_name' => 'Togo',
                'country_code' => '228',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => 224,
                'code' => 'TK',
                'country_name' => 'Tokelau',
                'country_code' => '690',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => 225,
                'code' => 'TO',
                'country_name' => 'Tonga',
                'country_code' => '676',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => 226,
                'code' => 'TT',
                'country_name' => 'Trinidad and Tobago',
                'country_code' => '1-868',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => 227,
                'code' => 'TN',
                'country_name' => 'Tunisia',
                'country_code' => '216',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => 228,
                'code' => 'TR',
                'country_name' => 'Turkey',
                'country_code' => '90',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => 229,
                'code' => 'TM',
                'country_name' => 'Turkmenistan',
                'country_code' => '993',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => 230,
                'code' => 'TC',
                'country_name' => 'Turks and Caicos Islands',
                'country_code' => '1-649',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => 231,
                'code' => 'TV',
                'country_name' => 'Tuvalu',
                'country_code' => '688',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => 232,
                'code' => 'UG',
                'country_name' => 'Uganda',
                'country_code' => '256',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => 233,
                'code' => 'UA',
                'country_name' => 'Ukraine',
                'country_code' => '380',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => 234,
                'code' => 'AE',
                'country_name' => 'United Arab Emirates',
                'country_code' => '971',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => 235,
                'code' => 'GB',
                'country_name' => 'United Kingdom',
                'country_code' => '44',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => 236,
                'code' => 'US',
                'country_name' => 'United States',
                'country_code' => '1',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => 237,
                'code' => 'UM',
                'country_name' => 'United States Minor Outlying Islands',
                'country_code' => '1',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => 238,
                'code' => 'UY',
                'country_name' => 'Uruguay',
                'country_code' => '598',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => 239,
                'code' => 'UZ',
                'country_name' => 'Uzbekistan',
                'country_code' => '998',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => 240,
                'code' => 'VU',
                'country_name' => 'Vanuatu',
                'country_code' => '678',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => 241,
                'code' => 'VE',
                'country_name' => 'Venezuela, Bolivarian Republic of',
                'country_code' => '58',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => 242,
                'code' => 'VN',
                'country_name' => 'Viet Nam',
                'country_code' => '84',
                'default_country' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => 245,
                'code' => 'WF',
                'country_name' => 'Wallis and Futuna',
                'country_code' => '681',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => 246,
                'code' => 'EH',
                'country_name' => 'Western Sahara',
                'country_code' => '212',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => 247,
                'code' => 'YE',
                'country_name' => 'Yemen',
                'country_code' => '967',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => 248,
                'code' => 'ZM',
                'country_name' => 'Zambia',
                'country_code' => '260',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => 249,
                'code' => 'ZW',
                'country_name' => 'Zimbabwe',
                'country_code' => '263',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            )
        ));
        
        
    }
}