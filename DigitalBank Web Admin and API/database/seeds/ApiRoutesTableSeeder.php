<?php

use Illuminate\Database\Seeder;

class ApiRoutesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('api_routes')->insert(
            [
                'route_name' => 'registerUser',
                'route_path' => '/register'
            ],
            [
                'route_name' => 'LoginUser',
                'route_path' => '/login'
            ],
            [
                'route_name' => 'forgotPassword',
                'route_path' => '/user/forgotPassword.json'
            ],
            [
                'route_name' => 'checkAvailability',
                'route_path' => '/check_availability.json'
            ],
            [
                'route_name' => 'sendUserNotification',
                'route_path' => '/send/device_notification.json'
            ],
            [
                'route_name' => 'getAllAvtars',
                'route_path' => '/user/avtars.json'
            ],
            [
                'route_name' => 'getAllCountryDetails',
                'route_path' => '/country_details.json'
            ],
            [
                'route_name' => 'UserDetails',
                'route_path' => '/user/details.json'
            ],
            [
                'route_name' => 'getAllParentsData',
                'route_path' => '/child/parents.json'
            ],
            [
                'route_name' => 'getAllChildData',
                'route_path' => '/parent/child.json'
            ],
            [
                'route_name' => 'changePassword',
                'route_path' => '/user/changePassword.json'
            ],
            [
                'route_name' => 'updateProfile',
                'route_path' => '/user/updateProfile.json'
            ],
            [
                'route_name' => 'updateProfilePic',
                'route_path' => '/user/profilePicUpdate.json'
            ],
            [
                'route_name' => 'childList',
                'route_path' => '/user/childlist.json'
            ],
            [
                'route_name' => 'getAllParents',
                'route_path' => '/parents.json'
            ],
            [
                'route_name' => 'getAllChild',
                'route_path' => '/child.json'
            ],
            [
                'route_name' => 'childDashboardDeatails',
                'route_path' => '/child/dashboard.json'
            ],
            [
                'route_name' => 'addRequest',
                'route_path' => '/user/request.json'
            ],
            [
                'route_name' => 'userResponse',
                'route_path' => '/user/response.json'
            ],
            [
                'route_name' => 'addChild',
                'route_path' => '/child/tag.json'
            ],
            [
                'route_name' => 'searchUser',
                'route_path' => '/user/search.json'
            ],
            [
                'route_name' => 'getAllRequest',
                'route_path' => '/user/notification.json'
            ],
            [
                'route_name' => 'getAllNotifications',
                'route_path' => '/user_notification.json'
            ],
            [
                'route_name' => 'stripePayment',
                'route_path' => '/user/payment.json'
            ],
            [
                'route_name' => 'getAllPackages',
                'route_path' => '/user/packages.json'
            ],
            [
                'route_name' => 'getAllTransaction',
                'route_path' => '/user/passbook.json'
            ],
            [
                'route_name' => 'getAllCreditTransaction',
                'route_path' => '/user/earn_list.json'
            ],
            [
                'route_name' => 'getAllDebitTransaction',
                'route_path' => '/user/spent_list.json'
            ],
            [
                'route_name' => 'getTransactionDetails',
                'route_path' => '/user/transaction_details.json'
            ],
            [
                'route_name' => 'referFriendRequest',
                'route_path' => '/user/refer_friend.json'
            ],
            [
                'route_name' => 'userLogout',
                'route_path' => '/user/logout.json'
            ],
            [
                'route_name' => 'getUserAccountDetails',
                'route_path' => '/user/account_details.json'
            ],
            [
                'route_name' => 'pointDisbursalToChild',
                'route_path' => '/user/point_disbursal.json'
            ],
            [
                'route_name' => 'setKidDefaultSetting',
                'route_path' => '/parent/kid_setting.json'
            ],
            [
                'route_name' => 'deviceRegisterFcm',
                'route_path' => '/user/device_register.json'
            ],
            [
                'route_name' => 'userProductRedemption',
                'route_path' => '/user/product_redemption.json'
            ],
            [
                'route_name' => 'getAllTasks',
                'route_path' => '/tasks.json'
            ],
            [
                'route_name' => 'getSingleTask',
                'route_path' => '/tasks/{id}.json'
            ],
            [
                'route_name' => 'searchTask',
                'route_path' => '/task/search.json'
            ],
            [
                'route_name' => 'SubmitTask',
                'route_path' => '/tasks/{id}/answer.json'
            ],
            [
                'route_name' => 'addAppreciationOnTask',
                'route_path' => 'tasks/{id}/addBonusPoint.json'
            ],
            [
                'route_name' => 'getAllLessons',
                'route_path' => '/lessons.json'
            ],
            [
                'route_name' => 'SubmitLesson',
                'route_path' => '/lesson/{id}/answer.json'
            ],
            [
                'route_name' => 'getCurrentDayLesson',
                'route_path' => '/lessons_of_day.json'
            ],
            [
                'route_name' => 'lessonHistory',
                'route_path' => '/user/lesson_history.json'
            ],
            [
                'route_name' => 'getAllCategories',
                'route_path' => '/categories.json'
            ],
            [
                'route_name' => 'searchCategory',
                'route_path' => '/category/search.json'
            ],
            [
                'route_name' => 'getSingleCategories',
                'route_path' => '/categories/{id}.json'
            ],
            [
                'route_name' => 'getCategoriesTasks',
                'route_path' => '/categories/{id}/tasks.json'
            ],
            [
                'route_name' => 'addAppreciationOnCategories',
                'route_path' => 'categories/{id}/addBonusPoint.json'
            ],
            [
                'route_name' => 'getAllAuction',
                'route_path' => '/auction/categories.json'
            ],
            [
                'route_name' => 'getAllProducts',
                'route_path' => '/auction/products.json'
            ],
            [
                'route_name' => 'getAllCategoryProducts',
                'route_path' => '/category/{id}/products.json'
            ],
            [
                'route_name' => 'addWishlistProduct',
                'route_path' => '/user/addwishlist.json'
            ],
            [
                'route_name' => 'deleteWishlistProduct',
                'route_path' => '/user/removewishlist.json'
            ],
            [
                'route_name' => 'getAllWishlistProducts',
                'route_path' => '/user/wishlistproducts.json'
            ],
            [
                'route_name' => 'getAllChildDetails',
                'route_path' => '/child/{id}/details.json'
            ],
            [
                'route_name' => 'getChildWishlist',
                'route_path' => '/child/{id}/wishlist.json'
            ],
            [
                'route_name' => 'getChildSpentlist',
                'route_path' => '/child/{id}/spentlist.json'
            ],
            [
                'route_name' => 'getChildEarnlist',
                'route_path' => '/child/{id}/earnlist.json'
            ],
            [
                'route_name' => 'getAllAuctions',
                'route_path' => '/auctions.json'
            ],
            [
                'route_name' => 'auctionHistory',
                'route_path' => '/user/auction_history.json'
            ],
            [
                'route_name' => 'userParticipateInAuction',
                'route_path' => '/auction/{id}/participate.json'
            ],
            [
                'route_name' => 'checkWishlistProducts',
                'route_path' => '/auction/{id}/check_wishlist.json'
            ],
            [
                'route_name' => 'setPriorityOfProducts',
                'route_path' => '/product/set_priority.json'
            ],
            [
                'route_name' => 'userRequestBid',
                'route_path' => '/user/bid.json'
            ],
            [
                'route_name' => 'getUserBidDetails',
                'route_path' => '/user/{id}/bid_details.json'
            ],
            [
                'route_name' => 'auctionBidDetails',
                'route_path' => '/auction/bid_details.json'
            ]
        );
    }
}