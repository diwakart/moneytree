<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//DB::table('category')->truncate();
    	$category = [
            ['category_name' => 'General Knowledge','status'=>'1'],
            ['category_name' => 'Reasoning','status'=>'1'],
        	['category_name' => 'English','status'=>'1']];
        DB::table('category')->insert($category);
    }
}
