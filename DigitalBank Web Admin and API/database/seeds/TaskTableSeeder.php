<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */ 
    public function run()
    {
        $category_id = DB::table('category')->select('id')->get()->toArray();
        $ids = array();
        foreach ($category_id as $key => $value) {
            array_push($ids,$value->id); 
        }
        if(in_array('1', $ids))
            {
                $data['category_id'] = $category_id[0]->id;
                $data['task_name'] = 'Capital of India _______';
                $data['question_type'] = '1';
                $data['task_points'] = '10';
                $data['attempt_1'] = '10';
                $data['attempt_2'] = '8';
                $data['attempt_3'] = '6';
                $data['above_3'] = '4';
                DB::table('task')->insert($data);
                //return $data;
            }
            if(in_array('2', $ids))
            {
                $data['category_id'] = $category_id[1]->id;
                $data['task_name'] = 'CMM, EOO, GQQ, _____, KUU';
                $data['question_type'] = '2';
                $data['task_points'] = '10';
                $data['attempt_1'] = '10';
                $data['attempt_2'] = '8';
                $data['attempt_3'] = '6';
                $data['above_3'] = '4';
                DB::table('task')->insert($data);
                $data['category_id'] = $category_id[1]->id;
                $data['task_name'] = '1_3_5_7_9';
                $data['question_type'] = '1';
                $data['task_points'] = '10';
                $data['attempt_1'] = '10';
                $data['attempt_2'] = '8';
                $data['attempt_3'] = '6';
                $data['above_3'] = '4';
                DB::table('task')->insert($data);
            }
            if(in_array('3', $ids))
            {
                $data['category_id'] = $category_id[2]->id;
                $data['task_name'] = 'Please arrange in Descending order';
                $data['question_type'] = '3';
                $data['task_points'] = '10';
                $data['attempt_1'] = '10';
                $data['attempt_2'] = '8';
                $data['attempt_3'] = '6';
                $data['above_3'] = '4';
                DB::table('task')->insert($data);
            }
        // foreach ($category_id as $key => $value) {
        // 	$data['category_id'] = $value->id;
        //     $data['task_name'] = '1_3_5_7_9';
        //     $data['question_type'] = '1';
        // 	$data['task_points'] = '10';
        // 	DB::table('task')->insert($data);
        // }
    }
}
