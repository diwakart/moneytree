<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'mobile' => '8960850272',
            'parent_id' => '1',
            'type' => '1',
            'password' => bcrypt('admin@123'),
        ]);
    }
}
