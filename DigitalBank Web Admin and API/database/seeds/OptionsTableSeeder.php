<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task_data = DB::table('task')->select('id')->get()->toArray();
        //$task_option = DB::table('task')->select('id','task_id')->get()->toArray();
        $ids = array();
        foreach ($task_data as $key => $value) {
            array_push($ids,$value->id); 
        }
        if(in_array('1', $ids))
        {
            $data['task_id'] = $task_data[0]->id;
            $data['options'] = '';
            $data['correct_options'] = 'Delhi';
            $data['status'] = '1';
            DB::table('option')->insert($data);
            //return $data;
        }
        if(in_array('2', $ids))
        {
            $data['task_id'] = $task_data[1]->id;
            $data['options'] = 'GRR';
            $data['correct_options'] = '0';
            $data['status'] = '1';
            DB::table('option')->insert($data);
            $data['task_id'] = $task_data[1]->id;
            $data['options'] = 'GSS';
            $data['correct_options'] = '0';
            $data['status'] = '1';
            DB::table('option')->insert($data);
            $data['task_id'] = $task_data[1]->id;
            $data['options'] = 'ISS';
            $data['correct_options'] = '1';
            $data['status'] = '1';
            DB::table('option')->insert($data);
            $data['task_id'] = $task_data[1]->id;
            $data['options'] = 'ITT';
            $data['correct_options'] = '0';
            $data['status'] = '1';
            DB::table('option')->insert($data);
            //return $data;
        }
        if(in_array('3', $ids))
        {
            $data['task_id'] = $task_data[2]->id;
            $data['options'] = '';
            $data['correct_options'] = '2';
            $data['status'] = '1';
            DB::table('option')->insert($data);
            $data['task_id'] = $task_data[2]->id;
            $data['options'] = '';
            $data['correct_options'] = '4';
            $data['status'] = '1';
            DB::table('option')->insert($data);
            $data['task_id'] = $task_data[2]->id;
            $data['options'] = '';
            $data['correct_options'] = '6';
            $data['status'] = '1';
            DB::table('option')->insert($data);
            $data['task_id'] = $task_data[2]->id;
            $data['options'] = '';
            $data['correct_options'] = '8';
            $data['status'] = '1';
            DB::table('option')->insert($data);
            //return $data;
        }
        if(in_array('4', $ids))
        {
            $data['task_id'] = $task_data[3]->id;
            $data['options'] = '5';
            $data['correct_options'] = '1';
            $data['status'] = '1';
            DB::table('option')->insert($data);
            $data['task_id'] = $task_data[3]->id;
            $data['options'] = '6';
            $data['correct_options'] = '2';
            $data['status'] = '1';
            DB::table('option')->insert($data);
            $data['task_id'] = $task_data[3]->id;
            $data['options'] = '2';
            $data['correct_options'] = '5';
            $data['status'] = '1';
            DB::table('option')->insert($data);
            $data['task_id'] = $task_data[3]->id;
            $data['options'] = '1';
            $data['correct_options'] = '6';
            $data['status'] = '1';
            DB::table('option')->insert($data);
            //return $data;
        }
    }
}
