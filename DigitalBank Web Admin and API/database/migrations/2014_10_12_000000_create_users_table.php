<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->bigInteger('mobile')->nullable();
            $table->integer('parent_id')->unsigned();
            //$table->foreign('parent_id')->references('id')->on('users');
            $table->integer('refer_id')->nullable();
            $table->text('avtar')->nullable();
            $table->enum('type', ['1', '2','3'])->default('2')->comment('1 => Admin, 2 => User , 3 => Parent');
            $table->string('password');
            $table->string('school_name')->nullable();
            $table->string('father_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->date('dob')->nullable();
            $table->text('address')->nullable();
            $table->string('access_token')->nullable();
            $table->string('refresh_token')->nullable();
            $table->enum('status', ['1', '2'])->default('1')->comment('1 => Active, 2 => Inactive');
            $table->rememberToken();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        DB::unprepared('
                CREATE TRIGGER account AFTER INSERT ON dg_users
                    FOR EACH ROW BEGIN
                    IF (NEW.type = 1 OR NEW.type = 3) THEN
                        INSERT INTO  dg_account(user_id, account_type, current_point, buffer_point, status) VALUES(NEW.id, 1,0,0,1);
                    ELSE
                        INSERT INTO  dg_account(user_id, account_type, current_point, buffer_point, status) VALUES(NEW.id, 1,0,0,1);
                        INSERT INTO  dg_account(user_id, account_type, current_point, buffer_point, status) VALUES(NEW.id, 2,0,0,1);
                        INSERT INTO dg_account(user_id, account_type, current_point, buffer_point, status) VALUES(NEW.id, 3,0,0,1);
                    END IF;
                END
        ');
        DB::unprepared('
                CREATE FUNCTION get_balance(user_id INT(10), type enum("1", "2", "3")) RETURNS int(10)
                DETERMINISTIC
                RETURN 
                (SELECT (dg_account.current_point-dg_account.buffer_point) FROM dg_account WHERE dg_account.account_type=type AND dg_account.user_id=user_id)
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        DB::unprepared('DROP EVENT IF EXISTS update_auction_status');
        DB::unprepared('DROP FUNCTION IF EXISTS get_balance');
        DB::unprepared('DROP TRIGGER IF EXISTS account');
    }
}
