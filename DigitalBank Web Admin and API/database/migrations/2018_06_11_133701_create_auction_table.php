<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuctionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auction_category_id')->unsigned();
            $table->foreign('auction_category_id')->references('id')->on('auction_categories')->onDelete('cascade')->onUpdate('cascade');
            $table->string('auction_price');
            $table->datetime('start_time');
            $table->datetime('end_time');
            $table->integer('single_bid_value');
            $table->integer('total_bids')->default('0');
            $table->integer('current_auction_value');
            $table->datetime('auction_closed_at');
            $table->integer('last_bid_user');
            $table->enum('incremented_bid_time', ['1', '2','3'])->comment('1,2,3 means increment time in minute');
            $table->enum('status', ['1', '2','3','4'])->default('2')->comment('1 => Active, 2 => Inactive,3=>In progress,4=>Complete');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
        \DB::statement("ALTER TABLE dg_auction MODIFY auction_closed_at DATETIME(6)");
        DB::unprepared("
                CREATE EVENT update_auction_status ON SCHEDULE EVERY 1 MINUTE STARTS '2018-07-12 16:03:23' ON COMPLETION NOT PRESERVE ENABLE DO 
                BEGIN
                UPDATE dg_auction SET status=3 WHERE status=1 AND start_time<=NOW(6);
                UPDATE dg_auction SET status=4 WHERE status=3 AND auction_closed_at<NOW(6);
                END
        ");
        DB::unprepared("
                CREATE EVENT update_transaction_log ON SCHEDULE EVERY 1 MONTH STARTS '2018-07-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO /*Update transaction log current month*/
                BEGIN
                INSERT INTO dg_transaction_log (SELECT * FROM dg_transaction WHERE MONTH(dg_transaction.created_at) = MONTH(CURRENT_DATE()));
                DELETE FROM dg_transaction WHERE MONTH(dg_transaction.created_at) = MONTH(CURRENT_DATE());
                END
        ");
        DB::unprepared('
                CREATE TRIGGER auction_bid_hash_entry BEFORE INSERT ON dg_auction
                FOR EACH ROW BEGIN
                INSERT INTO dg_auction_bid_hash (auction_id, bid_hash) VALUES(NEW.id,MD5(NOW()));
                END
        ');
        DB::unprepared("
                CREATE TRIGGER auction_bid_log AFTER UPDATE ON dg_auction
             FOR EACH ROW BEGIN
               UPDATE dg_auction_bid_hash SET bid_hash = MD5(NOW()) WHERE auction_id = OLD.id;
               IF(NEW.status = 4) THEN
               
               INSERT INTO dg_transaction SET user_id=NEW.last_bid_user,from_id=NEW.last_bid_user,to_id=1,account_type=1,transaction_points=NEW.current_auction_value,transaction_type=2,remarks='Auction Amount deduction';
               
               INSERT INTO dg_transaction SET user_id=1,from_id=NEW.last_bid_user,to_id=1,account_type=1,transaction_points=NEW.current_auction_value,transaction_type=1,remarks='Auction Amount deduction by user in Auction';
               
                  INSERT INTO dg_auction_bid_logs (user_id, auction_id, current_bid_value, last_bid_at, created_at, updated_at) SELECT dab.user_id,auction_id,dab.current_bid_value,dab.last_bid_at,dab.created_at,dab.updated_at FROM dg_auction_bids as dab WHERE dab.auction_id = NEW.id;
                  
                  DELETE FROM dg_auction_bids WHERE auction_id = NEW.id;
                  
                  INSERT INTO dg_notifications SET from_id=1,to_id=New.last_bid_user,message='Congratulation you Won the auction'; 
                  
                DELETE FROM dg_buffer_logs WHERE dg_buffer_logs.auction_id = NEW.id;
            END IF;

            END
        ");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS auction_bid_hash_entry');
        DB::unprepared('DROP TRIGGER IF EXISTS auction_bid_log');
        DB::unprepared('DROP EVENT IF EXISTS update_transaction_log');
        Schema::dropIfExists('auction');
    }
}
