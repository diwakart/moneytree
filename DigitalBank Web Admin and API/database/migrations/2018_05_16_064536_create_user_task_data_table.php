<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTaskDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_task_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('task_id')->unsigned();
            $table->foreign('task_id')->references('id')->on('task')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('parent_id');
            //$table->foreign('category_id')->references('id')->on('category');
            //$table->primary(['parent_id','user_id']);
            $table->integer('bonus_point');
            $table->enum('type', ['1','2'])->default('1')->comment('1 => normal, 2 => Depreciating');
            $table->string('depreciate_by')->nullable();
            $table->enum('depriciate_occurrence', ['Day', 'Month','Minute'])->nullable();
            $table->string('depreciate_value')->nullable();
            $table->dateTime('depreciate_start_at')->nullable();
            $table->string('total_attempt')->default('0');
            $table->enum('is_final_submitted', ['1','2'])->comment('1 => In Correct Answer, 2 => Correct Answer')->nullable();
            $table->dateTime('final_submission')->nullable();
            $table->enum('status', ['1', '2'])->default('1')->comment('1 => Active, 2 => Inactive');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_task_data');
    }
}
