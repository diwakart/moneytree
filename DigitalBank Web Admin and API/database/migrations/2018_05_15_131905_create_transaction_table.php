<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('from_id');
            $table->integer('to_id');
            $table->enum('account_type', ['1', '2','3'])->default('1')->comment('1 => Current, 2 => Saving , 3 => Money Jar');
            $table->integer('transaction_points')->unsigned();

            $table->enum('transaction_type', ['1', '2'])->comment('1 => Credit, 2 => Debit');
            $table->string('remarks')->nullable();
            //$table->integer('recorded_at');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        DB::unprepared('
                CREATE TRIGGER add_points AFTER INSERT ON dg_transaction
                FOR EACH ROW BEGIN
                    IF (NEW.transaction_type = 1) THEN
                        UPDATE dg_account SET current_point = (current_point +               NEW.transaction_points) WHERE user_id = NEW.user_id AND account_type = NEW.account_type;
                    ELSE
                        UPDATE dg_account SET current_point = (current_point - NEW.transaction_points) WHERE user_id = NEW.user_id AND account_type = NEW.account_type;
                    END IF;
                END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS add_points');
        Schema::dropIfExists('transaction');
    }
}
