<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuctionBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auction_bids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('auction_id')->unsigned();
            $table->foreign('auction_id')->references('id')->on('auction')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('current_bid_value')->nullable();
            $table->datetime('last_bid_at');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
        \DB::statement("ALTER TABLE dg_auction_bids MODIFY last_bid_at DATETIME(6)");
        DB::unprepared('
                CREATE TRIGGER check_balance_before_adding_bid BEFORE INSERT ON dg_auction_bids
                FOR EACH ROW BEGIN
   
                SET @auc_status=3;
                SET @cur_auc_value =0;
                SET @bid_value =0;
                SET @auc_closing_at = NOW(6);
                SELECT status, single_bid_value, current_auction_value, auction_closed_at INTO @auc_status, @bid_value, @cur_auc_value, @auc_closing_at FROM dg_auction WHERE id=NEW.auction_id;


                IF (get_balance(NEW.user_id,1)<NEW.current_bid_value) THEN
                    SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "Buffer points are more than user current avialable balance";
                    
                ELSEIF(@auc_status!=3) THEN 
                    SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "Auction is not available for bids.";
                 
                ELSEIF (@auc_closing_at < NOW(6)) THEN
                    SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "Auction is closed now. Bids not allowed.";
                ELSE 
                  SET NEW.current_bid_value=@cur_auc_value + @bid_value;
                  SET NEW.last_bid_at = NOW(6);
                END IF;

                END
        ');
        DB::unprepared('
                CREATE TRIGGER update_auction_details AFTER INSERT ON dg_auction_bids
                FOR EACH ROW BEGIN
                UPDATE dg_auction SET current_auction_value = (auction_price + ((total_bids+1)*single_bid_value)), total_bids = (total_bids + 1), last_bid_user = NEW.user_id, auction_closed_at = (IF(end_time > NOW(),end_time, TIMESTAMPADD(MINUTE,total_bids*incremented_bid_time,NOW()))) WHERE id = NEW.auction_id;

                UPDATE dg_buffer_logs SET transaction_points = (SELECT da.current_auction_value FROM dg_auction AS da WHERE da.id = NEW.auction_id) WHERE auction_id = NEW.auction_id AND from_id = NEW.user_id;
                END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS check_balance_before_adding_bid');
        DB::unprepared('DROP TRIGGER IF EXISTS update_auction_details');
        Schema::dropIfExists('auction_bids');
    }
}
