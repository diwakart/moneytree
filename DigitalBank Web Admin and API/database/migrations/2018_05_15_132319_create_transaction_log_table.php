<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('from_id');
            $table->integer('to_id');
            $table->enum('account_type', ['1', '2','3'])->default('1')->comment('1 => Current, 2 => Saving , 3 => Money Jar');
            $table->integer('transaction_points')->unsigned();

            $table->enum('transaction_type', ['1', '2'])->comment('1 => Credit, 2 => Debit');
            $table->string('remarks')->nullable();
            //$table->integer('recorded_at');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_log');
    }
}
