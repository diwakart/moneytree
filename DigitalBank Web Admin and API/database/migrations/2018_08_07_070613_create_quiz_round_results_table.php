<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizRoundResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_round_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->integer('school_id');
            $table->integer('question_id');
            $table->integer('round_id');
            $table->enum('check_answer', ['0', '1'])->nullable()->comment('1:correct 0:incorrect');
            $table->text('answered');
            $table->string('question_type');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_round_results');
    }
}
