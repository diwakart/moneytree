<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonOfDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_of_day', function (Blueprint $table) {
            $table->increments('id');
            $table->text('lesson_title');
            $table->text('file_attached')->nullable();
            $table->string('link_url')->nullable();
            $table->date('lesson_date');
            $table->text('lesson_description')->nullable();
            $table->enum('question_type', ['1', '2','3'])->comment('1 => Fill blank, 2 => multi,3 => Arrange')->nullable();
            $table->text('skipped_time')->nullable();
            $table->bigInteger('earn_points');
            $table->enum('status', ['1', '2'])->default('1')->comment('1 => Active, 2 => Inactive');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_of_day');
    }
}
