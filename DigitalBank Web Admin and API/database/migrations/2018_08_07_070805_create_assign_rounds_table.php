<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignRoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_rounds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('round_id');
            $table->integer('school_id');
            $table->integer('student_id');
            $table->enum('attended', ['0','1','2'])->default('0')->comment('0:not attended 1:attended 2:done');
            $table->enum('is_result_declared', ['1','2'])->default('1')->comment('1: not declared, 2 : declared');
            $table->timestamp('expiration_date')->nullable();
            $table->string('quiz_duration')->nullable();
            $table->datetime('quiz_started_at')->nullable();
            $table->integer('score')->default('0');
            $table->integer('total_question')->nullable();
            $table->string('quiz_name')->nullable();
            $table->string('round_name')->nullable();
            $table->integer('rank')->nullable();
            $table->integer('earn_points')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
        \DB::statement("ALTER TABLE dg_assign_rounds MODIFY quiz_started_at DATETIME(6)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_rounds');
    }
}
