<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBufferLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buffer_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_id')->unsigned();
            $table->integer('auction_id')->unsigned();
            //$table->foreign('task_id')->references('id')->on('task')->onDelete('cascade')->onUpdate('cascade');
            //$table->integer('user_id');
            $table->integer('from_id');
            $table->integer('to_id');
            $table->integer('transaction_points')->unsigned();
            //$table->string('transaction_type')->comment('1 => Credit, 2 => Debit');
            $table->string('remarks')->nullable();
            $table->enum('transaction_type', ['1', '2'])->comment('1 => Task, 2 => Auction');
            $table->enum('status', ['1', '2'])->default('1')->comment('1 => Active, 2 => Inactive');
            //$table->integer('recorded_at');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        DB::unprepared('
                CREATE TRIGGER check_balance_before_adding BEFORE INSERT ON dg_buffer_logs
                FOR EACH ROW BEGIN
                IF (get_balance(NEW.from_id,1)<NEW.transaction_points)
                    THEN 
                        SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "Buffer points are more than \tuser current avialable balance";
                END IF; 
                END
        ');
        DB::unprepared('
                CREATE TRIGGER update_buffer_balance_in_account AFTER DELETE ON dg_buffer_logs
                FOR EACH ROW BEGIN
                    UPDATE dg_account SET dg_account.buffer_point=(dg_account.buffer_point - OLD.transaction_points) WHERE dg_account.user_id=OLD.from_id AND dg_account.account_type=1;
                END
        ');
        DB::unprepared('
                CREATE TRIGGER update_buffer_points AFTER INSERT ON dg_buffer_logs
                FOR EACH ROW BEGIN
                UPDATE dg_account SET buffer_point = (buffer_point + NEW.transaction_points) WHERE user_id = NEW.from_id AND account_type = 1;
                END
        ');
        DB::unprepared('
            CREATE TRIGGER update_buffer_points_after_update AFTER UPDATE ON dg_buffer_logs
            FOR EACH ROW BEGIN
            IF(NEW.transaction_points != OLD.transaction_points AND (NEW.transaction_points-OLD.transaction_points)>0) THEN
            UPDATE dg_account SET buffer_point = (buffer_point + (NEW.transaction_points-OLD.transaction_points)) WHERE user_id = NEW.from_id AND account_type = 1;
            END IF;
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS check_balance_before_adding');
        DB::unprepared('DROP TRIGGER IF EXISTS update_buffer_balance_in_account');
        DB::unprepared('DROP TRIGGER IF EXISTS update_buffer_points');
        DB::unprepared('DROP TRIGGER IF EXISTS update_buffer_points_after_update');
        Schema::dropIfExists('buffer_logs');
    }
}
