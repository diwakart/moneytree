//== Class definition

var DatatableDataLocalDemo = function () {
  //== Private functions

  // demo initializer
  var studentdata;
  var datatable;
  var demo = function () {
    $.ajax({
      url: 'student/getAllStudents',
                beforeSend: function() {
                    setTimeout(function() {
                        
                        }, 2000);
                },
                success: function(response) { 
                  setTimeout(function() {
                    studentdata = response;
                    loaddatatable(studentdata);
                    console.log(studentdata);
                      }, 2000);
                }});
    function loaddatatable(constantdata)
    {
            var edit_url = 'student/edit';
            var delete_url = 'student/delete';
            var view_url = 'student/view';
      var dataJSONArray = studentdata;
      console.log(typeof dataJSONArray);
      var datatable = $('.m_datatable').mDatatable({
      // datasource definition
      data: {
        type: 'local',
        source: dataJSONArray,
        pageSize: 10
      },
      // layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        // height: 450, // datatable's body's fixed height
        footer: false // display/hide footer
      },

      // column sorting
      sortable: true,

      pagination: true,

      search: {
        input: $('#generalSearch')
      },

      // inline and bactch editing(cooming soon)
      // editable: false,

      // columns definition
      columns: [{
        field: "id",
        title: "ID",
        width: 100
      },
      {
          field: "name",
          title: "Name",
          textAlign: 'center'
      },
      {
        field: "email",
        title: "Email",
        textAlign: 'center'
       
      },
      {
          field: "contact",
          title: "Contact",
          textAlign: 'center'
      },
      {
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          field: 'Actions',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

            return '\
            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\
              <i class="la la-trash"></i>\
            </a>\
          ';
          },
        }
      ]
    });

    var query = datatable.getDataSourceQuery();

    $('#m_form_status').on('change', function () {
      datatable.search($(this).val(), 'Status');
    }).val(typeof query.Status !== 'undefined' ? query.Status : '');

    $('#m_form_type').on('change', function () {
      datatable.search($(this).val(), 'Type');
    }).val(typeof query.Type !== 'undefined' ? query.Type : '');

    $('#m_form_status, #m_form_type').selectpicker();

  };
}
  return {
    //== Public functions
    init: function () {
      // init dmeo
      demo();
      //handleSignInFormSubmit();
    }
  };
}();

jQuery(document).ready(function () {
  DatatableDataLocalDemo.init();
});