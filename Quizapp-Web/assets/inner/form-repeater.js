//== Class definition
var FormRepeater = function() {

    //== Private functions
    var demo1 = function() {
        $('#m_repeater_1').repeater({            
            initEmpty: false,
           
            defaultValues: {
                'text-input': 'foo'
            },
           
            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                if($(this).is(':first-child')){
                  swal('Warning','Atleast one option is compulsory','warning');
                }else{
                  $(this).slideUp(deleteElement);  
                }              
                               
            }   
        });
    }



    var demo2 = function() {
        var divcounter=1;

        $('#m_repeater_2').repeater({            
            initEmpty: false,
           
            defaultValues: {
                'text-input': 'foo'
            },
             
            show: function() {
                $(this).attr('id','clone'+divcounter); 
                $(this).slideDown(); 
                divcounter++;                              
            },

            hide: function(deleteElement) {                 
                if($(this).is(':first-child')){
                  swal('Warning','Atleast one part of question is compulsory','warning');
                }else{
                  $(this).slideUp(deleteElement);
                }                               
            }      
        });
        
    }


    var demo3 = function() {
        $('#m_repeater_3').repeater({            
            initEmpty: false,
           
            defaultValues: {
                'text-input': 'foo'
            },
             
            show: function() {
                $(this).slideDown();                               
            },

            hide: function(deleteElement) { 
                if($(this).is(':first-child')){
                  swal('Warning','Atleast one answer is compulsory','warning');
                }else{
                  $(this).slideUp(deleteElement);
                }                
                                                
            }      
        });
    }

    var demo4 = function() {
        $('#m_repeater_4').repeater({            
            initEmpty: false,
           
            defaultValues: {
                'text-input': 'foo'
            },
             
            show: function() {
                $(this).slideDown();                               
            },

            hide: function(deleteElement) {              
                $(this).slideUp(deleteElement);                                               
            }      
        });
    }

    var demo5 = function() {
        $('#m_repeater_5').repeater({            
            initEmpty: false,
           
            defaultValues: {
                'text-input': 'foo'
            },
             
            show: function() {
                $(this).slideDown();                               
            },

            hide: function(deleteElement) {              
                $(this).slideUp(deleteElement);                                               
            }      
        });
    }

     var demo6 = function() {
        $('#m_repeater_6').repeater({            
            initEmpty: false,
           
            defaultValues: {
                'text-input': 'foo'
            },
             
            show: function() {
                $(this).slideDown();                               
            },

            hide: function(deleteElement) {                  
                $(this).slideUp(deleteElement);                                                
            }      
        });
    }
    return {
        // public functions
        init: function() {
            demo1();
            demo2();
            demo3();
            demo4();
            demo5();
            demo6();
        }
    };
}();

jQuery(document).ready(function() {
    FormRepeater.init();
});

    