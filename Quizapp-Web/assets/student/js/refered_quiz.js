

var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].classList.add("display_block");
  x[n].classList.replace("display_none","display_block");
  //... and fix the Previous/Next buttons:

  if (n == 0) {
   
    document.getElementById("prevBtn").classList.remove("display_block");
    document.getElementById("prevBtn").classList.remove("display_inline");
    document.getElementById("prevBtn").classList.add("display_none");
  
  } else {
    document.getElementById("prevBtn").classList.remove("display_block");
    document.getElementById("prevBtn").classList.remove("display_none");
    document.getElementById("prevBtn").classList.add("display_inline");
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit Quiz";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {

    if(n == -1){

    }else{
      var tab_name = $('.display_block').attr('id');
    var quiz_round_id = $("#"+tab_name+" #quiz_round_id").val();
    var refered_by_id = $("#"+tab_name+" #refered_By").val();
    var question_id = $("#"+tab_name+" #question_id").val();
    var question_type = $("#"+tab_name+" #question_type").val();
    var auth_id = $("#auth_id").val();
    var answer_arr = [];
    if(quiz_round_id=='' || question_id=='' || question_type=='' ){
          swal("Error","Some Error Occured","error");
      }else{
       
       /*-========Question type Fill Blanks==========*/
        if(question_type==1){
          var i=0;
          $("#"+tab_name).find("#AnswersDiv input").each(function(){
              var inputanswer = $(this).val(); 
              console.log(inputanswer);
               answer_arr[i++] = inputanswer;
                   
          });


          if($("#"+tab_name).find("#AnswersDiv input").length != answer_arr.length){
            swal("Error","Fill all input boxes","error");
        }else{
          answers = answer_arr;
          $.ajax({
            type: 'POST',
            url: APP_URL+'/student/refer_submit_quiz',
            data: {
              quiz_round_id: quiz_round_id,
              question_id: question_id,
              question_type : question_type,
              answers: answer_arr,
              auth_id:auth_id,
              refered_by_id:refered_by_id
            },
             headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            success: function(data) {
              var res = $.parseJSON(data);
              if(res.status == 'error'){
                swal('Error',res.message,'error');
              }else{
               


              } 
            },
            error: function(data) {
              swal('Error',data,'error');
            }
          });

        }

        }
         /*-========End Question type Fill Blanks==========*/

        /*-========Question type Multi Choice==========*/
          if(question_type==2){
            var i=0;
          $("#"+tab_name).find(".quesDiv li").each(function(){
            if($(this).hasClass('active')){
              var inputanswer = $(this).text();
              answer_arr[i++]= inputanswer;
            }    
          });

          
           $.ajax({
              type: 'POST',
              url: APP_URL+'/student/refer_submit_quiz',
              data: {
                quiz_round_id: quiz_round_id,
                question_id: question_id,
                question_type : question_type,
                answers: answer_arr,
                auth_id:auth_id,
                refered_by_id:refered_by_id
              },
               headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
              success: function(data) {
                var res = $.parseJSON(data);
                if(res.status == 'error'){
                  swal('Error',res.message,'error');
                }else{
                 


                } 
              },
              error: function(data) {
                swal('Error',data,'error');
              }
            });

         

          }

        /*-========End : Question type Multi Choice==========*/

        /*-========Question type Multi Choice==========*/
          if(question_type==3){
            var i=0;
          $("#"+tab_name).find(".sort_order li").each(function(){
              var inputanswer = $(this).text();
              answer_arr[i++]= inputanswer;
             
          });

          
           $.ajax({
              type: 'POST',
              url: APP_URL+'/student/refer_submit_quiz',
              data: {
                quiz_round_id: quiz_round_id,
                question_id: question_id,
                question_type : question_type,
                answers: answer_arr,
                auth_id:auth_id,
                refered_by_id:refered_by_id
              },
               headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
              success: function(data) {
                var res = $.parseJSON(data);
                if(res.status == 'error'){
                  swal('Error',res.message,'error');
                }else{
                 


                } 
              },
              error: function(data) {
                swal('Error',data,'error');
              }
            });

         

          }

        /*-========End : Question type Multi Choice==========*/
    }
    

          
      }

  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  //if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  
    x[currentTab].classList.remove("display_block");
     x[currentTab].classList.remove("display_inline");
  x[currentTab].classList.add("display_none");
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {

   $(".animationload").css('display','block');
     setTimeout(function() {

      // ... the form gets submitted:
    var quiz_duration  = $("#timershow").text();
    var quiz_round_id = $("#"+tab_name+" #quiz_round_id").val();
    var auth_id = $("#auth_id").val();
     var refered_by_id = $("#"+tab_name+" #refered_By").val();
    var assign_round_id = $("#assign_round_id").val();
    var quiz_round_name = $("#quiz_round_name").val();
    var quiz_name = $("#quiz_name").val();

    $.ajax({
              type: 'POST',
              url: APP_URL+'/student/refer_calculate_score',
              data: {
                quiz_duration: quiz_duration,
                quiz_round_id: quiz_round_id,
                auth_id : auth_id,
                refered_by_id:refered_by_id,
                assign_round_id: assign_round_id,
                quiz_round_name:quiz_round_name,
                quiz_name: quiz_name,
              },
               headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
              success: function(data) {
                var res = $.parseJSON(data);
                if(res.status == 'error'){
                  swal('Error',res.message,'error');
                }else{
                   window.location.href = APP_URL+"/student/quiz-refer-result/"+quiz_round_id;
                } 
              },
              error: function(data) {
                swal('Error',data,'error');
              }
            });
   
  }, 500);


    

  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}



function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}


$(function () {
    $('.list-group.checked-list-box .list-group-item').each(function () {
        
        // Settings
        var $widget = $(this),
            $checkbox = $('<input type="checkbox" class="hidden" />'),
            color = ($widget.data('color') ? $widget.data('color') : "primary"),
            style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };
            
        $widget.css('cursor', 'pointer')
        $widget.append($checkbox);

        // Event Handlers
        $widget.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });
          

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $widget.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $widget.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$widget.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $widget.addClass(style + color + ' active');
            } else {
                $widget.removeClass(style + color + ' active');
            }
        }

        // Initialization
        function init() {
            
            if ($widget.data('checked') == true) {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
            }
            
            updateDisplay();

            // Inject the icon if applicable
            if ($widget.find('.state-icon').length == 0) {
                $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
            }
        }
        init();
    });
    
    $('#get-checked-data').on('click', function(event) {
        event.preventDefault(); 
        var checkedItems = {}, counter = 0;
        $("#check-list-box li.active").each(function(idx, li) {
            checkedItems[counter] = $(li).text();
            counter++;
        });
        $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
    });
});



 var seconds = 0, minutes = 0, hours = 0, t;

function add() {
    seconds++;
    if (seconds >= 60) {
        seconds = 0;
        minutes++;
        if (minutes >= 60) {
            minutes = 0;
            hours++;
        }
    }
    
   $('#timershow').text((hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds));

    timer();
}
function timer() {
    t = setTimeout(add, 1000);
}

$(document).ready(function() {
timer();
});


/*====disable f5=========*/
window.onload = function () {
        document.onkeydown = function (e) {
            return (e.which || e.keyCode) != 116;
        };
}


function showKeyCode(e) {
   // debugger;
    var keycode;
    if (window.event)
        keycode = window.event.keyCode;
    else if (e)
        keycode = e.which;

    // Mozilla firefox
    if ($.browser.mozilla) {
        if (keycode == 116 || (e.ctrlKey && keycode == 82)) {
            if (e.preventDefault) {
                e.preventDefault();
                e.stopPropagation();
            }
        }
    }
    // IE
    else if ($.browser.msie) {
        if (keycode == 116 || (window.event.ctrlKey && keycode == 82)) {
            window.event.returnValue = false;
            window.event.keyCode = 0;
            window.status = "Refresh is disabled";
        }
    }
    else {
        switch (e.keyCode) {

            case 116: // 'F5'
                event.returnValue = false;
                event.keyCode = 0;
                window.status = "Refresh is disabled";
                break;

        }
    }
}

/*===========disable back button===========*/
$(document).ready(function() {
        window.history.pushState(null, "", window.location.href);        
        window.onpopstate = function() {
            window.history.pushState(null, "", window.location.href);
        };
    });



/*============on reload tha page=========*/

/*window.onbeforeunload = setTimeout(function(){
  $(".animationload").css('display','block');
 submitQuiz();

 }, 1000); 

function tstdf(){
  console.log('sdf');
}*/

/*======submit on quit =============*/
$("#GoHome").on('click',function(){
   submitQuiz();
})



function submitQuiz(){
   
    var className = document.getElementsByClassName('que_ids');
    var classnameCount = className.length;
    var IdStore = new Array();
    for(var j = 0; j < classnameCount; j++){
        IdStore.push(className[j].id);
    }


    $.each( IdStore, function( index, value ){
      var tab_name = "tab_"+value;
      var quiz_round_id = $("#"+tab_name+" #quiz_round_id").val();
       var refered_by_id = $("#"+tab_name+" #refered_By").val();
      var question_id = $("#"+tab_name+" #question_id").val();
      var question_type = $("#"+tab_name+" #question_type").val();
      var auth_id = $("#auth_id").val();
      var answer_arr = [];
    if(quiz_round_id=='' || question_id=='' || question_type=='' ){
          swal("Error","Some Error Occured","error");
      }else{
       
       /*-========Question type Fill Blanks==========*/
        if(question_type==1){
          var i=0;
          $("#"+tab_name).find("#AnswersDiv input").each(function(){
              var inputanswer = $(this).val(); 
              console.log(inputanswer);
               answer_arr[i++] = inputanswer;
                   
          });


          if($("#"+tab_name).find("#AnswersDiv input").length != answer_arr.length){
            swal("Error","Fill all input boxes","error");
        }else{
          answers = answer_arr;
          $.ajax({
            type: 'POST',
            url: APP_URL+'/student/refer_submit_quiz_reload',
            data: {
              quiz_round_id: quiz_round_id,
              question_id: question_id,
              question_type : question_type,
              answers: answer_arr,
              auth_id:auth_id,
              refered_by_id:refered_by_id
            },
             headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            success: function(data) {
              var res = $.parseJSON(data);
              if(res.status == 'error'){
                swal('Error',res.message,'error');
              }else{
               


              } 
            },
            error: function(data) {
              swal('Error',data,'error');
            }
          });

        }

        }
         /*-========End Question type Fill Blanks==========*/

        /*-========Question type Multi Choice==========*/
          if(question_type==2){
            var i=0;
          $("#"+tab_name).find(".quesDiv li").each(function(){
            if($(this).hasClass('active')){
              var inputanswer = $(this).text();
              answer_arr[i++]= inputanswer;
            }    
          });

          
           $.ajax({
              type: 'POST',
              url: APP_URL+'/student/refer_submit_quiz_reload',
              data: {
                quiz_round_id: quiz_round_id,
                question_id: question_id,
                question_type : question_type,
                answers: answer_arr,
                auth_id:auth_id,
                refered_by_id:refered_by_id
              },
               headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
              success: function(data) {
                var res = $.parseJSON(data);
                if(res.status == 'error'){
                  swal('Error',res.message,'error');
                }else{
                 


                } 
              },
              error: function(data) {
                swal('Error',data,'error');
              }
            });

         

          }

        /*-========End : Question type Multi Choice==========*/

        /*-========Question type Multi Choice==========*/
          if(question_type==3){
            var i=0;
          $("#"+tab_name).find(".sort_order li").each(function(){
              var inputanswer = $(this).text();
              answer_arr[i++]= inputanswer;
             
          });

        
           $.ajax({
              type: 'POST',
              url: APP_URL+'/student/refer_submit_quiz_reload',
              data: {
                quiz_round_id: quiz_round_id,
                question_id: question_id,
                question_type : question_type,
                answers: answer_arr,
                auth_id:auth_id,
                refered_by_id:refered_by_id
              },
               headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
              success: function(data) {
                var res = $.parseJSON(data);
                if(res.status == 'error'){
                  swal('Error',res.message,'error');
                }else{
                 


                } 
              },
              error: function(data) {
                swal('Error',data,'error');
              }
            });

         

          }

        /*-========End : Question type Multi Choice==========*/

       }

    });



  $.when.apply($, IdStore).then(function(){
     $(".animationload").css('display','block');
     setTimeout(function() {

      // ... the form gets submitted:
    var quiz_duration  = $("#timershow").text();
    var quiz_round_id = $("#tab_"+IdStore[0]+" #quiz_round_id").val();
    var auth_id = $("#auth_id").val();
    var refered_by_id = $("#tab_"+IdStore[0]+" #refered_By").val();
    var assign_round_id = $("#assign_round_id").val();
    var quiz_round_name = $("#quiz_round_name").val();
    var quiz_name = $("#quiz_name").val();

    $.ajax({
              type: 'POST',
              url: APP_URL+'/student/refer_calculate_score_reload',
              data: {
                quiz_duration: quiz_duration,
                quiz_round_id: quiz_round_id,
                auth_id : auth_id,
                refered_by_id:refered_by_id,
                assign_round_id : assign_round_id,
                quiz_round_name:quiz_round_name,
                quiz_name: quiz_name,
              },
               headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
              success: function(data) {
                var res = $.parseJSON(data);
                if(res.status == 'error'){
                  swal('Error',res.message,'error');
                }else{
                   window.location.href = APP_URL+"/student/quiz-refer-result/"+quiz_round_id;
                } 
              },
              error: function(data) {
                swal('Error',data,'error');
              }
            });
   
  }, 500);
    
})


}