<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('Users')->insert([
          [ 
            'name' => 'MoneyTree',
            'email' => 'admin@webquiz.com',
            'country_code' => '+91',
            'contact' => '8888888888',
            'remember_token' => 'sQO42naS1qyE1xhfyLOAiYtcuqHM2ikk1unHIUpJgI2vKRYBaXQ6aXXrpTnI',
            'password' => Hash::make('secret'),
            'role' => '1',
          ]
        ]);
    }
}
