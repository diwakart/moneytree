<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\User;
use App\School;
use App\AssignRound;
use App\QuizRoundResult;
use App\PublishQuizRound;
use App\RegisteringStudent;
use Redirect;
use Session;
use Response;
use Image;
use Storage;
use Validator;
use Excel;
use DB;
use \Crypt;
use TextToNumber; 


class StudentController extends Controller
{
   public function index(){
     $schools = School::all();
     $countries = array('+60'=>'Malaysia (+60)','+91'=>'India (+91)','+44'=>'United Kingdom (+44)','+65'=>'Singapore (+65)','+1'=>'United State (+1)');

     return view('admin.student',compact('schools','countries'));
   }

    public function demopage(){
     return view('admin.student_demo');
   }

  public function getAllStudents(Request $request)
   {
    $perpage = $request->datatable['pagination']['perpage'];
    $page = $request->datatable['pagination']['page'];
    $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');

    /** WebQuiz changes for Digital Bank merger **/

    $mobile=(isset($request->datatable['query']['mobile']) && ($request->datatable['query']['mobile']!="") ? $request->datatable['query']['mobile'] : '');
    $student_id = (isset($request->datatable['query']['student_list']) && ($request->datatable['query']['student_list']!="all") ? $request->datatable['query']['student_list'] : '');
    $student_data = [];
    $allusers = new user();
    if($page=='1')
    {
      $offset = 0;
    }
    else{
      $offset = ($page-1)*$perpage;
    }
    
      /** WebQuiz changes for Digital Bank merger **/

      $allusers = $allusers
                      ->join('school_wq', 'users.school_id', '=', 'school_wq.id')
                      ->select('users.id as id','users.name as name','users.email as email','users.mobile as mobile','school_wq.name as school_name','users.school as school')
                      ->where(array('users.type'=>2));

     /** WebQuiz changes for Digital Bank merger **/
      
      $total = DB::table('users')
                      ->join('school_wq', 'users.school_id', '=', 'school.id')->select('users.*','school_wq.name as school_name')
                      ->where(array('users.type'=>2))->count();

      /** WebQuiz changes for Digital Bank merger **/
      
      if($student_id !='' && $mobile=='')
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        if($page =='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        $student_data_filter = [];
        $allusers = $allusers->where('school_wq.id',$student_id);

        /** WebQuiz changes for Digital Bank merger **/
        $total = DB::table('users')
                      ->join('school_wq', 'users.school_id', '=', 'school_wq.id')->select('users.*','school_wq.name as school_name')
                      ->where(array('users.type'=>2))->where('school_wq.id',$student_id)->orderby('id','asc')->count();
        // $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
        // return Response::json(array('data'=> $alluserslist,'meta'=> $meta));
      }

      /** WebQuiz changes for Digital Bank merger **/

      if($mobile!='' && $student_id!='')
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        if($page =='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        //$student_data_filter = [];

       /** WebQuiz changes for Digital Bank merger **/
        $allusers = $allusers->where('users.mobile',$mobile);
        $allusers = $allusers->where('school_wq.id',$student_id);

        /** WebQuiz changes for Digital Bank merger **/

        $total = DB::table('users')
                      ->join('school_wq', 'users.school_id', '=', 'school_wq.id')->select('users.*','school_wq.name as school_name')
                      ->where(array('users.type'=>2))->where('users.mobile',$mobile)->where('school_wq.id',$student_id)->count();
      }

      /** WebQuiz changes for Digital Bank merger **/

      if($mobile!='' && $student_id=='')
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        if($page =='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        //$student_data_filter = [];

        /** WebQuiz changes for Digital Bank merger **/

        $allusers = $allusers->where('users.mobile',$mobile);

        /** WebQuiz changes for Digital Bank merger **/

        $total = DB::table('users')
                      ->join('school_wq', 'users.school_id', '=', 'school_wq.id')->select('users.*','school_wq.name as school_name')
                      ->where(array('users.type'=>2))->where('users.mobile',$mobile)->count();
      }
      $allusers = $allusers->orderby('users.id','asc')->offset($offset)->limit($perpage)->get();
        $allusers = json_decode(json_encode($allusers));
      if(!empty($allusers))
      {
        $i = 1;
        foreach ($allusers as $key => $value) {
          $student_data[] = (array)$value;
          $student_data[$key]['S.no'] = $offset+1;

          /** WebQuiz changes for Digital Bank merger **/
          $student_data[$key]['mobile'] = $mobile;
          $offset++;
        }
      }
      $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
      return Response::json(array('data'=> $student_data,'meta'=> $meta));
   }

   public function store(Request $request)
    {
      if($request->ajax())
      {
        $level;

        $rules = array(
          'name'     => 'required',

          /** WebQuiz changes for Digital Bank merger **/

          'mobile'     => 'required',
          'email' => 'email',
          'country_code'     => 'required',
          'school'     => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        $level;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['name']))
            $errorMsg = $failedRules['name'][0] . "\n";
          if(isset($failedRules['country_code']))
            $errorMsg = $failedRules['country_code'][0] . "\n";

          /** WebQuiz changes for Digital Bank merger **/

          if(isset($failedRules['mobile']))
            $errorMsg = $failedRules['mobile'][0] . "\n";
          if(isset($failedRules['school']))
            $errorMsg = $failedRules['school'][0] . "\n";
            if(isset($failedRules['email']))
            $errorMsg = $failedRules['email'][0] . "\n";
          
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{
           $id=$request->id;
            if($request->email==''){

              /** WebQuiz changes for Digital Bank merger **/

              $check=User::where(['mobile'=>$request->mobile])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();
            }else{

              /** WebQuiz changes for Digital Bank merger **/

                $check=User::where(['email'=>$request->email,'mobile'=>$request->mobile])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();
            }
              
              if($check){
                  return (json_encode(array('status'=>'error','message'=>"Email or mobile already exist." )));
                }

              if($request->id){
                $user = User::find($request->id);

              }else{
                $user = new User();
                $user->password = Hash::make($request->password);

              }
        }

        $user->name = $request->name;

       /** WebQuiz changes for Digital Bank merger **/

        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->school_id = $request->school;
        $user->school = $request->other_school_name;
        $user->country_code = $request->country_code;
        try {

                 if($user->save()){

                  if($request->id==''){

                    $getallassignedquiz = AssignRound::where(['school_id'=>$user->school_id])->groupBy('round_id')->get();

                    if($getallassignedquiz->count()>0){
                    foreach ($getallassignedquiz as $value) {
                        $assign_round_data[] = [
                          'round_id' => $value->round_id,
                          'school_id' => $user->school_id,
                          'student_id' => $user->id,
                          'attended' => 0,
                          'is_result_declared' => $value->is_result_declared,
                          'expiration_date' => $value->expiration_date,
                          'quiz_duration' => "00:00:00.00",
                          'total_question' => $value->total_question,
                          'quiz_name' => $value->quiz_name,
                          'round_name' => $value->round_name,
                        ];
                        
                    }

                     DB::table('assign_round_wq')->insert($assign_round_data);
                   }
                      
                  }
                  return (json_encode(array('status'=>'success','message'=>sprintf('User "%s" successfully saved', $user->name))));
                 }
                  
                   
          }catch(\Illuminate\Database\QueryException $ex){ 
                   $error_code = $ex->errorInfo[1];
                   if($error_code == 1062){
                    $result = $ex->getMessage();
                    return (json_encode(array('status'=>'error','message'=>'Email or Contact already exist'))) ;
              }
            }
      }
    }


    public function exportCSV(){

      $filename = "students.csv";
      $handle = fopen($filename, 'w+');
      $School = School::where('name','!=','Other')->get();
      $school_names = array();
      foreach($School as $value) {
          $school_names[] = $value->name;
      }

      $array_first_row = array('Pick one of the value for Student School Column: ');
      $array_second_row = $school_names;
      $combine_first_row = array_merge($array_first_row,$array_second_row);

      $country_code_first = array('Pick one of the value for Country Code Column');
      $country_code_second = array('Malaysia : 60','India : 91', 'United Kingdom : 44','Singapore : 65','United State : 1');
      $combine_country_code = array_merge($country_code_first,$country_code_second);

      /** WebQuiz changes for Digital Bank merger **/


      fputcsv($handle, array('Name', 'Email', 'Password', 'Country Code','mobile', 'School'));
      fputcsv($handle, $combine_first_row );
      fputcsv($handle, $combine_country_code );
     
      fputcsv($handle, array('JohnDoe', 'john@gmail.com', 'johndoe123', '+91','8989898989','City Montessory School'));
      fclose($handle);

      $headers = array(
          'Content-Type' => 'text/csv',
      );

      return Response::download($filename, 'students.csv', $headers);
      
    }

 /* public function importCSV(Request $request)
  {

    if(Input::hasFile('csvfile')){
      $path = Input::file('csvfile')->getRealPath();
      $data = Excel::load($path, function($reader) {
      })->get();
      if(!empty($data) && $data->count()){
        
        foreach ($data as $key => $value) {


          if($value->name==''){
            Session::flash('errormessage', 'All rows for Name must be filled in your csv file'); 
              return back();
          }   
          if($value->mobile==''){
            Session::flash('errormessage', 'All rows for Contact must be filled in your csv file'); 
              return back();
          }
          if($value->email==''){
            Session::flash('errormessage', 'All rows for Email must be filled in your csv file'); 
              return back();
          }
          if($value->password==''){
            Session::flash('errormessage', 'All rows for Password must be filled in your csv file'); 
              return back();
          }


          $check=User::where(['email'=>$value->email,'contact'=>$value->contact])->exists();
           if($check){
              Session::flash('errormessage', 'Duplicate Email or Contact exist in your CSV File'); 
              return back();
           }else{
             $user = new User();
             $user->password = Hash::make($value->password);
             $user->name = $value->name;
             $user->email = $value->email;
             $user->contact = $value->contact;


           }

           

           
        }
        if($user->save()){
          Session::flash('successmessage', 'Student imported successfully'); 
          return back();
        }else{
          Session::flash('errormessage', 'Some error occured'); 
          return back();
        }
      }else{
        Session::flash('errormessage', 'No data available');
        return back(); 
      }
    }else{
      Session::flash('errormessage', 'File not selected'); 
      return back();
    }
    
  }*/

   public function importCSV(Request $request)
  {

    if(Input::hasFile('csvfile')){
      $School = School::all();
      $school_names = array();
      foreach($School as $value) {
          $school_names[] = $value->name;
      }
      $country_code_array = array('60','91', '44','65','1');
      $path = Input::file('csvfile')->getRealPath();
      $data = Excel::load($path, function($reader) {
      })->get()->toArray();
     
      
      $remaining_students = array(); 
     
     $csv_file='';
      $csv_file=time().'.'.$request->file('csvfile')->getClientOriginalExtension();
      $NewFilePath=$request->file('csvfile')->move(public_path('csv'), $csv_file);
      $insert_file = DB::table('import_csv_wq')->insert([
                              ['file_name' => $csv_file,'start_row'=>102,'end_row'=>202,'status'=>2,'file_type'=>'registered_student']
                      ]); 

     $output = array_slice($data, 2 , 100);
     if($insert_file){
      if(!empty($output) && count($output)){
        foreach ($output as $key => $value) {
       
          if (!in_array($value['country_code'], $country_code_array))
          {
            $remaining_students[] = ucwords($value['name']);
          }else{
            $country_code_value = '+'.$value['country_code'];
          }


          if($value['email']==''){

            /** WebQuiz changes for Digital Bank merger **/

            $check=User::where(['mobile'=>$value['mobile']])->exists();
          }else{

            /** WebQuiz changes for Digital Bank merger **/

             $check=User::where(['email'=>$value['email'],'mobile'=>$value['mobile']])->exists();
          }

          /** WebQuiz changes for Digital Bank merger **/
          
           if($value['name']=='' || $value['mobile']=='' || $value['password']=='' || $value['school']==''|| $value['country_code']=='' ||  $check){

              $remaining_students[] = ucwords($value['name']);

           }else{
            $school='';
             if (!in_array($value['school'], $school_names))
            {
                $school_name = School::where(['name'=>'Other'])->first();
                if(empty($school_name))
                {
                    $school_id = School::insertGetId(['name'=>'Other']);
                }
                else
                {
                    $school_id=$school_name->id;
                    $school = $value['school'];
                }
                

            }else{
               $school_name = School::where(['name'=>trim($value['school'])])->first();
               $school_id=$school_name->id;
            }


             $user = new User();
             $user->password = Hash::make($value['password']);
             $user->name = $value['name'];
             $user->email = $value['email'];

             /** WebQuiz changes for Digital Bank merger **/

             $user->mobile = $value['mobile'];
             $user->school_id = $school_id;
             $user->school = $school;
             $user->country_code = $country_code_value;

             try {
                
                  if($user->save()){

                    $getallassignedquiz = AssignRound::where(['school_id'=>$school_id,'is_result_declared'=>'1'])->where('expiration_date','>',NOW())->groupBy('round_id')->get();

                    if($getallassignedquiz->count()>0){
                    foreach ($getallassignedquiz as $value) {

                        $assign_round = new AssignRound();
                        
                          $assign_round->round_id = $value->round_id;
                          $assign_round->school_id = $user->school_id;
                          $assign_round->student_id = $user->id;
                          $assign_round->attended = 0;
                          $assign_round->is_result_declared = $value->is_result_declared;
                          $assign_round->expiration_date = $value->expiration_date;
                          $assign_round->total_question = $value->total_question;
                          $assign_round->quiz_name = $value->quiz_name;
                          $assign_round->round_name = $value->round_name;
                          $assign_round->save();
                        
                        
                    }



                     
                   }
                      
                  }
                  
          }catch(\Illuminate\Database\QueryException $ex){ 
                   $error_code = $ex->errorInfo[1];
                   if($error_code == 1062){
                    $result = $ex->getMessage();
                    $remaining_students[] = ucwords($value['name']);
              }
            }



           }

       



        
        }
       
        if(count($remaining_students)>0){
        
             $error_names =  array_unique($remaining_students, SORT_REGULAR);
              $students_name = implode(', ', $error_names);
             Session::flash('errormessage', 'Record Inserted Successfully. '.$students_name.' named student(s) may have empty row or duplicate email or mobile or may school name and country code are anot from the given list.Upload again after removing successfully inserted rows.');
             return back(); 
         }else{
            Session::flash('successmessage', 'Record Inserted Successfully.');
            return back(); 
         }

        Session::flash('successmessage', 'Student imported successfully'); 
        return back();


      }else{
        Session::flash('errormessage', 'No data available');
        return back(); 
      }
    }else{
      Session::flash('errormessage', 'Failed to Upload File');
        return back(); 
    }
    }else{
      Session::flash('errormessage', 'File not selected'); 
      return back();
    }
    
  }



  public function getstudentDetail(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = User::find($request->id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }



    public function getUserID(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = User::find($request->id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }


    public function changepassword(Request $request)
    {
      if($request->ajax())
      {
        $user;
        $rules = array(
          'password'     => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['password']))
            $errorMsg = $failedRules['password'][0] . "\n";
          
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        } else if($request->id){
          $user = User::find($request->id);
        }
        else{
          $user = new User();
          
        }

        $user->password = Hash::make($request->password);
        if($user->save()){
          return (json_encode(array('status'=>'success','message'=>sprintf('Passsword for User "%s" successfully saved', $user->name))));
          } else{
             return (json_encode(array('status'=>'error','message'=>'Failed to Change Password'))) ;
          }
      }
    }


     public function delete(Request $request)
    {
      if($request->ajax())
      {
        if($request->id){
          $user = User::find($request->id);
          if ($user->delete())
            return (json_encode(array('status'=>'success','message'=>"Student Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }
    }

public function getAssignedStudents($id)
    {
      $school = DB::table('school_wq')->where('id',$id)->first();
      return  view('admin.assigned_students',compact('school'));
    }

   public function getAssignedSchoolsStudents(Request $request,$school_id)
    {
      $perpage = $request->datatable['pagination']['perpage'];
      $page = $request->datatable['pagination']['page'];
      if($page=='1')
      {
        $offset = 0;
      }
      else
      {
        $offset = ($page-1)*$perpage;
      }
      $round_id= Session::get('roundid');
      $assigned_students = [];
      $allStudentsList = DB::table('users')->join('assign_round_wq', 'assign_round_wq.student_id', '=', 'users.id')->join('publish_quiz_round_wq', 'assign_round_wq.round_id', '=', 'publish_quiz_round_wq.id')->join('quiz_wq', 'publish_quiz_round_wq.quiz_id', '=', 'quiz_wq.id')->select('users.id as student_id','users.*','quiz_wq.*','publish_quiz_round_wq.*')->where(array('users.school_id'=>$school_id,'assign_round_wq.round_id'=>$round_id))->get();
      $total = DB::table('users')->join('assign_round_wq', 'assign_round_wq.student_id', '=', 'users.id')->join('publish_quiz_round_wq', 'assign_round_wq.round_id', '=', 'publish_quiz_round_wq.id')->join('quiz_wq', 'publish_quiz_round_wq.quiz_id', '=', 'quiz_wq.id')->select('users.id as student_id','users.*','quiz_wq.*','publish_quiz_round_wq.*')->where(array('users.school_id'=>$school_id,'assign_round_wq.round_id'=>$round_id))->count();
      $allStudentsList = json_decode(json_encode($allStudentsList));
      $meta = ['draw'=>1,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
      if(!empty($allStudentsList))
      {
        $i = 1;
        foreach ($allStudentsList as $key => $value) {
          $assigned_students[] = (array)$value;
          $assigned_students[$key]['S.no'] = $offset+1;
          $offset++;
        }
        return Response::json(array('data'=> $assigned_students,'meta'=> $meta));
        //return Response::json($assigned_students);
      }
      return Response::json(array('data'=> $allStudentsList,'meta'=> $meta));
      //return Response::json($allStudentsList);
    }


    public function register_student()
    {
      $schools = School::all();
      $countries = array('+60'=>'Malaysia (+60)','+91'=>'India (+91)','+44'=>'United Kingdom (+44)','+65'=>'Singapore (+65)','+1'=>'United State (+1)');
      return view('admin.register_student',compact('schools','countries'));
    }

    public function exportRegisteringStudentCSV(){

      $filename = "registering_students.csv";
      $handle = fopen($filename, 'w+');
      $School = School::where('name', '!=' , 'Other')->get();
      $school_names = array();
      foreach($School as $value) {
          $school_names[] = $value->name;
      }

      $array_first_row = array('Pick one of the value for Student School Column: ');
      $array_second_row = $school_names;
      $combine_first_row = array_merge($array_first_row,$array_second_row);

      $country_code_first = array('Pick one of the value for Country Code Column');
      $country_code_second = array('Malaysia : 60','India : 91', 'United Kingdom : 44','Singapore : 65','United State : 1');
      $combine_country_code = array_merge($country_code_first,$country_code_second);

      /** WebQuiz changes for Digital Bank merger **/

      fputcsv($handle, array('Name','Country Code','mobile', 'School'));
      fputcsv($handle, $combine_first_row );
      fputcsv($handle, $combine_country_code );
     
      fputcsv($handle, array('JohnDoe', '91','8989898989','MoneyTree'));
      fclose($handle);

      $headers = array(
          'Content-Type' => 'text/csv',
      );

      return Response::download($filename, 'registering_students.csv', $headers);
      
    }



     public function importRegisteringCSV(Request $request)
  {
    
    if(Input::hasFile('csvfile')){
      $School = School::all();
      $school_names = array();
      foreach($School as $value) {
          $school_names[] = $value->name;
      }
      $country_code_array = array('60','91', '44','65','1');
      $path = Input::file('csvfile')->getRealPath();
      $data = Excel::load($path, function($reader) {
      })->get()->toArray();

      $csv_file='';
      $csv_file=time().'.'.$request->file('csvfile')->getClientOriginalExtension();
      $NewFilePath=$request->file('csvfile')->move(public_path('csv'), $csv_file);
      $insert_file = DB::table('import_csv_wq')->insert([
                              ['file_name' => $csv_file,'start_row'=>102,'end_row'=>202,'status'=>2,'file_type'=>'non_registered_student']
                      ]);  
      $remaining_students = array();
      $output = array_slice($data, 2 , 100);
      if($insert_file){
      if(!empty($output) && count($output)){
      
        foreach ($output as $key => $value) {
       
      
          if($value['name']==''){
            $name = '';
          } else{
            $name =$value['name'];
          } 

          /** WebQuiz changes for Digital Bank merger **/
          
             $check_user=User::where(['mobile'=>$value['mobile']])->exists();

             /** WebQuiz changes for Digital Bank merger **/

             if($check_user || $value['mobile']=='' || $value['country_code']==''){
                 $remaining_students[] = $value['mobile'];
                if($value['school']!=''){
                  if (!in_array($value['school'], $school_names))
                  {

                    /** WebQuiz changes for Digital Bank merger **/

                    $remaining_students[] = $value['mobile'];
                  }
                }

                if (!in_array($value['country_code'], $country_code_array))
                {
                  /** WebQuiz changes for Digital Bank merger **/

                  $remaining_students[] = $value['mobile'];
                }
             }else{

              /** WebQuiz changes for Digital Bank merger **/

              $check=RegisteringStudent::where(['mobile'=>$value['mobile']])->exists();
             if($check ){

              /** WebQuiz changes for Digital Bank merger **/

                $remaining_students[] = $value['mobile'];
              }else{

                if($value['school']!=''){
                  if (in_array($value['school'], $school_names))
                  {
                    $school_name = $value['school'];
                  }
                }else{
                    $school_name = 'No School Added';
                }

                if (in_array($value['country_code'], $country_code_array))
                {
                     $country_code_value = '+'.$value['country_code'];
                }
                 
               //if(preg_match('/(?=^.{0,40}$)^[a-zA-Z]+(\s)?[a-zA-Z]+$/',$name
               if($name!=''
               ){
                    $name =$name;
                    $user = new RegisteringStudent();
                    $user->name = $name;

                    /** WebQuiz changes for Digital Bank merger **/
                    $user->mobile = $value['mobile'];
                    $user->school_name= $school_name;
                    $user->country_code = $country_code_value;
                     //$user->save();
    
                     if($user->save()){
      
                      /* $register_url_parameter = encrypt($user->name.'^'.$user->contact.'^'.$user->country_code);*/

                      /** WebQuiz changes for Digital Bank merger **/
    
                     $register_url_parameter = base64_encode(TextToNumber::text_to_number($user->name).'^'.TextToNumber::number_to_text(trim($user->mobile)).'^'.TextToNumber::number_to_text(trim($user->country_code)));
    
    
                      $message_text = "Here's your chance to win Amazing prizes! Register now for the Tune Talk QuizMe Challenge . Go to: ".url('/').'/register/'.$register_url_parameter;
    
                        $details['sms_from'] = '+15416159055';

                        /** WebQuiz changes for Digital Bank merger **/

                        $details['sms_to']   = $user->mobile;
                        $details['sms_to_name']   = $user->name;
                        $details['body_message'] = $message_text;
                        $details['priority']    = '2';
                        $details['country_code']   = $user->country_code;
                        DB::table('sms_queue_wq')->insert($details);
    
                      //Session::flash('successmessage', 'Student imported successfully'); 
                      //return back();
                      }else{

                        /** WebQuiz changes for Digital Bank merger **/
                          
                        $remaining_students[] = $value['mobile'];
                       }
                   
                } else {

                  /** WebQuiz changes for Digital Bank merger **/
                  
                     $remaining_students[] = $value['mobile'];
                }
                

                
                   
              }
             
             }

        

        
        }
       
       if(count($remaining_students)>0){
        
             $error_names =  array_unique($remaining_students, SORT_REGULAR);
              $students_name = implode(', ', $error_names);
             Session::flash('errormessage', 'Record Inserted Successfully. '.$students_name.' student(s) with mobile number may have empty row or duplicate mobile or may school name and country code are not from the given list or may contain name with extra space in CSV File.Upload again after removing successfully inserted rows.');
             return back(); 
         }else{
            Session::flash('successmessage', 'Record Inserted Successfully.');
            return back(); 
         }


      }else{
        Session::flash('errormessage', 'No data available');
        return back(); 
      }
    }else{
      Session::flash('errormessage', 'Failed to upload file'); 
      return back();
    }
    }else{
      Session::flash('errormessage', 'File not selected'); 
      return back();
    }
    
  }


  public function getAllRegisteringStudents(Request $request)
   {
    $perpage = $request->datatable['pagination']['perpage'];
    $page = $request->datatable['pagination']['page'];
    $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
    if($page=='1')
    {
      $offset = 0;
    }
    else{
      $offset = ($page-1)*$perpage;
    }
    $student_id = (isset($request->datatable['query']['student_list']) && ($request->datatable['query']['student_list']!="all") ? $request->datatable['query']['student_list'] : '');
      $student_data = [];

      /** WebQuiz changes for Digital Bank merger **/
      $allusers = DB::table('registering_student_wq')
                      ->select('id','name','mobile','school_name','country_code')->orderby('id','asc')->offset($offset)->limit($perpage)->get();

      /** WebQuiz changes for Digital Bank merger **/

      $total = DB::table('registering_student_wq')
                      ->select('id','name','mobile','school_name','country_code')->count();
      $allusers = json_decode(json_encode($allusers));
      if(!empty($allusers))
      {
        $i = 1;
        foreach ($allusers as $key => $value) {
          $student_data[] = (array)$value;
          $student_data[$key]['S.no'] = $offset+1;
          $offset++;
        }
      }
      $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
      if($student_id !='')
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        if($page =='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        $student_data_filter = [];

        /** WebQuiz changes for Digital Bank merger **/

        $alluserslist = DB::table('registering_student_wq')
                      ->select('id','name','mobile','school_name','country_code')
                      ->orderby('id','asc')->offset($offset)->limit($perpage)->get();

        /** WebQuiz changes for Digital Bank merger **/

        $total = DB::table('registering_student_wq')
                      ->select('id','name','mobile','school_name','country_code')
                     ->orderby('id','asc')->count();
        $alluserslist = json_decode(json_encode($alluserslist));
        if(!empty($alluserslist))
        {
          $i = 1;
          foreach ($alluserslist as $key => $value) {
            $student_data_filter[] = (array)$value;
            $student_data_filter[$key]['student_list'] = $student_id;
            $student_data_filter[$key]['S.no'] = $offset+1;
            $offset++;
          }
          $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
          return Response::json(array('data'=> $student_data_filter,'meta'=> $meta));
        }
        $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=> $alluserslist,'meta'=> $meta));
      }
      return Response::json(array('data'=> $student_data,'meta'=> $meta));
   }

    public function nonRegisterStudentDetails(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = RegisteringStudent::find($request->id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }

    public function updateDetails(Request $request)
    {
        if($request->ajax())
        {
          //$level;
        
          $rules = array(
            'name'     => 'required',

          /** WebQuiz changes for Digital Bank merger **/

            'mobile'     => 'required',
            'country_code'     => 'required',
            'school'     => 'required',
          );
        
          $validator = Validator::make($request->all(), $rules);
        
          //$level;
          if ($validator->fails()) {
            $failedRules = $validator->getMessageBag()->toArray();
            $errorMsg = "";
            if(isset($failedRules['name']))
              $errorMsg = $failedRules['name'][0] . "\n";
            if(isset($failedRules['country_code']))
              $errorMsg = $failedRules['country_code'][0] . "\n";

            /** WebQuiz changes for Digital Bank merger **/

            if(isset($failedRules['mobile']))
              $errorMsg = $failedRules['mobile'][0] . "\n";
            if(isset($failedRules['school']))
              $errorMsg = $failedRules['school'][0] . "\n";
            
            return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
        
          }
          else{
             $id=$request->id;

             /** WebQuiz changes for Digital Bank merger **/

              $check=RegisteringStudent::where(['mobile'=>$request->mobile])
                  ->where(function($query) use ($id){
                      if(isset($id)){
                        $query->where('id' , '<>' ,$id);
                      }
                  })->exists();
              if($check){

                /** WebQuiz changes for Digital Bank merger **/

                  return (json_encode(array('status'=>'error','message'=>"mobile already exist." )));
                }
                $user = RegisteringStudent::find($id);
          }
        
          $user->name = $request->name;

          /** WebQuiz changes for Digital Bank merger **/

          $user->mobile = $request->mobile;
          $user->school_name = $request->school;
          $user->country_code = $request->country_code;
          try {
                   $user->save();
                    return (json_encode(array('status'=>'success','message'=>sprintf('User "%s" successfully saved', $user->name))));
                     
            }catch(\Illuminate\Database\QueryException $ex){ 
                     $error_code = $ex->errorInfo[1];
                     if($error_code == 1062){
                      $result = $ex->getMessage();
                      return (json_encode(array('status'=>'error','message'=>'Contact already exist'))) ;
                }
              }
        }
    }
    public function registering_delete(Request $request)
    {
      if($request->ajax())
      {
        if($request->id){
          $user = RegisteringStudent::find($request->id);

          if ($user->delete())
            return (json_encode(array('status'=>'success','message'=>"Student Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }
    }


    public function bulkSmsToStudents(Request $request)
    {
      $student_id = $request->input('student_id');
      //$arr['id'] = array_values($student_id);
      //print_r($student_id);
      //die;
      $from = '+15416159055';
      $priority = '2';
      $body_message = htmlspecialchars('Hello {username} Please Register on this Given Link '.url('/').'/register');

      /** WebQuiz changes for Digital Bank merger **/

      $query  = DB::table('sms_queue_wq')->insert(RegisteringStudent::select(DB::raw("'".$from."' as sms_from,country_code as country_code,mobile as sms_to,name as sms_to_name,'".$body_message."' as body_message,'".$priority."' as priority"))->whereIn('id',$student_id)->get()->toArray());
      if($query)
      {
        return (json_encode(array('status'=>'success','message'=>"Mesaage Send Successfully")));
      }
    }
    
    public function reminderSmsToStudents(Request $request)
    {
      $student_id = $request->input('student_id');
      $message = $request->input('message');
      $from = '+15416159055';
      $priority = '2';
      //$body_message = 'Hello Please Register on this Link'.base_path('register');

      /** WebQuiz changes for Digital Bank merger **/
      
      $query  = DB::table('sms_queue_wq')->insert(User::select(DB::raw("'".$from."' as sms_from,country_code as country_code,mobile as sms_to,name as sms_to_name,'".$message."' as body_message,'".$priority."' as priority"))->whereIn('id',$student_id)->get()->toArray());
      if($query)
      {
        return (json_encode(array('status'=>'success','message'=>"Reminder Mesaage Send Successfully")));
      }
    }
}


 

