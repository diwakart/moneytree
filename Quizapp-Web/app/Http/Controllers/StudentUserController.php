<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\User;
use App\School;
use Redirect;
use Session;
use Response;
use Image;
use Auth;
use Storage;
use Validator;

class StudentUserController extends Controller
{
    public function index(){

      if(Auth::user()->school_name!=''){
      	$school_name = Auth::user()->school_name;
      }else{
      	$school = School::where(['id'=>Auth::user()->school_id])->first();
      	$school_name = $school->name;
      }
      return view('student.profile',compact('school_name'));
    }


    public function update_profile(Request $request)
    {
        
         if ($request->hasFile('picture'))
	      {
			 
		

         $validator = Validator::make($request->file(), [
                    'picture' => 'image|mimes:jpeg,png,jpg|max:10480',
                   
                ]);
        
                if ($validator->fails()) {
                    Session::flash('errormessage', 'Image size should be less than 10 MB'); 
        	         return Redirect::to(url('/').'/student/profile');
                }
	
	      }
	      
	      

        $id = $request->id;
        if($request->email==''){

        	/** WebQuiz changes for Digital Bank merger **/

        	$check=User::where(['mobile'=>$request->mobile])
        ->where(function($query) use ($id){
            if(isset($id)){
              $query->where('id' , '<>' ,$id);
            }
        })->exists();
        }else{

        	/** WebQuiz changes for Digital Bank merger **/

        	$check=User::orWhere(['email'=>$request->email,'mobile'=>$request->mobile])
	        ->where(function($query) use ($id){
	            if(isset($id)){
	              $query->where('id' , '<>' ,$id);
	            }
	        })->exists();
	      }
        

      if($check){
          Session::flash('errormessage', 'Email Address or Contact already exist.'); 
          return back();
        }else{
          if($id){
	        $user = User::find($id);
	      }else{
	        $user = new User();
	      }


	    $user->name = $request->name;
	    $user->email = $request->email;

	    /** WebQuiz changes for Digital Bank merger **/
	    
	    $user->mobile =$request->mobile;
	    $user->country_code =$request->country_code;
	    
	      if ($request->hasFile('picture'))
	      {
			 

       
		
	      	 $image = $request->file('picture');
		     $path = storage_path('app/profile');
		     $filename = time() . '.' . $image->getClientOriginalExtension();
		     $image->move($path, $filename);

		     $user->picture = 'profile/'.$filename;
	       
	      }

	     
	    if($user->save()){
	         Session::flash('successmessage', 'Profile Updated Successfully'); 
	        return Redirect::to(url('/').'/student/profile');
	        } else{
	         Session::flash('errormessage', 'Failed to update'); 
	         return Redirect::to(url('/').'/student/profile');

	      }
          
        }
    }
    
    
    public function update_password(Request $request)
    {
        $id = Auth::user()->id;
        if($request->password_text=='' && $request->confirm_password_text=='')
        {
            Session::flash('errormessage', 'Password and confirm password required'); 
            return Redirect::to(url('/').'/student/profile');
        }
        if($request->password_text != $request->confirm_password_text)
        {
            Session::flash('errormessage', 'Password and confirm password not match'); 
            return Redirect::to(url('/').'/student/profile');
        }
        if($id){
	        $user = User::find($id);
	        $user->password =  Hash::make($request->password_text);
	     
	        if($user->save()){
	         Session::flash('successmessage', 'Password Updated Successfully'); 
	        return Redirect::to(url('/').'/student/profile');
	        } else{
	         Session::flash('errormessage', 'Failed to update'); 
	         return Redirect::to(url('/').'/student/profile');
	       }
      }    
      
    }
    
    
}
