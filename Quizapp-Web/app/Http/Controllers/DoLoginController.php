<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\ReferQuiz;
use Auth;
use Cookie;
use \Crypt;
use Redirect;
use Session;

class DoLoginController extends Controller
{
	public function __construct() {
      $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri_segments = explode('/', $uri_path);
        if($uri_segments[3]!=='' && User::where('digital_key',$uri_segments[3])->exists())
        {
           $digital_key =  $uri_segments[3];
           $user_data = User::where('digital_key',$digital_key)->first();
           Auth::loginUsingId($user_data->id);
           if(Auth::user()->type=='2')
           {
              return redirect()->intended('/student/home'); 
           }
        }
        $credentials = [
            'mobile' => $request->mobile,
            'password' => $request->password
        ];   

	    if (auth()->attempt($credentials)) {
	    	$user = \App\User::find(auth::id());
	        if(Session::get('quiz_url')){
                $quiz_url = Session::get('quiz_url');

                preg_match("/[^\/]+$/", $quiz_url, $matches);
                $last_word = $matches[0];


                $refer_id = Crypt::decrypt($last_word);
              
                $refered_data = explode('^',$refer_id);
                $refer_quiz = ReferQuiz::find($refered_data[1]);
                $refer_quiz->student_id = $user->id;
                $refer_quiz->save();

               return redirect()->intended('/student/home');

	        }

	        /** WebQuiz changes for Digital Bank merger **/

	        if($user->type == 1) {
	            return redirect()->intended('/administration/home');
	        } elseif ($user->type == 2) {
	            return redirect()->intended('/student/home');
	        }
	    }
    }

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   /* protected $redirectTo = 'administration/home';*/

    /**
     * Create a new controller instance.
     *
     * @return void
     */


    protected function authenticated(Request $request,$user)
    {
         if(Session::get('quiz_url')){
                $quiz_url = Session::get('quiz_url');

                preg_match("/[^\/]+$/", $quiz_url, $matches);
                $last_word = $matches[0];


                $refer_id = Crypt::decrypt($last_word);
              
                $refered_data = explode('^',$refer_id);
                $refer_quiz = ReferQuiz::find($refered_data[1]);
                $refer_quiz->student_id = $user->id;
                $refer_quiz->save();

               return redirect()->intended('/student/home');

        }

        /** WebQuiz changes for Digital Bank merger **/

        if($user->type == 1) {
            return redirect()->intended('/administration/home');
        } elseif ($user->type == 2) {
            return redirect()->intended('/student/home');
        }
    }

}
