<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Quiz;
use App\School;
use App\AssignRound;
use App\QuizRound;
use App\PublishQuizRound;
use App\PublishQuestions;
use Redirect;
use Session;
use Response;
use Validator;
use DB;
use Storage;
use Image;
use \Crypt;


class AssignQuizController extends Controller
{
    public function index(){
    	/** WebQuiz changes for Digital Bank merger **/
    	$schools = DB::table('school_wq')->join('users', 'school_wq.id', '=', 'users.school_id')->select('school_wq.*')->groupBy('school_wq.id')->get();

       $args = func_get_args();
       $round_id= $args[0];



       $get_roundid = PublishQuizRound::where(['round_id'=>$round_id,'status'=>1])->select('id')->first();


       Session::put('roundid', $get_roundid->id);



      $checkQuizScore=PublishQuestions::where(['quiz_round_id'=>$get_roundid->id,'published'=>1])->exists();
       if($checkQuizScore){
        $check_ques = 1;
        
        $publish_round_id = $get_roundid->id;
       }else{
        $check_ques = 0;
        $publish_round_id='';
       }

       

        return view('admin.assign_quiz',compact('schools','check_ques','publish_round_id'));
    }

    public function getAllAssignedSchools(Request $request)
    {
      $perpage = $request->datatable['pagination']['perpage'];
      $page = $request->datatable['pagination']['page'];
      if($page=='1')
      {
        $offset = 0;
      }
      else
      {
        $offset = ($page-1)*$perpage;
      }
      $round_id= Session::get('roundid');
      $assigned_data = [];
      $allassignedschool = DB::table('assign_round_wq')->join('school_wq', 'school_wq.id', '=', 'assign_round_wq.school_id')->select('assign_round_wq.*','school_wq.name as school_name')->groupBy('school_wq.id')->where('assign_round_wq.round_id',$round_id)->get();
      $total = $allassignedschool->count();
      $allassignedschool = json_decode(json_encode($allassignedschool));
      $meta = ['draw'=>1,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
      if(!empty($allassignedschool))
      {
        //$i = 1;
        foreach ($allassignedschool as $key => $value) {
          $assigned_data[] = (array)$value;
          $assigned_data[$key]['S.no'] = $offset+1;
          $offset++;
        }
        return Response::json(array('data'=> $assigned_data,'meta'=> $meta));
        //return Response::json($assigned_data);
      }
      return Response::json(array('data'=> $allassignedschool,'meta'=> $meta));
      //return Response::json($allassignedschool);
    }

    public function assign_quiz(Request $request){

    	if($request->ajax())
      {

        $rules = array(
          'school'     => 'required',
          'round'     => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        $assign_round;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['school']))
            $errorMsg = $failedRules['school'][0] . "\n";
          if(isset($failedRules['round']))
            $errorMsg = $failedRules['round'][0] . "\n";
          
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{
           $id=$request->id;
              $check=AssignRound::where(['school_id'=>$request->school,'round_id'=>$request->round])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();

              if($check){
                  return (json_encode(array('status'=>'error','message'=>"Already Assigned to this school." )));
                }

        /** WebQuiz changes for Digital Bank merger **/
               $quiz_round_data =PublishQuizRound::where(['id'=>$request->round])->first();
               $current_date = date("Y-m-d H:i:s");
               $Date = date('Y-m-d H:i:s', strtotime($current_date. ' + '.trim($quiz_round_data->active_days).' days'));

               $totalquestions = DB::table('publish_questions_wq')->where(array('quiz_round_id'=>$request->round,'published'=>1))->count();
      
               $quiz = Quiz::where(array('id'=>$quiz_round_data->quiz_id))->first();

                AssignRound::insert(User::select(DB::raw('id as student_id,school_id as school_id,'."'$request->round'".' as round_id,'."'$Date'".' as expiration_date,'."'$totalquestions'".' as total_question,'."'$quiz_round_data->round_name'".' as round_name,'."'$quiz->title'".' as quiz_name'))->where('type','2')->where('school_id',$request->school)->get()->toArray());

                // $publishQuizRoundData = PublishQuizRound::where(['id'=>$request->round])->first();

               
                // $assign_id = $request->round;

            /** WebQuiz changes for Digital Bank merger **/

                // $this->sms_queue(User::select(DB::raw('name as sms_to_name,country_code as country_code,mobile as mobiles'))->where(['type'=>2,'school_id'=>$request->school])->where('mobile' , '<>' ,'')->get()->toArray(),$publishQuizRoundData,$assign_id);
        }
            return (json_encode(array('status'=>'success','message'=>'Quiz Round Assigned to School')));
                   
          
      }
    }


   

    public function sms_queue($data,$publishQuizRoundData,$assign_id)
    {

      

       $quizName = Quiz::where(['id'=>$publishQuizRoundData->quiz_id])->first(); 
     

      foreach ($data as $key => $value) {

        /** WebQuiz changes for Digital Bank merger **/

        $user_id = User::where(['mobile'=>$value['mobile']])->first();
        $quiz_url = Crypt::encrypt($user_id->mobile.'^'.$user_id->id.'^'.$assign_id); 
        

       $message_text = " A New Round has Begun!  Let's Rock This!".$publishQuizRoundData->round_name." for ".$quizName->title." . Login in at ".url('/');

        $details['sms_from'] = '+15416159055';

        /** WebQuiz changes for Digital Bank merger **/

        $details['sms_to']   = $value['mobiles'];
        $details['sms_to_name']   = $value['sms_to_name'];
        $details['body_message'] = $message_text;
        $details['priority']    = '1';
        $details['country_code']   = $value['country_code'];
        DB::table('sms_queue_wq')->insert($details);
      }
    }
    
     public function delete(Request $request)
    {
      if($request->ajax())
      {
        if($request->school_id &&  $request->round_id){
          $assign_quiz = AssignRound::where(['school_id'=>$request->school_id,'round_id'=>$request->round_id]);
          if ($assign_quiz->delete())
            return (json_encode(array('status'=>'success','message'=>"Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }
    }

    public function getAssignedStudentDetail(Request $request)
    {
      if($request->ajax())
      {

        $status = "success";
        if($request->school_id)
          {
            $round_id= Session::get('roundid');
            $allStudentsList = DB::table('users')->join('assign_round_wq', 'assign_round_wq.student_id', '=', 'users.id')->select('users.*')->where(array('users.school_id'=>$request->school_id,'assign_round_wq.round_id'=>$round_id))->get();

            $allStudentsCount = DB::table('users')->join('assign_round_wq', 'assign_round_wq.student_id', '=', 'users.id')->select('users.*')->where(array('users.school_id'=>$request->school_id,'assign_round_wq.round_id'=>$round_id))->count();

            $res = array();
            


            if(!empty($allStudentsList))
            {
              foreach($allStudentsList as $stud)
              {
               if($stud->picture!='' ){
                 $profile_img = url('/').Storage::url('app/'.$stud->picture);
               }else{
                 $profile_img = url('/').Storage::url('app/dummy.jpg');
               }
                $data = array(
                  'id'  => $stud->id,
                  'name'  => ucwords($stud->name),
                  'email'  => $stud->email,

                  /** WebQuiz changes for Digital Bank merger **/
                  'mobile'  => $stud->mobile,
                  'profile_img'  => $profile_img,
                );
                array_push($res,$data);
              }
             return (json_encode(array('status'=>$status,'student'=>$res,'total_student'=>$allStudentsCount))) ;
            }

          }

                 
      }
    }
}
