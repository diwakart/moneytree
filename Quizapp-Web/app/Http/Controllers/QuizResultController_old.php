<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\User;
use App\AssignRound;
use App\School;
use App\QuizRound;
use App\PublishQuizRound;
use App\Quiz;
use App\Question;
use App\QuizRoundResult;
use App\Answer;
use App\StudentRank;
use App\PublishQuestions;
use Redirect;
use Session;
use Response;
use Validator;
use DB;
use Storage;
use Image;
use Excel;



class QuizResultController extends Controller
{
    public function index(){
    
      $declared_quiz_count = AssignRound::where(['attended'=>2,'is_result_declared'=>2])->count();
      $schools = School::all();
      if($declared_quiz_count>0){
          $declared_quiz = AssignRound::where(['attended'=>2,'is_result_declared'=>2])->groupBy('round_id')->get();
        
          $declared_quiz_name = DB::table('publish_quiz_round')->join('assign_round', 'assign_round.round_id', '=', 'publish_quiz_round.id')->join('quiz', 'publish_quiz_round.quiz_id', '=', 'quiz.id')->select('quiz.id as quiz_id','quiz.title','assign_round.*')->where(array('attended'=>2,'is_result_declared'=>2))->groupBy('quiz.id')->get();


      }else{
        $declared_quiz ='';
        $declared_quiz_name='';
      }


     return view('admin.quiz_result',compact('declared_quiz','declared_quiz_name','schools'));
   }



    public function getAllResults(Request $request)
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        if($page=='1')
        {
          $offset = 0;
        }
        else
        {
          $offset = ($page-1)*$perpage;
        }

        $Quiz = DB::table('assign_round')->join('publish_quiz_round', 'assign_round.round_id', '=', 'publish_quiz_round.id')->join('school', 'school.id', '=', 'assign_round.school_id')->select('assign_round.*','school.name')->groupBy('school.id','assign_round.round_id')->offset($offset)->limit($perpage)->get();
        $total = $Quiz->count();
        $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];

        //$Quiz = Quiz::get(array('id','user_id','title','status'));
        $quiz_data = [];
        $Quiz = json_decode(json_encode($Quiz));
        if(!empty($Quiz))
        {
          $i = 1;
          foreach ($Quiz as $key => $value) {
          $quiz_data[] = (array)$value;
          $quiz_data[$key]['S.no'] = $offset+1;
          $offset++;
        }
        return Response::json(array('data'=> $quiz_data,'meta'=> $meta));
        //return Response::json($quiz_data);
        }
        return Response::json(array('data'=> $Quiz,'meta'=> $meta));
        //return Response::json($Quiz);
      }

   

   public function getallstudentWithResult(Request $request)
    {
      if($request->ajax())
      {

        $status = "success";
        if($request->school_id && $request->round_id)
          {

            $allStudentsList =DB::table('assign_round')->join('users', 'assign_round.student_id', '=', 'users.id')->where(['assign_round.school_id'=>$request->school_id,'assign_round.round_id'=>$request->round_id,'assign_round.attended'=>2])->select('assign_round.*','users.name','users.picture')->get();

            $allStudentsListCount =DB::table('assign_round')->join('users', 'assign_round.student_id', '=', 'users.id')->where(['assign_round.school_id'=>$request->school_id,'assign_round.round_id'=>$request->round_id,'assign_round.attended'=>2])->select('assign_round.*','users.name','users.picture')->count();

            $res = array();
            


            if(!empty($allStudentsList))
            {
              foreach($allStudentsList as $stud)
              {
               if($stud->picture!='' ){
                 $profile_img = url('/').Storage::url('app/'.$stud->picture);
               }else{
                 $profile_img = url('/').Storage::url('app/dummy.jpg');
               }
                $data = array(
                  'name'  => ucwords($stud->name),
                  'profile_img'  => $profile_img,
                  'score'  => $stud->score,
                  'total_question'  => $stud->total_question,
                  'quiz_duration'  => $stud->quiz_duration,
                  'quiz_name'  => $stud->quiz_name,
                  'round_name'  => $stud->round_name,
                );
                array_push($res,$data);
              }
             return (json_encode(array('status'=>$status,'student'=>$res,'total_student'=>$allStudentsListCount))) ;
            }

          }

                 
      }
    }

    public function upload_result(){
      $quiz = DB::table('quiz')->get();
      return view('admin.upload_result',compact('quiz'));        
    }

   
    public function upload_result_getSchool_id(Request $request)
    {
      if($request->ajax())
      {

        $status = "success";
        if($request->round_id)
          {
            $allschool = DB::table('school')->get();

            $res = array();
            if(!empty($allschool))
            {
              foreach($allschool as $stud)
              {
              
                $data = array(
                  'id'  => $stud->id,
                  'name'  => ucwords($stud->name),
                );
                array_push($res,$data);
              }
             return (json_encode(array('status'=>$status,'school'=>$res)));
            }

          }

                 
      }
    }

    public function upload_result_getquiz_id(Request $request)
    {
      if($request->ajax())
      {

        $status = "success";
        if($request->quiz_id)
          {
            $allQuizRound = DB::table('publish_quiz_round')->where(['publish_quiz_round.quiz_id'=>$request->quiz_id,'status'=>1])->get();

            $res = array();
            if(!empty($allQuizRound))
            {
              foreach($allQuizRound as $stud)
              {
              
                $data = array(
                  'id'  => $stud->id,
                  'round_name'  => ucwords($stud->round_name),
                );
                array_push($res,$data);
              }
             return (json_encode(array('status'=>$status,'quiz_round'=>$res)));
            }

          }

                 
      }
    }


    public function export_result_csv(Request $request){

       $question_number = PublishQuestions::where(['quiz_round_id'=>$request->round_id,'published'=>1])->count(); 

       $questions = PublishQuestions::where(['quiz_round_id'=>$request->round_id,'published'=>1])->get(); 

       $ques = array();
       $ques_text = array();

        foreach(range(1, $question_number) as $i) {
            $ques[] = 'Question '.$i;
        }

        foreach($questions as $que) {
            $ques_text[] =$que->question;
        }

  
      $array1 = array('Student Name', 'Contact Number', 'School Name', 'Quiz Name','Round Name');
      $array_duration = array('Duration (format: 01H 00M 00S 00MS)');
      $merge = array_merge($array1, $ques,$array_duration);
    
      $array2 = array('', '', '','Correct Answer: Enter Y','Incorrect Answer : Enter N');
      $merge2 = array_merge($array2, $ques_text);

      $filename = "result.csv";
      $handle = fopen($filename, 'w+');

      fputcsv($handle, $merge);
      fputcsv($handle, $merge2);



       $students = DB::table('users')->join('school1', 'users.school_id', '=', 'school.id')->join('assign_round', 'users.id', '=', 'assign_round.student_id')->where(['assign_round.school_id'=>$request->school_id,'assign_round.round_id'=>$request->round_id,'assign_round.attended'=>0])->select('users.*','school.name as school_name')->get();



        $quiz_name = DB::table('quiz')->where(['id'=>$request->quiz_id])->first();
        $round_name = DB::table('publish_quiz_round')->where(['id'=>$request->round_id])->first();
        $j=1;
        $array_new = array();
        foreach($students as $stu) {

          /** WebQuiz changes for Digital Bank merger **/

            $array_new = array($stu->name, $stu->mobile, $stu->school_name,$quiz_name->title,$round_name->round_name);
            fputcsv($handle, $array_new);
           
        }

        



      fclose($handle);

      $headers = array(
          'Content-Type' => 'text/csv',
      );

      return Response::download($filename, 'result.csv', $headers);
   
      
    }


    public function importCSVResult(Request $request)
    {
     if(Input::hasFile('csv_file_result')){

      $path = Input::file('csv_file_result')->getRealPath();
      $data = Excel::load($path, function($reader) {
      })->get()->toArray();
      $quiz_id = $request->UploadResultQuizId;
      $round_id = $request->UploadResultRoundId;
      $school_id = $request->UploadResultSchoolId;

      $remaining_students = array();
      $wrong_duration_students_name = array();
      $check_rank = AssignRound::where(['round_id'=>$round_id,'school_id'=>$school_id])->where(function($query){
                      $query->where('is_result_declared' , '=' ,'2');
                   
                })->count();
      if($check_rank>0){
        Session::flash('errormessage', 'Result declared for this School and Quiz Round'); 
        return back();
      }else{
          $csv_file='';
          $csv_file=time().'.'.$request->file('csv_file_result')->getClientOriginalExtension();
          $NewFilePath=$request->file('csv_file_result')->move(public_path('csv'), $csv_file);
          $insert_file = DB::table('import_csv')->insert([
                                  ['file_name' => $csv_file,'start_row'=>102,'end_row'=>202,'status'=>2,'file_type'=>'student_result']
                      ]); 
                      
        $output = array_slice($data, 2 , 100); 
        if(!empty($output) && count($output)){
        //All data row
          $SchoolName = School::where(['id'=>$school_id])->first();
          $quiz = Quiz::where('id',$quiz_id)->get()->toArray();
          $RoundName = PublishQuizRound::where(['quiz_id'=>$quiz[0]['id'],'id'=>$round_id,'status'=>'1'])->first();
          if($SchoolName->name == $data[1]['school_name'] && $RoundName->round_name==$data[1]['round_name']){
            foreach ($output as $key => $value) {
          //Excluding  Heading

              /** WebQuiz changes for Digital Bank merger **/

            $student_id = User::where(['mobile'=>$value['mobile_number']])->first();
            
            $check_exist=AssignRound::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id,'attended'=>2])->exists();
            // Check Assign round have data or not
            if($check_exist){
            }else{
            //   $check=AssignRound::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id]);
            //   if($check->delete()){

               $assign_round = new AssignRound();
               $assign_round->student_id = $student_id->id;
               $assign_round->round_id = $round_id;
               $assign_round->school_id = $school_id;
               $assign_round->attended = 2;
               $assign_round->expiration_date = date('Y-m-d H:i:s');

               if($assign_round->save()){

                 $questions_id =PublishQuestions::where(['quiz_round_id'=>$round_id,'published'=>1])->get();
                 $count_que = 1;
                 foreach ($questions_id as $key => $que_id) {

                    $check_question_id=QuizRoundResult::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id,'question_id'=>$que_id->id])->exists();

                    if($check_question_id){
                      $check_question_id=QuizRoundResult::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id,'question_id'=>$que_id->id]);
                      if($check_question_id->delete()){
                        
                      $quiz_round_result = new QuizRoundResult();
                      $quiz_round_result->student_id = $student_id->id;
                      $quiz_round_result->school_id = $school_id;
                      $quiz_round_result->round_id = $round_id;
                      $quiz_round_result->question_id = $que_id->id;
                      if($value['question_'.$count_que] == "Y"){
                         $quiz_round_result->check_answer = 1;
                      }else{
                         $quiz_round_result->check_answer = 0;
                      }
                      $quiz_round_result->question_type = $que_id->question_type;
                     

                      if($quiz_round_result->save()){
                      
                      }else{
                        Session::flash('errormessage', 'Some error occured'); 
                        return back();
                      }
                    }
                    }else{

                      $quiz_round_result = new QuizRoundResult();
                      $quiz_round_result->student_id = $student_id->id;
                      $quiz_round_result->school_id = $school_id;
                      $quiz_round_result->round_id = $round_id;
                      $quiz_round_result->question_id = $que_id->id;
                      if($value['question_'.$count_que] == "Y"){
                         $quiz_round_result->check_answer = 1;
                      }else{
                         $quiz_round_result->check_answer = 0;
                      }
                      $quiz_round_result->question_type = $que_id->question_type;
                     
                      if($quiz_round_result->save()){
                      
                      }else{
                        Session::flash('errormessage', 'Some error occured'); 
                        return back();
                      }
                    }

                    
                    $count_que++;
                 }



               }
            }
                $score = QuizRoundResult::where(array('student_id'=>$student_id->id,'round_id'=>$round_id,'check_answer'=>1,'school_id'=>$school_id))->count();

                $total_que = PublishQuestions::where(array('quiz_round_id'=>$round_id,'published'=>1))->count();

                $quiz_info = DB::table('quiz')->join('publish_quiz_round', 'publish_quiz_round.quiz_id', '=', 'quiz.id')->select('quiz.title','publish_quiz_round.round_name')->where(array('publish_quiz_round.id'=>$round_id))->first(); 
                
                if($value['duration_format_01h_00m_00s_00ms']==''){
                  $delete_ques = QuizRoundResult::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id]);
                  if($delete_ques->delete()){



                      $calculate_score = AssignRound::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id])->update([
                      'attended'=>0,
                      'total_question'=>$total_que,
                        'quiz_name'=> $quiz_info->title,
                        'round_name'=>$quiz_info->round_name,
                    ]);

                  }
                  $remaining_students[] = ucwords($value['student_name']);
                 
                }else{

                  if (preg_match('/^[0-9]{2}+[H]{1}+[ ]+[0-9]{2}+[M]{1}+[ ]+[0-9]{2}+[S]{1}+[ ]+[0-9]{2}+[MS]{2}+$/', $value['duration_format_01h_00m_00s_00ms'])) {
                      

                        $calculate_duration = $value['duration_format_01h_00m_00s_00ms'];
                        $pieces = explode(" ", $calculate_duration);
                        $Hour_ = preg_replace("/[^0-9,.]/", "", $pieces[0]); // h
                        $Min_ = preg_replace("/[^0-9,.]/", "", $pieces[1]); // m
                        $Sec_ = preg_replace("/[^0-9,.]/", "", $pieces[2]); // m
                        $MSec_ = preg_replace("/[^0-9,.]/", "", $pieces[3]); // m



                        $calculate_score = AssignRound::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id])->update([
                            'quiz_duration'=>$Hour_.':'.$Min_.':'.$Sec_.'.'.$MSec_,
                            'score'=>$score,
                            'total_question'=>$total_que,
                            'quiz_name'=> $quiz_info->title,
                            'round_name'=>$quiz_info->round_name,
                          ]);
                    } else {
                       $wrong_duration_students_name[] = ucwords($value['student_name']);
                       
                      
                    }

                  
                }

         
        }

         if(count($remaining_students)>0 || count($wrong_duration_students_name)>0){
        
             $error_names =  array_unique(array_merge($remaining_students,$wrong_duration_students_name), SORT_REGULAR);
              $duration_students_name = implode(', ', $error_names);
             Session::flash('errormessage', 'Record Inserted Successfully. '.$duration_students_name.' students have empty quiz duration or may have wrong format duration.Remove All rows except row with wrong data and upload again after corrections');
             return back(); 
         }else{
            Session::flash('successmessage', 'Record Inserted Successfully.');
            return back(); 
         }
          }else{
              Session::flash('errormessage', 'Upload result for School name and Round name mentioned in csv file');
               return back(); 
          }

      }else{
          Session::flash('errormessage', 'No data available');
          return back(); 
      }
      }
      
      
      

    }else{
      Session::flash('errormessage', 'File not selected'); 
      return back();
    }
    
  }

public function declare_result(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = AssignRound::where(['round_id'=>$request->round_id,'school_id'=>$request->school_id])->first();
          $today_date = strtotime(date('Y-m-d H:i'));
         // $date = str_replace('/', '-', $result->expiration_date);
          $expiration_date = strtotime(date('Y-m-d H:i', strtotime($result->expiration_date)));
          if($today_date==$expiration_date || $today_date>$expiration_date){
            $declare_check = 2;//today is expiration date
          }else if($today_date<$expiration_date){
            $declare_check = 1; // coming soon
          }

         /* $tommorow_date = strtotime(date('Y-m-d H:i',strtotime($expiration_date . "+1 days")));
          if($tommorow_date<=$today_date){
            $result_declare_day = "now";
          }else{
            $result_declare_day = "not_now";
          }*/
    
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result,'declare_check'=>$declare_check,'expiration_date'=>date('Y-m-d H:i',$expiration_date)))) ;         
      }
    }


    public function declare_result_for_school_and_round(Request $request)
    {
      if($request->ajax())
      {
          $status = "success";
        if($request->school_id && $request->round_id)
          {


        /*DB::statement(DB::raw('set @rownumber=0'));
          $update_rank = DB::update('UPDATE assign_round SET rank = CASE WHEN score = 0 THEN 0 ELSE (@rownumber:=@rownumber+1) END,attended=4 WHERE school_id='.$request->school_id.' AND round_id='.$request->round_id.' and attended=3  ORDER BY (score + earn_points) DESC, quiz_duration ASC');*/

          $countAttended = AssignRound::where(['school_id'=>$request->school_id,'round_id'=>$request->round_id,'attended'=>2])->count();
         
            $update_result_declared = DB::update('UPDATE assign_round  SET is_result_declared=2 WHERE school_id='.$request->school_id.'   AND round_id='.$request->round_id);


          if($countAttended>0){

             

            $update_rank =  DB::update('UPDATE assign_round ar_s SET ar_s.is_result_declared=2,ar_s.rank = (SELECT ranks FROM ( SELECT id, @curRank := IF(@prevRank = CONCAT(score + earn_points , assign_round.quiz_duration), @curRank, @incRank) AS ranks, IF(@prevRank = CONCAT(score + earn_points , assign_round.quiz_duration), @curRank,@incRank := @incRank + 1) AS prevr, 
@prevRank := CONCAT(score + earn_points , assign_round.quiz_duration)  AS newr FROM assign_round, (
SELECT @curRank :=0, @prevRank := NULL, @incRank := 1
) r
WHERE school_id='.$request->school_id.' AND round_id='.$request->round_id.' AND attended=2 ORDER BY (score + earn_points) DESC, CAST(assign_round.quiz_duration AS TIME) ASC ) res WHERE res.id=ar_s.id) WHERE school_id='.$request->school_id.' AND attended=2  AND round_id='.$request->round_id);
          

            if($update_rank)
            {
               $publishQuizRoundData = PublishQuizRound::where(['id'=>$request->round_id])->first();

               /** WebQuiz changes for Digital Bank merger **/

               $this->sms_queue(User::select(DB::raw('name as sms_to_name,country_code as country_code,mobile as mobiles'))->where(['type'=>2,'school_id'=>$request->school_id])->where('mobile' , '<>' ,'')->get()->toArray(),$publishQuizRoundData);


             return (json_encode(array('status'=>$status,'message'=>"Result Declare Successfully"))) ;
            }

          }else{
            $update_rank = DB::update('UPDATE assign_round SET rank=0,is_result_declared=2 WHERE school_id='.$request->school_id.' AND round_id='.$request->round_id);
          

            if($update_rank)
            {
               $publishQuizRoundData = PublishQuizRound::where(['id'=>$request->round_id])->first();

               /** WebQuiz changes for Digital Bank merger **/

               $this->sms_queue(User::select(DB::raw('name as sms_to_name,country_code as country_code,mobile as mobiles'))->where(['type'=>2,'school_id'=>$request->school_id])->where('mobile' , '<>' ,'')->get()->toArray(),$publishQuizRoundData);


             return (json_encode(array('status'=>$status,'message'=>"Result Declare Successfully"))) ;
            }
          }


          

          
          }
      }
    }

    public function getStudentsResult($id)
  {
    $school = DB::table('school')->where('id',$id)->first();
    return  view('admin.quiz_school_result',compact('school'));
  }

  public function getSchoolsResult(Request $request,$school_id,$round_id)
  {
    $perpage = $request->datatable['pagination']['perpage'];
    $page = $request->datatable['pagination']['page'];
    $value = (isset($request->datatable['query']['value']) && ($request->datatable['query']['value']!="") ? $request->datatable['query']['value'] : '');
    if($page=='1')
    {
      $offset = 0;
    }
    else
    {
      $offset = ($page-1)*$perpage;
    }
    $school_data = [];
    $allStudentsList = new AssignRound;
    $allStudentsList =$allStudentsList->join('users', 'assign_round.student_id', '=', 'users.id')->where(['assign_round.school_id'=>$school_id,'assign_round.round_id'=>$round_id])->select('assign_round.*','users.name');
    $total =DB::table('assign_round')->join('users', 'assign_round.student_id', '=', 'users.id')->where(['assign_round.school_id'=>$school_id,'assign_round.round_id'=>$round_id])->orderby('rank','asc')->count();
    if($value!=='')
    {
        if (is_numeric($value)) {

          /** WebQuiz changes for Digital Bank merger **/

            $allStudentsList =$allStudentsList->where('users.mobile','like',$value.'%');
        }
        else
        {
           $allStudentsList =$allStudentsList->where('users.name','like',$value.'%'); 
        }
         $total =$allStudentsList->count();
        // $allStudentsList =$allStudentsList->orwhere('users.mobile','like',$value.'%');
    }
    $allStudentsList =$allStudentsList->orderby('rank','asc')->offset($offset)->limit($perpage)->get();
    
    $allStudentsList = json_decode(json_encode($allStudentsList));
    $meta = ['draw'=>1,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
    if(!empty($allStudentsList))
    {
      $i = 1;
      foreach ($allStudentsList as $key => $value) {
        $school_data[] = (array)$value;
        $school_data[$key]['S.no'] = $offset+1;
        $offset++;
      }
      return Response::json(array('data'=> $school_data,'meta'=> $meta));
      //return Response::json($school_data);
    }
    return Response::json(array('data'=> $allStudentsList,'meta'=> $meta));
    //return Response::json($allStudentsList);
  }


    public function selected_quiz_rank(Request $request)
    {

      if($request->ajax())
      {

        $status = "success";
        if($request->school_id)
          {
           
          

            $countallstudent =  DB::select("SELECT rank from assign_round WHERE round_id=".$request->round_id." AND  school_id=".$request->school_id." AND attended=2 AND is_result_declared=2 AND rank!=0 ORDER BY rank ASC");

            if(count($countallstudent)>10){
             /* $getlastrank =  DB::select("SELECT rank from assign_round WHERE round_id=".$request->round_id." AND  school_id=".$request->school_id." AND attended=2 AND is_result_declared=2 AND rank!=0 ORDER BY (score + earn_points) DESC, quiz_duration ASC LIMIT 10,1");*/

               $getlastrank =  DB::select("SELECT rank from assign_round WHERE round_id=".$request->round_id." AND  school_id=".$request->school_id." AND attended=2 AND is_result_declared=2 AND rank!=0 ORDER BY rank ASC LIMIT 9,1");


           
           $last_rank = $getlastrank[0]->rank;




           $allStudentsList =  DB::select("SELECT users.name,users.id as userid,assign_round.rank,(assign_round.score + assign_round.earn_points) as final_score, assign_round.score , assign_round.earn_points,assign_round.quiz_duration FROM assign_round INNER JOIN users ON assign_round.student_id=users.id WHERE assign_round.round_id=".$request->round_id." AND  assign_round.school_id=".$request->school_id." AND assign_round.attended=2 AND is_result_declared=2 AND (assign_round.rank<=$last_rank) AND assign_round.rank!=0  ORDER BY assign_round.rank ASC, users.name ASC");
            }else{


           $allStudentsList =  DB::select("SELECT users.name,users.id as userid,assign_round.rank,(assign_round.score + assign_round.earn_points) as final_score, assign_round.score , assign_round.earn_points,assign_round.quiz_duration FROM assign_round INNER JOIN users ON assign_round.student_id=users.id WHERE assign_round.round_id=".$request->round_id." AND  assign_round.school_id=".$request->school_id." AND assign_round.attended=2  AND is_result_declared=2 AND assign_round.rank!=0  ORDER BY assign_round.rank ASC, users.name ASC");
            }

           

         



           /* $myRank = DB::table('users')->join('assign_round', 'assign_round.student_id', '=', 'users.id')->select('users.name','users.id as userid','assign_round.rank','(assign_round.score + assign_round.earn_points) as final_score', 'assign_round.score' , 'assign_round.earn_points','assign_round.quiz_duration')->where(array('assign_round.school_id'=>$request->school_id,'assign_round.round_id'=>$request->round_id,'attended'=>4,'assign_round.student_id'=>Auth::user()->id))->first();*/

            $res = array();
            if(!empty($allStudentsList))
            {
              foreach($allStudentsList as $stud)
              {
               
                $data = array(
                  'name'  => ucwords($stud->name),
                  'duration'  => $stud->quiz_duration,
                  'points'  => $stud->score,
                  'final_score'  => $stud->final_score,
                  'rank'  => $stud->rank,
                  'userid' => $stud->userid,
                );
                array_push($res,$data);
              }

             return (json_encode(array('status'=>$status,'student'=>$res))) ;
            }

          }

                 
      }
    }


    public function selected_quiz_overall_rank(Request $request)
    {
        

      if($request->ajax())
      {

        $status = "success";
        if($request->school_id && $request->quiz_id)
          {

            $allRounds = DB::table('quiz')->join('publish_quiz_round', 'publish_quiz_round.quiz_id', '=', 'quiz.id')->join('assign_round', 'assign_round.round_id', '=', 'publish_quiz_round.id')->select('assign_round.round_id')->where(array('assign_round.school_id'=>$request->school_id,'publish_quiz_round.quiz_id'=>$request->quiz_id,'attended'=>2))->where(function($query){
                      $query->where('assign_round.rank' , '<>' ,0);
                  
                })->groupBy('assign_round.round_id')->get();

          

            $round_ids = array();
           

            foreach ($allRounds as $value) {
              array_push($round_ids,$value->round_id);
            }

            $ids = join("','",$round_ids); 

            $school = $request->school_id;
           
            
              

               //$getlastrank = DB::select("SELECT assign_round.*, (@rank := if(@avgrank = total , @rank, if(@avgrank := total, @rank + 1, @rank + 1 ) ) ) AS ranking FROM (SELECT assign_round.student_id AS studentid,assign_round.id,AVG(assign_round.rank) AS total,SUM(TIME_TO_SEC(assign_round.quiz_duration) ) AS final_duration, SUM((assign_round.score + assign_round.earn_points )) AS `score`,users.name AS username FROM assign_round JOIN `users` ON users.id=assign_round.student_id WHERE round_id IN ('$ids') AND assign_round.school_id=$school AND is_result_declared='2' AND attended='2' AND rank!=0 GROUP BY `student_id` ORDER BY total DESC, final_duration ASC) assign_round  cross join (SELECT @rank := 0, @avgrank := -1) params ORDER BY score DESC, final_duration ASC LIMIT 9,1");
               
               $getlastrank = DB::select("SELECT * FROM (SELECT assign_round.*, (@rank := if(@avgrank = total , @rank, if(@avgrank := total, @rank + 1, @rank + 1 ) ) ) AS ranking FROM (SELECT assign_round.student_id AS studentid,assign_round.id,AVG(assign_round.rank) AS total,(SUM(TIME_TO_SEC(assign_round.quiz_duration)) + SUM((MICROSECOND(assign_round.quiz_duration)/1000000)))  AS final_duration,LEFT(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(assign_round.quiz_duration)) + SUM((MICROSECOND(assign_round.quiz_duration)/1000000))),'%H:%i:%s.%f'),11) AS format_duration, SUM((assign_round.score + assign_round.earn_points )) AS `score`,users.name AS username FROM assign_round JOIN `users` ON users.id=assign_round.student_id WHERE round_id IN ('$ids') AND assign_round.school_id=$school AND is_result_declared='2' AND attended='2' AND rank!=0 GROUP BY `student_id` ORDER BY total DESC, final_duration ASC) assign_round  cross join (SELECT @rank := 0, @avgrank := -1) params ORDER BY score DESC, final_duration ASC ) AS Final_res  LIMIT 9,1");
               
              
           $last_rank_where = "";
            if(!empty($getlastrank)){
              $last_rank = $getlastrank[0]->ranking;
              $last_rank_where = " WHERE (ranking<=$last_rank)";
            }
              
              //$allQuizRank = DB::select("SELECT assign_round.*, (@rank := if(@avgrank = total , @rank, if(@avgrank := total, @rank + 1, @rank + 1 ) ) ) AS ranking FROM (SELECT assign_round.student_id AS studentid,assign_round.id,AVG(assign_round.rank) AS total,SUM(TIME_TO_SEC(assign_round.quiz_duration) ) AS final_duration, SUM((assign_round.score + assign_round.earn_points )) AS `score`,users.name AS username FROM assign_round JOIN `users` ON users.id=assign_round.student_id WHERE round_id IN ('$ids') AND assign_round.school_id=$school AND is_result_declared='2' AND attended='2' AND rank!=0 GROUP BY `student_id` ORDER BY total DESC, final_duration ASC) assign_round  cross join (SELECT @rank := 0, @avgrank := -1) params ".$last_rank_where." ORDER BY score DESC, final_duration ASC ");
            
              $allQuizRank = DB::select("SELECT * FROM ( SELECT assign_round.*, (@rank := if(@avgrank = total , @rank, if(@avgrank := total, @rank + 1, @rank + 1 ) ) ) AS ranking FROM (SELECT assign_round.student_id AS studentid,assign_round.id,AVG(assign_round.rank) AS total,(SUM(TIME_TO_SEC(assign_round.quiz_duration)) + SUM((MICROSECOND(assign_round.quiz_duration)/1000000)))  AS final_duration,LEFT(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(assign_round.quiz_duration)) + SUM((MICROSECOND(assign_round.quiz_duration)/1000000))),'%H:%i:%s.%f'),11) AS format_duration, SUM((assign_round.score + assign_round.earn_points )) AS `score`,users.name AS username FROM assign_round JOIN `users` ON users.id=assign_round.student_id WHERE round_id IN ('$ids') AND assign_round.school_id=$school AND is_result_declared='2' AND attended='2' AND rank!=0 GROUP BY `student_id` ORDER BY total DESC, final_duration ASC) assign_round  cross join (SELECT @rank := 0, @avgrank := -1) params ORDER BY score DESC, final_duration ASC ) AS Final_res $last_rank_where");
           
            $res = array();
            if(!empty($allQuizRank))
            {
              foreach($allQuizRank as $stud)
              {
                $data = array(
                  'name'  => ucwords($stud->username),
                  'score'  => $stud->score,
                  'quiz_duration'  => $stud->format_duration,
                  'student_id'  => $stud->studentid,
                  'rank'  => $stud->ranking,
                );
                array_push($res,$data);
              }

            
             return (json_encode(array('status'=>$status,'student'=>$res))) ;
            }


          

          }

                 
      }
    }

       public function sms_queue($data,$publishQuizRoundData)
    {

      

       $quizName = Quiz::where(['id'=>$publishQuizRoundData->quiz_id])->first(); 
     

      foreach ($data as $key => $value) {

        /** WebQuiz changes for Digital Bank merger **/

        $user_id = User::where(['mobile'=>$value['mobiles']])->first();
       


       $message_text =  $publishQuizRoundData->round_name." ".$quizName->title.". Who have you beaten so far? Find our Here :".url('/');

        $details['sms_from'] = '+15416159055';

        /** WebQuiz changes for Digital Bank merger **/
        
        $details['sms_to']   = $value['mobiles'];
        $details['sms_to_name']   = $value['sms_to_name'];
        $details['body_message'] = $message_text;
        $details['priority']    = '4';
        $details['country_code']   = $value['country_code'];
        DB::table('sms_queue')->insert($details);
      }
    }

}
