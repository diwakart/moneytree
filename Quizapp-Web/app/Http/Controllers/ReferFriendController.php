<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReferQuiz;
use App\User;
use App\AssignRound;
use App\School;
use App\QuizRound;
use App\PublishQuizRound;
use App\Quiz;
use App\Question;
use App\QuizRoundResult;
use App\PublishQuestions;
use App\PublishAnswers;
use App\Answer;
use App\StudentRank;
use App\ReferQuizRoundResult;
use Auth;
use \Crypt;
use Redirect;
use Session;
use Response;
use Validator;
use DB;
use Twilio\Rest\Client;

class ReferFriendController extends Controller
{
    public function refer_quiz(Request $request){
    	if($request->ajax())
      {

        $rules = array(
          'name'     => 'required',

          /** WebQuiz changes for Digital Bank merger **/

          'mobile'     => 'required',
          'refered_country_code'     => 'required',
        );
        $sid = 'ACbfcf0a5d3a4790cc95d0ad12b2e7dd19';
        $token = '5930ab90756eade8563f0acc74ba5f69';
        $client = new Client($sid, $token);

        $validator = Validator::make($request->all(), $rules);

        $assign_round;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['name']))
            $errorMsg = $failedRules['name'][0] . "\n";

          /** WebQuiz changes for Digital Bank merger **/

          if(isset($failedRules['mobile']))
            $errorMsg = $failedRules['mobile'][0] . "\n";
        if(isset($failedRules['refered_country_code']))
            $errorMsg = $failedRules['refered_country_code'][0] . "\n";
          
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{

          /** WebQuiz changes for Digital Bank merger **/

        	$check_mobile = User::where(['mobile'=>$request->mobile])->exists(); //check if user exist or not

          /** WebQuiz changes for Digital Bank merger **/

              if($check_mobile){ //if user exist

              		$get_user = User::where(['mobile'=>$request->mobile])->first(); //get exist user detail

              		$check_assigned_quiz = AssignRound::where(['school_id'=>$get_user->school_id,'round_id'=>$request->round_id])->exists(); //check if exist user assigned by quiz or not
              		 if($check_assigned_quiz){
              		 	return (json_encode(array('status'=>'error','message'=>"Referer is already part of this quiz round" )));
              		 }else{

                  
                  /* $refer_score = AssignRound::where(['student_id'=>Auth::user()->id,'school_id'=>Auth::user()->school_id,'round_id'=>$request->round_id])->update(['earn_points'=>1]);*/
                  

              		 	//send sms for quiz link to student
              		 	//assign quiz to student
                 /*  echo 'student_id  '.Auth::user()->id.'school_id '.Auth::user()->school_id.'round_id '.$request->round_id;die;*/
              		 	$get_expiration_date = AssignRound::where(['student_id'=>Auth::user()->id,'school_id'=>Auth::user()->school_id,'round_id'=>$request->round_id])->first();
                   /* print_r($get_expiration_date);die;*/

              		 	$assign_quiz = New AssignRound();
              		 	$assign_quiz->round_id = $request->round_id;
              		 	$assign_quiz->school_id = $get_user->school_id; 
              		 	$assign_quiz->student_id = $get_user->id; 
              		 	$assign_quiz->expiration_date = $get_expiration_date->expiration_date; 
              		 	

              		 	if($assign_quiz->save()){

                      AssignRound::where(['id'=>$request->assign_id])->update(['earn_points' => 1]);

                      /** WebQuiz changes for Digital Bank merger **/

              		 		$parameter_enc = Crypt::encrypt($get_user->mobile.'^'.$get_user->id.'^'.$assign_quiz->id);   
              		 		$quiz_url = url('/').'/refer-quiz/'.$parameter_enc;
                      $client->messages->create(
                        // the number you'd like to send the message to

                        /** WebQuiz changes for Digital Bank merger **/

                        $request->refered_country_code.$request->mobile,
                      array(
                          // A Twilio phone number you purchased at twilio.com/console
                          'from' => '+15416159055',
                          // the body of the text message you'd like to send
                          'body' => 'Hey '.$request->name.'! '.Auth::user()->name.' referred a quiz to you.Click '.url('/').' and login to see details. Good luck!',
                      )
                      );


              		 		return (json_encode(array('status'=>'success','message'=>'Quiz Refered to this student','quiz_url'=>$quiz_url)));
              		 	}else{
              		 		return (json_encode(array('status'=>'error','message'=>'Failed to refer')));
              		 	}
              		 }
                  
                }else{ //if user not exist

                  /** WebQuiz changes for Digital Bank merger **/

                  $check_referer = ReferQuiz::where(['round_id'=>$request->round_id,'mobile'=>$request->mobile])->exists();
                  if($check_referer){

                    return (json_encode(array('status'=>'error','message'=>'Quiz have already referred to this mobile number')));
                    /*$refer_score = AssignRound::where(['student_id'=>$request->refered_by,'round_id'=>$request->round_id])->update(['earn_points'=>1]);*/
                  }else{
                  $refer_quiz = new ReferQuiz();
                  $refer_quiz->round_id = $request->round_id;
                  $refer_quiz->refered_by = $request->refered_by;

                  /** WebQuiz changes for Digital Bank merger **/

                  $refer_quiz->mobile = $request->mobile;
                   if($refer_quiz->save()){
                    AssignRound::where(['id'=>$request->assign_id])->update(['earn_points' => 1]);

                    /** WebQuiz changes for Digital Bank merger **/
                   	 $parameter_enc = Crypt::encrypt($request->mobile.'^'.$refer_quiz->id.'^'.$request->assign_id);
                   	
                   	 $quiz_url = url('/').'/refer-quiz/'.$parameter_enc;
                     $test  =  $client->messages->create(
                        // the number you'd like to send the message to

                      /** WebQuiz changes for Digital Bank merger **/

                        $request->refered_country_code.$request->mobile,
                      array(
                          // A Twilio phone number you purchased at twilio.com/console
                          'from' => '+15416159055',
                          // the body of the text message you'd like to send
                          'body' =>  'Hey '.$request->name.'! '.Auth::user()->name.' referred a quiz to you.Click '.$quiz_url.' and register yourself to see details. Good luck!',
                          /*'mediaUrl' => 'http://big5kayakchallenge.com/wp-content/uploads/2018/01/inspirational-wallpaper-for-mobile-samsung-hii-wallpapers-to-your-cell-phone-hello-hi-wallpaper-for-mobile-samsung.jpg'*/
                      )
                      );

                    

                   	 return (json_encode(array('status'=>'success','message'=>'Refered Successfully','quiz_url'=>$quiz_url)));
                   }else{
                   	return (json_encode(array('status'=>'error','message'=>'Failed to refer')));
                   }

                  }

                }
   
        }
          
          
      }
    }


    public function index(){

        $args = func_get_args();

      $exploded_para= Crypt::decrypt($args[0]);
      $exploded_parameter = explode('^',$exploded_para);
      if(count($exploded_parameter)>0){
         $assign_quiz_id = $exploded_parameter[1];

      $assign_round = ReferQuiz::where(array('id'=>$assign_quiz_id,'attended'=>1))->first();
      $assign_count = ReferQuiz::where(array('id'=>$assign_quiz_id,'attended'=>1))->count();

      if($assign_count>0){

       /*---Question List------*/
       $questions = DB::table('publish_questions_wq')->where(array('quiz_round_id'=>$assign_round->round_id,'published'=>1))->get();
      $totalquestions = DB::table('publish_questions_wq')->where(array('quiz_round_id'=>$assign_round->round_id,'published'=>1))->count();
      /*-----Quiz Round Detail-----*/
      $quiz_round = PublishQuizRound::where(array('id'=>$assign_round->round_id))->first();
      /*----School Detail------*/
      
      $refered_By = User::where(array('id'=>$assign_round->refered_by))->first();
      /*----Quiz Detail------*/
      $quiz = Quiz::where(array('id'=>$quiz_round->quiz_id))->first();

      

     
    
      return view('student.refered_quiz',compact('questions','assign_round','quiz_round','refered_By','quiz','totalquestions'));
     }else{
       return abort(404);
     }
      }else{
       return abort(404); 
      }
   	 
   }




   public function refer_submit_quiz(Request $request){


   	if($request->ajax())
      {
      	 $quiz_result;
        
      
      	 if($request->question_type==1){
      	    $answers = $request->answers;
            $answers = array_map(function($v){
                return trim($v);},$answers);
      	    $answ = array();
      	    $real_answers = PublishAnswers::where('question_id',$request->question_id)->get();
      	     foreach ($real_answers as $ans) {
              array_push($answ,trim($ans->correct_answer)); 
            }
      	    if(array_map('strtolower', $answ) == array_map('strtolower', $answers)){
      	    	$check_ans = 1;
      	    }else{
      	    	$check_ans = 0;
      	    }
            $given_answer = implode(":::", $request->answers);
      	   	$result = New ReferQuizRoundResult;
      	    $result->question_id = $request->question_id;
      	    $result->question_type = $request->question_type;
      	    $result->round_id = $request->quiz_round_id;
      	    $result->student_id = $request->auth_id;
      	    $result->check_answer = $check_ans;
      	    $result->refered_by = $request->refered_by_id;
            $result->answered = $given_answer;

      	     $check=ReferQuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'refered_by'=>$request->refered_by_id])->exists();
      	    if(!$check){
      	    	$result->save();
      	    }else{
      	    	ReferQuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'refered_by'=>$request->refered_by_id])->delete();
      	    	$result->save();
      	    }

      	    

      	 }

      	 if($request->question_type==2){

      	 	$answers = $request->answers;
      	    $answ = array();
      	   
            $val=$request->question_id;

      	   $real_answers = PublishAnswers::where(array('correct_answer'=>1,'question_id'=>$val))->get();

        

      	    foreach ($real_answers as $ans) {

      	    	array_push($answ,$ans->option_text); 
      	    }
      	   
      	    if(count($answers)>0){
            if(array_map('strtolower', $answ) == array_map('strtolower', $answers)){
              $check_ans = 1;
            }else{
              $check_ans = 0;
            }
            }else{
              $check_ans = 0;
            }
            $given_answer = '';
      	   	$result = New ReferQuizRoundResult;
      	    $result->question_id = $request->question_id;
      	    $result->question_type = $request->question_type;
      	    $result->round_id = $request->quiz_round_id;
      	    $result->student_id = $request->auth_id;
      	    $result->check_answer = $check_ans;
      	    $result->refered_by = $request->refered_by_id;
            $result->answered = $given_answer;

      	    $check=ReferQuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'refered_by'=>$request->refered_by_id])->exists();
      	    if(!$check){
      	    	$result->save();
      	    }else{
      	    	ReferQuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'refered_by'=>$request->refered_by_id])->delete();
      	    	$result->save();
      	    }

      	    
      	 }


      	 if($request->question_type==3){

      	 	$answers = $request->answers;
      	    $answ = array();
      	   
            $val=$request->question_id;

      	   $real_answers = PublishAnswers::where(array('question_id'=>$val))->get();

      	    foreach ($real_answers as $ans) {

      	    	array_push($answ,$ans->correct_answer); 
      	    }
      	   
      	    if(array_map('strtolower', $answ) == array_map('strtolower', $answers)){
      	    	$check_ans = 1;
      	    }else{
      	    	$check_ans = 0;
      	    }
             $given_answer = implode(":::", $request->answers);
      	   	$result = New ReferQuizRoundResult;
      	    $result->question_id = $request->question_id;
      	    $result->question_type = $request->question_type;
      	    $result->round_id = $request->quiz_round_id;
      	    $result->student_id = $request->auth_id;
      	    $result->check_answer = $check_ans;
      	    $result->refered_by = $request->refered_by_id;
            $result->answered = $given_answer;
      	      $check=ReferQuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'refered_by'=>$request->refered_by_id])->exists();
      	    if(!$check){
      	    	$result->save();
      	    }else{
      	    	ReferQuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'refered_by'=>$request->refered_by_id])->delete();
      	    	$result->save();
      	    }

      	 }

          return (json_encode(array('status'=>'success','message'=>'Quiz Round Attended by Student')));
      }


   }


  

   public function refer_calculate_score(Request $request){
   		if($request->ajax())
      {
      	 $quiz_score;
      	
      	    $quiz_duration = $request->quiz_duration;
      	    $quiz_round_id = $request->quiz_round_id;
      	    $auth_id = $request->auth_id;
      	   

      	    $score = ReferQuizRoundResult::where(array('student_id'=>$auth_id,'round_id'=>$quiz_round_id,'check_answer'=>1,'refered_by'=>$request->refered_by_id))->count();

      	   

      	    $total_que = PublishQuestions::where(array('quiz_round_id'=>$quiz_round_id,'published'=>1))->count();


      	 	 $result  = DB::statement("UPDATE dg_refer_quiz_wq SET score='".$score."',total_question='".$total_que."',quiz_name='".$request->quiz_name."',round_name='".$request->quiz_round_name."', attended=3,quiz_duration=IF('".$quiz_duration."'='00:00:00',TIME_FORMAT(TIMEDIFF(NOW(),quiz_started_at), '%T'),'".$quiz_duration."') WHERE student_id=$auth_id AND round_id=$quiz_round_id AND refered_by=$request->refered_by_id");

      	    if($result){
      	     return (json_encode(array('status'=>'success','message'=>'Quiz Round Attended by Student')));
      	    }

          
      }


   }

    public function refer_show_result(){
      $args = func_get_args();
      $round_id= $args[0];
      $currentuserid = Auth::user()->id;
     
      $referer = User::where('id',$currentuserid)->first();

     
      $result = DB::table('refer_quiz_wq')->join('publish_quiz_round_wq', 'refer_quiz_wq.round_id', '=', 'publish_quiz_round_wq.id')->select('refer_quiz_wq.*','publish_quiz_round_wq.round_name as quizroundname')->where(array('refer_quiz_wq.student_id'=>$currentuserid,'refer_quiz_wq.round_id'=>$round_id))->first();

       return view('student.refer_result',compact('result'));
   }




    public function refer_submit_quiz_reload(Request $request){

   
   	if($request->ajax())
      {

     /* $delete_result = QuizRoundResult::where(['round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'school_id'=>$request->school_id]);
       $delete_result->delete();*/

      	 $quiz_result;
      	 if($request->question_type==1){
      	    $answers = $request->answers;
             $answers = array_map(function($v){
                return trim($v);},$answers);
      	    $answ = array();
      	    $real_answers = PublishAnswers::where('question_id',$request->question_id)->get();
      	    foreach ($real_answers as $ans) {
              array_push($answ,trim($ans->correct_answer)); 
            }
      	    if(array_map('strtolower', $answ) == array_map('strtolower', $answers)){
      	    	$check_ans = 1;
      	    }else{
      	    	$check_ans = 0;
      	    }
      	 	$result = New ReferQuizRoundResult;
      	    $result->question_id = $request->question_id;
      	    $result->question_type = $request->question_type;
      	    $result->round_id = $request->quiz_round_id;
      	    $result->student_id = $request->auth_id;
      	    $result->check_answer = $check_ans;
      	    $result->refer_by = $request->refered_by_id;

      	    $check=ReferQuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'refer_by'=>$request->refered_by_id])->exists();
      	    if(!$check){
      	    	$result->save();
      	    }

      	 }

      	 if($request->question_type==2){

      	 	$answers = $request->answers;
      	    $answ = array();
      	   
            $val=$request->question_id;

      	   $real_answers = PublishAnswers::where(array('correct_answer'=>1,'question_id'=>$val))->get();

        

      	    foreach ($real_answers as $ans) {

      	    	array_push($answ,$ans->option_text); 
      	    }
      	   
      	    if(count($answers)>0){
            if(array_map('strtolower', $answ) == array_map('strtolower', $answers)){
              $check_ans = 1;
            }else{
              $check_ans = 0;
            }
            }else{
              $check_ans = 0;
            }
      	 	$result = New ReferQuizRoundResult;
      	    $result->question_id = $request->question_id;
      	    $result->question_type = $request->question_type;
      	    $result->round_id = $request->quiz_round_id;
      	    $result->student_id = $request->auth_id;
      	    $result->check_answer = $check_ans;
      	    $result->refer_by = $request->refered_by_id;

      	    $check=ReferQuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'refer_by'=>$request->refered_by_id])->exists();
      	    if(!$check){

      	    	$result->save();
      	    }
      	  
      	 }


      	 if($request->question_type==3){

      	 	$answers = $request->answers;
      	    $answ = array();
      	   
            $val=$request->question_id;

      	   $real_answers = PublishAnswers::where(array('question_id'=>$val))->get();

      	    foreach ($real_answers as $ans) {

      	    	array_push($answ,$ans->correct_answer); 
      	    }
      	   
      	    if(array_map('strtolower', $answ) == array_map('strtolower', $answers)){
      	    	$check_ans = 1;
      	    }else{
      	    	$check_ans = 0;
      	    }
      	 	$result = New ReferQuizRoundResult;
      	    $result->question_id = $request->question_id;
      	    $result->question_type = $request->question_type;
      	    $result->round_id = $request->quiz_round_id;
      	    $result->student_id = $request->auth_id;
      	    $result->check_answer = $check_ans;
      	    $result->refer_by = $request->refered_by_id;

      	    $check=ReferQuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'refer_by'=>$request->refered_by_id])->exists();
      	    if(!$check){
      	    	$result->save();
      	    }
      	 }

          return (json_encode(array('status'=>'success','message'=>'Quiz Round Attended by Student')));
      }


   }


  

   public function refer_calculate_score_reload(Request $request){
   		if($request->ajax())
      {

      	
      	 $quiz_score;
      	
      	    $quiz_duration = $request->quiz_duration;
      	    $quiz_round_id = $request->quiz_round_id;
      	    $auth_id = $request->auth_id;

      	    $score = ReferQuizRoundResult::where(array('student_id'=>$auth_id,'round_id'=>$quiz_round_id,'check_answer'=>1,'refered_by'=>$request->refered_by_id))->count();

      	   

      	    $total_que = PublishQuestions::where(array('quiz_round_id'=>$quiz_round_id,'published'=>1))->count();


      	 

           $result  = DB::statement("UPDATE dg_refer_quiz_wq SET score='".$score."',total_question='".$total_que."',quiz_name='".$request->quiz_name."',round_name='".$request->quiz_round_name."', attended=3,quiz_duration=IF('".$quiz_duration."'='00:00:00',TIME_FORMAT(TIMEDIFF(NOW(),quiz_started_at), '%T'),'".$quiz_duration."') WHERE student_id=$auth_id AND round_id=$quiz_round_id AND refered_by=$request->refered_by_id");

      	    if($result){
      	     return (json_encode(array('status'=>'success','message'=>'Quiz Round Attended by Student')));
      	    }

          
      }


   }


  public function refered_rounds(){
   
    $args = func_get_args();
      $quiz_id= $args[0];

     $quiz_name = Quiz::find($quiz_id); 

    $count_refered_rounds = DB::table('refer_quiz_wq')->join('publish_quiz_round_wq', 'refer_quiz_wq.round_id', '=', 'publish_quiz_round_wq.id')->join('quiz_wq', 'publish_quiz_round_wq.quiz_id', '=', 'quiz_wq.id')->select('publish_quiz_round_wq.round_name','publish_quiz_round_wq.id as quizroundid','refer_quiz_wq.*')->where(array('refer_quiz_wq.student_id'=>Auth::user()->id,'refer_quiz_wq.attended'=>0,'publish_quiz_round_wq.quiz_id'=>$quiz_id))->groupBy('publish_quiz_round_wq.id')->count(); 

    if($count_refered_rounds>0){
      $refered_rounds = DB::table('refer_quiz_wq')->join('publish_quiz_round_wq', 'refer_quiz_wq.round_id', '=', 'publish_quiz_round_wq.id')->join('quiz_wq', 'publish_quiz_round_wq.quiz_id', '=', 'quiz_wq.id')->select('publish_quiz_round_wq.round_name as roundname','publish_quiz_round_wq.id as quizroundid','refer_quiz_wq.*')->where(array('refer_quiz_wq.student_id'=>Auth::user()->id,'refer_quiz_wq.attended'=>0,'publish_quiz_round_wq.quiz_id'=>$quiz_id))->groupBy('publish_quiz_round_wq.id')->get(); 
    }else{
      $refered_rounds='';
    }

    if($refered_rounds!=''){
     return view('student.refered_quiz_rounds',compact('refered_rounds','quiz_name'));
     }else{
       return abort(404); 
     }
    
    
  
    
  
  }

  public function refered_quiz_attended(Request $request){
     if($request->ajax())
      {


        $rules = array(
          'id'     => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        $assign_round;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['id']))
            $errorMsg = $failedRules['id'][0] . "\n";
        
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{

           $id=$request->id;

           $attend_quiz = ReferQuiz::find($id);
                $attend_quiz->attended = 1;
                $attend_quiz->quiz_started_at = NOW();
                $attend_quiz->save();

                /** WebQuiz changes for Digital Bank merger **/
                
                $parameter_enc = Crypt::encrypt(Auth::user()->mobile.'^'.$attend_quiz->id);   

              
        }
   
            return (json_encode(array('status'=>'success','message'=>'Quiz Round Attended by Student','parameter_enc'=>$parameter_enc)));
                   
          
      }
   }

}
