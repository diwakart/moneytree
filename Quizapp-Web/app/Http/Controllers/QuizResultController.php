<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\User;
use App\AssignRound;
use App\School;
use App\QuizRound;
use App\PublishQuizRound;
use App\Quiz;
use App\Question;
use App\QuizRoundResult;
use App\Answer;
use App\StudentRank;
use App\PublishQuestions;
use Redirect;
use Session;
use Response;
use Validator;
use DB;
use Storage;
use Image;
use Excel;



class QuizResultController extends Controller
{
    public function index(){
        
    
    $declared_school = AssignRound::join('school_wq', 'school_wq.id', '=', 'assign_round_wq.school_id')->where(['attended'=>2,'is_result_declared'=>2])->select('assign_round_wq.school_id','school_wq.id as id','school_wq.name')->groupBy('assign_round_wq.school_id')->get();
      
      $schools = School::all();
     return view('admin.quiz_result',compact('schools','declared_school'));
   }



    public function getAllResults(Request $request)
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        if($page=='1')
        {
          $offset = 0;
        }
        else
        {
          $offset = ($page-1)*$perpage;
        }

        $Quiz = DB::table('assign_round_wq')->join('publish_quiz_round_wq', 'assign_round_wq.round_id', '=', 'publish_quiz_round_wq.id')->join('school_wq', 'school_wq.id', '=', 'assign_round_wq.school_id')->select('assign_round_wq.*','school_wq.name')->groupBy('school_wq.id','assign_round_wq.round_id')->offset($offset)->limit($perpage)->get();
        $total = $Quiz->count();
        $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];

        //$Quiz = Quiz::get(array('id','user_id','title','status'));
        $quiz_data = [];
        $Quiz = json_decode(json_encode($Quiz));
        if(!empty($Quiz))
        {
          $i = 1;
          foreach ($Quiz as $key => $value) {
          $quiz_data[] = (array)$value;
          $quiz_data[$key]['S.no'] = $offset+1;
          $offset++;
        }
        return Response::json(array('data'=> $quiz_data,'meta'=> $meta));
        //return Response::json($quiz_data);
        }
        return Response::json(array('data'=> $Quiz,'meta'=> $meta));
        //return Response::json($Quiz);
      }

   

   public function getallstudentWithResult(Request $request)
    {
      if($request->ajax())
      {

        $status = "success";
        if($request->school_id && $request->round_id)
          {

            $allStudentsList =DB::table('assign_round_wq')->join('users', 'assign_round_wq.student_id', '=', 'users.id')->where(['assign_round_wq.school_id'=>$request->school_id,'assign_round_wq.round_id'=>$request->round_id,'assign_round_wq.attended'=>2])->select('assign_round_wq.*','users.name','users.picture')->get();

            $allStudentsListCount =DB::table('assign_round_wq')->join('users', 'assign_round_wq.student_id', '=', 'users.id')->where(['assign_round_wq.school_id'=>$request->school_id,'assign_round_wq.round_id'=>$request->round_id,'assign_round_wq.attended'=>2])->select('assign_round_wq.*','users.name','users.picture')->count();

            $res = array();
            


            if(!empty($allStudentsList))
            {
              foreach($allStudentsList as $stud)
              {
               if($stud->picture!='' ){
                 $profile_img = url('/').Storage::url('app/'.$stud->picture);
               }else{
                 $profile_img = url('/').Storage::url('app/dummy.jpg');
               }
                $data = array(
                  'name'  => ucwords($stud->name),
                  'profile_img'  => $profile_img,
                  'score'  => $stud->score,
                  'total_question'  => $stud->total_question,
                  'quiz_duration'  => $stud->quiz_duration,
                  'quiz_name'  => $stud->quiz_name,
                  'round_name'  => $stud->round_name,
                );
                array_push($res,$data);
              }
             return (json_encode(array('status'=>$status,'student'=>$res,'total_student'=>$allStudentsListCount))) ;
            }

          }

                 
      }
    }

    public function upload_result(){
      $quiz = DB::table('quiz')->get();
      return view('admin.upload_result',compact('quiz'));        
    }

   
    public function upload_result_getSchool_id(Request $request)
    {
      if($request->ajax())
      {

        $status = "success";
        if($request->round_id)
          {
            $allschool = DB::table('school')->get();

            $res = array();
            if(!empty($allschool))
            {
              foreach($allschool as $stud)
              {
              
                $data = array(
                  'id'  => $stud->id,
                  'name'  => ucwords($stud->name),
                );
                array_push($res,$data);
              }
             return (json_encode(array('status'=>$status,'school'=>$res)));
            }

          }

                 
      }
    }

    public function upload_result_getquiz_id(Request $request)
    {
      if($request->ajax())
      {

        $status = "success";
        if($request->quiz_id)
          {
            $allQuizRound = DB::table('publish_quiz_round_wq')->where(['publish_quiz_round_wq.quiz_id'=>$request->quiz_id,'status'=>1])->get();

            $res = array();
            if(!empty($allQuizRound))
            {
              foreach($allQuizRound as $stud)
              {
              
                $data = array(
                  'id'  => $stud->id,
                  'round_name'  => ucwords($stud->round_name),
                );
                array_push($res,$data);
              }
             return (json_encode(array('status'=>$status,'quiz_round'=>$res)));
            }

          }

                 
      }
    }
    
    public function getDeclaredSchoolsRound(Request $request)
    {
        if($request->ajax())
        {
        
            $status = "success";
            if($request->school_id)
            {
                $allDeclaredRound = AssignRound::join('publish_quiz_round_wq','publish_quiz_round_wq.id','=','assign_round_wq.round_id')->join('quiz_wq','quiz_wq.id','=','publish_quiz_round_wq.quiz_id')->select('assign_round_wq.round_id as id','publish_quiz_round_wq.round_name','quiz_wq.title')->where(['assign_round_wq.attended'=>2,'assign_round_wq.is_result_declared'=>2,'assign_round_wq.school_id'=>$request->school_id])->groupBy('assign_round_wq.round_id')->get();
                if(!empty($allDeclaredRound))
                {
                    return (json_encode(array('status'=>$status,'school_round'=>$allDeclaredRound)));
                }
                else
                {
                   return (json_encode(array('status'=>'error','school_round'=>$allDeclaredRound))); 
                }
            }
        
        
        }
    }
    
    public function getDeclaredSchoolsQuiz(Request $request)
    {
        if($request->ajax())
        {
        
            $status = "success";
            if($request->school_id)
            {
                $allDeclaredQuiz = AssignRound::join('publish_quiz_round_wq','publish_quiz_round_wq.id','=','assign_round_wq.round_id')->join('quiz_wq','quiz_wq.id','=','publish_quiz_round_wq.quiz_id')->select('quiz_wq.title','quiz_wq.id')->where(['assign_round_wq.attended'=>2,'assign_round_wq.is_result_declared'=>2,'assign_round_wq.school_id'=>$request->school_id])->groupBy('quiz_wq.id')->get();
                if(!empty($allDeclaredQuiz))
                {
                    return (json_encode(array('status'=>$status,'school_quiz'=>$allDeclaredQuiz)));
                }
                else
                {
                   return (json_encode(array('status'=>'error','school_quiz'=>$allDeclaredQuiz))); 
                }
            }
        
        
        }
    }


    public function export_result_csv(Request $request){
        $school = School::where('id',$request->school_id)->first();
        $quiz   = Quiz::where('id',$request->quiz_id)->first();
        $round  = PublishQuizRound::where('id',$request->round_id)->first();
        $questions = PublishQuestions::where(['quiz_round_id'=>$request->round_id,'published'=>1])->get();
        $ques = array();
        $ques_text = array();
               
        $i=1;
        foreach($questions as $que) {
            $ques[] = 'Question '.$i;
            $ques_text[] =$que->question;
            $i++;
        }
        $array1 = array('Student Name', 'Contact Number', 'School Name', 'Quiz Name','Round Name');
        $array_duration = array('Duration (format: 01H 00M 00S 00MS)');
        $merge = array_merge($array1, $ques,$array_duration);
        
        $array2 = array('', '', '','Correct Answer: Enter Y','Incorrect Answer : Enter N');
        $merge2 = array_merge($array2, $ques_text);
        
        $filename = "result.csv";
        $handle = fopen($filename, 'w+');
        
        fputcsv($handle, $merge);
        fputcsv($handle, $merge2);
        
        
        
        $students = DB::table('users')->join('school_wq', 'users.school_id', '=', 'school_wq.id')->join('assign_round_wq', 'users.id', '=', 'assign_round_wq.student_id')->where(['assign_round_wq.school_id'=>$request->school_id,'assign_round_wq.round_id'=>$request->round_id,'assign_round_wq.attended'=>0])->select('users.*','school_wq.name as school_name')->get();
        
        $quiz_name = DB::table('quiz_wq')->where(['id'=>$request->quiz_id])->first();
        $round_name = DB::table('publish_quiz_round_wq')->where(['id'=>$request->round_id])->first();
        $j=1;
        $array_new = array();

        /** WebQuiz changes for Digital Bank merger **/

        foreach($students as $stu) {
            $array_new = array($stu->name, $stu->mobile, $stu->school_name,$quiz_name->title,$round_name->round_name);
            fputcsv($handle, $array_new);
           
        }
        fclose($handle);
        
        $headers = array(

          'Content-Type' => 'text/csv',
        );
        
        return Response::download($filename, 'result.csv', $headers);
    }


    public function importCSVResult(Request $request)
    {
     if(Input::hasFile('csv_file_result')){

      $path = Input::file('csv_file_result')->getRealPath();
      $data = Excel::load($path, function($reader) {
      })->get()->toArray();
      $quiz_id = $request->UploadResultQuizId;
      $round_id = $request->UploadResultRoundId;
      $school_id = $request->UploadResultSchoolId;
      $upload_settings_arr = array('quiz_id'=>$quiz_id,'round_id'=>$round_id,'school_id'=>$school_id);
      $upload_settings = json_encode($upload_settings_arr);
      $check_rank = AssignRound::where(['round_id'=>$round_id,'school_id'=>$school_id])->where(function($query){
                      $query->where('is_result_declared' , '=' ,'2');
                   
                })->count('id');


      if($check_rank>0){
        Session::flash('errormessage', 'Result declared for this School and Quiz Round'); 
        return back();
      }else{
        if(!empty($data) && count($data)>2){
        //All data row
          $SchoolName = School::where(['id'=>$school_id])->first();
          $RoundName = PublishQuizRound::where(['quiz_id'=>$quiz_id,'id'=>$round_id,'status'=>'1'])->first();
          if($SchoolName->name == $data[1]['school_name'] && $RoundName->round_name==$data[1]['round_name']){
              $csv_file='';
              $csv_file=time().'.'.$request->file('csv_file_result')->getClientOriginalExtension();
              $NewFilePath=$request->file('csv_file_result')->move(public_path('csv'), $csv_file);
              $insert_file = DB::table('import_csv_wq')->insert([
                                  ['file_name' => $csv_file,'start_row'=>2,'end_row'=>100,'status'=>2,'file_type'=>'student_result','upload_settings'=>$upload_settings]
                      ]);
              if($insert_file){
                  Session::flash('successmessage', 'File is successfully saved and processing data.');
                  return back();
              }else{
                  Session::flash('errormessage', 'Unable to process record. Please try again.');
                  return back();
              }
          }else{
              Session::flash('errormessage', 'Upload result for School name and Round name mentioned in csv file');
              return back(); 
          }

        }else{
          Session::flash('errormessage', 'No data available');
          return back(); 
        }
      }
      
      
      

    }else{
      Session::flash('errormessage', 'File not selected'); 
      return back();
    }
    
  }

public function declare_result(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = AssignRound::where(['round_id'=>$request->round_id,'school_id'=>$request->school_id])->first();
          $today_date = strtotime(date('Y-m-d H:i'));
         // $date = str_replace('/', '-', $result->expiration_date);
          $expiration_date = strtotime(date('Y-m-d H:i', strtotime($result->expiration_date)));
          if($today_date==$expiration_date || $today_date>$expiration_date){
            $declare_check = 2;//today is expiration date
          }else if($today_date<$expiration_date){
            $declare_check = 1; // coming soon
          }

         /* $tommorow_date = strtotime(date('Y-m-d H:i',strtotime($expiration_date . "+1 days")));
          if($tommorow_date<=$today_date){
            $result_declare_day = "now";
          }else{
            $result_declare_day = "not_now";
          }*/
    
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result,'declare_check'=>$declare_check,'expiration_date'=>date('Y-m-d H:i',$expiration_date)))) ;         
      }
    }


    public function declare_result_for_school_and_round(Request $request)
    {
      if($request->ajax())
      {
          $status = "success";
        if($request->school_id && $request->round_id)
          {


        /*DB::statement(DB::raw('set @rownumber=0'));
          $update_rank = DB::update('UPDATE assign_round SET rank = CASE WHEN score = 0 THEN 0 ELSE (@rownumber:=@rownumber+1) END,attended=4 WHERE school_id='.$request->school_id.' AND round_id='.$request->round_id.' and attended=3  ORDER BY (score + earn_points) DESC, quiz_duration ASC');*/

          $countAttended = AssignRound::where(['school_id'=>$request->school_id,'round_id'=>$request->round_id,'attended'=>2])->count();
         
            $update_result_declared = DB::update('UPDATE assign_round_wq  SET is_result_declared=2 WHERE school_id='.$request->school_id.'   AND round_id='.$request->round_id);


          if($countAttended>0){

             

            $update_rank =  DB::update('UPDATE assign_round_wq ar_s SET ar_s.is_result_declared=2,ar_s.rank = (SELECT ranks FROM ( SELECT id, @curRank := IF(@prevRank = CONCAT(score + earn_points , assign_round_wq.quiz_duration), @curRank, @incRank) AS ranks, IF(@prevRank = CONCAT(score + earn_points , assign_round_wq.quiz_duration), @curRank,@incRank := @incRank + 1) AS prevr, 
@prevRank := CONCAT(score + earn_points , assign_round_wq.quiz_duration)  AS newr FROM assign_round_wq, (
SELECT @curRank :=0, @prevRank := NULL, @incRank := 1
) r
WHERE school_id='.$request->school_id.' AND round_id='.$request->round_id.' AND attended=2 ORDER BY (score + earn_points) DESC, CAST(assign_round_wq.quiz_duration AS TIME) ASC ) res WHERE res.id=ar_s.id) WHERE school_id='.$request->school_id.' AND attended=2  AND round_id='.$request->round_id);
          

            if($update_rank)
            {
               $publishQuizRoundData = PublishQuizRound::where(['id'=>$request->round_id])->first();

               /** WebQuiz changes for Digital Bank merger **/

               $this->sms_queue(User::select(DB::raw('name as sms_to_name,country_code as country_code,mobile as mobiles'))->where(['type'=>2,'school_id'=>$request->school_id])->where('mobile' , '<>' ,'')->get()->toArray(),$publishQuizRoundData);


             return (json_encode(array('status'=>$status,'message'=>"Result Declare Successfully"))) ;
            }

          }else{
            $update_rank = DB::update('UPDATE assign_round_wq SET rank=0,is_result_declared=2 WHERE school_id='.$request->school_id.' AND round_id='.$request->round_id);
          

            if($update_rank)
            {
               $publishQuizRoundData = PublishQuizRound::where(['id'=>$request->round_id])->first();

               /** WebQuiz changes for Digital Bank merger **/

               $this->sms_queue(User::select(DB::raw('name as sms_to_name,country_code as country_code,mobile as mobiles'))->where(['type'=>2,'school_id'=>$request->school_id])->where('mobile' , '<>' ,'')->get()->toArray(),$publishQuizRoundData);


             return (json_encode(array('status'=>$status,'message'=>"Result Declare Successfully"))) ;
            }
          }


          

          
          }
      }
    }

    public function getStudentsResult($id)
  {
    $school = DB::table('school_wq')->where('id',$id)->first();
    return  view('admin.quiz_school_result',compact('school'));
  }

  public function getSchoolsResult(Request $request,$school_id,$round_id)
  {
    $perpage = $request->datatable['pagination']['perpage'];
    $page = $request->datatable['pagination']['page'];
    $value = (isset($request->datatable['query']['value']) && ($request->datatable['query']['value']!="") ? $request->datatable['query']['value'] : '');
    if($page=='1')
    {
      $offset = 0;
    }
    else
    {
      $offset = ($page-1)*$perpage;
    }
    $school_data = [];
    $allStudentsList = new AssignRound;
    $allStudentsList =$allStudentsList->join('users', 'assign_round_wq.student_id', '=', 'users.id')->where(['assign_round_wq.school_id'=>$school_id,'assign_round_wq.round_id'=>$round_id])->select('assign_round_wq.*','users.name');
    $total =DB::table('assign_round_wq')->join('users', 'assign_round_wq.student_id', '=', 'users.id')->where(['assign_round_wq.school_id'=>$school_id,'assign_round_wq.round_id'=>$round_id])->orderby('rank','asc')->count();
    if($value!=='')
    {
        if (is_numeric($value)) {

          /** WebQuiz changes for Digital Bank merger **/

            $allStudentsList =$allStudentsList->where('users.mobile','like',$value.'%');
        }
        else
        {
           $allStudentsList =$allStudentsList->where('users.name','like',$value.'%'); 
        }
         $total =$allStudentsList->count();
        // $allStudentsList =$allStudentsList->orwhere('users.mobile','like',$value.'%');
    }
    $allStudentsList =$allStudentsList->orderby('rank','asc')->offset($offset)->limit($perpage)->get();
    
    $allStudentsList = json_decode(json_encode($allStudentsList));
    $meta = ['draw'=>1,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
    if(!empty($allStudentsList))
    {
      $i = 1;
      foreach ($allStudentsList as $key => $value) {
        $school_data[] = (array)$value;
        $school_data[$key]['S.no'] = $offset+1;
        $offset++;
      }
      return Response::json(array('data'=> $school_data,'meta'=> $meta));
      //return Response::json($school_data);
    }
    return Response::json(array('data'=> $allStudentsList,'meta'=> $meta));
    //return Response::json($allStudentsList);
  }


    public function selected_quiz_rank(Request $request)
    {

      if($request->ajax())
      {

        $status = "success";
        if($request->school_id)
          {
           
          

            $countallstudent =  DB::select("SELECT rank from assign_round_wq WHERE round_id=".$request->round_id." AND  school_id=".$request->school_id." AND attended=2 AND is_result_declared=2 AND rank!=0 ORDER BY rank ASC");

            if(count($countallstudent)>10){
             /* $getlastrank =  DB::select("SELECT rank from assign_round WHERE round_id=".$request->round_id." AND  school_id=".$request->school_id." AND attended=2 AND is_result_declared=2 AND rank!=0 ORDER BY (score + earn_points) DESC, quiz_duration ASC LIMIT 10,1");*/

               $getlastrank =  DB::select("SELECT rank from assign_round_wq WHERE round_id=".$request->round_id." AND  school_id=".$request->school_id." AND attended=2 AND is_result_declared=2 AND rank!=0 ORDER BY rank ASC LIMIT 9,1");


           
           $last_rank = $getlastrank[0]->rank;




           $allStudentsList =  DB::select("SELECT users.name,users.id as userid,assign_round_wq.rank,(assign_round_wq.score + assign_round_wq.earn_points) as final_score, assign_round_wq.score , assign_round_wq.earn_points,assign_round_wq.quiz_duration FROM assign_round_wq INNER JOIN users ON assign_round_wq.student_id=users.id WHERE assign_round_wq.round_id=".$request->round_id." AND  assign_round_wq.school_id=".$request->school_id." AND assign_round_wq.attended=2 AND is_result_declared=2 AND (assign_round_wq.rank<=$last_rank) AND assign_round_wq.rank!=0  ORDER BY assign_round_wq.rank ASC, users.name ASC");
            }else{


           $allStudentsList =  DB::select("SELECT users.name,users.id as userid,assign_round_wq.rank,(assign_round_wq.score + assign_round_wq.earn_points) as final_score, assign_round_wq.score , assign_round_wq.earn_points,assign_round_wq.quiz_duration FROM assign_round_wq INNER JOIN users ON assign_round_wq.student_id=users.id WHERE assign_round_wq.round_id=".$request->round_id." AND  assign_round_wq.school_id=".$request->school_id." AND assign_round_wq.attended=2  AND is_result_declared=2 AND assign_round_wq.rank!=0  ORDER BY assign_round.rank ASC, users.name ASC");
            }

           

         



           /* $myRank = DB::table('users')->join('assign_round', 'assign_round.student_id', '=', 'users.id')->select('users.name','users.id as userid','assign_round.rank','(assign_round.score + assign_round.earn_points) as final_score', 'assign_round.score' , 'assign_round.earn_points','assign_round.quiz_duration')->where(array('assign_round.school_id'=>$request->school_id,'assign_round.round_id'=>$request->round_id,'attended'=>4,'assign_round.student_id'=>Auth::user()->id))->first();*/

            $res = array();
            if(!empty($allStudentsList))
            {
              foreach($allStudentsList as $stud)
              {
               
                $data = array(
                  'name'  => ucwords($stud->name),
                  'duration'  => $stud->quiz_duration,
                  'points'  => $stud->score,
                  'final_score'  => $stud->final_score,
                  'rank'  => $stud->rank,
                  'userid' => $stud->userid,
                );
                array_push($res,$data);
              }

             return (json_encode(array('status'=>$status,'student'=>$res))) ;
            }

          }

                 
      }
    }


    public function selected_quiz_overall_rank(Request $request)
    {
        

      if($request->ajax())
      {

        $status = "success";
        if($request->school_id && $request->quiz_id)
          {

            $allRounds = DB::table('quiz_wq')->join('publish_quiz_round_wq', 'publish_quiz_round_wq.quiz_id', '=', 'quiz_wq.id')->join('assign_round_wq', 'assign_round_wq.round_id', '=', 'publish_quiz_round_wq.id')->select('assign_round_wq.round_id')->where(array('assign_round_wq.school_id'=>$request->school_id,'publish_quiz_round_wq.quiz_id'=>$request->quiz_id,'attended'=>2))->where(function($query){
                      $query->where('assign_round_wq.rank' , '<>' ,0);
                  
                })->groupBy('assign_round_wq.round_id')->get();

          

            $round_ids = array();
           

            foreach ($allRounds as $value) {
              array_push($round_ids,$value->round_id);
            }

            $ids = join("','",$round_ids); 

            $school = $request->school_id;
           
            
              

               //$getlastrank = DB::select("SELECT assign_round.*, (@rank := if(@avgrank = total , @rank, if(@avgrank := total, @rank + 1, @rank + 1 ) ) ) AS ranking FROM (SELECT assign_round.student_id AS studentid,assign_round.id,AVG(assign_round.rank) AS total,SUM(TIME_TO_SEC(assign_round.quiz_duration) ) AS final_duration, SUM((assign_round.score + assign_round.earn_points )) AS `score`,users.name AS username FROM assign_round JOIN `users` ON users.id=assign_round.student_id WHERE round_id IN ('$ids') AND assign_round.school_id=$school AND is_result_declared='2' AND attended='2' AND rank!=0 GROUP BY `student_id` ORDER BY total DESC, final_duration ASC) assign_round  cross join (SELECT @rank := 0, @avgrank := -1) params ORDER BY score DESC, final_duration ASC LIMIT 9,1");
               
               $getlastrank = DB::select("SELECT * FROM (SELECT assign_round_wq.*, (@rank := if(@avgrank = total , @rank, if(@avgrank := total, @rank + 1, @rank + 1 ) ) ) AS ranking FROM (SELECT assign_round_wq.student_id AS studentid,assign_round_wq.id,AVG(assign_round_wq.rank) AS total,(SUM(TIME_TO_SEC(assign_round_wq.quiz_duration)) + SUM((MICROSECOND(assign_round_wq.quiz_duration)/1000000)))  AS final_duration,LEFT(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(assign_round_wq.quiz_duration)) + SUM((MICROSECOND(assign_round_wq.quiz_duration)/1000000))),'%H:%i:%s.%f'),11) AS format_duration, SUM((assign_round_wq.score + assign_round_wq.earn_points )) AS `score`,users.name AS username FROM assign_round_wq JOIN `users` ON users.id=assign_round_wq.student_id WHERE round_id IN ('$ids') AND assign_round_wq.school_id=$school AND is_result_declared='2' AND attended='2' AND rank!=0 GROUP BY `student_id` ORDER BY total DESC, final_duration ASC) assign_round_wq  cross join (SELECT @rank := 0, @avgrank := -1) params ORDER BY score DESC, final_duration ASC ) AS Final_res  LIMIT 9,1");
               
              
           $last_rank_where = "";
            if(!empty($getlastrank)){
              $last_rank = $getlastrank[0]->ranking;
              $last_rank_where = " WHERE (ranking<=$last_rank)";
            }
              
              //$allQuizRank = DB::select("SELECT assign_round.*, (@rank := if(@avgrank = total , @rank, if(@avgrank := total, @rank + 1, @rank + 1 ) ) ) AS ranking FROM (SELECT assign_round.student_id AS studentid,assign_round.id,AVG(assign_round.rank) AS total,SUM(TIME_TO_SEC(assign_round.quiz_duration) ) AS final_duration, SUM((assign_round.score + assign_round.earn_points )) AS `score`,users.name AS username FROM assign_round JOIN `users` ON users.id=assign_round.student_id WHERE round_id IN ('$ids') AND assign_round.school_id=$school AND is_result_declared='2' AND attended='2' AND rank!=0 GROUP BY `student_id` ORDER BY total DESC, final_duration ASC) assign_round  cross join (SELECT @rank := 0, @avgrank := -1) params ".$last_rank_where." ORDER BY score DESC, final_duration ASC ");
            
              $allQuizRank = DB::select("SELECT * FROM ( SELECT assign_round_wq.*, (@rank := if(@avgrank = total , @rank, if(@avgrank := total, @rank + 1, @rank + 1 ) ) ) AS ranking FROM (SELECT assign_round_wq.student_id AS studentid,assign_round_wq.id,AVG(assign_round_wq.rank) AS total,(SUM(TIME_TO_SEC(assign_round_wq.quiz_duration)) + SUM((MICROSECOND(assign_round_wq.quiz_duration)/1000000)))  AS final_duration,LEFT(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(assign_round_wq.quiz_duration)) + SUM((MICROSECOND(assign_round_wq.quiz_duration)/1000000))),'%H:%i:%s.%f'),11) AS format_duration, SUM((assign_round_wq.score + assign_round_wq.earn_points )) AS `score`,users.name AS username FROM assign_round_wq JOIN `users` ON users.id=assign_round_wq.student_id WHERE round_id IN ('$ids') AND assign_round_wq.school_id=$school AND is_result_declared='2' AND attended='2' AND rank!=0 GROUP BY `student_id` ORDER BY total DESC, final_duration ASC) assign_round_wq  cross join (SELECT @rank := 0, @avgrank := -1) params ORDER BY score DESC, final_duration ASC ) AS Final_res $last_rank_where");
           
            $res = array();
            if(!empty($allQuizRank))
            {
              foreach($allQuizRank as $stud)
              {
                $data = array(
                  'name'  => ucwords($stud->username),
                  'score'  => $stud->score,
                  'quiz_duration'  => $stud->format_duration,
                  'student_id'  => $stud->studentid,
                  'rank'  => $stud->ranking,
                );
                array_push($res,$data);
              }

            
             return (json_encode(array('status'=>$status,'student'=>$res))) ;
            }


          

          }

                 
      }
    }

       public function sms_queue($data,$publishQuizRoundData)
    {

      

       $quizName = Quiz::where(['id'=>$publishQuizRoundData->quiz_id])->first(); 
     

      foreach ($data as $key => $value) {

        /** WebQuiz changes for Digital Bank merger **/

        $user_id = User::where(['mobile'=>$value['mobiles']])->first();
       


       $message_text =  $publishQuizRoundData->round_name." ".$quizName->title.". Who have you beaten so far? Find our Here :".url('/');

        $details['sms_from'] = '+15416159055';

        /** WebQuiz changes for Digital Bank merger **/
        
        $details['sms_to']   = $value['mobiles'];
        $details['sms_to_name']   = $value['sms_to_name'];
        $details['body_message'] = $message_text;
        $details['priority']    = '4';
        $details['country_code']   = $value['country_code'];
        DB::table('sms_queue_wq')->insert($details);
      }
    }

}
