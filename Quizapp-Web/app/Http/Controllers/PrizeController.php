<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Quiz;
use App\Prizes;
use App\AssignRound;
use App\AssignPrize;
use App\QuizRoundResult;
use App\PublishQuizRound;
use Redirect;
use Session;
use Response;
use Validator;
use DB;

class PrizeController extends Controller
{
   public function index(){
   	return view('admin.prizes');
   }

    public function add_prizes(){
     
      $args = func_get_args();
      
      if(isset($args[0]) && $args[0]!=''){
        $gift_id= $args[0];
        $prize = Prizes::where(['id'=>$gift_id])->first();
      }else{
        $prize = '';
      }


    return view('admin.add_prizes',compact('prize'));
   }

   public function getAllPrizes(Request $request)
    {
      $perpage = $request->datatable['pagination']['perpage'];
      $page = $request->datatable['pagination']['page'];
      if($page=='1')
      {
        $offset = 0;
      }
      else
      {
        $offset = ($page-1)*$perpage;
      }
      $allschools = \DB::table('prizes_wq')->orderby('id','asc')->offset($offset)->limit($perpage)->get();
      $total = \DB::table('prizes_wq')->orderby('id','asc')->count();
      $school_data = [];
      $allschools = json_decode(json_encode($allschools));
      $meta = ['draw'=>1,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
      if(!empty($allschools))
      {
        foreach ($allschools as $key => $value)
        {
          $school_data[] = (array)$value;
          $school_data[$key]['S.no'] = $offset+1;
          $offset++;
        }
        return Response::json(array('data'=> $school_data,'meta'=> $meta));
        //return Response::json($school_data);
      }
      return Response::json(array('data'=> $allschools,'meta'=> $meta));
      //return Response::json($allschools);
    }

    public function store(Request $request)
    {
        // print_r($request->all());
        // die;
        $id = $request->id;
        
         $check=Prizes::where(['name'=>$request->gift_title])
        ->where(function($query) use ($id){
            if(isset($id)){
              $query->where('id' , '<>' ,$id);
            }
        })->exists();
      if($check){
          Session::flash('errormessage', 'Prize Title already exist.'); 
          return back();
        }else{
          if(isset($id) && $id!='' ){
        $prize = Prizes::find($id);
      }else{
        $prize = new Prizes();
      }
      
     /** 21staug2018 quiz changes **/
    if($_POST['gift_point']!='')
    {
        $prize->points = $_POST['gift_point'];
        $prize->picture ='';
    }
    $prize->name = $_POST['gift_title'];
    $prize->description = $_POST['prize_desc'];
    //$prize->picture ='';
      if ($request->hasFile('gift_img'))
      {
        $image = $request->file('gift_img');
       $path = storage_path('app/prizes');
       $filename = time() . '.' . $image->getClientOriginalExtension();
       $image->move($path, $filename);

       $prize->picture = 'prizes/'.$filename;
       
      }

     
    if($prize->save()){
         Session::flash('successmessage', 'Prize saved Successfully'); 
        return Redirect::to(url('/').'/administration/prizes');
        } else{
         Session::flash('errormessage', 'Failed to save'); 
         return Redirect::to(url('/').'/administration/prizes');

      }
          
        }
    }

    public function delete(Request $request)
    {
      if($request->ajax())
      {
        if($request->id){
          $prizes = Prizes::find($request->id);
           
          if ($prizes->delete())
            return (json_encode(array('status'=>'success','message'=>"Prize Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }
    }

    public function assign_prizes(){
      $allRounds = DB::table('quiz_wq')->join('publish_quiz_round_wq', 'quiz_wq.id', '=', 'publish_quiz_round_wq.quiz_id')->select('quiz_wq.title','quiz_wq.id as quiz_id','publish_quiz_round_wq.*')->where(array('publish_quiz_round_wq.status'=>1))->get();

      $prizes = Prizes::get();
      return view('admin.assign_prize',compact('allRounds','prizes'));
    }

    public function get_selected_gift(Request $request){
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = Prizes::find($request->prize_id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }

    public function assiging_prizes(Request $request){
      if($request->ajax())
      {

        $rules = array(
          'prize'     => 'required',
         
        );

        $validator = Validator::make($request->all(), $rules);

        $assign_round;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['prize']))
            $errorMsg = $failedRules['prize'][0] . "\n";
         
         
          
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{
           $id=$request->id;
              $check=AssignPrize::where(['publish_round_id'=>$request->round_id])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();

              if($check){
                  return (json_encode(array('status'=>'error','message'=>"Selected Quiz Round already have prize." )));
                }
                
                
                  $assign_prize = new AssignPrize();
                  $assign_prize->publish_round_id = $request->round_id;
                  $assign_prize->prize_id = $request->prize;
                  $assign_prize->quiz_id = $request->quiz_id;
                 
                  if($assign_prize->save()){
                    return (json_encode(array('status'=>'success','message'=>'Assigned prize to select quiz round')));
                  }else{
                    return (json_encode(array('status'=>'error','message'=>'Failed to assign')));
                  }
        }
            
                   
          
      }
    }



    public function getAllAssignedPrizes(Request $request)
    {
      $perpage = $request->datatable['pagination']['perpage'];
      $page = $request->datatable['pagination']['page'];
      if($page=='1')
      {
        $offset = 0;
      }
      else
      {
        $offset = ($page-1)*$perpage;
      }

      

      $assigned_prizes = \DB::table('assign_prize_wq')->join('prizes_wq', 'prizes_wq.id', '=', 'assign_prize_wq.prize_id')->join('publish_quiz_round_wq', 'publish_quiz_round_wq.id', '=', 'assign_prize_wq.publish_round_id')->join('quiz_wq', 'publish_quiz_round_wq.quiz_id', '=', 'quiz_wq.id')->select('quiz_wq.title','publish_quiz_round_wq.round_name','prizes_wq.name','prizes_wq.description','prizes_wq.picture','assign_prize_wq.*')->orderby('assign_prize_wq.id','asc')->offset($offset)->limit($perpage)->get();
      $total = \DB::table('assign_prize_wq')->join('prizes_wq', 'prizes_wq.id', '=', 'assign_prize_wq.prize_id')->join('publish_quiz_round_wq', 'publish_quiz_round_wq.id', '=', 'assign_prize_wq.publish_round_id')->join('quiz_wq', 'publish_quiz_round_wq.quiz_id', '=', 'quiz_wq.id')->select('quiz_wq.title','publish_quiz_round_wq.round_name','prizes_wq.name','prizes_wq.description','prizes_wq.picture','assign_prize_wq.*')->orderby('id','asc')->count();
      $assigned_prize_data = [];
      $assigned_prizes = json_decode(json_encode($assigned_prizes));
      $meta = ['draw'=>1,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
      if(!empty($assigned_prizes))
      {
        foreach ($assigned_prizes as $key => $value)
        {
          $assigned_prize_data[] = (array)$value;
          $assigned_prize_data[$key]['S.no'] = $offset+1;
          $offset++;
        }
        return Response::json(array('data'=> $assigned_prize_data,'meta'=> $meta));
        //return Response::json($school_data);
      }
      return Response::json(array('data'=> $assigned_prizes,'meta'=> $meta));
      //return Response::json($allschools);
    }

     public function assigned_prize_delete(Request $request)
    {
      if($request->ajax())
      {
        if($request->id){
          $assign_prizes = AssignPrize::find($request->id);
           
          if ($assign_prizes->delete())
            return (json_encode(array('status'=>'success','message'=>"Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }
    }

}
