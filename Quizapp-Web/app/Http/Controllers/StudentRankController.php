<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\User;
use App\AssignRound;
use App\School;
use App\QuizRound;
use App\PublishQuizRound;
use App\Quiz;
use App\Question;
use App\QuizRoundResult;
use App\Answer;
use App\StudentRank;
use App\PublishQuestions;
use Redirect;
use Session;
use Response;
use Validator;
use DB;
use Storage;
use Image;
use Excel;

class StudentRankController extends Controller
{
    
    public function index(){
      $declared_quiz_count = AssignRound::where(['is_result_declared'=>2,'student_id'=>Auth::user()->id])->count();
      if($declared_quiz_count>0){
          $declared_quiz = AssignRound::where(['is_result_declared'=>2,'student_id'=>Auth::user()->id])->get();
        
          $declared_quiz_name = DB::table('publish_quiz_round_wq')->join('assign_round_wq', 'assign_round_wq.round_id', '=', 'publish_quiz_round_wq.id')->join('quiz_wq', 'publish_quiz_round_wq.quiz_id', '=', 'quiz_wq.id')->select('quiz_wq.id as quiz_id','quiz_wq.title','assign_round_wq.*')->where(array('assign_round_wq.is_result_declared'=>2,'assign_round_wq.student_id'=>Auth::user()->id))->groupBy('quiz_wq.id')->get();


      }else{
        $declared_quiz ='';
        $declared_quiz_name='';
      }

       $args = func_get_args();
      if(isset($args[0])){
          
            $round_id= $args[0];
            $check_quiz_round = PublishQuizRound::where(['id'=>$round_id])->exists();
            if($check_quiz_round){
               $round=$round_id; 
            }else{
               return abort(404);
            }


      }else{
           $round= '';
      }
     
    


      
      return view('student.student_rank',compact('declared_quiz_count','declared_quiz','declared_quiz_name','round'));
    }

   

     public function selected_quiz_rank(Request $request)
    {

      if($request->ajax())
      {

        $status = "success";
        if($request->school_id)
          {

                $last_rank ='';
                $last_rank_where = "";
            /*$getlastrank =  DB::select("SELECT rank from assign_rounddf WHERE round_id=".$request->round_id." AND  school_id=".$request->school_id." AND attended=2 AND is_result_declared=2 AND rank!=0 ORDER BY (score + earn_points) DESC, quiz_duration ASC LIMIT 10,1");*/
            $getlastrank =  DB::select("SELECT rank from assign_round_wq WHERE round_id=".$request->round_id." AND  school_id=".$request->school_id." AND attended=2 AND is_result_declared=2 AND rank!=0 ORDER BY rank ASC LIMIT 9,1");

         
            if(!empty($getlastrank)){
              $last_rank = $getlastrank[0]->rank;
              $last_rank_where = " AND (assign_round_wq.rank<=$last_rank)";
            }

            $query = "SELECT users.name,users.id as userid,assign_round_wq.rank,(assign_round_wq.score + assign_round_wq.earn_points) as final_score, assign_round_wq.score , assign_round_wq.earn_points,assign_round_wq.quiz_duration FROM assign_round_wq INNER JOIN users ON assign_round_wq.student_id=users.id WHERE assign_round_wq.round_id=".$request->round_id." AND  assign_round_wq.school_id=".$request->school_id." AND assign_round_wq.attended=2 AND assign_round_wq.is_result_declared=2 ". $last_rank_where ." AND assign_round_wq.rank!=0  ORDER BY assign_round_wq.rank ASC, users.name ASC";
            
            $allStudentsList =  DB::select($query);
            
            

            $myRank =  DB::select("SELECT users.name,users.id as userid,assign_round_wq.rank,(assign_round_wq.score + assign_round_wq.earn_points) as final_score, assign_round_wq.score , assign_round_wq.earn_points,assign_round_wq.quiz_duration,assign_round_wq.attended FROM assign_round_wq INNER JOIN users ON assign_round_wq.student_id=users.id WHERE assign_round_wq.round_id=".$request->round_id." AND  assign_round_wq.school_id=".$request->school_id." AND  assign_round_wq.student_id=".Auth::user()->id." AND assign_round_wq.is_result_declared=2");
           
    
            $res = array();
            if(!empty($allStudentsList))
            {
              foreach($allStudentsList as $stud)
              {
               
                $data = array(
                  'name'  => ucwords($stud->name),
                  'duration'  => $stud->quiz_duration,
                  'points'  => $stud->score,
                  'final_score'  => $stud->final_score,
                  'rank'  => $stud->rank,
                  'userid' => $stud->userid,
                  
                );
                array_push($res,$data);
              }

             return (json_encode(array('status'=>$status,'student'=>$res,'myRank'=>$myRank))) ;
            }

          }

                 
      }
    }


       public function selected_quiz_overall_rank(Request $request)
    {

      if($request->ajax())
      {

        $status = "success";
        if($request->school_id && $request->quiz_id)
          {

            $school = $request->school_id;

           
           

            $getlastrank = DB::select("SELECT * FROM (SELECT assign_round_wq.*, (@rank := if(@avgrank = total , @rank, if(@avgrank := total, @rank + 1, @rank + 1 ) ) ) AS ranking FROM (SELECT assign_round_wq.student_id AS studentid,assign_round_wq.id,AVG(assign_round_wq.rank) AS total,(SUM(TIME_TO_SEC(assign_round_wq.quiz_duration)) + SUM((MICROSECOND(assign_round_wq.quiz_duration)/1000000)))  AS final_duration,LEFT(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(assign_round_wq.quiz_duration)) + SUM((MICROSECOND(assign_round_wq.quiz_duration)/1000000))),'%H:%i:%s.%f'),11) AS format_duration, SUM((assign_round_wq.score + assign_round_wq.earn_points )) AS `score`,users.name AS username FROM assign_round_wq JOIN `users` ON users.id=assign_round_wq.student_id WHERE round_id IN (SELECT DISTINCT `id` FROM `publish_quiz_round_wq` WHERE `quiz_id`=$request->quiz_id) AND assign_round_wq.school_id=$school AND is_result_declared='2' AND rank!=0 GROUP BY `student_id` ORDER BY total DESC, final_duration ASC) assign_round_wq  cross join (SELECT @rank := 0, @avgrank := -1) params ORDER BY score DESC, final_duration ASC) AS Final_res  LIMIT 9,1");


           
             $last_rank_where = "";
            if(!empty($getlastrank)){
              $last_rank = $getlastrank[0]->ranking;
              $last_rank_where = " WHERE (ranking<=$last_rank)";
            }
              
              $allQuizRank = DB::select("SELECT * FROM (SELECT assign_round_wq.*, (@rank := if(@avgrank = total , @rank, if(@avgrank := total, @rank + 1, @rank + 1 ) ) ) AS ranking FROM (SELECT assign_round_wq.student_id AS studentid,assign_round_wq.id,AVG(assign_round_wq.rank) AS total,(SUM(TIME_TO_SEC(assign_round_wq.quiz_duration)) + SUM((MICROSECOND(assign_round_wq.quiz_duration)/1000000)))  AS final_duration,LEFT(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(assign_round.quiz_duration)) + SUM((MICROSECOND(assign_round_wq.quiz_duration)/1000000))),'%H:%i:%s.%f'),11) AS format_duration, SUM((assign_round_wq.score + assign_round_wq.earn_points )) AS `score`,users.name AS username FROM assign_round_wq JOIN `users` ON users.id=assign_round_wq.student_id WHERE round_id IN (SELECT DISTINCT `id` FROM `publish_quiz_round_wq` WHERE `quiz_id`=$request->quiz_id) AND assign_round_wq.school_id=$school AND is_result_declared='2' AND attended='2' AND rank!=0 GROUP BY `student_id` ORDER BY total DESC, final_duration ASC) assign_round_wq  cross join (SELECT @rank := 0, @avgrank := -1) params ORDER BY score DESC, final_duration ASC) AS Final_res $last_rank_where");


              $user_id = Auth::user()->id;


            $res = array();
            $is_current_user=false;
            if(!empty($allQuizRank))
            {
              foreach($allQuizRank as $stud)
              {
               
                $data = array(
                  'name'  => ucwords($stud->username),
                  'score'  => $stud->score,
                  'quiz_duration'  => $stud->format_duration,
                  'student_id'  => $stud->studentid,
                  'rank'  => $stud->ranking,
                  'is_current_user' => 2
                );
                
                if($user_id==$stud->studentid){
                    $is_current_user=true;
                    $data['is_current_user'] = 1;
                }
                array_push($res,$data);
              }
              
              $myRank=array();
            if(!$is_current_user){
                $myRank = DB::select("SELECT * FROM (SELECT assign_round_wq.*, (@rank := if(@avgrank = total , @rank, if(@avgrank := total, @rank + 1, @rank + 1 ) ) ) AS ranking FROM (SELECT assign_round_wq.student_id AS studentid,assign_round_wq.attended,assign_round_wq.id,AVG(assign_round_wq.rank) AS total,(SUM(TIME_TO_SEC(assign_round_wq.quiz_duration)) + SUM((MICROSECOND(assign_round_wq.quiz_duration)/1000000)))  AS final_duration,LEFT(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(assign_round_wq.quiz_duration)) + SUM((MICROSECOND(assign_round_wq.quiz_duration)/1000000))),'%H:%i:%s.%f'),11) AS format_duration, SUM((assign_round_wq.score + assign_round_wq.earn_points )) AS `score`,users.name AS username FROM assign_round_wq JOIN `users` ON users.id=assign_round_wq.student_id WHERE round_id IN (SELECT DISTINCT `id` FROM `publish_quiz_round_wq` WHERE `quiz_id`=$request->quiz_id) AND assign_round_wq.school_id=$school AND is_result_declared='2' AND rank!=0 AND student_id=$user_id  GROUP BY `student_id` ORDER BY total DESC, final_duration ASC) assign_round_wq  cross join (SELECT @rank := 0, @avgrank := -1) params) AS Final_res");
            }
            
             return (json_encode(array('status'=>$status,'student'=>$res,'myownrank'=>$myRank))) ;
            }


          

          }

                 
      }
    }


}
