<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Auth;
use Session;
use App\ReferQuiz;
use App\AssignRound;
use \Crypt;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
     /*protected $redirectTo = '/student/home';*/

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
   
    
    
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
          /*  'email' => 'required|string|email|max:255|unique:users',*/
            'password' => 'required|string|min:6|confirmed',
            'school_id' => 'required',
            /** WebQuiz changes for Digital Bank merger **/
            'mobile' => 'required|max:12|unique:users',
            'country_code' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'school_id' =>$data['school_id'],

            /** WebQuiz changes for Digital Bank merger **/
            'mobile' => $data['mobile'],
            'country_code' => $data['country_code'],
            'school' => $data['other_school'],
        ]);
    }

     protected function registered(Request $request,$user)
    {

       
       $getallassignedquiz = AssignRound::where(['school_id'=>$user->school_id])->groupBy('round_id')->get();

        if($getallassignedquiz->count()>0){
        foreach ($getallassignedquiz as $value) {
            $assign_round_data[] = [
              'round_id' => $value->round_id,
              'school_id' => $user->school_id,
              'student_id' => $user->id,
              'attended' => 0,
              'is_result_declared' => $value->is_result_declared,
              'expiration_date' => $value->expiration_date,
              'quiz_duration' => "00:00:00.00",
              'total_question' => $value->total_question,
              'quiz_name' => $value->quiz_name,
              'round_name' => $value->round_name,
            ];
            
        }

         /** WebQuiz changes for Digital Bank merger **/

         DB::table('assign_round_wq')->insert($assign_round_data);
       }

        if(Session::get('quiz_url')){
                $quiz_url = Session::get('quiz_url');

                preg_match("/[^\/]+$/", $quiz_url, $matches);
                $last_word = $matches[0];


                $refer_id = Crypt::decrypt($last_word);
              
                $refered_data = explode('^',$refer_id);
                $refer_quiz = ReferQuiz::find($refered_data[1]);
                $refer_quiz->student_id = $user->id;
                $refer_quiz->save();

                 return redirect()->intended('/student/home');

        }else{
          return redirect()->intended('/student/home');
        }
    }
    

}
