<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\ReferQuiz;
use Auth;
use Cookie;
use \Crypt;
use Redirect;
use Session;
class LoginController extends Controller
{
    public function __construct() {
      $this->middleware('guest')->except('logout');
    }

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   /* protected $redirectTo = 'administration/home';*/

    /**
     * Create a new controller instance.
     *
     * @return void
     */


    protected function authenticated(Request $request,$user)
    {
         if(Session::get('quiz_url')){
                $quiz_url = Session::get('quiz_url');

                preg_match("/[^\/]+$/", $quiz_url, $matches);
                $last_word = $matches[0];


                $refer_id = Crypt::decrypt($last_word);
              
                $refered_data = explode('^',$refer_id);
                $refer_quiz = ReferQuiz::find($refered_data[1]);
                $refer_quiz->student_id = $user->id;
                $refer_quiz->save();

               return redirect()->intended('/student/home');

        }

        /** WebQuiz changes for Digital Bank merger **/

        if($user->type == 1) {
            return redirect()->intended('/administration/home');
        } elseif ($user->type == 2) {
            return redirect()->intended('/student/home');
        }
    }


}
