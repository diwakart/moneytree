<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Quiz;
use App\User;
use App\QuizRound;
use App\PublishQuizRound;
use App\Question;
use App\QuizRoundResult;
use App\AssignRound;
use App\Answer;
use Redirect;
use Session;
use Response;
use Validator;
use DB;

class QuizRoundController extends Controller
{
   
    public function index(){
     
      $args = func_get_args();
      $quiz_id= $args[0];
      Session::put('quiz_id', $quiz_id);
      $quiz = Quiz::where(array('id'=>$quiz_id))->select('id', 'title')
      ->first();
     if(count($quiz)>0){
       return view('admin.add_round',compact('quiz'));
      }else{
          return abort('404');
      }
    	
    }

   
    public function store(Request $request){
    	if($request->ajax())
      {

        $rules = array(
          'quiz_round'     => 'required',
          'active_days'     => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        $quizround;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['quiz_round']))
            $errorMsg = $failedRules['quiz_round'][0] . "\n";
          if(isset($failedRules['active_days']))
            $errorMsg = $failedRules['active_days'][0] . "\n";
          
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        } 
        else{
        	  $id=$request->id;
              $check=QuizRound::where(['round_name'=>$request->quiz_round])
              ->where(function($query) use ($id){
            if(isset($id)){
              $query->where('id' , '<>' ,$id);
            }
        })->exists();

              if($check){
                  return (json_encode(array('status'=>'error','message'=>"Quiz Round Name Already Exist." )));
                }else{

                $quizround = new QuizRound();
      	

        $quizround->quiz_id = $request->quiz_id;
        $quizround->active_days = $request->active_days;
        $quizround->round_name = $request->quiz_round;
        $quizround->user_id = $request->user_id;
        if($quizround->save()){
           Session::put('quiz_round_id', $quizround->id);
        	return (json_encode(array('status'=>'success','quizround_id' => $quizround->id,'round_name' => $quizround->round_name,'message'=>sprintf('Quiz Round "%s" successfully saved', $quizround->round_name))));
          } else{
          	 return (json_encode(array('status'=>'error','message'=>'Failed to Add Quiz Round'))) ;
          }
        }

       }
      }
    }

    public function getAllQuizRounds(Request $request)
    {
      $perpage = $request->datatable['pagination']['perpage'];
      $page = $request->datatable['pagination']['page'];
      if($page=='1')
      {
        $offset = 0;
      }
      else
      {
        $offset = ($page-1)*$perpage;
      }
      $quiz_id= Session::get('quiz_id');
      $quiz_data = [];
     //$Rounds = QuizRound::where(array('quiz_id'=>$quiz_id))->get(array('id','active_days','round_name','status'));
      $Rounds = \DB::table('quiz_round_wq')->select('id','active_days','round_name','status')->where('quiz_id',$quiz_id)->orderby('id','asc')->offset($offset)->limit($perpage)->get();
      $total = \DB::table('quiz_round_wq')->select('id','active_days','round_name','status')->where('quiz_id',$quiz_id)->count();
      $Rounds = json_decode(json_encode($Rounds));
      $meta = ['draw'=>1,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
      if(!empty($Rounds))
      {
        $i = 1;
        foreach ($Rounds as $key => $value) {
          $quiz_data[] = (array)$value;
          $quiz_data[$key]['S.no'] = $i;
          $i++;
        }
        return Response::json(array('data'=> $quiz_data,'meta'=> $meta));
        //return Response::json($quiz_data);
      }
        return Response::json(array('data'=> $Rounds,'meta'=> $meta));
        //return Response::json($Rounds);
    }
  
     public function delete(Request $request)
    {
      if($request->ajax())
      {
        if($request->id){
          $quiz_round = QuizRound::find($request->id);
                
          if ($quiz_round->delete())
            return (json_encode(array('status'=>'success','message'=>"Quiz Round deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Quiz Round is Found!!!")));
      }
    }


    public function edit_quiz_round(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = QuizRound::find($request->id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }



     public function update_quiz_round(Request $request)
    {
      if($request->ajax())
      {
        $user;
        $rules = array(
          'round_name'     => 'required',
          'active_days'     => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['round_name']))
            $errorMsg = $failedRules['round_name'][0] . "\n";
          if(isset($failedRules['active_days']))
            $errorMsg = $failedRules['active_days'][0] . "\n";
          
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        } else if($request->id){
          $quiz_round = QuizRound::find($request->id);
        }
        else{
          $quiz_round = new QuizRound();
          
        }

        $quiz_round->round_name = $request->round_name;
        $quiz_round->active_days = $request->active_days;
        if($quiz_round->save()){
          return (json_encode(array('status'=>'success','message'=>'Quiz Round updated successfully')));
          } else{
             return (json_encode(array('status'=>'error','message'=>'Failed to update Quiz Round'))) ;
          }
      }
    }

   
  public function publish_round(Request $request)
    {
      if($request->ajax())
      {
        if($request->id){
            $quiz_round = QuizRound::find($request->id);
            $quiz_round->status=1;
            $data = array(
                'type' =>'3',
                'notification_for_id' => $request->id,
                'start_row'=>'0',
                'end_row'=>'100',
            );
            if(!DB::table('send_notification')->where(['type'=>'3','notification_for_id' => $request->id])->exists())
            {
             DB::table('send_notification')->insert($data);
            }
            if ($quiz_round->save())
                return (json_encode(array('status'=>'success','message'=>"Quiz Round Published Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Quiz Round is Found!!!")));
      }
    }

    public function unpublish_round(Request $request)
    {
      if($request->ajax())
      {
        if($request->id){
          $quiz_round = QuizRound::find($request->id);
          $quiz_round->status=0;
          if ($quiz_round->save())
            return (json_encode(array('status'=>'success','message'=>"Quiz Round Unpublished Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Quiz Round is Found!!!")));
      }
    }

    
}
