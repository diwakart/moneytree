<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Quiz;
use App\User;
use App\Question;
use App\PublishQuestions;
use App\QuizRound;
use App\PublishQuizRound;
use App\Answer;
use App\PublishAnswers;
use Redirect;
use Session;
use Response;
use Validator;
use DB;
use Auth;


class QuestionController extends Controller
{

  public function index(){

     $quiz_id= Session::get('quiz_id');
     $Quiz = Quiz::where(array('id'=>$quiz_id))->select('title','id')->first();
     $args = func_get_args();
     $round_id= $args[0];
      Session::put('quiz_round_id', $round_id);
     $quiz_round = QuizRound::where(array('id'=>$round_id))->select('round_name','id', 'active_days','status')->first();
     
     if(count($quiz_round)>0){
       return view('admin.question',compact('Quiz','quiz_round'));
      }else{
          return abort('404');
      }

     
   }

    public function store_question_fill_blanks(Request $request){
     $user = Auth::user();
     if($request->ajax())
      {
       
        $question;
        $answer;
        $answers = $request->answers;
        if($answers!=''){

            if(isset($request->question_id)){
               $del_ans = Answer::where('question_id',$request->question_id);
               $del_ans->delete();

               $question = Question::find($request->question_id);
               $question->question = htmlspecialchars($request->question);
               
            }else{
              $question = new Question();
        
            }


               
               $question->question = htmlspecialchars($request->question);
               $question->quiz_round_id = $request->round_id;

               /** WebQuiz changes for Digital Bank merger **/
               $question->question_type= 1;
               $question->user_id= $user->id;
              if($question->save()){
                $last_insert_id = $question->id;
               
              }

              if(isset($request->answer_id)){
                $answer = Answer::find($request->answer_id);
               

              }else{
                foreach($answers as $fill_ans)
                {
                    $answer = new Answer();
                   
                    if(strlen($fill_ans)>0)
                    {
                      $answer->question_id = $last_insert_id;
                      $answer->correct_answer = $fill_ans;

                    
                      if($answer->save()){
        
                      } else{
                         return (json_encode(array('status'=>'error','message'=>'Failed to Add Question'))) ;
                      }
                    }

                }
              }

              return (json_encode(array('status'=>'success','message'=>'Question successfully saved')));
            }else{
              return (json_encode(array('status'=>'error','message'=>'Answer is not filled')));
            }

        }

    }



    public function store_question_multi_choice(Request $request){
     $user = Auth::user();
     if($request->ajax())
      {
       
        $question;
        $answer;
        $options = $request->options;
        $correct_answer = $request->correct_answer;

        if($request->question!='' || $request->round_id!='' ){
        if($options!=''){
         
         if (in_array("1", $correct_answer)){
            if(isset($request->question_id)){
               $del_ans = Answer::where('question_id',$request->question_id);
               $del_ans->delete();

                $question = Question::find($request->question_id);
                $question->question = htmlspecialchars($request->question);
            }else{
              $question = new Question();
            }


               
               $question->question = htmlspecialchars($request->question);
               $question->quiz_round_id = $request->round_id;

               /** WebQuiz changes for Digital Bank merger **/
               $question->question_type= 2;
               $question->user_id= $user->id;
              if($question->save()){
                $last_insert_id = $question->id;
               
              }

              if(isset($request->answer_id)){
                $answer = Answer::find($request->answer_id);
               
              }else{
                
                  foreach( $options as $index => $option ) {
                         $answer = new Answer();
                        
                        $answer->question_id = $last_insert_id;
                        $answer->option_text = $option;
                       
                        if($correct_answer[$index]==1){
                          $answer->correct_answer = $correct_answer[$index];
                        }else{
                          $answer->correct_answer = 0; 
                        }

                        if($answer->save()){
          
                        } else{
                           return (json_encode(array('status'=>'error','message'=>'Failed to Add Question'))) ;
                        }
                  }


              }

              return (json_encode(array('status'=>'success','message'=>'Question successfully saved')));
            }else{
              return (json_encode(array('status'=>'error','message'=>'Check correct answer(s)')));
            }
          }else{
             return (json_encode(array('status'=>'error','message'=>'Please fill all input box')));
          }
          }else{
             return (json_encode(array('status'=>'error','message'=>'Please fill all input box')));
          }



        }

    }



    public function store_question_arrange_order(Request $request){
     $user = Auth::user();
     if($request->ajax())
      {
       
        $question;
        $answer;
        $incorrect_order = $request->incorrect_order;
        $correct_order = $request->correct_order;

        
       if($request->question!='' ||  $request->round_id!=''){
        if($incorrect_order!=''){
         
         if ($correct_order!=''){
            if(isset($request->question_id)){
               $del_ans = Answer::where('question_id',$request->question_id);
               $del_ans->delete();
               
                $question = Question::find($request->question_id);
                $question->question = htmlspecialchars($request->question);
            }else{
              $question = new Question();
            }


                $question->question = htmlspecialchars($request->question);
                $question->quiz_round_id = $request->round_id;
                /** WebQuiz changes for Digital Bank merger **/
                $question->question_type= 3;
                $question->user_id= $user->id;
             
              
              if($question->save()){
                $last_insert_id = $question->id;
              }

              if(isset($request->answer_id)){
                $answer = Answer::find($request->answer_id);
              }else{
                
                  foreach( $incorrect_order as $index => $incorrectorder ) {
                        $answer = new Answer();
                        $answer->question_id = $last_insert_id;
                        $answer->option_text = $incorrectorder;
                        $answer->correct_answer = $correct_order[$index];
                        if($answer->save()){
          
                        } else{
                           return (json_encode(array('status'=>'error','message'=>'Failed to Add Question'))) ;
                        }
                  }


              }

              return (json_encode(array('status'=>'success','message'=>'Question successfully saved')));
            }else{
              return (json_encode(array('status'=>'error','message'=>'Arrange correct order')));
            }
          }else{
             return (json_encode(array('status'=>'error','message'=>'Please fill all input box')));
          }

          }else{
             return (json_encode(array('status'=>'error','message'=>'Please fill all input box')));
          }

        }

    }

    public function getAllQuestions(Request $request)
    {
      $perpage = $request->datatable['pagination']['perpage'];
      $page = $request->datatable['pagination']['page'];
      if($page=='1')
      {
        $offset = 0;
      }
      else
      {
        $offset = ($page-1)*$perpage;
      }
      $round_id= Session::get('quiz_round_id');
      $question_data = [];

      $Questions = DB::table('question_wq')->join('quiz_round_wq', 'quiz_round_wq.id', '=', 'question_wq.quiz_round_id')->select('question_wq.*','quiz_round_wq.status')->where(array('question_wq.quiz_round_id'=>$round_id))->orderby('id','asc')->offset($offset)->limit($perpage)->get();
      $total = DB::table('question_wq')->join('quiz_round_wq', 'quiz_round_wq.id', '=', 'question_wq.quiz_round_id')->select('question_wq.*','quiz_round_wq.status')->where(array('question_wq.quiz_round_id'=>$round_id))->orderby('id','asc')->count();
      $Questions = json_decode(json_encode($Questions));
      $meta = ['draw'=>1,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
      if(!empty($Questions))
      {
        foreach ($Questions as $key => $value) {
          $question_data[] = (array)$value;
          $question_data[$key]['S.no'] = $offset+1;
          $offset++;
        }
        return Response::json(array('data'=> $question_data,'meta'=> $meta));
        //return Response::json($question_data);
      }
      return Response::json(array('data'=> $Questions,'meta'=> $meta));
      //return Response::json($Questions);
    }


     public function delete_question(Request $request)
    {
      if($request->ajax())
      {

        if($request->id){
           
             $question = 'DELETE question_wq.*,answers_wq.* FROM question_wq JOIN answers_wq on question_wq.id = answers_wq.question_id where question_wq.id=?';        
                $delete_publish = \DB::delete($question, array($request->id));
              
         
            return (json_encode(array('status'=>'success','message'=>"Question Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Question Found!!!")));
      }
    }


    public function getquestionDetail(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])

          $round_id = $request->round_id;
          $round_status = QuizRound::where(['id'=>$round_id])->first();
          // if($round_status->status==1){
          //   $result = DB::table('question')->join('publish_quiz_round', 'publish_quiz_round.id', '=', 'question.quiz_round_id')->select('question.*','publish_quiz_round.status')->where(array('question.id'=>$request->id))->first();
          // }else{
            $result = DB::table('question_wq')->join('quiz_round_wq', 'quiz_round_wq.id', '=', 'question_wq.quiz_round_id')->select('question_wq.*','quiz_round_wq.status')->where(array('question_wq.id'=>$request->id))->first();
          // }
          
        } catch(QueryException $ex){ 
          //dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }


        if($request->id)
          {
            $allAnswers = Answer::where(array('question_id'=>$request->id))
            ->get(array(
              'id',
              'question_id',
                'option_text',
                'correct_answer',
                'option_order',
              ));

            $corect_ans = Answer::where(array('question_id'=>$request->id))
            ->get(array(
              'id',
              'question_id',
                'option_text',
                'correct_answer',
                'option_order',
              ))->sortBy('correct_answer',false);

            $res = array();
            $answ = array();


            if(!empty($allAnswers) || !empty($corect_ans))
            {
              foreach($allAnswers as $ans)
              {
                $data = array(
                  'id'  => $ans->id,
                  'question_id'  => $ans->question_id,
                  'option_text'  => $ans->option_text,
                  'correct_answer'  => $ans->correct_answer,
                  'option_order'  => $ans->option_order
                );
                array_push($res,$data);
              }

              foreach($corect_ans as $ans)
              {
                $data = array(
                  'id'  => $ans->id,
                  'question_id'  => $ans->question_id,
                  'option_text'  => $ans->option_text,
                  'correct_answer'  => $ans->correct_answer,
                  'option_order'  => $ans->option_order
                );
                array_push($answ,$data);
              }
             return (json_encode(array('status'=>$status,'message'=>$result,'answers'=>$res,'correctOrder'=>$answ))) ;
            }

          }

                 
      }
    }


     public function publish_question(Request $request)
    {
      if($request->ajax())
      {

      
        if($request->id){
           $question = Question::find($request->id);
           $question->publish = 1;
          
          if ($question->save()){
            return (json_encode(array('status'=>'success','message'=>"Question Published Successfully")));
          }
            
         
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Question Found!!!")));
      }
    }

}
