<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\User;
use Redirect;
use Session;
use Response;
use Image;
use Storage;
use Validator;


class AdminController extends Controller
{

    public function index()
    {
        return view('admin.profile');
    }

    public function update_profile(Request $request)
    {
        if ($request->hasFile('picture'))
	      {
          $validator = Validator::make($request->file(), [
                    'picture' => 'image|mimes:jpeg,png,jpg|max:10480',
                   
                ]);
        
                if ($validator->fails()) {
                    Session::flash('errormessage', 'Image size should be less than 10 MB'); 
        	         return Redirect::to(url('/').'/student/profile');
                }
	
	      }
	      
        $id = $_POST['id'];
        if($request->email==''){

          /** WebQuiz changes for Digital Bank merger **/

          $check=User::where(['mobile'=>$request->mobile])
        ->where(function($query) use ($id){
            if(isset($id)){
              $query->where('id' , '<>' ,$id);
            }
        })->exists();

        /** WebQuiz changes for Digital Bank merger **/

        }else{
          $check=User::where(['email'=>$request->email,'mobile'=>$request->mobile])
        ->where(function($query) use ($id){
            if(isset($id)){
              $query->where('id' , '<>' ,$id);
            }
        })->exists();
        //*****end***********//
        }
        

      if($check){
          Session::flash('errormessage', 'Contact or Email Address already exist.'); 
          return back();
        }else{
          if($id){
        $user = User::find($id);
      }else{
        $user = new User();
      }


    $user->name = $_POST['name'];
    $user->email = $_POST['email'];

    /** WebQuiz changes for Digital Bank merger **/

    $user->mobile =$_POST['mobile'];

    //*****end***********//
    
      if ($request->hasFile('picture'))
      {
      	 $image = $request->file('picture');
	     $path = storage_path('app/profile');
	     $filename = time() . '.' . $image->getClientOriginalExtension();
	     $image->move($path, $filename);

	     $user->picture = 'profile/'.$filename;
       
      }

     
    if($user->save()){
         Session::flash('successmessage', 'Profile Updated Successfully'); 
        return Redirect::to(url('/').'/administration/profile');
        } else{
         Session::flash('errormessage', 'Failed to update'); 
         return Redirect::to(url('/').'/administration/profile');

      }
          
        }
    }


    public function update_password(Request $request)
    {
        $id = $_POST['id'];
        if($id){
	        $user = User::find($id);
	        $user->password =  Hash::make($_POST['new_password']);
	     
	        if($user->save()){
	         Session::flash('successmessage', 'Password Updated Successfully'); 
	        return Redirect::to(url('/').'/administration/profile');
	        } else{
	         Session::flash('errormessage', 'Failed to update'); 
	         return Redirect::to(url('/').'/administration/profile');
	       }
      }    
      
    }
}
