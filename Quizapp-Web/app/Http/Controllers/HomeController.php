<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\AssignRound;
use App\School;
use App\Quiz;
use App\QuizRound;
use App\PublishQuizRound;
use App\Question;
use App\PublishQuestions;
use App\PublishAnswers;
use Redirect;
use Session;
use Response;
use Validator;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      /** WebQuiz changes for Digital Bank merger **/
      
        if(Auth::user()->type == 1){
          $count_student = User::where(['type'=>2])->count();
          $count_quiz = Quiz::count();
          $count_quiz_result = AssignRound::where(['attended'=>2])->count();
          $count_school = School::count();
          return view('home',compact('count_student','count_quiz','count_quiz_result','count_school'));
        }else{

           $count_assigned_quiz = DB::table('assign_round_wq')->join('publish_quiz_round_wq', 'assign_round_wq.round_id', '=', 'publish_quiz_round_wq.id')->join('quiz_wq', 'publish_quiz_round_wq.quiz_id', '=', 'quiz_wq.id')->select('quiz_wq.*')->where(array('assign_round_wq.student_id'=>Auth::user()->id,'assign_round_wq.attended'=>0,'assign_round_wq.is_result_declared'=>1))->groupBy('publish_quiz_round_wq.quiz_id')->count(); 

            //date_default_timezone_set('Asia/Kuala_Lumpur');
            $currentdate = date('Y-m-d', time());

           $assignrecentquiz = DB::table('assign_round_wq')->where(array('student_id'=>Auth::user()->id,'attended'=>0,'is_result_declared'=>1,['expiration_date',">=","$currentdate"]))->groupBy('quiz_name')->get();

           



            if(!$assignrecentquiz){
              $assignrecentquiz ='';
            }


            /*---refered quiz------*/
            $referedrecentquiz = DB::table('refer_quiz_wq')->join('publish_quiz_round_wq', 'refer_quiz_wq.round_id', '=', 'publish_quiz_round_wq.id')->join('quiz_wq', 'publish_quiz_round_wq.quiz_id', '=', 'quiz_wq.id')->select('quiz_wq.*')->where(array('refer_quiz_wq.student_id'=>Auth::user()->id,'refer_quiz_wq.attended'=>0))->groupBy('publish_quiz_round_wq.quiz_id')->get(); 
            if(!$referedrecentquiz){
              $referedrecentquiz ='';
            }



           
            $recent_quiz_score_count =  DB::table('assign_round_wq')->where(array('student_id'=>Auth::user()->id))->whereIn('attended', [2])->orderBy('id','desc')->take(2)->count(); 
           if($recent_quiz_score_count>0){

              $recent_quiz_score =  DB::table('assign_round_wq')->where(array('student_id'=>Auth::user()->id))->whereIn('attended', [2])->orderBy('id','desc')->take(2)->get(); 
           }else{
              $recent_quiz_score = '';
            
           }


          return view('student_home',compact('assignrecentquiz','count_assigned_quiz','recent_quiz_score','recent_quiz_score_count','referedrecentquiz','count_refered_quiz'));
        }
    }

    public function load_result(Request $request){
       if($request->ajax())
      {
        $status = "success";
        if($request->id){
           $id = $request->id;
           $result_count =  DB::table('assign_round_wq')->where(array('student_id'=>Auth::user()->id))->whereIn('is_result_declared',[1,2])->whereIn('attended', [2])->orderBy('id',true)
           ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<' ,$id);
                    }
                })->count();
   
            $totalRowCount = $result_count;
            $showLimit = 4;

            $results = DB::table('assign_round_wq')->where(array('student_id'=>Auth::user()->id))->whereIn('is_result_declared',[1,2])->where('attended', [2])
            ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<' ,$id);
                    }
                })->orderBy('id',true)->take($showLimit)->get();

           

            $res_array = array();
             if(!empty($results))
            {
              foreach($results as $result)
              {
               
                $data = array(
                  'id'  => $result->id,
                  'round_id'  => $result->round_id,
                  'quiz_duration'  => $result->quiz_duration,
                  'score'  => $result->score,
                  'total_question'  =>  $result->total_question,
                  'quiz_name'  =>  $result->quiz_name,
                  'round_name'  =>  $result->round_name,
                  'earn_points'  =>  $result->earn_points,
                  'attended'  =>  $result->attended,
                  'expiration_date'  =>date("F jS Y", strtotime(date('Y-m-d H:i:s',strtotime($result->expiration_date . "+1 days")))),
                  'is_result_declared'  => $result->is_result_declared,
                );
                array_push($res_array,$data);
              }
             return (json_encode(array('status'=>$status,'quiz_res'=>$res_array,'totalRowCount'=>$totalRowCount))) ;
            }
           

        }
      }
    }
    
    public function test(){
        return view('test');
    }
}
