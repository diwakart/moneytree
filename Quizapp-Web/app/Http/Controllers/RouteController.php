<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Auth;
use App\User;
use App\AssignRound;
use App\School;
use App\QuizRound;
use App\PublishQuizRound;
use App\Quiz;
use App\Question;
use App\QuizRoundResult;
use App\PublishQuestions;
use App\PublishAnswers;
use App\Answer;
use App\StudentRank;
use Redirect;
use Session;
use Response;
use Validator;
use DB;
use \Crypt;
use Twilio\Rest\Client;
use TextToNumber; 

class RouteController extends Controller
{
    public function index(Request $request){
        if(request()->route('digital_key')!=''){
            $data=User::select('id')->where('digital_key',request()->route('digital_key'))->first();
            if(!empty($data))
            {
                Auth::loginUsingId($data->id);
            }
            else
            {
                Session::flash('errormessage', 'Digital Key Expired');
                return view('auth.login');
            }
            
        }

        if (Auth::check()) {
          if(Auth::user()->type == 1) {
            //dd(Auth::user());
                return redirect()->intended('/administration/home');
            } elseif (Auth::user()->type == 2) {
                return redirect()->intended('/student/home');
            }
          }else{
            return view('auth.login');
          }
    	
    }
    
    
    public function view_login(){
         $quiz_url = url()->current();

      if (strpos($quiz_url,'/refer-quiz/') !== false) {
       $store_quiz_url = Session::put('quiz_url', $quiz_url);
      }
        if (Auth::check()) {
          if(Auth::user()->type == 1) {
                return redirect()->intended('/administration/home');
            } elseif (Auth::user()->type == 2) {
                return redirect()->intended('/student/home');
            }
          }else{
          return view('auth.login');
          }
         
    }

    public function participated_student_result(){

      $args = func_get_args();
      $enc_parameter= $args[0];
      $parameter_id =  Crypt::decrypt($enc_parameter);
      $explode_parameter = explode('^',$parameter_id);
      $round_id = $explode_parameter[0];
      $currentuserid = $explode_parameter[1];
      
   

      $school = User::where('id',$currentuserid)->first();
     
      $result = DB::table('assign_round_wq')->join('publish_quiz_round_wq', 'assign_round_wq.round_id', '=', 'publish_quiz_round_wq.id')->select('assign_round_wq.*','publish_quiz_round_wq.round_name as roundname')->where(array('assign_round_wq.student_id'=>$currentuserid,'assign_round_wq.round_id'=>$round_id,'school_id'=>$school->school_id))->whereIn('attended', [2])->first();
      
      
      if($result!=''){
         return view('student.student_result_public',compact('result','school'));
      }else{
        return abort(404);
      }

      
    }

    public function forget_password(){
      return view('auth.forget_password');
    }

    public function send_forget_password(Request $request){
      if($request->ajax())
      {

         $sid = 'ACbfcf0a5d3a4790cc95d0ad12b2e7dd19';
        $token = '5930ab90756eade8563f0acc74ba5f69';
        $client = new Client($sid, $token);

        /** WebQuiz changes for Digital Bank merger **/

        $rules = array(
          'mobile'     => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        $assign_round;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";

          /** WebQuiz changes for Digital Bank merger **/

          if(isset($failedRules['mobile']))
            $errorMsg = $failedRules['mobile'][0] . "\n";
        
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{

         /** WebQuiz changes for Digital Bank merger **/

           $mobile=$request->mobile;

          /** WebQuiz changes for Digital Bank merger **/

           $user_mobile = User::where(['mobile'=>$mobile])->exists();
           if($user_mobile){
                $get_user_data = User::where(['mobile'=>$mobile])->first();
                $password = substr($get_user_data->mobile, -4);
                $enc_passsword = Hash::make($password);  
                $id = $get_user_data->id;

                $user =  User::find($id);
                $user->password = $enc_passsword;
                if($user->save()){

                    $client->messages->create(
                        // the number you'd like to send the message to

                      /** WebQuiz changes for Digital Bank merger **/

                       $get_user_data->country_code.$get_user_data->mobile,
                      array(
                          // A Twilio phone number you purchased at twilio.com/console
                          'from' => '+15416159055',
                          // the body of the text message you'd like to send
                          'body' => 'Hey '.$user->name.'! Here is the new password '.$password,
                      )
                      );
                    return (json_encode(array('status'=>'success','message'=>'Password Updated Successfully and Sent Via SMS')));
                }else{
                  return (json_encode(array('status'=>'error','message'=>'User not found')));
                }


           }else{
             return (json_encode(array('status'=>'error','message'=>'mobile Number not found')));
           }
               
            

              
        }     
          
      }
    }


   
   public function register_view(){

    /** WebQuiz changes for Digital Bank merger **/

      if(request()->route('name_mobile')){

        /** WebQuiz changes for Digital Bank merger **/

        $parameter = request()->route('name_mobile');
        $enc_paramater = $parameter;
        if(isset($enc_paramater) && $enc_paramater!=''){

          /** WebQuiz changes for Digital Bank merger **/

          $name_mobile =base64_decode($enc_paramater);

          /** WebQuiz changes for Digital Bank merger **/

          $tesct =  explode("^",$name_mobile);
          if(count($tesct)==3){
          $name = ucwords(TextToNumber::text_to_number_decode($tesct[0]));

          /** WebQuiz changes for Digital Bank merger **/

          $mobile =TextToNumber::number_to_text_decode($tesct[1]);
          $country_code = '+'.TextToNumber::number_to_text_decode($tesct[2]);
        }else{
          return redirect()->route('register');
        }
        }else{

          $name ='';

          /** WebQuiz changes for Digital Bank merger **/

          $mobile ='';
          $country_code = '';
        }
      }

      /** WebQuiz changes for Digital Bank merger **/

      return view('auth.register',compact('name','mobile','country_code'));
    }
}
