<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Quiz;
use App\QuizRound;
use App\PublishQuizRound;
use App\User;
use App\Question;
use App\QuizRoundResult;
use App\AssignRound;
use App\Answer;
use Redirect;
use Session;
use Response;
use Validator;
use DB;

class QuizController extends Controller
{

  
    public function index(){
     
    	return view('admin.quiz');
    }

    public function view_round_page(){

      $args = func_get_args();
      $quiz_id= $args[0];
      Session::put('quiz_id', $quiz_id);
      $quiz = Quiz::where(array('id'=>$quiz_id))->select('id', 'title')
      ->first();
    	return view('admin.add_round',compact('quiz'));
    }

    public function store(Request $request){
    	if($request->ajax())
      {
       
        $rules = array(
          'title'     => 'required',
          'user_id'     => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        $quiz;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['title']))
            $errorMsg = $failedRules['title'][0] . "\n";
          if(isset($failedRules['user_id']))
            $errorMsg = $failedRules['user_id'][0] . "\n";
          
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        } 
        else{
              $check=Quiz::where(['title'=>$request->title])->exists();

              if($check){
                  return (json_encode(array('status'=>'error','message'=>"Quiz Title Already Exist." )));
                }

                $quiz = new Quiz();
             }

        $quiz->title = $request->title;
        $quiz->user_id = $request->user_id;
        if($quiz->save()){
        	return (json_encode(array('status'=>'success','quiz_id' => $quiz->id,'quiz_title' => $quiz->title,'message'=>sprintf('Quiz "%s" successfully saved', $quiz->title))));
          } else{
          	 return (json_encode(array('status'=>'error','message'=>'Failed to Add Quiz'))) ;
          }
        }
    }


    public function deactivate_quiz(Request $request){
      if($request->ajax())
      {
       
        $rules = array(
          'id'     => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        $quiz;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['id']))
            $errorMsg = $failedRules['id'][0] . "\n";
          
          
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        } 
        else{
                if($request->id){
                  $quiz = Quiz::find($request->id);
               }else{
                  $quiz = new Quiz();
                }
             }
        $quiz->status = 1;

        if($quiz->save()){
          return (json_encode(array('status'=>'success','message'=>'Quiz deactivated successfully')));
          } else{
             return (json_encode(array('status'=>'error','message'=>'Failed to Deactivat Quiz'))) ;
          }
        }
    }


     public function activate_quiz(Request $request){
      if($request->ajax())
      {
       
        $rules = array(
          'id'     => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        $quiz;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['id']))
            $errorMsg = $failedRules['id'][0] . "\n";
          
          
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        } 
        else{
                if($request->id){
                  $quiz = Quiz::find($request->id);
               }else{
                  $quiz = new Quiz();
                }
             }
        $quiz->status = 0;

        if($quiz->save()){
          return (json_encode(array('status'=>'success','message'=>'Quiz Activated successfully')));
          } else{
             return (json_encode(array('status'=>'error','message'=>'Failed to Activate Quiz'))) ;
          }
        }
    }


    public function delete(Request $request)
    {
      if($request->ajax())
      {
        if($request->id){
         
            $quiz = Quiz::find($request->id);
          if ($quiz->delete())
            return (json_encode(array('status'=>'success','message'=>"Quiz Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }
    }

    public function getAllQuiz(Request $request)
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        if($page=='1')
        {
          $offset = 0;
        }
        else
        {
          $offset = ($page-1)*$perpage;
        }

        $Quiz = \DB::table('quiz_wq')->select('id','user_id','title','status')->orderby('id','asc')->offset($offset)->limit($perpage)->get();
        $total = \DB::table('quiz_wq')->select('id','user_id','title','status')->count();
        $meta = ['draw'=>1,'perpage'=>$perpage,'total'=>$total,'page'=>$page];

        //$Quiz = Quiz::get(array('id','user_id','title','status'));
        $quiz_data = [];
        $Quiz = json_decode(json_encode($Quiz));
        if(!empty($Quiz))
        {
          $i = 1;
          foreach ($Quiz as $key => $value) {
          $quiz_data[] = (array)$value;
          $quiz_data[$key]['S.no'] = $offset+1;
          $offset++;
        }
        return Response::json(array('data'=> $quiz_data,'meta'=> $meta));
        //return Response::json($quiz_data);
        }
        return Response::json(array('data'=> $Quiz,'meta'=> $meta));
        //return Response::json($Quiz);
      }

    public function editquiz(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = Quiz::find($request->id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }


    public function update_quiz(Request $request)
    {
      if($request->ajax())
      {
        $user;
        $rules = array(
          'title'     => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['title']))
            $errorMsg = $failedRules['title'][0] . "\n";
          
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        } else if($request->id){
          $quiz = Quiz::find($request->id);
        }
        else{
          $quiz = new Quiz();
          
        }

        $quiz->title = $request->title;
        if($quiz->save()){
          return (json_encode(array('status'=>'success','message'=>'Quiz Title updated successfully')));
          } else{
             return (json_encode(array('status'=>'error','message'=>'Failed to update Quiz Title'))) ;
          }
      }
    }



  
}
