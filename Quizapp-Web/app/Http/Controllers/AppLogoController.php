<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\AppLogo;
use Redirect;
use Session;
use Response;
use Image;
use Storage;

class AppLogoController extends Controller
{
    public function index(){
    	return view('admin.app_logo');
    }

    public function update(Request $request){

    	$id = $request->id;
        
	      if($id){
	        $applogo = AppLogo::find($id);
	      }else{
	        $applogo = new AppLogo();
	      }

	     
	      if ($request->hasFile('logo'))
	      {
		      	$image = $request->file('logo');
			    $path = storage_path('app/logo');

			    $filename = time() . '.' . $image->getClientOriginalExtension();
			    $image->move($path, $filename);
			    $applogo->logo = 'logo/'.$filename;

		        if($applogo->save()){
		         Session::flash('successmessage', 'Logo Updated Successfully'); 
		        return Redirect::to(url('/').'/administration/app-logo');
		        } else{
		         Session::flash('errormessage', 'Failed to update'); 
		         return Redirect::to(url('/').'/administration/app-logo');
		        }
	       
	      }
	      
	}
}
