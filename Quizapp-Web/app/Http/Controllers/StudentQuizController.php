<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\AssignRound;
use App\School;
use App\QuizRound;
use App\PublishQuizRound;
use App\Quiz;
use App\Question;
use App\QuizRoundResult;
use App\PublishQuestions;
use App\PublishAnswers;
use App\Answer;
use App\StudentRank;
use App\ReferQuiz;
use Redirect;
use Session;
use Response;
use Validator;
use DB;
use \Crypt;


class StudentQuizController extends Controller
{
   public function index(){
      $args = func_get_args();
      $exploded_para= Crypt::decrypt($args[0]);
      $exploded_parameter = explode('^',$exploded_para);
      $assign_quiz_id = $exploded_parameter[2];
     
      $assign_round = AssignRound::where(array('id'=>$assign_quiz_id,'attended'=>1))->first();
      $assign_count = AssignRound::where(array('id'=>$assign_quiz_id,'attended'=>1))->count();

      if($assign_count>0){

       /*---Question List------*/
       $questions = DB::table('publish_questions_wq')->where(array('quiz_round_id'=>$assign_round->round_id,'published'=>1))->get();
      $totalquestions = DB::table('publish_questions_wq')->where(array('quiz_round_id'=>$assign_round->round_id,'published'=>1))->count();
      /*-----Quiz Round Detail-----*/
      $quiz_round = PublishQuizRound::where(array('id'=>$assign_round->round_id))->first();
      /*----School Detail------*/
      
      $school = School::where(array('id'=>$assign_round->school_id))->first();
      /*----Quiz Detail------*/
      $quiz = Quiz::where(array('id'=>$quiz_round->quiz_id))->first();

     
     
    
      return view('student.quiz',compact('questions','assign_round','quiz_round','school','quiz','totalquestions'));
     }else{
       return abort(404);
     }
    
     
   }


   public function quiz_attended(Request $request){
     if($request->ajax())
      {


        $rules = array(
          'id'     => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        $assign_round;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['id']))
            $errorMsg = $failedRules['id'][0] . "\n";
        
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{

           $id=$request->id;

           $attend_quiz = AssignRound::find($id);
                $attend_quiz->attended = 1;
                $attend_quiz->quiz_started_at = NOW();
                $attend_quiz->save();

                /** WebQuiz changes for Digital Bank merger **/
                
                $parameter_enc = Crypt::encrypt(Auth::user()->mobile.'^'.Auth::user()->id.'^'.$attend_quiz->id); 

              
        }
           
            return (json_encode(array('status'=>'success','message'=>'Quiz Round Attended by Student','parameter_enc'=>$parameter_enc)));
                   
          
      }
   }



   public function submit_quiz(Request $request){


    if($request->ajax())
      {
         $quiz_result;
        
      
         if($request->question_type==1){
            $answers = $request->answers;
            $answers = array_map(function($v){
                return trim($v);},$answers);
            $answ = array();
            $real_answers = PublishAnswers::where('question_id',$request->question_id)->get();
            foreach ($real_answers as $ans) {
              array_push($answ,trim($ans->correct_answer)); 
            }

            if(array_map('strtolower', $answ) == array_map('strtolower', $answers)){
              $check_ans = 1;
            }else{
              $check_ans = 0;
            }
            $given_answer = implode(":::", $request->answers);
            $result = New QuizRoundResult;
            $result->question_id = $request->question_id;
            $result->question_type = $request->question_type;
            $result->round_id = $request->quiz_round_id;
            $result->student_id = $request->auth_id;
            $result->check_answer = $check_ans;
            $result->school_id = $request->school_id;
            $result->answered = $given_answer;

             $check=QuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'school_id'=>$request->school_id])->exists();
            if(!$check){
              $result->save();
            }else{
              QuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'school_id'=>$request->school_id])->delete();
              $result->save();
            }

            

         }

         if($request->question_type==2){

          $answers = $request->answers;
            $answ = array();
           
            $val=$request->question_id;

           $real_answers = PublishAnswers::where(array('correct_answer'=>1,'question_id'=>$val))->get();

        

            foreach ($real_answers as $ans) {

              array_push($answ,$ans->option_text); 
            }
            if(count($answers)>0){
            if(array_map('strtolower', $answ) == array_map('strtolower', $answers)){
              $check_ans = 1;
            }else{
              $check_ans = 0;
            }
            }else{
              $check_ans = 0;
            }
            $given_answer = '';
            $result = New QuizRoundResult;
            $result->question_id = $request->question_id;
            $result->question_type = $request->question_type;
            $result->round_id = $request->quiz_round_id;
            $result->student_id = $request->auth_id;
            $result->check_answer = $check_ans;
            $result->school_id = $request->school_id;
            $result->answered = $given_answer;

            $check=QuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'school_id'=>$request->school_id])->exists();
            if(!$check){
              $result->save();
            }else{
              QuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'school_id'=>$request->school_id])->delete();
              $result->save();
            }

            
         }


         if($request->question_type==3){

          $answers = $request->answers;
            $answ = array();
           
            $val=$request->question_id;

           $real_answers = PublishAnswers::where(array('question_id'=>$val))->get();

            foreach ($real_answers as $ans) {

              array_push($answ,$ans->correct_answer); 
            }
           
            if(array_map('strtolower', $answ) == array_map('strtolower', $answers)){
              $check_ans = 1;
            }else{
              $check_ans = 0;
            }
             $given_answer = implode(":::", $request->answers);
            $result = New QuizRoundResult;
            $result->question_id = $request->question_id;
            $result->question_type = $request->question_type;
            $result->round_id = $request->quiz_round_id;
            $result->student_id = $request->auth_id;
            $result->check_answer = $check_ans;
            $result->school_id = $request->school_id;
            $result->answered = $given_answer;
              $check=QuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'school_id'=>$request->school_id])->exists();
            if(!$check){
              $result->save();
            }else{
              QuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'school_id'=>$request->school_id])->delete();
              $result->save();
            }

         }

          return (json_encode(array('status'=>'success','message'=>'Quiz Round Attended by Student')));
      }


   }


  

   public function calculate_score(Request $request){
      if($request->ajax())
      {
         $quiz_score;
        
            $quiz_duration = $request->quiz_duration;
            $quiz_round_id = $request->quiz_round_id;
            $auth_id = $request->auth_id;
           

            $score = QuizRoundResult::where(array('student_id'=>$auth_id,'round_id'=>$quiz_round_id,'check_answer'=>1,'school_id'=>$request->school_id))->count();

           

            $total_que = PublishQuestions::where(array('quiz_round_id'=>$quiz_round_id,'published'=>1))->count();


         

           $result  = DB::statement("UPDATE dg_assign_round_wq SET score='".$score."',total_question='".$total_que."',quiz_name='".$request->quiz_name."',round_name='".$request->quiz_round_name."', attended=2,quiz_duration=IF('".$quiz_duration."'='00:00:00.00',TIME_FORMAT(TIMEDIFF(NOW(),quiz_started_at), '%T'),'".$quiz_duration."') WHERE student_id=$auth_id AND round_id=$quiz_round_id AND school_id=$request->school_id");

            if($result){
             return (json_encode(array('status'=>'success','message'=>'Quiz Round Attended by Student')));
            }

          
      }


   }

    public function show_result(){

      $args = func_get_args();
      $round_id= $args[0];
      $roundid= $round_id;
      $currentuserid = Auth::user()->id;

      $school = User::where('id',$currentuserid)->first();
     
      $result = DB::table('assign_round_wq')->join('publish_quiz_round_wq', 'assign_round_wq.round_id', '=', 'publish_quiz_round_wq.id')->select('assign_round_wq.*','publish_quiz_round_wq.round_name')->where(array('assign_round_wq.student_id'=>$currentuserid,'assign_round_wq.round_id'=>$round_id,'school_id'=>$school->school_id))->first();


       return view('student.result',compact('result','roundid'));
   }




    public function submit_quiz_reload(Request $request){

   
    if($request->ajax())
      {

     /* $delete_result = QuizRoundResult::where(['round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'school_id'=>$request->school_id]);
       $delete_result->delete();*/

         $quiz_result;
         if($request->question_type==1){
            $answers = $request->answers;
            $answers = array_map(function($v){
                return trim($v);},$answers);
            $answ = array();
            $real_answers = PublishAnswers::where('question_id',$request->question_id)->get();
            foreach ($real_answers as $ans) {
              array_push($answ,trim($ans->correct_answer)); 
            }

            if(array_map('strtolower', $answ) == array_map('strtolower', $answers)){
              $check_ans = 1;
            }else{
              $check_ans = 0;
            }
          $result = New QuizRoundResult;
            $result->question_id = $request->question_id;
            $result->question_type = $request->question_type;
            $result->round_id = $request->quiz_round_id;
            $result->student_id = $request->auth_id;
            $result->check_answer = $check_ans;
            $result->school_id = $request->school_id;

            $check=QuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'school_id'=>$request->school_id])->exists();
            if(!$check){
              $result->save();
            }

         }

         if($request->question_type==2){

          $answers = $request->answers;
            $answ = array();
           
            $val=$request->question_id;

           $real_answers = PublishAnswers::where(array('correct_answer'=>1,'question_id'=>$val))->get();

        

            foreach ($real_answers as $ans) {

              array_push($answ,$ans->option_text); 
            }
           
            if(count($answers)>0){
            if(array_map('strtolower', $answ) == array_map('strtolower', $answers)){
              $check_ans = 1;
            }else{
              $check_ans = 0;
            }
            }else{
              $check_ans = 0;
            }
          $result = New QuizRoundResult;
            $result->question_id = $request->question_id;
            $result->question_type = $request->question_type;
            $result->round_id = $request->quiz_round_id;
            $result->student_id = $request->auth_id;
            $result->check_answer = $check_ans;
            $result->school_id = $request->school_id;

            $check=QuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'school_id'=>$request->school_id])->exists();
            if(!$check){

              $result->save();
            }
          
         }


         if($request->question_type==3){

          $answers = $request->answers;
            $answ = array();
           
            $val=$request->question_id;

           $real_answers = PublishAnswers::where(array('question_id'=>$val))->get();

            foreach ($real_answers as $ans) {

              array_push($answ,$ans->correct_answer); 
            }
           
            if(array_map('strtolower', $answ) == array_map('strtolower', $answers)){
              $check_ans = 1;
            }else{
              $check_ans = 0;
            }
          $result = New QuizRoundResult;
            $result->question_id = $request->question_id;
            $result->question_type = $request->question_type;
            $result->round_id = $request->quiz_round_id;
            $result->student_id = $request->auth_id;
            $result->check_answer = $check_ans;
            $result->school_id = $request->school_id;

            $check=QuizRoundResult::where(['question_id'=>$request->question_id,'question_type'=>$request->question_type,'round_id'=>$request->quiz_round_id,'student_id'=>$request->auth_id,'school_id'=>$request->school_id])->exists();
            if(!$check){
              $result->save();
            }
         }

          return (json_encode(array('status'=>'success','message'=>'Quiz Round Attended by Student')));
      }


   }


  

   public function calculate_score_reload(Request $request){
      if($request->ajax())
      {

        
         $quiz_score;
        
            $quiz_duration = $request->quiz_duration;
            $quiz_round_id = $request->quiz_round_id;
            $auth_id = $request->auth_id;

            $score = QuizRoundResult::where(array('student_id'=>$auth_id,'round_id'=>$quiz_round_id,'check_answer'=>1,'school_id'=>$request->school_id))->count();

           

            $total_que = PublishQuestions::where(array('quiz_round_id'=>$quiz_round_id,'published'=>1))->count();


          $result  = DB::statement("UPDATE dg_assign_round_wq SET score='".$score."',total_question='".$total_que."',quiz_name='".$request->quiz_name."',round_name='".$request->quiz_round_name."', attended=2,quiz_duration=IF('".$quiz_duration."'='00:00:00.00',TIME_FORMAT(TIMEDIFF(NOW(),quiz_started_at), '%T'),'".$quiz_duration."') WHERE student_id=$auth_id AND round_id=$quiz_round_id AND school_id=$request->school_id");

            if($result){
             return (json_encode(array('status'=>'success','message'=>'Quiz Round Attended by Student')));
            }

          
      }


   }



   public function view_detail_quiz_result(Request $request)
    {
      if($request->ajax())
      {

        $status = "success";
        if($request->result_id)
          {
              $recent_quiz_score =  DB::table('assign_round_wq')->where(array('id'=>$request->result_id))->orderBy('updated_at',false)->first();
              $recent_quiz_school = DB::table('school_wq')->where('id',$recent_quiz_score->school_id)->first();
             return (json_encode(array('status'=>$status,'recent_quiz_score'=>$recent_quiz_score,'recent_quiz_school'=>$recent_quiz_school))) ;
           

          }

                 
      }
    }



    public function getallquizs(){
      
           $currentdate = date('Y-m-d', time());
        
           $Assigned_Quiz_Count = DB::table('users')->join('assign_round_wq', 'assign_round_wq.student_id', '=', 'users.id')->select('assign_round_wq.*')->where(array('assign_round_wq.student_id'=>Auth::user()->id,'assign_round_wq.attended'=>0,'assign_round_wq.is_result_declared'=>1,['expiration_date',">=","$currentdate"]))->orderBy('assign_round_wq.updated_at',false)->count(); 

         
            if($Assigned_Quiz_Count>0){
              $Assigned_Quizes = DB::table('users')->join('assign_round_wq', 'assign_round_wq.student_id', '=', 'users.id')->select('assign_round_wq.*')->where(array('assign_round_wq.student_id'=>Auth::user()->id,'assign_round_wq.attended'=>0,'assign_round_wq.is_result_declared'=>1,['expiration_date',">=","$currentdate"]))->orderBy('assign_round_wq.updated_at',false)->get();
            }else{
               $Assigned_Quizes=''; 
            }


              $recent_quiz_score_count =  DB::table('assign_round_wq')->where(array('student_id'=>Auth::user()->id))->whereIn('attended', [2])->orderBy('updated_at',false)->take(2)->count(); 
           if($recent_quiz_score_count>0){

              $recent_quiz_score =  DB::table('assign_round_wq')->where(array('student_id'=>Auth::user()->id))->whereIn('attended', [2])->orderBy('updated_at',false)->take(2)->get(); 
           }else{
              $recent_quiz_score = '';
            
           }


          


          

      return view('student.all_quiz',compact('Assigned_Quizes','Assigned_Quiz_Count','recent_quiz_score','recent_quiz_score_count'));
    }

    public function quiz_result(){

      $results = DB::table('assign_round_wq')->where(['student_id'=>Auth::user()->id])->whereIn('attended', [2])->get();
      $referefresults = DB::table('refer_quiz_wq')->where(['student_id'=>Auth::user()->id])->whereIn('attended', [2])->get();
      return view('student.quiz_results',compact('results','referefresults'));
    }

     public function selected_quiz_result(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = AssignRound::where(['school_id'=>$request->school_id,'round_id'=>$request->round_id,'student_id'=>$request->student_id])->first();
          $school = School::where(['id'=>$request->school_id])->first();
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result,'school'=>$school ))) ;         
      }
    }

    public function selected_refered_quiz_result(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = ReferQuiz::where(['refered_by'=>$request->refered_by_id,'round_id'=>$request->round_id,'student_id'=>$request->student_id])->first();
          $user = User::where(['id'=>$request->refered_by_id])->first();
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result,'user'=>$user ))) ;         
      }
    }

  

  public function get_rounds(){

   
    $args = func_get_args();
     $quiz_id= $args[0];



     $quiz_name = Quiz::find($quiz_id); 

      //date_default_timezone_set('Asia/Kuala_Lumpur');
      $currentdate = date('Y-m-d', time());

    $count_assigned_rounds = DB::table('assign_round_wq')->join('publish_quiz_round_wq', 'assign_round_wq.round_id', '=', 'publish_quiz_round_wq.id')->join('quiz_wq', 'publish_quiz_round_wq.quiz_id', '=', 'quiz_wq.id')->select('publish_quiz_round_wq.round_name','publish_quiz_round_wq.id as quizroundid','assign_round_wq.*')->where(array('assign_round_wq.student_id'=>Auth::user()->id,'assign_round_wq.attended'=>0,'publish_quiz_round_wq.quiz_id'=>$quiz_id,'assign_round_wq.is_result_declared'=>1,['expiration_date',">=","$currentdate"]))->groupBy('publish_quiz_round_wq.id')->count(); 

    if($count_assigned_rounds>0){
      $assigned_rounds = DB::table('assign_round_wq')->join('publish_quiz_round_wq', 'assign_round_wq.round_id', '=', 'publish_quiz_round_wq.id')->join('quiz_wq', 'publish_quiz_round_wq.quiz_id', '=', 'quiz_wq.id')->select('publish_quiz_round_wq.round_name as roundname','publish_quiz_round_wq.id as quizroundid','assign_round_wq.*')->where(array('assign_round_wq.student_id'=>Auth::user()->id,'assign_round_wq.attended'=>0,'publish_quiz_round_wq.quiz_id'=>$quiz_id,'assign_round_wq.is_result_declared'=>1,['expiration_date',">=","$currentdate"]))->groupBy('publish_quiz_round_wq.id')->get(); 
    }else{
      $assigned_rounds='';
    }

     $countries = array('+60'=>'Malaysia (+60)','+91'=>'India (+91)','+44'=>'United Kingdom (+44)','+65'=>'Singapore (+65)','+1'=>'United State (+1)');

     if($assigned_rounds!=''){
      return view('student.quiz_rounds',compact('assigned_rounds','quiz_name','countries'));
     }else{
       return abort(404); 
     }
    
   
    
  }
   
}
