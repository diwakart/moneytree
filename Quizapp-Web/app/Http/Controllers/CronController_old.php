<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Response;
use DB;
use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
//use Twilio\RestException;
use App\User;
use App\School;
use Excel;
use Session;
use Image;
use Log;
use App\AssignRound;
use App\QuizRoundResult;
use App\PublishQuizRound;
use App\RegisteringStudent;
use \Crypt;
use TextToNumber; 
use Validator;
use App\QuizRound;
use App\Quiz;
use App\Question;
use App\Answer;
use App\PublishQuestions;







class CronController extends Controller
{
    public function index(){
        
        $this->user_csv();
        $sid = 'ACbfcf0a5d3a4790cc95d0ad12b2e7dd19';
        $token = '5930ab90756eade8563f0acc74ba5f69';
        $client = new Client($sid, $token);

        $data = DB::table('sms_queue')
                        ->orderBy('priority', 'desc')
                        ->orderBy('timestamp', 'desc')
                        ->limit(15)
                        ->get()
                        ->toArray();
        foreach ($data as $key => $value) {
            try {
                // $client->messages->create(
                //     // the number you'd like to send the message to
                //     $value->country_code.$value->sms_to,
                //     array(
                //         // A Twilio phone number you purchased at twilio.com/console
                //         'from' => '+15416159055',
                //         // the body of the text message you'd like to send
                //         'body' => htmlspecialchars_decode(str_replace("{username}",$value->sms_to_name,$value->body_message)),
                //         // 'mediaUrl' => 'http://big5kayakchallenge.com/wp-content/uploads/2018/01/inspirational-wallpaper-for-mobile-samsung-hii-wallpapers-to-your-cell-phone-hello-hi-wallpaper-for-mobile-samsung.jpg'
                //     )
                // );
                DB::table('sms_queue')
                    ->where('id', $value->id)
                    ->update(['status' => '2','remark'=>"Skiped for the moment from sending it."]);
            } catch ( RestException $e ) {
                DB::table('sms_queue')
                    ->where('id', $value->id)
                    ->update(['status' => '2','remark'=>$e->getMessage()]);
            } finally {
                $delete = DB::table('sms_queue')->delete($value->id);
            }
            }  
    }
    
    
    public function user_csv(){
      $School = School::all();
      $school_names = array();
      foreach($School as $value) {
          $school_names[] = $value->name;
      }
      $country_code_array = array('60','91', '44','65','1');
      
      $remaining_students = array();
      $wrong_duration_students_name = array();
      $ERROR ='';
      
      $file_name = DB::table('import_csv')->first();
       if(count($file_name)>0){
      $path = public_path().'/csv/'.$file_name->file_name;
      $data = Excel::load($path, function($reader) {
      })->get()->toArray();
      
      if($file_name->file_type=='registered_student'){
      /**************Start: Registered student csv*******************/
      
     
    
    
      $output = array_slice($data, ($file_name->start_row) , 100);
      
      if(empty($output)){
          
         DB::table('import_csv')
                    ->where('id', $file_name->id)
                    ->delete();
         
      }
      
      /* echo $file_name->start_row.'- '.$file_name->end_row.'<br>';
     echo "<pre>";print_r($output);
     */
      Log::info('Output for CSV');
      Log::info(json_encode($output));
      if($file_name->status==2){
          foreach ($output as $key => $value) {
            \Log::error('Under the loop');
            \Log::info(json_encode($value));
       
          if (!in_array($value['country_code'], $country_code_array))
          {
            $remaining_students[] = ucwords($value['name']);
          }else{
            $country_code_value = '+'.$value['country_code'];
          }

          /** WebQuiz changes for Digital Bank merger **/

          if($value['email']==''){
            $check=User::where(['mobile'=>$value['mobile']])->exists();
          }else{
             $check=User::where(['email'=>$value['email'],'mobile'=>$value['mobile']])->exists();
          }
          /** WebQuiz changes for Digital Bank merger **/
          
           if($value['name']=='' || $value['mobile']=='' || $value['password']=='' || $value['school']==''|| $value['country_code']=='' ||  $check){

              $remaining_students[] = ucwords($value['name']);

           }else{

             if (!in_array($value['school'], $school_names))
            {
                $school_name = School::where(['name'=>'Other'])->first();
                $school = $value['school'];

            }else{
               $school_name = School::where(['name'=>trim($value['school'])])->first();
               $school='';
            }


             $user = new User();
             $user->password = Hash::make($value['password']);
             $user->name = $value['name'];
             $user->email = $value['email'];

             /** WebQuiz changes for Digital Bank merger **/

             $user->mobile = $value['mobile'];
             $user->school_id = $school_name->id;
             $user->school = $school;
             $user->country_code = $country_code_value;

             try {
                 if($user->save()){

                    $getallassignedquiz = AssignRound::where(['school_id'=>$school_name->id])->groupBy('round_id')->get();



                    if($getallassignedquiz->count()>0){
                    foreach ($getallassignedquiz as $value) {

                        $assign_round = new AssignRound();
                        
                          $assign_round->round_id = $value->round_id;
                          $assign_round->school_id = $user->school_id;
                          $assign_round->student_id = $user->id;
                          $assign_round->attended = 0;
                          $assign_round->is_result_declared = $value->is_result_declared;
                          $assign_round->expiration_date = $value->expiration_date;
                          $assign_round->quiz_duration = "00:00:00.00";
                          $assign_round->total_question = $value->total_question;
                          $assign_round->quiz_name = $value->quiz_name;
                          $assign_round->round_name = $value->round_name;
                          $assign_round->save();
                        
                    }
                     
                   }
                      
                  }
                  
          }catch(\Illuminate\Database\QueryException $ex){ 
                   $error_code = $ex->errorInfo[1];
                   if($error_code == 1062){
                    $result = $ex->getMessage();
                    $remaining_students[] = ucwords($value['name']);
              }
            }



           }

         
        }
        
        if(count($remaining_students)>0){
        
             $error_names =  array_unique($remaining_students, SORT_REGULAR);
              $students_name = implode(', ', $error_names);
              $ERROR =   'Record Inserted Successfully. '.$students_name.' named student(s) may have empty row or duplicate email or mobile or may school name and country code are anot from the given list.Upload again after removing successfully inserted rows.';
           
         }
         
        
         DB::table('import_csv')
                    ->where('id', $file_name->id)
                    ->update(['status' => '2','start_row'=>$file_name->end_row,'end_row'=>$file_name->end_row+100,'message'=>$ERROR]);
        
      }
      
     /************************End: Registered student csv**********************/
      }
      else if($file_name->file_type=='non_registered_student'){
         
        /****************Start: Non Registered Student csv*********************/
    
      $output = array_slice($data, ($file_name->start_row) , 100);
      
      if(empty($output)){
          
         DB::table('import_csv')
                    ->where('id', $file_name->id)
                    ->delete();
         
      }
      
      if($file_name->status==2){
          foreach ($output as $key => $value) {
          
            if($value['name']==''){
              $name = '';
            } else{
              $name =$value['name'];
            } 
            
            /** WebQuiz changes for Digital Bank merger **/

            $check_user=User::where(['mobile'=>$value['mobile']])->exists();
             if($check_user || $value['mobile']=='' || $value['country_code']==''){
                 $remaining_students[] = $value['mobile'];
                if($value['school']!=''){
                  if (!in_array($value['school'], $school_names))
                  {
                    $remaining_students[] = $value['mobile'];
                  }
                }

                if (!in_array($value['country_code'], $country_code_array))
                {
                  $remaining_students[] = $value['mobile'];
                }
             }else{

              $check=RegisteringStudent::where(['mobile'=>$value['mobile']])->exists();
             if($check ){
                $remaining_students[] = $value['mobile'];
              }else{

                if($value['school']!=''){
                  if (in_array($value['school'], $school_names))
                  {
                    $school_name = $value['school'];
                  }
                }else{
                    $school_name = 'No School Added';
                }

                if (in_array($value['country_code'], $country_code_array))
                {
                     $country_code_value = '+'.$value['country_code'];
                }
                
            //if(preg_match('/(?=^.{0,40}$)^[a-zA-Z]+(\s)?[a-zA-Z]+$/',$name))
            if($name!=='')
               {
                    $name =$name;
                    $user = new RegisteringStudent();
                    $user->name = $name;

                    /** WebQuiz changes for Digital Bank merger **/

                    $user->mobile = $value['mobile'];
                    $user->school_name= $school_name;
                    $user->country_code = $country_code_value;
                     //$user->save();
    
                     if($user->save()){
    
                      /* $register_url_parameter = encrypt($user->name.'^'.$user->mobile.'^'.$user->country_code);*/

                      /** WebQuiz changes for Digital Bank merger **/
    
                     $register_url_parameter = base64_encode(TextToNumber::text_to_number($user->name).'^'.TextToNumber::number_to_text(trim($user->mobile)).'^'.TextToNumber::number_to_text(trim($user->country_code)));
    
    
                      $message_text = "Here's your chance to win Amazing prizes! Register now for the Tune Talk QuizMe Challenge . Go to: ".url('/').'/register/'.$register_url_parameter;
    
                        $details['sms_from'] = '+15416159055';

                        /** WebQuiz changes for Digital Bank merger **/

                        $details['sms_to']   = $user->mobile;
                        $details['sms_to_name']   = $user->name;
                        $details['body_message'] = $message_text;
                        $details['priority']    = '2';
                        $details['country_code']   = $user->country_code;
                        DB::table('sms_queue')->insert($details);
    
                      //Session::flash('successmessage', 'Student imported successfully'); 
                      //return back();

                        /** WebQuiz changes for Digital Bank merger **/

                      }else{
                        $remaining_students[] = $value['mobile'];
                       }
                   
                } else {
                     $remaining_students[] = $value['mobile'];
                }
                

                
                   
              }
             
             }

         
        }
        
        if(count($remaining_students)>0){
        
             $error_names =  array_unique($remaining_students, SORT_REGULAR);
              $students_name = implode(', ', $error_names);
              $ERROR =   $students_name;
           
         }
         
        
         DB::table('import_csv')
                    ->where('id', $file_name->id)
                    ->update(['status' => '2','start_row'=>$file_name->end_row,'end_row'=>$file_name->end_row+100,'message'=>$ERROR]);
        
      }
      
        /****************End : Non Registered Student csv*********************/
      }else if($file_name->file_type=='student_result'){

        $output = array_slice($data, ($file_name->start_row) , 100);
        if(empty($output)){
          
         DB::table('import_csv')
                    ->where('id', $file_name->id)
                    ->delete();
         
      }

      if($file_name->status==2){
         foreach ($output as $key => $value) {
          
          /** WebQuiz changes for Digital Bank merger **/
          
            $student_id = User::where(['mobile'=>$value['mobile_number']])->first();
            $quiz = Quiz::where('title',$value['quiz_name'])->get()->toArray();
            $round = PublishQuizRound::where(['quiz_id'=>$quiz[0]['id'],'round_name'=>$value['round_name'],'status'=>'1'])->first();
            $round_id = $round->id;
            $school = School::where(['name'=>$value['school_name']])->first();
            $school_id = $school->id;

            $check_exist=AssignRound::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id,'attended'=>2])->exists();
            // Check Assign round have data or not
            if($check_exist){
            }else{

            //   $check=AssignRound::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id])->get();
            //   print_r($check); die;
            //   if($check->delete()){
            //       echo "dasda"; die;

               $assign_round = new AssignRound();
               $assign_round->student_id = $student_id->id;
               $assign_round->round_id = $round_id;
               $assign_round->school_id = $school_id;
               $assign_round->attended = 2;
               $assign_round->expiration_date = date('Y-m-d H:i:s');

               if($assign_round->save()){

                 $questions_id =PublishQuestions::where(['quiz_round_id'=>$round_id,'published'=>1])->get();
                 $count_que = 1;
                 foreach ($questions_id as $key => $que_id) {

                    $check_question_id=QuizRoundResult::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id,'question_id'=>$que_id->id])->exists();

                    if($check_question_id){
                      $check_question_id=QuizRoundResult::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id,'question_id'=>$que_id->id]);
                      if($check_question_id->delete()){
                        
                      $quiz_round_result = new QuizRoundResult();
                      $quiz_round_result->student_id = $student_id->id;
                      $quiz_round_result->school_id = $school_id;
                      $quiz_round_result->round_id = $round_id;
                      $quiz_round_result->question_id = $que_id->id;
                      if($value['question_'.$count_que] == "Y"){
                         $quiz_round_result->check_answer = 1;
                      }else{
                         $quiz_round_result->check_answer = 0;
                      }
                      $quiz_round_result->question_type = $que_id->question_type;
                     

                      if($quiz_round_result->save()){
                      
                      }else{
                        Session::flash('errormessage', 'Some error occured'); 
                        return back();
                      }
                    }
                    }else{

                      $quiz_round_result = new QuizRoundResult();
                      $quiz_round_result->student_id = $student_id->id;
                      $quiz_round_result->school_id = $school_id;
                      $quiz_round_result->round_id = $round_id;
                      $quiz_round_result->question_id = $que_id->id;
                      if($value['question_'.$count_que] == "Y"){
                         $quiz_round_result->check_answer = 1;
                      }else{
                         $quiz_round_result->check_answer = 0;
                      }
                      $quiz_round_result->question_type = $que_id->question_type;
                     
                      if($quiz_round_result->save()){
                      
                      }else{
                        Session::flash('errormessage', 'Some error occured'); 
                        return back();
                      }
                    }

                    
                    $count_que++;
                 }



               }
          }

                $score = QuizRoundResult::where(array('student_id'=>$student_id->id,'round_id'=>$round_id,'check_answer'=>1,'school_id'=>$school_id))->count();

                $total_que = PublishQuestions::where(array('quiz_round_id'=>$round_id,'published'=>1))->count();

                $quiz_info = DB::table('quiz')->join('publish_quiz_round', 'publish_quiz_round.quiz_id', '=', 'quiz.id')->select('quiz.title','publish_quiz_round.round_name')->where(array('publish_quiz_round.id'=>$round_id))->first(); 
                
                if($value['duration_format_01h_00m_00s_00ms']==''){
                  $delete_ques = QuizRoundResult::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id]);
                  if($delete_ques->delete()){



                      $calculate_score = AssignRound::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id])->update([
                      'attended'=>0,
                      'total_question'=>$total_que,
                        'quiz_name'=> $quiz_info->title,
                        'round_name'=>$quiz_info->round_name,
                    ]);

                  }
                  $remaining_students[] = ucwords($value['student_name']);
                 
                }else{

                  if (preg_match('/^[0-9]{2}+[H]{1}+[ ]+[0-9]{2}+[M]{1}+[ ]+[0-9]{2}+[S]{1}+[ ]+[0-9]{2}+[MS]{2}+$/', $value['duration_format_01h_00m_00s_00ms'])) {


                        $calculate_duration = $value['duration_format_01h_00m_00s_00ms'];
                        $pieces = explode(" ", $calculate_duration);
                        $Hour_ = preg_replace("/[^0-9,.]/", "", $pieces[0]); // h
                        $Min_ = preg_replace("/[^0-9,.]/", "", $pieces[1]); // m
                        $Sec_ = preg_replace("/[^0-9,.]/", "", $pieces[2]); // m
                        $MSec_ = preg_replace("/[^0-9,.]/", "", $pieces[3]); // m



                        $calculate_score = AssignRound::where(['student_id'=>$student_id->id,'round_id'=>$round_id,'school_id'=>$school_id])->update([
                            'quiz_duration'=>$Hour_.':'.$Min_.':'.$Sec_.'.'.$MSec_,
                            'score'=>$score,
                            'total_question'=>$total_que,
                            'quiz_name'=> $quiz_info->title,
                            'round_name'=>$quiz_info->round_name,
                          ]);
                    } else {
                      
                       $wrong_duration_students_name[] = ucwords($value['student_name']);
                       
                      
                    }

                  
                }

         
        }
        
        if(count($remaining_students)>0 || count($wrong_duration_students_name)>0){
        
             $error_names =  array_unique(array_merge($remaining_students,$wrong_duration_students_name), SORT_REGULAR);
             $duration_students_name = implode(', ', $error_names);

             $ERROR =   $duration_students_name;
           
         }
         
        
         DB::table('import_csv')
                    ->where('id', $file_name->id)
                    ->update(['status' => '2','start_row'=>$file_name->end_row,'end_row'=>$file_name->end_row+100,'message'=>$ERROR]);
      }
          
          
      }
     } 
      
     
    }
}
