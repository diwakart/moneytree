<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use App\School;
use App\AssignRound;
use App\QuizRoundResult;
use App\PublishQuizRound;
use Redirect;
use Session;
use Response;
use Validator;


class SchoolController extends Controller
{
   public function index(){
   	 return view('admin.school');
   }



   public function getAllSchools(Request $request)
    {
      $perpage = $request->datatable['pagination']['perpage'];
      $page = $request->datatable['pagination']['page'];
      if($page=='1')
      {
        $offset = 0;
      }
      else
      {
        $offset = ($page-1)*$perpage;
      }
      $allschools = \DB::table('school_wq')->orderby('id','asc')->offset($offset)->limit($perpage)->get();
      $total = \DB::table('school_wq')->orderby('id','asc')->count();
      $school_data = [];
      $allschools = json_decode(json_encode($allschools));
      $meta = ['draw'=>1,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
      if(!empty($allschools))
      {
        foreach ($allschools as $key => $value)
        {
          $school_data[] = (array)$value;
          $school_data[$key]['S.no'] = $offset+1;
          $offset++;
        }
        return Response::json(array('data'=> $school_data,'meta'=> $meta));
        //return Response::json($school_data);
      }
      return Response::json(array('data'=> $allschools,'meta'=> $meta));
      //return Response::json($allschools);
    }
    
   public function store(Request $request)
    {
      if($request->ajax())
      {

        $rules = array(
          'name'     => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        $school;
        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['name']))
            $errorMsg = $failedRules['name'][0] . "\n";
        
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{
           $id=$request->id;
              $check=School::where(['name'=>$request->name])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();

              if($check){
                  return (json_encode(array('status'=>'error','message'=>"School already exist." )));
                }

              if($request->id){
                $school = School::find($request->id);
              }else{
                $school = new School();
              }
        }

        $school->name = $request->name;
        try {
                 $school->save();
                  return (json_encode(array('status'=>'success','message'=>sprintf('School "%s" successfully saved', $school->name))));
                   
          }catch(\Illuminate\Database\QueryException $ex){ 
                   $error_code = $ex->errorInfo[1];
                   if($error_code == 1062){
                    $result = $ex->getMessage();
                    return (json_encode(array('status'=>'error','message'=>'School already exist'))) ;
              }
            }
      }
    }


   
  public function getschoolDetail(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = School::find($request->id);
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }



    

     public function delete(Request $request)
    {
      if($request->ajax())
      {
        if($request->id){
          $school = School::find($request->id);
           AssignRound::where('school_id', $request->id)->delete();
           User::where('school_id', $request->id)->delete();
           QuizRoundResult::where('school_id', $request->id)->delete();
          
          if ($school->delete())
            return (json_encode(array('status'=>'success','message'=>"School Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }
    }

}
