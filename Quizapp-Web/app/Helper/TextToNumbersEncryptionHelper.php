<?php
namespace App\Helper;
 

class TextToNumbersEncryptionHelper {

    public static function text_to_number($name) {
    	$encoded_name = array_map(function($name){
		$alphabet = range('A', 'Z');
   				return array_search($name, $alphabet);
		},str_split(strtoupper($name),1));
		return implode("~",$encoded_name);
 		
    }

    public static function number_to_text($num) {
    	$numbr = str_replace('+', '', $num);
    	$encoded_number = array_map(function($numbr){
	    $alphabet = range('a', 'k');
	      return $alphabet[$numbr];
	    },str_split($numbr,1));
		return implode("~",$encoded_number);
 		
    }


    public static function text_to_number_decode($num) {
        $pattern   = "~~";
       
        if( strpos( $num, $pattern ) !== false ) {
             $full_name = str_replace("~~","^",$num);
             $student_name = explode("^",$full_name);
            
             $firstname = explode("~", $student_name[0]);
         	 $decoded_firstname = array_map(function($firstname){
    	     $alphabet = range('a', 'z');
    	      return $alphabet[$firstname];
    	    },$firstname);
    	    
    	    $lastname = explode("~", $student_name[1]);
         	 $decoded_lastname = array_map(function($lastname){
    	     $alphabet = range('a', 'z');
    	      return $alphabet[$lastname];
    	    },$lastname);
    	    
	     	return implode("",$decoded_firstname).' '.implode("",$decoded_lastname);
        }else{
           $numbr = explode("~", $num);
           $decoded_number = array_map(function($numbr){
	       $alphabet = range('a', 'z');
	      return $alphabet[$numbr];
	      },$numbr);
		   return implode("",$decoded_number); 
        }

    	
    }

     public static function number_to_text_decode($chr) {
     	$name = explode("~", $chr);
    	$encoded_name = array_map(function($name){
		$alphabet = range('A', 'Z');
   				return array_search($name, $alphabet);
		},str_split(strtoupper($chr),1));
		return implode("",$encoded_name);
 		
    }

  



}
