<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublishAnswers extends Model
{
    public $table = "publish_answers_wq";
}
