<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisteringStudent extends Model
{
   public $table = "registering_student_wq";
}
