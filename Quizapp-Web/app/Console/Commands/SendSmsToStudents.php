<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Twilio\Rest\Client;

class SendSmsToStudents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendSmsToStudents:sendsmstostudents';

    /**
     * The console command description.
     *
     * @var string
     */
   protected $description = 'Sms Send to All Students of School';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sid = 'ACbfcf0a5d3a4790cc95d0ad12b2e7dd19';
        $token = '5930ab90756eade8563f0acc74ba5f69';
        $client = new Client($sid, $token);

        $data = DB::table('sms_queue')
                        ->orderBy('priority', 'desc')
                        ->orderBy('timestamp', 'desc')
                        ->limit(15)
                        ->get()
                        ->toArray();
        foreach ($data as $key => $value) {
            $client->messages->create(
            // the number you'd like to send the message to
            $value->country_code.$value->sms_to,
            array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => '+15416159055',
                // the body of the text message you'd like to send
                'body' => htmlspecialchars_decode(str_replace("{username}",$value->sms_to_name,$value->body_message)),
                // 'mediaUrl' => 'http://big5kayakchallenge.com/wp-content/uploads/2018/01/inspirational-wallpaper-for-mobile-samsung-hii-wallpapers-to-your-cell-phone-hello-hi-wallpaper-for-mobile-samsung.jpg'
            )
            );
            $delete = DB::table('sms_queue')->delete($value->id);
            //echo "<pre>";
            //print_r($value);
            echo "Message Send Successfully";
            //echo "</br>";
            }
    }
}
