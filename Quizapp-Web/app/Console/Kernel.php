<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
       '\App\Console\Commands\SendSmsToStudents',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
       if (!file_exists(base_path('app/Console/log/sms'))) 
        {
            mkdir(base_path('app/Console/log/sms'), 0755, true);
        }
        $filePath = base_path('app/Console/log/sms/sms.log');
        $schedule->command('SendSmsToStudents:sendsmstostudents')
                  ->everyMinute()
                  ->appendOutputTo($filePath);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
