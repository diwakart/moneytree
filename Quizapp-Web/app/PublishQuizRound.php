<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublishQuizRound extends Model
{
    public $table = "publish_quiz_round_wq";
}
