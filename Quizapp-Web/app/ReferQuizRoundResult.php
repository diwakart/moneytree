<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferQuizRoundResult extends Model
{
    public $table = "refer_quiz_round_result_wq";
}
