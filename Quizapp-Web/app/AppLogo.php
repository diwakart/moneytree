<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppLogo extends Model
{
   public $table = "app_logo_wq";
}
