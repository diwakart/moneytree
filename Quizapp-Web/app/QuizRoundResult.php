<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizRoundResult extends Model
{
    public $table = "quiz_round_result_wq";
}
