<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'RouteController@index');

Route::get('/login/{digital_key}', 'RouteController@index');
Route::get('/{digital_key}', 'RouteController@index');
Route::get('participated_student_result/{encrypted_parameter_round_id}', 'RouteController@participated_student_result');

Route::get('cron_sms', 'CronController@index');

Route::get('cron_csv', 'CronController@user_csv');

//**********Ashutosh***************//
//************contact to mobile **********//

Route::get('/refer-quiz/{mobile_userid_assign_quiz}', 'RouteController@view_login');

//**********Ashutosh***************//
//************contact to mobile **********//

Route::get('/register/{name_mobile}', 'RouteController@register_view')->name('register_view');



Route::get('/forget-password', 'RouteController@forget_password')->name('forget-password');
Route::post('/forgot-password', 'RouteController@send_forget_password')->name('forgot-password');


Auth::routes();
Route::group(array('middleware' => ['admin','iefix']), function (){
    

  /*********************App logo****************/
    Route::get('administration/app-logo', 'AppLogoController@index')->name('app-logo');
    Route::post('administration/app-logo/update-logo', 'AppLogoController@update')->name('update-logo');
   /****************Admin Profile**********************/	
   Route::get('administration/home', 'HomeController@index')->name('home');
   Route::get('administration/profile', 'AdminController@index')->name('profile');
   Route::post('administration/profile/update_profile', 'AdminController@update_profile')->name('update_profile');
   Route::post('administration/profile/update_password', 'AdminController@update_password')->name('update_password');

   /****************Student**********************/	
   Route::get('administration/student', 'StudentController@index')->name('student');
   Route::get('administration/register-student', 'StudentController@register_student')->name('register-student');
    Route::post('administration/student/bulkSms', 'StudentController@bulkSmsToStudents')->name('bulkSmsToContacts');
   Route::get('administration/student-demo', 'StudentController@demopage')->name('student-demo');
   Route::get('administration/student/getAllStudents', 'StudentController@getAllStudents')->name('getAllStudents');
   Route::post('administration/student/store', 'StudentController@store')->name('store');
   Route::get('administration/student/exportCSV','StudentController@exportCSV')->name('exportCSV');
   Route::post('administration/student/importCSV', 'StudentController@importCSV')->name('importCSV');
   Route::post('administration/student/getstudentDetail', 'StudentController@getstudentDetail')->name('getstudentDetail');
   Route::post('administration/student/getUserID', 'StudentController@getUserID')->name('getUserID');
   Route::post('administration/student/changepassword', 'StudentController@changepassword');
   Route::post('administration/student/delete', 'StudentController@delete');
   Route::get('administration/assigned-students/{id}', 'StudentController@getAssignedStudents')->name('assignedstudent');
   Route::get('administration/getAssignedSchoolsStudents/{id}', 'StudentController@getAssignedSchoolsStudents')->name('assignedschoolstudent');
   Route::get('administration/student/exportRegisteringStudentCSV','StudentController@exportRegisteringStudentCSV')->name('exportRegisteringStudentCSV');
   Route::post('administration/student/importRegisteringCSV', 'StudentController@importRegisteringCSV')->name('importRegisteringCSV');
   Route::post('administration/student/reminderSms', 'StudentController@reminderSmsToStudents')->name('ReminderSms');
   Route::get('administration/student/getAllRegisteringStudents', 'StudentController@getAllRegisteringStudents')->name('getAllRegisteringStudents');
   Route::post('administration/student/nonRegisterStudentDetails', 'StudentController@nonRegisterStudentDetails')->name('nonRegisterStudentDetails');
   Route::post('administration/register-student/store', 'StudentController@updateDetails')->name('UpdateDetails');
   Route::post('administration/student/registering_delete', 'StudentController@registering_delete');

   /***********Quiz Creation********************/
    Route::post('administration/manage-quiz', 'QuizController@store')->name('store');
    Route::get('administration/manage-quiz', 'QuizController@index')->name('index');
    Route::post('administration/quiz/delete', 'QuizController@delete');
    Route::post('administration/quiz/editquiz', 'QuizController@editquiz')->name('editquiz');
    Route::post('administration/quiz/update_quiz', 'QuizController@update_quiz');
    Route::get('administration/manage-round/{quiz_id}', 'QuizRoundController@index')->name('manage-round');
    Route::post('administration/edit_quiz_round', 'QuizRoundController@edit_quiz_round')->name('edit_quiz_round');
    Route::post('administration/update_quiz_round', 'QuizRoundController@update_quiz_round');
    Route::post('administration/manage-round', 'QuizRoundController@store')->name('manage-round');
    Route::post('administration/manage-round/delete/', 'QuizRoundController@delete')->name('delete-round');
    Route::post('administration/manage-round/publish_round', 'QuizRoundController@publish_round')->name('publish_round');
    Route::post('administration/manage-round/unpublish_round', 'QuizRoundController@unpublish_round')->name('unpublish_round');
    Route::get('administration/manage-question/{round_id}', 'QuestionController@index')->name('manage-question');
     Route::post('administration/getquestionDetail', 'QuestionController@getquestionDetail')->name('getquestionDetail');
     Route::post('administration/delete_question', 'QuestionController@delete_question');
    Route::post('administration/store_question_fill_blanks', 'QuestionController@store_question_fill_blanks')->name('store_question_fill_blanks');
    Route::post('administration/store_question_multi_choice', 'QuestionController@store_question_multi_choice')->name('store_question_multi_choice');
    Route::post('administration/store_question_arrange_order', 'QuestionController@store_question_arrange_order')->name('store_question_arrange_order');
    Route::get('administration/getAllQuestions', 'QuestionController@getAllQuestions')->name('getAllQuestions');
    Route::get('administration/getAllQuiz', 'QuizController@getAllQuiz')->name('getAllQuiz');
    Route::get('administration/getAllQuizRounds', 'QuizRoundController@getAllQuizRounds')->name('getAllQuizRounds');
     Route::post('administration/publish_question', 'QuestionController@publish_question');



    /***************School********************/
    Route::get('administration/school', 'SchoolController@index')->name('school');
     Route::get('administration/school/getAllSchools', 'SchoolController@getAllSchools')->name('getAllSchools');
    Route::post('administration/school/store', 'SchoolController@store')->name('store');
    Route::post('administration/school/getschoolDetail', 'SchoolController@getschoolDetail')->name('getschoolDetail');
    Route::post('administration/school/delete', 'SchoolController@delete');



    /**************Assign Quiz*********************/
     Route::get('administration/assign-quiz/{round_id}', 'AssignQuizController@index')->name('assign-quiz');
     Route::post('administration/assign_quiz', 'AssignQuizController@assign_quiz')->name('assign_quiz');
     Route::get('administration/getAllAssignedSchools', 'AssignQuizController@getAllAssignedSchools')->name('getAllAssignedSchools');
     Route::post('administration/delete_assigned_school', 'AssignQuizController@delete');
     Route::post('administration/getAssignedStudentDetail', 'AssignQuizController@getAssignedStudentDetail')->name('getAssignedStudentDetail');
     Route::post('administration/declare_result', 'QuizResultController@declare_result')->name('declare_result');
     Route::post('administration/declare_result_for_school_and_round', 'QuizResultController@declare_result_for_school_and_round')->name('declare_result_for_school_and_round');
     Route::post('administration/quiz-result/selected_quiz_overall_rank', 'QuizResultController@selected_quiz_overall_rank')->name('selected_quiz_overall_rank');
     Route::post('administration/quiz-result/selected_quiz_rank', 'QuizResultController@selected_quiz_rank')->name('selected_quiz_rank');


     /*************Quiz Result*********************/
    Route::get('administration/quiz-result', 'QuizResultController@index')->name('quiz-result');
    Route::get('administration/result/getAllResults', 'QuizResultController@getAllResults')->name('getAllResults');
    Route::post('administration/getallstudentWithResult', 'QuizResultController@getallstudentWithResult')->name('getallstudentWithResult');
    Route::get('administration/upload-result', 'QuizResultController@upload_result')->name('upload-result');
    Route::post('administration/upload_result_getSchool_id', 'QuizResultController@upload_result_getSchool_id')->name('upload_result_getSchool_id');
    Route::post('administration/upload_result_getquiz_id', 'QuizResultController@upload_result_getquiz_id')->name('upload_result_getquiz_id');
    Route::get('/administration/export_result_csv/{school_id}/{round_id}/{quiz_id}','QuizResultController@export_result_csv')->name('export_result_csv');
    Route::post('administration/importCSVResult', 'QuizResultController@importCSVResult')->name('importCSVResult');
    Route::get('administration/quiz_students_result/{id}/{round_id}', 'QuizResultController@getStudentsResult')->name('studentresult');
   Route::get('administration/getSchoolsResult/{id}/{round_id}', 'QuizResultController@getSchoolsResult')->name('schoolstudentsresult');
   Route::get('/administration/quiz-result/selected_school_round', 'QuizResultController@getDeclaredSchoolsRound')->name('getDeclaredSchoolsRound');
   Route::get('/administration/quiz-result/selected_school_quiz', 'QuizResultController@getDeclaredSchoolsQuiz')->name('getDeclaredSchoolsQuiz');
   


   /****************Quiz waste**********************/  

  Route::get('administration/quiz', 'QuizController@index')->name('quiz');
   Route::get('administration/quiz/add-quiz', 'QuizController@addQuiz')->name('add-quiz');
   Route::post('administration/quiz/add-quiz/store', 'QuizController@store')->name('store');
   Route::post('administration/quiz/deactivate_quiz', 'QuizController@deactivate_quiz')->name('deactivate_quiz');
   Route::post('administration/quiz/activate_quiz', 'QuizController@activate_quiz')->name('activate_quiz');
   Route::post('administration/quiz/edit', 'QuizController@edit')->name('edit');
   Route::post('administration/quiz/delete', 'QuizController@delete')->name('delete');

  /***************Prizes********************/
    Route::get('administration/prizes', 'PrizeController@index')->name('prizes');
    Route::get('administration/add-prizes', 'PrizeController@add_prizes')->name('add-prizes');
    Route::get('administration/add-prizes/{id}', 'PrizeController@add_prizes')->name('edit-prizes');
    Route::get('administration/prizes/getAllPrizes', 'PrizeController@getAllPrizes')->name('getAllPrizes');
    Route::post('administration/prizes/store', 'PrizeController@store')->name('store');
    Route::post('administration/prizes/getprizeDetail', 'PrizeController@getprizeDetail')->name('getprizeDetail');
    Route::post('administration/prizes/delete', 'PrizeController@delete');
    Route::get('administration/assign-prizes', 'PrizeController@assign_prizes')->name('assign-prizes');
    Route::post('administration/get_selected_gift', 'PrizeController@get_selected_gift')->name('get_selected_gift');
    Route::post('administration/assigning-prizes', 'PrizeController@assiging_prizes')->name('assigning-prizes');
    Route::get('administration/assign-prizes/getAllAssignedPrizes', 'PrizeController@getAllAssignedPrizes')->name('getAllAssignedPrizes');
    Route::post('administration/assign-prizes/delete', 'PrizeController@assigned_prize_delete');

    
    


  
});


/*=================Student Routes===================*/
Route::group(array('middleware' => ['student','iefix']), function (){
    
    Route::get('student/test', 'HomeController@test')->name('test');
    
    
  Route::get('student/home', 'HomeController@index')->name('home');
  Route::POST('student/load_result', 'HomeController@load_result')->name('load_result');
  Route::get('student/profile', 'StudentUserController@index')->name('student_profile');
  Route::post('student/update_profile', 'StudentUserController@update_profile')->name('update_profile');
  
  Route::post('student/profile/update_password', 'StudentUserController@update_password')->name('update_password');

  /*======Student Attend Quiz===========*/
  Route::get('student/quiz/{hashed_section}', 'StudentQuizController@index')->name('student_quiz');
  Route::get('student/rounds/{assign_quiz_id}', 'StudentQuizController@get_rounds')->name('get-rounds');

  Route::post('student/quiz_attended', 'StudentQuizController@quiz_attended')->name('quiz_attended');
  Route::post('student/submit_quiz', 'StudentQuizController@submit_quiz')->name('submit_quiz');
  Route::post('student/calculate_score', 'StudentQuizController@calculate_score')->name('calculate_score');
  Route::post('student/submit_quiz_reload', 'StudentQuizController@submit_quiz_reload')->name('submit_quiz_reload');
  Route::post('student/calculate_score_reload', 'StudentQuizController@calculate_score_reload')->name('calculate_score_reload');
  Route::get('student/result/{round_id}', 'StudentQuizController@show_result')->name('result');
  Route::post('student/view_detail_quiz_result', 'StudentQuizController@view_detail_quiz_result')->name('view_detail_quiz_result');
  Route::get('student/quizes', 'StudentQuizController@getallquizs')->name('quizes');
  /*Ranking Board Page*/
  Route::get('student/quiz-results', 'StudentQuizController@quiz_result')->name('quiz-results');
  Route::post('student/selected_quiz_result', 'StudentQuizController@selected_quiz_result')->name('selected_quiz_result');
  Route::post('student/selected_refered_quiz_result', 'StudentQuizController@selected_refered_quiz_result')->name('selected_refered_quiz_result');
  Route::get('student/student-rank/{round_id?}/{school_id?}', 'StudentRankController@index')->name('student-rank');

  Route::post('student/selected_quiz_rank', 'StudentRankController@selected_quiz_rank')->name('selected_quiz_rank');
  Route::post('student/selected_quiz_overall_rank', 'StudentRankController@selected_quiz_overall_rank')->name('selected_quiz_overall_rank');
  
  /**************Refer Friend*****************/
  Route::post('student/refer_quiz', 'ReferFriendController@refer_quiz')->name('refer_quiz');
  Route::get('student/refer-quiz/{refer_hashed_parameter}', 'ReferFriendController@index')->name('refered_quiz');

   Route::post('student/refer_submit_quiz', 'ReferFriendController@refer_submit_quiz')->name('refer_submit_quiz');
  Route::post('student/refer_calculate_score', 'ReferFriendController@refer_calculate_score')->name('refer_calculate_score');
  Route::post('student/refer_submit_quiz_reload', 'ReferFriendController@refer_submit_quiz_reload')->name('refer_submit_quiz_reload');
  Route::post('student/refer_calculate_score_reload', 'ReferFriendController@refer_calculate_score_reload')->name('refer_calculate_score_reload');
  Route::get('student/quiz-refer-result/{round_id}', 'ReferFriendController@refer_show_result')->name('quiz-refer-result');

  Route::get('student/refered-rounds/{refered_id}', 'ReferFriendController@refered_rounds')->name('refered-rounds');
  Route::post('student/refered_quiz_attended', 'ReferFriendController@refered_quiz_attended')->name('refered_quiz_attended');
});

/*** SMS TEST****/
//Route::get('sms', 'CronController@index')->name('sms-test');