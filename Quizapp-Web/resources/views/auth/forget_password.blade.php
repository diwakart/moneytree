@extends('layouts.app')
@section('page_title') 
Web Quiz Application-Forgot Password
@endsection
@section('content')

 <!--<div class="">{{ __('Reset Passwords') }}</div>-->
<h1 class="login_heading">Reset Password</h1>
<div class="m-login__signin">
      @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

    <form class="m-login__form m-form" method="POST" >
         @csrf
        <div class="form-group m-form__group">
          {{-- WebQuiz changes for Digital Bank merger --}}
            <label>Contact Number</label>
          <input id="mobile" type="contact" name="mobile" required class="form-control m-input" placeholder="Contact Number">
        </div>
         
        <div class="m-login__form-action"> 
            <button type="button" id="ForgotPasswordbtn" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                    {{ __('Send Password') }}
                                </button>
        </div>
    </form>
</div>
@endsection


@section('content_heading')
 The Hottest Quiz Challenge in Town
@endsection

@section('page_script')
<script>
    $("#ForgotPasswordbtn").on('click',function(){

      /** WebQuiz changes for Digital Bank merger **/

       mobile = $("#mobile").val();

       /** WebQuiz changes for Digital Bank merger **/

    if(mobile==''){
          swal("Error","Enter Contact Number","error");
      }else{
          $.ajax({
            type: 'POST',
            url: APP_URL+'/forgot-password',
            data: {

              /** WebQuiz changes for Digital Bank merger **/

              mobile: mobile
            },
             headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            success: function(data) {
              var res = $.parseJSON(data);
              if(res.status == 'error'){
                swal('Error',res.message,'error');
              }else{
                 swal('Success',res.message,'success');

                 /** WebQuiz changes for Digital Bank merger **/
                 
                 $("#mobile").val('');
              
              } 
            },
            error: function(data) {
              swal('Error',data,'error');
            }
          });
      }

    })

    $(".confirm").on('click',function(){
        window.location.href = APP_URL;
    })
</script>
@endsection

