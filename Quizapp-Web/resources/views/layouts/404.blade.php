<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Version: 5.0.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			{{'Web Quiz Application | Admin'}}
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
           var APP_URL = {!! json_encode(url('/')) !!} 
		</script>

		<!--end::Web font -->
        <!--begin::Base Styles -->
		<link href="{{ asset('assets/inner/404/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/inner/404//style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="{{ asset('assets/inner/404/favicon.ico') }}" />
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid  m-error-3" style="background-image: url({{ asset('assets/inner/404/bg3.jpg')}});">
				<div class="m-error_container">
					<span class="m-error_number">
						<h1>
							404
						</h1>
					</span>
					<p class="m-error_title m--font-light">
						How did you get here
					</p>
					<p>
					<a href="javascript:;" class="m-error_title m--font-light" id="visitthis" style="    font-size: 20px;
    color: #101025!important;">
					Click Here to Jump Home	
					</a>
				</p>
					<p class="m-error_subtitle">
						Sorry we can't seem to find the page you're looking for.
					</p>
					<p class="m-error_description">
						There may be amisspelling in the URL entered,
						<br>
						or the page you are looking for may no longer exist.
					</p>
				</div>
			</div>
		</div>
		<!-- end:: Page -->
    	<!--begin::Base Scripts -->
		<script src="{{ asset('assets/inner/404/vendors.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/inner/404/scripts.bundle.js') }}" type="text/javascript"></script>
		<script>
			$("#visitthis").on('click',function(e){
				e.preventDefault();
				window.location.href=APP_URL;
			})
		</script>
		<!--end::Base Scripts -->
	</body>
	<!-- end::Body -->
</html>
