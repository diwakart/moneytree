<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			@yield('page_title')  
		</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });

           var APP_URL = {!! json_encode(url('/')) !!} 
		</script>
		<!--end::Web font -->
        <!--begin::Base Styles -->  
        <!--begin::Page Vendors -->
		<link href="{{ asset('assets/inner/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors -->
		<link href="{{ asset('assets/inner/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/inner/demo/demo5/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
       <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="{{ asset('assets/logo/favicon.ico') }}" />
		@yield('page_css')  
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body class="m-page--wide m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default"  >
		<input type="hidden" name="id" id="adminid" value="{{ Auth::user()->id }}">
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<!-- begin::Header -->
			  @include('header.admin_header')
			<!-- end::Header -->		
		<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
						@yield('content')

						<!-- Modal for Add Quiz -->
						<div class="modal fade" id="CreateQuizModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
							    <div class="modal-content">
							        <div class="modal-header">
							            <h5 class="modal-title" id="exampleModalLabel">
							               
							            </h5>
							            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                <span aria-hidden="true">
							                    &times;
							                </span>
							            </button>
							        </div>
							        <form name="fm-student">
							        <div class="modal-body">
							          
							                <div class="form-group">
							                    <label for="quizTitle" class="form-control-label">
							                      Quiz  Title:
							                    </label>
							                    <input type="text" class="form-control" id="quizTitle" name="quizTitle">
							                </div>
							            
							        </div>
							        <div class="modal-footer">
							            <button type="button" class="btn btn-secondary" data-dismiss="modal">
							                Close
							            </button>
							            <button type="button" class="btn btn-primary" id="AddQuizModalButton">
							                Save changes
							            </button>
							        </div>
							        </form>
							    </div>
							</div>
						</div> 

					</div>
				</div>
			</div>
			<!-- end::Body -->
            <!-- begin::Footer -->
			<footer class="m-grid__item m-footer ">
				<div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
					<div class="m-footer__wrapper">
						<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
							<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
								<span class="m-footer__copyright">
									2018 &copy; Web Quiz Application
								</span>
							</div>
							<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
							</div>
						</div>
					</div>
				</div>
			</footer>
			<!-- end::Footer -->
		</div>
		<!-- end:: Page -->   
	    <!-- begin::Scroll Top -->
		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
			<i class="la la-arrow-up"></i>
		</div>
		
    	<!--begin::Base Scripts -->
		<script src="{{ asset('assets/inner/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/inner/demo/demo5/base/scripts.bundle.js') }}" type="text/javascript"></script>
		<!--end::Base Scripts -->   
        <!--begin::Page Vendors -->
		<script src="{{ asset('assets/inner/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
		<!--end::Page Vendors -->  
        <!--begin::Page Snippets -->
		<script src="{{ asset('assets/inner/app/js/dashboard.js') }}" type="text/javascript"></script>
		<!--end::Page Snippets -->
		<script>
			$(".CreateQuizButton").on('click', function(){
				$('#CreateQuizModal').modal('show');
			})

			$("#AddQuizModalButton").on('click', function(){
		        var AdminId = $("#adminid").val();
		        var title = $("#quizTitle").val();
		 
		        if(title=='' ){
		            swal("Error","Please Enter Quiz Name","error");
		        }else{
		            $.ajax({
		              type: 'POST',
		              url: APP_URL+'/administration/manage-quiz',
		              data: {
		                user_id: AdminId,
		                title: title
		              },
		               headers: {
		                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		               },
		              success: function(data) {
		                var res = $.parseJSON(data);
		                if(res.status == 'error'){
		                  swal('Error',res.message,'error');
		                }else{
		                  window.location.href = APP_URL + "/administration/manage-round/"+res.quiz_id;
		                } 
		              },
		              error: function(data) {
		                swal('Error',data,'error');
		              }
		            });
		        }
            })
		</script>
		@yield('page_script') 
	</body>
	<!-- end::Body -->
</html>
