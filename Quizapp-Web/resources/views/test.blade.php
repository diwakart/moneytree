@extends('layouts.student_test')
@section('page_css')
<link href="{{ asset('assets/student/css/enjoyhint.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('content')


<div class="s_main"> 
<section class="quiz_div"> 
    <div class="np">
    <div class="row">
        <div class="col-xs-7"><div class="quiz_title">
            
            <img src="{{ asset('assets/student/images/left_triangle.jpg') }}" />


            <h3 class="">Active Quiz</h3></div></div>  
        <div class="col-xs-5"> 
        <button type="button" class="btn enter-quiz">View Rules</button>
               
        </div> 
    </div>
  </section>
 
    <section class="question_section">
    <div class="row">
        <div class="col-xs-12">
            <div class="question_div">
                <div class="question_block">
            <h2>General Knowledge</h2>
            </div>
                 <div class="question_start">
             <button type="button" class="btn start_quiz_btn">CLICK TO ENTER QUIZ</button>
            </div> 
                <div class="notice_div">
                <img src="{{ asset('assets/student/images/price.png') }}">
                    <p> Be among the top 10 for this quiz <br> to win a prize! </p>
                </div>
        
            </div>
             
        </div>
        </div>
        
    </section>
 
    <section class="quiz_div"> 
    <div class="np">
    <div class="row">
        <div class="col-xs-9"><div class="quiz_title">
          <img src="{{ asset('assets/student/images/left_triangle.jpg') }}" />
            <h3 class="">Recent Result</h3></div></div>  
        <div class="col-xs-3"> 
        <!--<button type="button" class="btn enter-quiz">View Rules</button>-->
               
        </div> 
    </div>
  </section>
        
         <section class="result_section">
    <div class="row">
        <div class="col-xs-12">
            <div class="question_div">
                <div class="question_block result_pad">
            <h4 class="score_title">Score</h4>
                    <h3>33</h3>
                     <h4 class="tune_title">Tune Talk Call Record</h4>
                    <h3>00:00:33</h3>
            </div>
                 <div class="question_start">
             <button type="button" class="btn start_quiz_btn">VIEW DETAILS</button>
            </div> 
            </div>
             
        </div>
        </div>
        
    </section>
        
        
      
        
@endsection



@section('page_js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="{{ asset('assets/student/js/enjoyhint.js') }}"></script>
<script src="{{ asset('assets/student/js/webstep.js') }}"></script>

<script>



</script>
@endsection
