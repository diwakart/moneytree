@extends('layouts.default')
@section('page_title') 
Web Quiz Application-Dashboard
@endsection
@section('page_css')
<style>
.m-widget24 .m-widget24__item .m-widget24__title{
    font-size: 20px;
}
.card_height{
    padding: 20px;
}
</style>
@endsection
<div class="m-subheader ">
    <div class="d-flex align-items-center">
@section('content')
<!-- BEGIN: Subheader -->
        <div class="mr-auto">
            <h3 class="m-subheader__title ">
                Dashboard
   
            </h3>
             @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
             <button type="button" class="btn m-btn--square  btn-outline-success btn-lg m-btn m-btn--custom CreateQuizButton" style="float:right">
             Create Quiz
            </button>
        </div>
        <div>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <!--Begin::Main Portlet-->
    <div class="m-portlet ">
    <div class="m-portlet__body  m-portlet__body--no-padding">
        
        <div class="row m-row--no-padding m-row--col-separator-xl">
           

            <!--<div class="col-md-12 col-lg-6 col-xl-3 card_height">-->
            <!--    <div class="m-widget24">-->
            <!--        <div class="m-widget24__item">-->
            <!--            <h4 class="m-widget24__title">-->
            <!--                Student(s)-->
            <!--            </h4>-->
            <!--            <br>-->
            <!--            <span class="m-widget24__desc">-->
            <!--                Total Student-->
            <!--            </span>-->
            <!--            <span class="m-widget24__stats m--font-brand">-->
            <!--                {{$count_student}}-->
            <!--            </span>-->
            <!--            <div class="m--space-10"></div>-->
            <!--        </div>-->
            <!--    </div>-->
            <!--</div>-->
            <div class="col-md-12 col-lg-6 col-xl-4 card_height">
                <!--begin::New Feedbacks-->
                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                            Quiz(s)
                        </h4>
                        <br>
                        <span class="m-widget24__desc">
                            Total quiz
                        </span>
                        <span class="m-widget24__stats m--font-info">
                           {{$count_quiz}}
                        </span>
                        <div class="m--space-10"></div>
                    </div>
                </div>
                <!--end::New Feedbacks-->
            </div>
            <div class="col-md-12 col-lg-6 col-xl-4 card_height">
                <!--begin::New Orders-->
                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                          Result
                        </h4>
                        <br>
                        <span class="m-widget24__desc">
                           Quiz Results 
                        </span>
                        <span class="m-widget24__stats m--font-danger">
                          {{$count_quiz_result}}
                        </span>
                        <div class="m--space-10"></div>
                    </div>
                </div>
                <!--end::New Orders-->
            </div>
            <div class="col-md-12 col-lg-6 col-xl-4 card_height">
                <!--begin::New Users-->
                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                           School(s)
                        </h4>
                        <br>
                        <span class="m-widget24__desc">
                             Total School
                        </span>
                        <span class="m-widget24__stats m--font-success">{{$count_school}}</span>
                          
                        <div class="m--space-10"></div>
    
                    </div>
                </div>
                <!--end::New Users-->
            </div>
        </div>
    </div>
</div>
    <!--End::Main Portlet-->




</div>
@endsection
