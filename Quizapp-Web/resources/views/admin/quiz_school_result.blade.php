@extends('layouts.default')
@section('page_title') 
Result
@endsection 
@section('page_css')
<style>

</style>
@endsection
@section('content')

<div class="m-subheader ">
        <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
               Quiz Result
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{route('quiz')}}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="{{url('administration/quiz-result')}}" class="m-nav__link">
                        <span class="m-nav__link-text">
                            All Quiz Result
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a class="m-nav__link">
                        <span class="m-nav__link-text">
                            {{$school->name}} Result
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        </div>
</div>
 
 <!-- END: Subheader -->
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Student(s)
                        <small>
                           Assigned Student of {{$school->name}}
                        </small>
                    </h3>

                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="student name or mobile" id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
<!--begin: Datatable -->
             <div class="loader_msg" style='display: block;'>
                <img src="{{ asset('assets/inner/loader.gif') }}" width='132px' height='132px' style="height: 70px;width: 67px;margin-left: 40%;">
            </div>
            <input type="hidden" name="school_id" id="school_id" value="{{$school->id}}">
            <input type="hidden" name="round_id" id="round_id" value="{{request()->route('round_id')}}">
            <div class="assigned_school_students" id="ajax_data"></div>
            <!--end: Datatable -->
        </div>
    </div>
</div>
@endsection

@section('page_script')
<script>

 var datatable;
          (function() {
                $('.loader_msg').css('display','none');
                var iDisplayIndex=1;
                var school_id = $('#school_id').val();
                var round_id = $("#round_id").val();
                datatable = $('.assigned_school_students').mDatatable({
                // datasource definition
                data: {
                type: 'remote',
                source: {
                  read: {
                      url: APP_URL + "/administration/getSchoolsResult/"+school_id+'/'+round_id,
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                              generalSearch: '',
                          }
                      },
                      map: function(raw) {
                          console.log('ITS RUN');
                          console.log(raw);
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                               dataSet = raw.data;
                          }
                          console.log('Result');
                          console.log(dataSet);
                          return dataSet;
                      },
                  }
                },
                pageSize: 10,
                  saveState: {
                    cookie: false,
                    webstorage: false
                },

                serverPaging: true,
                serverFiltering: false,
                serverSorting: false
            },
          // layout definition
            layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
              input: $('#generalSearch')
            },

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
        field: "S.no",
        title: "S.No",
        width: 100
      },
      {
          field: "name",
          title: "Student Name",
          textAlign: 'center'
      },
      {
          field: "quiz_name",
          title: "Quiz Name",
          textAlign: 'center'
      },
      {
          field: "round_name",
          title: "Round Name",
          textAlign: 'center'
      },
      {
          field: "quiz_duration",
          title: "Quiz Duration",
          textAlign: 'center'
      },
      {
          field: "score",
          title: "Quiz Score",
          textAlign: 'center'
      },
      {
          field: "attended",
          title: "Quiz Status",
          width: 130,
          textAlign: 'center',
          template: function(row) {
          var status = row.attended;
          if(status == 2)
          {
            return '\
            <span>Participated</span>\
          ';
          }

          return '\
            <span>Not Participated</span>\
          ';
          }
      },
      {
          field: "rank",
          title: "Quiz Rank",
          textAlign: 'center'
      }]
          });
          var query = datatable.getDataSourceQuery();
          
          $('#generalSearch').on('change',function(){
          var value = $(this).val().trim();
          datatable.setDataSourceQuery({value:value});
          datatable.reload();
        });
    })();
</script>
@endsection
