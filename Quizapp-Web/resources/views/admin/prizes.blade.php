@extends('layouts.default')
@section('page_title') 
Prize
@endsection 
@section('page_css')
<style>
#ResponseHeading{
 color: #4CAF50;
}
</style>
@endsection
@section('content')

<div class="m-subheader ">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="m-subheader__title m-subheader__title--separator">
        Prizes
      </h3>
      <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
        <li class="m-nav__item m-nav__item--home">
          <a href="{{url('administration/home')}}" class="m-nav__link m-nav__link--icon">
            <i class="m-nav__link-icon la la-home"></i>
          </a>
        </li>
        <li class="m-nav__separator">
          -
        </li>
        <li class="m-nav__item">
          <a href="{{url('administration/prizes')}}" class="m-nav__link">
            <span class="m-nav__link-text">
              Manage Prizes
            </span>
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>

<!-- END: Subheader -->
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Prize(s)
                        <small>
                           All Prize(s)
                        </small>
                    </h3>

                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                @if(Session::has('errormessage'))
                      <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        <strong>
                            Oh snap!
                        </strong>{{ Session::get('errormessage') }}
                      </div>
                      @endif

                      @if(Session::has('successmessage'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        <strong>
                            Well done!
                        </strong>{{ Session::get('successmessage') }}
                    </div>
                @endif
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <a href="{{route('add-prizes')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" >
                            <span>
                                <i class="la la-gift"></i>
                                <span>
                                    Add Prize
                                </span>
                            </span>
                        </a>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
<!--begin: Datatable -->
             <div class="loader_msg" style='display: block;'>
                <img src="{{ asset('assets/inner/loader.gif') }}" width='132px' height='132px' style="height: 70px;width: 67px;margin-left: 40%;">
            </div>
            <div class="prize_datatable" id="ajax_data"></div>
            <!--end: Datatable -->
        </div>
    </div>
</div>

<div class="modal fade" id="addPrizeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    &times;
                </span>
            </button>
        </div>
        <form name="fm-student" action="{{ asset('administration/prizes/store') }}"  method="post" id="SubmitPrize" enctype = "multipart/form-data">
        <div class="modal-body">
          
                <div class="form-group">
                    <label for="gift_name" class="form-control-label">
                      Prize Name:
                    </label>
                    <input type="text" class="form-control" id="gift_name" name="gift_name">
                </div>

                <div class="form-group">
                    <label for="name" class="form-control-label">
                      Description:
                    </label>
                    <textarea class="form-control" id="gift_desc" name="gift_desc"></textarea>
                </div>

                <div class="form-group">
                    <label for="name" class="form-control-label">
                      Image:
                    </label>
                    <input type="file" class="form-control" id="gift_img" name="gift_img" accept="image/x-png,image/jpeg" >
                </div>
                
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
            <button type="submit" class="btn btn-primary">
                Save changes
            </button>
        </div>
        </form>
    </div>
</div>
</div>


<!-- Modal for success response -->
    <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <form name="fm-student">
                <div class="modal-body">
                    <h5 id="ResponseHeading"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadSchoolDatatable">
                        OK
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_script')

<script>

     var datatable;
          (function() {
                $('.loader_msg').css('display','none');
                datatable = $('.prize_datatable').mDatatable({
                // datasource definition
                data: {
                type: 'remote',
                source: {
                  read: {
                      url: APP_URL+'/administration/prizes/getAllPrizes',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                              generalSearch: '',
                          }
                      },
                      map: function(raw) {
                          console.log('ITS RUN');
                          console.log(raw);
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                               dataSet = raw.data;
                          }
                          console.log('Result');
                          console.log(dataSet);
                          return dataSet;
                      },
                  }
                },
                pageSize: 10,
                  saveState: {
                    cookie: false,
                    webstorage: false
                },

                serverPaging: true,
                serverFiltering: false,
                serverSorting: false
            },
          // layout definition
            layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
              input: $('#generalSearch')
            },

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
        field: "S.no",
        title: "S.No",
        width: 100
      },
      {
          field: "name",
          title: "Title",
          textAlign: 'center'
      },
      {
          field: "description",
          title: "Description",
          textAlign: 'center'
      },
      {
          title: 'Prize Image',
          field: 'picture',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';
            if(row.picture=='' || row.picture==null){
              var gift_img = APP_URL+'/assets/student/images/prize.gif';
            }            
            else{
              var gift_url = "{{url('/').'/storage/app/'}}";
              var gift_img = gift_url+row.picture;
              
            }
            return '\
            <img src='+gift_img+' style="width:50px;height:50px;">\
          ';
          }
      },
      {
          field: "points",
          title: "Points",
          textAlign: 'center'
      },
      {
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          field: 'Actions',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

            return '\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit" onclick=getprizeDetail('+row.id+')></i>\
            </a>\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deletePrize('+row.id+')>\
              <i class="la la-trash"></i>\
            </a>\
          ';
          },
        }]
          });


          var query = datatable.getDataSourceQuery();

        var query = datatable.getDataSourceQuery();

       
      
        $('#formSubmit').on('click',function(){
          datatable.reload();
        });

       
      
        $("#LoadSchoolDatatable").on('click',function(){
          datatable.reload();
        });

    })();


$('#addPrize').click(function(e){
    e.preventDefault();
    $('#addPrizeModal').modal('show');
    $('#addPrizeModal').data('id','');
    $('#addPrizeModal').find('.modal-title').html('Add Prize');
    $('#name').val('');
  });


  $('form#SubmitPrize').submit(function(e){
    e.preventDefault();
    var gift_name = $("#gift_name").val();
    var gift_desc = $("#gift_desc").val();
    var gift_img = $("#gift_img").val();

    $.ajax({
      type: 'POST',
      url: APP_URL +'/administration/prizes/store',
      data:{
        gift_name:gift_name,
        gift_desc:gift_desc,
        gift_img:gift_img,
      },
      headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(data) {
        var res = $.parseJSON(data);
        if(res.status == 'error'){
          swal('Error',res.message,'error');
        }else{
          $('#addSchoolModal').modal('hide');
          $("#ResponseSuccessModal").modal('show');
          $("#ResponseSuccessModal #ResponseHeading").text(res.message);
        } 
      },
      error: function(data) {
        swal('Error',data,'error');
      }
    });
  });
 
  function getprizeDetail(id){
        window.location.href = APP_URL + "/administration/add-prizes/"+id;
  }




  function deletePrize(id){
    var path = APP_URL + "/administration/prizes/delete";
    var _this = $(this);
    swal({
      title: "Are you sure to delete this Prize?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        var data = id;
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: data,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               $('.sweet-overlay').remove();
               $('.showSweetAlert ').remove();
              $("#ResponseSuccessModal").modal('show');
              $("#ResponseSuccessModal #ResponseHeading").text(res.message);
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
  }

      
</script>
@endsection


