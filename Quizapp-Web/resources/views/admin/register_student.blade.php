@extends('layouts.default')
@section('page_title') 
Non Registered Students
@endsection 
@section('page_css')
<style>
#ResponseHeading{
 color: #4CAF50;
}
</style>
@endsection
@section('content')

<div class="m-subheader ">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="m-subheader__title m-subheader__title--separator">
       Non Registered Students
      </h3>
      <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
        <li class="m-nav__item m-nav__item--home">
          <a href="{{url('administration/home')}}" class="m-nav__link m-nav__link--icon">
            <i class="m-nav__link-icon la la-home"></i>
          </a>
        </li>
        <li class="m-nav__separator">
          -
        </li>
        <li class="m-nav__item">
          <a href="{{url('administration/register-student')}}" class="m-nav__link">
            <span class="m-nav__link-text">
             Non Registered Students
            </span>
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                @if(Session::has('errormessage'))
                      <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        <strong>
                            Oh snap!
                        </strong>{{ Session::get('errormessage') }}
                      </div>
                      @endif

                      @if(Session::has('successmessage'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        <strong>
                            Well done!
                        </strong>{{ Session::get('successmessage') }}
                    </div>
                @endif
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">

                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <button class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" id="sendBulkSms">
                            <span>
                                <i class="fa fa-envelope-o"></i>
                                <span>
                                    SEND SMS
                                </span>
                            </span>
                        </button>

                        <a href="javascript;:" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="addRegisteringStudentCSV">
                            <span>
                                <i class="la la-upload"></i>
                                <span>
                                    CSV Upload
                                </span>
                            </span>
                        </a>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
<!--begin: Datatable -->
             <div class="loader_msg" style='display: block;'>
                <img src="{{ asset('assets/inner/loader.gif') }}" width='132px' height='132px' style="height: 70px;width: 67px;margin-left: 40%;">
            </div>
            <div class="registering_student_datatable" id="ajax_data"></div>
            <!--end: Datatable -->
        </div>
    </div>
</div>




<div class="modal fade" id="addRegisteringStudentModalCSV" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    &times;
                </span>
            </button>
        </div>
        <form name="fm-student" id="submit_form_csv" action="{{ asset('administration/student/importRegisteringCSV') }}" method="post" enctype = "multipart/form-data">
        <div class="modal-body">
          {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="form-control-label">
                        Upload File:
                    </label>
                    <input type="file" class="form-control" id="csvfile" name="csvfile" accept=".csv">
                </div>
            
                <div class="form-group">

                  {{--  WebQuiz changes for Digital Bank merger --}}

                    <label for="mobile" class="form-control-label">
                      Download Sample File from <a href="{{route('exportRegisteringStudentCSV')}}">here</a>.
                    </label>
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
            <button type="submit" class="btn btn-primary" id="formSubmitCSV">
                Save changes
            </button>
        </div>
        </form>
    </div>
</div>
</div>

<!-- Edit Student Details Modal-->

<div class="modal fade" id="editStudentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    &times;
                </span>
            </button>
        </div>
        <form name="fm-student">
        <div class="modal-body">
                
                <div class="form-group">
                    <label for="name" class="form-control-label">
                        School:
                    </label>
                    <select class="form-control" id="school_name" name="school_name">
                      <option value="">Select School
                      </option>
                      @foreach($schools as $school)
                      <option value="{{$school->name}}">{{$school->name}}</option>
                      @endforeach
                  </select>
                </div>

                <div class="form-group">
                    <label for="name" class="form-control-label">
                        Name:
                    </label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="country_code" class="form-control-label">
                       Country:
                    </label>
                    <select class="form-control" id="country_code" name="country_code">
                      <option value="">Select Country Code
                      </option>
                      @foreach($countries as $key => $value)
                      <option value="{{$key}}" @if($key== '+60') {{ 'selected' }}@endif>{{$value}}</option>
                      @endforeach
                  </select>
                </div>

                {{-- WebQuiz changes for Digital Bank merger --}}  

                <div class="form-group">
                    <label for="mobile" class="form-control-label">
                       mobile:
                    </label>

                     {{-- WebQuiz changes for Digital Bank merger --}}  


                    <input type="number" class="form-control" id="mobile" name="mobile">
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
            <button type="button" class="btn btn-primary" id="formSubmit">
                Save changes
            </button>
        </div>
        </form>
    </div>
</div>
</div>


<!-- Modal for success response -->
    <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <form name="fm-student" id="arrange-order-form">
                <div class="modal-body">
                    <h5 id="ResponseHeading"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadQuestionDatatable">
                        OK
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_script')


<script>
$("#addRegisteringStudentCSV").on('click',function(e){
  e.preventDefault();
  $("#addRegisteringStudentModalCSV").modal('show');
})

 var datatable;
          (function() {
                $('.loader_msg').css('display','none');
                datatable = $('.registering_student_datatable').mDatatable({
                // datasource definition
                data: {
                type: 'remote',
                source: {
                  read: {
                      url: APP_URL+'/administration/student/getAllRegisteringStudents',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                              generalSearch: '',
                              student_list:'',
                          }
                      },
                      map: function(raw) {
                          console.log('ITS RUN');
                          console.log(raw);
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                               dataSet = raw.data;
                          }
                          console.log('Result');
                          console.log(dataSet);
                          return dataSet;
                      },
                  }
                },
                pageSize: 10,
                  saveState: {
                    cookie: false,
                    webstorage: false
                },

                serverPaging: true,
                serverFiltering: false,
                serverSorting: false
            },
          // layout definition
            layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
              input: $('#generalSearch')
            },

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
        field: "id",
        title: "id",
        width: 100,
        selector: {class: 'm-checkbox--solid m-checkbox--brand'}
      },
      {
          field: "name",
          title: "Name",
          textAlign: 'center'
      },

      /** WebQuiz changes for Digital Bank merger **/  


      {

          field: "mobile",
          title: "Contact",
          textAlign: 'center'
      },
      {
          field: "school_name",
          title: "School",
          textAlign: 'center'
      },
      {
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          field: 'Actions',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

            return '\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit" onclick=getstudentDetail('+row.id+')></i>\
            </a>\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteRegisteringStudent('+row.id+')>\
              <i class="la la-trash"></i>\
            </a>\
          ';
          },
        }]
          });

                 /** WebQuiz changes for Digital Bank merger **/  



                  $('.registering_student_datatable').on('m-datatable--on-check', function (e, args) {
              var count = datatable.getSelectedRecords().length;
              selmobile = $.map(datatable.getSelectedRecords(), function (item) {
              return $(item).find("td").eq(0).find("input").val();
              });
            $('#m_datatable_selected_number').html(count);
            if (count > 0) {
              $('#sendBulkSms').show();
            }
          })
          .on('m-datatable--on-uncheck m-datatable--on-layout-updated', function (e, args) {
            var count = datatable.getSelectedRecords().length;
            $('#m_datatable_selected_number').html(count);
            if (count === 0) {
              $('#sendBulkSms').hide();
            }
          });

        $('#sendBulkSms').on('click', function(){
          var path = APP_URL + "/administration/student/bulkSms";

          /** WebQuiz changes for Digital Bank merger **/

          var mobile_id = JSON.parse(JSON.stringify(selmobile));
          $.ajax({
          type: 'POST',
          url: path,
          data: {

            /** WebQuiz changes for Digital Bank merger **/

            student_id : mobile_id,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               swal('Success',res.message,'success');
               datatable.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
        })

        $("#LoadQuestionDatatable").on('click',function(){
          datatable.reload();
        });

    })();
    
    $('#formSubmit').click(function(e)
    {
      e.preventDefault();
      var id = $('#editStudentModal').data('id');
      var name  = $('#name').val();

      /** WebQuiz changes for Digital Bank merger **/

      var mobile = $('#mobile').val();
      var school = $("#school_name").val();
      var country_code = $("#country_code").val();

      /** WebQuiz changes for Digital Bank merger **/

      if(mobile.length > 10){
         swal("","Contacts Not Greater than 10","error");

        /** WebQuiz changes for Digital Bank merger **/

         $('#mobile').val('');
         return false;
      }
      $.ajax({
        type: 'POST',
        url: APP_URL +'/administration/register-student/store',
        data: {
          id: id,
          name: name,

         /** WebQuiz changes for Digital Bank merger **/

          mobile: mobile,
          school:school,
          country_code:country_code,
        },
         headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          var res = $.parseJSON(data);
          if(res.status == 'error'){
            swal('Error',res.message,'error');
          }else{
            $('#editStudentModal').modal('hide');
            $("#ResponseSuccessModal").modal('show');
            $("#ResponseSuccessModal #ResponseHeading").text(res.message);
          } 
        },
        error: function(data) {
          swal('Error',data,'error');
        }
      });
    });

    function getstudentDetail(id)
    {
      var path = APP_URL + "/administration/student/nonRegisterStudentDetails";
      $.ajax({
        type: "POST",
        url: path,
        data: {
          id: id
        },
         headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
        success: function(result){
          //console.log(result);
          var res = $.parseJSON(result);

          if(res.status == 'error'){

          }else{
            var data = $.parseJSON(JSON.stringify(res.message));
            $('#editStudentModal').data('id',data.id);
            $('#editStudentModal').find('.modal-title').html('Edit student Details of ' + data.name);
            $('#name').val(data.name);

            /** WebQuiz changes for Digital Bank merger **/
            
            $('#mobile').val(data.mobile);
            $('#country_code').val(data.country_code).prop('selected', true);
            $('#school_name').val(data.school_name).prop('selected', true);
            $('#editStudentModal').modal('show');
          }
        },
        error: function(){
          alert("Error");
        }
      }); 
    }

    function deleteRegisteringStudent(id){
    var path = APP_URL + "/administration/student/registering_delete";
    var _this = $(this);
    swal({
      title: "Are you sure to delete this student?",
      text: "Your will lost all records of this Student",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        var data = id;
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: data,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               $('.sweet-overlay').remove();
               $('.showSweetAlert ').remove();
              $("#ResponseSuccessModal").modal('show');
              $("#ResponseSuccessModal #ResponseHeading").text(res.message);
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
  }
</script>
@endsection


