@extends('layouts.default')
@section('page_title') 
Question
@endsection 
@section('page_css')
<style>
ul#ArrangeOrderDiv li {
    background: #8bc34a;
    padding: 10px 15px;
    margin: 3px;
    color: #fff;
    border-radius: 4px;
}
ul#ArrangeOrderDiv li {
    position: relative;
    left: -21px;
}
#ResponseHeading{
 color: #4CAF50;
}

   
</style>
@endsection
@section('content')
<input type="hidden" name="id" id="adminid" value="{{ Auth::user()->id }}">
 <input type="hidden" name="round_id" value="{{request()->route('round_id')}}" id="round_id">
<div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Quiz
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{route('quiz')}}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="{{ asset('administration/manage-round/'.$Quiz->id) }}" class="m-nav__link">
                        <span class="m-nav__link-text">
                            {{$Quiz->title}}
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="{{ asset('administration/manage-question/'.$quiz_round->id) }}" class="m-nav__link">
                        <span class="m-nav__link-text">
                            {{$quiz_round->round_name}}
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="javascript:;" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Manage Questions
                        </span>
                    </a>
                </li>
            </ul>
        </div>

        <!--  <button type="button" class="btn btn-success" id="AddQuizRoundBtn">Add Round</button> -->
       
        </div>
    </div>

    <div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Quiz Round Question(s)
                        <small>
                           List of added quiz rounds questions
                        </small>
                    </h3>

                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        @if($quiz_round->status==1)
                        @else
                        <div class="m-dropdown m-dropdown--inline m-dropdown--arrow" data-dropdown-toggle="click" style="padding-right: 67px;">
                            
                            
                             <a href="javascript:;" class="m-dropdown__toggle btn btn-primary dropdown-toggle">
                                  Add Question
                             </a>
                           
                       

                            <div class="m-dropdown__wrapper">
                              <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                              <div class="m-dropdown__inner">
                                <div class="m-dropdown__body">
                                  <div class="m-dropdown__content">
                                    <ul class="m-nav">
                                      <li class="m-nav__section m-nav__section--first">
                                        <span class="m-nav__section-text">
                                          Select Question Type
                                        </span>
                                      </li>
                                      <li class="m-nav__item">
                                        <a href="javascript:;" id="FillBlankQuestionType" class="m-nav__link">
                                          <i class="m-nav__link-icon flaticon-share"></i>
                                          <span class="m-nav__link-text">
                                            Fill in the Blank question
                                          </span>
                                        </a>
                                      </li>
                                      <li class="m-nav__item">
                                        <a href="javascript:;" class="m-nav__link" id="MultiChoiceQuestionType">
                                          <i class="m-nav__link-icon flaticon-chat-1"></i>
                                          <span class="m-nav__link-text">
                                            Multi Choice Question
                                          </span>
                                        </a>
                                      </li>
                                      <li class="m-nav__item">
                                        <a href="javascript:;" id="ArrangeOrderQuestionType" class="m-nav__link">
                                          <i class="m-nav__link-icon flaticon-info"></i>
                                          <span class="m-nav__link-text">
                                            Arrange Order
                                          </span>
                                        </a>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                           @endif


                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
<!--begin: Datatable -->
            <div class="loader_msg" style='display: block;'>
                <img src="{{ asset('assets/inner/loader.gif') }}" width='132px' height='132px' style="height: 70px;width: 67px;margin-left: 40%;">
            </div>
            <div class="quiz_round_question_datatable" id="QuizRoundQuestionData"></div>
            <!--end: Datatable -->
        </div>
    </div>
    </div>

 <!-- Modal for short question type -->
    <div class="modal fade" id="FillBlankTypeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                       Fill in the blank(s).
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <form name="fm-student" id="fill-blanks-form">
                   
                <div class="modal-body">
                        
                        <div class="form-group">
                            <label for="fillblank-input" class="form-control-label">
                              Question :
                            </label>
                            <textarea type="text" name="fill_question_input" id="shortQuestion-input" class="form-control"></textarea>
                        </div>

                        <div id="FillInTheBlanksAnswers">
                            <div class="form-group  m-form__group row">
                                <label for="fillblank-input" class="form-control-label">
                                 Answer for blank(s) :
                               </label>
                                <div data-repeater-list="" id="FillAnswerResponse" class="col-lg-12 ">
                                    <div data-repeater-item class="row m--margin-bottom-10 clonedInputsForBlank">
                                        <div class="col-lg-9">
                                            <div class="input-group">
                                            <input type="text" class="form-control form-control-danger ans_input" name="fill_answer_input" placeholder="Answer" id="">
                                            </div>
                                        </div>
                                       
                                        <div class="col-lg-3">
                                            <a href="javascript:;" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                                <i class="la la-remove"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col">
                                    <div data-repeater-create="" class="btn btn btn-primary m-btn m-btn--icon">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>
                                                Add
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary" id="FillBlankTypeBtn">
                        Submit
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div> 
   

<!-- Modal for multi choice question type -->
    <div class="modal fade" id="MultiChoiceQuestionTypeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                      Multi Choice Question/Answer 
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <form name="fm-student" id="multi-choice-form">
                <div class="modal-body">
                        
                    <div class="form-group">
                        <label for="shortQuestion-input" class="form-control-label">
                          Question :
                        </label>
                        <textarea name="multiQuestion-input" id="multiQuestion-input" class="form-control"></textarea>
                    </div>

                        
                    <div id="MultiChoiceOptionDiv">
                        <div class="form-group  m-form__group">
                            <label for="multiQuestion-option" class="form-control-label">
                              Option(s) :
                            </label>
                            <div data-repeater-list="" id="MultiAnswerResponse">
                                <div data-repeater-item class="form-group m-form__group row align-items-center clonedOptionsForMultiChoice">
                                    <div class="col-md-9">
                                        <div class="input-group m-form__group">
                                            <span class="input-group-addon">
                                               
                                                    <input type="checkbox" class="check_answer" >
                                                    
                                               
                                            </span>
                                            <input type="text" class="form-control options_value" aria-label="Enter Option">
                                        </div>
                                        <div class="d-md-none m--margin-bottom-10"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
                                            <span>
                                                <i class="la la-trash-o"></i>
                                                <span>
                                                    Delete
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-form__group form-group row">
                            <label class="col-lg-2 col-form-label"></label>
                            <div class="col-lg-4">
                                <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            Add
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary" id="MultiChoiceQuestionTypeBtn">
                        Submit
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div> 



<!-- Modal for multi choice question type -->
    <div class="modal fade" id="ArrangeOrderQuestionTypeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                      Arrange Order Question/Answer 
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <form name="fm-student" id="arrange-order-form">
                <div class="modal-body">
                       
                        <div id="ArrangeOrderParts">

                        <div class="form-group">
                            <label for="arrangeQuestion-input" class="form-control-label">
                              Question :
                            </label>
                            <textarea name="arrangeQuestion-input" id="arrangeQuestion-input" class="form-control"></textarea>
                        </div>
                       
                        <div class="form-group  m-form__group row" id="DoClone">
                            <label  class="col-lg-2 col-form-label">
                                Parts:
                            </label>
                            <div data-repeater-list=""  class="col-lg-10 " id="ArrangeOrderPartsDiv">
                                <div data-repeater-item class="m--margin-bottom-10 clone_number" id="clone0">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="la la-check"></i>
                                        </span>
                                        <input type="text" class="form-control form-control-danger incorrect_order" placeholder="Enter Question Part">
                                        <span class="input-group-btn" data-repeater-delete="">
                                            <a href="javascript:;" class="btn btn-danger m-btn m-btn--icon" >
                                                <i class="la la-close"></i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col">
                                <div data-repeater-create="" class="btn btn btn-warning m-btn m-btn--icon">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            Add
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-info" id="ArrangeOrderBtn">Arrange Correct Order</button>
                            </div>

                        </div>
                    </div>
                       <br><br> 
                    <div class="form-group">
                        <ul id="ArrangeOrderDiv" type="none">
                        </ul>

                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary" id="ArrangeOrderQuestionTypeBtn">
                        Submit
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>     
   

<!-- Modal for success response -->
    <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <form name="fm-student" id="arrange-order-form">
                <div class="modal-body">
                    <h5 id="ResponseHeading"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadQuestionDatatable">
                        OK
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal for view question -->
    <!-- Modal for multi choice question type -->
    <div class="modal fade" id="QuestionViewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <form name="fm-student" >
                <div class="modal-body">
                        <div class="form-group">
                            <label for="view_question" class="form-control-label">
                              Question :
                            </label>
                            <textarea name="view_question" id="view_question" class="form-control"></textarea>
                        </div>
                </div>
                <div id="AnsResponse">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="UpdateQuestionBtn">
                        Submit
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>     
   
@endsection

@section('page_script')
<script src="{{asset('assets/inner/question-page.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<script>
 $("#FillBlankQuestionType").on('click',function(){
    $("#FillBlankTypeModal").modal('show');
 }) 

 $("#MultiChoiceQuestionType").on('click',function(){
    $("#MultiChoiceQuestionTypeModal").modal('show');
 }) 

 $("#ArrangeOrderQuestionType").on('click',function(){
     $("#ArrangeOrderQuestionTypeModal")
    .find("input,textarea,select")
       .val('');
    $("#ArrangeOrderQuestionTypeModal #ArrangeOrderDiv").html('');
     $(this).data('id','');

    $("#ArrangeOrderQuestionTypeModal").modal('show');
 })

$("#ArrangeOrderQuestionTypeModal #DoSortable").sortable({
    stop : function(event, ui){
}
});


  var datatable;
          (function() {
                $('.loader_msg').css('display','none');
                datatable = $('.quiz_round_question_datatable').mDatatable({
                // datasource definition
                data: {
                type: 'remote',
                source: {
                  read: {
                      url: APP_URL+'/administration/getAllQuestions',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                              generalSearch: '',
                          }
                      },
                      map: function(raw) {
                          console.log('ITS RUN');
                          console.log(raw);
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                               dataSet = raw.data;
                          }
                          console.log('Result');
                          console.log(dataSet);
                          return dataSet;
                      },
                  }
                },
                pageSize: 10,
                  saveState: {
                    cookie: false,
                    webstorage: false
                },

                serverPaging: true,
                serverFiltering: false,
                serverSorting: false
            },
          // layout definition
            layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
              input: $('#generalSearch')
            },

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
              field: "S.no",
              title: "S.No",
              textAlign: 'center'
            }, {
              field: "question",
              title: "Question"
            }, {
              field: "question_type",
              title: "Question Type",
              template: function(row) {
                var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';
                if(row.question_type==1){
                     return '\
                <p>Fill Blanks</p>\
              ';
                }else if(row.question_type==2){
                     return '\
                <p>Multiple Choice</p>\
              ';
                }else{
                    return '\
                <p>Arrange Order</p>\
              ';
                }
              },
            },{
              field: 'Actions',
              width: 110,
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              template: function(row) {
                var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';
                if(row.status==1){
                    return '\
                <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="View" onclick=getquestionDetail('+row.id+')>\
                  <i class="la la-eye"></i>\
                </a>\
              ';

                }else{
                    return '\
                <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick=deleteQuestion('+row.id+')>\
                  <i class="la la-trash"></i>\
                </a>\
                \
                <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit" onclick=getquestionDetail('+row.id+')>\
                  <i class="la la-eye"></i>\
                </a>\
              ';
                }
                  
              },
          }]
          });


          var query = datatable.getDataSourceQuery();
        
        $('#LoadQuestionDatatable').on('click',function(e){
            e.preventDefault();
            datatable.reload();
        });

    })();


$("#ArrangeOrderQuestionTypeModal #ArrangeOrderBtn").on('click', function(){
   var list_number = 1;
   $("#ArrangeOrderQuestionTypeModal #ArrangeOrderDiv").html('');
  $("#ArrangeOrderPartsDiv .clone_number").each(function(){
    var order_text = $(this).find('input').val();
    var OrderList = "<li id="+list_number+">"+order_text+"</li>";
    $("#ArrangeOrderQuestionTypeModal #ArrangeOrderDiv").append(OrderList);
    list_number++;
   }); 

   $("#ArrangeOrderDiv").sortable({
    stop : function(event, ui){
     }
   });

})


/*========Fill in the blanks===========*/
$(function(){
        $("#fill-blanks-form").submit(function(event){
            event.preventDefault();
               

                var answers = [];
                i = 0;
                $('.ans_input').each(function()
                { 
                    if($(this).val()!=''){
                        answers[i++] = $(this).val();
                    }
                     
                });

               
                var question = $("#shortQuestion-input").val();
                var round_id = $("#round_id").val();

                var questionid = $('#FillBlankTypeModal').data('id');
               
                if(question!='' ||  round_id!=''){
                    $.ajax({
                              type: 'POST',
                              url: APP_URL+'/administration/store_question_fill_blanks',
                              data: {
                                answers : answers,
                                question: question,
                                round_id:round_id,
                                question_id:questionid,
                              },
                               headers: {
                                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                               },
                              success: function(data) {
                                var res = $.parseJSON(data);
                                if(res.status == 'error'){
                                  swal('Error',res.message,'error');
                                }else{
                                    $("#FillBlankTypeModal").modal('hide'); 
                                    $("#ResponseSuccessModal").modal('show');
                                    $("#ResponseSuccessModal #ResponseHeading").text(res.message);

                                } 
                              },
                              error: function(data) {
                                swal('Error',data,'error');
                              }
                            });
                }else{
                    swal('Error','Please fill out input boxes','error');
                   
                }
                
           });
    });

/*========Multi Choice==================*/

$(function(){
        $("#multi-choice-form").submit(function(event){
            event.preventDefault();
               

                var options = [];
                var correct_answer = [];
                i = 0;
                $('.options_value').each(function()
                { 
                    if($(this).val()!=''){
                        correct_answer[i] = $(this).siblings().children().val();
                        options[i++] = $(this).val();
                    }
                     
                });


                var question = $("#multiQuestion-input").val();
                var round_id = $("#round_id").val();

               var questionid = $('#MultiChoiceQuestionTypeModal').data('id');
                if(question!='' ||  round_id!=''){
                    $.ajax({
                              type: 'POST',
                              url: APP_URL+'/administration/store_question_multi_choice',
                              data: {
                                options : options,
                                question: question,
                                round_id:round_id,
                                correct_answer : correct_answer,
                                question_id:questionid,
                              },
                               headers: {
                                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                               },
                              success: function(data) {
                                var res = $.parseJSON(data);
                                if(res.status == 'error'){
                                  swal('Error',res.message,'error');
                                }else{
                                  $("#MultiChoiceQuestionTypeModal").modal('hide'); 
                                  $("#ResponseSuccessModal").modal('show');
                                  $("#ResponseSuccessModal #ResponseHeading").text(res.message);
                                } 
                              },
                              error: function(data) {
                                swal('Error',data,'error');
                              }
                            });
                }else{
                    swal('Error','Please fill out input boxes','error');
                   
                }
                
           });
    });


$(document).on("change", "input[class='check_answer']", function () {
    if (this.checked) {
     $(this).val(1);
     }else{
    $(this).val(0);
     }
});

/*======Arrange Order==============*/

$(function(){
        $("#arrange-order-form").submit(function(event){
            event.preventDefault();
               

                var incorrect_order = [];
                var correct_order = [];
                i = 0;
                $('.incorrect_order').each(function()
                { 
                    if($(this).val()!=''){
                        incorrect_order[i++] = $(this).val();
                    }
                     
                });
                 $('#ArrangeOrderDiv li').each(function(index)
                {
                        correct_order.push($(this).text());
                });

                 console.log(correct_order);
                 console.log(incorrect_order);
                var question = $("#arrangeQuestion-input").val();
                var round_id = $("#round_id").val();

                 var questionid = $('#ArrangeOrderQuestionTypeModal').data('id');

               
                if(question!='' ||  round_id!=''){
                        $.ajax({
                              type: 'POST',
                              url: APP_URL+'/administration/store_question_arrange_order',
                              data: {
                                incorrect_order : incorrect_order,
                                correct_order : correct_order,
                                question: question,
                                round_id:round_id,
                                question_id:questionid,
                              },
                               headers: {
                                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                               },
                              success: function(data) {
                                var res = $.parseJSON(data);
                                if(res.status == 'error'){
                                  swal('Error',res.message,'error');
                                }else{
                                  $("#ArrangeOrderQuestionTypeModal").modal('hide'); 
                                  $("#ResponseSuccessModal").modal('show');
                                    $("#ResponseSuccessModal #ResponseHeading").text(res.message);
                                    $("#ArrangeOrderDiv").sortable({
                                        stop : function(event, ui){
                                         }
                                       });
                                } 
                              },
                              error: function(data) {
                                swal('Error',data,'error');
                              }
                            });
                }else{
                    swal('Error','Please fill out input boxes','error');
                   
                }
                
           });
    });


$('#FillBlankTypeModal').on('hidden.bs.modal', function (e) {
  $(this)
    .find("input,textarea,select")
       .val('')
       .end()
    .find("input[type=checkbox], input[type=radio]")
       .prop("checked", "")
       .end();
        $(this).data('id','');
})


$('#MultiChoiceQuestionTypeModal').on('hidden.bs.modal', function (e) {
  $(this)
    .find("input,textarea,select")
       .val('')
       .end()
    .find("input[type=checkbox], input[type=radio]")
       .prop("checked", "")
       .end();
        $(this).data('id','');
})


$('#ArrangeOrderQuestionTypeModal').on('hidden.bs.modal', function (e) {
  $(this)
    .find("input,textarea,select")
       .val('')
       .end()
    .find("input[type=checkbox], input[type=radio]")
       .prop("checked", "")
       .end();
    $("#ArrangeOrderDiv").html('');
     $(this).data('id','');
})


function deleteQuestion(ques_id){
   
            swal({
              title: "Are you sure to delete this Question?",
              text: "Your will not be able to recover this Question!",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false,
            },
            function(isConfirm) {
              if (isConfirm) {
                var id = ques_id;
                $.ajax({
                      type: 'POST',
                      url: APP_URL+'/administration/delete_question',
                      data: {
                        id: id,
                      },
                       headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                      success: function(data) {
                        var res = $.parseJSON(data);
                        if(res.status == 'error'){
                          swal('Error',res.message,'error');
                        }else{
                          $('.sweet-overlay').remove();
                          $('.showSweetAlert ').remove();
                          $("#ResponseSuccessModal").modal('show');
                          $("#ResponseSuccessModal #ResponseHeading").text(res.message);
                        } 
                      },
                      error: function(data) {
                        swal('Error',data,'error');
                      }
                    });
              } else {

              }
            });
            }


        function getquestionDetail(id){
        var round_id = $("#round_id").val();
        var path = APP_URL + "/administration/getquestionDetail";
        $.ajax({
          type: "POST",
          url: path,
          data: {
            id: id,
            round_id : round_id
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);

            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              var answer = $.parseJSON(JSON.stringify(res.answers));
              var ansHtml = '';
              var i=0;
              if(data.question_type==1){
                $("#FillBlankTypeModal #shortQuestion-input").val(data.question);
                $('#FillBlankTypeModal').data('id',data.id);

                $.each(answer, function(idx,values){
                  
                      ansHtml += '<div data-repeater-item="" class="row m--margin-bottom-10 clonedInputsForBlank" style=""><div class="col-lg-9"><div class="input-group"><input type="text" class="form-control form-control-danger ans_input" name="['+i+'][fill_answer_input]" placeholder="Answer" id="" value="'+values.correct_answer+'"></div></div><div class="col-lg-3"><a href="javascript:;" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"><i class="la la-remove"></i></a></div></div>';
                   
                    i++;
                });
                $("#FillBlankTypeModal #FillAnswerResponse").html(ansHtml);
                $('#FillBlankTypeModal').modal('show');
                
              }else if(data.question_type==2){
                $("#MultiChoiceQuestionTypeModal #multiQuestion-input").val(data.question);
                $('#MultiChoiceQuestionTypeModal').data('id',data.id);

                $.each(answer, function(idx,values){
                        if(values.correct_answer==1){
                            var checked = 'checked';
                            var chk_val = 1;
                        }else{
                            var checked = '';
                            var chk_val = 0;
                        }
                      ansHtml += '<div data-repeater-item="" class="form-group m-form__group row align-items-center clonedOptionsForMultiChoice" style=""><div class="col-md-9"><div class="input-group m-form__group"><span class="input-group-addon"><input type="checkbox" class="check_answer" value="'+chk_val+'" '+checked+'></span><input type="text" class="form-control options_value" aria-label="Enter Option" value="'+values.option_text+'"></div><div class="d-md-none m--margin-bottom-10"></div></div><div class="col-md-3"><div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill"><span><i class="la la-trash-o"></i><span>Delete</span></span></div></div></div>';
                   
                    
                });
                $("#MultiChoiceQuestionTypeModal #MultiAnswerResponse").html(ansHtml);

                $('#MultiChoiceQuestionTypeModal').modal('show');

              }else{
                $("#ArrangeOrderQuestionTypeModal #arrangeQuestion-input").val(data.question);
                $('#ArrangeOrderQuestionTypeModal').data('id',data.id);
                var j=1;
                resultres = '';
                $.each(answer, function(idx,values){
                      ansHtml += '<div data-repeater-item="" class="m--margin-bottom-10 clone_number" id="clone'+j+'" style=""><div class="input-group"><span class="input-group-addon"><i class="la la-check"></i></span><input type="text" class="form-control form-control-danger incorrect_order" placeholder="Enter Question Part" value="'+values.option_text+'"><span class="input-group-btn" data-repeater-delete=""><a href="javascript:;" class="btn btn-danger m-btn m-btn--icon"><i class="la la-close"></i></a></span></div></div>';

                       resultres += '<li id="'+j+'">'+values.correct_answer+'</li>';
                     
                   j++;
                    
                });
                $("#ArrangeOrderQuestionTypeModal #ArrangeOrderPartsDiv").html(ansHtml);
                $("#ArrangeOrderQuestionTypeModal #ArrangeOrderDiv").html(resultres);


                $('#ArrangeOrderQuestionTypeModal').modal('show');
                 $("#ArrangeOrderDiv").sortable({
                                        stop : function(event, ui){
                                         }
                                       });

              }

              if(data.status==1){
                $("#ArrangeOrderQuestionTypeBtn").css('display','none');
                $("#FillBlankTypeBtn").css('display','none');
                $("#MultiChoiceQuestionTypeBtn").css('display','none');
              }else{
                $("#ArrangeOrderQuestionTypeBtn").css('display','block');
                $("#FillBlankTypeBtn").css('display','block');
                $("#MultiChoiceQuestionTypeBtn").css('display','block');
              }

            }


          },
          error: function(){
            alert("Error");
          }
        }); 
  } 



  function publishQuestion(ques_id){
   
            swal({
              title: "Are you sure to publish this Question?",
              text: "Your will not be able to edit and delete this Question after publish!",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-success",
              confirmButtonText: "Yes, Publish!",
              closeOnConfirm: false,
            },
            function(isConfirm) {
              if (isConfirm) {
                var id = ques_id;
                
                $.ajax({
                      type: 'POST',
                      url: APP_URL+'/administration/publish_question',
                      data: {
                        id: id,
                      },
                       headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                      success: function(data) {
                        var res = $.parseJSON(data);
                        if(res.status == 'error'){
                          swal('Error',res.message,'error');
                        }else{
                          $('.sweet-overlay').remove();
                          $('.showSweetAlert ').remove();
                          $("#ResponseSuccessModal").modal('show');
                          $("#ResponseSuccessModal #ResponseHeading").text(res.message);
                        } 
                      },
                      error: function(data) {
                        swal('Error',data,'error');
                      }
                    });
              } else {

              }
            });
            }   
</script>

@endsection


