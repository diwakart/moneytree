@extends('layouts.default')
@section('page_title') 
Student
@endsection 
@section('page_css')
<style>
#ResponseHeading{
 color: #4CAF50;
}
</style>
@endsection
@section('content')

<div class="m-subheader ">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="m-subheader__title m-subheader__title--separator">
        Students
      </h3>
      <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
        <li class="m-nav__item m-nav__item--home">
          <a href="{{url('administration/home')}}" class="m-nav__link m-nav__link--icon">
            <i class="m-nav__link-icon la la-home"></i>
          </a>
        </li>
        <li class="m-nav__separator">
          -
        </li>
        <li class="m-nav__item">
          <a href="{{url('administration/student')}}" class="m-nav__link">
            <span class="m-nav__link-text">
              Manage Students
            </span>
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Student(s)
                        <small>
                           Registered Student(s)
                        </small>
                    </h3>

                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                @if(Session::has('errormessage'))
                      <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        <strong>
                            Oh snap!
                        </strong>{{ Session::get('errormessage') }}
                      </div>
                      @endif

                      @if(Session::has('successmessage'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        <strong>
                            Well done!
                        </strong>{{ Session::get('successmessage') }}
                    </div>
                @endif
                <div class="row align-items-center">
                    <div class="col-xl-6 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                          <div class="col-md-6">
                              <select class="selectpicker" id="student_list" name="student_list">
                                <option value="">Select School Name</option>
                                @foreach($schools as $school)
                                <option value="{{$school->id}}">{{$school->name}}</option>
                                @endforeach
                              </select>
                            </div>

                            <div class="col-md-6">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Contact Number..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 order-1 order-xl-2 m--align-right">
                        
                        <button href="javascript;:" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="sendSms" data-toggle="modal" data-target="#ReminderMessageModal">
                            <span>
                                <i class="fa fa-envelope-o"></i>
                                <span>
                                    Reminder Sms
                                </span>
                            </span>
                        </button>
                        
                        <a href="javascript;:" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="addStudent">
                            <span>
                                <i class="la la-user"></i>
                                <span>
                                    Add Student
                                </span>
                            </span>
                        </a>

                        <a href="javascript;:" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="addStudentCSV">
                            <span>
                                <i class="la la-upload"></i>
                                <span>
                                    CSV Upload
                                </span>
                            </span>
                        </a>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
<!--begin: Datatable -->
             <div class="loader_msg" style='display: block;'>
                <img src="{{ asset('assets/inner/loader.gif') }}" width='132px' height='132px' style="height: 70px;width: 67px;margin-left: 40%;">
            </div>
            <div class="student_datatable" id="ajax_data"></div>
            <!--end: Datatable -->
        </div>
    </div>
</div>

<div class="modal fade" id="addStudentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    &times;
                </span>
            </button>
        </div>
        <form name="fm-student">
        <div class="modal-body">
                
                <div class="form-group">
                    <label for="name" class="form-control-label">
                        School:
                    </label>
                    <select class="form-control" id="school_name" name="school_name">
                      <option value="">Select School
                      </option>
                      @foreach($schools as $school)
                      <option value="{{$school->id}}">{{$school->name}}</option>
                      @endforeach
                  </select>
                </div>
                
                <div class="form-group" id="other_school_name" style="display:none;">
                    <label for="name" class="form-control-label">
                        Other School Name:
                    </label>
                    <input type="text" class="form-control" id="other_name" name="other_name">
                </div>

                <div class="form-group">
                    <label for="name" class="form-control-label">
                        Name:
                    </label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="email" class="form-control-label">
                        Email:
                    </label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>
                <div class="form-group" id="PasswordInput">
                    
                </div>
                <div class="form-group">
                    <label for="country_code" class="form-control-label">
                       Country:
                    </label>
                    <select class="form-control" id="country_code" name="country_code">
                      <option value="">Select Country Code
                      </option>
                      @foreach($countries as $key => $value)
                      <option value="{{$key}}" @if($key== '+60') {{ 'selected' }}@endif>{{$value}}</option>
                      @endforeach
                  </select>
                </div>

                 {{-- WebQuiz changes for Digital Bank merger --}} 

                <div class="form-group">
                    <label for="contact" class="form-control-label">
                       mobile:
                    </label>
                    <input type="number" class="form-control" id="mobile" name="mobile">
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
            <button type="button" class="btn btn-primary" id="formSubmit">
                Save changes
            </button>
        </div>
        </form>
    </div>
</div>
</div>


<div class="modal fade" id="addStudentModalCSV" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    &times;
                </span>
            </button>
        </div>
        <form name="fm-student" id="submit_form_csv" action="{{ asset('administration/student/importCSV') }}" method="post" enctype = "multipart/form-data">
        <div class="modal-body">
          {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="form-control-label">
                        Upload File:
                    </label>
                    <input type="file" class="form-control" id="csvfile" name="csvfile" accept=".csv">
                </div>
            
                <div class="form-group">
                    <label for="contact" class="form-control-label">
                      Download Sample File from <a href="{{route('exportCSV')}}">here</a>.
                    </label>
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
            <button type="submit" class="btn btn-primary" id="formSubmitCSV">
                Save changes
            </button>
        </div>
        </form>
    </div>
</div>
</div>


<div class="modal fade" id="ChangePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    &times;
                </span>
            </button>
        </div>
        <form name="fm-student" id="change_password_form">
        <div class="modal-body">
          {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="form-control-label">
                        Password:
                    </label>
                    <input type="password" class="form-control" id="change_password" name="change_password" accept=".csv">
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
            <button type="submit" class="btn btn-primary" id="ChangePasswordBtn">
                Save changes
            </button>
        </div>
        </form>
    </div>
</div>
</div>

<!-- Modal For Reminder Message-->

<div class="modal fade" id="ReminderMessageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               Reminder Sms
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    &times;
                </span>
            </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
              <label for="name" class="form-control-label">
                  Message:
              </label>
              <textarea class="form-control" id="message" name="message"></textarea>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
            <button class="btn btn-primary" id="sendReminder">
                Send
            </button>
        </div>
      </div>
  </div>
</div>

<!-- Modal for success response -->
    <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <form name="fm-student" id="arrange-order-form">
                <div class="modal-body">
                    <h5 id="ResponseHeading"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadQuestionDatatable">
                        OK
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_script')
<script src="{{asset('assets/inner/datatable/data-ajax.js')}}" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

<script>
    
     var datatable;
          (function() {
                $('.loader_msg').css('display','none');
                datatable = $('.student_datatable').mDatatable({
                // datasource definition
                data: {
                type: 'remote',
                source: {
                  read: {
                      url: APP_URL+'/administration/student/getAllStudents',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {

                            /** WebQuiz changes for Digital Bank merger **/

                              mobile: '',
                              student_list:'',
                          }
                      },
                      map: function(raw) {
                          console.log('ITS RUN');
                          console.log(raw);
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                               dataSet = raw.data;
                          }
                          console.log('Result');
                          console.log(dataSet);
                          return dataSet;
                      },
                  }
                },
                pageSize: 10,
                  saveState: {
                    cookie: false,
                    webstorage: false
                },

                serverPaging: true,
                serverFiltering: false,
                serverSorting: false
            },
          // layout definition
            layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

           /* search: {
              input: $('#generalSearch')
            },*/

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
        field: "id",
        title: "id",
        width: 30,
        selector: {class: 'm-checkbox--solid m-checkbox--brand'}
      },
      {
          field: "name",
          title: "Name",
          textAlign: 'center'
      },
      {
        field: "email",
        title: "Email",
        textAlign: 'center'
       
      },
      {

        /** WebQuiz changes for Digital Bank merger **/

          field: "mobile",
          title: "Contact",
          textAlign: 'center'
      },
      {
          field: "school_name",
          title: "School",
          textAlign: 'center',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

            if(row.school_name=='Other'){
               return '\
            '+row.school_name+' - '+row.school+'\
          ';
            }else{
               return '\
            '+row.school_name+'\
          ';
            }

           
          }
      },
      {
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          field: 'Actions',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

            return '\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit" onclick=getstudentDetail('+row.id+')></i>\
            </a>\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Change Password">\
              <i class="la la-key" onclick=getUserID('+row.id+')></i>\
            </a>\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteStudent('+row.id+')>\
              <i class="la la-trash"></i>\
            </a>\
          ';
          },
        }]
          });
        $('.student_datatable').on('m-datatable--on-check', function (e, args) 
        {
            var count = datatable.getSelectedRecords().length;

            /** WebQuiz changes for Digital Bank merger **/

            selmobile = $.map(datatable.getSelectedRecords(), function (item) {
            return $(item).find("td").eq(0).find("input").val();
            });
            $('#m_datatable_selected_number').html(count);
            if (count > 0) {
            $('#sendSms').show();
            }
        })
          .on('m-datatable--on-uncheck m-datatable--on-layout-updated', function (e, args) {
            var count = datatable.getSelectedRecords().length;
            $('#m_datatable_selected_number').html(count);
            if (count === 0) {
              $('#sendSms').hide();
            }
          });
          
        $('#sendReminder').on('click', function(){
          var message = $('#message').val();
          if(message == '')
          {
            swal("","Message Filed Required","error");
            return false;
          }
          var path = APP_URL + "/administration/student/reminderSms";

          /** WebQuiz changes for Digital Bank merger **/

          var mobile_id = JSON.parse(JSON.stringify(selmobile));
          $.ajax({
          type: 'POST',
          url: path,
          data: {

            /** WebQuiz changes for Digital Bank merger **/

            student_id : mobile_id,
            message : message
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
              $('#message').val('');
              $('#ReminderMessageModal').modal('hide');
              swal('Success',res.message,'success');
              datatable.reload();
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
        })
      
        $('#formSubmit').on('click',function(){
          datatable.reload();
        });

        $("#ChangePasswordBtn").on('click',function(){
          datatable.reload();
        });

        $('#student_list').on('change',function(){
          var value = $(this).val();

          /** WebQuiz changes for Digital Bank merger **/

          var mobile = $('#generalSearch').val();
          datatable.setDataSourceQuery({student_list:value,mobile:mobile});
          datatable.reload();
        });

        $('#generalSearch').on('change',function(){
          var value = $(this).val();
          var student_id = $('#student_list').val();

          /** WebQuiz changes for Digital Bank merger **/

          datatable.setDataSourceQuery({mobile:value,student_id:student_id});
          datatable.reload();
        });
      
        $("#LoadQuestionDatatable").on('click',function(){
          datatable.reload();
        });

    })();



$('#addStudentCSV').click(function(e){
    e.preventDefault();
    $('#addStudentModalCSV').modal('show');
    $('#addStudentModalCSV').data('id','');
    $('#addStudentModalCSV').find('.modal-title').html('CSV Upload');
    $('#csvfile').val('');
  });
$(document).ready(function(){
    $('#school_name').on('change',function(){
        var school_name = $('#school_name option:selected').text();
        if(school_name=='Other')
        {
            $('#other_school_name').show();
        }
        else
        {
          $('#other_school_name').hide();  
        }
    });
    
});
$('#addStudent').click(function(e){
    e.preventDefault();
    $('#addStudentModal').modal('show');
    $('#addStudentModal').data('id','');
    $('#addStudentModal').find('.modal-title').html('Add Student');
    $('#name').val('');
    $('#email').val('');

    /** WebQuiz changes for Digital Bank merger **/

    $('#mobile').val('');
    $('#other_school_name').hide();  
     $('#school_name').val('');
    var pwd_input = '<label for="password" class="form-control-label">Password:</label><input type="password" class="form-control" id="password" name="password">';
    $("#PasswordInput").html(pwd_input);
  });


  $('#formSubmit').click(function(e){
    e.preventDefault();

    var id = $('#addStudentModal').data('id');
    var name  = $('#name').val();
    var email  = $('#email').val();

    /** WebQuiz changes for Digital Bank merger **/

    var mobile = $('#mobile').val();
    var school = $("#school_name").val();
    var other_school_name = $("#other_name").val();
    var country_code = $("#country_code").val();

    /** WebQuiz changes for Digital Bank merger **/
    if(mobile.length > 13){
       swal("","Contacts Not Greater than 13","error");

       /** WebQuiz changes for Digital Bank merger **/

       $('#mobile').val('');
       return false;
    }
    if(id=='undefined'){
       var password  = '';
    }else{
       if($('#password').val()==''){
      swal("","Password field is required","error");
      return false;
       }
        var password  = $('#password').val();
    }
    $.ajax({
      type: 'POST',
      url: APP_URL +'/administration/student/store',
      data: {
        id: id,
        name: name,
        email:email,

        /** WebQuiz changes for Digital Bank merger **/

        mobile: mobile,
        password: password,
        school:school,
        other_school_name:other_school_name,
        country_code:country_code,
      },
       headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(data) {
        var res = $.parseJSON(data);
        if(res.status == 'error'){
          swal('Error',res.message,'error');
        }else{
          $('#addStudentModal').modal('hide');
          $("#ResponseSuccessModal").modal('show');
          $("#ResponseSuccessModal #ResponseHeading").text(res.message);
        } 
      },
      error: function(data) {
        swal('Error',data,'error');
      }
    });
  });
  $('#formSubmitCSV').click(function(e){
   
    if($('#csvfile').val()==''){
        e.preventDefault();
      swal("","Please upload file","error");
      return false;
    }
  });

  function getstudentDetail(id){
        var path = APP_URL + "/administration/student/getstudentDetail";
        $.ajax({
          type: "POST",
          url: path,
          data: {
            id: id
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);

            if(res.status == 'error'){

            }else{
                 
              var data = $.parseJSON(JSON.stringify(res.message));
              $('#addStudentModal').data('id',data.id);
              $('#addStudentModal').find('.modal-title').html('Update student ' + data.name);
              $('#name').val(data.name);
              $('#email').val(data.email);

              /** WebQuiz changes for Digital Bank merger **/
              
              $('#mobile').val(data.mobile);
               $('#school_name').val(data.school_id).prop('selected', true);
               if(data.school!='')
               {
                 $('#other_school_name').show();
                 $("#other_name").val(data.school);
               }
               else
               {
                  $('#other_school_name').hide();   
               }
              $("#PasswordInput").html('');
              $('#addStudentModal').modal('show');
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
  }



  function getUserID(id){
    var path = APP_URL + "/administration/student/getUserID";
    $.ajax({
      type: "POST",
      url: path,
      data: {
        id: id
      },
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
      success: function(result){
        //console.log(result);
        var res = $.parseJSON(result);

        if(res.status == 'error'){

        }else{
          var data = $.parseJSON(JSON.stringify(res.message));
          $('#ChangePasswordModal').data('id',data.id);
          $('#ChangePasswordModal').find('.modal-title').text('Change Password of Student  ' + data.name);
          $('#ChangePasswordModal').modal('show');
        }
      },
      error: function(){
        alert("Error");
      }
    }); 
  }


  $('#ChangePasswordBtn').click(function(e){
    e.preventDefault();
    
    var id = $('#ChangePasswordModal').data('id');
    var password  = $('#change_password').val();
    var path = APP_URL+'/administration/student/changepassword';
    $.ajax({
      type: 'POST',
      url: path,
      data: {
        id: id,
        password: password
      },
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
      success: function(data) {
        var res = $.parseJSON(data);
        if(res.status == 'error'){
          swal('Error',res.message,'error');
        }else{
          $('#ChangePasswordModal').modal('toggle');
          $('#change_password').val('');
          $('#ChangePasswordModal').data('id','');
         $("#ResponseSuccessModal").modal('show');
         $("#ResponseSuccessModal #ResponseHeading").text(res.message);
        } 
      },
      error: function(data) {
        swal('Error',data,'error');
      }
    });
  });


  function deleteStudent(id){
    var path = APP_URL + "/administration/student/delete";
    var _this = $(this);
    swal({
      title: "Are you sure to delete this student?",
      text: "Your will lost all records of this Student",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        var data = id;
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: data,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               $('.sweet-overlay').remove();
               $('.showSweetAlert ').remove();
              $("#ResponseSuccessModal").modal('show');
              $("#ResponseSuccessModal #ResponseHeading").text(res.message);
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
  }

      
</script>
@endsection


