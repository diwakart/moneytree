@extends('layouts.default')
@section('page_title') 
Assign Quiz
@endsection 
@section('page_css')
<style>

</style>
@endsection
@section('content')

<div class="m-subheader ">
        <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
               Assign Quiz Round to Students
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{url('administration/home')}}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="{{url('administration/quiz')}}" class="m-nav__link">
                        <span class="m-nav__link-text">
                            All Quiz
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                
                <li class="m-nav__item">
                    <a class="m-nav__link">
                        <span class="m-nav__link-text">
                            Assign Quiz To Students
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        </div>
</div>

@if($check_ques==1)
 <input type="hidden" name="round_id" value="{{$publish_round_id}}" id="round_id">
<div class="m-content">
  <div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <span class="m-portlet__head-icon m--hide">
            <i class="flaticon-statistics"></i>
          </span>
          <h3 class="m-portlet__head-text">
            Assign Quiz Round to Students
          </h3>
          <!-- <h2 class="m-portlet__head-label m-portlet__head-label--warning">
            <span>
             Select School
            </span>
          </h2> -->
        </div>
      </div>
    </div>
    <div class="m-portlet__body">
      
      <div class="form-group m-form__group row">
        <div class="col-md-10 col-md-offset-1">
          <select class="form-control select_student">
            <option value="">School</option>
            @foreach($schools as $school)
            <option value="{{$school->id}}">
              {{$school->name}}
            </option>
          
            @endforeach
          </select>
        </div>
      </div>
      <div class="form-group m-form__group row" >
        <div class="col-md-10 col-md-offset-1">
        <button type="button" name="AssignQuizRound" id="AssignQuizRound" class="btn btn-primary" style="float:right">Assign</button>
      </div>
      </div>
    </div>
  </div>
</div>


<div class="m-content" style="position: relative;top: -99px;">
  <div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <span class="m-portlet__head-icon m--hide">
            <i class="flaticon-statistics"></i>
          </span>
          <h3 class="m-portlet__head-text">
            List of School(s) Assigned to this Quiz Round
          </h3>
          <!-- <h2 class="m-portlet__head-label m-portlet__head-label--warning">
            <span>
            Assigned Quiz Round To School(s)
            </span>
          </h2> -->
        </div>
      </div>
    </div>
    <div class="m-portlet__body" >
      <div class="form-group m-form__group row">
        <div class="col-md-12">
         <div class="loader_msg" style='display: block;'>
            <img src="{{ asset('assets/inner/loader.gif') }}" width='132px' height='132px' style="height: 70px;width: 67px;margin-left: 40%;">
          </div>
          <div class="assigned_school_datatable" id="ajax_data"></div>
        </div>
      </div>
    </div>
  </div>
</div>

@else
<div class="m-content">
  <div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h2 class="m-portlet__head-label m-portlet__head-label--warning">
            <span>
            No Published Questions Available for this quiz round.
            </span>
          </h2>
        </div>
      </div>
    </div>
    
  </div>
</div>
@endif


<!-- List of School Students that have assigned  quiz round -->
<div class="modal fade" id="StudentListModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <form name="fm-student">
                <div class="modal-body">
                    <div class="m-section m-section--last">
                      <div class="m-section__content">
                        <!--begin::Preview-->
                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                          <div class="m-demo__preview" style="padding: 0px 21px;">
                            <div class="m-list-search">
                              <div class="m-list-search__results">
                                <span class="m-list-search__result-category">
                                  Student List with mobile Number <p>Number of Students : <span id="TotalStudent"></span></p>
                                </span>
                                <div id="StudentListing">
                                
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--end::Preview-->
                      </div>
                    </div>
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="" style="float: right;margin-bottom: 20px;">
                        Close
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>

<!-- Modal for success response -->
    <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <form name="fm-student">
                <div class="modal-body">
                    <h5 id="ResponseHeading"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadSchoolDatatable">
                        OK
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_script')
<script>

 var datatable;
          (function() {
                $('.loader_msg').css('display','none');
                var view_url = "../../administration/assigned-students";
                datatable = $('.assigned_school_datatable').mDatatable({
                // datasource definition
                data: {
                type: 'remote',
                source: {
                  read: {
                      url: APP_URL+'/administration/getAllAssignedSchools',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                              generalSearch: '',
                          }
                      },
                      map: function(raw) {
                          console.log('ITS RUN');
                          console.log(raw);
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                               dataSet = raw.data;
                          }
                          console.log('Result');
                          console.log(dataSet);
                          return dataSet;
                      },
                  }
                },
                pageSize: 10,
                  saveState: {
                    cookie: false,
                    webstorage: false
                },

                serverPaging: true,
                serverFiltering: false,
                serverSorting: false
            },
          // layout definition
            layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
              input: $('#generalSearch')
            },

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
        field: "S.no",
        title: "S.No",
        width: 100
      },
      {
          field: "school_name",
          title: "School Name",
          textAlign: 'center'
      },
      {
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          field: 'Actions',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

            return '\
            <a href="'+view_url+'/'+row.school_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Student details">\
              <i class="la la-list"></i>\
            </a>\
          ';
          
           /* return '\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Student details">\
              <i class="la la-list" onclick=getAssignedStudentDetail('+row.school_id+')></i>\
            </a>\
          ';*/
          },
        }]
          });


          var query = datatable.getDataSourceQuery();
      
        $("#LoadSchoolDatatable").on('click',function(){
          datatable.reload();
        });

        $("#AssignQuizRound").on('click', function(){
          datatable.reload();
        })

    })();



$("#AssignQuizRound").on('click',function(){
  var school = $(".select_student").val();
  var round_id = $("#round_id").val();
  if(school==''){
    swal('Alert','Select School!','warning');
  }else{
        var path = APP_URL + "/administration/assign_quiz";
        $.ajax({
          type: "POST",
          url: path,
          data: {
            school: school,
            round : round_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
              swal('Success',res.message,'success');
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        }); 
  }
})




  function deleteAssignedSchool(id){
    var path = APP_URL + "/administration/delete_assigned_school";
    var _this = $(this);
    swal({
      title: "Are you sure to delete this row?",
      text: "Your will not be able to recover this row!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        var data = id;
        var round_id = $("#round_id").val();
        console.log(data);
        $.ajax({
          type: 'POST',
          url: path,
          data: {
             school_id: data,
             round_id : round_id,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               $('.sweet-overlay').remove();
               $('.showSweetAlert ').remove();
              $("#ResponseSuccessModal").modal('show');
              $("#ResponseSuccessModal #ResponseHeading").text(res.message);
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
  }



  function getAssignedStudentDetail(id){
        var path = APP_URL + "/administration/getAssignedStudentDetail";
        $.ajax({
          type: "POST",
          url: path,
          data: {
            school_id: id
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);

            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.student));
              var studentHtml = '';

              /** WebQuiz changes for Digital Bank merger **/
              
              $.each(data, function(idx,values){
                      studentHtml += '<a href="javascript:;" class="m-list-search__result-item"><span class="m-list-search__result-item-pic"><img class="m--img-rounded" src="'+values.profile_img+'" title="" style="height: 35px;width: 35px"></span><span class="m-list-search__result-item-text">'+values.name+'</span><span class="m-list-search__result-item-text">'+values.mobile+'</span></a>';
                   
                });
                $("#StudentListModal #StudentListing").html(studentHtml);
                 $("#StudentListModal #TotalStudent").html(res.total_student);
                $('#StudentListModal').modal('show');
              
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
  }



</script>
@endsection
