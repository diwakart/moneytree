@extends('layouts.default')
@section('page_title') 
Assign Prize
@endsection 
@section('page_css')
<style>
.block-display{
      display: inline-flex;
}
#PrizeImg{
    width: 159px;
}
#PrizeTitle{
    font-size: 19px;
    color: #3d3b56;
}
</style>
@endsection
@section('content')

<div class="m-subheader ">
        <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
               Assign Prizes to Quiz Rounds
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{url('administration/home')}}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="{{url('administration/prizes')}}" class="m-nav__link">
                        <span class="m-nav__link-text">
                            All Prizes
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                
                <li class="m-nav__item">
                    <a class="m-nav__link">
                        <span class="m-nav__link-text">
                            Assign Prizes To Round
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        </div>
</div>


<div class="m-content">
  <div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <span class="m-portlet__head-icon m--hide">
            <i class="flaticon-statistics"></i>
          </span>
          <h3 class="m-portlet__head-text">
            Assign Prizes to Rounds
          </h3>
        </div>
      </div>
    </div>
    <div class="m-portlet__body">
      
      <div class="form-group m-form__group row">
        <div class="col-md-10 col-md-offset-1">
          <select class="form-control" id="select_quiz_rounds">
            <option value="">Quiz - Rounds</option>
            @foreach($allRounds as $rounds)
              <option value="{{$rounds->id}}" quiz_id="{{$rounds->quiz_id}}">{{$rounds->title.' - '.$rounds->round_name}}</option>
            @endforeach
           
          </select>
        </div>
      </div>

       <div class="form-group m-form__group row">
        <div class="col-md-10 col-md-offset-1">
          <select class="form-control" id="select_prizes">
            <option value="">Prizes</option>
            @foreach($prizes as $prize)
              <option value="{{$prize->id}}">{{$prize->name}}</option>
            @endforeach
           
          </select>
        </div>
      </div>

      <div class="form-group m-form__group row">
         <div class="col-md-12 " >
          <label id="PrizeTitle"></label>
         </div>
         <div class="col-md-12 block-display">
           <div class="col-md-2 m-card-profile__pic-wrapper">
             <img src="" id="PrizeImg" class="rounded img-thumbnail">
           </div>
           <div class="col-md-5">
             <p id="PrizeDesc"></p>
           </div>
         </div>
      </div>


      <div class="form-group m-form__group row" >
        <div class="col-md-10 col-md-offset-1">
        <button type="button" name="AssignPrize" id="AssignPrize" class="btn btn-primary" style="float:right">Assign</button>
      </div>
      </div>
    </div>
  </div>
</div>


<div class="m-content" style="position: relative;top: -99px;">
  <div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <span class="m-portlet__head-icon m--hide">
            <i class="flaticon-statistics"></i>
          </span>
          <h3 class="m-portlet__head-text">
            List of Prize Assigned to Quiz Rounds
          </h3>
  
        </div>
      </div>
    </div>
    <div class="m-portlet__body" >
      <div class="form-group m-form__group row">
        <div class="col-md-12">
         <div class="loader_msg" style='display: block;'>
            <img src="{{ asset('assets/inner/loader.gif') }}" width='132px' height='132px' style="height: 70px;width: 67px;margin-left: 40%;">
          </div>
          <div class="assigned_prize_datatable" id="ajax_data"></div>
        </div>
      </div>
    </div>
  </div>
</div>



<!-- List of School Students that have assigned  quiz round -->
<div class="modal fade" id="StudentListModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <form name="fm-student">
                <div class="modal-body">
                    <div class="m-section m-section--last">
                      <div class="m-section__content">
                        <!--begin::Preview-->
                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                          <div class="m-demo__preview" style="padding: 0px 21px;">
                            <div class="m-list-search">
                              <div class="m-list-search__results">
                                <span class="m-list-search__result-category">
                                  Student List with mobile <p>Number of Students : <span id="TotalStudent"></span></p>
                                </span>
                                <div id="StudentListing">
                                
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--end::Preview-->
                      </div>
                    </div>
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="" style="float: right;margin-bottom: 20px;">
                        Close
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>

<!-- Modal for success response -->
    <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <form name="fm-student">
                <div class="modal-body">
                    <h5 id="ResponseHeading"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadSchoolDatatable">
                        OK
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_script')
<script>

$("#select_prizes").on('change',function(){
    var prize_id = $(this).val();
     var path = APP_URL + "/administration/get_selected_gift";
        $.ajax({
          type: "POST",
          url: path,
          data: {
            prize_id: prize_id
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);

            if(res.status == 'error'){

            }else{
             var data = $.parseJSON(JSON.stringify(res.message));
              $("#PrizeTitle").text(data.name);
              $("#PrizeDesc").text(data.description);
              if(data.picture==''){
                prize_img = APP_URL+'/assets/student/images/prize.gif';
              }else{
                var prize_img = APP_URL+'/storage/app/'+data.picture;
              }
              $("#PrizeImg").attr('src',prize_img);
              
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
})

$("#AssignPrize").on('click',function(){
   var prize = $("#select_prizes").val();
   var quiz_id = $('#select_quiz_rounds option:selected').attr('quiz_id');
   var round_id = $('#select_quiz_rounds option:selected').val();
   if(prize=='' || round_id=='' ){
    swal('Alert','Select Quiz Round and Prize!','warning');
  }else{
        var path = APP_URL + "/administration/assigning-prizes";
        $.ajax({
          type: "POST",
          url: path,
          data: {
            prize: prize,
            quiz_id : quiz_id,
            round_id : round_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
              swal('Success',res.message,'success');
             $("#select_prizes").val('');
             $('#select_quiz_rounds').val('');
             $("#PrizeTitle").text('');
             $("#PrizeDesc").text('');
             $("#PrizeImg").attr('src','');

            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        }); 
  }
})

 var datatable;
          (function() {
                $('.loader_msg').css('display','none');
                datatable = $('.assigned_prize_datatable').mDatatable({
                // datasource definition
                data: {
                type: 'remote',
                source: {
                  read: {
                      url: APP_URL+'/administration/assign-prizes/getAllAssignedPrizes',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                              generalSearch: '',
                          }
                      },
                      map: function(raw) {
                          console.log('ITS RUN');
                          console.log(raw);
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                               dataSet = raw.data;
                          }
                          console.log('Result');
                          console.log(dataSet);
                          return dataSet;
                      },
                  }
                },
                pageSize: 10,
                  saveState: {
                    cookie: false,
                    webstorage: false
                },

                serverPaging: true,
                serverFiltering: false,
                serverSorting: false
            },
          // layout definition
            layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
              input: $('#generalSearch')
            },

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
        field: "S.no",
        title: "S.No",
        width: 100
      },
      {
          field: "title",
          title: "Quiz Name",
          textAlign: 'center'
      },
      {
          field: "round_name",
          title: "Round Name",
          textAlign: 'center'
      },
      {
          field: "name",
          title: "Prize Title",
          textAlign: 'center'
      },
       {
          field: "description",
          title: "Prize Description",
          textAlign: 'center'
      },
      {
          field: "picture",
          title: "Prize Image",
          textAlign: 'center',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

            if(row.picture=='' || row.picture==null){
              var gift_img = APP_URL+'/assets/student/images/prize.gif';
            }            
            else{
              var gift_url = "{{url('/').'/storage/app/'}}";
              var gift_img = gift_url+row.picture;
              
            }
            return '\
            <img src='+gift_img+' style="width:50px;height:50px;">\
          ';
          },
      },
      {
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          field: 'Actions',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

            return '\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteAssigned('+row.id+')>\
              <i class="la la-trash"></i>\
            </a>\
          ';
          },
        }]
          });


          var query = datatable.getDataSourceQuery();

        var query = datatable.getDataSourceQuery();

       
      
        $('#formSubmit').on('click',function(){
          datatable.reload();
        });

       
      
        $("#LoadSchoolDatatable").on('click',function(){
          datatable.reload();
        });

        $("#AssignPrize").on('click', function(){
          datatable.reload();
        })

    })();


    function deleteAssigned(id){
    var path = APP_URL + "/administration/assign-prizes/delete";
    var _this = $(this);
    swal({
      title: "Are you sure to delete this?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        var data = id;
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: data,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               $('.sweet-overlay').remove();
               $('.showSweetAlert ').remove();
              $("#ResponseSuccessModal").modal('show');
              $("#ResponseSuccessModal #ResponseHeading").text(res.message);
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
  }


</script>
@endsection
