@extends('layouts.default')
@section('page_title') 
Add Prize
@endsection 
@section('page_css')
<style>
#ResponseHeading{
 color: #4CAF50;
}
</style>
@endsection
@section('content')

<div class="m-subheader ">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="m-subheader__title m-subheader__title--separator">
        Prizes
      </h3>
      <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
        <li class="m-nav__item m-nav__item--home">
          <a href="{{url('administration/home')}}" class="m-nav__link m-nav__link--icon">
            <i class="m-nav__link-icon la la-home"></i>
          </a>
        </li>
        <li class="m-nav__separator">
          -
        </li>
        <li class="m-nav__item">
          <a href="{{url('administration/prizes')}}" class="m-nav__link">
            <span class="m-nav__link-text">
              Manage Prizes
            </span>
          </a>
        </li>
        <li class="m-nav__separator">
          -
        </li>
        <li class="m-nav__item">
          <a href="{{url('administration/add-prizes')}}" class="m-nav__link">
            <span class="m-nav__link-text">
              Add Prizes
            </span>
          </a>
        </li>

      </ul>
    </div>
  </div>
</div>

  <div class="m-portlet" style="margin-top:20px">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
              <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
              Add Prize
            </h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ asset('administration/prizes/store') }}" method="post" enctype = "multipart/form-data">
        {{ csrf_field() }}
        @if(isset($prize) && $prize!='')
         @php($gift_id = $prize->id)
         @php($gift_title = $prize->name)
         @php($gift_desc = $prize->description)
          @php($gift_points = $prize->points)
         @if($prize->picture=='')
          @php($gift_img = asset('assets/student/images/prize.gif'))
         @else
          @php($gift_img = url('/').Storage::url('app/'.$prize->picture))
         @endif
         

        @else
          @php($gift_id = '')
          @php($gift_title = '')
          @php($gift_desc = '')
           @php($gift_points = '')
          @php($gift_img = asset('assets/student/images/prize.gif'))
        @endif
       
         <input type="hidden" name="id" value="{{$gift_id}}">
        <div class="m-portlet__body">
          @if(Session::has('errormessage'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                      <strong>
                          Oh snap!
                      </strong>{{ Session::get('errormessage') }}
                    </div>
                    @endif

                    @if(Session::has('successmessage'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                      <strong>
                          Well done!
                      </strong>{{ Session::get('successmessage') }}
                  </div>
              @endif

          <div class="form-group m-form__group row">
            <label class="col-lg-2 col-form-label">
              Title:
            </label>
            <div class="col-lg-6">
              <input type="text" class="form-control m-input" placeholder="Enter Prize Title" name="gift_title" id="gift_title" value="{{$gift_title}}">
              <span class="m-form__help">
                Please enter prize title
              </span>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <label class="col-lg-2 col-form-label">
              Description:
            </label>
            <div class="col-lg-6">
              <textarea class="form-control m-input" placeholder="Enter Prize Description" id="prize_desc" name="prize_desc">{{$gift_desc}}</textarea>
              <span class="m-form__help">
                Please enter prize description
              </span>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <label class="col-lg-2 col-form-label">
              Image:
            </label>
            <div class="col-lg-6">
             <input type="file" class="form-control m-input" placeholder="Select Prize Image" name="gift_img" id="gift_img" accept="image/x-png,image/jpeg">
              <span class="m-form__help">
                Please select image
              </span>
            </div>
            <div class="m-card-profile__pic col-lg-4">
                <div class="m-card-profile__pic-wrapper">
                    <img src="{{$gift_img}}" alt="" class="previewHolder" style="    height: 80px;width: 80px;" />
                </div>
            </div>
          </div>
          
          <div class="form-group m-form__group row">
            <label class="col-lg-2 col-form-label">
              Number of Points:
            </label>
            <div class="col-lg-6">
              <input type="number" min=0 class="form-control m-input" placeholder="Enter Point(s)" name="gift_point" id="gift_point" value="{{$gift_points}}">
              <span class="m-form__help">
                Please enter points
              </span>
            </div>
          </div>
          
        </div>
        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
          <div class="m-form__actions m-form__actions--solid">
            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-6">
                <button type="submit" class="btn btn-brand" id="ValidateButn">
                  Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                  Cancel
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>
      <!--end::Form-->
    </div>
@endsection

@section('page_script')

<script>
    function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.previewHolder').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#gift_img").change(function() {
  readURL(this);
});


$("#ValidateButn").click(function(){
  
  if($("#gift_title").val()=='') {
    swal("Error","Title must be filled","error");
    return false;
  }
  if(($("#prize_desc").val()=='') && ($("#gift_point").val()=='')) {
    swal("Error","Description and Image or Point must be filled","error");
    return false;
}

if(($("#prize_desc").val()!='' || $("#gift_img").val()!='') && ($("#gift_point").val()!='')) {
  swal("Error","Only Description and Image or Point must be filled","error");
  return false;
}
});

// $("#ValidateButn").click(function(){
      
//       if($("#gift_title").val()=='' || $("#prize_desc").val()==''){
//         swal("Error","Title and Description must be filled","error");
//         return false;
//       }
//     })
</script>
@endsection


