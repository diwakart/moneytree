@extends('layouts.student')
@section('page_title')
Rank
@endsection
@section('page_css')
<link href="{{ asset('assets/student/css/enjoyhint.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('content')

<input type="hidden" id="url_param" value="{{$round}}">

<div class="s_main"> 
   <input type="hidden" id="currentuser" value="{{Auth::user()->id}}">
<section class="quiz_div"> 
    <div class="np">
    <div class="row">
        <div class="col-xs-5"><div class="quiz_title">
            <img src="{{asset('assets/student/images/left_triangle.jpg')}}">
            <h3 class="">Your Rank</h3></div>
          </div>  
        <div class="col-xs-7 rank_btn text-right"> 
         <button type="button" class="button_1" id="QuizRanking">Quiz Ranking</button>
         <button type="button" class="button_1" id="QuizRoundRanking" style="display: none;">Quiz-Round Ranking</button>
               
        </div> 
    </div>
  </div></section>
 
   
  <section class="tune_talk_history_block">
    <div class="row">
        <div class="col-xs-12">
            <div class="question_div">

                <div class="row s_rank" id="QuizRoundSelect">
                  <div class="question_block"> 
                    <h3>Select Quiz Round</h3>
                  </div>
                  <div class="question_start clearfix quiz_ranking" >
                    <select class="" id="SelectBoxQuiz">
                      <option value="">Select Quiz-Round</option>
                    @if(isset($declared_quiz) && $declared_quiz!='')
                    @foreach($declared_quiz as $quiz)
                    <option value="{{$quiz->round_id}}" school="{{$quiz->school_id}}">{{$quiz->quiz_name.' : '.$quiz->round_name}}</option>
                    @endforeach
                    @endif
                   </select> 
                  </div>
                 </div>

                 <div class="row s_rank" id="QuizSelect" style="display: none;">
                  <div class="question_block"> 
                    <h3>Select Quiz</h3>
                  </div>
                  <div class="question_start clearfix quiz_ranking" >
                    <select class="" id="SelectQuizBoxQuiz">
                      <option value="">Select Quiz</option>
                      @if(isset($declared_quiz_name) && $declared_quiz_name!='')
                      @foreach($declared_quiz_name as $quiz_name)
                      <option value="{{$quiz_name->quiz_id}}" school="{{$quiz_name->school_id}}">{{$quiz_name->title}}</option>
                      @endforeach
                      @endif
                   </select> 
                  </div>
                 </div>

                
                <div class="s_table table-responsive" id="RankDetail" style="display: none;">
                <p>Quiz-Round Ranking Board</p>
                <div class="fortable">
                <table class="table border0 mb50">
                    <thead>
                    <tr>
                        <th>Rank</th>
                        <th>Name</th>
                        <th>Points</th>
                        <th>Time</th>
                        </tr>
                    </thead>
                    <tbody id="RankRows">
                    </tbody>
                    </table>
</div>
                    <div class="col-sm-12 stn-sn text-center">
                      <h2 id="studentschool">
                       
                      </h2>
                    </div>
                </div>


                <div class="s_table table-responsive" id="QuizRankDetail" style="display: none;">
                <p>Quiz Ranking Board</p>
                 <div class="fortable">
                <table class="table border0 mb50" id="QuizRankTble">
                    <thead>
                    <tr>
                        <th>Rank</th>
                        <th>Name</th>
                        <th>Points</th>
                        <th>Quiz Duration</th>
                        </tr>
                    </thead>
                    <tbody id="QuizRankRows">
                  </tbody>
                    </table>
</div>
                    <div class="col-sm-12 stn-sn text-center">
                      <h2 id="studentschool">
                       
                      </h2>
                    </div>
                </div>

                   
            </div> 
            </div>
        </div> 
         
    </section>
 
</div>
      
@endsection



@section('page_js')

<script>
  var round = $("#url_param").val();
  if(round!=''){
     $("#SelectBoxQuiz").val(round);
     var round_id = round;
     var school_id = $('option:selected', $("#SelectBoxQuiz")).attr('school');
     var path = APP_URL + "/student/selected_quiz_rank";
      $.ajax({
          type: "POST",
          url: path,
          data: {
            round_id: round_id,
            school_id: school_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            console.log(result);
            var res = $.parseJSON(result);
           
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.student));
              var myrank = $.parseJSON(JSON.stringify(res.myRank));
              var studentHtml = '';
              var student_own='';

              $.each(data, function(idx,values){
                      if($("#currentuser").val()==values.userid){
                         studentHtml += '<tr class="rank-row"><td>'+values.rank+'</td><td>'+values.name+'</td><td>'+values.final_score+'</td><td>'+values.duration+'</td></tr>';
                       }else{
                         studentHtml += '<tr><td>'+values.rank+'</td><td>'+values.name+'</td><td>'+values.final_score+'</td><td>'+values.duration+'</td></tr>';

                       }
                     
              });
              
             
              $("#RankRows").html(studentHtml);
              
              if($('#RankRows tr').hasClass('rank-row')){
                
              }else{
                 student_own = '<tr class="rank-row"><td>'+myrank[0]['rank']+'</td><td>'+myrank[0]['name']+'</td><td>'+myrank[0]['final_score']+'</td><td>'+myrank[0]['quiz_duration']+'</td></tr>';
              $('#RankRows').append(student_own);
              }
              $("#RankDetail").css('display','block');
             
            }
          },
          error: function(){
            alert("Error");
          }
        });
  }

 $("#SelectBoxQuiz").on('change',function(){
  if($('option:selected', this).val()!=''){
   var round_id =  $('option:selected', this).val();
   var school_id =  $('option:selected', this).attr('school');
   var path = APP_URL + "/student/selected_quiz_rank";
   $.ajax({
          type: "POST",
          url: path,
          data: {
            round_id: round_id,
            school_id: school_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            console.log(result);
            var res = $.parseJSON(result);
           
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.student));
              var myrank = $.parseJSON(JSON.stringify(res.myRank));
              var studentHtml = '';
              var student_own='';

              
               $.each(data, function(idx,values){
                      if($("#currentuser").val()==values.userid){
                         studentHtml += '<tr class="rank-row"><td>'+values.rank+'</td><td>'+values.name+'</td><td>'+values.final_score+'</td><td>'+values.duration+'</td></tr>';
                       }else{
                         studentHtml += '<tr><td>'+values.rank+'</td><td>'+values.name+'</td><td>'+values.final_score+'</td><td>'+values.duration+'</td></tr>';

                       }
                     
              });
              
             
              $("#RankRows").html(studentHtml);
              
              if($('#RankRows tr').hasClass('rank-row')){
                
              }else{
                  if(myrank!=''){
                      if(myrank[0]['attended']==2){
                          student_own = '<tr class="rank-row-loser"><td>'+myrank[0]['rank']+'</td><td>'+myrank[0]['name']+'</td><td>'+myrank[0]['final_score']+'</td><td>'+myrank[0]['quiz_duration']+'</td></tr>'; 
                      }else{
                          student_own = '<tr class="rank-row-not-participated"><td colspan=4>You did not participate in this round.</td></tr>'; 
                      }
                        
                         
              $('#RankRows').append(student_own);
                  }
                
              }
              $("#RankDetail").css('display','block');
             
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
    }else{
         swal('Error','Select Assigned Quiz Name','error');
          $("#RankDetail").css('display','none');
              $('#studentrank').html('');
              $("#studentscore").text('');
              $("#studentschool").text('');
    }
  
 })


 $("#QuizRanking").on('click',function(){
  $("#SelectBoxQuiz").val('');
  $("#QuizRanking").css('display','none');
  $("#QuizRoundSelect").css('display','none'); 
  $("#RankDetail").css('display','none');

  $("#QuizSelect").css('display','block');
  $("#QuizRoundRanking").css('display','block');
 })

  $("#QuizRoundRanking").on('click',function(){
    $("#QuizRoundRanking").css('display','none');
    $("#QuizRankDetail").css('display','none');
    $("#QuizSelect").css('display','none');
    $("#QuizRanking").css('display','block');
     $("#QuizRoundSelect").css('display','block');

 })



 $("#SelectQuizBoxQuiz").on("change",function(){
    if($('option:selected', this).val()!=''){
      var quiz_id =  $('option:selected', this).val();
      var school_id =  $('option:selected', this).attr('school');
      var path = APP_URL + "/student/selected_quiz_overall_rank";
      $.ajax({
          type: "POST",
          url: path,
          data: {
            quiz_id: quiz_id,
            school_id: school_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            console.log(result);
            if(result=='' || result=='undefined'){
              
               var no_result_tr = '<tr ><td colspan="4">No Result Available</td></tr>';
                $('#QuizRankRows').html('');
              $('#QuizRankRows').append(no_result_tr);
                
            }else{
             var res = $.parseJSON(result);
           
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.student));
              var myownrank = $.parseJSON(JSON.stringify(res.myownrank));
             
             

              
              var studentHtml = '';
              var student_own = '';
              var count = 1;
              $.each(data, function(idx,values){

                  if($("#currentuser").val()==values.student_id){
                      
                     studentHtml += '<tr  class="rank-row "><td id="ranking'+count+'" >'+values.rank+'</td><td>'+values.name+'</td><td>'+values.score+'</td><td>'+values.quiz_duration+'</td></tr>';

                  }else{
                       studentHtml += '<tr><td id="ranking'+count+'"  >'+values.rank+'</td><td>'+values.name+'</td><td>'+values.score+'</td><td>'+values.quiz_duration+'</td></tr>';
                  }
                      
                  

                    count++;  
                     
              });

              $("#QuizRankRows").html(studentHtml);
            

              
              if($('#QuizRankRows tr').hasClass('rank-row')){
                
              }else{
            if(myownrank!=''){
                student_own = '<tr  class="rank-row-loser"><td></td><td>'+myownrank[0]['username']+'</td><td>'+myownrank[0]['score']+'</td><td>'+myownrank[0]['format_duration']+'</td></tr>';
                $('#QuizRankRows').append(student_own);
            }   
                 
              
              }
              
              $("#QuizRankDetail").css('display','block');
              $("#SelectQuizBoxQuiz").val('');
              
             
            } 
            }
            
          },
          error: function(){
            alert("Error");
          }
        }); 
    }else{
      swal("Error","Select Quiz","error")
    }
   
 })

</script>
@endsection
