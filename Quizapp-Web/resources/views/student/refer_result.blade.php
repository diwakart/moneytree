@extends('layouts.student')
@section('page_title')
Result
@endsection
@section('page_css')
<link href="{{ asset('assets/student/css/enjoyhint.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('content')


<div class="s_main"> 
<section class="quiz_div">  
    <div class="row">
        <div class="col-xs-12">
            <div class="quiz_title">
            
            <img src="{{ asset('assets/student/images/left_triangle.jpg') }}" />

            <div class="tune_talk_history">
            <h4 class=""><span>Refered Quiz - {{$result->quizroundname}} Result |</span> {{ ucfirst(Auth::user()->name) }} </h4> 
                </div>
            </div>
            <div class="tankTune">
               
                    <p>To reload your Tune Talk Sim Card, Click Here : </p>
                 <img src="{{asset('assets/student/images/icons/new_logo.png')}}">
                </div>
        </div> 
        
    </div>
  </section>
 
   
     <section class="tune_talk_history_block">
    <div class="row">
        <div class="col-xs-9">
             <div class="question_div thankDiv">
                <div class="question_block text-right"> 
                    <h5>Answered Correctly</h5>
                        <h3>{{$result->score}}/{{$result->total_question}}</h3>
                  <div class="symobol"><i class="fa fa-check"></i></div>  
            </div>                
                          
            </div> 
             <div class="question_div thankDiv">
                <div class="question_block text-right"> 
                    <h5>Score</h5>
                     @php($precentage_resul = ($result->score/$result->total_question)*100)
                        <h3>{{number_format((float)$precentage_resul, 2, '.', '')}}%</h3>
                  <div class="symobol"><i class="fa fa-thumbs-up"></i></div>  
            </div>                
                          
            </div> 
             <div class="question_div thankDiv">
                <div class="question_block text-right"> 
                    <h5>Time Spent</h5>
                        <h3>{{$result->quiz_duration}}</h3>
                  <div class="symobol"><i class="fa fa-clock-o"></i></div>  
            </div>                
                          
            </div> 
            </div>
             
         <div class="col-xs-3">
        </div>
        </div> 
         <div class="row pdt30">
        <div class="col-xs-12 text-center">
            <button class="button_1" id="GoHome">Home</button>
        </div>
         </div>
    </section>
        
     
</div>
      
@endsection



@section('page_js')
<script>
 $("#GoHome").on('click',function(){
        window.location.href= APP_URL+"/student/home";
    })

</script>
@endsection
