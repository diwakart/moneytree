<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{'Web Quiz Application | Quiz'}}</title>

<link href="{{ asset('assets/student/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/student/css/style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/student/css/responsive.css') }}" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
<link rel="shortcut icon" href="{{ asset('assets/logo/favicon.ico') }}" />

<script>
   var APP_URL = {!! json_encode(url('/')) !!} 
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<style>
ul#SortAnswer li { 
    background: #fff; 
    text-align: left;
    margin-bottom: 0;
}
ul.list-group.checked-list-box li {
    font-size: 14px;
}
.quiz_note{
      text-align: left;
    padding-left: 15px;
    color: #ed2f23;
}
.list-group.checked-list-box {
    text-align: left;
    padding-left: 15px;
    margin-bottom: 5px;
}

.display_block{
  display:block;
}
.display_inline{
  display: inline;
}
.display_none{
  display: none;
}
.animationload {
    background-color: transparent;
    height: 100%;
    left: 0;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 10000;
    display: none;
}
.osahanloading {
    animation: 1.5s linear 0s normal none infinite running osahanloading;
    background: #fed37f none repeat scroll 0 0;
    border-radius: 50px;
    height: 50px;
    left: 50%;
    margin-left: -25px;
    margin-top: -25px;
    position: absolute;
    top: 50%;
    width: 50px;
}
.osahanloading::after {
    animation: 1.5s linear 0s normal none infinite running osahanloading_after;
    border-color: #85d6de transparent;
    border-radius: 80px;
    border-style: solid;
    border-width: 10px;
    content: "";
    height: 80px;
    left: -15px;
    position: absolute;
    top: -15px;
    width: 80px;
}
    #GoHome span {
    margin-right: 15px;
    font-size: 15px;
    text-transform: uppercase;
    color: #ed2f23;
    font-weight: bold;
}
    .quiz-bot {
    text-align: center;
}
    
    .s_main {
    padding-bottom: 130px;
    }
    
    .my_footer {
   border: none;
    padding: 10px 0;
    background: transparent;
    }
    .my_footer ul {
    margin-bottom: 0;
    padding-top: 10px;
    margin-top: 10px;
    border-top: 1px solid #dcdada;
}
@keyframes osahanloading {
0% {
    transform: rotate(0deg);
}
50% {
    background: #85d6de none repeat scroll 0 0;
    transform: rotate(180deg);
}
100% {
    transform: rotate(360deg);
}
}

</style>
</head>

<body onkeydown="return showKeyCode(event)" >

<div class="container">
  <div class="row"> 
  <div class="animationload">
   <div class="osahanloading"></div>
  </div>
  </div>
</div>


<section class="main-head clearfix">
  <div class="container-fluid">
    <div class="row ds-flex">
    <div class="col-sm-2 col-xs-5 logo">
      <div>
        <a href="#"><img src="{{ asset('assets/logo/logo.png') }}" class="img-responsive"></a>
      </div>
    </div>
    <div class="col-sm-10 col-xs-7 top-right main-head-right">
      <div class="row">
        <div class="col-sm-6 hidden-xs">
          <div class="welcome">
            <h2>Welcome</h2>
            <span>|</span>
            <h3>{{ ucfirst(Auth::user()->name) }}</h3>
          </div>
        </div>
        <div class="col-sm-6 text-right main-head-profile">
          <ul>
            <li><a href="javascript:;" id="GoHome"><span>Quit</span></a></li>
          </ul>
        </div>
      </div>
    </div>
    </div>
  </div>
</section>

<div class="s_main"> 
  <section class="quiz_div">  
    <div class="row">
      <div class="col-xs-8">
        <div class="quiz_title">
          <img src="{{ asset('assets/student/images/left_triangle.jpg') }}" />
          <div class="tune_talk_history">
            <h4 class="">{{$school->name}}</span></h4>
            <h5 class="">{{$quiz->title}} - <span>{{$quiz_round->round_name}}</span></h5>
            <h5 class="">Total Questions - <span>{{$totalquestions}}</span></h5>
             <input type="hidden" name="auth_id" id="auth_id" value="{{Auth::user()->id}}">
             <input type="hidden" name="quiz_round_name" id="quiz_round_name" value="{{$quiz_round->round_name}}">
             <input type="hidden" name="quiz_name" id="quiz_name" value="{{$quiz->title}}">
          </div>
        </div>
      </div> 
      <div class="col-xs-4">
        <div class="gradient">
          <h3 id="timershow"><time>00:00:00.00</time></h3>
        </div>
      </div>
    </div>
  </section>


  <section class="tune_talk_history_block">
    <div class="row">
      <div class="col-xs-12">
        <form id="SubmitQuizForm" onsubmit="return false">
          <div class="quiz-list quiz-option">

           
            @inject('answers','App\PublishAnswers')
            @foreach($questions as $question)
            <div class="tab" id="tab_{{$question->id}}" >

            <span style="display: none;" class="que_ids" id="{{$question->id}}"></span>
           
            @php($exploded_para= Crypt::decrypt(request()->route('hashed_section')))
            @php($exploded_parameter= explode('^',$exploded_para))
            @php($assign_quiz_id = $exploded_parameter[2])
             <input type="hidden" id="assign_round_id" value="{{$assign_quiz_id}}">
           
            <input type="hidden" id="question_type" value="{{$question->question_type}}">
            <input type="hidden" name="question_id" id="question_id" value="{{$question->id}}">
            <input type="hidden" name="quiz_round_id" id="quiz_round_id" value="{{$quiz_round->id}}">

            <input type="hidden" name="school_id" id="school_id" value="{{$school->id}}">
            <section class="tune_talk_history_block">
              <div class="row">
                <div class="col-xs-12">
                  <div class="question_div">

                    <div class="question_block"> 
                      <h3>{{htmlspecialchars_decode($question->question)}}</h3>
                    </div>
                    @if($question->question_type==1)
                      @inject('fill_answers','App\PublishAnswers')
                      @php($fillanswers = $fill_answers->where('question_id','=',$question->id)->get())
                      <div class="question_start" id="AnswersDiv">
                        @php($ans_no  = 1)
                       @foreach($fillanswers as $answer)
                        <div class="form-group">
                          <input type="text" placeholder="Write Your Answer Here..." class="textbox" name="{{$answer->id}}" id="answer{{$ans_no}}" />
                        </div>
                        @php($ans_no++)
                        @endforeach 
                      </div>

                    @elseif($question->question_type==2)
                      @inject('answers','App\PublishAnswers')
                      @php($answers = $answers->where('question_id','=',$question->id)->get())
                      <ul class="question_start clearfix quesDiv" >
                      @php($chk_count = 1)
                      @foreach($answers as $answer)
                      <li class="custom_checkbox">
                        <h4>{{$answer->option_text}}</h4>
                        <input class="styled-checkbox" id="{{'option-'.$chk_count.'-'.$question->id.'mcq'}}" type="checkbox" value="">
                        <label for="{{'option-'.$chk_count.'-'.$question->id.'mcq'}}" onclick="CheckCorrectAns($(this).attr('for'))"></label> 
                      </li>
                      @php($chk_count++)
                      @endforeach
                    </ul>

                    @elseif($question->question_type==3)
                      @inject('orders','App\PublishAnswers')
                      @php($arrange_order = $orders->where('question_id','=',$question->id)->get())
                      <div class="question_start clearfix">
                      <ul class="list-group checked-list-box" style="font-size: 17px;">
                       @php($i=1)
                        @foreach($arrange_order as $ordr)
                          <li class="">{{$i.' : '.$ordr->option_text}}
                          </li>
                          @php($i++)
                         @endforeach
                     </ul>
                     <p class="quiz_note">Drag lists to arrange in order</p>
                     <ul id="SortAnswer" class="sort_order">
                        @php($j = 1)
                        @foreach($arrange_order as $ordr)
                          <li class="custom_checkbox" id="sort_list{{$j.'-'.$question->id.'arr'}}" style="cursor: pointer;">{{$ordr->option_text}}
                          </li>
                           @php($j++)
                        @endforeach 
                     </ul>      
                    </div>

                    @endif
                    


                  </div> 
                </div>
              </div>
            </section>
            </div>
            @endforeach

            
          </div>
          <div> 
            <span class="step"></span> <span class="step"></span> 
          </div>
        </form>
      </div>
    </div> 
  </section>
</div>


  <section class="my_footer">
    <div class="quiz-bot"> 
      <div class="row">
        <div class="col-xs-12 text-center tune_talk_history_block">
          <button type="button" id="prevBtn" class="button_2" onclick="nextPrev(-1)">Previous</button>
          <button type="button" id="nextBtn" class="button_1" onclick="nextPrev(1)">Next</button>
          <!--<p>Something about this Quiz does not feel right?</p>-->
          <!--<a id="feedback_url">Give Feedback</a>-->
        </div>
      </div>
    </div>
    <ul class="list-inline">
      <li>Brought to you by :</li>
      <li><img src="{{ asset('assets/student/images/icons/new_logo.png') }}" width="50" /></li>
      <li><img src="{{ asset('assets/student/images/icons/mt_logo.png') }}"width="100"  /></li>
    </ul> 
  </section>
    
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!--<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('assets/student/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/student/js/quiz.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script src="{{ asset('assets/student/js/jquery.ui.touch-punch.js') }}"></script>

<script>

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();


    

$(".sort_order").sortable({
    stop : function(event, ui){
}
});

function CheckCorrectAns(check_id){
  if($("#"+check_id).val()=='1'){
     $("#"+check_id).val('0');
     $("#"+check_id).parent().removeClass('active');
  }else{
     $("#"+check_id).val('1');
     $("#"+check_id).parent().addClass('active');
  }
}


$(document).keypress(function(e) {
    var keycode = (e.keyCode ? e.keyCode : e.which);
    if (keycode == '13') {
      $("#nextBtn").click();
    }
});




</script>
</body>
</html>
