@extends('layouts.student')
@section('page_title')
Quiz-Round
@endsection
@section('page_css')
<link href="{{ asset('assets/student/css/enjoyhint.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('content')

@php($currentdate = date('Y-m-d', time()))
<div class="s_main"> 
<section class="quiz_div">  
    <div class="row">
        <div class="col-xs-12"><div class="quiz_title">
            <img src="{{ asset('assets/student/images/left_triangle.jpg') }}" />
            <h3 class="">{{$quiz_name->title}} - Rounds</h3></div></div>
    </div>
</section>
 
<style>
    .mb-10
    {
        margin-bottom:10px;
    }
</style>
<section class="tune_talk">
  @if(isset($assigned_rounds) && $assigned_rounds!='')
   @foreach($assigned_rounds as $rounds)
    @if($currentdate<$rounds->expiration_date)
    <div class="row">
        <div class="col-xs-12">
            <div class="question_div before100">
                <div class="question_block"> 
                    <h3>{{$rounds->roundname}}</h3>
                    
                    @inject('publish_questions','App\PublishQuestions')
                    @php($count_qustions = $publish_questions->where(['quiz_round_id'=>$rounds->quizroundid,'published'=>1])->count())
                    <h4 class="tune_title">Total Questions <span>{{$count_qustions}}</span></h4>
                  </br>
                  <h4 class="tune_title">Round Expiration Date <span style="font-size: 15px;color: #ed2f23;">{{date("F jS, Y", strtotime(date('Y-m-d H:i:s',strtotime($rounds->expiration_date))))
                    }}</span></h4>
                   </br>
                  <h4 class="tune_title">Result Date <span style="font-size: 15px;color: #ed2f23;">{{date("F jS, Y", strtotime(date('Y-m-d H:i:s',strtotime($rounds->expiration_date . "+1 days"))))
                    }}</span></h4>
                   </br>
                   <!-- @if($rounds->earn_points==1)-->
                   <!--   <h4 class="tune_title" id="ReferalPoint">Referal Point <span>{{$rounds->earn_points}}</span></h4>-->
                   <!-- @endif-->
                   <!--<h4 class="tune_title" style="display: none;" id="ShowReferral">Referal Point <span>1</span></h4>-->
                  
                </div>
                <ul class="question_start clearfix">
                    <li><button type="button" class="button_1" onclick="AttendQuiz({{$rounds->id}})" >START QUIZ</button></li>
                    <!--<li><button type="button" class="button_2 mb-10" onclick="ReferStudent({{$rounds->round_id}},{{$rounds->id}})" >REFER A FRIEND</button></li>-->
                </ul>
                <div class="tune_talk_bottom">
                  @inject('assigned_prize','App\AssignPrize')

                  @php($count_assign_prize = $assigned_prize->where(['publish_round_id'=>$rounds->round_id])->count())
              
                  @if($count_assign_prize>0)  
                  @php($assign_prize = $assigned_prize->where(['publish_round_id'=>$rounds->round_id])->first())

                  @inject('prize','App\Prizes')
                     @php($prize = $prize->where(['id'=>$assign_prize->prize_id])->first()) 
                      

                      @if($prize->picture=='')
                        @php($gift_img = asset('assets/student/images/prize.gif'))
                       @else
                        @php($gift_img = url('/').Storage::url('app/'.$prize->picture))
                       @endif
                       <div class="prize-div">
                           <p class="prize-name">Prize for this Round : {{$prize->name}}</p>
                           <p class="prize-desc">Tap on picture for more prize information.</p>
                       </div>
                    <figure class="snip0016">
                        <img src="{{$gift_img}}" class="img-responsive" /> 
                        <figcaption>
                            <h2>{{$prize->name}}</h2>
                            <p>{{$prize->description}}</p> 
                        </figcaption>			
                    </figure>
                  
                  @else
                    <figure class="snip0016">
                        <img class="content-image" src="{{ asset('assets/student/images/thumb.png') }}">
                        <figcaption>
                            <h2>No Gift Available</h2>
                            <p></p> 
                        </figcaption>			
                    </figure>
                  
                  @endif
                    
                </div> 
            </div> 
        </div>
    </div>

     @endif
    @endforeach
    @else
       <div class="row">
        <div class="col-xs-12">
            <div class="question_div before100">
                <div class="question_block"> 
                    <h3>No Rounds Avaialble</h3>
               
                </div> 
            </div> 
        </div>
    </div>
    @endif


</section>
     
   
</div>



<!-- Modal for Refer a friend -->
      <div class="modal fade" id="ReferFriendModal" role="dialog">
        <div class="modal-dialog"> 
          
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Refer a Friend</h4>
            </div>
            <div class="modal-body">
              <input type="hidden" name="refered_by" id="refered_by" value="{{Auth::user()->id}}">
              <input type="hidden" name="refer_round_id" id="refer_round_id" value="">
              <input type="hidden" name="refer_assigned_id" id="refer_assigned_id" value="">
              <div class="row quiz-result-data">
                <div class="clearfix">

                  <div class="form-group clearfix">
                    <label class="col-md-4">Name</label>
                    <div class="col-md-8">
                      <input type="text" name="refered_name" id="refered_name" class="form-control">
                    </div>
                  </div>

                  <div class="form-group clearfix">
                    <label class="col-md-4">Country Code</label>
                    <div class="col-md-8">
                      <select class="form-control" id="refered_country_code" name="refered_country_code">
                      <option value="">Select Country Code
                      </option>
                      @foreach($countries as $key => $value)
                      <option value="{{$key}}" @if($key== '+60') {{ 'selected' }}@endif>{{$value}}</option>
                      @endforeach
                  </select>
                    </div>
                  </div>

                  <div class="form-group clearfix">
                    <label class="col-md-4">mobile Number</label>
                    <div class="col-md-8">
                      <input type="number" name="refered_mobile_number" id="refered_mobile_number" class="form-control">
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button  type="button" class="btn sel-quiz-btn" id="ReferFriendBtn"><i class="fa fa-share"></i>&nbsp; Share</button>
            </div>
          </div>
        </div>
      </div>


@endsection



@section('page_js')
<script>

  function ReferStudent(roundid,id){
    $("#ReferFriendModal").modal('show');
    $("#refer_round_id").val(roundid);
    $("#refer_assigned_id").val(id);
    $("#refered_mobile_number").val('');
    $("#refered_name").val('');
   

  }


  $("#ReferFriendBtn").on('click',function(){
    var id = $("#ReferFriendModal #refer_round_id").val();
    var assign_id = $("#ReferFriendModal #refer_assigned_id").val();
    var refered_by = $("#ReferFriendModal #refered_by").val();
    var refered_to_name = $("#ReferFriendModal #refered_name").val();
    var refered_to_mobile = $("#ReferFriendModal #refered_mobile_number").val();
    var refered_country_code = $("#refered_country_code").val();
    $.ajax({
            type: 'POST',
            url: APP_URL+'/student/refer_quiz',
            data: {
              round_id: id,
              refered_by: refered_by,
              name: refered_to_name,
              mobile: refered_to_mobile,
              refered_country_code: refered_country_code,
              assign_id: assign_id,
            },
             headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            success: function(data) {
              var res = $.parseJSON(data);
              if(res.status == 'error'){
                swal('Error',res.message,'error');
              }else{
                swal('Success',res.message+'. Thank you for referring a friend. 1 Point has been awarded to you for this round for your first referral .','success');
                $("#ReferFriendModal").modal('hide');
                $("#refer_round_id").val('');
                $("#refer_assigned_id").val('');
                $("#refered_mobile_number").val('');
                $("#refered_name").val('');
                if($("#ReferalPoint").length == 0) {
                  $("#ShowReferral").css('display','block');
                 }else{
                    $("#ShowReferral").css('display','none');
                 }

              } 
            },
            error: function(data) {
              swal('Error',data,'error');
            }
          });
  })


function AttendQuiz(id){
    var id = id;
    if(id==''){
          swal("Error","Some error occured","error");
      }else{
        console.log(id);
          $.ajax({
            type: 'POST',
            url: APP_URL+'/student/quiz_attended',
            data: {
              id: id
            },
             headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            success: function(data) {
              var res = $.parseJSON(data);
              if(res.status == 'error'){
                swal('Error',res.message,'error');
              }else{
                window.location.href = APP_URL + "/student/quiz/"+res.parameter_enc;
              } 
            },
            error: function(data) {
              swal('Error',data,'error');
            }
          });
      }
  }
</script>
@endsection
