@extends('layouts.student')
@section('page_title')
Result
@endsection
@section('page_css')

@endsection

@section('content')

<section class="main-menu clearfix">
  <div class="container">
    <div class="row">
      <div class="menu-item text-center">
        <ul class="pd-b-0" >

          <li> <a href="{{route('quizes')}}"> <span><img src="{{ asset('assets/student/images/icons/monitor-task.svg') }}"></span>
            <p>Quiz</p>
            </a> 
           </li>
      
          <li> <a href="{{route('student-rank')}}"> <span><img src="{{ asset('assets/student/images/icons/rank.svg') }}"></span>
            <p>Rank</p>
            </a> 
          </li>
        </ul>
        <div class="text-right add-img"> 
            <p>Brought to you by:</p>
            <a href="javascript:;"> <span><img src="{{ asset('assets/student/images/icons/new_logo.png') }}"></span>            
            </a> 
        </div>
      </div>
    </div>
  </div>
</section>


<section class="main-1 clearfix">
  <div class="quiz-box quiz-main container">
    <div class="col-sm-12">
      <div class="quiz-board clearfix">
        <div class="quiz-head quiz-info"> <span>Your Result</span><a href="javascript:;" class="pull-right" style="color: #ffffff;" id="ReferedQuiz">See Refered Quiz Results attended by you</a><a href="javascript:;" class="pull-right" style="color: #ffffff;display:none;" id="AssignedQuiz" >See Assigned Quiz Results</a></div>
        <div class="qz-list clearfix">
          <label>Select Quiz</label>
          <form>
            <div class="col-sm-12 col-xs-12 form-group" id="AssignedQuizSelectBoxDiv">
              <select class="form-control" id="QuizName">
                <option value="">Select Quiz</option>
                @foreach($results as $result)
                 <option value="{{$result->id}}" round_id="{{$result->round_id}}" school_id="{{$result->school_id}}" student_id="{{$result->student_id}}">{{$result->quiz_name.'- '.$result->round_name}}</option>
                @endforeach
              </select>
            </div>

            <div class="col-sm-12 col-xs-12 form-group" id="ReferedQuizSelectBoxDiv" style="display: none;">
              <select class="form-control" id="ReferedQuizName">
                <option value="">Select Refered Quiz</option>
                @foreach($referefresults as $referresult)
                 <option value="{{$referresult->id}}" round_id="{{$referresult->round_id}}" refered_by="{{$referresult->refered_by}}" student_id="{{$referresult->student_id}}">{{$referresult->quiz_name.'- '.$referresult->round_name}}</option>
                @endforeach
              </select>
            </div>

          </form>
        </div>
        <div class="round-list" style="display: none;">
          <div class="qz-details clearfix">
            <div class="qz-name clearfix"><a href="javascript:;">
              <h2 id="ResultQuizName"></h2>
              </a> </div>
            <div class="qz-name qz-round clearfix result-detail">
              <div class="col-sm-12 col-xs-12">
                <h3 id="ResultSchoolName"></h3>
                <h3 id="ResultQuizDuration"></h3>
                <h3 id="ResultNumberofQuestion"></h3>
                <h3 id="ResultScore"></h3>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog"> 
          
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Questions</h4>
            </div>
            <div class="modal-body">
              <div class="row quiz-result-data text-center">
                <div class="col-sm-12 col-xs-12 quiz-score">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Question</th>
                        <th>Answer</th>
                        <th>Check</th>
                      </tr>
                    </thead>
                    <tbody id="ShowQuestionAnswer">
                    </tbody>
                  </table>
                </div>
                
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button class="btn sel-quiz-btn"><i class="fas fa-share"></i>&nbsp; Share</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
@section('page_js')
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
<script>

 $("#QuizName").on('change',function(){
  if($('option:selected', this).val()!=''){
   var round_id =  $('option:selected', this).attr('round_id');
   var school_id =  $('option:selected', this).attr('school_id');
   var student_id =  $('option:selected', this).attr('student_id');
   var path = APP_URL + "/student/selected_quiz_result";
   $.ajax({
          type: "POST",
          url: path,
          data: {
            round_id: round_id,
            school_id: school_id,
            student_id: student_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            console.log(result);
            var res = $.parseJSON(result);
           
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              var school = $.parseJSON(JSON.stringify(res.school));
              $(".round-list").css('display','block');
              $('#ResultQuizName').html('<span><i class="fa fa-file-alt"></i></span>'+data.quiz_name+" : "+data.round_name);
              $("#ResultSchoolName").text(school.name);
              $("#ResultQuizDuration").text('Duration : '+data.quiz_duration);
              $("#ResultNumberofQuestion").text('Number of Questions : '+data.total_question);
              $("#ResultScore").text('Score : '+data.score+'/'+data.total_question);

            }
          },
          error: function(){
            alert("Error");
          }
        }); 
    }else{
         swal('Error','Select Assigned Quiz Name','error');
          $(".round-list").css('display','none');
          $('#ResultQuizName').html('');
          $("#ResultSchoolName").text('');
          $("#ResultQuizDuration").text('');
          $("#ResultNumberofQuestion").text('');
          $("#ResultScore").text('');
    }
  
 })



 $("#ReferedQuizName").on('change',function(){
  if($('option:selected', this).val()!=''){
   var round_id =  $('option:selected', this).attr('round_id');
   var refered_by_id =  $('option:selected', this).attr('refered_by');
   var student_id =  $('option:selected', this).attr('student_id');
   var path = APP_URL + "/student/selected_refered_quiz_result";
   $.ajax({
          type: "POST",
          url: path,
          data: {
            round_id: round_id,
            refered_by_id: refered_by_id,
            student_id: student_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            console.log(result);
            var res = $.parseJSON(result);
           
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              var user = $.parseJSON(JSON.stringify(res.user));
              $(".round-list").css('display','block');
              $('#ResultQuizName').html('<span><i class="fa fa-file-alt"></i></span>'+data.quiz_name+" : "+data.round_name);
              $("#ResultSchoolName").text('Refered By : '+user.name);
              $("#ResultQuizDuration").text('Duration : '+data.quiz_duration);
              $("#ResultNumberofQuestion").text('Number of Questions : '+data.total_question);
              $("#ResultScore").text('Score : '+data.score+'/'+data.total_question);

            }
          },
          error: function(){
            alert("Error");
          }
        }); 

  }else{
    swal('error','Select Refered Quiz Name','error');
    $(".round-list").css('display','none');
              $('#ResultQuizName').html('');
              $("#ResultSchoolName").text('');
              $("#ResultQuizDuration").text('');
              $("#ResultNumberofQuestion").text('');
              $("#ResultScore").text('');
  }
 })


$("#ReferedQuiz").on('click',function(){
   $("#ReferedQuizName").val('');
    $("#AssignedQuizSelectBoxDiv").css("display","none");
    $("#ReferedQuiz").css("display","none");
    $(".round-list").css('display','none');
          $('#ResultQuizName').html('');
          $("#ResultSchoolName").text('');
          $("#ResultQuizDuration").text('');
          $("#ResultNumberofQuestion").text('');
          $("#ResultScore").text('');
    $("#ReferedQuizSelectBoxDiv").css("display","block");
    $("#AssignedQuiz").css('display','block');
 
})

$("#AssignedQuiz").on('click',function(){
    $("#QuizName").val('');
    $("#ReferedQuizSelectBoxDiv").css("display","none");
    $("#AssignedQuiz").css("display","none");
    $(".round-list").css('display','none');
          $('#ResultQuizName').html('');
          $("#ResultSchoolName").text('');
          $("#ResultQuizDuration").text('');
          $("#ResultNumberofQuestion").text('');
          $("#ResultScore").text('');

    $("#AssignedQuizSelectBoxDiv").css("display","block");
    $("#ReferedQuiz").css('display','block');


 
})
  </script>
  @endsection