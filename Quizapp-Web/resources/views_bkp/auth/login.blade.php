@extends('layouts.app')
@section('page_title') 
Web Quiz Application-Login
@endsection
@section('content')

<div class="m-login__signin">
     
    <form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
         @csrf
        <div class="form-group m-form__group">
            <label>Contact&nbsp;&nbsp;<span style="font-size: 10px!important;color: #ec2f23!important;font-weight: bold!important;
">(Example: 0172213301)</span></label>
            <input class="form-control{{ $errors->has('contact') ? ' is-invalid' : '' }} m-input" type="text" placeholder="" name="contact" value="{{ old('contact') }}" autocomplete="off" required>
            @if ($errors->has('contact'))
            <span class="invalid-feedback">
                    <strong>{{ $errors->first('contact') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group m-form__group">
            <label>Password&nbsp;&nbsp;<span style="font-size: 10px!important;color: #ec2f23!important;font-weight: bold!important;
">(1st time login = last 4-digits of your mobile)</span></label>
            <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} m-input m-login__form-input--last" type="password" placeholder="" name="password" required>
            @if ($errors->has('password'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="row m-login__form-sub">
            <div class="col m--align-center">
                <label class="m-checkbox m-checkbox--focus">
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                    {{ __('Remember Me') }}
                    <span></span>
                </label>
            </div>
            
        </div>
        <div class="m-login__form-action">
            <div class="col m--align-center">
                <a href="{{ route('forget-password') }}" id="m_login_forget_password" class="m-link">
                    Forget Password ?
                </a>
            </div>
            <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air" type="submit">
               {{ __('Sign In') }} 
            </button>
        </div>
    </form>
</div>

<div class="m-login__forget-password">
    <div class="m-login__head">
        <h3 class="m-login__title">
            <a href="{{ route('forget-password') }}">
             {{ __('Forgot Your Password?') }}
            </a>
            
        </h3>
    </div>
</div>


<div class="m-stack__item m-stack__item--center">
    <div class="m-login__account">
        <span class="m-login__account-msg">
            Register Student ? 
        </span>
        &nbsp;&nbsp;
        <a href="{{ route('register') }}" id="m_login_signup" class="m-link m-link--focus m-login__account-link">
            Sign Up
        </a>
        Here
    </div>
</div>
@endsection


@section('content_heading')
 The Hottest Quiz Challenge in Town
@endsection
