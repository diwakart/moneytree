@extends('layouts.app')
@section('page_title') 
Web Quiz Application-Register
@endsection
@section('content')
 
              
    
      <style>
body {
      width: 100%;  
    /*display: table;*/
        }
        .reg{
        display: inherit !important;
        width:100%;
}
.m-grid.m-grid--hor:not(.m-grid--desktop):not(.m-grid--desktop-and-tablet):not(.m-grid--tablet):not(.m-grid--tablet-and-mobile):not(.m-grid--mobile) {
    width: 100%;
    background-size: cover !important;
    display: flex;
    flex-direction: row-reverse !important;
}
</style>
<h1 class="login_heading">Register</h1>


<div class="m-login__signin">
      @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

    <form class="m-login__form m-form"  method="POST" action="{{ route('register') }}" >
         @csrf
                       <div class="form-group row">
                            <div class="col-md-12">
                                  @if(isset($name) && $name!='')
                                  @php($studentname = $name)
                                   
                                @else
                                        @php($studentname = old('name'))
                                @endif 
                                 <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}  m-input" name="name" value="{{$studentname}}" required autofocus placeholder="Name">

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
        
        
        
                        <div class="form-group row">
                            <div class="col-md-12">
                                <select class="form-control{{ $errors->has('school_id') ? ' is-invalid' : '' }} m-input" id="m_select2_1" name="school_id" required>
									@inject('school_name','App\School')
                                    @php($school_name = $school_name->get())
                                    @foreach($school_name as $school)
                                    <option value="{{$school->id}}">{{$school->name}}</option>
                                    @endforeach
								</select>
                                
                                @if ($errors->has('school_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row" id="OtherSchoolDiv" style="display: none;">
                            <div class="col-md-12">
                                <input  type="text" id="other_school" name="other_school" class="form-control m-input" placeholder="Enter School Name" value="">
                                @if ($errors->has('other_school'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('other_school') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group row">
                            <div class="col-md-12">
                                <select id="country_code" class="form-control{{ $errors->has('country_code') ? ' is-invalid' : '' }} m-input" name="country_code" required>
                                    <option value="">Country Code</option>
                                    @php($countries = array('+60'=>'Malaysia (+60)','+91'=>'India (+91)','+44'=>'United Kingdom (+44)','+65'=>'Singapore (+65)','+1'=>'United State (+1)'))
                                    @foreach($countries as $key => $value)

                                     @if(isset($country_code) && $country_code!='')
                                       
                                        @php($code = $country_code)
                                     @else
                                        @php($code = '+60')
                                     @endif
                                      <option value="{{$key}}" @if($key== $code) {{ 'selected' }}@endif>{{$value}}</option>
                                      @endforeach

                                </select>
                                
                                @if ($errors->has('country_code'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('country_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                
                                 @if(isset($contact) && $contact!='')
                                       
                                        @php($contact = $contact)
                                     @else
                                        @php($contact ='')
                                     @endif
                                <input  type="text" name="contact" class="form-control{{ $errors->has('contact') ? ' is-invalid' : '' }} m-input" required placeholder="Contact" value="{{$contact}}">
                                    
                                @if ($errors->has('contact'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                           
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} m-input" name="email" value="{{ old('email') }}" placeholder="Email Address">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} m-input" name="password" required placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            
                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control m-input" name="password_confirmation" required placeholder="Confirm Password">
                            </div>
                        </div>
        
        
         <div class="m-login__form-action"> 
              <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                    {{ __('Register') }}
                                </button>
             
            
        </div>
        
    </form>
</div>


 
                    
                 
             
@endsection

@section('content_heading')
The Hottest Quiz Challenge in Town

@endsection

@section('page_script')
<script>
$('#m_select2_1').select2({
            placeholder: "Select an option",
            maximumSelectionLength: 20
});

$("#m_select2_1").on('change',function(){
   var other_school = $( "#m_select2_1 option:selected" ).text();
   if(other_school=='Other'){
    $("#OtherSchoolDiv").css('display','block');
    $("#other_school").attr('required',true);
   }else{
    $("#OtherSchoolDiv").css('display','none');
    $("#other_school").attr('required',false);
   }

})
</script>
@endsection
