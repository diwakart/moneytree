@extends('layouts.default')
@section('page_title') 
Upload Result
@endsection 
@section('page_css')
<style>

</style>
@endsection
@section('content')

<div class="m-subheader ">
        <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
              Upload Result
            </h3>
        </div>
        </div>
</div>



<div class="m-content">
  <div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <span class="m-portlet__head-icon m--hide">
            <i class="flaticon-statistics"></i>
          </span>
          <h3 class="m-portlet__head-text">
            Assign Quiz Round to Students
          </h3>
          <!-- <h2 class="m-portlet__head-label m-portlet__head-label--warning">
            <span>
             Select School
            </span>
          </h2> -->
        </div>
      </div>
    </div>
    <div class="m-portlet__body">

    @if(Session::has('errormessage'))
                      <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        <strong>
                            Oh snap!
                        </strong>{{ Session::get('errormessage') }}
                      </div>
                      @endif

                      @if(Session::has('successmessage'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        <strong>
                            Well done!
                        </strong>{{ Session::get('successmessage') }}
                    </div>
                @endif
                

      <div class="form-group m-form__group row">
        <div class="col-md-10 col-md-offset-1">
          <select class="form-control" id="SelectQuiz">
            <option value="">Select Quiz</option>
             @foreach($quiz as $quiz)
            <option value="{{$quiz->id}}">{{$quiz->title}} </option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group m-form__group row" id="SelectRoundDiv" style="display: none;">
        <div class="col-md-10 col-md-offset-1">
          <select class="form-control" id="SelectRound">
            <option value="">Select School</option>
          </select>
        </div>
      </div>

      <div class="form-group m-form__group row" id="SelectSchoolDiv" style="display: none;">
        <div class="col-md-10 col-md-offset-1">
          <select class="form-control" id="SelectSchool">
          </select>
        </div>
      </div>


      <div class="form-group m-form__group row" id="ExportCSVDiv" style="display: none;">
        <div class="col-md-10 col-md-offset-1">
          <a href="" id="ExportCSV">Download CSV File and Fill Marks
          </a>
        </div>
      </div>

       <div class="form-group m-form__group row" id="BtnResultUploadDiv" style="display: none;" >
        <div class="col-md-10 col-md-offset-1">
        <button type="button"  id="BtnUploadCSVResult" class="btn btn-primary" style="float:right">Upload Result</button>
      </div>
      </div> 
    </div>
  </div>
</div>


<!-- Modal for Add Rounds -->
    <div class="modal fade" id="UploadResultMOdal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                       
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <form name="fm-student" id="submit_form_csv_result" action="{{ asset('administration/importCSVResult') }}" method="post" enctype = "multipart/form-data">
                   {{ csrf_field() }}
                <div class="modal-body">
                  
                        <div class="form-group">
                            <label for="quizTitle" class="form-control-label">
                             Upload Result:
                            </label>
                            <input type="hidden" name="UploadResultQuizId" id="UploadResultQuizId">
                            <input type="hidden" name="UploadResultRoundId" id="UploadResultRoundId">
                            <input type="hidden" name="UploadResultSchoolId" id="UploadResultSchoolId">

                            <input type="file" accept=".csv" class="form-control" id="csv_file_result" name="csv_file_result">
                        </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary" id="UploadCSVResult">
                        Save changes
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div> 


@endsection

@section('page_script')
<script>

$("#SelectQuiz").on('change',function(){
  if($('option:selected', this).val()!=''){
   var quiz_id =  $('option:selected', this).val();
   var path = APP_URL + "/administration/upload_result_getquiz_id";
   $.ajax({
          type: "POST",
          url: path,
          data: {
            quiz_id: quiz_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            var res = $.parseJSON(result);
           
            if(res.status == 'error'){

            }else{
              $('#SelectRoundDiv').css('display','block');
              var data = $.parseJSON(JSON.stringify(res.quiz_round));
              $.each(data, function(idx,values){

                    $('#SelectRound').find('option').remove().end();
                    var mySelect = $('#SelectRound');
                    mySelect.append(
                            $('<option></option>').val('').html('Select Round')
                        );
                   $.each(data, function(idx,values){
                        mySelect.append($('<option></option>').val(values.id).html(values.round_name));
                         });
                });


                  $('#SelectSchool').append(
                            $('<option></option>').val('').html('Select School')
                        );
                  $('#SelectSchoolDiv').css('display','none');
                  $('#ExportCSVDiv').css('display','none');
                  $('#BtnResultUploadDiv').css('display','none');
                  
              
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
 }else{
   $('#SelectRound').append(
                            $('<option></option>').val('').html('Select Round')
                        );
                  $('#SelectRoundDiv').css('display','none');

    $('#SelectSchool').append(
                            $('<option></option>').val('').html('Select School')
                        );
                  $('#SelectSchoolDiv').css('display','none');
                   $('#ExportCSVDiv').css('display','none');
                   $('#BtnResultUploadDiv').css('display','none');
 }
})

$("#SelectRound").on('change',function(){
   if($('option:selected', this).val()!=''){
     var round_id =  $('option:selected', this).val();
   
   var path = APP_URL + "/administration/upload_result_getSchool_id";
   $.ajax({
          type: "POST",
          url: path,
          data: {
            round_id: round_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            var res = $.parseJSON(result);
           
            if(res.status == 'error'){

            }else{
              $('#SelectSchoolDiv').css('display','block');
              var data = $.parseJSON(JSON.stringify(res.school));
              $.each(data, function(idx,values){

                    $('#SelectSchool').find('option').remove().end();
                    var mySelect = $('#SelectSchool');
                    mySelect.append(
                            $('<option></option>').val('').html('Select School')
                        );
                   $.each(data, function(idx,values){
                        mySelect.append($('<option></option>').val(values.id).html(values.name));
                         });
                });

              $('#ExportCSVDiv').css('display','none');
              $('#BtnResultUploadDiv').css('display','none');
              
            }
          },
          error: function(){
            alert("Error");
          }
        });

 }else{
   $('#SelectSchool').append(
                            $('<option></option>').val('').html('Select School')
                        );
                  $('#SelectSchoolDiv').css('display','none');
                  $('#ExportCSVDiv').css('display','none');
                  $('#BtnResultUploadDiv').css('display','none');
 }
})

$("#SelectSchool").on('change',function(){
  $('#ExportCSVDiv').css('display','block');
  $('#BtnResultUploadDiv').css('display','block');
  var school_id = $( "#SelectSchool option:selected" ).val();
  var round_id = $( "#SelectRound option:selected" ).val();
  var quiz_id = $( "#SelectQuiz option:selected" ).val();

  if(school_id =='' || round_id =='' || quiz_id =='' ){
   swal('Error','Select Fields','error');
   $("#ExportCSV").hide();
  }else{
  $("#ExportCSV").attr('href',APP_URL+'/administration/export_result_csv/'+school_id+'/'+round_id+'/'+quiz_id);
  $("#ExportCSV").show();
   }
})


$("#BtnUploadCSVResult").on('click',function(){
   $("#UploadResultMOdal").modal('show');
    var quiz_id = $("#SelectQuiz option:selected" ).val();
    var school_id = $( "#SelectSchool option:selected" ).val();
    var round_id = $( "#SelectRound option:selected" ).val();
    $("#UploadResultQuizId").val(quiz_id);
    $("#UploadResultRoundId").val(round_id);
    $("#UploadResultSchoolId").val(school_id);

})
</script>
@endsection
