@extends('layouts.default')

@section('page_css')
<style>
#ResponseHeading{
 color: #4CAF50;
}
</style>
@endsection
@section('content')

<div class="m-subheader ">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="m-subheader__title m-subheader__title--separator">
        Students
      </h3>
      <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
        <li class="m-nav__item m-nav__item--home">
          <a href="{{url('administration/home')}}" class="m-nav__link m-nav__link--icon">
            <i class="m-nav__link-icon la la-home"></i>
          </a>
        </li>
        <li class="m-nav__separator">
          -
        </li>
        <li class="m-nav__item">
          <a href="{{url('administration/student')}}" class="m-nav__link">
            <span class="m-nav__link-text">
              Manage Students
            </span>
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Student(s)
                        <small>
                           Registered Student(s)
                        </small>
                    </h3>

                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                @if(Session::has('errormessage'))
                      <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        <strong>
                            Oh snap!
                        </strong>{{ Session::get('errormessage') }}
                      </div>
                      @endif

                      @if(Session::has('successmessage'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        <strong>
                            Well done!
                        </strong>{{ Session::get('successmessage') }}
                    </div>
                @endif
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                          <div class="col-md-4">
                              <select class="selectpicker" id="student_list" name="student_list">
                                <option value="">Select School Name</option>
                                @foreach($schools as $school)
                                <option value="{{$school->id}}">{{$school->name}}</option>
                                @endforeach
                              </select>
                            </div>

                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <a href="javascript;:" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="addStudent">
                            <span>
                                <i class="la la-user"></i>
                                <span>
                                    Add Student
                                </span>
                            </span>
                        </a>

                        <a href="javascript;:" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="addStudentCSV">
                            <span>
                                <i class="la la-upload"></i>
                                <span>
                                    CSV Upload
                                </span>
                            </span>
                        </a>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
<!--begin: Datatable -->
             <div class="loader_msg" style='display: block;'>
                <img src="{{ asset('assets/inner/loader.gif') }}" width='132px' height='132px' style="height: 70px;width: 67px;margin-left: 40%;">
            </div>
            <div class="student_datatable" id="ajax_data"></div>
            <!--end: Datatable -->
        </div>
    </div>
</div>

<div class="modal fade" id="addStudentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    &times;
                </span>
            </button>
        </div>
        <form name="fm-student">
        <div class="modal-body">
                
                <div class="form-group">
                    <label for="name" class="form-control-label">
                        School:
                    </label>
                    <select class="form-control" id="school_name" name="school_name">
                      <option value="">Select School
                      </option>
                      @foreach($schools as $school)
                      <option value="{{$school->id}}">{{$school->name}}</option>
                      @endforeach
                  </select>
                </div>

                <div class="form-group">
                    <label for="name" class="form-control-label">
                        Name:
                    </label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="email" class="form-control-label">
                        Email:
                    </label>
                    <input type="text" class="form-control" id="email" name="email">
                </div>
                <div class="form-group" id="PasswordInput">
                    
                </div>
                <div class="form-group">
                    <label for="country_code" class="form-control-label">
                       Country:
                    </label>
                    <select class="form-control" id="country_code" name="country_code">
                      <option value="">Select Country Code
                      </option>
                      @foreach($countries as $key => $value)
                      <option value="{{$key}}" @if($key== '+60') {{ 'selected' }}@endif>{{$value}}</option>
                      @endforeach
                  </select>
                </div>

                <div class="form-group">
                    <label for="contact" class="form-control-label">
                       Contact:
                    </label>
                    <input type="number" class="form-control" id="contact" name="contact">
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
            <button type="button" class="btn btn-primary" id="formSubmit">
                Save changes
            </button>
        </div>
        </form>
    </div>
</div>
</div>


<div class="modal fade" id="addStudentModalCSV" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    &times;
                </span>
            </button>
        </div>
        <form name="fm-student" id="submit_form_csv" action="{{ asset('administration/student/importCSV') }}" method="post" enctype = "multipart/form-data">
        <div class="modal-body">
          {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="form-control-label">
                        Upload File:
                    </label>
                    <input type="file" class="form-control" id="csvfile" name="csvfile" accept=".csv">
                </div>
            
                <div class="form-group">
                    <label for="contact" class="form-control-label">
                      Download Sample File from <a href="{{route('exportCSV')}}">here</a>.
                    </label>
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
            <button type="submit" class="btn btn-primary" id="formSubmitCSV">
                Save changes
            </button>
        </div>
        </form>
    </div>
</div>
</div>


<div class="modal fade" id="ChangePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    &times;
                </span>
            </button>
        </div>
        <form name="fm-student" id="change_password_form">
        <div class="modal-body">
          {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="form-control-label">
                        Password:
                    </label>
                    <input type="password" class="form-control" id="change_password" name="change_password" accept=".csv">
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
            <button type="submit" class="btn btn-primary" id="ChangePasswordBtn">
                Save changes
            </button>
        </div>
        </form>
    </div>
</div>
</div>

<!-- Modal for success response -->
    <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <form name="fm-student" id="arrange-order-form">
                <div class="modal-body">
                    <h5 id="ResponseHeading"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadQuestionDatatable">
                        OK
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_script')
<script src="{{asset('assets/inner/datatable/data-ajax.js')}}" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

<script>

     var datatable;
          (function() {
                $('.loader_msg').css('display','none');
                datatable = $('.student_datatable').mDatatable({
                // datasource definition
                data: {
                type: 'remote',
                source: {
                  read: {
                      url: APP_URL+'/administration/student/getAllStudents',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                              generalSearch: '',
                              student_list:'',
                          }
                      },
                      map: function(raw) {
                          console.log('ITS RUN');
                          console.log(raw);
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                               dataSet = raw.data;
                          }
                          console.log('Result');
                          console.log(dataSet);
                          return dataSet;
                      },
                  }
                },
                pageSize: 10,
                  saveState: {
                    cookie: false,
                    webstorage: false
                },

                serverPaging: true,
                serverFiltering: false,
                serverSorting: false
            },
          // layout definition
            layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
              input: $('#generalSearch')
            },

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
        field: "S.no",
        title: "S.No",
        width: 100
      },
      {
          field: "name",
          title: "Name",
          textAlign: 'center'
      },
      {
        field: "email",
        title: "Email",
        textAlign: 'center'
       
      },
      {
          field: "contact",
          title: "Contact",
          textAlign: 'center'
      },
      {
          field: "school_name",
          title: "School",
          textAlign: 'center'
      },
      {
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          field: 'Actions',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

            return '\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit" onclick=getstudentDetail('+row.id+')></i>\
            </a>\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Change Password">\
              <i class="la la-key" onclick=getUserID('+row.id+')></i>\
            </a>\
            <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteStudent('+row.id+')>\
              <i class="la la-trash"></i>\
            </a>\
          ';
          },
        }]
          });


          var query = datatable.getDataSourceQuery();

        var query = datatable.getDataSourceQuery();

       
      
        $('#formSubmit').on('click',function(){
          datatable.reload();
        });

        $("#ChangePasswordBtn").on('click',function(){
          datatable.reload();
        });

        $('#student_list').on('change',function(){
          var value = $(this).val();
          datatable.setDataSourceQuery({student_list:value});
          datatable.reload();
        });
      
        $("#LoadQuestionDatatable").on('click',function(){
          datatable.reload();
        });

    })();



$('#addStudentCSV').click(function(e){
    e.preventDefault();
    $('#addStudentModalCSV').modal('show');
    $('#addStudentModalCSV').data('id','');
    $('#addStudentModalCSV').find('.modal-title').html('CSV Upload');
    $('#csvfile').val('');
  });

$('#addStudent').click(function(e){
    e.preventDefault();
    $('#addStudentModal').modal('show');
    $('#addStudentModal').data('id','');
    $('#addStudentModal').find('.modal-title').html('Add Student');
    $('#name').val('');
    $('#email').val('');
    $('#contact').val('');
     $('#school_name').val('');
    var pwd_input = '<label for="password" class="form-control-label">Password:</label><input type="password" class="form-control" id="password" name="password">';
    $("#PasswordInput").html(pwd_input);
  });


  $('#formSubmit').click(function(e){
    e.preventDefault();

    var id = $('#addStudentModal').data('id');
    var name  = $('#name').val();
    var email  = $('#email').val();
    var contact = $('#contact').val();
    var school = $("#school_name").val();
    var country_code = $("#country_code").val();
    if(contact.length > 10){
       swal("","Contacts Not Greater than 10","error");
       $('#contact').val('');
       return false;
    }
    if(id=='undefined'){
       var password  = '';
    }else{
       if($('#password').val()==''){
      swal("","Password field is required","error");
      return false;
       }
        var password  = $('#password').val();
    }
    $.ajax({
      type: 'POST',
      url: APP_URL +'/administration/student/store',
      data: {
        id: id,
        name: name,
        email:email,
        contact: contact,
        password: password,
        school:school,
        country_code:country_code,
      },
       headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(data) {
        var res = $.parseJSON(data);
        if(res.status == 'error'){
          swal('Error',res.message,'error');
        }else{
          $('#addStudentModal').modal('hide');
          $("#ResponseSuccessModal").modal('show');
          $("#ResponseSuccessModal #ResponseHeading").text(res.message);
        } 
      },
      error: function(data) {
        swal('Error',data,'error');
      }
    });
  });
  $('#formSubmitCSV').click(function(e){
   
    if($('#csvfile').val()==''){
        e.preventDefault();
      swal("","Please upload file","error");
      return false;
    }
  });

  function getstudentDetail(id){
        var path = APP_URL + "/administration/student/getstudentDetail";
        $.ajax({
          type: "POST",
          url: path,
          data: {
            id: id
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);

            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              $('#addStudentModal').data('id',data.id);
              $('#addStudentModal').find('.modal-title').html('Update student ' + data.name);
              $('#name').val(data.name);
              $('#email').val(data.email);
              $('#contact').val(data.contact);
               $('#school_name').val(data.school_id).prop('selected', true);
              $("#PasswordInput").html('');
              $('#addStudentModal').modal('show');
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
  }



  function getUserID(id){
    var path = APP_URL + "/administration/student/getUserID";
    $.ajax({
      type: "POST",
      url: path,
      data: {
        id: id
      },
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
      success: function(result){
        //console.log(result);
        var res = $.parseJSON(result);

        if(res.status == 'error'){

        }else{
          var data = $.parseJSON(JSON.stringify(res.message));
          $('#ChangePasswordModal').data('id',data.id);
          $('#ChangePasswordModal').find('.modal-title').text('Change Password of Student  ' + data.name);
          $('#ChangePasswordModal').modal('show');
        }
      },
      error: function(){
        alert("Error");
      }
    }); 
  }


  $('#ChangePasswordBtn').click(function(e){
    e.preventDefault();
    
    var id = $('#ChangePasswordModal').data('id');
    var password  = $('#change_password').val();
    var path = APP_URL+'/administration/student/changepassword';
    $.ajax({
      type: 'POST',
      url: path,
      data: {
        id: id,
        password: password
      },
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
      success: function(data) {
        var res = $.parseJSON(data);
        if(res.status == 'error'){
          swal('Error',res.message,'error');
        }else{
          $('#ChangePasswordModal').modal('toggle');
          $('#change_password').val('');
          $('#ChangePasswordModal').data('id','');
         $("#ResponseSuccessModal").modal('show');
         $("#ResponseSuccessModal #ResponseHeading").text(res.message);
        } 
      },
      error: function(data) {
        swal('Error',data,'error');
      }
    });
  });


  function deleteStudent(id){
    var path = APP_URL + "/administration/student/delete";
    var _this = $(this);
    swal({
      title: "Are you sure to delete this student?",
      text: "Your will lost all records of this Student",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        var data = id;
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: data,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               $('.sweet-overlay').remove();
               $('.showSweetAlert ').remove();
              $("#ResponseSuccessModal").modal('show');
              $("#ResponseSuccessModal #ResponseHeading").text(res.message);
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
  }

      
</script>
@endsection


