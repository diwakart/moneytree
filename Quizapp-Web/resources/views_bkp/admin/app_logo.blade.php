@extends('layouts.default')
@section('page_title') 
Add Logo
@endsection 
@section('page_css')

@endsection
@section('content')
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title ">
                Application Logo
            </h3>
        </div>
        <div>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <div class="row">
        <div class="col-lg-4">
            <div class="m-portlet m-portlet--full-height  ">
                <div class="m-portlet__body">
                    <div class="m-card-profile">
                        <div class="m-card-profile__title m--hide">
                            Website Logo
                            @if(Auth::user()->picture!='' )
                             @php($img = url('/').Storage::url('app/'.Auth::user()->picture))
                            @else
                             @php($img = url('/').Storage::url('app/dummy-logo.png'))
                            @endif
                        </div>
                        <div class="m-card-profile__pic">
                            <div class="m-card-profile__pic-wrapper" style="border-radius: 0px!important" >
                                <img src="{{$img}}" alt="" class="previewHolder" style="    height: 80px;width: 80px;border-radius: 0px!important" />
                            </div>
                        </div>
                        <div class="m-card-profile__details">
                            <span class="m-card-profile__name">
                              Website Logo
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            @if(Session::has('errormessage'))
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                <strong>
                    Oh snap!
                </strong>{{ Session::get('errormessage') }}
              </div>
              @endif

              @if(Session::has('successmessage'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                <strong>
                    Well done!
                </strong>{{ Session::get('successmessage') }}</div>
              @endif
            <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                    <i class="flaticon-share m--hide"></i>
                                   Change Logo
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="m_user_profile_tab_1">
                        <form class="m-form m-form--fit m-form--label-align-right"
                        action="{{ asset('administration/app-logo/update-logo') }}" method="post" enctype = "multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group m--margin-top-10 m--hide">
                                    <div class="alert m-alert m-alert--default" role="alert">
                                        Uploaded Image will be replaced by Website Logo
                                    </div>
                                </div>
            
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Upload Logo
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="file" id="logo" name="logo" accept="image/x-png,image/jpeg"  >
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-2"></div>
                                        <div class="col-7">
                                            <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom" id="ValidateButn">
                                                Save changes
                                            </button>
                                            &nbsp;&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_script')
<script>
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.previewHolder').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#logo").change(function() {
  readURL(this);
});
   

   
</script>
@endsection


