@extends('layouts.default')

@section('page_css')

@endsection
@section('content')


<!-- END: Subheader -->
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Student(s)
                        <small>
                           Registered Student(s)
                        </small>
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <a href="javascript:;" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="addStudent">
                            <span>
                                <i class="la la-user"></i>
                                <span>
                                    Add Student
                                </span>
                            </span>
                        </a>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>

                         <div class="table-container">
                          <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                            <thead>
                              <tr role="row" class="heading">
                                <th width="5%"> Record&nbsp;# </th>
                                <th> name </th>
                                <th> email </th>
                                <th> contact </th>
                                <th> action </th>
                              </tr>
                            </thead>
                            <tbody> </tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
<!--begin: Datatable -->
            <div class="" id="admin_student_list"></div>
            <!--end: Datatable -->
        </div>
    </div>
</div>

<div class="modal fade" id="addStudentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    &times;
                </span>
            </button>
        </div>
        <form name="fm-student">
        <div class="modal-body">
          
                <div class="form-group">
                    <label for="name" class="form-control-label">
                        Name:
                    </label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="email" class="form-control-label">
                        Email:
                    </label>
                    <input type="text" class="form-control" id="email" name="email">
                </div>
                <div class="form-group" id="PasswordInput">
                    
                </div>
                <div class="form-group">
                    <label for="contact" class="form-control-label">
                       Contact:
                    </label>
                    <input type="text" class="form-control" id="contact" name="contact">
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
            <button type="button" class="btn btn-primary" id="formSubmit">
                Save changes
            </button>
        </div>
        </form>
    </div>
</div>
</div>
@endsection

@section('page_script')

<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js">
</script>
<script>

    var DatatableDataLocalDemo = function () {

  // demo initializer
  
  var demo = function () {
   var edit_url = 'student/edit';
            var delete_url = 'student/delete';
            var view_url = 'student/view';
      var datatable = $('#admin_student_list').mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            // sample GET method
            method: 'GET',
            url: 'http://localhost/webquiz/administration/student/getAllStudents',
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 10,
        saveState: {
          cookie: true,
          webstorage: true,
        },
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
      },
      // layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        // height: 450, // datatable's body's fixed height
        footer: false // display/hide footer
      },

      // column sorting
      sortable: true,

      pagination: true,

      search: {
        input: $('#generalSearch')
      },

      // inline and bactch editing(cooming soon)
      // editable: false,

      // columns definition
      columns: [{
        field: "id",
        title: "ID",
        width: 100
      },
      {
          field: "name",
          title: "Name",
          textAlign: 'center'
      },
      {
        field: "email",
        title: "Email",
        textAlign: 'center'
       
      },
      {
          field: "contact",
          title: "Contact",
          textAlign: 'center'
      },
       {
          field: 'Actions',
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

            return '\
            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\
              <i class="la la-trash"></i>\
            </a>\
          ';
          },
        }
      ]
    });
}
  return {
    //== Public functions
    init: function () {
      // init dmeo
      demo();
    }
  };
}();

jQuery(document).ready(function () {
  DatatableDataLocalDemo.init();
});

</script>
@endsection