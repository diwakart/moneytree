@extends('layouts.default')
@section('page_title') 
Add Quiz-Round
@endsection 
@section('page_css')

@endsection
@section('content')
<input type="hidden" name="id" id="adminid" value="{{ Auth::user()->id }}">
<input type="hidden" name="id" id="quiz_id" value="{{$quiz->id}}">
<div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Quiz
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{url('administration/home')}}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="{{ asset('administration/quiz') }}" class="m-nav__link">
                        <span class="m-nav__link-text">
                            {{$quiz->title}}
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="{{ asset('administration/manage-round/'.$quiz->id) }}" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Manage Rounds
                        </span>
                    </a>
                </li>
            </ul>
        </div>

        
       
        </div>
    </div>

    <div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Quiz Round(s)
                        <small>
                           List of added quiz rounds
                        </small>
                    </h3>

                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <button type="button" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="AddQuizRoundBtn">
                            <span>
                                <i class="la la-th-list"></i>
                                <span>
                                    Add Round
                                </span>
                            </span>
                        </button>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
<!--begin: Datatable -->
            <div class="loader_msg" style='display: block;'>
                <img src="{{ asset('assets/inner/loader.gif') }}" width='132px' height='132px' style="height: 70px;width: 67px;margin-left: 40%;">
            </div>
            <div class="quiz_round_datatable" id="QuizRoundData"></div>
            <!--end: Datatable -->
        </div>
    </div>
</div>



    <!-- Modal for Add Rounds -->
    <div class="modal fade" id="AddQuizRoundModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                       
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <form name="fm-student">
                <div class="modal-body">
                  
                        <div class="form-group">
                            <label for="quizTitle" class="form-control-label">
                              Quiz Round Name:
                            </label>
                            <input type="text" class="form-control" id="quizRound" name="quizRound">
                        </div>

                        <div class="form-group">
                            <label for="quizTitle" class="form-control-label">
                              Active for :
                            </label>
                            <input type="number" class="form-control" id="activeDaysForRound" min="0" name="activeDaysForRound" style="width:33%!important;"><span style=" position: relative;top: -26px;left: 165px;">Day(s)</span>
                              
                        </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="AddRoundModalButton">
                        Save changes
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div> 



    <div class="modal fade" id="EditQuizRoundTitleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                   
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form name="fm-student" id="edit_quiz_title_form">
            <div class="modal-body">
              {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name" class="form-control-label">
                            Quiz Title:
                        </label>
                        <input type="text" class="form-control" id="quiz_round_title" name="quiz_round_title">
                    </div>

                    <div class="form-group">
                        <label for="name" class="form-control-label">
                            Active Days:
                        </label>
                        <input type="number" class="form-control" id="round_active_days" min="0" name="round_active_days" style="width:33%;"><span style=" position: relative;top: -26px;left: 165px;">Day(s)</span>
                    </div>
                
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <button type="submit" class="btn btn-primary" id="QuizRoundTitleUpdatebtn">
                    Save changes
                </button>
            </div>
            </form>
        </div>
    </div>
    </div>


    <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <form name="fm-student" id="arrange-order-form">
                <div class="modal-body">
                    <h5 id="ResponseHeading"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadQuestionDatatable">
                        OK
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>


     
@endsection

@section('page_script')
<!-- 
<script src="{{asset('assets/inner/datatable/quiz_round.js')}}" type="text/javascript"></script> -->

<script>
      var datatable;
          (function() {
                $('.loader_msg').css('display','none');
                datatable = $('.quiz_round_datatable').mDatatable({
                // datasource definition
                data: {
                type: 'remote',
                source: {
                  read: {
                      url: APP_URL+'/administration/getAllQuizRounds',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                              generalSearch: '',
                          }
                      },
                      map: function(raw) {
                          console.log('ITS RUN');
                          console.log(raw);
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                               dataSet = raw.data;
                          }
                          console.log('Result');
                          console.log(dataSet);
                          return dataSet;
                      },
                  }
                },
                pageSize: 10,
                  saveState: {
                    cookie: false,
                    webstorage: false
                },

                serverPaging: true,
                serverFiltering: false,
                serverSorting: false
            },
          // layout definition
            layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
              input: $('#generalSearch')
            },

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
              field: "S.no",
              title: "S.No",
              textAlign: 'center'
            }, {
              field: "round_name",
              title: "Quiz Round Name"
            }, {
              field: "active_days",
              title: "Active Days"
            },
            {
              field: 'Status',
              width: 180,
              title: 'Status',
              sortable: false,
              overflow: 'visible',
              template: function(row) {
                var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';
                if(row.status==1){
                  return '\
                <a href="javascript:;" class="btn btn-danger" title="Unpublish Quiz Round" onclick=unPublishRound('+row.id+')>\
                  Unpublish</i>\
                </a>\
              ';
                }else{
                  return '\
                <a href="javascript:;" class="btn btn-success" title="Publish Quiz Round" onclick=PublishRound('+row.id+')>\
                  Publish\
                </a>\
              ';
                }

              },
          },{
              field: 'Actions',
              width: 180,
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              template: function(row) {
                var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

                if(row.status==1){
                   return '\
                   <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill DeleteRound" title="Manage Questions" onclick=getroundDetail('+row.id+') >\
                  <i class="la la-edit"></i>\
                </a>\
                <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="View Round Info" onclick=editQuizRoundTitle('+row.id+')>\
                  <i class="la la-info"></i>\
                </a>\
                <a href="javascript:;" class="btn btn-success" title="Assign Quiz To Student" onclick=assignQuizToStudent('+row.id+')>\
                 Assign Quiz Round\
                </a>\
              ';
                }else{
                  return '\
                <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill DeleteRound" title="Manage Questions" onclick=getroundDetail('+row.id+') >\
                  <i class="la la-edit"></i>\
                </a>\
                <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteRound('+row.id+') >\
                  <i class="la la-trash"></i>\
                </a>\
                <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Round Info" onclick=editQuizRoundTitle('+row.id+')>\
                  <i class="la la-info"></i>\
                </a>\
              ';
                }

                
              },
          }]
          });


          var query = datatable.getDataSourceQuery();

        var query = datatable.getDataSourceQuery();

        $('#m_form_status').on('change', function () {
          datatable.search($(this).val(), 'Status');
        }).val(typeof query.Status !== 'undefined' ? query.Status : '');
        $('#showEmail').on('click',function(){
          datatable.showColumn('email_id');
        });
        $('#AddRoundModalButton').on('click',function(){
          datatable.reload();
        });
        $("#LoadQuestionDatatable").on('click',function(){
          datatable.reload();
        });
        $('#m_form_type').on('change', function () {
          datatable.search($(this).val(), 'Type');
        }).val(typeof query.Type !== 'undefined' ? query.Type : '');

        $('#m_form_status, #m_form_type').selectpicker();
        


    })();

     function getroundDetail(id){
          var round_id = id;
          window.location.href = APP_URL+"/administration/manage-question/"+round_id;
             
      }

     function assignQuizToStudent(id){
          var round_id = id;
          window.location.href = APP_URL+"/administration/assign-quiz/"+round_id;
     } 

      function deleteRound(id){
          swal('In Progress');
            swal({
              title: "Are you sure to delete this Round?",
              text: "Your will not be able to recover data related to this Round!",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false,
            },
            function(isConfirm) {
              if (isConfirm) {
                var round_id = id;
                $.ajax({
                      type: 'POST',
                      url: APP_URL+'/administration/manage-round/delete',
                      data: {
                        id: round_id,
                      },
                       headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                      success: function(data) {
                        var res = $.parseJSON(data);
                        if(res.status == 'error'){
                          swal('Error',res.message,'error');
                        }else{
                          $('.sweet-overlay').remove();
                          $('.showSweetAlert ').remove();
                          $("#ResponseSuccessModal").modal('show');
                          $("#ResponseSuccessModal #ResponseHeading").text(res.message);
                        } 
                      },
                      error: function(data) {
                        swal('Error',data,'error');
                      }
                    });
              } else {

              }
            });
            }


            function PublishRound(id){
            swal({
              title: "Are you sure to publish this Round?",
              text: "Your will not be able to edit,delete and update data related to this Round!",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes, Publish!",
              closeOnConfirm: false,
            },
            function(isConfirm) {
              if (isConfirm) {
                var round_id = id;
                $.ajax({
                      type: 'POST',
                      url: APP_URL+'/administration/manage-round/publish_round',
                      data: {
                        id: round_id,
                      },
                       headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                      success: function(data) {
                        var res = $.parseJSON(data);
                        if(res.status == 'error'){
                          swal('Error',res.message,'error');
                        }else{
                          $('.sweet-overlay').remove();
                          $('.showSweetAlert ').remove();
                          $("#ResponseSuccessModal").modal('show');
                          $("#ResponseSuccessModal #ResponseHeading").text(res.message);
                        } 
                      },
                      error: function(data) {
                        swal('Error',data,'error');
                      }
                    });
              } else {

              }
            });
            }


            function unPublishRound(id){
            swal({
              title: "Are you sure to unpublish this Round?",
              text: "This round will not be active for students!",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes, Unpublish!",
              closeOnConfirm: false,
            },
            function(isConfirm) {
              if (isConfirm) {
                var round_id = id;
                $.ajax({
                      type: 'POST',
                      url: APP_URL+'/administration/manage-round/unpublish_round',
                      data: {
                        id: round_id,
                      },
                       headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                      success: function(data) {
                        var res = $.parseJSON(data);
                        if(res.status == 'error'){
                          swal('Error',res.message,'error');
                        }else{
                          $('.sweet-overlay').remove();
                          $('.showSweetAlert ').remove();
                          $("#ResponseSuccessModal").modal('show');
                          $("#ResponseSuccessModal #ResponseHeading").text(res.message);
                        } 
                      },
                      error: function(data) {
                        swal('Error',data,'error');
                      }
                    });
              } else {

              }
            });
            }

    </script>



<script>
$("#AddQuizRoundBtn").on('click',function(){
    $("#AddQuizRoundModal").modal('show');
})

$("#AddRoundModalButton").on('click', function(){
                var quiz_round = $("#quizRound").val();
                var activeDaysForRound = $("#activeDaysForRound").val();
                var quiz_id = $("#quiz_id").val();
                 var AdminId = $("#adminid").val();
                if(quiz_round=='' || activeDaysForRound=='' ){
                    swal("Error","Please Enter Quiz Round Title and Select Active Days","error");
                }else{
                  if(activeDaysForRound>=0){
                    $.ajax({
                      type: 'POST',
                      url: APP_URL+'/administration/manage-round',
                      data: {
                        quiz_round: quiz_round,
                        active_days: activeDaysForRound,
                        quiz_id : quiz_id,
                        user_id: AdminId
                      },
                       headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                      success: function(data) {
                        var res = $.parseJSON(data);
                        if(res.status == 'error'){
                          swal('Error',res.message,'error');
                        }else{
                          window.location.href = APP_URL + "/administration/manage-question/"+res.quizround_id;
                        } 
                      },
                      error: function(data) {
                        swal('Error',data,'error');
                      }
                    });
                  }else{
                       swal("Error","Please enter valid expiration day.","error");
                  }
                    
                }
            }) 



    function editQuizRoundTitle(id){
        var path = APP_URL + "/administration/edit_quiz_round";
        $.ajax({
          type: "POST",
          url: path,
          data: {
            id: id
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);

            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              $('#EditQuizRoundTitleModal').data('id',data.id);
              $('#EditQuizRoundTitleModal').find('.modal-title').html('Update quiz round  ' + data.round_name);
              $('#quiz_round_title').val(data.round_name);
              $('#round_active_days').val(data.active_days);
              if(data.status==1){
                $("#QuizRoundTitleUpdatebtn").css('display','none');
              }else{
                 $("#QuizRoundTitleUpdatebtn").css('display','block');
              }
              $('#EditQuizRoundTitleModal').modal('show');
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
  }


  $('#QuizRoundTitleUpdatebtn').click(function(e){
    e.preventDefault();
    
    var id = $('#EditQuizRoundTitleModal').data('id');
    var round_name  = $('#quiz_round_title').val();
    var active_days  = $('#round_active_days').val();
    var path = APP_URL+'/administration/update_quiz_round';
    if(round_name=='' || active_days=='' ){
        swal("Error","Please Enter Quiz Round Title and Select Active Days","error");
     }else{
        if(active_days>=0){
            $.ajax({
              type: 'POST',
              url: path,
              data: {
                id: id,
                round_name: round_name,
                active_days: active_days,
              },
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
              success: function(data) {
                var res = $.parseJSON(data);
                if(res.status == 'error'){
                  swal('Error',res.message,'error');
                }else{
                  $('#EditQuizRoundTitleModal').modal('hide');
                  $('#quiz_round_title').val('');
                  $('#round_active_days').val('');
                  $('#EditQuizRoundTitleModal').data('id','');
                 $("#ResponseSuccessModal").modal('show');
                 $("#ResponseSuccessModal #ResponseHeading").text(res.message);
                } 
              },
              error: function(data) {
                swal('Error',data,'error');
              }
            });
        } else {
               swal("Error","Please enter valid expiration day.","error");
        }
     }
  });


  function declareQuizRoundResult(id){
        var path = APP_URL + "/administration/declare_result";
        $.ajax({
          type: "POST",
          url: path,
          data: {
            id: id
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);

            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              if(res.declare_check==0){
                var declare_message = "Result To Be Declared Tomorrow<br>Expiration Date :"+res.expiration_date;
              }else if(res.declare_check==1){
                var declare_message = "Result Declaration Date coming soon<br>Expiration Date :"+res.expiration_date;
              }else if(res.declare_check==2){
                var declare_message = "Result not declared yet<br>Expiration Date :"+res.expiration_date;
              }
              $("#RoundExpirationDate").html(declare_message);
              $('#DeclareResultModal').modal('show');
            }
          },
          error: function(){
            alert("Error");
          }
        });
  }
</script>
@endsection


