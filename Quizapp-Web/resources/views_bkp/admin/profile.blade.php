@extends('layouts.default')
@section('page_title') 
Profile
@endsection 
@section('page_css')

@endsection
@section('content')
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title ">
                My Profile
            </h3>
        </div>
        <div>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <div class="row">
        <div class="col-lg-4">
            <div class="m-portlet m-portlet--full-height  ">
                <div class="m-portlet__body">
                    <div class="m-card-profile">
                        <div class="m-card-profile__title m--hide">
                            Your Profile
                            @if(Auth::user()->picture!='' )
                             @php($img = url('/').Storage::url('app/'.Auth::user()->picture))
                            @else
                             @php($img = url('/').Storage::url('app/dummy.jpg'))
                            @endif
                        </div>
                        <div class="m-card-profile__pic">
                            <div class="m-card-profile__pic-wrapper">
                                <img src="{{$img}}" alt="" class="previewHolder" style="    height: 80px;width: 80px;" />
                            </div>
                        </div>
                        <div class="m-card-profile__details">
                            <span class="m-card-profile__name">
                                {{ Auth::user()->name }}
                            </span>
                            <a href="" class="m-card-profile__email m-link">
                                {{ Auth::user()->email }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            @if(Session::has('errormessage'))
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                <strong>
                    Oh snap!
                </strong>{{ Session::get('errormessage') }}
              </div>
              @endif

              @if(Session::has('successmessage'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                <strong>
                    Well done!
                </strong>{{ Session::get('successmessage') }}</div>
              @endif
            <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                    <i class="flaticon-share m--hide"></i>
                                    Update Profile
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_3" role="tab">
                                    Change Password
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="m_user_profile_tab_1">
                        <form class="m-form m-form--fit m-form--label-align-right"
                        action="{{ asset('administration/profile/update_profile') }}" method="post" enctype = "multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group m--margin-top-10 m--hide">
                                    <div class="alert m-alert m-alert--default" role="alert">
                                        The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-10 ml-auto">
                                        <h3 class="m-form__section">
                                        Personal Details
                                        </h3>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Full Name
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{ Auth::user()->name }}" required name="name" id="name">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Email Address
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="email" value="{{ Auth::user()->email }}" name="email" id="email">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Country Code
                                    </label>
                                    <div class="col-7">
                                        <select name="country_code" id="country_code" class="form-control m-input">
                                          @php($countries = array('+60'=>'Malaysia (+60)','+91'=>'India (+91)','+44'=>'United Kingdom (+44)','+65'=>'Singapore (+65)','+1'=>'United State (+1)'))
                                            @foreach($countries as $key => $value)
                                              <option value="{{$key}}" @if($key== Auth::user()->country_code) {{ 'selected' }}@endif>{{$value}}</option>
                                              @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Phone No.
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="number" value="{{ Auth::user()->contact }}" name="contact" >
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Profile Image
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="file" id="profileimg" name="picture" accept="image/x-png,image/jpeg" >
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-2"></div>
                                        <div class="col-7">
                                            <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom" id="ValidateButn">
                                                Save changes
                                            </button>
                                            &nbsp;&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="m_user_profile_tab_3">
                        <form  class="m-form m-form--fit m-form--label-align-right" action="{{ asset('administration/profile/update_password') }}" method="post" >
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group m--margin-top-10 m--hide">
                                    <div class="alert m-alert m-alert--default" role="alert">
                                        The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-10 ml-auto">
                                        <h3 class="m-form__section">
                                        Change Password
                                        </h3>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        New Password
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="password" name="new_password" id="new_password" >
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Confirm New Password
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="password" name="confirm_password" id="confirm_password">
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-2"></div>
                                        <div class="col-7">
                                            <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom" id="ValidateButnPassword">
                                                Save changes
                                            </button>
                                            &nbsp;&nbsp;
                                            <button type="button" class="btn btn-secondary m-btn m-btn--air m-btn--custom" id="ClearFORM">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_script')
<script>
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.previewHolder').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$('#profileimg').bind('change', function() {
         
         if(this.files[0].size/1024/1024>10 ){
            swal('','This file size is: ' + this.files[0].size/1024/1024 + 'MB. Image size should not exceed 10MB ','warning'); 
         }else{
            readURL(this) ;
         }
            
});
    $("form#FormProfile").submit(function(e){
       e.preventDefault();
       var pathname = window.location.pathname;
       $.ajax({
        type: 'POST',
        url: pathname+"/update_profile",
        data: $(this).serialize(),
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
        success: function(data) {
    
      },
      error: function(data) {
       
      }
    });
     });

    $("#ValidateButn").click(function(){
      
      if($("#name").val()=='' || $("#country_code").val()=='' || $("#contact").val()==''){
        swal("Error","Name and Email Address must be filled","error");
        return false;
      }
    })

    $("#ValidateButnPassword").click(function(){
      
      if($("#new_password").val()=='' || $("#confirm_password").val()==''){
        swal("Error","All fields are required","error");
        return false;
      }
      if($("#new_password").val()!= $("#confirm_password").val()){
        swal("Error","Password not matched","error");
        return false;
      }
    })

    $("#ClearFORM").click(function(){
      $("#new_password").val('');
        $("#confirm_password").val('');
    })
        
   
</script>
@endsection


