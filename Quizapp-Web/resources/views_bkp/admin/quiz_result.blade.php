@extends('layouts.default')
@section('page_title') 
Result
@endsection 
@section('page_css')
<style>
#ResponseHeading{
 color: #4CAF50;
}
</style>
@endsection
@section('content')

<div class="m-subheader ">
        <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
               Quiz Result
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{route('quiz')}}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="{{url('administration/quiz-result')}}" class="m-nav__link">
                        <span class="m-nav__link-text">
                            All Quiz Result
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        </div>
</div>

<!-- END: Subheader -->
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Results(s)
                        <small>
                           Quiz Round Result(s)
                        </small>
                    </h3>

                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                @if(Session::has('errormessage'))
                      <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        <strong>
                            Oh snap!
                        </strong>{{ Session::get('errormessage') }}
                      </div>
                      @endif

                      @if(Session::has('successmessage'))
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        <strong>
                            Well done!
                        </strong>{{ Session::get('successmessage') }}
                    </div>
                @endif
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <select class="selectpicker" id="school_list" name="school_list">
                                    <option value="">Select School Name</option>
                                    @foreach($schools as $school)
                                    <option value="{{$school->id}}">{{$school->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Quiz Name" id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="m-input-icon ">
                                   <a href="javascript:;" class="btn btn-success" data-toggle="modal" data-target="#StudentQuizRankModal">Quiz Rank</a>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="m-input-icon ">
                                   <a href="javascript:;" class="btn btn-success" data-toggle="modal" data-target="#StudentQuizRoundRankModal">Quiz-Round Rank</a>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
<!--begin: Datatable -->
             <div class="loader_msg" style='display: block;'>
                <img src="{{ asset('assets/inner/loader.gif') }}" width='132px' height='132px' style="height: 70px;width: 67px;margin-left: 40%;">
            </div>
            <div class="result_datatable" id="ajax_data"></div>
            <!--end: Datatable -->
        </div>
    </div>
</div>



<!-- List of School Students that have assigned  quiz round -->
<div class="modal fade" id="StudentListModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <form name="fm-student">
                <div class="modal-body">
                    <div class="m-section m-section--last">
                      <div class="m-section__content">
                        <!--begin::Preview-->
                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                          <div class="m-demo__preview" style="padding: 0px 21px;">
                            <div class="m-list-search">
                              <div class="m-list-search__results">
                                <span class="m-list-search__result-category">
                                  Student List    <span id="TotalStudent" style="float: right;"></span>
                                </span>
                               
                                <table class="table">
                                <thead>
                                  <tr>
                                    <th>Name</th>
                                    <th>Duration</th>
                                    <th>Marks</th>
                                  </tr>
                                </thead>
                                <tbody id="StudentListing">
                                </tbody>
                              </table>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--end::Preview-->
                      </div>
                    </div>
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="" style="float: right;margin-bottom: 20px;">
                        Close
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="DeclareResultModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                  Declare Result 
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form name="fm-student" id="declare_result_form">
            <div class="modal-body">
              <input type="hidden" name="Declareschoolid" id="Declareschoolid" >
              <input type="hidden" name="Declareroundid" id="Declareroundid" >
             <p>Expiration Date: <span id="RoundExpirationDate"></span></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-primary" id="DeclareResultbtn" style="display: none;">
                    Declare Result
                </button>
            </div>
            </form>
        </div>
    </div>
    </div>


    <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <form name="fm-student" id="arrange-order-form">
                <div class="modal-body">
                    <h5 id="ResponseHeading"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadQuestionDatatable">
                        OK
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>


    <!-- modal for All over quiz rank  -->
    <!-- List of School Students that have assigned  quiz round -->
<div class="modal fade" id="StudentQuizRankModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                  Top 10 Students - Quiz
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>

                <form name="fm-student">
                <div class="modal-body">
                    
                    <div class="form-group">
                      <select class="form-control" id="SelectQuizSchoolname">
                        <option value="">Select School</option>
                        @foreach($declared_school as $school)
                        <option value="{{$school->id}}">{{$school->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    
                    <div class="form-group">
                      <select class="form-control" id="SelectQuizName">
                        <option value="">Select Quiz Name</option>
                        <!--@if(isset($declared_quiz_name) && $declared_quiz_name!='')-->
                        <!--@foreach($declared_quiz_name as $quiz_name)-->
                        <!--<option value="{{$quiz_name->quiz_id}}" school="{{$quiz_name->school_id}}">{{$quiz_name->title}}</option>-->
                        <!--@endforeach-->
                        <!--@endif-->
                      </select>
                    </div>

                    <div class="form-group" id="QuizRankTable" style="display: none;">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Rank</th>
                            <th>Name</th>
                            <th>Points</th>
                            <th>Time</th>
                          </tr>
                        </thead>
                        <tbody id="StudentQuizRankRows">
                        </tbody>
                      </table>
                    </div>
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="" style="float: right;margin-bottom: 20px;">
                        Close
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>


      <!-- List of School Students that have assigned  quiz round -->
<div class="modal fade" id="StudentQuizRoundRankModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                  Top 10 Students - Quiz-Round
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>

                <form name="fm-student">
                <div class="modal-body">
                    
                    <div class="form-group">
                      <select class="form-control" id="SelectSchoolname">
                        <option value="">Select School</option>
                        @foreach($declared_school as $school)
                        <option value="{{$school->id}}">{{$school->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    
                    <div class="form-group">
                      <select class="form-control" id="SelectQuizRoundName">
                        <option value="">Select Quiz Round</option>
                        <!-- @if(isset($declared_quiz) && $declared_quiz!='')-->
                        <!--@foreach($declared_quiz as $quiz)-->
                        <!--<option value="{{$quiz->round_id}}" school="{{$quiz->school_id}}">{{$quiz->quiz_name.' : '.$quiz->round_name}}</option>-->
                        <!--@endforeach-->
                        <!--@endif-->
                      </select>
                    </div>

                    <div class="form-group" id="QuizRoundRankTable" style="display: none;">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Rank</th>
                            <th>Name</th>
                            <th>Points</th>
                            <th>Time</th>
                          </tr>
                        </thead>
                        <tbody id="StudentQuizRoundRankRows">
                        </tbody>
                      </table>
                    </div>
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="" style="float: right;margin-bottom: 20px;">
                        Close
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_script')

<script>

     var datatable;
          (function() {
                $('.loader_msg').css('display','none');
                var view_url = "../administration/quiz_students_result";
                datatable = $('.result_datatable').mDatatable({
                // datasource definition
                data: {
                type: 'remote',
                source: {
                  read: {
                      url: APP_URL+'/administration/result/getAllResults',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                              generalSearch: '',
                          }
                      },
                      map: function(raw) {
                          console.log('ITS RUN');
                          console.log(raw);
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                               dataSet = raw.data;
                          }
                          console.log('Result');
                          console.log(dataSet);
                          return dataSet;
                      },
                  }
                },
                pageSize: 10,
                  saveState: {
                    cookie: false,
                    webstorage: false
                },

                serverPaging: true,
                serverFiltering: false,
                serverSorting: false
            },
          // layout definition
             layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            // search: {
            //   input: $('#generalSearch')
            // },

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
        field: "S.no",
        title: "S.No",
        width: 100
      },
      {
          field: "name",
          title: "School Name",
          textAlign: 'center'
      },
      {
          field: "quiz_name",
          title: "Quiz Name",
          textAlign: 'center'
      },
      {
          field: "round_name",
          title: "Round Name",
          textAlign: 'center'
      },
      {
          field: "expiration_date",
          title: "Expiration Date",
          textAlign: 'center',
          template: function(row) {
          var date = row.expiration_date.split(" ");
            return '\
            <span>'+date[0]+'</span>\
          ';
          }
      },
      {
          field: "attended",
          title: "Result Status",
          textAlign: 'center',
          template: function(row) {
          var status = row.is_result_declared;
          if(status == 2)
          {
            return '\
            <span>Declared</span>\
          ';
          }

          return '\
          <a href="javascript:;" class="btn btn-info" title="Declare Result" onclick=declareQuizRoundResult('+row.round_id+','+row.school_id+') style="margin-top: 5px;">\
               Declare Result\
              </a>\
          ';
          }
      },
      {
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          field: 'Actions',
          template: function(row) {
            var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

           return '\
            <a href="'+view_url+'/'+row.school_id+'/'+row.round_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Quiz Details">\
              <i class="la la-list"></i>\
            </a>\
          ';
          },
        }]
          });


          var query = datatable.getDataSourceQuery();

        var query = datatable.getDataSourceQuery();

       
      
        $('#formSubmit').on('click',function(){
          datatable.reload();
        });

         $("#LoadQuestionDatatable").on('click',function(){
          datatable.reload();
        });
        
        $('#generalSearch').on('change',function(){
          var school_list = $('#school_list').val();
          var value = $(this).val().trim();
          datatable.setDataSourceQuery({search:value,school_list:school_list});
          datatable.reload();
        });
        
        $('#school_list').on('change',function(){
          var search = $('#generalSearch').val().trim();
          var value = $(this).val();
          datatable.setDataSourceQuery({search:search,school_list:value});
          datatable.reload();
        });
    })();


function getresultDetail(round_id,school_id){
  
        var path = APP_URL + "/administration/getallstudentWithResult";
        $.ajax({
          type: "POST",
          url: path,
          data: {
            round_id: round_id,
            school_id: school_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);

            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.student));
              var studentHtml = '';
              $.each(data, function(idx,values){
                      studentHtml += '<tr><td><a href="javascript:;" class="m-list-search__result-item"><span class="m-list-search__result-item-pic"><img class="m--img-rounded" src="'+values.profile_img+'" title="" style="height: 35px;width: 35px"></span><span class="m-list-search__result-item-text">'+values.name+'</span></a></td><td>'+values.quiz_duration+'</td><td>'+values.score+'/'+values.total_question+'</td></tr>';
                   
                });
                $("#StudentListModal #StudentListing").html(studentHtml);
                 $("#StudentListModal #TotalStudent").html('Number of Students : '+res.total_student);
                $('#StudentListModal').modal('show');
              
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
  }


  function declareQuizRoundResult(round_id,school_id){
  
        var path = APP_URL + "/administration/declare_result";
        $.ajax({
          type: "POST",
          url: path,
          data: {
            round_id: round_id,
            school_id: school_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);

            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              if(res.declare_check==1){
                var declare_message = "Result Declaration time coming soon<br>Expiration Date : "+res.expiration_date;
              }else if(res.declare_check==2){
                var declare_message = "Result not declared yet<br>Expiration Date : "+res.expiration_date;
                $("#DeclareResultbtn").css('display','block');
              }
              $("#RoundExpirationDate").html(declare_message);
              
              $("#Declareschoolid").val(data.school_id);
              $("#Declareroundid").val(data.round_id);
              $('#DeclareResultModal').modal('show');

            }
          },
          error: function(){
            alert("Error");
          }
        }); 
  }

  $("#DeclareResultbtn").on('click',function(){
    var school_id = $("#Declareschoolid").val();
    var round_id = $("#Declareroundid").val();
    var path = APP_URL + "/administration/declare_result_for_school_and_round";
    $.ajax({
          type: "POST",
          url: path,
          data: {
            school_id: school_id,
            round_id: round_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);

            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              $('#DeclareResultModal').modal('hide');
             $("#ResponseSuccessModal").modal('show');
             $("#ResponseSuccessModal #ResponseHeading").text(res.message);


            }
          },
          error: function(){
            alert("Error");
          }
        });
  })

      

    $("#SelectQuizName").on("change",function(){
    if($('option:selected', this).val()!=''){
      var quiz_id =  $('option:selected', this).val();
      var school_id =  $('#SelectQuizSchoolname').val();
      var path = APP_URL + "/administration/quiz-result/selected_quiz_overall_rank";
      $.ajax({
          type: "POST",
          url: path,
          data: {
            quiz_id: quiz_id,
            school_id: school_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            console.log(result);
            var res = $.parseJSON(result);
           
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.student));
              
              
              var studentHtml = '';
              var count = 1;
              $.each(data, function(idx,values){
                 studentHtml += '<tr><td id="ranking'+count+'" >'+values.rank+'</td><td>'+values.name+'</td><td>'+values.score+'</td><td>'+values.quiz_duration+'</td></tr>';
                    count++;  
                     
              });

              $("#StudentQuizRankRows").html(studentHtml);
             
              
              $("#QuizRankTable").css('display','block');
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
    }else{
        $("#StudentQuizRankRows").html('');
        $("#QuizRankTable").css('display','none');
        //swal("Error","Select Quiz","error")
    }
   
 });
 
    $("#SelectSchoolname").on("change",function(){
        if($('option:selected', this).val()!='')
        {
            var school_id =  $('option:selected', this).val();
            var path = APP_URL + "/administration/quiz-result/selected_school_round";
            $.ajax({
                    type: "GET",
                    url: path,
                    data: {
                    school_id: school_id,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(result){
                    console.log(result);
                    var res = $.parseJSON(result);
                    
                    if(res.status == 'error'){
                    
                    }else{
                        var data = $.parseJSON(JSON.stringify(res.school_round));
                        $.each(data, function(idx,values){

                            $('#SelectQuizRoundName').find('option').remove().end();
                            var mySelect = $('#SelectQuizRoundName');
                            mySelect.append(
                                $('<option></option>').val('').html('Select Quiz Round'));
                           $.each(data, function(idx,values){
                               var data = values.title+' : '+values.round_name;
                                mySelect.append($('<option></option>').val(values.id).html(data));
                                 });
                        });
                    }
                },
                error: function(){
                    alert("Error");
                }
            }); 
        }
        else
        {
            $('#SelectQuizRoundName').html('<option value="">Select Quiz Round</option>');
            $("#StudentQuizRoundRankRows").html('');
            $("#QuizRoundRankTable").css('display','none');
            //swal("Error","Select Quiz","error")
        }
   
    });
    
        $("#SelectQuizSchoolname").on("change",function(){
        if($('option:selected', this).val()!='')
        {
            var school_id =  $('option:selected', this).val();
            var path = APP_URL + "/administration/quiz-result/selected_school_quiz";
            $.ajax({
                    type: "GET",
                    url: path,
                    data: {
                    school_id: school_id,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(result){
                    console.log(result);
                    var res = $.parseJSON(result);
                    
                    if(res.status == 'error'){
                    
                    }else{
                        var data = $.parseJSON(JSON.stringify(res.school_quiz));
                        $('#SelectQuizName').find('option').remove().end();
                            var mySelect = $('#SelectQuizName');
                            mySelect.append(
                                $('<option></option>').val('').html('Select Quiz Name'));
                            $.each(data, function(idx,values){
                                mySelect.append($('<option></option>').val(values.id).html(values.title));
                                 });
                    }
                },
                error: function(){
                    alert("Error");
                }
            }); 
        }
        else
        {
            $('#SelectQuizName').html('<option value="">Select Quiz Name</option>');
            $("#StudentQuizRankRows").html('');
            $("#QuizRankTable").css('display','none');
            //swal("Error","Select Quiz","error")
        }
   
    });




 $("#SelectQuizRoundName").on('change',function(){
  if($('option:selected', this).val()!=''){
   var round_id =  $('option:selected', this).val();
   var school_id =  $("#SelectSchoolname").val();
   var path = APP_URL + "/administration/quiz-result/selected_quiz_rank";
   $.ajax({
          type: "POST",
          url: path,
          data: {
            round_id: round_id,
            school_id: school_id,
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            console.log(result);
            var res = $.parseJSON(result);
           
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.student));
              var studentHtml = '';
              $.each(data, function(idx,values){
                         studentHtml += '<tr><td>'+values.rank+'</td><td>'+values.name+'</td><td>'+values.final_score+'</td><td>'+values.duration+'</td></tr>';
                     
              });
              
             
              $("#StudentQuizRoundRankRows").html(studentHtml);
              $("#QuizRoundRankTable").css('display','block');
             
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
    }else{
         //swal('Error','Please Select Quiz-Round Name','error');
         $("#StudentQuizRoundRankRows").html('');
          $("#QuizRoundRankTable").css('display','none');
    }
  
 })

</script>
@endsection


