@extends('layouts.default')
@section('page_title') 
Quiz
@endsection 
@section('page_css')

@endsection
@section('content')
<div class="m-subheader ">
        <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
               Manage Quiz(s)
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
            <li class="m-nav__item m-nav__item--home">
                <a href="{{url('administration/home')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                </a>
            </li>
            <li class="m-nav__separator">
              -
            </li>
            <li class="m-nav__item">
              <a href="{{url('administration/quiz')}}" class="m-nav__link">
                <span class="m-nav__link-text">
                  Manage Quiz
                </span>
              </a>
            </li>
             </ul>
        </div>
        </div>
</div>


 <div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Quiz 
                        <small>
                           List of added quiz 
                        </small>
                    </h3>

                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <a href="javascript:;" class="btn btn-primary CreateQuizButton">
                          Add Quiz
                        </a>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
<!--begin: Datatable -->
            <div class="loader_msg" style='display: block;'>
                <img src="{{ asset('assets/inner/loader.gif') }}" width='132px' height='132px' style="height: 70px;width: 67px;margin-left: 40%;">
            </div>
            <div class="quiz_datatable" id="QuizData"></div>
            <!--end: Datatable -->
        </div>
    </div>
    </div>

  


<div class="modal fade" id="EditQuizTitleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                    &times;
                </span>
            </button>
        </div>
        <form name="fm-student" id="edit_quiz_title_form">
        <div class="modal-body">
          {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="form-control-label">
                        Quiz Ttitle:
                    </label>
                    <input type="text" class="form-control" id="quiz_title" name="quiz_title">
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>
            <button type="submit" class="btn btn-primary" id="QuizTitleUpdatebtn">
                Save changes
            </button>
        </div>
        </form>
    </div>
</div>
</div>


<div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                
                <form name="fm-student" id="arrange-order-form">
                <div class="modal-body">
                    <h5 id="ResponseHeading"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="LoadQuestionDatatable">
                        OK
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('page_script')
<script>
   var datatable;
          (function() {
                $('.loader_msg').css('display','none');
                datatable = $('.quiz_datatable').mDatatable({
                // datasource definition
                data: {
                type: 'remote',
                source: {
                  read: {
                      url: APP_URL+'/administration/getAllQuiz',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                              generalSearch: '',
                          }
                      },
                      map: function(raw) {
                          console.log('ITS RUN');
                          console.log(raw);
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                               dataSet = raw.data;
                          }
                          console.log('Result');
                          console.log(dataSet);
                          return dataSet;
                      },
                  }
                },
                pageSize: 10,
                  saveState: {
                    cookie: false,
                    webstorage: false
                },

                serverPaging: true,
                serverFiltering: false,
                serverSorting: false
            },
          // layout definition
            layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
              input: $('#generalSearch')
            },

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
             field: "S.no",
             title: "S.No",
              textAlign: 'center'
            }, {
              field: "title",
              title: "Quiz Name"
            }, {
              field: "status",
              title: "Status",
              template: function(row) {
                var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';
                if(row.status==0){
                     return '\
                <p>Active</p>\
              ';
                }else{
                     return '\
                <p>Inactive</p>\
              ';
                }
              },
            },{
              field: 'Actions',
              width: 110,
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              template: function(row) {
                var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';

                return '\
                <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill DeleteQuiz" title="Manage Rounds" onclick=getquizDetail('+row.id+')>\
                  <i class="la la-edit"></i>\
                </a>\
                <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete Quiz" onclick=deleteQuiz('+row.id+')>\
                  <i class="la la-trash"></i>\
                </a>\
                \
                <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Quiz" onclick=editQuizTitle('+row.id+')>\
                  <i class="la la-info"></i>\
                </a>\
              ';
              },
          }]
          });


          var query = datatable.getDataSourceQuery();
        
        $("#LoadQuestionDatatable").on('click',function(){
          datatable.reload();
        });

    })();


    function getquizDetail(id){
        var round_id = id;
        window.location.href = APP_URL+"/administration/manage-round/"+round_id;
             
    }

    function deleteQuiz(id){
    var path = APP_URL + "/administration/quiz/delete";
    var _this = $(this);
    swal({
      title: "Are you sure to delete this Quiz?",
      text: "Your will not be able to recover data related to this Quiz!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(isConfirm) {
      if (isConfirm) {
        var data = id;
        $.ajax({
          type: 'POST',
          url: path,
          data: {
            id: data,
          },
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          success: function(data) {
            var res = $.parseJSON(data);
            if(res.status == 'error'){
              swal('Error',res.message,'error');
            }else{
               $('.sweet-overlay').remove();
               $('.showSweetAlert ').remove();
              $("#ResponseSuccessModal").modal('show');
              $("#ResponseSuccessModal #ResponseHeading").text(res.message);
            } 
          },
          error: function(data) {
            swal('Error',data,'error');
          }
        });
      } else {

      }
    });
  }


  function editQuizTitle(id){
        var path = APP_URL + "/administration/quiz/editquiz";
        $.ajax({
          type: "POST",
          url: path,
          data: {
            id: id
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);

            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              $('#EditQuizTitleModal').data('id',data.id);
              $('#EditQuizTitleModal').find('.modal-title').html('Update quiz ' + data.title);
              $('#quiz_title').val(data.title);
              $('#EditQuizTitleModal').modal('show');
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
  }


  $('#QuizTitleUpdatebtn').click(function(e){
    e.preventDefault();
    
    var id = $('#EditQuizTitleModal').data('id');
    var quiz_title  = $('#quiz_title').val();
    var path = APP_URL+'/administration/quiz/update_quiz';
    $.ajax({
      type: 'POST',
      url: path,
      data: {
        id: id,
        title: quiz_title
      },
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
      success: function(data) {
        var res = $.parseJSON(data);
        if(res.status == 'error'){
          swal('Error',res.message,'error');
        }else{
          $('#EditQuizTitleModal').modal('toggle');
          $('#quiz_title').val('');
          $('#EditQuizTitleModal').data('id','');
         $("#ResponseSuccessModal").modal('show');
         $("#ResponseSuccessModal #ResponseHeading").text(res.message);
        } 
      },
      error: function(data) {
        swal('Error',data,'error');
      }
    });
  });
</script>
@endsection
