@extends('layouts.default')
@section('page_title') 
Add Quiz
@endsection 
@section('page_css')

@endsection
@section('content')
<input type="hidden" name="id" id="adminid" value="{{ Auth::user()->id }}">
<div class="m-subheader ">
        <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Add Quiz
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{route('quiz')}}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-th-list"></i>
                    </a>
                </li>
                <li class="m-nav__item m-nav__item--home">
                    -
                </li>
                <li class="m-nav__item m-nav__item--home">
                    <span>Number Of Question - <span id="NumberQuestion">0</span></span>
                </li>
            </ul>

            <button type="button" class="btn btn-success" iD>
                Success
            </button>
        </div>
        </div>
    </div>

    <div class="m-content">
    <div class="col-md-12">
       
       <div class="m-portlet m-portlet--tab" id="AddQuizFormDiv">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                       Add Quiz
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right">
            <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <label for="quiz-title-input" class="col-2 col-form-label">
                   Quiz Title
                </label>
                <div class="col-10">
                    <input class="form-control m-input" type="text" value="" id="quiz-title-input" name="quiz-title-input">
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label for="quiz-desc-input" class="col-2 col-form-label">
                   About Quiz
                </label>
                <div class="col-10">
                    <textarea class="form-control m-input" id="quiz-desc-input" name="quiz-desc-input"></textarea>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label for="quiz-status-input" class="col-2 col-form-label">
                    Quiz Status
                </label>
                <div class="col-10" >
                    <select id="quiz-status-input" name="quiz-status-input" class="form-control m-input" >
                        <option value="">Select Status</option>
                        <option value="0">Active</option>
                        <option value="1">Inactive</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-10">
                        <button type="button" class="btn btn-success" id="AddQuiz">
                            Continue
                        </button>
                        <button type="button" class="btn btn-secondary" id="CancelQuiz">
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
        </form>
      </div>



      <div class="m-portlet m-portlet--tab" id="AddQuestionFormDiv" style="display: none;">
        <input type="hidden" name="QuizID" id="QuizID">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                       Add Question
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right">
            <div class="m-portlet__body">
                <div class="form-group m-form__group m--margin-top-10">
                    <div class="alert m-alert m-alert--default" role="alert" id="Quiztitle">
                        
                    </div>
                </div>

            <div class="form-group m-form__group row">
                <label for="question-type-input" class="col-2 col-form-label">
                    Question Type
                </label>
                <div class="col-10" >
                    <select id="question-type-input" name="question-type-input" class="form-control m-input" >
                        <option value="">Select Question Type</option>
                        <option value="0">Short Answer</option>
                        <option value="1">Multi Choice</option>
                        <option value="2">Arrange Order</option>
                    </select>
                </div>
            </div>

            <!-- Short Answer -->
            <div id="ShortAnwerDiv" style="display: none">
            <div class="form-group m-form__group row">
                <label for="short-ques-input" class="col-2 col-form-label">
                    Question
                </label>
                <div class="col-10" >
                    <input id="short-ques-input" name="short-ques-input" class="form-control m-input" type="text" placeholder="Enter Question" >
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="short-ans-input" class="col-2 col-form-label">
                    Answer
                </label>
                <div class="col-10" >
                    <input id="short-ans-input" name="short-ans-input" class="form-control m-input" type="text" placeholder="Enter Answer" >
                </div>
            </div>
            </div>
            <!-- End Short Answer -->

            <!-- Multi Choice -->
            <div id="MultiChoiceDiv" style="display: none">
            <div class="form-group m-form__group row">
                <label for="multi-ques-input" class="col-2 col-form-label">
                    Question
                </label>
                <div class="col-10" >
                    <input id="multi-ques-input" name="multi-ques-input" class="form-control m-input" type="text" placeholder="Enter Question" >
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="multi-ans-input-option1" class="col-2 col-form-label">
                    Option A
                </label>
                <div class="col-10" >
                    <input id="multi-ans-input-option1" name="multi-ans-input-option1" class="form-control m-input" type="text" placeholder="Enter Option 1" >
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="multi-ans-input-option2" class="col-2 col-form-label">
                    Option B
                </label>
                <div class="col-10" >
                    <input id="multi-ans-input-option2" name="multi-ans-input-option2" class="form-control m-input" type="text" placeholder="Enter Option 2" >
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="multi-ans-input-option3" class="col-2 col-form-label">
                    Option C
                </label>
                <div class="col-10" >
                    <input id="multi-ans-input-option3" name="multi-ans-input-option3" class="form-control m-input" type="text" placeholder="Enter Option 3" >
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="multi-ans-input-option4" class="col-2 col-form-label">
                    Option D
                </label>
                <div class="col-10" >
                    <input id="multi-ans-input-option4" name="multi-ans-input-option4" class="form-control m-input" type="text" placeholder="Enter Option 4" >
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="multi-ans-input" class="col-2 col-form-label">
                   Answer
                </label>
                <div class="col-10" >
                    <select id="multi-ans-input" name="multi-ans-input" class="form-control m-input" >
                        <option value="">Select Option</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                    </select>
                </div>
            </div>
            </div>
            <!-- End Multi Choice -->

            <!-- Arrange Order -->
            <div id="ArrangeOrderDiv" style="display: none">
            <div class="form-group m-form__group row">
                <label for="arrange-input-desc" class="col-2 col-form-label">
                    Description
                </label>
                <div class="col-10" >
                    <input id="arrange-input-desc" name="arrange-input-desc" class="form-control m-input" type="text" placeholder="Enter Description" >
                </div>
            </div>
                
            <div class="form-group m-form__group row">
                <label for="arrange-input-order1" class="col-2 col-form-label">
                    Part A
                </label>
                <div class="col-10" >
                    <input id="arrange-input-order1" name="arrange-input-order1" class="form-control m-input" type="text" placeholder="Enter Part A of Question" >
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="arrange-input-order2" class="col-2 col-form-label">
                    Part B
                </label>
                <div class="col-10" >
                    <input id="arrange-input-order2" name="arrange-input-order2" class="form-control m-input" type="text" placeholder="Enter Part B of Question" >
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="arrange-input-order3" class="col-2 col-form-label">
                    Part C
                </label>
                <div class="col-10" >
                    <input id="arrange-input-order3" name="arrange-input-order3" class="form-control m-input" type="text" placeholder="Enter Part C of Question" >
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="arrange-input-order4" class="col-2 col-form-label">
                    Part D
                </label>
                <div class="col-10" >
                    <input id="arrange-input-order4" name="arrange-input-order4" class="form-control m-input" type="text" placeholder="Enter Part D of Question" >
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="arrange-input-order4" class="col-2 col-form-label">
                    Answer
                </label>
                <div class="col-10" >
                    <input id="arrange-input-ans" name="arrange-input-ans" class="form-control m-input" type="text" placeholder="Enter Order (eg: A-B-C-D)"  >
                </div>
            </div>
            </div>
            <!-- End Multi Choice -->
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-10">
                        <button type="reset" class="btn btn-success" id="AddQuestion">
                            Add Question
                        </button>
                        <button type="reset" class="btn btn-secondary" id="CancelAddQue">
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
        </form>
      </div>


      <div class="m-portlet m-portlet--tabs" id="AddAnotherDiv" style="display: none;">
      <div class="m-portlet__body" >
        <!--begin::Add Another-->
        <div class="m-section">
            <div class="m-section__content">
                <div class="m-demo m-demo--last" data-code-preview="true" data-code-html="true" data-code-js="false" style="text-align: center;">
                    <div class="m-demo__preview" >
                        <button type="button" class="btn m-btn--pill    btn-outline-success btn-block" style="width: 48%;display: inline;" id="AddAnotherbtn">
                            Add Aother Question
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Section-->
      </div>
      </div>


    </div>
    </div>

@endsection

@section('page_script')
<script>

    $("#arrange-input-ans").focusout(function() {
        var $regexname= "[A-D]{1}[ -][A-D]{1}[ -][A-D]{1}[ -][A-D]{1}";
         if (!$(this).val().match($regexname)) {
              // there is a mismatch, hence show the error message
                 swal("Error","Enter correct pattern (eg.  A-B-C-D)","error");
             }
  })

    $("#question-type-input").on('change', function(){
        if($(this).val()==0){
            $("#MultiChoiceDiv").css('display','none');
            $("#ArrangeOrderDiv").css('display','none');
            $("#ShortAnwerDiv").slideDown();
        }else if($(this).val()==1){
            $("#ArrangeOrderDiv").css('display','none');
            $("#ShortAnwerDiv").css('display','none');
            $("#MultiChoiceDiv").slideDown();
        }else if($(this).val()==2){
            $("#ShortAnwerDiv").css('display','none');
            $("#MultiChoiceDiv").css('display','none');
            $("#ArrangeOrderDiv").slideDown();
        }else{
            $("#ShortAnwerDiv").css('display','none');
            $("#MultiChoiceDiv").css('display','none');
            $("#ArrangeOrderDiv").css('display','none');
        }
    })

    $("#AddQuiz").on('click', function(){
        var AdminId = $("#adminid").val();
        var title = $("#quiz-title-input").val();
        var quiz_status = $("#quiz-status-input").val();
        var quiz_desc = $("#quiz-desc-input").val();
        var pathname = window.location.pathname;
        if(title=='' || quiz_status=='' || quiz_desc=='' ){
            swal("Error","Please Enter Quiz Name,Description and Select Status","error");
        }else{
            $.ajax({
              type: 'POST',
              url: pathname+'/store',
              data: {
                user_id: AdminId,
                title: title,
                status: quiz_status,
                quiz_desc : quiz_desc
              },
               headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
              success: function(data) {
                var res = $.parseJSON(data);
                if(res.status == 'error'){
                  swal('Error',res.message,'error');
                }else{
                   $("#QuizID").val(res.quiz_id);
                   $("#Quiztitle").text(res.quiz_title);
                  swal('Success',res.message,'success');
                  $("#AddQuizFormDiv").css("display","none");
                  $("#AddQuestionFormDiv").slideDown();
                } 
              },
              error: function(data) {
                swal('Error',data,'error');
              }
            });
        }
    })

    $("#AddQuestion").on('click', function(){
        var pathname = window.location.pathname;
        var que_type = $("#question-type-input").val();
        var quiz_id = $("#QuizID").val();
        if(que_type==''){
            swal("Error","Select Question Type","error");
        }else{

            if(que_type==0){
                var question = $("#short-ques-input").val();
                var answer = $("#short-ans-input").val();
            }else if(que_type==1){
                var question = $("#multi-ques-input").val();
                var answer  = $("#multi-ans-input").val();
            }else{
                var question = $("#arrange-input-desc").val();
                var answer  = $("#arrange-input-ans").val();
            }
        
        
              var multi_option1 = $("#multi-ans-input-option1").val(); 
              var multi_option2 = $("#multi-ans-input-option2").val();
              var multi_option3 = $("#multi-ans-input-option3").val();
              var multi_option4 = $("#multi-ans-input-option4").val();
            
            
              var arrange_part1 = $("#arrange-input-order1").val(); 
              var arrange_part2 = $("#arrange-input-order2").val(); 
              var arrange_part3 = $("#arrange-input-order3").val(); 
              var arrange_part4 = $("#arrange-input-order4").val(); 

            $.ajax({
              type: 'POST',
              url: pathname+'/store_question',
              data: {
                quiz_id:quiz_id,
                question_type: que_type,
                question: question,
                answer:answer,
                multi_option1:multi_option1,
                multi_option2:multi_option2,
                multi_option3:multi_option3,
                multi_option4:multi_option4,
                arrange_part1:arrange_part1,
                arrange_part2:arrange_part2,
                arrange_part3:arrange_part3,
                arrange_part4:arrange_part4
              },
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
              success: function(data) {
                var res = $.parseJSON(data);
                if(res.status == 'error'){
                  swal('Error',res.message,'error');
                }else{
                  swal('Success',res.message,'success');
                  $("#NumberQuestion").text(res.totalQue);
                  $("#AddQuestionFormDiv").css('display','none');
                  $("#AddAnotherDiv").slideDown();
                } 
              },
              error: function(data) {
                swal('Error',data,'error');
              }
            });
        }
    })

    $("#AddAnotherbtn").on('click', function(){
       $("#question-type-input").val('');
       $("#short-ques-input").val('');
       $("#short-ans-input").val('');
       $("#multi-ques-input").val('');
       $("#multi-ans-input-option1").val(''); 
       $("#multi-ans-input-option2").val('');
       $("#multi-ans-input-option3").val('');
       $("#multi-ans-input-option4").val('');
       $("#multi-ans-input").val('');
       $("#arrange-input-ans").val('');
       $("#arrange-input-order1").val(''); 
       $("#arrange-input-order2").val(''); 
       $("#arrange-input-order3").val(''); 
       $("#arrange-input-order4").val(''); 
       $("#AddAnotherDiv").css('display','none');
       $("#ShortAnwerDiv").css('display','none');
       $("#MultiChoiceDiv").css('display','none');
       $("#ArrangeOrderDiv").css('display','none');
       $("#AddQuestionFormDiv").slideDown();
    })
</script>
@endsection


