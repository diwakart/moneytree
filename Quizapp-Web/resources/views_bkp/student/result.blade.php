@extends('layouts.student')
@section('page_title')
Result
@endsection
@section('page_css')
<link href="{{ asset('assets/student/css/enjoyhint.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('content')


<div class="s_main"> 
<section class="quiz_div">  
    <div class="row">
        <div class="col-xs-12">
            <div class="quiz_title">
            
            <img src="{{ asset('assets/student/images/left_triangle.jpg') }}" />

            <div class="tune_talk_history">
             <h4 class=""><span>Thankyou for completing <strong>{{$result->round_name}}</strong> </span> &nbsp;| <span style="color: #ed3125;">Result will be declared on {{date("F jS, Y", strtotime(date('Y-m-d H:i:s',strtotime($result->expiration_date . "+1 days"))))}}</span> </h4>

                </div>
            </div>
            <div class="tankTune">
               
                    <p>To reload your Tune Talk Sim Card, Click Here : </p>
                 <a href="http://www.tunetalk.com/" target="_blank"><img src="{{ asset('assets/student/images/icons/new_logo.png') }}"></a>
                </div>
        </div> 
        
    </div>
  </section>
 
   
     <section class="tune_talk_history_block">
    <div class="row">
        <div class="col-xs-9">
             <div class="question_div thankDiv">
                <div class="question_block text-right"> 
                    <h5>Answered Correctly</h5>
                        <h3>{{$result->score}}/{{$result->total_question}}</h3>
                  <div class="symobol"><i class="fa fa-check"></i></div>  
                </div>       
            </div> 
             <div class="question_div thankDiv">
              <div class="question_block text-right"> 
                  <h5>Score</h5>
                     @php($precentage_resul = ($result->score/$result->total_question)*100)
                      <h3>{{number_format((float)$precentage_resul, 2, '.', '')}}%</h3>
                  <div class="symobol"><i class="fa fa-thumbs-up"></i></div>  
              </div>     
            </div> 
             <div class="question_div thankDiv">
                <div class="question_block text-right"> 
                    <h5>Time Spent</h5>
                        <h3>{{$result->quiz_duration}}</h3>
                  <div class="symobol"><i class="clock_icon"><img src="{{asset('assets/student/images/new_icon/clock.png')}}"></i></div>  
            </div>                
                          
            </div> 
            </div>
             
         <div class="col-xs-3">
          @php($encparameter = Crypt::encrypt($roundid.'^'.Auth::user()->id))
          @php($share_url = url('/').'/participated_student_result/'.$encparameter)
              <ul class="thanksocial">
                 <li><a href="https://www.facebook.com/sharer/sharer.php?u={{$share_url}}"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="https://twitter.com/intent/tweet?text={{'Quiz - '.$result->round_name.' Result'}}&amp;url={{$share_url}}"><i class="fa fa-twitter"></i></a></li> 
                  <li><a href="https://plus.google.com/share?url={{$share_url}}"><i class="fa fa-google-plus"></i></a></li> 
                  <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{$share_url}}&amp;title={{'Quiz - '.$result->round_name.' Result'}}&amp;summary=Check my Score"><i class="fa fa-linkedin"></i></a></li> 
                </ul>   
        </div>
        </div> 
         <div class="row pdt30">
        <div class="col-xs-12 text-center">
            <p>Something about this Quiz does not feel right?</p>
            <p><a href="https://www.quizme.com.my/#contact">Give Feedback</a></p>
            <button class="button_1" id="GoHome">Home</button>
             </div>
         </div>
    </section>
        
     
</div>
      
@endsection



@section('page_js')

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="{{ asset('assets/student/js/share.js') }}"></script>
<script>
$("#GoHome").on('click',function(){
        window.location.href= APP_URL+"/student/home";
    })
</script>
@endsection
