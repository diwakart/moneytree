@extends('layouts.student')
@section('page_title')
All Quiz
@endsection
@section('content')

@php($currentdate = date('Y-m-d', time()))
<div class="s_main"> 
<section class="quiz_div"> 
    <div class="np">
    <div class="row">
        <div class="col-xs-7"><div class="quiz_title">
            <img src="{{ asset('assets/student/images/left_triangle.jpg') }}" />
            <h3 class="">Active Quiz</h3></div>
        </div>  
        <div class="col-xs-5"> 
            <button type="button" class="btn enter-quiz GameRulesModal">View Rules</button>
        </div> 
    </div>
</section>
 
<section class="question_section">
  <div class="row">
    <div class="col-xs-12">
         @if($Assigned_Quiz_Count>0)
            @foreach($Assigned_Quizes as $quiz)
             @inject('round_name','App\PublishQuizRound')
             @inject('quiz_name','App\Quiz')
             @inject('school_name','App\School')
             @inject('publish_question','App\PublishQuestions')

              @php($quiz_round_name = $round_name->where('id','=',$quiz->round_id)->first())
              @php($quiz_name = $quiz_name->where('id','=',$quiz_round_name->quiz_id)->first())
              @php($school_name = $school_name->where('id','=',$quiz->school_id)->first())
               @php($questions = $publish_question->where(['quiz_round_id'=>$quiz->round_id,'published'=>1])->count())
                <div class="question_div question_div_dashboard">
                  <div class="question_block quiz_school">
                  <h2>{{$quiz_name->title}} : {{$quiz_round_name->round_name}}</h2>
                  <h5 class="ml-18">{{$school_name->name}} | Question : {{$questions}}</h5>
                  </div>
                  <div class="question_start">
                   <a href="javascript:;" onclick="AttendQuiz({{$quiz->id}})"  class="btn start_quiz_btn">START QUIZ</a>
                  </div>
                </div>
           @endforeach
              @else
            <div class="question_div question_div_dashboard">
              <div class="question_block">
              <h2>No Quiz Available</h2>
              </div>
            </div>
           @endif
          <div class="notice_div">
              <img src="{{ asset('assets/student/images/price.png') }}">
              <p> Be among the top 10 for this quiz <br> to win a prize! </p>
          </div>
    </div>
  </div>
</section>



<section class="result_section" >
 @if($recent_quiz_score_count>0)
  <div class="postList">
  @foreach($recent_quiz_score as $quiz_score)
  @php($postID = $quiz_score->id)
  @if($currentdate<$quiz_score->expiration_date)
   
  <div class="row">
    <div class="col-xs-12">
      <div class="question_div">
        <div class="question_block result_pad">
          <h4 class="score_title">Score 
            @if($quiz_score->earn_points==1)
            <span class="refreal_point">Referral Point
            <badge>1</badge>  </span>
            @endif
          </h4>
          <h3>{{$quiz_score->score}}/{{$quiz_score->total_question}}</h3>
          <h4 class="tune_title">{{$quiz_score->quiz_name.' - '.$quiz_score->round_name}}</h4>
          <h3>{{$quiz_score->quiz_duration}}</h3>
        </div>
        <div class="question_start">
          @if($quiz_score->is_result_declared==1)
            @php($exp_date = date("F jS, Y", strtotime(date('Y-m-d H:i:s',strtotime($quiz_score->expiration_date . "+1 days")))))
           <button type="button" class="btn start_quiz_btn graybtn" id="ViewDetailModal" onclick="sweetAlert('','Result will be declared on {{$exp_date}}','warning')">VIEW DETAILS</button>
          @else
           <button type="button" class="btn start_quiz_btn redbtn" id="ViewDetailModal" onclick="ViewDetailQuizRank({{$quiz_score->id}},{{$quiz_score->attended}},{{$quiz_score->round_id}})">VIEW DETAILS</button>
          @endif
          
        </div> 
      </div>
    </div>
  </div>
  @endif
  @endforeach
  @if($recent_quiz_score_count>2)
    <div class="show_more_main" id="show_more_main{{$postID}}">
    <span id="{{$postID}}" class="show_more btn" title="Load more results">Show more</span>
    <span class="loding" style="display: none;"><img src="{{asset('assets/student/images/loading.gif')}}" class="loding_txt"></span>
    </div>
  @endif
  </div>
  @else
  @php($postID = '')
  <div class="row">
    <div class="col-xs-12">
      <div class="question_div">
        <div class="question_block result_pad">
           <h4 class="score_title">No Result Available</h4>
           <br>
        </div>
      </div>
    </div>
  </div>
  @endif 
</section>
        
        
  <!-- Modal for View details===== -->
 <div class="modal fade" id="ViewQuizResult" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="ViewSchoolName"></h4>
                    </div>
                <div class="modal-body">
                    <center>
                    <img src="{{ asset('assets/student/images/quiz.png') }}" name="aboutme" width="90" height="90" border="0" class="img-circle"></a>
                    <h3 class="media-heading" id="QuizName"></h3>
                    <p id="RoundName"></p>
                    <span><strong>Attended on : </strong></span>
                        <span class="label label-info" id="AttendedOn"></span>
                    </center>
                    <hr>
                    <center>
                    <p style="text-align: center"><strong>Details: </strong><br>
                        Number of Questions : <span id="QuestionNumber"></span><br>
                        Score : <span id="ScoreNumber"></span><br>
                        Duration : <span id="DurationQuiz"></span><br>
                      </p>
                    <br>
                    </center>
                </div>
                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
<!-- End Modal for view details -->

   <!-- Modal for Rules of Game===== -->
 <div class="modal fade" id="ViewRulesofGame" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-center">RULES OF THE GAME</h4>
                    </div>
                <div class="modal-body">
                  <center>
                   <p>1. There will be four rounds of quizzes in total.</p>
                   <p>2. There are 6 questions in each rounds. You are required to answer all questions to complete the Rounds</p>
                   <p>3. You will be ranked according to the number of points you have earned and the amount of time you took to complete each round. You would be awarded ONE point for every correct answer and you earn NO points for wrong answers. Your timing for completing each round will be taken based on Timer that appears on screen.</p>
                   <p>4. There will be two ranking boards (1) Round Leaderboard - Ranking for each Round (2) Overall Leaderboard - Ranking for the entire Quiz</p>
                   <p>5. You may also earn a Bonus Point for referring ONE Friend to join the Quiz Round.</p>
                  </center>
                </div>
                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-default red-btn" data-dismiss="modal">Close</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
<!-- End Modal for view details -->   
@endsection
@section('page_js')
<script>
var gift_id = "{{$postID}}";

$(".GameRulesModal").on('click',function(){
    $("#ViewRulesofGame").modal('show');
})

 /* function ViewDetailQuizResult(id){
        var path = APP_URL + "/student/view_detail_quiz_result";
        $.ajax({
          type: "POST",
          url: path,
          data: {
            result_id: id
          },
           headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          success: function(result){
            var res = $.parseJSON(result);
            if(res.status == 'error'){

            }else{
              var recent_quiz_score = $.parseJSON(JSON.stringify(res.recent_quiz_score));
              var recent_quiz_school = $.parseJSON(JSON.stringify(res.recent_quiz_school));
                var quiz_date = recent_quiz_score.created_at;
                $('#ViewQuizResult').modal('show');
                $('#ViewQuizResult #ViewSchoolName').text(recent_quiz_school.name);
                $("#QuizName").text(recent_quiz_score.quiz_name);
                $("#RoundName").text(recent_quiz_score.round_name);
                $("#QuestionNumber").text(recent_quiz_score.total_question);
                $("#ScoreNumber").text(recent_quiz_score.score+"/"+recent_quiz_score.total_question);
                $("#DurationQuiz").text(recent_quiz_score.quiz_duration);
                $("#AttendedOn").text(new Date(quiz_date).toDateString());

                
              
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
  }
*/

 function ViewDetailQuizRank(id,attended,round_id){
       if(attended==3){
         sweetAlert('','Result not declared yet','warning');
       }else{
         window.location.href=APP_URL+"/student/student-rank/"+round_id;
       }
  }


  function AttendQuiz(id){
    var id = id;
    if(id==''){
          swal("Error","Some error occured","error");
      }else{
          $.ajax({
            type: 'POST',
            url: APP_URL+'/student/quiz_attended',
            data: {
              id: id
            },
             headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            success: function(data) {
              var res = $.parseJSON(data);
              if(res.status == 'error'){
                swal('Error',res.message,'error');
              }else{
                window.location.href = APP_URL + "/student/quiz/"+id;
              } 
            },
            error: function(data) {
              swal('Error',data,'error');
            }
          });
      }
  }

/*Load More*/
$(function() {   
    $(document).on( "click", ".show_more", function(ev) {
        var ID = gift_id;
        $('.show_more').hide();
        $('.loding').show();
        $.ajax({
            type:'POST',
            url: APP_URL+'/student/load_result',
            data:{
              id:ID,
            },
             headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            
            success: function(data) {
              var res = $.parseJSON(data);
              if(res.status == 'error'){
                swal('Error',res.message,'error');
              }else{
                var data = $.parseJSON(JSON.stringify(res.quiz_res));
                var studentHtml='';
                var post_id='';
                var showmore='';
                $.each(data, function(idx,values){
                   post_id = values.id;

                    if(values.earn_points==1){
                    var refereal_spoint = '<span class="refreal_point">Referral Point<badge>1</badge>  </span>';
                   }else{
                    var refereal_spoint = '';
                   }
                  
                   if(values.is_result_declared==1){
                       
                      var msg = "sweetAlert('','Result will be declared on "+values.expiration_date+"','warning')";
                    var btnView = '<button type="button" class="btn start_quiz_btn graybtn" id="ViewDetailModal" onclick="'+msg+'">VIEW DETAILS</button>';
                    
                   }else{
                    var btnView = '<button type="button" class="btn start_quiz_btn redbtn" id="ViewDetailModal" onclick="ViewDetailQuizRank('+values.id+','+values.attended+','+values.round_id+')">VIEW DETAILS</button>';
                   }
                 
                 
                    studentHtml += '<div class="row"><div class="col-xs-12"><div class="question_div"><div class="question_block result_pad"><h4 class="score_title">Score '+refereal_spoint+'</h4><h3>'+values.score+'/'+values.total_question+'</h3><h4 class="tune_title">'+values.quiz_name+' - '+values.round_name+'</h4><h3>'+values.quiz_duration+'</h3></div><div class="question_start">'+btnView+'</div> </div></div></div>';
                  


                   
                });
               
                 if(res.totalRowCount>4){
                    showmore = '<div class="show_more_main" id="show_more_main'+post_id+'"><span id="'+post_id+'" class="show_more btn" title="Load more posts">Show more</span><span class="loding" style="display: none;"><img src="'+APP_URL+'/assets/student/images/loading.gif" class="loding_txt"></span></div>';
                    
                  }
                  
                  gift_id = post_id;
                 $('#show_more_main'+ID).remove();
                 $('.postList').append(studentHtml);
                 $('.postList').append(showmore);
                  }
              } ,
              error: function(data) {
              swal('Error',data,'error');
            }
            
        });
    }); 
});

  </script>
  @endsection