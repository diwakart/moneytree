@extends('layouts.student')
@section('page_title')
Profile
@endsection
@section('page_css')
<link href="{{ asset('assets/student/css/enjoyhint.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('content')


<div class="s_main"> 
<section class="quiz_div"> 
    <div class="np">
    <div class="row">
        <div class="col-xs-5"><div class="quiz_title">
            
            <img src="{{ asset('assets/student/images/left_triangle.jpg') }}" />


            <h3 class="">School : {{$school_name}}</h3>
          
    </div></div>  
        <div class="col-xs-7"> 
        @if(Session::has('errormessage'))
       <div class="alert alert_profile alert-autocloseable-info">
           {{ Session::get('errormessage') }}
      </div>
      @endif

      @if(Session::has('successmessage'))
      <div class="alert alert_profile_success alert-autocloseable-info">
            {{ Session::get('successmessage') }}
      </div>
      @endif
        </div> 
    </div>
    </div>
  </section>
   <form action="{{ asset('student/update_profile') }}" method="post" enctype = "multipart/form-data" >
    <input type="hidden" name="id" value="{{ Auth::user()->id }}">
    {{ csrf_field() }}
    <section class="user_profile">
    <div class="row">
        <div class="col-xs-12 text-center">
            <div class="user_div">
              @if(Auth::user()->picture!='' )
               @php($img = url('/').Storage::url('app/'.Auth::user()->picture))
              @else
               @php($img = url('/').Storage::url('app/dummy.jpg'))
              @endif
              <img src="{{$img}}" class="previewHolder" /> 
              <div class="upload_btn">
                    <i class="browse_file"><img src="{{ asset('assets/student/images/new_icon/browse.png') }}" /></i>
                    <input type="file" class="input_file" id="profileimg" name="picture"/>
              </div>
              <h2>{{ Auth::user()->name }}</h2> 
              <h3>{{ Auth::user()->contact }}</h3>
            </div>
        </div>
        </div>
        
    </section>
 
    <section class="">
        
        <div class="profile_form">
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" placeholder="Name"  value="{{ Auth::user()->name }}" required name="name" id="name" />
            </div>
             <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" placeholder="Email" value="{{ Auth::user()->email }}"  name="email" id="email" />
            </div>
             <div class="form-group">
            <label>Contact Code</label>
            <select name="country_code"   class="form-control" required id="country_code">
                        @php($countries = array('+60'=>'Malaysia (+60)','+91'=>'India (+91)','+44'=>'United Kingdom (+44)','+65'=>'Singapore (+65)','+1'=>'United State (+1)'))
                        @foreach($countries as $key => $value)
                          <option value="{{$key}}" @if($key== Auth::user()->country_code) {{ 'selected' }}@endif>{{$value}}</option>
                          @endforeach
            </select>
            </div>
             <div class="form-group">
            <label>Contact</label>
            <input type="number" value="{{ Auth::user()->contact }}" name="contact"   class="form-control" required placeholder="Contact" id="contact" />
            </div>
            <div class="text-center">
            <button type="submit" value="Update" id="ValidateButn" class="button_1">Update</button>
                <button type="button" class="button_2" id="ChangePasswordBtn" style="width: 157px;">Change Password</button>
            </div>
        </div>
       
  </section>
   </form>
         
</div>  



        
@endsection



@section('page_js')
<script>
  function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.previewHolder').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}



$('#profileimg').bind('change', function() {
         
         if(this.files[0].size/1024/1024>10 ){
            swal('','This file size is: ' + this.files[0].size/1024/1024 + 'MB. Image size should not exceed 10MB ','warning'); 
         }else{
            readURL(this) ;
         }
            
});

 $("#ValidateButn").click(function(){
      
      if($("#name").val()=='' || $("#country_code").val()=='' || $("#contact").val()==''){
        swal("Error","Name,Country Code and Contact must be filled","error");
        return false;
      }
    })


  $('.alert-autocloseable-info').delay(10000).fadeOut( "slow", function() {
        $('#alert-autocloseable-info').prop("disabled", false);
      });
      

 $("#ChangePasswordBtn").on('click',function(){
     $("#ChangePasswordModal").modal('show');
 })      
  </script>
@endsection
