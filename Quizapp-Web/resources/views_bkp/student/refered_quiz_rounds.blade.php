@extends('layouts.student')
@section('page_title')
Quiz-Round
@endsection
@section('page_css')
<link href="{{ asset('assets/student/css/enjoyhint.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('content')


<div class="s_main"> 
<section class="quiz_div">  
    <div class="row">
        <div class="col-xs-12"><div class="quiz_title">
            <img src="{{ asset('assets/student/images/left_triangle.jpg') }}" />
            <h3 class="">{{$quiz_name->title}} -  Rounds</h3></div></div>
    </div>
</section>
 
   
<section class="tune_talk">
   @foreach($refered_rounds as $rounds)
    <div class="row">
        <div class="col-xs-12">
            <div class="question_div before100">
                <div class="question_block"> 
                    <h3>{{$rounds->roundname}}</h3>
                     @inject('publish_questions','App\PublishQuestions')
               @php($count_qustions = $publish_questions->where(['quiz_round_id'=>$rounds->quizroundid,'published'=>1])->count())
                    <h4 class="tune_title">Total Questions <span>{{$count_qustions}}</span></h4>
                </div>
                <ul class="question_start clearfix">
                    <li><button type="button" class="button_1" onclick="AttendReferQuiz({{$rounds->id}})" >START QUIZ</button></li>
                </ul> 
            </div> 
        </div>
    </div>
    @endforeach
</section>
     
   
</div>




@endsection



@section('page_js')
<script>

 


function AttendReferQuiz(id){
    var id = id;
    if(id==''){
          swal("Error","Some error occured","error");
      }else{
        console.log(id);
          $.ajax({
            type: 'POST',
            url: APP_URL+'/student/refered_quiz_attended',
            data: {
              id: id
            },
             headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            success: function(data) {
              var res = $.parseJSON(data);
              if(res.status == 'error'){
                swal('Error',res.message,'error');
              }else{
                window.location.href = APP_URL + "/student/refer-quiz/"+res.parameter_enc;
              } 
            },
            error: function(data) {
              swal('Error',data,'error');
            }
          });
      }
  }
</script>
@endsection
