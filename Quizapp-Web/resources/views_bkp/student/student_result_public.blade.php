<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="og:image" content="https://scotch.io/wp-content/themes/scotch/img/scotch-home.jpg">
<meta property="og:site_name" content="Scotch">
<title>Quiz Result</title>
<link href="{{ asset('assets/student/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/student/css/style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/student/css/responsive.css') }}" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="{{ asset('assets/logo/favicon.ico') }}" />
<script src="{{ asset('assets/student/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/student/js/bootstrap.min.js') }}"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
<script>
   var APP_URL = {!! json_encode(url('/')) !!} 
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<title>Quiz</title>
</head>

<body><div class="s_main"> 
<section class="quiz_div">  
    <div class="row">
        <div class="col-xs-12">
            <div class="quiz_title">
            
            <img src="{{ asset('assets/student/images/left_triangle.jpg') }}" />

            <div class="tune_talk_history">
            <h4 class=""><span>Quiz Result {{$result->roundname}} | <span>{{ ucfirst($school->name) }}</h4> 

                </div>
            </div>
            <div class="tankTune">
               
                    <p>To reload your Tune Talk Sim Card, Click Here : </p>
                 <a href="http://www.tunetalk.com/" target="_blank"><img src="{{ asset('assets/student/images/icons/new_logo.png') }}"></a>
                </div>
        </div> 
        
    </div>
  </section>
 
   
     <section class="tune_talk_history_block">
    <div class="row">
        <div class="col-xs-12">
             <div class="question_div thankDiv">
                <div class="question_block text-right"> 
                    <h5>Answered Correctly</h5>
                        <h3>{{$result->score}}/{{$result->total_question}}</h3>
                  <div class="symobol"><i class="fa fa-check"></i></div>  
                </div>       
            </div> 
             <div class="question_div thankDiv">
              <div class="question_block text-right"> 
                  <h5>Score</h5>
                     @php($precentage_resul = ($result->score/$result->total_question)*100)
                      <h3>{{number_format((float)$precentage_resul, 2, '.', '')}}%</h3>
                  <div class="symobol"><i class="fa fa-thumbs-up"></i></div>  
              </div>     
            </div> 
             <div class="question_div thankDiv">
                <div class="question_block text-right"> 
                    <h5>Time Spent</h5>
                        <h3>{{$result->quiz_duration}}</h3>
                  <div class="symobol"><i class="clock_icon"><img src="{{asset('assets/student/images/new_icon/clock.png')}}"></i></div>  
            </div>                
                          
            </div> 
            </div>
             
         
        </div> 
         <div class="row pdt30">
        <div class="col-xs-12 text-center">
            <button class="button_1" id="GoHome">Home</button>
             
             </div>
         </div>
    </section>
        
     
</div>

<script>
    $("#GoHome").on('click',function(){
        window.location.href= APP_URL;
    })
</script>
</body>
</html>
