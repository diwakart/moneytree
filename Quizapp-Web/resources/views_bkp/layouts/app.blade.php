<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />
        <title>
            @yield('page_title')
        </title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="Latest updates and statistic charts">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

        <script src="{{ asset('assets/login/js/app.js') }}" defer></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
       <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <script>
              var APP_URL = {!! json_encode(url('/')) !!} 
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!--end::Web font -->
        <!--begin::Base Styles -->
        <link href="{{ asset('assets/inner/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors -->
        <link href="{{ asset('assets/inner/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/inner/demo/demo5/base/style1.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
       <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <link rel="shortcut icon" href="{{asset('assets/logo/favicon.ico')}}" />
         @yield('page_css')
    </head>
    <!-- end::Head -->
    <!-- end::Body -->
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <!-- begin:: Page -->

<div class="reg" style="display:flex; height:100%;">
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--singin" id="m_login">
                <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
                    <div class="m-stack m-stack--hor m-stack--desktop">
                        <div class="m-stack__item m-stack__item--fluid">
                            <div class="m-login__wrapper login-sec login_brought">
                                <p>Brought to you by :  <img src="{{asset('assets/student/images/icons/new_logo.png')}}"> </p> 
                                @yield('content')
                                <h5 class="powered_by">Powered by :  <img src="{{asset('assets/student/images/icons/mt_logo.png')}}"> </h5> 


                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1  m-login__content" >
                    
                    <div class="m-grid__item m-grid__item--middle text-center">
                        <div class="m-login__logo">
                                    <a href="{{ url('/') }}">
                                        <img src="{{asset('assets/student/images/large_logo.png')}}">
                                    </a>
                                </div>
                        <h3 class="m-login__welcome">
                             @yield('content_heading')
                           
                        </h3>
                    </div>
                </div>
            </div>
        </div>

</div>
        <!-- end:: Page -->
        <script src="{{ asset('assets/inner/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/inner/demo/demo5/base/scripts.bundle.js') }}" type="text/javascript"></script>
        <!--end::Base Scripts -->   
        <!--begin::Page Vendors -->
       <!--   <script src="{{ asset('assets/inner/select/select2.js') }}" type="text/javascript"></script> -->
        <script src="{{ asset('assets/inner/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
        <!--end::Page Vendors -->  
        <!--begin::Page Snippets -->
        <script src="{{ asset('assets/inner/app/js/dashboard.js') }}" type="text/javascript"></script>
        
      
        <!--begin::Base Scripts -->
        @yield('page_script')
       
        <!--end::Page Snippets -->
    </body>
    <!-- end::Body -->
</html>
