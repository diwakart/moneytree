@php 
    $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri_segments = explode('/', $uri_path);
    $href = '';
    if($uri_segments[2]=='student-rank')
    {
        $href = 'https://quizme.com.my/blog';
    }
@endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" xmlns:svg="http://www.w3.org/2000/svg">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@yield('page_title')</title>

<link href="{{ asset('assets/student/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="{{ asset('assets/logo/favicon.ico') }}" />
<link href="{{ asset('assets/student/css/style.css') }}" rel="stylesheet" type="text/css"> 
   
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    
<link href="{{ asset('assets/student/css/responsive.css') }}" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  @yield('page_css')
<script src="{{ asset('assets/student/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/student/js/bootstrap.min.js') }}"></script>
<script>
   var APP_URL = {!! json_encode(url('/')) !!} 
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>
    <script>
   $(document).ready(function() {
 if(typeof $(".faq_slider")!='undefined'){
  $(".faq_slider").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  });
 }
 
});
    </script>
    <style>
        @media (max-width: 767px){
 .faq_slider img{
            width: 100% !important;
    padding: 5px;
        }
        }
        .faq_slider{
            
        }
         .faq_slider .heading{
                 color: #ea493f;
    margin: 0;
    font-weight: 600;
    padding-bottom: 8px;
    font-size: 15px;
            
        }
         .faq_slider img{
            width: 50%;
             margin: auto;
             display: block;
    padding: 5px;
        }
         .faq_slider .description{
                margin-top: 5px;
    margin-bottom: 0;
    font-size: 14px;
        }
        .faq_slider .owl-prev, .owl-next {
    display: inline-block;
    background: #ea483e;
    margin: 2px;
    padding: 2px 5px;
    text-transform: capitalize;
    font-size: 14px;
    color: #fff;
    border-radius: 2px;
}
        .faq_slider .owl-buttons {
    text-align: center;
    margin-top: 10px;
}
    </style>
</head>

<body>
<section class="main-head clearfix">
  <div class="container-fluid">
    <div class="row ds-flex">
      <div class="col-md-2 col-sm-4 col-xs-5 logo">
        <div><a href="{{ route('home')}}"><img src="{{ asset('assets/logo/logo.png') }}" class="img-responsive"></a></div>
      </div>
      <div class="col-sm-10 col-xs-7 top-right main-head-right">
        <div class="row">
          <div class="col-sm-6 hidden-xs">
            <div class="welcome">
              <h2>Welcome</h2>
              <span>|</span>
              <h3>{{ ucfirst(Auth::user()->name) }}</h3>
            </div>
          </div> 
          <div class="text-right main-head-profile">
            @if(Auth::user()->picture!='' )
             @php($img = url('/').Storage::url('app/'.Auth::user()->picture))
            @else
             @php($img = url('/').Storage::url('app/dummy.jpg'))
            @endif
            <ul>
             
              <li class="dropdown profile"><a href="javascript:;" data-toggle="dropdown" role="button"> <span>{{ Auth::user()->name }}</span> <img src="{{$img}}"> <span class="caret"></span> </a>
                <ul class="dropdown-menu header_dropdown" >
                  <li class="active"><a href="{{ route('home')}}"><i class="icon1"></i><span>Dashboard</span></a></li>
                  <li><a href="{{ route('quizes')}}"><i class="icon2"></i><span>Quiz</span></a></li>
                    <li><a href="{{ route('student-rank')}}"><i class="icon3"></i><span>Rank</span></a></li>
                  <li><a href="{{ route('student_profile')}}"><i class="icon4"></i><span>Profile</span></a></li>
                    
                  <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="icon5"></i><span>Sign out</span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                  </li>
                </ul>
              </li>
              <li style="display: none;"><a href="#" ><span><i class="fa fa-sign-in"></i></span></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

 @yield('content')
    
     <section class="my_footer">
              <p class="faq"><a href="javascript:(0);" data-target="#FAQModal" data-toggle="modal">FAQ</a> | For help, contact us by clicking : @if($href!='')<a href="https://quizme.com.my/blog">here</a>@else<a href="https://quizme.com.my/#contact">here</a>@endif</p>
            <ul class="list-inline">
                <li>Brought to you by : </li>
                <li><img src="{{ asset('assets/student/images/icons/new_logo.png') }}" width="50" /></li>
                <li><img src="{{ asset('assets/student/images/icons/mt_logo.png') }}"width="100"  /></li>
            </ul> 
        </section>
         
    
 @yield('page_js')
 
 
 
<!-- FAQ Modal Start-->
<div id="FAQModal" class="modal fade" role="dialog">
   <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-center">FAQ&nbsp;<span style="font-size: 11px;">(Slide for more instructions)</span></h4>
                    </div>
                  <div class="modal-body">                  
                    <div class="owl-carousel owl-theme faq_slider">
                        <div class="item">
                        <h4 class="heading">1. How do you enter your quiz </h4>
                            <img src="{{ asset('assets/student/images/slider/slider1.png') }}" />
                            <p class="description"></p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">1. How do you enter your quiz </h4>
                            <img src="{{ asset('assets/student/images/slider/slider2.png') }}" />
                            <p class="description">Once you have clicked on enter quiz, click on “Start Quiz”</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">3. How do I check what prize to win for each round?</h4>
                            <img src="{{ asset('assets/student/images/slider/slider3.png') }}" />
                            <p class="description">The prize for each Quiz round will be shown below the “Refer a Friend” button for each Quiz Round. There may be a different prize for each quiz round</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">4. How to answer a MCQ question </h4>
                            <img src="{{ asset('assets/student/images/slider/slider4.png') }}" />
                            <p class="description">Click any circle to pick your answer & Click Next</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">5. How to fill in a blank (Enter your answer & click next)</h4>
                            <img src="{{ asset('assets/student/images/slider/slider5.png') }}" />
                            <p class="description">Enter your answer in the field provided and click Next</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">6. How do I know when a round starts? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider6.png') }}" />
                            <p class="description">When a round has started, the quiz will appear and the button to enter will appear</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">7. Will I get a reminder when a round starts? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider_default.png') }}" />
                            <p class="description">Yes, a SMS will be sent to your registered mobile number</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">8. How do I know what’s my score for the round? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider8.png') }}" />
                            <p class="description">After completing your quiz round, your score will be shown</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">9. How do I earn bonus point for each quiz round? (By referring a friend for each quiz round)</h4>
                            <img src="{{ asset('assets/student/images/slider/slider9.png') }}" />
                            <p class="description">You can refer a friend to each quiz round and earn 1 bonus point. That is the maximum you can earn for each round. Before starting your Quiz, click on Refer A Friend. You have to refer your friend BEFORE starting quiz</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading"></h4>
                            <img src="{{ asset('assets/student/images/slider/slider9.1.png') }}" />
                            <p class="description">Enter your friend’s name and phone number. After that, click “Share”. You will be awarded 1 Referral Point.</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">10. Checking your referral point</h4>
                            <img src="{{ asset('assets/student/images/slider/slider10.png') }}" />
                            <p class="description">Your referral point will be shown at your result for the round.</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">11. What is the maximum bonus point for each round? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider_default.png') }}" />
                            <p class="description">Only ONE bonus point from referral for each round</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">12. What is time elapse? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider12.png') }}" />
                            <p class="description">This is the time you took to complete the quiz round</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">13. Why is the time elapse important? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider_default.png') }}" />
                            <p class="description">If your score is tied with another player, the user with shorter time elapse wins</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">14. How do I know if I have won for the round? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider14.png') }}" />
                            <p class="description">You have to appear as one of the TOP 10 players for the Quiz Round.</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">15. How do I check the ranking board? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider15.png') }}" />
                            <p class="description">Once the result is out, click on the “View Details” button for the specific round.</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">16. When will the ranking board be released </h4>
                            <img src="{{ asset('assets/student/images/slider/slider16.png') }}" />
                            <p class="description">The date for the result will be shown before you start the quiz. </p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">17. Will I get an SMS when the results are announced? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider_default.png') }}" />
                            <p class="description">Yes, a SMS will be sent to your registered mobile number</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">18. How do I get chosen for the finale? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider18.png') }}" />
                            <p class="description">First, click on Quiz Ranking button to check the Ranking for the entire quiz</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading"></h4>
                            <img src="{{ asset('assets/student/images/slider/slider18.1.png') }}" />
                            <p class="description">You have to appear among the TOP 10 in the Quiz Ranking Board</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">19. How is the overall quiz ranking determined? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider_default.png') }}" />
                            <p class="description">Your score for each quiz round will be added. Your total score will be used to determine your overall quiz ranking.</p>
                        </div>

                        <div class="item">
                        <h4 class="heading">20. What is the finale about?</h4>
                            <img src="{{ asset('assets/student/images/slider/slider_default.png') }}" />
                            <p class="description">The TOP 10 players in the overall quiz ranking will be invited for a special session. These TOP 10 students will be competing against each other in a class to win exclusive prizes!</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">21. How do I check my profile? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider21.png') }}" />
                            <p class="description">Click on the arrow for a dropdown menu. Then click profile</p>
                        </div>

                        <div class="item">
                        <h4 class="heading">22. How do I change items in my profile? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider22.png') }}" />
                            <p class="description">Fill in the blank in your profile and click update</p>
                        </div>
                        
                        <div class="item">
                        <h4 class="heading">23. What if I forget my password? </h4>
                            <img src="{{ asset('assets/student/images/slider/slider23.png') }}" />
                            <p class="description">Click forget password at login page. Fill in your phone number and you will be given a temporary password via your SMS. Remember to change your password after</p>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-default red-btn" data-dismiss="modal">Close</button>
                    </center>
                </div>
            </div>
        </div>   
</div>
    
   <!-- FAQ Modal End-->
   
   
<!-- Modal for Change Password -->
      <div class="modal fade" id="ChangePasswordModal" role="dialog">
        <div class="modal-dialog"> 
          
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">
            <form  action="{{ asset('student/profile/update_password') }}" method="post" >
                            {{ csrf_field() }}
              <div class="row quiz-result-data">
                <div class="clearfix">

                  <div class="form-group clearfix">
                    <label class="col-md-4">Password</label>
                    <div class="col-md-8">
                      <input type="password" name="password_text" id="password_text" class="form-control">
                    </div>
                  </div>
                  
                  <div class="form-group clearfix">
                    <label class="col-md-4">Confirm Password</label>
                    <div class="col-md-8">
                      <input type="password" name="confirm_password_text" id="confirm_password_text" class="form-control">
                    </div>
                  </div>

                
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button  type="submit" class="btn sel-quiz-btn"  ><i class="fa fa-key"></i>&nbsp; Change Password</button>
            </div>
            </form>
          </div>
        </div>
      </div>
       
    
    
</body>
</html>
