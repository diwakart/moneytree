<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{Auth::user()->name}}</title>

<link href="{{ asset('assets/student/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/student/css/style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/student/css/responsive.css') }}" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
<link rel="shortcut icon" href="{{asset('assets/logo/favicon.ico')}}" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  @yield('page_css')
<script src="{{ asset('assets/student/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/student/js/bootstrap.min.js') }}"></script>
<script>
   var APP_URL = {!! json_encode(url('/')) !!} 
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
</head>

<body>
<section class="main-head clearfix">
  <div class="container-fluid">
    <div class="row ds-flex">
      <div class="col-sm-2 col-xs-5 logo">
        <div><a href="javascript:;"><img src="{{ asset('assets/logo/favicon.ico') }}" class="img-responsive"></a></div>
      </div>
      <div class="col-sm-10 col-xs-7 top-right main-head-right">
        <div class="row">
          <div class="col-sm-6 hidden-xs">
            <div class="welcome">
              <h2>Welcome</h2>
              <span>|</span>
              <h3>{{ ucfirst(Auth::user()->name) }}</h3>
            </div>
          </div>
          <div class="col-sm-6 text-right main-head-profile">
            @if(Auth::user()->picture!='' )
             @php($img = url('/').Storage::url('app/'.Auth::user()->picture))
            @else
             @php($img = url('/').Storage::url('app/dummy.jpg'))
            @endif
            <ul>
             <li class="dropdown profile"><a href="javascript:;" data-toggle="dropdown" role="button"> <span>{{ Auth::user()->name }}</span> <img src="{{$img}}"> <span class="caret"></span> </a>
                <ul class="dropdown-menu header_dropdown" >
                  <li class="active"><a href="{{ route('home')}}"><i class="icon1"></i><span>Dashboard</span></a></li>
                  <li><a href="{{ route('quizes')}}"><i class="icon2"></i><span>Quiz</span></a></li>
                    <li><a href="{{ route('student-rank')}}"><i class="icon3"></i><span>Rank</span></a></li>
                  <li><a href="{{ route('student_profile')}}"><i class="icon4"></i><span>Profile</span></a></li>
                    
                  <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="icon5"></i><span>Sign out</span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                  </li>
              </li>
              <li><a href="#"><span><i class="fa fa-sign-in"></i></span></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

 @yield('content')

 @yield('page_js')

<section class="footer clearfix">
  <p>Privacy Policy  Terms & Conditions |  © WebQuiz 2018</p>
</section>
</body>
</html>
