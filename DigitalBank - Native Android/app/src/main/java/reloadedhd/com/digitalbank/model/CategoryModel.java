package reloadedhd.com.digitalbank.model;

public class CategoryModel {
    private String id;
    private String categoryName;
    private String category_boosted;

    public String getBoosted_value() {
        return boosted_value;
    }

    public void setBoosted_value(String boosted_value) {
        this.boosted_value = boosted_value;
    }

    private String boosted_value;
    private boolean selected = false;

    public String getCategory_boosted() {
        return category_boosted;
    }

    public void setCategory_boosted(String category_boosted) {
        this.category_boosted = category_boosted;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
