package reloadedhd.com.digitalbank.customAlert;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import reloadedhd.com.digitalbank.R;

public class CustomAlert {

    Animation animation1, animation2;

    public void showAlertWithAnimation(Context context, String title, String msg) {
        ImageView alertCancelButton;
        TextView alertTitle, alertMessage;
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.alert_anim);
        alertTitle = dialog.findViewById(R.id.alertTitle);
        alertMessage = dialog.findViewById(R.id.alertMessage);
        alertCancelButton = dialog.findViewById(R.id.alertCancelButton);
        alertTitle.setText(title);
        alertMessage.setText(msg);
        alertTitle.setVisibility(View.GONE);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.gravity = Gravity.CENTER_VERTICAL;

        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

      /*animation1 = new AlphaAnimation(0.0f, 1.0f);
        animation1.setDuration(500);
        animation1.setStartOffset(500);

        animation2 = new AlphaAnimation(1.0f, 0.0f);
        animation2.setDuration(500);
        animation2.setStartOffset(500);*/

     /*alertImage.startAnimation(animation1);
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                alertImage.startAnimation(animation2);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
*/
     /*   animation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                alertImage.startAnimation(animation1);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });*/

        alertCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*   animation1.cancel();
                animation2.cancel();*/
                dialog.cancel();

            }

        });

        dialog.show();

    }

    public void showAlert(Context context, int val, String msg) {

        TextView alertMessage;
        ImageView alertImage, cancelImage;
        //val ==1 for success  0 for error
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.alert_msg);
        alertMessage = dialog.findViewById(R.id.alertMessage);
        alertImage = dialog.findViewById(R.id.alertImage);
        cancelImage = dialog.findViewById(R.id.cancelImage);
        alertMessage.setText(msg);
        dialog.setCancelable(false);

        if (val == 0) {
            alertImage.setImageDrawable(context.getResources().getDrawable(R.drawable.failure));
        } else if (val == 1) {
            alertImage.setImageDrawable(context.getResources().getDrawable(R.drawable.success));
        }
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.gravity = Gravity.CENTER_VERTICAL;
        //todo for remove the animation
        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        cancelImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();

            }

        });

        dialog.show();

    }

    public void showEarnBadges(Context context, int val, String msg) {

        TextView alertMessage;
        ImageView alertImage, cancelImage;

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.alert_msg);
        alertMessage = dialog.findViewById(R.id.alertMessage);
        alertImage = dialog.findViewById(R.id.alertImage);
        cancelImage = dialog.findViewById(R.id.cancelImage);
        alertMessage.setText(msg);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.gravity = Gravity.CENTER_VERTICAL;
        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        cancelImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();

            }

        });

        dialog.show();

    }

}
