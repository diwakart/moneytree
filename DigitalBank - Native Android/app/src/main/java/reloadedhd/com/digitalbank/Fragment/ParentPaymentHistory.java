package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.ParentHistoryAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.PaymentHistoryModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import reloadedhd.com.digitalbank.WebService.Service;

public class ParentPaymentHistory extends Fragment implements View.OnClickListener {
    private TextView parentPaymentNodata, toolbarTitle;
    private RelativeLayout toolbarRelativeBack;
    private RecyclerView parentPaymentList;
    private LinearLayoutManager manager;
    private ArrayList<PaymentHistoryModel> arrayList;
    private ParentHistoryAdapter adapter;
    private ShimmerFrameLayout parentHistoryShimmer;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.parent_payment_history, null);
        initView(v);
        return v;
    }

    private void initView(View v) {

        parentPaymentNodata = v.findViewById(R.id.parentPaymentNodata);
        parentPaymentList = v.findViewById(R.id.parentPaymentList);

        toolbarRelativeBack = v.findViewById(R.id.toolbarRelativeBack);
        toolbarTitle = v.findViewById(R.id.toolbarTitle);

        parentHistoryShimmer=v.findViewById(R.id.parentHistoryShimmer);

        toolbarTitle.setText("Payment History");

        manager = new LinearLayoutManager(getActivity());
        parentPaymentList.setLayoutManager(manager);
        arrayList = new ArrayList<>();
        arrayList.clear();
        adapter = new ParentHistoryAdapter(getActivity(), arrayList);
        parentPaymentList.setAdapter(adapter);

        getPaymentHistory();

        toolbarRelativeBack.setOnClickListener(this);

    }

    private void getPaymentHistory() {
        parentHistoryShimmer.setVisibility(View.VISIBLE);
        parentHistoryShimmer.startShimmer();
      //  final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getParentPaymentHistory(Global.acceptHeader, Global.token, "v1/user/passbook.json");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                parentHistoryShimmer.stopShimmer();
                parentHistoryShimmer.setVisibility(View.GONE);

                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                String res = response.body().toString();
                Log.e("resssb", res);
                try {

                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.optString("status").equalsIgnoreCase("success")) {

                        JSONArray array = jsonObject.optJSONArray("data");

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);
                            PaymentHistoryModel model = new PaymentHistoryModel();
                            model.setTransaction_points(object.optString("transaction_points"));
                            model.setRemarks(object.optString("remarks"));
                            model.setTO(object.optString("TO"));
                            model.setFROM(object.optString("FROM"));
                            model.setId(object.optString("id"));
                            model.setTransaction_time(object.optString("transaction_time"));
                            arrayList.add(model);

                        }

                        if (!arrayList.isEmpty()) {

                            adapter.notifyDataSetChanged();
                            parentPaymentList.setVisibility(View.VISIBLE);
                            parentPaymentNodata.setVisibility(View.GONE);

                        } else {

                            parentPaymentNodata.setVisibility(View.VISIBLE);
                            parentPaymentList.setVisibility(View.GONE);

                        }

                    } else {

                        new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.toolbarRelativeBack:

                getActivity().onBackPressed();

                break;

        }

    }

}
