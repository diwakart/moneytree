package reloadedhd.com.digitalbank.Fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import reloadedhd.com.digitalbank.Adapter.PagerAdapter;
import reloadedhd.com.digitalbank.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class History extends Fragment implements View.OnClickListener {

    private RelativeLayout toolbarRelativeBack;
    private TextView toolbarTitle;
    private ViewPager historyPager;
    private TabLayout historyTabLayout;

    public History() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        initView(view);
        return view;

    }

    private void initView(View view) {

        toolbarRelativeBack = view.findViewById(R.id.toolbarRelativeBack);
        toolbarTitle = view.findViewById(R.id.toolbarTitle);
        historyPager = view.findViewById(R.id.historyPager);
        historyTabLayout = view.findViewById(R.id.historyTabLayout);
        toolbarTitle.setText("History");

        historyPager.setAdapter(new PagerAdapter(getChildFragmentManager()));

        historyTabLayout.setupWithViewPager(historyPager);

        toolbarRelativeBack.setOnClickListener(this);

        historyTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {

                    case 0:
                        new ChildEarnedHistory();

                    case 1:
                        new ChildSpentHistory();

                    case 2:
                        new AuctionHistory();

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.toolbarRelativeBack:

                getActivity().onBackPressed();

                break;

        }

    }

}
