package reloadedhd.com.digitalbank.WebService;

import android.app.Activity;
import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import reloadedhd.com.digitalbank.Fragment.ChangePassword;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 5/28/2018.
 */

public class ChangePassword_Api {
    Context context;
    String confirmPassword;
    ProgressDialog progressDialog;

    public void changePassword(FragmentActivity activity, String confirmPassword) {
        this.context = activity;
        this.confirmPassword = confirmPassword;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.changePassword(Global.acceptHeader, Global.token,confirmPassword,confirmPassword);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();
                String msgIsSuccess = response.isSuccessful()+"";
                if (msgIsSuccess.equals("true"))
                {

                    try {

                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    String status = jsonObject.get("status").toString();
                    if (status.equalsIgnoreCase("success"))
                    {
                        String message = jsonObject.get("message").toString();
                        Toast.makeText(context,  message+"", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        String message = jsonObject.get("message").toString();
                        Toast.makeText(context,  message+"", Toast.LENGTH_SHORT).show();
                    }


                    } catch (JSONException e) {
                    e.printStackTrace();
                }
                }
                else
                {
                    Toast.makeText(context,  response.message()+"", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context,  "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
