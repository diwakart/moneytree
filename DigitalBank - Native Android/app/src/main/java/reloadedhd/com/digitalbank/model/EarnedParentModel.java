package reloadedhd.com.digitalbank.model;

public class EarnedParentModel {

    private String category_name;
    private String question;
    private String earn_points;

    public String getEarn_point_time() {
        return earn_point_time;
    }

    public void setEarn_point_time(String earn_point_time) {
        this.earn_point_time = earn_point_time;
    }

    private String earn_point_time;

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getEarn_points() {
        return earn_points;
    }

    public void setEarn_points(String earn_points) {
        this.earn_points = earn_points;
    }

}
