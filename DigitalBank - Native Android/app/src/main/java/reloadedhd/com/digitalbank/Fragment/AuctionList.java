package reloadedhd.com.digitalbank.Fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.AuctionAlertAdapter;
import reloadedhd.com.digitalbank.Adapter.AuctionListAdapter;
import reloadedhd.com.digitalbank.Adapter.ChildProductListAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.AuctionAlertModel;
import reloadedhd.com.digitalbank.model.AuctionListModel;
import reloadedhd.com.digitalbank.model.ChildProductModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import reloadedhd.com.digitalbank.transform.CircleTransform;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuctionList extends Fragment implements View.OnClickListener {
    private RecyclerView auctionRecyclerView;
    private LinearLayoutManager manager;
    private AuctionListAdapter adapter;
    private ArrayList<AuctionListModel> arrayList;
    private TextView noAuction;
    //todo for toolbar
    private RelativeLayout toolbarRelativeBack;
    private TextView toolbarTitle;
    private ShimmerFrameLayout shimmerAuctionList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.auction_list, container, false);
        arrayList = new ArrayList<>();

        initView(v);

        return v;
    }

    private void initView(View v) {
        arrayList.clear();
        toolbarRelativeBack = v.findViewById(R.id.toolbarRelativeBack);
        toolbarTitle = v.findViewById(R.id.toolbarTitle);
        noAuction = v.findViewById(R.id.noAuction);
        auctionRecyclerView = v.findViewById(R.id.auctionRecyclerView);
        shimmerAuctionList=v.findViewById(R.id.shimmerAuctionList);
        manager = new LinearLayoutManager(getActivity());
        auctionRecyclerView.setLayoutManager(manager);
        adapter = new AuctionListAdapter(getActivity(), arrayList);
        auctionRecyclerView.setAdapter(adapter);

        toolbarTitle.setText("Auction List");
        getActionList();

        adapter.onAuctionListClick(new AuctionListAdapter.ListClick() {

            @Override
            public void onParticipateClick(int pos) {

                participateInAuction(arrayList.get(pos).getAuction_id());

            }

            @Override
            public void onGoCLick(int pos) {

                Auction auction = new Auction();

                Bundle bundle = new Bundle();
                bundle.putString("val", "2");
                bundle.putSerializable("list", arrayList.get(pos));
                auction.setArguments(bundle);

                Global.changeFragment(getActivity(), auction);

            }

        });

        toolbarRelativeBack.setOnClickListener(this);

    }

    private void participateInAuction(String id) {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        //will modify according to the requirement
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/auction/" + id + "/participate.json");

        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                pd.cancel();
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                String str = response.body().toString();

                try {


                    JSONObject jsonObject = new JSONObject(str);
                    Log.e("auctionlist", jsonObject.toString());

                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                        if (jsonObject.optJSONArray("data").length() == 1) {

                            Auction auction = new Auction();

                            Bundle bundle = new Bundle();
                            bundle.putString("val", "1");
                            bundle.putString("list", jsonObject.optJSONArray("data").optJSONObject(0).toString());

                            auction.setArguments(bundle);

                            Global.changeFragment(getActivity(), auction);

                        } else {

                            ArrayList<AuctionAlertModel> models = new ArrayList<>();
                            JSONArray array = jsonObject.optJSONArray("data");

                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object = array.optJSONObject(i);
                                AuctionAlertModel model = new AuctionAlertModel();
                                model.setId(object.optString("id"));
                                model.setAuctionId(object.optString("auction_id"));
                                model.setProductId(object.optString("product_id"));
                                model.setProductImage(object.optString("product_image"));
                                model.setProductName(object.optString("product_name"));
                                models.add(model);

                            }

                            if (!models.isEmpty()) {

                                setPriority(models);

                            }

                        }

                    }
                    else {

                        if (jsonObject.has("data") && jsonObject.optJSONArray("data").length() > 0) {

                            showCategoryProduct(jsonObject.optJSONArray("data").optJSONObject(0).optString("auction_category_id"));

                        } else {

                            new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                        }

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0,"Failure");

            }

        });

    }

    private void showCategoryProduct(String id) {

        Log.e("catid", id);
        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Loading wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/category/" + id + "/products.json");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {
                //pd.cancel();
                String res = response.body().toString();
                Log.e("catprdct", res);

                try {

                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.optString("status").equalsIgnoreCase("success")) {

                        JSONArray array = jsonObject.optJSONArray("data");
                        ArrayList<ChildProductModel> wishList = new ArrayList<>();

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);
                            ChildProductModel model = new ChildProductModel();
                            model.setCategory_id(object.optString("category_id"));
                            model.setCategory_name(object.optString("category_name"));
                            model.setId(object.optString("id"));
                            model.setProduct_id(object.optString("id"));
                            model.setProduct_name(object.optString("product_name"));
                            model.setProduct_image(object.optString("product_image"));
                            model.setProduct_price(object.optString("base_price"));
                            wishList.add(model);

                        }

                        //if(!wishList.isEmpty()){
                        addProductWishlist(wishList);
                        //}

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //  pd.cancel();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();

            }

        });

    }

    private void getActionList() {
        shimmerAuctionList.startShimmer();
        shimmerAuctionList.setVisibility(View.VISIBLE);
      //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        //will modify according to the requirement
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/auctions.json");

        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                shimmerAuctionList.stopShimmer();
                shimmerAuctionList.setVisibility(View.GONE);
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                String str = response.body().toString();
               // pd.cancel();

                try {

                    JSONObject jsonObject = new JSONObject(str);
                    Log.e("auctionlist", jsonObject.toString());

                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                        JSONArray array = jsonObject.optJSONArray("data");

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);
                            AuctionListModel model = new AuctionListModel();

                            model.setAuction_id(object.optString("auction_id"));
                            model.setAuction_price(object.optString("auction_price"));
                            model.setCategory_name(object.optString("category_name"));
                            model.setCurrent_auction_value(object.optString("current_auction_value"));
                            model.setEnd_time(object.optString("end_time"));
                            model.setStart_time(object.optString("start_time"));
                            model.setSingle_bid_value(object.optString("single_bid_value"));

                            model.setBid_hash(object.optString("bid_hash"));
                            model.setUser_participated(object.optString("user_participated"));
                            model.setProduct_name(object.optString("product_name"));
                            model.setProduct_image(object.optString("product_image"));
                            model.setIncremented_bid_time(object.optString("incremented_bid_time"));
                            model.setLast_bidder(object.optString("last_bidder"));
                            arrayList.add(model);

                        }

                        if (!arrayList.isEmpty()) {

                            noAuction.setVisibility(View.GONE);
                            auctionRecyclerView.setVisibility(View.VISIBLE);
                            adapter.notifyDataSetChanged();

                        } else {

                            auctionRecyclerView.setVisibility(View.GONE);
                            noAuction.setVisibility(View.VISIBLE);

                        }

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

              //pd.cancel();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();

            }

        });

    }

    private void setPriority(final ArrayList<AuctionAlertModel> modelArrayList) {

        RecyclerView auctionProductAlertRecycler;
        Button alertAuctionSubmit;
        ImageView setPriorityBack;
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.set_priority);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.gravity = Gravity.CENTER_VERTICAL;
        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        alertAuctionSubmit = dialog.findViewById(R.id.alertAuctionSubmit);
        auctionProductAlertRecycler = dialog.findViewById(R.id.auctionProductAlertRecycler);
        setPriorityBack = dialog.findViewById(R.id.setPriorityBack);
        auctionProductAlertRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        final AuctionAlertAdapter alertAdapter = new AuctionAlertAdapter(getActivity(), modelArrayList);

        auctionProductAlertRecycler.setAdapter(alertAdapter);
        setPriorityBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        alertAdapter.setOnPriorityListener(new AuctionAlertAdapter.AuctionAlertClick() {
            @Override
            public void setPriority(int pos) {

                for (int i = 0; i < modelArrayList.size(); i++) {

                    if (i == pos) {

                        if (modelArrayList.get(i).isSelected()) {
                            modelArrayList.get(i).setSelected(false);
                        } else
                            modelArrayList.get(i).setSelected(true);

                    } else {

                        modelArrayList.get(i).setSelected(false);

                    }

                }

                alertAdapter.notifyDataSetChanged();

            }

        });

        dialog.show();

        alertAuctionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String productIdd = null, auctionIdd = null;
                int oldval = -1;

                for (int i = 0; i < modelArrayList.size(); i++) {

                    if (modelArrayList.get(i).isSelected()) {

                        productIdd = modelArrayList.get(i).getProductId();
                        auctionIdd = modelArrayList.get(i).getAuctionId();
                        oldval = i;

                    }

                }

                if (oldval == -1) {

                    new CustomAlert().showAlert(getActivity(), 0, "Please select the priority for the product");
                    return;

                }

                HashMap<String, RequestBody> body = new HashMap<>();
                body.put("product_id", RequestBody.create(MediaType.parse("multipart/form-data"), productIdd));
                body.put("auction_id", RequestBody.create(MediaType.parse("multipart/form-data"), auctionIdd));
                setPriorityServer(body);

                dialog.cancel();

            }

        });

    }

    private void setPriorityServer(HashMap<String, RequestBody> body) {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        //Call<JsonObject> call = service.setPriorityResponse(Global.acceptHeader, Global.token, prdctId, actnId);
        Call<JsonObject> call = service.getPostResponse(Global.acceptHeader, Global.token, "v1/product/set_priority.json", body);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                pd.cancel();
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {
                String res = response.body().toString();
                Log.e("resssss", res + "//");


                try {

                    JSONObject jsonObject = new JSONObject(res);
                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                        new CustomAlert().showAlert(getActivity(), 1, jsonObject.optString("message"));
                    }


                }
                catch (JSONException e) {

                    e.printStackTrace();

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.cancel();
                new CustomAlert().showAlert(getActivity(), 1, "Failure");

            }

        });

    }

    private void addProductWishlist(final ArrayList<ChildProductModel> wishList) {

        TextView auctionAlertTitle, categoryAlertNodata;
        RecyclerView auctionProductAlertRecycler;
        Button alertAuctionSubmit;
        ImageView setPriorityBack;
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.set_priority);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.gravity = Gravity.CENTER_VERTICAL;
        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        auctionAlertTitle = dialog.findViewById(R.id.auctionAlertTitle);
        categoryAlertNodata = dialog.findViewById(R.id.categoryAlertNodata);
        auctionProductAlertRecycler = dialog.findViewById(R.id.auctionProductAlertRecycler);
        alertAuctionSubmit = dialog.findViewById(R.id.alertAuctionSubmit);
        setPriorityBack = dialog.findViewById(R.id.setPriorityBack);
        alertAuctionSubmit.setVisibility(View.GONE);
        auctionAlertTitle.setText("Add Product To Wishlist");
        auctionProductAlertRecycler.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        final ChildProductListAdapter adapter = new ChildProductListAdapter(getActivity(), wishList);
        auctionProductAlertRecycler.setAdapter(adapter);
        dialog.show();

        if (wishList.isEmpty()) {

            categoryAlertNodata.setVisibility(View.VISIBLE);
            auctionProductAlertRecycler.setVisibility(View.GONE);

        } else {

            categoryAlertNodata.setVisibility(View.GONE);
            auctionProductAlertRecycler.setVisibility(View.VISIBLE);

        }

        setPriorityBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        adapter.onAddItemCLick(new ChildProductListAdapter.ChildAdapterClick() {
            @Override
            public void onAddItem(final int pos) {

                TextView alertMessage, alertOk, alertCancel;
                ImageView customAlertImage;
                final Dialog alert = new Dialog(getActivity());
                View v = LayoutInflater.from(getActivity()).inflate(R.layout.show_custom_alert, null);
                alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alertMessage = v.findViewById(R.id.alertMessage);
                alertOk = v.findViewById(R.id.alertOk);
                alertCancel = v.findViewById(R.id.alertCancel);
                customAlertImage = v.findViewById(R.id.customAlertImage);
                alert.setCancelable(false);
                Picasso.with(getActivity()).load(ServiceURLs.PRODUCTIMAGEBASEURL + wishList.get(pos).getProduct_image()).transform(new CircleTransform(20, 0)).fit().into(customAlertImage);
                alert.setContentView(v);
                alertCancel.setVisibility(View.GONE);

                alertMessage.setText(wishList.get(pos).getProduct_name() + "\n\n" + "Are you sure to add this product into your wishlist?");
                alert.show();

                alertOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        alert.cancel();
                        Log.e("str", "okk");
                        getAddProduct(wishList.get(pos).getProduct_id());
                        dialog.cancel();

                    }
                });

            }
        });

    }

    private void getAddProduct(String iddd) {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user/addwishlist.json?product_id=" + iddd);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                String res = response.body().toString();
                Log.e("addPrdtRes", res);

                try {

                    JSONObject jsonObject = new JSONObject(res);
                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                        new CustomAlert().showAlert(getActivity(), 1, jsonObject.optString("message"));
                    }

                    else {
                        new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));
                    }

                    //Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.toolbarRelativeBack:

                getActivity().onBackPressed();

                break;

        }

    }

}
