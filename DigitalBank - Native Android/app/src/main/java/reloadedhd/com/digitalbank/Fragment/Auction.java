package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.AuctionListModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A class for Auction where user can participate in the auction and can do bidding for the same
 */
public class Auction extends Fragment implements View.OnClickListener {
    private RelativeLayout relLayout_back_button_auction, biddingRelative;
    private Bundle bundle;
    private ImageView auctionImage;
    private TextView auctionName, auctionBasePrice,/* auctionCurrentPrice,*/
            auctionBiddingPrice, lastBidderName, lastBiddingPoints, timerText,
            userCurrentBal;
    private String bid_hash = null, auction_id = null, start_time, end_time;
    private boolean callService = true;
    private RelativeLayout biddingLinear;
    private SharedPreferences preferences;

    public Auction() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        System.gc();
        //Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_auction, container, false);
        preferences = DigitalBankContoller.getGlobalPreferences();
        initView(view);

        bundle = getArguments();

        if (bundle != null && bundle.getString("val").equalsIgnoreCase("1")) {

            try {

                JSONObject object = new JSONObject(bundle.getString("list"));
                Glide.with(getActivity()).load(ServiceURLs.PRODUCTIMAGEBASEURL + object.optString("product_image").trim()).into(auctionImage);
                auctionName.setText(object.optString("product_name"));
              //  auctionBasePrice.setText("Base Point " + object.optString("auction_price") + " Points");
               //todo for changes on client demand on 28Aug 2018 from base points to auction updated points
                auctionBasePrice.setText("Base Point " + object.optString("current_auction_value") + " Points");
                //auctionCurrentPrice.setText(object.optString("current_auction_value") + " Points");
                auctionBiddingPrice.setText(object.optString("single_bid_value") + " Points");
                lastBidderName.setText(object.optString("last_bidder"));
                lastBiddingPoints.setText(object.optString("current_auction_value") + " Points");
                userCurrentBal.setText("Your Balance : " + preferences.getString("point", "") + " Points");
                bid_hash = object.optString("bid_hash");
                auction_id = object.optString("auction_id");
                start_time = object.optString("start_time");
                end_time = object.optString("end_time");

                checkStartDateNew(start_time, end_time);

            } catch (JSONException e) {

                e.printStackTrace();

            } catch (ParseException pe) {

            }

        } else if (bundle != null && bundle.getString("val").equalsIgnoreCase("2")) {

            AuctionListModel model;
            model = (AuctionListModel) bundle.getSerializable("list");
            Glide.with(getActivity()).load(ServiceURLs.PRODUCTIMAGEBASEURL + model.getProduct_image().trim()).into(auctionImage);
            auctionName.setText(model.getProduct_name());

           // auctionBasePrice.setText(model.getAuction_price() + " Points");
            auctionBasePrice.setText(model.getCurrent_auction_value()+" Points");
            //auctionCurrentPrice.setText(model.getCurrent_auction_value() + " Points");
            auctionBiddingPrice.setText(model.getSingle_bid_value() + " Points");
            lastBidderName.setText(model.getLast_bidder());
            lastBiddingPoints.setText(model.getCurrent_auction_value() + " Points");
            userCurrentBal.setText("Your Balance : " + preferences.getString("point", "") + " Points");
            bid_hash = model.getBid_hash();
            auction_id = model.getAuction_id();

            start_time = model.getStart_time();
            end_time = model.getEnd_time();

            try {

                checkStartDateNew(start_time, end_time);

            } catch (ParseException e) {

                e.printStackTrace();

            }

        }

        return view;

    }

    private void checkStartDateNew(String strtDate, String endDate) throws ParseException {
        System.gc();
        Date localTime = new Date();

        //creating DateFormat for converting time from local timezone to GMT
        DateFormat converter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        String currentDateTimeString = converter.format(localTime);

        Log.e("currenttime", localTime.getTime() + "//");

        String parsedDate = null, auctionEndDate = null, auctionEndTime = null;
        Date initDate = null;

        try {

            initDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            parsedDate = formatter.format(initDate);

            Log.e("endinitdate", initDate.getTime() + "//");

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (localTime.getTime() > initDate.getTime()) {
            timerText.setText("Auction Already completed");
            biddingLinear.setVisibility(View.GONE);
            // startCounter("Auction Completed ", (initDate.getTime()-localTime.getTime()), 0);
        } else if (localTime.getTime() == initDate.getTime()) {
            timerText.setText("Auction Already completed");
            biddingLinear.setVisibility(View.GONE);
            //startCounter("Auction Completed ", (initDate.getTime()-localTime.getTime()), 0);
        } else if (localTime.getTime() < initDate.getTime()) {
            startCounter(" ", (initDate.getTime() - localTime.getTime()), 0);
            biddingLinear.setVisibility(View.VISIBLE);
        }

    }

    //todo for check the various condition before showing the countdown for auction
    private void checkStartDate(String strtDate, String endDate) throws ParseException {
        System.gc();
        Date localTime = new Date();

        //creating DateFormat for converting time from local timezone to GMT
        DateFormat converter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        String currentDateTimeString = converter.format(localTime);

        Log.e("currenttime", localTime.getTime() + "//");

        Log.e("timm", currentDateTimeString + "//" + strtDate + "//" + endDate);

        StringTokenizer token2 = new StringTokenizer(currentDateTimeString, " ");
        String currentDate = null, currentTIme = null;

        while (token2.hasMoreTokens()) {

            currentDate = token2.nextToken();
            currentTIme = token2.nextToken();

        }

        Date date5 = converter.parse(strtDate);
        String strrrr = converter.format(date5);
        Log.e("strttime", date5.getTime() + "//");
        StringTokenizer token1 = new StringTokenizer(strrrr, " ");
        String auctionStartDate = null, auctionStartTime = null;

        while (token1.hasMoreTokens()) {
            auctionStartDate = token1.nextToken();
            auctionStartTime = token1.nextToken();
        }

        Log.e("dss", auctionStartDate + "//" + auctionStartTime);
        Log.e("dss10", currentDate + "//" + currentTIme);

        String parsedDate = null, auctionEndDate = null, auctionEndTime = null;

        try {

            Date initDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            parsedDate = formatter.format(initDate);

            Log.e("endinitdate", initDate.getTime() + "//");

        } catch (ParseException e) {
            e.printStackTrace();
        }

        StringTokenizer tokenn = new StringTokenizer(parsedDate, " ");

        while (tokenn.hasMoreTokens()) {

            auctionEndDate = tokenn.nextToken();
            auctionEndTime = tokenn.nextToken();

        }

        Log.e("enddatt", auctionEndDate + "//" + auctionEndTime);

        //todo for timer with different situation

        //  if (currentDate.compareTo(auctionStartDate) > 0) {
        //   Log.e("da", "currentDate occurs after auctionStartDate");
        if (currentDate.compareTo(auctionEndDate) > 0) {
            Log.e("auc", "current date > enddate");
            timerText.setText("Auction Already completed");
            biddingLinear.setVisibility(View.GONE);
        } else if (currentDate.compareTo(auctionEndDate) < 0) {
            Log.e("audd", "enddate occur after current date");
            biddingLinear.setVisibility(View.VISIBLE);
            SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
            Date dd = time.parse(auctionEndTime);
            Date dd1 = time.parse(currentTIme);
            long milisec = dd.getTime() - dd1.getTime();
//todo for timer
            startCounter(" ", milisec, 0);

        } else if (currentDate.compareTo(auctionEndDate) == 0) {

            Log.e("aghf", "currentdate and auctnend date is today");

            if (currentTIme.compareTo(auctionStartTime) > 0) {

                Log.e("okf", "current time is occur after start time");

                compareCurrentTimeAndEndTime(" ", currentTIme, auctionEndTime);

            } else if (currentTIme.compareTo(auctionStartTime) == 0) {

                Log.e("okf", "current time and start time is same");
                //todo for timer

                compareCurrentTimeAndEndTime(" ", currentTIme, auctionEndTime);

            } else if (currentTIme.compareTo(auctionStartTime) < 0) {

                Log.e("okf", "current time occur before starttime");
                biddingLinear.setVisibility(View.GONE);
                SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
                Date dd = null, dd1 = null;

                try {

                    dd = time.parse(auctionStartTime);
                    dd1 = time.parse(currentTIme);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long milisec = dd.getTime() - dd1.getTime();

                //todo for timer
                startCounter("Auction will start from ", milisec, 1);

            }

        }

    /*    } else if (currentDate.compareTo(auctionStartDate) < 0) {

            Log.e("da", "auctionStartDate occurs after currentDate//currentDate occurs before auctionStartDate");
            timerText.setText("Auction will start from " + auctionStartDate);
            biddingLinear.setVisibility(View.GONE);

        } else if (currentDate.compareTo(auctionStartDate) == 0) {

            Log.e("da", "currentDate and auctionStartDate occurs on the same day");

            if (currentTIme.compareTo(auctionStartTime) > 0) {

                Log.e("time", "current time is greater then start time");
                SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
                Date dd = time.parse(auctionEndTime);
                Date dd1 = time.parse(currentTIme);
                long milisec = dd.getTime() - dd1.getTime();
                Log.e("mill", milisec + "//");
                biddingLinear.setVisibility(View.VISIBLE);

//todo for timer
                startCounter(" ", milisec, 0);

            } else if (currentTIme.compareTo(auctionStartTime) < 0) {
                Log.e("time1", "current time is less then start time");
                //timerText.setText("Auction will start from "+auctionStartTime);
                SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
                Date dd = time.parse(auctionStartTime);
                Date dd1 = time.parse(currentTIme);
                long milisec = dd.getTime() - dd1.getTime();
                Log.e("mill", milisec + "//");
                biddingLinear.setVisibility(View.GONE);
//todo for timer
                startCounter("Auction will start within ", milisec, 1);

            }

        }*/

    }

    private void compareCurrentTimeAndEndTime(String msg, String currentTIme, String auctionEndTime) {

        if (currentTIme.compareTo(auctionEndTime) > 0) {

            Log.e("time", "current time is greater then end time");
            timerText.setText("Auction Already completed");
            biddingLinear.setVisibility(View.GONE);

        } else if (currentTIme.compareTo(auctionEndTime) < 0) {

            Log.e("time1", "current time is less then end time");
            biddingLinear.setVisibility(View.VISIBLE);
            SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
            Date dd = null, dd1 = null;
            try {
                dd = time.parse(auctionEndTime);
                dd1 = time.parse(currentTIme);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long milisec = dd.getTime() - dd1.getTime();
//todo for timer
            startCounter(msg, milisec, 0);

        }

    }

    //todo for show countdown
    private void startCounter(final String msg, long time, final int val) {
        Log.e("timmm", time + "/");
        new CountDownTimer(time, 1000) {

            public void onTick(long millisUntilFinished) {

                timerText.setText((msg + TimeUnit.MILLISECONDS.toDays(millisUntilFinished) + " Days " + String.format("%02d:%02d:%02d",
                        /*TimeUnit.MILLISECONDS.toDays(millisUntilFinished),*/
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)))) + " Minutes ");

            }

            public void onFinish() {

               // timerText.setText("done!");
                if (val == 1) {
                    biddingLinear.setVisibility(View.VISIBLE);
                    //need to start the timer from current to end time
                    try {
                        checkStartDate(start_time, end_time);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    biddingLinear.setVisibility(View.GONE);
                }

            }

        }.start();

    }

    //todo for init view
    private void initView(View view) {

        biddingRelative = view.findViewById(R.id.biddingRelative);

        userCurrentBal = view.findViewById(R.id.userCurrentBal);
        auctionImage = view.findViewById(R.id.auctionImage);
        auctionName = view.findViewById(R.id.auctionName);
        auctionBasePrice = view.findViewById(R.id.auctionBasePrice);
        // auctionCurrentPrice = view.findViewById(R.id.auctionCurrentPrice);
        auctionBiddingPrice = view.findViewById(R.id.auctionBiddingPrice);
        lastBidderName = view.findViewById(R.id.lastBidderName);
        lastBiddingPoints = view.findViewById(R.id.lastBiddingPoints);
        timerText = view.findViewById(R.id.timerText);
        biddingLinear = view.findViewById(R.id.biddingLinear);

        relLayout_back_button_auction = view.findViewById(R.id.relLayout_back_button_auction);
        relLayout_back_button_auction.setOnClickListener(this);
        biddingRelative.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.relLayout_back_button_auction:

                getActivity().onBackPressed();

                break;

            case R.id.biddingRelative:
                bidding();
                break;
        }

    }

    //todo for bidding
    private void bidding() {

        callService = false;
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("auction_id", RequestBody.create(MediaType.parse("multipart/form-data"), auction_id));
        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getPostResponse(Global.acceptHeader, Global.token, "v1/user/bid.json", body);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                pd.cancel();
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {


                String res = response.body().toString();
                Log.e("bidres", res);

                try {

                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                        JSONObject object = jsonObject.optJSONObject("user_details");

                        //auctionCurrentPrice.setText(object.optString("current_auction_value") + " Points");
                        //auctionBiddingPrice.setText(object.optString("current_auction_value"));
                        lastBidderName.setText(object.optString("name"));
                        //todo on client request 28Aug 2018
                        auctionBasePrice.setText(object.optString("current_auction_value") + " Points");
                        lastBiddingPoints.setText(object.optString("current_auction_value") + " Points");
                        userCurrentBal.setText("Your Balance : " + object.optString("current_bal") + " Points");


                    }
                    else {

                        new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }
                callService = true;
                updateData();
            }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                //Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    @Override
    public void onStart() {
        super.onStart();
        callService = true;
        updateData();

    }

    @Override
    public void onStop() {
        super.onStop();
        callService = false;

    }

    //todo for get bidding data after a perticular time
    private void updateData() {

        Timer t = new Timer();
        //Set the schedule function and rate

        t.scheduleAtFixedRate(new TimerTask() {

                                  @Override
                                  public void run() {

                                      if (callService) {

                                          HashMap<String, RequestBody> body = new HashMap<>();
                                          //auction_id : required,bid_hash
                                          body.put("auction_id", RequestBody.create(MediaType.parse("multipart/form-data"), auction_id));
                                          body.put("bid_hash", RequestBody.create(MediaType.parse("multipart/form-data"), bid_hash));
                                          Service service = Service.retrofit.create(Service.class);
                                          Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/auction/bid_details.json?auction_id=" + auction_id + "&bid_hash=" + bid_hash);
                                          call.enqueue(new Callback<JsonObject>() {
                                              @Override
                                              public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                                  if (response.code()==401){
                                                      Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                                                      getActivity().startActivity(loginIntent);
                                                      getActivity().finish();

                                                  }

                                                  String res = response.body().toString();
                                                  Log.e("refre", res);

                                                  try {

                                                      JSONObject jsonObject = new JSONObject(res);

                                                      if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                                                          JSONArray array = jsonObject.optJSONArray("data");

                                                          for (int i = 0; i < array.length(); i++) {

                                                              JSONObject object = array.optJSONObject(i);
                                                              //auctionCurrentPrice.setText(object.optString("current_auction_value") + " Points");
                                                              //auctionBiddingPrice.setText(object.optString("current_auction_value"));
                                                              lastBidderName.setText(object.optString("last_bidder"));
                                                              lastBiddingPoints.setText(object.optString("current_auction_value") + " Points");
                                                             //todo on client request on 28 Aug 2018
                                                              auctionBasePrice.setText(object.optString("current_auction_value") + " Points");

                                                              //userCurrentBal.setText("Your Balance : "+object.optString("current_bal")+" Points");

                                                          }

                                                      }


                                                  } catch (JSONException e) {

                                                      e.printStackTrace();

                                                  }

                                              }

                                              @Override
                                              public void onFailure(Call<JsonObject> call, Throwable t) {

                                                  Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();

                                              }

                                          });

                                      }

                                  }

                              },
                //Set how long before to start calling the TimerTask (in milliseconds)
                1000,
                //Set the amount of time between each execution (in milliseconds)
                20000);

    }

}
