package reloadedhd.com.digitalbank.Activity;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import reloadedhd.com.digitalbank.R;

public class TermsConditionActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout toolbarRelativeBack;
    private TextView toolbarTitle;
    private WebView webview;
    private String url;
    private ProgressBar webviewProgressbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.term_condition);
        toolbarRelativeBack = findViewById(R.id.toolbarRelativeBack);
        toolbarTitle = findViewById(R.id.toolbarTitle);
        webview = findViewById(R.id.webview);
        webviewProgressbar = findViewById(R.id.webviewProgressbar);

        if (getIntent().getStringExtra("val").equalsIgnoreCase("1")) {
            //todo for terms
            toolbarTitle.setText(getString(R.string.policy));
            url = "https://quizme.com.my/digital_bank/policy";
        } else if (getIntent().getStringExtra("val").equalsIgnoreCase("2")) {
            //todo for privacy policy
            toolbarTitle.setText(getString(R.string.term_condition));
            url = "https://quizme.com.my/digital_bank/terms";
        }

        toolbarRelativeBack.setOnClickListener(this);
        webview.getSettings().setJavaScriptEnabled(true); // enable javascript

        webview.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(TermsConditionActivity.this, description, Toast.LENGTH_SHORT).show();
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webviewProgressbar.setVisibility(View.GONE);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                webviewProgressbar.setVisibility(View.VISIBLE);
            }

        });

        webview.loadUrl(url);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.toolbarRelativeBack:

                onBackPressed();

                break;

        }

    }

}
