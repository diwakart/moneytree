package reloadedhd.com.digitalbank.model;

public class ChildSpentHistoryModel {
    private String transaction_id;
    private String transaction_points;
    private String remarks;
    private String transaction_time;
    private String type;

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getTransaction_points() {
        return transaction_points;
    }

    public void setTransaction_points(String transaction_points) {
        this.transaction_points = transaction_points;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTransaction_time() {
        return transaction_time;
    }

    public void setTransaction_time(String transaction_time) {
        this.transaction_time = transaction_time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
