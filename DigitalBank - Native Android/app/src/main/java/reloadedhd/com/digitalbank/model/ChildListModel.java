package reloadedhd.com.digitalbank.model;

import java.io.Serializable;

public class ChildListModel  implements Serializable{

    private String id;
    private String request_type;
    private String request_status;
    private String points;
    private String student_id;
    private String name;
    private String mobile;
    private String email;
    private String dob;
    private String address;
    private String avtar;
    private String current_point;
    private String account_type;
    private String requested_time;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRequest_type() {
        return request_type;
    }

    public void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    public String getRequest_status() {
        return request_status;
    }

    public void setRequest_status(String request_status) {
        this.request_status = request_status;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvtar() {
        return avtar;
    }

    public void setAvtar(String avtar) {
        this.avtar = avtar;
    }

    public String getCurrent_point() {
        return current_point;
    }

    public void setCurrent_point(String current_point) {
        this.current_point = current_point;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getRequested_time() {
        return requested_time;
    }

    public void setRequested_time(String requested_time) {
        this.requested_time = requested_time;
    }


}
