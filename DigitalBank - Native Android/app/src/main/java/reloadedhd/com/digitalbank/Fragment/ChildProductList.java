package reloadedhd.com.digitalbank.Fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.ChildProductListAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.ChildProductModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import reloadedhd.com.digitalbank.transform.CircleTransform;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChildProductList extends Fragment implements View.OnClickListener {
    private RecyclerView childProductList;
    private RelativeLayout toolbarRelativeBack;
    private TextView toolbarTitle, childListNodata, heading;
    private LinearLayoutManager manager;
    private ArrayList<ChildProductModel> arrayList;
    private ChildProductListAdapter adapter;
    private ShimmerFrameLayout productListShimmer;
    private int currentPage = 1, totalItemCount, lastVisibleItem;
    private float totalPages;
    private boolean loading = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.child_product_list, container, false);
        arrayList = new ArrayList<>();
        arrayList.clear();

        initView(view);
        return view;
    }

    private void initView(View view) {

        productListShimmer = view.findViewById(R.id.productListShimmer);
        childProductList = view.findViewById(R.id.childProductList);
        toolbarRelativeBack = view.findViewById(R.id.toolbarRelativeBack);
        toolbarTitle = view.findViewById(R.id.toolbarTitle);
        childListNodata = view.findViewById(R.id.childListNodata);
        heading = view.findViewById(R.id.heading);
        toolbarTitle.setText("Catalog");
        productListShimmer.stopShimmer();
        manager = new GridLayoutManager(getActivity(), 2);
        childProductList.setLayoutManager(manager);
        adapter = new ChildProductListAdapter(getActivity(), arrayList);
        childProductList.setAdapter(adapter);

        adapter.onAddItemCLick(new ChildProductListAdapter.ChildAdapterClick() {
            @Override
            public void onAddItem(final int pos) {

                TextView alertMessage, alertOk, alertCancel;
                ImageView customAlertImage;
                final Dialog dialog = new Dialog(getActivity());
                View v = LayoutInflater.from(getActivity()).inflate(R.layout.show_custom_alert, null);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertMessage = v.findViewById(R.id.alertMessage);
                alertOk = v.findViewById(R.id.alertOk);
                alertCancel = v.findViewById(R.id.alertCancel);
                customAlertImage = v.findViewById(R.id.customAlertImage);
                Picasso.with(getActivity()).load(ServiceURLs.PRODUCTIMAGEBASEURL + arrayList.get(pos).getProduct_image()).transform(new CircleTransform(20, 0)).fit().into(customAlertImage);
                dialog.setContentView(v);

                alertMessage.setText(arrayList.get(pos).getProduct_name() + "\n\n" + "Are you sure to add this product into your wishlist?");
                dialog.show();

                alertOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.cancel();
                        Log.e("str", "okk");
                        getAddProduct(pos);
                        arrayList.get(pos).setAdded(true);
                        adapter.notifyDataSetChanged();

                    }
                });

                alertCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

            }

        });

        toolbarRelativeBack.setOnClickListener(this);


        this.manager = new GridLayoutManager(getActivity(), 2) {

            @Override
            public boolean canScrollVertically() {
                return true;
            }

        };

        childProductList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == 0 && totalPages > (float) currentPage) {

                    currentPage++;
                    getProductList();

                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


            }

        });

        //this.childProductList.setLayoutManager(this.manager);

        getProductList();

    }

    private void getAddProduct(int pos) {

        Log.e("prdctid", arrayList.get(pos).getProduct_id() + "");
        Log.e("id", arrayList.get(pos).getId() + "");
        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user/addwishlist.json?product_id=" + arrayList.get(pos).getId());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();

                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                String res = response.body().toString();
                Log.e("addPrdtRes", res);

                try {

                    JSONObject jsonObject = new JSONObject(res);
                    //Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                        new CustomAlert().showAlert(getActivity(), 1, jsonObject.optString("message"));
                    } else {
                        new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));
                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                //Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT);
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    private void getProductList() {

        loading = false;

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/auction/products.json" + "?perpage=5&current_page=" + currentPage);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                productListShimmer.stopShimmer();
                productListShimmer.setVisibility(View.GONE);
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                    productListShimmer.stopShimmer();
                    productListShimmer.setVisibility(View.GONE);

                    String res = response.body().toString();
                    Log.e("ressss", res);

                    try {

                        JSONObject jsonObject = new JSONObject(res);

                        JSONObject jsonObject1 = jsonObject.optJSONObject("meta");
                        totalPages = Float.valueOf(Float.valueOf(jsonObject1.optString("total")) / Float.valueOf(jsonObject1.optString("perpage")));
                        //  Log.e("ttlpg", totalPages + "//");

                        JSONArray array = jsonObject.optJSONArray("data");

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);
                            ChildProductModel model = new ChildProductModel();
                            model.setId(object.optString("id"));
                            model.setProduct_id(object.optString("product_id"));
                            model.setProduct_image(object.optString("product_image"));
                            model.setProduct_name(object.optString("product_name"));
                            model.setProduct_price(object.optString("base_price"));
                            model.setCategory_id(object.optString("category_id"));
                            model.setCategory_name(object.optString("category_name"));
                            arrayList.add(model);

                        }

                        if (!arrayList.isEmpty()) {
                            adapter.notifyDataSetChanged();
                            childListNodata.setVisibility(View.GONE);
                            heading.setVisibility(View.VISIBLE);
                            childProductList.setVisibility(View.VISIBLE);

                        } else {

                            heading.setVisibility(View.GONE);
                            childProductList.setVisibility(View.GONE);
                            childListNodata.setVisibility(View.VISIBLE);

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                new CustomAlert().showAlert(getActivity(), 0, "Failure");
            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.toolbarRelativeBack:

                Global.changeFragment(getActivity(), new Wishlist());

                break;

        }

    }

}
