package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TagParent extends Fragment implements View.OnClickListener {
    private Button button_invite_addParent;
    private ImageView searchParent, parentAddImageProfile;
    private EditText search_edt_addParent;
    private LinearLayout linear_parentDetail_addParent,addParentDOBLinear;
    private TextView toolbarTitle, tagParentName, tagParentEmail, tagParentMobile, tagParentDOB;
    private RelativeLayout toolbarRelativeBack;
    private String id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tag_parent, null);
        initView(v);

        return v;
    }

    private void initView(View v) {

        toolbarRelativeBack = v.findViewById(R.id.toolbarRelativeBack);
        toolbarTitle = v.findViewById(R.id.toolbarTitle);

        button_invite_addParent = v.findViewById(R.id.button_invite_addParent);
        searchParent = v.findViewById(R.id.searchParent);
        parentAddImageProfile = v.findViewById(R.id.parentAddImageProfile);
        search_edt_addParent = v.findViewById(R.id.search_edt_addParent);
        linear_parentDetail_addParent = v.findViewById(R.id.linear_parentDetail_addParent);
        tagParentName = v.findViewById(R.id.tagParentName);
        tagParentEmail = v.findViewById(R.id.tagParentEmail);
        tagParentMobile = v.findViewById(R.id.tagParentMobile);
        tagParentDOB = v.findViewById(R.id.tagParentDOB);

        addParentDOBLinear = v.findViewById(R.id.addParentDOBLinear);

        toolbarTitle.setText("Tag Parent");

        button_invite_addParent.setOnClickListener(this);
        searchParent.setOnClickListener(this);
        toolbarRelativeBack.setOnClickListener(this);
        getTaggedParent();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.toolbarRelativeBack:

                getActivity().onBackPressed();

                break;

            case R.id.searchParent:
                if (search_edt_addParent.getText().toString().trim().length() == 0) {
                    new CustomAlert().showAlert(getActivity(),0, "Enter mobile to search...");
                    return;
                }
                searchParentByMobile(search_edt_addParent.getText().toString().trim());

                break;

            case R.id.button_invite_addParent:
                inviteParent(id);

                break;

        }

    }

    private void getTaggedParent() {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user/details.json");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {
                String res = response.body().toString();
                Log.e("fdf", res);
                pd.cancel();

                try {

                    JSONObject jsonObject = new JSONObject(res);
                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                        JSONArray array = jsonObject.getJSONArray("data");

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);

                            if (object.has("parent_id")) {
                                id = object.optString("parent_id");
                            }

                            if (object.has("parent_name") && object.optString("parent_name") != null && !object.optString("parent_name").equalsIgnoreCase("null")) {
                                linear_parentDetail_addParent.setVisibility(View.VISIBLE);
                                tagParentName.setText(object.optString("parent_name"));
                                tagParentEmail.setText(object.optString("parent_name"));
                            }

                            if (object.has("parent_mobile")) {
                                tagParentMobile.setText(object.optString("parent_mobile"));
                            }

                            if (object.has("email") && !object.optString("email").isEmpty() && !object.optString("email").trim().equalsIgnoreCase("") && !object.optString("email").equalsIgnoreCase("null")) {
                                tagParentDOB.setText(object.optString("email"));
                                addParentDOBLinear.setVisibility(View.VISIBLE);
                            }else {
                                addParentDOBLinear.setVisibility(View.GONE);

                            }

                            if (object.has("parent_avtar")) {
                                Picasso.with(getActivity()).load(ServiceURLs.USERPROFILEIMAGEBASEURL + object.optString("parent_avtar")).fit().into(parentAddImageProfile);
                            }

                            button_invite_addParent.setVisibility(View.INVISIBLE);

                        }

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.cancel();
               new CustomAlert().showAlert(getActivity(),0, "Failure");

            }

        });

    }

    private void inviteParent(String id) {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);

        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/child/tag.json?user_id=" + id);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }
                pd.cancel();
                String res = response.body().toString();
                Log.e("ress", res);

                try {

                    JSONObject jsonObject = new JSONObject(res);
                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                        Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();

                    }else {
                      new CustomAlert().showAlert(getActivity(),0, jsonObject.optString("message"));
                    }
                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                new CustomAlert().showAlert(getActivity(),0, "Failure");
            }

        });

    }

    private void searchParentByMobile(String mobile) {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);

        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user/search.json?mobile=" + mobile + "&type=3");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                pd.cancel();
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }
                String res = response.body().toString();
                Log.e("srchprtres", res);

                try {

                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                        linear_parentDetail_addParent.setVisibility(View.VISIBLE);

                        JSONObject object = jsonObject.getJSONObject("data");
                        id = object.get("id").toString();
                        tagParentName.setText(object.optString("name"));
                        tagParentDOB.setText(object.optString("email"));
                        tagParentMobile.setText(object.optString("mobile"));
                        tagParentEmail.setText(object.optString("name"));

                        button_invite_addParent.setVisibility(View.VISIBLE);

                        if (!object.optString("avtar").equalsIgnoreCase(" ") && object.optString("avtar") != null && !object.optString("avtar").equalsIgnoreCase("null")) {
                            ImageLoader imageLoader = ImageLoader.getInstance();
                            imageLoader.displayImage(ServiceURLs.USERPROFILEIMAGEBASEURL + object.optString("avtar").trim(), parentAddImageProfile, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                    Log.e("imgstrt", "ok/" + imageUri);
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    Log.e("imgfail", "fail" + failReason.toString());
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    parentAddImageProfile.setImageBitmap(loadedImage);

                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {
                                    Log.e("imgcanc", imageUri + "");
                                }
                            });
                        }

                    } else {

                       new CustomAlert().showAlert(getActivity(),0, jsonObject.optString("message"));

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                new CustomAlert().showAlert(getActivity(),0, "Failure");
            }

        });

    }

}
