package reloadedhd.com.digitalbank.model;

public class SpentParentModel {
    private String product_name;
    private String spent_points;

    public String getSpent_time() {
        return spent_time;
    }

    public void setSpent_time(String spent_time) {
        this.spent_time = spent_time;
    }

    private String spent_time;

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getSpent_points() {
        return spent_points;
    }

    public void setSpent_points(String spent_points) {
        this.spent_points = spent_points;
    }

    public String getProdcut_image() {
        return prodcut_image;
    }

    public void setProdcut_image(String prodcut_image) {
        this.prodcut_image = prodcut_image;
    }

    private String prodcut_image;
}
