package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.model.EarnedParentModel;

/**
 * Created by Admin on 5/30/2018.
 */

public class EarnedParentAdapter extends RecyclerView.Adapter<EarnedParentAdapter.ViewHolder> {
    private Context context;
    private ArrayList<EarnedParentModel> arrayList;

    public EarnedParentAdapter(Context context, ArrayList<EarnedParentModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.earned_parent_adapter, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.earnedCategoryName.setText(arrayList.get(position).getCategory_name());
        holder.earnedPoints.setText(arrayList.get(position).getEarn_points() + " Points");
        holder.earnedQuestion.setText(arrayList.get(position).getQuestion());
        Date date = null;
        String outputString = null;

        try {

            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(arrayList.get(position).getEarn_point_time());

        } catch (ParseException e) {

            e.printStackTrace();

        }

        outputString = new SimpleDateFormat("dd-MM-yyyy").format(date);
        String newString = new SimpleDateFormat("HH:mm").format(date);

        //holder.auctionStartTime.setText("Auction Start Date : " + outputString + " Time : " + newString);

        holder.earnedPointsDate.setText("EarnPoint Date : " + outputString + " Time : " + newString);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView earnedCategoryName, earnedQuestion, earnedPoints, earnedPointsDate;

        public ViewHolder(View itemView) {
            super(itemView);
            earnedCategoryName = itemView.findViewById(R.id.earnedCategoryName);
            earnedQuestion = itemView.findViewById(R.id.earnedQuestion);
            earnedPoints = itemView.findViewById(R.id.earnedPoints);
            earnedPointsDate = itemView.findViewById(R.id.earnedPointsDate);

        }

    }

}
