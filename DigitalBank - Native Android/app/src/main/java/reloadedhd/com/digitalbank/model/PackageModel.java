package reloadedhd.com.digitalbank.model;

public class PackageModel {
    private String id;
    private String name;
    private String amount;
    private String points;
    private String S_No;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getS_No() {
        return S_No;
    }

    public void setS_No(String s_No) {
        S_No = s_No;
    }


}
