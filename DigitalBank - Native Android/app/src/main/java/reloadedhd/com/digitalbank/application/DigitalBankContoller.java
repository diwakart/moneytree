package reloadedhd.com.digitalbank.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDexApplication;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.io.File;

import reloadedhd.com.digitalbank.imageLoader.AuthImageDownloaderNew;

public class DigitalBankContoller extends MultiDexApplication {

    static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .imageDownloader(new AuthImageDownloaderNew(getApplicationContext(), 10000, 10000))
                .writeDebugLogs() // Remove for release app
                .build();
        //Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);

       // TypefaceUtil.overrideFont(getApplicationContext(), "serif", "Fonts/Roboto_Bold.ttf");

        deleteCache(context);


    }

    public static Context getContext() {
        return context;
    }

    public static SharedPreferences getGlobalPreferences() {

        return getContext().getSharedPreferences("LoginPref", 0);

    }

    public static SharedPreferences getTokenPreferences() {

        return getContext().getSharedPreferences("Token", 0);

    }

    public static void deleteCache(Context context) {

        try {

            File dir = context.getCacheDir();
            deleteDir(dir);

        } catch (Exception e) {
        }

    }

    public static boolean deleteDir(File dir) {

        if (dir != null && dir.isDirectory()) {

            String[] children = dir.list();

            for (int i = 0; i < children.length; i++) {

                boolean success = deleteDir(new File(dir, children[i]));

                if (!success) {

                    return false;

                }

            }

            return dir.delete();

        } else if (dir != null && dir.isFile()) {

            return dir.delete();

        } else {

            return false;

        }

    }

    /* to check network connection */
    public boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifi != null && wifi.isConnected()) {
            return true;
        }

        NetworkInfo mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobile != null && mobile.isConnected()) {
            return true;
        }

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }


        return false;
    }

}
