package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Fragment.ParentScreen;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.global.DigitalGlobal;
import reloadedhd.com.digitalbank.model.WishlistParentModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import reloadedhd.com.digitalbank.transform.CircleTransform;

public class WishlistAdapter extends RecyclerView.Adapter<WishlistAdapter.Holder> {
    private Context context;
    private ArrayList<WishlistParentModel> arrayList;

    public WishlistAdapter(Context context, ArrayList<WishlistParentModel> arrayList) {

        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.wishlist_adapter, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        holder.wishlistProductName.setText(arrayList.get(position).getProduct_name());
        holder.wishlistProductPoints.setText(arrayList.get(position).getProduct_price() + " Points");

        if (arrayList.get(position).getProduct_image() != null && !arrayList.get(position).getProduct_image().trim().isEmpty()) {
         /*ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(ServiceURLs.PRODUCTIMAGEBASEURL + arrayList.get(position).getProduct_image(), holder.wishlistAdapterImage, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    Log.e("imgstrt", "ok/" + imageUri);
                }
                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.wishlistAdapterImage.setImageBitmap(loadedImage);
                }
                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });*/

            Picasso.with(context).load(ServiceURLs.PRODUCTIMAGEBASEURL + arrayList.get(position).getProduct_image()).transform(new CircleTransform(20, 0)).fit().into(holder.wishlistAdapterImage);

        }

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView wishlistAdapterImage;
        TextView wishlistProductName, wishlistProductPoints;

        public Holder(View itemView) {
            super(itemView);
            wishlistAdapterImage = itemView.findViewById(R.id.wishlistAdapterImage);
            wishlistProductName = itemView.findViewById(R.id.wishlistProductName);
            wishlistProductPoints = itemView.findViewById(R.id.wishlistProductPoints);
        }

    }

}
