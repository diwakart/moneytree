package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.model.WishlistImageModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import reloadedhd.com.digitalbank.transform.CircleTransform;

public class WishListImageAdapter extends RecyclerView.Adapter<WishListImageAdapter.ImageHolder> {
    private Context context;
    private ArrayList<WishlistImageModel> arrayList;

    public WishListImageAdapter(Context context, ArrayList<WishlistImageModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.image_adapter, parent, false);
        return new ImageHolder(v);

    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {

        Picasso.with(context).load(ServiceURLs.PRODUCTIMAGEBASEURL + arrayList.get(position).getProduct_image()).transform(new CircleTransform(20, 0)).fit().into(holder.wishImageAdapterImage);
        holder.wishImageAdapterText.setText(arrayList.get(position).getProduct_name());

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    class ImageHolder extends RecyclerView.ViewHolder {
        ImageView wishImageAdapterImage;
        TextView wishImageAdapterText;

        public ImageHolder(View itemView) {
            super(itemView);
            wishImageAdapterImage = itemView.findViewById(R.id.wishImageAdapterImage);
            wishImageAdapterText = itemView.findViewById(R.id.wishImageAdapterText);

        }

    }

}
