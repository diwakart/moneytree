package reloadedhd.com.digitalbank.WebService;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Activity.MainActivity2;
import reloadedhd.com.digitalbank.Utility.Global;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 5/25/2018.
 */

public class Login_Api {

    Context context;
    String email_id, password;
    ProgressDialog progressDialog;

    public void loginIn(LoginScreen loginScreen, String email_id, final String password) {
        this.context = loginScreen;
        this.email_id = email_id;
        this.password = password;

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.login(email_id, password);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();

                try {

                    String responsLogin = response.body().toString();
                    Log.e("reslogin", responsLogin);

                    JSONObject jsonObject = new JSONObject(responsLogin);

                        if (jsonObject.has("success")) {

                            JSONObject jsonObject1 = jsonObject.getJSONObject("success");

                            String token = jsonObject1.get("token").toString();
                            Log.e("tokenGet", token);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                String name = jsonObject2.get("name").toString();
                                String mobile = jsonObject2.get("mobile").toString();
                                String type = jsonObject2.get("type").toString();

                                SharedPreferences sharedPreferences = context.getSharedPreferences("LoginPref", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                editor.putString("token", "Bearer " + token);
                                editor.putString("name", name);

                                if (jsonObject2.has("email") && jsonObject2.optString("email") != null) {

                                    editor.putString("email", jsonObject2.optString("email"));

                                }

                                editor.putString("mobile", mobile);
                                editor.putString("loginStatus", type);
                                editor.putString("avtar", jsonObject2.optString("avtar"));
                                editor.putString("point", jsonObject2.optString("current_point"));
                                editor.putString("user_id", jsonObject2.optString("user_id"));

                                //editor.putString("password", password);
                                editor.commit();

                                Global.token = "Bearer " + token;

                            }

                            Intent intent = new Intent(context, MainActivity2.class);
                            context.startActivity(intent);
                            ((Activity) context).finish();
                        } else if (jsonObject.has("error")) {

                            Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();

                        }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                progressDialog.dismiss();

                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();

            }

        });











/*
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.login("application/x-www-form-urlencoded",email_id,password);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e("responseLogin",response.message());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(context,  "Failure", Toast.LENGTH_SHORT).show();
                Log.e("getError",t.getMessage());
                Log.e("getError",t.getCause()+"");
                Log.e("getError",t.getLocalizedMessage());

            }
        });
*/


    }
}
