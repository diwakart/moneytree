package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.model.AuctionListModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import reloadedhd.com.digitalbank.transform.CircleTransform;

public class AuctionListAdapter extends RecyclerView.Adapter<AuctionListAdapter.Holder> {
    private Context context;
    private ArrayList<AuctionListModel> arrayList;
    ListClick click;

    public AuctionListAdapter(Context context, ArrayList<AuctionListModel> arrayList) {

        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.auction_list_adapter, parent, false);
        return new Holder(v);

    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        holder.auctionCategoryName.setText(arrayList.get(position).getCategory_name());
        holder.auctionPrice.setText("Base Price : " + arrayList.get(position).getAuction_price() + " Points");
        if (arrayList.get(position).getProduct_name() != null && !arrayList.get(position).getProduct_name().isEmpty() && !arrayList.get(position).getProduct_name().trim().isEmpty()) {
            holder.auctionProductName.setVisibility(View.VISIBLE);
            holder.auctionProductName.setText("Product Name : " + arrayList.get(position).getProduct_name());
        } else {
            holder.auctionProductName.setVisibility(View.GONE);
        }

        Picasso.with(context).load(ServiceURLs.PRODUCTIMAGEBASEURL + arrayList.get(position).getProduct_image()).transform(new CircleTransform(20, 0)).fit().into(holder.auctionListAdapterImage);
        Date date = null, date1 = null;
        String outputString = null, endDate = null, endTime = null;

        try {

            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(arrayList.get(position).getStart_time());
            date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(arrayList.get(position).getEnd_time());

        } catch (ParseException e) {

            e.printStackTrace();

        }

        outputString = new SimpleDateFormat("dd-MM-yyyy").format(date);
        String newString = new SimpleDateFormat("HH:mm").format(date);

        endDate = new SimpleDateFormat("dd-MM-yyyy").format(date1);
        endTime = new SimpleDateFormat("HH:mm").format(date1);

        holder.auctionStartTime.setText("Start Date : " + outputString + "\nTime : " + newString);
        Date localTime = new Date();

        //creating DateFormat for converting time from local timezone to GMT
        DateFormat converter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        String currentDateTimeString = converter.format(localTime);

        StringTokenizer token2 = new StringTokenizer(currentDateTimeString, " ");
        String currentDate = null, currentTIme = null;

        while (token2.hasMoreTokens()) {

            currentDate = token2.nextToken();
            currentTIme = token2.nextToken();

        }
        Log.e("okll", currentDate + "//" + outputString + "//" + currentTIme + "/" + newString + "/" + outputString + "/" + currentTIme + "/" + newString);
        if (arrayList.get(position).getUser_participated().equalsIgnoreCase("1")) {

            if (currentDate.compareTo(outputString) < 0) {
                holder.upcomingAuction.setVisibility(View.VISIBLE);
                holder.auctionParticipate.setVisibility(View.GONE);
                holder.auctionGo.setVisibility(View.GONE);

            } else if (currentTIme.compareTo(newString) < 0) {

                holder.upcomingAuction.setVisibility(View.VISIBLE);
                holder.auctionParticipate.setVisibility(View.GONE);
                holder.auctionGo.setVisibility(View.GONE);

            } else {

                holder.auctionGo.setVisibility(View.VISIBLE);
                holder.auctionParticipate.setVisibility(View.GONE);
            }

        } else {

            if (currentDate.compareTo(outputString) < 0) {
                holder.upcomingAuction.setVisibility(View.VISIBLE);
                holder.auctionParticipate.setVisibility(View.GONE);
                holder.auctionGo.setVisibility(View.GONE);

            } else if (currentDate.compareTo(outputString) > 0) {
                holder.upcomingAuction.setVisibility(View.GONE);
                holder.auctionParticipate.setVisibility(View.VISIBLE);
                holder.auctionGo.setVisibility(View.GONE);

            } else if (currentTIme.compareTo(newString) < 0) {

                holder.upcomingAuction.setVisibility(View.VISIBLE);
                holder.auctionParticipate.setVisibility(View.GONE);
                holder.auctionGo.setVisibility(View.GONE);

            } else {

                holder.upcomingAuction.setVisibility(View.GONE);
                holder.auctionParticipate.setVisibility(View.VISIBLE);

            }

        }

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView upcomingAuction, auctionCategoryName, auctionPrice, auctionStartTime, auctionProductName;
        Button auctionParticipate, auctionGo;
        ImageView auctionListAdapterImage;

        public Holder(View itemView) {

            super(itemView);
            upcomingAuction = itemView.findViewById(R.id.upcomingAuction);
            auctionCategoryName = itemView.findViewById(R.id.auctionCategoryName);
            auctionPrice = itemView.findViewById(R.id.auctionPrice);
            auctionStartTime = itemView.findViewById(R.id.auctionStartTime);
            auctionListAdapterImage = itemView.findViewById(R.id.auctionListAdapterImage);
            auctionProductName = itemView.findViewById(R.id.auctionProductName);

            auctionParticipate = itemView.findViewById(R.id.auctionParticipate);
            auctionGo = itemView.findViewById(R.id.auctionGo);
            auctionParticipate.setOnClickListener(this);
            auctionGo.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.auctionGo:

                    click.onGoCLick(getAdapterPosition());

                    break;

                case R.id.auctionParticipate:

                    click.onParticipateClick(getAdapterPosition());

                    break;

            }

        }

    }

    public void onAuctionListClick(ListClick click) {
        this.click = click;
    }

    public interface ListClick {

        public void onParticipateClick(int pos);

        public void onGoCLick(int pos);

    }

}
