package reloadedhd.com.digitalbank.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import org.json.JSONException;
import org.json.JSONObject;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.global.DigitalGlobal;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentGatewayStripe extends Fragment {
    RelativeLayout relLayout_back_button_paymentgateway;

    String CardNumber, Cvc, Currency;
    Integer ExpMonth, ExpYear;
    //public static final String PUBLISHABLE_KEY = "pk_live_SD5o8FdYouJQlI2Mx7FYGFDz"; // live mode
    public static final String PUBLISHABLE_KEY = "pk_test_UEMWJO4bGsJVHPeubK0GeeVO";  // test mode
    //https://dashboard.stripe.com/account/apikeys
    Button button;
    EditText cardNumber, editText_CVV, cardHolder_id;
    ProgressDialog progressDialog;
    Spinner spinner_month, spinner_year;
    String month = "";
    String year = "";
    String amountPoints, points;
    private SharedPreferences preferences;

    public PaymentGatewayStripe() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.payment_gateway, container, false);

        relLayout_back_button_paymentgateway = view.findViewById(R.id.relLayout_back_button_paymentgateway);
        preferences = DigitalBankContoller.getGlobalPreferences();

        Bundle bundle = getArguments();
        if (bundle != null) {
            amountPoints = bundle.getString("amount");
            points = bundle.getString("amountPoint");
        }

        relLayout_back_button_paymentgateway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        button = view.findViewById(R.id.button);
        cardNumber = view.findViewById(R.id.cardNumber);
        cardHolder_id = view.findViewById(R.id.cardHolder_id);
        editText_CVV = view.findViewById(R.id.editText_CVV);
        spinner_month = view.findViewById(R.id.spinner_month);
        spinner_year = view.findViewById(R.id.spinner_year);

        spinner_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                month = adapterView.getItemAtPosition(i).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                year = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DigitalGlobal().hideKeyboard(getActivity());

                if (cardHolder_id.getText().toString() == null || cardHolder_id.getText().toString().equalsIgnoreCase("")) {
                    cardHolder_id.setError("Enter Card Holder name");
                } else if (cardNumber.getText().toString() == null || cardNumber.getText().toString().equalsIgnoreCase("")) {
                    cardNumber.setError("Enter Card number");
                } else if (cardNumber.getText().toString().length() < 16) {
                    cardNumber.setError("Enter Correct Card number");
                } else if (month == null || month.equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Select month", Toast.LENGTH_SHORT).show();
                } else if (year == null || year.equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Select year", Toast.LENGTH_SHORT).show();
                } else if (editText_CVV.getText().toString() == null || editText_CVV.getText().toString().equalsIgnoreCase("")) {
                    editText_CVV.setError("Enter CVV");
                } else if (editText_CVV.getText().length() < 3) {
                    editText_CVV.setError("Enter Complete CVV");
                } else {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Please Wait..");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    int monthInt = Integer.parseInt(month);
                    int yearInt = Integer.parseInt(year);
                    Log.e("getResult", cardNumber.getText().toString().trim());
                    Log.e("getResult", monthInt + "");
                    Log.e("getResult", yearInt + "");
                    Log.e("getResult", editText_CVV.getText().toString().trim());

                    saveCreditCardLat(cardNumber.getText().toString().trim(), monthInt, yearInt, editText_CVV.getText().toString().trim(), amountPoints);

                }

            }
        });

        return view;

    }

    private void saveCreditCardLat(String cardNumber, int monthInt, int yearInt, String cvv, String amount) {

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.stripePayment(Global.acceptHeader, Global.token, cardNumber, monthInt, yearInt, cvv, amount);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                progressDialog.dismiss();

                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                String res = response.isSuccessful() + "";
                if (res.equalsIgnoreCase("true")) ;
                {

                    try {

                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        String status = jsonObject.get("status").toString();

                        if (status.equalsIgnoreCase("success")) {

                            Toast.makeText(getActivity(), jsonObject.get("message") + "", Toast.LENGTH_SHORT).show();
                            String totalPoints = String.valueOf(Integer.valueOf(preferences.getString("point", "")) + Integer.valueOf(points));
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("point", totalPoints);
                            editor.commit();

                            getActivity().onBackPressed();

                        } else {
                            Toast.makeText(getActivity(), jsonObject.get("message") + "", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void saveCreditCard(String acc, int month, int year, String cvc, final ProgressDialog progressDialog) {
        CardNumber = acc;
        ExpMonth = month;
        ExpYear = year;
        Cvc = cvc;
        //  Currency = "inr";
        //  Currency = "cad";
        Card card = new Card(
                CardNumber,
                ExpMonth,
                ExpYear,
                Cvc);
        //  card.setCurrency(Currency);

        boolean validation = card.validateCard();
        if (validation) {

            Stripe stripe = new Stripe(getActivity(), PUBLISHABLE_KEY);
            stripe.createToken(
                    card,
                    PUBLISHABLE_KEY,
                    new TokenCallback() {
                        public void onSuccess(Token token) {
                            Log.e("tokengetHere", "t  " + token.getId() + "");
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Successful", Toast.LENGTH_SHORT).show();
                            //  payment(token);
                            String tokenVAl = token.getId() + "";
                            //   StripePayment_Api stripePayment_api = new StripePayment_Api();
                            //    stripePayment_api.payment(getActivity(),"59e098cca75696113e10fc71","5adeeb4aa75696056b5b5c82","25","cad",tokenVAl,progressDialog);
                        }

                        public void onError(Exception error) {
                            Log.e("arfvsdf111", error + "  refgr");
                            Toast.makeText(getActivity(), error.getLocalizedMessage() + "", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    });
        } else if (!card.validateNumber()) {
            progressDialog.dismiss();
            Toast.makeText(getActivity(), "The card number that you entered is invalid", Toast.LENGTH_SHORT).show();
        } else if (!card.validateExpiryDate()) {
            progressDialog.dismiss();
            Toast.makeText(getActivity(), "The expiration date that you entered is invalid", Toast.LENGTH_SHORT).show();
        } else if (!card.validateCVC()) {
            progressDialog.dismiss();
            Toast.makeText(getActivity(), "The CVC code that you entered is invalid", Toast.LENGTH_SHORT).show();
        } else {
            progressDialog.dismiss();
            Toast.makeText(getActivity(), "The card details that you entered are invalid", Toast.LENGTH_SHORT).show();
        }

    }

}

