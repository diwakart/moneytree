package reloadedhd.com.digitalbank.Fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Activity.MainActivity2;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.global.DigitalGlobal;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfile extends Fragment implements View.OnClickListener {

    private RelativeLayout relLayout_back_profile;
    private ImageView profile_pic_profile, img_userProfile_background, editProfileEdit;
    private static final String IMAGE_DIRECTORY_NAME = "DigiBank";
    byte[] byteArray;
    Uri fileUri;
    private TextView editProfileUserName;
    private EditText edt_username_userProfile, edt_email_userProfile, edt_contact_userProfile, edt_schoolName_userProfile,
            edt_dob_userProfile, edt_address_userProfile;
    private SharedPreferences preferences;
    private Button buttonSubmitUserProfile;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private RadioButton radioButtonMale, radioButtonFeMale;
    private RadioGroup radioGroupEdit;

    public UserProfile() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);

        //------------to avoid file uri exposure exception------------------
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        preferences = DigitalBankContoller.getGlobalPreferences();

        editProfileEdit = view.findViewById(R.id.editProfileEdit);
        relLayout_back_profile = view.findViewById(R.id.relLayout_back_profile);
        profile_pic_profile = view.findViewById(R.id.profile_pic_profile);
        img_userProfile_background = view.findViewById(R.id.img_userProfile_background);

        edt_username_userProfile = view.findViewById(R.id.edt_username_userProfile);
        edt_email_userProfile = view.findViewById(R.id.edt_email_userProfile);
        edt_contact_userProfile = view.findViewById(R.id.edt_contact_userProfile);
        edt_schoolName_userProfile = view.findViewById(R.id.edt_schoolName_userProfile);
        edt_dob_userProfile = view.findViewById(R.id.edt_dob_userProfile);
        edt_address_userProfile = view.findViewById(R.id.edt_address_userProfile);
        buttonSubmitUserProfile = view.findViewById(R.id.buttonSubmitUserProfile);
        buttonSubmitUserProfile.setVisibility(View.GONE);
        editProfileUserName = view.findViewById(R.id.editProfileUserName);

        radioGroupEdit = view.findViewById(R.id.radioGroupEdit);

        radioButtonMale = view.findViewById(R.id.radioButtonMale);
        radioButtonFeMale = view.findViewById(R.id.radioButtonFeMale);

        if (preferences.contains("name")) {

            editProfileUserName.setText(preferences.getString("name", ""));
            edt_username_userProfile.setText(preferences.getString("name", ""));
        }

        if (preferences.contains("bitmap") && preferences.getString("bitmap", "") != null) {

            profile_pic_profile.setImageBitmap(new DigitalGlobal().StringToBitMap(preferences.getString("bitmap", "")));

        } else {

            profile_pic_profile.setImageDrawable(getResources().getDrawable(R.drawable.profile_holder));

        }

        relLayout_back_profile.setOnClickListener(this);
        profile_pic_profile.setOnClickListener(this);
        buttonSubmitUserProfile.setOnClickListener(this);
        edt_dob_userProfile.setOnClickListener(this);
        editProfileEdit.setOnClickListener(this);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        final Calendar newCalendar = Calendar.getInstance();

        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Log.e("dateee", year + "/" + monthOfYear + "/" + dayOfMonth);
                Log.e("dateee12", Calendar.YEAR + "//" + newCalendar.get(Calendar.YEAR));

                if (year >= newCalendar.get(Calendar.YEAR)) {
                    Toast.makeText(getActivity(), "Select a valid date of birth", Toast.LENGTH_SHORT).show();
                    edt_dob_userProfile.setText("");
                    edt_dob_userProfile.setHint("D.O.B");
                    return;
                }

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                edt_dob_userProfile.setText(dateFormatter.format(newDate.getTime()));
                //datePickerDialog.dismiss();
            }

        }, newCalendar.get(Calendar.YEAR) - 2, newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        setEditable();
        getUserData();
        return view;
    }

    void setEditable() {

        edt_username_userProfile.setEnabled(false);
        edt_email_userProfile.setEnabled(false);
        edt_contact_userProfile.setEnabled(false);
        edt_schoolName_userProfile.setEnabled(false);
        // edt_motherName_userProfile.setEnabled(false);
        // edt_fatherName_userProfile.setEnabled(false);
        edt_dob_userProfile.setEnabled(false);

        for (int i = 0; i < radioGroupEdit.getChildCount(); i++) {
            radioGroupEdit.getChildAt(i).setEnabled(false);
        }

    }

    private void getUserData() {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user/details.json");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();

                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                    String res = response.body().toString();
                    Log.e("profileres", res);

                    try {

                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                            JSONArray array = jsonObject.optJSONArray("data");

                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object = array.optJSONObject(i);
                                editProfileUserName.setText(object.optString("name"));
                                edt_username_userProfile.setText(object.optString("name"));

                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("avtar", object.optString("avtar"));
                                editor.commit();

                                ImageLoader imageLoader = ImageLoader.getInstance();
                                imageLoader.displayImage(ServiceURLs.USERPROFILEIMAGEBASEURL + object.optString("avtar"), profile_pic_profile, new ImageLoadingListener() {
                                    @Override
                                    public void onLoadingStarted(String imageUri, View view) {

                                        Log.e("imgstrt", "ok/" + imageUri);

                                    }

                                    @Override
                                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                                        Log.e("imgfail", "fail" + failReason.toString());

                                    }

                                    @Override
                                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                                        profile_pic_profile.setImageBitmap(loadedImage);
                                        MainActivity2.profile_pic_main2.setImageBitmap(loadedImage);

                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putString("bitmap", new DigitalGlobal().BitMapToString(loadedImage));
                                        editor.commit();

                                    }

                                    @Override
                                    public void onLoadingCancelled(String imageUri, View view) {

                                        Log.e("imgcanc", imageUri + "");

                                    }

                                });
                                if (!object.optString("mobile").trim().equalsIgnoreCase("null") && !object.optString("mobile").trim().equalsIgnoreCase("")) {
                                    edt_contact_userProfile.setText(object.optString("mobile"));
                                }

                                if (!object.optString("email").equalsIgnoreCase("null")) {

                                    edt_email_userProfile.setText(object.optString("email"));

                                } else {

                                    edt_email_userProfile.setText("N/A");

                                }

                                if (!object.optString("school_name").equalsIgnoreCase("null")) {

                                    edt_schoolName_userProfile.setText(object.optString("school_name"));

                                } else {

                                    edt_schoolName_userProfile.setText("N/A");

                                }

                                if (!object.optString("dob").equalsIgnoreCase("null")) {
                                    edt_dob_userProfile.setText(object.optString("dob"));

                                }

                                if (!object.optString("address").equalsIgnoreCase("null")) {

                                    edt_address_userProfile.setText(object.optString("address"));

                                } else {

                                    edt_address_userProfile.setText("N/A");

                                }

                                if (!object.optString("gender").trim().equalsIgnoreCase("null") && !object.optString("gender").trim().equalsIgnoreCase("")) {

                                    if (object.optString("gender").equalsIgnoreCase("1")) {

                                        radioButtonMale.setChecked(true);
                                        radioButtonFeMale.setChecked(false);

                                    } else if (object.optString("gender").equalsIgnoreCase("2")) {

                                        radioButtonFeMale.setChecked(true);
                                        radioButtonMale.setChecked(false);

                                    }

                                }

                            }

                        } else {

                            new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    public void saveUserData() {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("name", RequestBody.create(MediaType.parse("multipart/form-data"), edt_username_userProfile.getText().toString().trim()));

        body.put("mobile", RequestBody.create(MediaType.parse("multipart/form-data"), edt_contact_userProfile.getText().toString().trim()));

        if (radioButtonFeMale.isChecked()) {
            body.put("gender", RequestBody.create(MediaType.parse("multipart/form-data"), "2"));
        } else if (radioButtonMale.isChecked()) {
            body.put("gender", RequestBody.create(MediaType.parse("multipart/form-data"), "1"));
        }

        if (edt_schoolName_userProfile.getText().toString().trim().length() > 0 && !edt_schoolName_userProfile.getText().toString().trim().equalsIgnoreCase("N/A"))
            body.put("school_name", RequestBody.create(MediaType.parse("multipart/form-data"), edt_schoolName_userProfile.getText().toString().trim()));

        if (edt_email_userProfile.getText().toString().trim().length() > 0 && !edt_email_userProfile.getText().toString().trim().equalsIgnoreCase("N/A"))
            body.put("email", RequestBody.create(MediaType.parse("multipart/form-data"), edt_email_userProfile.getText().toString().trim()));

        if (edt_dob_userProfile.getText().toString().trim().length() > 0 && !edt_dob_userProfile.getText().toString().trim().equalsIgnoreCase("N/A"))
            body.put("dob", RequestBody.create(MediaType.parse("multipart/form-data"), edt_dob_userProfile.getText().toString().trim()));
        if (edt_dob_userProfile.getText().toString().trim().length() > 0 && !edt_address_userProfile.getText().toString().trim().equalsIgnoreCase("N/A"))
            body.put("address", RequestBody.create(MediaType.parse("multipart/form-data"), edt_address_userProfile.getText().toString().trim()));

        Log.e("bddd", body.toString());

        //picture optional key
        //will modify according to the requirement
        Call<JsonObject> call = service.saveUserProfileData(Global.acceptHeader, Global.token, body);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {


                    String res = response.body().toString();
                    Log.e("updateprofile", res);
                    try {
                        JSONObject object = new JSONObject(res);
                        getUserData();
                        new CustomAlert().showAlert(getActivity(), 1, object.optString("message"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.cancel();

                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    //************************************************Camera Permission**********************************************
    @TargetApi(Build.VERSION_CODES.M)
    private void insertDummyContactWrapper() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 11);
        } else
            selectImage();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // TODO Auto-generated method stub
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 11:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();

                } else {
                    // Permission Denied
                    Toast.makeText(getActivity(), "CAMERA Denied", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    //**********************image picker gallery or camera********************
    private void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {

                    openCamera();

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, 1);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    public String getReadableFileSize(long size) {
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;

        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == 1) {

                try {
                    profile_pic_profile.setImageURI(fileUri);
                    //img_userProfile_background.setImageURI(fileUri);

                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), fileUri);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                    byteArray = byteArrayOutputStream.toByteArray();
                    File file = new File(fileUri.getPath());

                    updateProfilePicture(byteArray);

                } catch (IOException e) {

                    e.printStackTrace();

                } catch (NullPointerException e) {

                } catch (Exception e) {

                }

            } else if (requestCode == 2) {

                try {

                    Uri resultUri = data.getData();
                    profile_pic_profile.setImageURI(resultUri);
                    // img_userProfile_background.setImageURI(resultUri);

                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), resultUri);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                    byteArray = byteArrayOutputStream.toByteArray();

                    updateProfilePicture(byteArray);
                } catch (IOException e) {

                    e.printStackTrace();

                } catch (NullPointerException e) {


                }

            }

        }

    }

    private void updateProfilePicture(byte[] imageArray) {

        RequestBody imagefile1;
        MultipartBody.Part image1 = null;
        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);

        try {

            Log.e("byte_array1", "" + imageArray);
            //imagefile1 = RequestBody.create(MediaType.parse("image/*"), byteArray);
            //imagefile1 = RequestBody.create(MediaType.parse("file/*"), byteArray);
            imagefile1 = RequestBody.create(MediaType.parse("file/*"), imageArray);
            image1 = MultipartBody.Part.createFormData("picture", "a.jpeg", imagefile1);
            Log.e("imagefile1", "im1 " + imagefile1.toString());
            Log.e("imagefile1", image1.toString());

        } catch (Exception e) {

        }

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.uploadPic(Global.acceptHeader, Global.token, image1);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.dismiss();
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                    String res = response.body().toString();
                    Log.e("imggg", res);
                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.optString("status").toString();
                        if (status.equalsIgnoreCase("success")) {

                            String message = jsonObject.optString("message").toString();
                            new CustomAlert().showAlert(getActivity(), 1, message);
                            JSONObject object = jsonObject.optJSONObject("data");

                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("avtar", object.optString("avtar"));
                            editor.commit();

                            ImageLoader imageLoader = ImageLoader.getInstance();
                            imageLoader.displayImage(ServiceURLs.USERPROFILEIMAGEBASEURL + object.optString("avtar"), profile_pic_profile, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {

                                    Log.e("imgstrt", "ok/" + imageUri);

                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                                    Log.e("imgfail", "fail" + failReason.toString());

                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                                    profile_pic_profile.setImageBitmap(loadedImage);
                                    MainActivity2.profile_pic_main2.setImageBitmap(loadedImage);

                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("bitmap", new DigitalGlobal().BitMapToString(loadedImage));
                                    editor.commit();

                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {

                                    Log.e("imgcanc", imageUri + "");

                                }

                            });

                        } else {

                            String message = jsonObject.optString("message").toString();
                            new CustomAlert().showAlert(getActivity(), 0, message + "");
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.dismiss();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.relLayout_back_profile:

                MainActivity2.rel_header.setVisibility(View.VISIBLE);
                Global.changeFragment(getActivity(), new Dashboard());

                break;

            case R.id.buttonSubmitUserProfile:

                if (edt_username_userProfile.length() < 4) {

                    edt_username_userProfile.setError("User Name must be atleast 4 characters");

                } /*else if (edt_contact_userProfile.length() < 6 || edt_contact_userProfile.length() > 14) {
                    edt_contact_userProfile.setError("Enter valid Phone number");
                }*/ else {

                    saveUserData();

                }

                break;

            case R.id.profile_pic_profile:

                insertDummyContactWrapper();

                break;

            case R.id.edt_dob_userProfile:

                datePickerDialog.show();

                break;

            case R.id.editProfileEdit:
                editProfileEdit.setVisibility(View.GONE);
                buttonSubmitUserProfile.setVisibility(View.VISIBLE);
                enableEditable();

                break;

        }

    }

    private void enableEditable() {
        edt_username_userProfile.setEnabled(true);
        edt_email_userProfile.setEnabled(true);
        edt_contact_userProfile.setEnabled(true);
        edt_schoolName_userProfile.setEnabled(true);
        edt_dob_userProfile.setEnabled(true);
        for (int i = 0; i < radioGroupEdit.getChildCount(); i++) {
            radioGroupEdit.getChildAt(i).setEnabled(true);
        }
        edt_address_userProfile.setEnabled(true);
        profile_pic_profile.setEnabled(true);
    }

}
