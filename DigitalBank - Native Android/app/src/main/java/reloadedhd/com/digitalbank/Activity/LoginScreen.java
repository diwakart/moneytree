package reloadedhd.com.digitalbank.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.hbb20.CountryCodePicker;
import com.twilio.Twilio;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Pattern;

import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.global.DigitalGlobal;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginScreen extends Activity implements View.OnClickListener {
    TextView signup_txt_login, txtView_forgotPassword, passwordShow;
    EditText edt_email_login, edt_password_login;
    boolean check_email = false;
    boolean check_mobile = false;
    private Button loginButton;
    public static String countryCode, mobileNumber;
    int otp;
    private CountryCodePicker countryCodePicker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        Twilio.init(getResources().getString(R.string.ACCOUNT_SID), getResources().getString(R.string.AUTH_TOKEN));

        signup_txt_login = findViewById(R.id.signup_txt_login);
        loginButton = findViewById(R.id.loginButton);
        txtView_forgotPassword = findViewById(R.id.txtView_forgotPassword);
        edt_email_login = findViewById(R.id.edt_email_login);
        edt_password_login = findViewById(R.id.edt_password_login);
        countryCodePicker = findViewById(R.id.countryCodePicker);
        passwordShow = findViewById(R.id.passwordShow);

        countryCode = getResources().getConfiguration().locale.getCountry();
        Log.e("ctry", getResources().getConfiguration().locale.getISO3Country());
        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //CountryID= manager.getSimCountryIso().toUpperCase();
        Log.e("idd", manager.getSimCountryIso().toUpperCase());

        countryCodePicker.setCountryForNameCode(manager.getSimCountryIso().toUpperCase());
        String localeCountry = manager.getNetworkCountryIso();

        if (localeCountry != null) {

            Locale loc = new Locale("", localeCountry);
            Log.e("okjl", "User is from " + loc.getISO3Country());

        }

        passwordShow.setOnClickListener(this);

        txtView_forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DigitalGlobal().hideKeyboard(LoginScreen.this);

                if (edt_email_login.getText().toString().trim().length() == 0) {

                    new CustomAlert().showAlert(LoginScreen.this, 0, "Enter user name to change password");
                } else if (edt_email_login.getText().toString().trim().length() < 4) {
                    // } else if (!isValidMobile(edt_email_login.getText().toString().trim())) {

                    edt_email_login.setError("Username must be at least 4 characters");

                } else {

                    mobileNumber = edt_email_login.getText().toString().trim();
                    Log.e("ccd", countryCodePicker.getSelectedCountryCode() + "//" + countryCodePicker.getSelectedCountryCodeWithPlus() + "//" + countryCodePicker.getSelectedCountryName() + "/" + countryCodePicker.getSelectedCountryNameCode());
                    Log.e("mobbb", mobileNumber);
                    checkUserExistance(mobileNumber);

                }

            }
        });

        signup_txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DigitalGlobal().hideKeyboard(LoginScreen.this);
                startActivity(new Intent(LoginScreen.this, RegisterScreen.class));
                finish();

            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DigitalGlobal().hideKeyboard(LoginScreen.this);
                //isValidMail(edt_email_login.getText().toString().trim());
                isValidMobile(edt_email_login.getText().toString().trim());
                Log.e("fbvfg", check_mobile + "   yes");

                if (edt_email_login.getText().toString().trim().length() == 0) {
                    edt_email_login.setError("Invalid User name");
                } else if (edt_email_login.getText().toString().trim().length() < 4) {
                    edt_email_login.setError("Username must be at least 4 characters");
                } else if (edt_password_login.length() < 4) {
                    edt_password_login.setError("Password should be atleast 4 characters");
                } else {
                    String email_id = edt_email_login.getText().toString().trim();
                    String password = edt_password_login.getText().toString().trim();

                    login(email_id, password);

                }
            }
        });

    }

    private void login(String userMobile, String pass) {
        final ProgressDialog pd = ProgressDialog.show(this, "", "Please wait...", true);

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "login" + "?user_name=" + userMobile + "&password=" + pass);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                try {

                    pd.cancel();
                    if (response.isSuccessful()) {
                        String responsLogin = response.body().toString();
                        Log.e("reslogin", responsLogin);

                        JSONObject jsonObject = new JSONObject(responsLogin);

                        if (jsonObject.has("success")) {

                            JSONObject jsonObject1 = jsonObject.getJSONObject("success");

                            String token = jsonObject1.get("token").toString();
                            Log.e("tokenGet", token);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                String name = jsonObject2.get("name").toString();
                                String mobile = jsonObject2.get("mobile").toString();
                                String type = jsonObject2.get("type").toString();

                                SharedPreferences sharedPreferences = LoginScreen.this.getSharedPreferences("LoginPref", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                editor.putString("token", "Bearer " + token);
                                editor.putString("name", name);

                                if (jsonObject2.has("email") && jsonObject2.optString("email") != null) {

                                    editor.putString("email", jsonObject2.optString("email"));

                                }

                                editor.putString("mobile", mobile);
                                editor.putString("loginStatus", type);
                                editor.putString("avtar", jsonObject2.optString("avtar"));
                                editor.putString("totalpoint", jsonObject2.optString("current_point"));
                                editor.putString("user_id", jsonObject2.optString("user_id"));
                                editor.putString("digital_key", jsonObject2.optString("digital_key"));
                                editor.putString("add", "0");
                                editor.putString("gender", jsonObject2.optString("gender"));

                                if (jsonObject2.has("buffer_point")) {

                                    String str = String.valueOf(Integer.valueOf(jsonObject2.optString("current_point")) - Integer.valueOf(jsonObject2.optString("buffer_point")));

                                    editor.putString("buffer_point", jsonObject2.optString("buffer_point"));
                                    editor.putString("point", jsonObject2.optString("current_point"));

                                }

                                if (jsonObject2.has("total_wishlist_products")) {

                                    editor.putString("wishlist", jsonObject2.optString("total_wishlist_products"));

                                }

                                if (jsonObject2.has("total_wishlist_points")) {

                                    editor.putString("wishlistpoints", jsonObject2.optString("total_wishlist_points"));

                                }

                                editor.commit();

                                Global.token = "Bearer " + token;

                            }

                            Intent intent = new Intent(LoginScreen.this, MainActivity2.class);
                            LoginScreen.this.startActivity(intent);
                            ((Activity) LoginScreen.this).finish();

                        } else if (jsonObject.has("error")) {

                            new CustomAlert().showAlert(LoginScreen.this, 0, jsonObject.optString("message"));

                        }

                    } else {

                        Log.e("ressssssds", response.errorBody().toString());
                        new CustomAlert().showAlert(LoginScreen.this, 0, "Please check your credentials");

                    }

                } catch (Exception e) {

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                Log.e("falilog", call.toString() + "");
                pd.cancel();
                new CustomAlert().showAlert(LoginScreen.this, 0, "Failure");

            }

        });

    }

    private void checkUserExistance(String userMobile) {

        final ProgressDialog pd = ProgressDialog.show(this, "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponseWithoutHeader("check/userexist.json?user_name=" + userMobile);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String res = response.body().toString().trim();
                Log.e("forgotres", res);

                try {

                    JSONObject object = new JSONObject(res);

                    if (object.optString("status").equalsIgnoreCase("success")) {
                        mobileNumber = "";
                        //  mobileNumber = object.optJSONObject("data").optString("mobile");
                        mobileNumber = object.optJSONObject("data").optString("country_code") + object.optJSONObject("data").optString("mobile");
                        Random rnd = new Random();
                        otp = 100000 + rnd.nextInt(900000);
                        Log.e("phnnnn", mobileNumber);
                        Log.e("otp", otp + "");
                        Toast.makeText(LoginScreen.this, "We are sending you an otp to your registered mobile number", Toast.LENGTH_SHORT).show();

                        new SendSMS().execute();
                        showAlert(String.valueOf(otp));

                    } else {

                        //Toast.makeText(LoginScreen.this, object.optString("message"), Toast.LENGTH_SHORT).show();
                        new CustomAlert().showAlert(LoginScreen.this, 0, object.optString("message"));
                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

                pd.cancel();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                Toast.makeText(LoginScreen.this, "Failure", Toast.LENGTH_SHORT).show();

            }

        });

    }

    private void showAlert(final String dialogOtp) {
        TextView otpText;
        final EditText otpEdit;
        Button otpButton, cancelOtpButton;
        final Dialog dialog = new Dialog(LoginScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.otp_verification);
        dialog.show();
        otpText = dialog.findViewById(R.id.otpText);
        otpEdit = dialog.findViewById(R.id.otpEdit);
        otpButton = dialog.findViewById(R.id.otpButton);
        cancelOtpButton = dialog.findViewById(R.id.cancelOtpButton);
        cancelOtpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        otpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otpEdit.getText().toString().trim().length() == 0) {
                    Toast.makeText(LoginScreen.this, "Enter the otp", Toast.LENGTH_SHORT).show();
                    return;

                } else if (otpEdit.getText().toString().trim().equalsIgnoreCase(dialogOtp)) {
                    dialog.dismiss();
                    setPassword(dialogOtp);
                } else {
                    new CustomAlert().showAlert(LoginScreen.this, 0, "OTP does not match enter the corroct OTP");
                    //Toast.makeText(LoginScreen.this, "OTP does not match enter the corroct OTP", Toast.LENGTH_SHORT).show();
                    return;
                }

            }

        });

    }

    private void setPassword(String pass) {

        final ProgressDialog pd = ProgressDialog.show(LoginScreen.this, "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);

        Call<JsonObject> call = service.forgotPassword(mobileNumber, pass);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();
                String res = response.body().toString().trim();
                Log.e("ress", res);

                try {

                    JSONObject jsonObject = new JSONObject(res);
                    new CustomAlert().showAlert(LoginScreen.this, 1, jsonObject.optString("message"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();

            }

        });

    }

    private boolean isValidMail(String email) {

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (email.matches(EMAIL_STRING)) {

            check_email = true;

        } else {

            check_email = false;

        }

        return check_email;

    }

    //------------------mobile number validation---------------------
    private boolean isValidMobile(String phone) {

        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check_mobile = false;

            } else {
                check_mobile = true;
            }
        } else {
            check_mobile = false;
        }
        return check_mobile;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.passwordShow:
                if (passwordShow.getText().toString().trim().equalsIgnoreCase("HIDE")) {
                    passwordShow.setText("SHOW");
                    //  edt_password_login.setTransformationMethod(new PasswordTransformationMethod());
                    edt_password_login.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else if (passwordShow.getText().toString().trim().equalsIgnoreCase("SHOW")) {
                    passwordShow.setText("HIDE");
                    // edt_password_login.setTransformationMethod(null);
                    edt_password_login.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }

                break;
        }
    }

    /*check_mobile = android.util.Patterns.PHONE.matcher(phone).matches();
      return check_mobile;*///}
    class SendSMS extends AsyncTask<Void, Void, Void> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(LoginScreen.this);
            pd.setCancelable(false);
            pd.setMessage("Please wait...");
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String mobile = /*countryCodePicker.getSelectedCountryCodeWithPlus()+*/ "+" + mobileNumber;
            Log.e("mobb", mobile);
            HttpClient httpclient = new DefaultHttpClient();

            HttpPost httppost = new HttpPost("https://api.twilio.com/2010-04-01/Accounts/" + getResources().getString(R.string.ACCOUNT_SID) + "/SMS/Messages");
            //HttpPost httppost = new HttpPost("https://api.twilio.com/2010-04-01/Accounts/" + ACCOUNT_SID + "/Calls");
            String base64EncodedCredentials = "Basic "
                    + Base64.encodeToString(
                    (getResources().getString(R.string.ACCOUNT_SID) + ":" + getResources().getString(R.string.AUTH_TOKEN)).getBytes(),
                    Base64.NO_WRAP);

            httppost.setHeader("Authorization", base64EncodedCredentials);

            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("From", "+601130116052"));
                nameValuePairs.add(new BasicNameValuePair("To", mobile));//+919452979342
                nameValuePairs.add(new BasicNameValuePair("Body", " DigitalBank forgot password Your OTP is " + otp));

                Log.e("bodyotp", nameValuePairs.toString());

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                //Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                String res = EntityUtils.toString(entity);
                System.out.println("Entity post is: "
                        + res);
                Log.e("sendmsg", res);

            } catch (ClientProtocolException e) {

                Log.e("sendmsgexc", e.toString());

            } catch (IOException e) {

                Log.e("sendmsgexc2", e.toString());

            }

            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (pd != null && pd.isShowing()) {

                pd.cancel();

            }

            Log.e("onPostmethd", "okk");
            Toast.makeText(LoginScreen.this, "An OTP has been sent to your mobile ", Toast.LENGTH_SHORT);

        }

    }

}




