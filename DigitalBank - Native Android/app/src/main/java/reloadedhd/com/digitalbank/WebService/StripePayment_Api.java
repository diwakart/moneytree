package reloadedhd.com.digitalbank.WebService;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import reloadedhd.com.digitalbank.Utility.Global;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 6/6/2018.
 */

public class StripePayment_Api {
    Context context;
    String cardNumber,cvv,amount;
    int yearInt,monthInt;
    ProgressDialog progressDialog;
    public void stripePayment(final Context context, String cardNumber, int monthInt, int yearInt, String cvv, String amount,final ProgressDialog progressDialog) {
        this.context=context;
        this.cardNumber=cardNumber;
        this.monthInt=monthInt;
        this.yearInt=yearInt;
        this.cvv=cvv;
        this.amount=amount;
        this.progressDialog=progressDialog;


        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.stripePayment(Global.acceptHeader,Global.token,cardNumber,monthInt,yearInt,cvv,amount);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();

                String res = response.isSuccessful()+"";
                if (res.equalsIgnoreCase("true"));
                {

                    try {

                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        String status = jsonObject.get("status").toString();
                        if (status.equalsIgnoreCase("success"))
                        {
                            Toast.makeText(context,jsonObject.get("message")+ "", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(context,jsonObject.get("message")+ "", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context,  "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
