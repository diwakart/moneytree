package reloadedhd.com.digitalbank.Fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;

/**
 * A simple {@link Fragment} subclass.
 */
public class Setting extends Fragment implements View.OnClickListener {
    private TextView settingChangePass, settingHistory;
    private RelativeLayout relLayout_back_button_setting;
    private SharedPreferences preferences;
    private LinearLayout settingHistoryLinear;

    public Setting() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        preferences = DigitalBankContoller.getGlobalPreferences();
        initView(view);
        return view;
    }

    private void initView(View v) {

        settingHistoryLinear = v.findViewById(R.id.settingHistoryLinear);
        settingChangePass = v.findViewById(R.id.settingChangePass);
        relLayout_back_button_setting = v.findViewById(R.id.relLayout_back_button_setting);
        settingHistory = v.findViewById(R.id.settingHistory);

        if (preferences.contains("loginStatus") && preferences.getString("loginStatus", "").equalsIgnoreCase("3")) {

            settingHistoryLinear.setVisibility(View.VISIBLE);

        }

        settingChangePass.setOnClickListener(this);
        relLayout_back_button_setting.setOnClickListener(this);
        settingHistory.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.settingChangePass:

                Global.changeFragment(getActivity(), new ChangePassword());

                break;

            case R.id.relLayout_back_button_setting:

                getActivity().onBackPressed();

                break;

            case R.id.settingHistory:

                Global.changeFragment(getActivity(), new ParentPaymentHistory());

                break;

        }

    }

}
