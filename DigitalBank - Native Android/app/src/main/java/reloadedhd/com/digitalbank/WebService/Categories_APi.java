package reloadedhd.com.digitalbank.WebService;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import reloadedhd.com.digitalbank.Fragment.CategoryManagement;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 5/29/2018.
 */

public class Categories_APi  {
    Context context;
    String acceptHeader,token;
    ProgressDialog progressDialog;
    ArrayList<HashMap<String,String>> arrayList = new ArrayList<>();
    public void categories(FragmentActivity activity, String acceptHeader, String token) {
        this.context=activity;
        this.acceptHeader=acceptHeader;
        this.token=token;

        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");


        Log.e("wfgr",token);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.categories(acceptHeader,token);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();

                String res = response.isSuccessful()+"";
                if (res.equalsIgnoreCase("true"))
                {
                    try {

                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    String status = jsonObject.get("status").toString();
                    String message = jsonObject.get("message").toString();
                    if (status.equalsIgnoreCase("success")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("meta");
                        String perpage = jsonObject1.get("perpage").toString();
                        String total = jsonObject1.get("total").toString();
                        String page = jsonObject1.get("page").toString();

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int f = 0; f < jsonArray.length(); f++) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            JSONObject jsonObject2 = jsonArray.getJSONObject(f);

                            String id = jsonObject2.get("id").toString();
                            String category_name = jsonObject2.get("category_name").toString();

                            hashMap.put("category_name", category_name);
                            hashMap.put("id", id);

                            arrayList.add(hashMap);
                        }


                        Global.getArrayList_categoriesList().clear();
                        Global.setArrayList_categoriesList(arrayList);

                        Global.changeFragment(context, new CategoryManagement());


                    }
                    else
                    {
                        Toast.makeText(context, message+ "", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                }
                else
                {
                   Toast.makeText(context, response.message()+ "", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context,  "Failure", Toast.LENGTH_SHORT).show();

            }
        });
    }
}
