package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import github.nisrulz.recyclerviewhelper.RVHAdapter;
import github.nisrulz.recyclerviewhelper.RVHViewHolder;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.model.OptionModel;

public class TaskDetailAdapter extends RecyclerView.Adapter<TaskDetailAdapter.TaskHolder> implements RVHAdapter {
    private Context context;
    private ArrayList<OptionModel> arrayList;
    private boolean onBind;

    public TaskDetailAdapter(Context context, ArrayList<OptionModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public TaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.task_detail_adapter, parent, false);
        return new TaskHolder(v);
    }

    @Override
    public void onBindViewHolder(TaskHolder holder, final int position) {

        if (arrayList.get(position).getQuestionType().equalsIgnoreCase("3")) {
            holder.optionAdapterCheckbox.setVisibility(View.GONE);
        } else {
            holder.optionAdapterCheckbox.setVisibility(View.VISIBLE);
        }

        holder.optionAdapterTextView.setText(arrayList.get(position).getLabel());
        holder.optionAdapterCheckbox.setChecked(arrayList.get(position).isSelected());

        holder.optionAdapterCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                onBind = true;

                if (isChecked) {
                    arrayList.get(position).setSelected(true);
                } else {
                    arrayList.get(position).setSelected(false);
                }

                onBind = false;
                if (!onBind) {

                    notifyDataSetChanged();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();

    }

    @Override
    public void onItemDismiss(int position, int direction) {
        remove(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        swap(fromPosition, toPosition);
        return false;
    }

    private void remove(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
    }

    private void swap(int firstPosition, int secondPosition) {
        Collections.swap(arrayList, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    class TaskHolder extends RecyclerView.ViewHolder implements RVHViewHolder {
        CheckBox optionAdapterCheckbox;
        TextView optionAdapterTextView;

        public TaskHolder(View itemView) {
            super(itemView);
            optionAdapterCheckbox = itemView.findViewById(R.id.optionAdapterCheckbox);
            optionAdapterTextView = itemView.findViewById(R.id.optionAdapterTextView);
        }

        @Override
        public void onItemClear() {

        }

        @Override
        public void onItemSelected(int actionstate) {
        }

    }

}



