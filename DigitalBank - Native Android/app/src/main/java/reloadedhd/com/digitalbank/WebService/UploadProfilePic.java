package reloadedhd.com.digitalbank.WebService;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.nio.file.Path;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 5/29/2018.
 */

public class UploadProfilePic {
    Context context;
    String token, acceptHeader;
    byte[] byteArray;

    ProgressDialog progressDialog;
    RequestBody imagefile1, pic;
    //  Uri resss;

    public void uploadPic(FragmentActivity activity, byte[] byteArray, String token, String acceptHeader) {
        this.context = activity;
        this.byteArray = byteArray;
        this.token = token;
        this.acceptHeader = acceptHeader;

        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");

        MultipartBody.Part image1 = null;

        try {

            Log.e("byte_array1", "" + byteArray);
           // imagefile1 = RequestBody.create(MediaType.parse("image/*"), byteArray);
            imagefile1 = RequestBody.create(MediaType.parse("file/*"), byteArray);
            image1 = MultipartBody.Part.createFormData("picture", "a.jpeg", imagefile1);
            Log.e("imagefile1", "im1 " + imagefile1.toString());
        } catch (Exception e) {

        }

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.uploadPic(acceptHeader, token, image1);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();

                String res = response.body().toString();
                Log.e("imggg", res);
                try {

                    JSONObject jsonObject = new JSONObject(res);
                    String status = jsonObject.optString("status").toString();
                    if (status.equalsIgnoreCase("success")) {
                        String message = jsonObject.optString("message").toString();
                        Toast.makeText(context, message + "", Toast.LENGTH_SHORT).show();
                    } else {
                        String message = jsonObject.optString("message").toString();
                        Toast.makeText(context, message + "", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();

            }
        });


    }
}
