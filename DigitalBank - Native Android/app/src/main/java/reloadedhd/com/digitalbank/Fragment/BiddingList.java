package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.BiddingListAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.BiddingModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BiddingList extends Fragment implements View.OnClickListener {
    private Bundle bundle;
    private String auctionId;
    private ArrayList<BiddingModel> arrayList;
    private RecyclerView biddingList;
    private BiddingListAdapter adapter;

    private RelativeLayout toolbarRelativeBack;
    private TextView toolbarTitle;

    private ShimmerFrameLayout biddingShimmer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bidding_list, container, false);
        arrayList = new ArrayList<>();
        toolbarTitle = v.findViewById(R.id.toolbarTitle);
        toolbarRelativeBack = v.findViewById(R.id.toolbarRelativeBack);
        biddingList = v.findViewById(R.id.biddingList);
        biddingShimmer = v.findViewById(R.id.biddingShimmer);

        toolbarTitle.setText("Bidding List");
        biddingList.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new BiddingListAdapter(getActivity(), arrayList);
        biddingList.setAdapter(adapter);
        bundle = getArguments();
        if (bundle != null) {
            auctionId = bundle.getString("auctionId");
            getBiddingList();
        }
        toolbarRelativeBack.setOnClickListener(this);
        return v;
    }

    ///v1/user/{id}/bid_details.json
    private void getBiddingList() {
        biddingShimmer.setVisibility(View.VISIBLE);
        biddingShimmer.startShimmer();
        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);

        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user/" + auctionId + "/bid_details.json");
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                biddingShimmer.stopShimmer();
                biddingShimmer.setVisibility(View.GONE);
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {
                //  pd.cancel();

                String res = response.body().toString();
                try {
                    JSONObject jsonObject = new JSONObject(res);
                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                        JSONArray array = jsonObject.optJSONArray("bid_data");
                        for (int a = 0; a < array.length(); a++) {
                            JSONObject object = array.optJSONObject(a);
                            BiddingModel model = new BiddingModel();
                            model.setUser_id(object.optString("user_id"));
                            model.setAuction_id(object.optString("auction_id"));
                            model.setAuction_name(object.optString("auction_name"));
                            model.setBid_time(object.optString("bid_time"));
                            model.setBid_value(object.optString("bid_value"));
                            arrayList.add(model);
                        }

                        if (!arrayList.isEmpty()) {
                            adapter.notifyDataSetChanged();
                        }
                    }
                    else {
                        new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbarRelativeBack:
                getActivity().onBackPressed();
                break;
        }
    }

}
