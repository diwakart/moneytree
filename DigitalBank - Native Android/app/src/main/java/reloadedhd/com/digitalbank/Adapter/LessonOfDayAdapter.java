package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import java.util.ArrayList;
import java.util.Collections;
import github.nisrulz.recyclerviewhelper.RVHAdapter;
import github.nisrulz.recyclerviewhelper.RVHViewHolder;
import reloadedhd.com.digitalbank.R;

public class LessonOfDayAdapter extends RecyclerView.Adapter<LessonOfDayAdapter.LessonHolder> implements RVHAdapter {
    private Context context;
    private ArrayList<String> arrayList;

    public LessonOfDayAdapter(Context context, ArrayList<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public LessonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.lesson_adapter, parent, false);
        return new LessonHolder(v);
    }

    @Override
    public void onBindViewHolder(LessonHolder holder, int position) {
        holder.lessonAdapterCheck.setText("Option " + position + 1);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public void onItemDismiss(int position, int direction) {
        remove(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        swap(fromPosition, toPosition);
        return false;
    }

    private void remove(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
    }

    private void swap(int firstPosition, int secondPosition) {
        Collections.swap(arrayList, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    class LessonHolder extends RecyclerView.ViewHolder implements RVHViewHolder {
        CheckBox lessonAdapterCheck;

        public LessonHolder(View itemView) {
            super(itemView);
            lessonAdapterCheck = itemView.findViewById(R.id.lessonAdapterCheck);
        }

        @Override
        public void onItemClear() {

        }

        @Override
        public void onItemSelected(int actionstate) {

        }

    }

}
