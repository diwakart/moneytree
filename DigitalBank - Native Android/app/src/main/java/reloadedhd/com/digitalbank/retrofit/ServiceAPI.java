package reloadedhd.com.digitalbank.retrofit;

import java.util.HashMap;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

public interface ServiceAPI {

    @Multipart
    @POST
    Call<ResponseBody> callPostMethod(@Header("Authorization") String auth1, @Url String url, @PartMap RequestBody body);

   /* @GET()
    Call<ResponseBody> callGetMethod(@Header("Accept") String auth, @Header("Authorization") String auth1, @Url String url);
  */


    @GET()
    Call<ResponseBody> callGetMethod(/*@HeaderMap HashMap<String, String> map,*/ @Url String url);

}
