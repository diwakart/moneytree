package reloadedhd.com.digitalbank.fcm;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import reloadedhd.com.digitalbank.application.DigitalBankContoller;

public class FirebaseService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String token = FirebaseInstanceId.getInstance().getToken();
        Log.e("token12", token + "//");

        SharedPreferences preferences = DigitalBankContoller.getTokenPreferences();

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("fcmtoken", token);
        editor.putString("add", "0");
        editor.commit();

      /*SharedPreferences.Editor editor = preferences.edit();
        editor.putString("fcmtoken", token.trim());
        editor.commit();*/

    }

}
