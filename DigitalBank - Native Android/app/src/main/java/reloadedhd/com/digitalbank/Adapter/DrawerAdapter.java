package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Activity.MainActivity2;
import reloadedhd.com.digitalbank.R;

/**
 * Created by Admin on 5/31/2018.
 */

public class DrawerAdapter extends BaseAdapter {
    Context context;
    ArrayList mStringOfDrawer;
    int[] drawerIcon;
    LayoutInflater inflater;
    boolean[] count;
    public static RelativeLayout relLay_drawerList;
    ArrayList<String> hashMapArrayList = new ArrayList<String>();

    public DrawerAdapter(MainActivity2 mainActivity2, ArrayList mStringOfDrawer, int[] drawerIcon) {
        this.context = mainActivity2;
        this.mStringOfDrawer = mStringOfDrawer;
        this.drawerIcon = drawerIcon;
        count = new boolean[mStringOfDrawer.size()];

        for (int i = 0; i < count.length; i++) {
            hashMapArrayList.add("false");
        }

    }

    @Override
    public int getCount() {
        return mStringOfDrawer.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.drawer_adapter, viewGroup, false);

        //View view1 = LayoutInflater.from(context).inflate(R.layout.drawer_adapter,false);

        TextView txt_drawer_item = itemView.findViewById(R.id.txt_drawer_item);
        ImageView drawer_image = itemView.findViewById(R.id.drawer_image);
        relLay_drawerList = itemView.findViewById(R.id.relLay_drawerList);

        txt_drawer_item.setText(mStringOfDrawer.get(i).toString());
        drawer_image.setImageResource(drawerIcon[i]);

        return itemView;

    }

}
