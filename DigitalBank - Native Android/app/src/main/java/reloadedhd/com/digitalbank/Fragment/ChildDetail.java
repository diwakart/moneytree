package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.global.DigitalGlobal;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import reloadedhd.com.digitalbank.WebService.Service;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChildDetail extends Fragment implements View.OnClickListener {

    RelativeLayout relLayout_back_button_child_detail;
    ImageView img_boost_childDetail, childDetailBackgroundImage, circleChildDetailImage;
    private TextView childDetailName, txtView_totalsPints_childDetail, txtView_purchased_childDetail, txtView_earning_childDetail, txtView_spent_childDetail;
    String id;
    Bundle b;
    private SharedPreferences preferences;

    private CardView childDetailCard, childDetailEarnPoint, childDetailSpent;

    public ChildDetail() {

        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.child_detail, container, false);

        preferences = DigitalBankContoller.getGlobalPreferences();

        relLayout_back_button_child_detail = view.findViewById(R.id.relLayout_back_button_child_detail);
        childDetailEarnPoint = view.findViewById(R.id.childDetailEarnPoint);
        childDetailSpent = view.findViewById(R.id.childDetailSpent);
        img_boost_childDetail = view.findViewById(R.id.img_boost_childDetail);

        childDetailName = view.findViewById(R.id.childDetailName);

        txtView_totalsPints_childDetail = view.findViewById(R.id.txtView_totalsPints_childDetail);
        txtView_purchased_childDetail = view.findViewById(R.id.txtView_purchased_childDetail);
        txtView_earning_childDetail = view.findViewById(R.id.txtView_earning_childDetail);
        txtView_spent_childDetail = view.findViewById(R.id.txtView_spent_childDetail);
        childDetailCard = view.findViewById(R.id.childDetailCard);

        circleChildDetailImage = view.findViewById(R.id.circleChildDetailImage);
        childDetailBackgroundImage = view.findViewById(R.id.childDetailBackgroundImage);

        if (preferences.contains("childImage")) {

            SharedPreferences.Editor editor = preferences.edit();
            editor.remove("childImage");
            editor.commit();

        }

        b = getArguments();

        if (b != null) {

            id = b.getString("id");

            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("childId", id);
            editor.commit();

            getChildDetail();

        } else if (id != null) {

            getChildDetail();

        }

        img_boost_childDetail.setOnClickListener(this);
        relLayout_back_button_child_detail.setOnClickListener(this);
        childDetailEarnPoint.setOnClickListener(this);
        childDetailSpent.setOnClickListener(this);
        childDetailCard.setOnClickListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (preferences != null && preferences.contains("childId")) {
            id = preferences.getString("childId", "");
        }
        getChildDetail();

    }

    public void getChildDetail() {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);

        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, ServiceURLs.GETCHILDDETAIL + id + "/details.json");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {
                    String res = response.body().toString();
                    Log.e("childdetail", res);

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        JSONArray array = jsonObject.optJSONArray("data");

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);

                            childDetailName.setText(object.optString("name"));
                            txtView_totalsPints_childDetail.setText(object.optString("total_points"));
                            txtView_purchased_childDetail.setText(object.optString("total_packages"));
                            txtView_earning_childDetail.setText(object.optString("earn_points"));
                            txtView_spent_childDetail.setText(object.optString("spent_points"));

                            if (object.has("avtar") && object.optString("avtar") != null && !object.optString("avtar").equalsIgnoreCase("null")) {

                                ImageLoader imageLoader = ImageLoader.getInstance();
                                imageLoader.displayImage(ServiceURLs.USERPROFILEIMAGEBASEURL + object.optString("avtar", ""), circleChildDetailImage, new ImageLoadingListener() {
                                    @Override
                                    public void onLoadingStarted(String imageUri, View view) {

                                        Log.e("imgstrt", "ok/" + imageUri);

                                    }

                                    @Override
                                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                                        Log.e("imgfail", "fail" + failReason.toString());

                                    }

                                    @Override
                                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                                        circleChildDetailImage.setImageBitmap(loadedImage);
                                        //childDetailBackgroundImage.setImageBitmap(loadedImage);
                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putString("childImage", new DigitalGlobal().BitMapToString(loadedImage));
                                        editor.commit();

                                    }

                                    @Override
                                    public void onLoadingCancelled(String imageUri, View view) {

                                        Log.e("imgcanc", imageUri + "");

                                    }

                                });

                            }

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.cancel();

                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_boost_childDetail:

                CategoryManagement management = new CategoryManagement();
                Bundle b = new Bundle();
                b.putString("id", id);
                b.putString("name", childDetailName.getText().toString());
                management.setArguments(b);
                Global.changeFragment(getActivity(), management);

                break;

            case R.id.relLayout_back_button_child_detail:

                if (preferences.contains("childId")) {

                    SharedPreferences.Editor editor = preferences.edit();
                    editor.remove("childId");
                    editor.remove("childImage");
                    editor.commit();

                }

                if (preferences.contains("childImage")) {

                    SharedPreferences.Editor editor = preferences.edit();
                    editor.remove("childImage");
                    editor.commit();

                }

                getActivity().onBackPressed();

                break;

            case R.id.childDetailEarnPoint:

                EarnedParent earnedParent = new EarnedParent();
                Bundle bundle = new Bundle();
                bundle.putString("id", id);
                earnedParent.setArguments(bundle);

                Global.changeFragment(getActivity(), earnedParent);

                break;

            case R.id.childDetailSpent:

                SpentParent spentParent = new SpentParent();
                Bundle spentBundle = new Bundle();
                spentBundle.putString("id", id);
                spentParent.setArguments(spentBundle);
                Global.changeFragment(getActivity(), spentParent);

                break;

            case R.id.childDetailCard:

                WishlistParent wishlistParent = new WishlistParent();
                Bundle wishBundle = new Bundle();
                wishBundle.putString("id", id);
                wishlistParent.setArguments(wishBundle);
                Global.changeFragment(getActivity(), wishlistParent);

                break;

        }

    }

}

