package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.model.ParentInviteModel;

public class ParentInviteAdapter extends RecyclerView.Adapter<ParentInviteAdapter.ParentHolder> {

    private Context context;
    private ArrayList<ParentInviteModel> arrayList;
    ParentInviteClick click;

    public ParentInviteAdapter(Context context, ArrayList<ParentInviteModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public ParentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.parent_invite_adapter, parent, false);
        return new ParentHolder(v);

    }

    @Override
    public void onBindViewHolder(ParentHolder holder, int position) {

        holder.parentInviteAdapterText.setText("Got invitation from " + arrayList.get(position).getName() + ".\nDo You want to accept it?");
        if (!arrayList.get(position).getRequest_status().equalsIgnoreCase("1")) {

            holder.bottomLinearParent.setVisibility(View.INVISIBLE);
            holder.inviteParentYes.setVisibility(View.INVISIBLE);
            holder.inviteParentCancel.setVisibility(View.INVISIBLE);

        }else {

            holder.bottomLinearParent.setVisibility(View.VISIBLE);
            holder.inviteParentYes.setVisibility(View.VISIBLE);
            holder.inviteParentCancel.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ParentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView inviteParentYes, inviteParentCancel, parentInviteAdapterText;
        LinearLayout bottomLinearParent;

        public ParentHolder(View itemView) {
            super(itemView);
            inviteParentYes = itemView.findViewById(R.id.inviteParentYes);
            inviteParentCancel = itemView.findViewById(R.id.inviteParentCancel);
            parentInviteAdapterText = itemView.findViewById(R.id.parentInviteAdapterText);
            bottomLinearParent = itemView.findViewById(R.id.bottomLinearParent);

            inviteParentYes.setOnClickListener(this);
            inviteParentCancel.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.inviteParentCancel:

                    click.onCancelClick(getAdapterPosition());

                    break;

                case R.id.inviteParentYes:

                    click.onYesClick(getAdapterPosition());

                    break;

            }

        }

    }

    public void onParentInviteAdapterClick(ParentInviteClick click) {
        this.click = click;
    }

    public interface ParentInviteClick {

        public void onYesClick(int pos);

        public void onCancelClick(int pos);

    }

}
