package reloadedhd.com.digitalbank.model;

import java.util.ArrayList;

public class TaskDetailModel {
    private String id;
    private String category_name;
    private String question;
    private String question_type;
    private ArrayList<OptionModel> optionModelArrayList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(String question_type) {
        this.question_type = question_type;
    }

    public ArrayList<OptionModel> getOptionModelArrayList() {
        return optionModelArrayList;
    }

    public void setOptionModelArrayList(ArrayList<OptionModel> optionModelArrayList) {
        this.optionModelArrayList = optionModelArrayList;
    }


}
