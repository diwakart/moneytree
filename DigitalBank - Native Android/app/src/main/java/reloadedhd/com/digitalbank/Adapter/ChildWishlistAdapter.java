package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.model.ChildWishlistModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import reloadedhd.com.digitalbank.transform.CircleTransform;

public class ChildWishlistAdapter extends RecyclerView.Adapter<ChildWishlistAdapter.ChildHolder> {

    private Context context;
    private ArrayList<ChildWishlistModel> arrayList;
    ChildWishClick click;

    public ChildWishlistAdapter(Context context, ArrayList<ChildWishlistModel> arrayList) {

        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public ChildHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.child_wishlist_adapter, parent, false);
        ChildHolder viewHolder = new ChildHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ChildHolder holder, int position) {
        holder.childWishProductName.setText(arrayList.get(position).getProduct_name());
        holder.childWishProductPoint.setText(arrayList.get(position).getProduct_price() + " Points");

        if (arrayList.get(position).getProduct_image() != null && !arrayList.get(position).getProduct_image().isEmpty() && !arrayList.get(position).getProduct_image().equalsIgnoreCase("null")) {
            Picasso.with(context).load(ServiceURLs.PRODUCTIMAGEBASEURL + arrayList.get(position).getProduct_image()).transform(new CircleTransform(20, 0)).fit().into(holder.childWishImage);
        }

      /*int val = Integer.valueOf(arrayList.get(position).getRedeem_price());
        if (val > 0) {
            holder.redeem.setVisibility(View.VISIBLE);
        } else {
            holder.redeem.setVisibility(View.GONE);
        }*/

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    class ChildHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView childWishImage, wishlistAdapterDelete;
        TextView childWishProductName, childWishProductPoint;
        LinearLayout redeem;

        public ChildHolder(View itemView) {
            super(itemView);
            redeem = itemView.findViewById(R.id.childWishlistRedeemProduct);
            childWishProductPoint = itemView.findViewById(R.id.childWishProductPoint);
            childWishProductName = itemView.findViewById(R.id.childWishProductName);
            childWishImage = itemView.findViewById(R.id.childWishImage);
            wishlistAdapterDelete = itemView.findViewById(R.id.wishlistAdapterDelete);
            wishlistAdapterDelete.setOnClickListener(this);

        }

        @Override

        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.wishlistAdapterDelete:

                    click.onDeleteClick(getAdapterPosition());

                    break;

            }

        }

    }

    public void onChildWishClickListener(ChildWishClick click) {
        this.click = click;
    }

    public interface ChildWishClick {

        void onDeleteClick(int pos);

    }

}
