package reloadedhd.com.digitalbank.WebService;

import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Admin on 5/25/2018.
 */

public interface Service {

    /*OkHttpClient okHttpClient = new OkHttpClient().newBuilder().connectTimeout(35, TimeUnit.SECONDS).build();*/
    //OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();

    OkHttpClient okHttpClient = new OkHttpClient.Builder().readTimeout(2, TimeUnit.MINUTES).connectTimeout(2, TimeUnit.MINUTES).build();

    Retrofit retrofit = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://quizme.com.my/digital_bank/api/").client(okHttpClient)
            .build();

    //https://quizme.com.my/digital_bank/api/
    //https://103.233.2.220/digital_bank/api/

    Retrofit retrofit1 = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api.twilio.com/2010-04-01/Accounts/").client(okHttpClient)
            .build();

    @GET("login")
    Call<JsonObject> login(@Query("user_name") String email_id, @Query("password") String password);

    @POST("register")
    Call<JsonObject> register(@Query("user_name") String user_name, @Query("email") String email, @Query("type") String type, @Query("password") String password, @Query("mobile") String mobile);

    //todo by Ashu to register the user without email
    @POST("register")
    Call<JsonObject> registerOne(@Query("user_name") String user_name, /*@Query("email") String email,*/ @Query("type") String type, @Query("password") String password, @Query("mobile") String mobile);

    @POST("user/forgotPassword.json")
    @FormUrlEncoded
    Call<JsonObject> forgotPassword(@Field("mobile") String mobile, @Field("otp") String otp);

    @POST("v1/user/changePassword.json")
    @FormUrlEncoded
    Call<JsonObject> changePassword(@Header("Accept") String accpt, @Header("Authorization") String accpt1, @Field("new_password") String confirmPassword, @Field("re_password") String confirmPassword1);

    @Multipart
    @POST("v1/user/profilePicUpdate.json")
    Call<JsonObject> uploadPic(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Part MultipartBody.Part image1);

    @POST("v1/user/updateProfile.json")
    @FormUrlEncoded
    Call<JsonObject> updateUserProfile(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Field("name") String edt_username_userProfile, @Field("mobile") String edt_contact_userProfile, @Field("email") String email);

    @POST("v1/user/updateProfile.json")
    @FormUrlEncoded
    Call<JsonObject> updateUserProfileOne(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Field("name") String edt_username_userProfile, @Field("mobile") String edt_contact_userProfile);

    @GET("v1/categories.json")
    Call<JsonObject> categories(@Header("Accept") String acceptHeader, @Header("Authorization") String token);

    @GET("v1/user/details.json")
    Call<JsonObject> getProfile(@Header("Accept") String acceptHeader, @Header("Authorization") String token);

    @GET("v1/user/childlist.json")
    Call<JsonObject> getChildListDetail(@Header("Accept") String acceptHeader, @Header("Authorization") String token);

    @GET("v1/user/payment.json")
    Call<JsonObject> stripePayment(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Query("card_no") String cardNumber, @Query("ccExpiryMonth") int monthInt,
                                   @Query("ccExpiryYear") int yearInt, @Query("cvvNumber") String cvv, @Query("package_id") String amount);

    @GET("v1/user/search.json")
    Call<JsonObject> searchChild(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Query("mobile") String mobile, @Query("type") String type);

    @GET("v1/child/tag.json")
    Call<JsonObject> inviteChild(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Query("user_id") String user_id/*,@Query("request_type") String request_type*/);

    @GET("v1/child/tag.json")
    Call<JsonObject> resendInviteChild(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Query("user_id") String user_id, @Query("resend") String resend);

    //https://103.233.2.220/digital_bank/api/v1/child/{id}/details.json
    //todo by ashu
    @GET("v1/child/details.json")
    Call<JsonObject> getChildDetail(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Query("id") String id);

    @GET("v1/user/notification.json")
    Call<JsonObject> getChildInvitationNotification(@Header("Accept") String acceptHeader, @Header("Authorization") String token);

    @GET("v1/user/packages.json")
    Call<JsonObject> getPackage(@Header("Accept") String acceptHeader, @Header("Authorization") String token);

    @Multipart
    @POST("v1/user/updateProfile.json")
    Call<JsonObject> saveUserProfileData(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @PartMap HashMap<String, RequestBody> body);

    @POST("v1/user/response.json")
    @FormUrlEncoded
    Call<JsonObject> updateChildInvitation(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Field("user_id") String parentId, @Field("request_type") String requestType, @Field("request_status") String requestStatus);

    //todo by ashu
    @GET()
    Call<JsonObject> getResponse(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Url String url);

    @GET()
    Call<JsonObject> getResponseWithoutHeader(@Url String url);

    @GET()
    Call<JsonObject> getResponseNormal(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Url String url);

    @Multipart
    @POST()
    Call<JsonObject> getPostResponse(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Url String url, @PartMap HashMap<String, RequestBody> body);

    @GET
    Call<JsonObject> getParentPaymentHistory(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Url String url);

    @POST
    @FormUrlEncoded
    Call<JsonObject> boostTaskResponse(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Url String url, @Field("student_id") String student_id, @Field("boost_points") String boost_value);

    @POST
    @FormUrlEncoded
    Call<JsonObject> taskAnswerResponse(@Header("Accept") String acceptHeader, @Header("Authorization") String token, @Url String url, @Field("answer") String option_id);

    @POST("v1/product/set_priority.json")
    @FormUrlEncoded
    Call<JsonObject> setPriorityResponse(@Header("Accept") String acceptHeader, @Header("Authorization") String token,/* @Url String url, */@Field("product_id") String product_id, @Field("auction_id") String auction_id);

    @Multipart
    @POST
    Call<JsonObject> getPostResponseWithoutHeader( @Url String url, @PartMap HashMap<String, RequestBody> body);

}
