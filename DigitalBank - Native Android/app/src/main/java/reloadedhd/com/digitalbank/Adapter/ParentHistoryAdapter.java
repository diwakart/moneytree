package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import reloadedhd.com.digitalbank.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import reloadedhd.com.digitalbank.model.PaymentHistoryModel;

public class ParentHistoryAdapter extends RecyclerView.Adapter<ParentHistoryAdapter.ParentHolder> {
    private Context context;
    private ArrayList<PaymentHistoryModel> arrayList;

    public ParentHistoryAdapter(Context context, ArrayList<PaymentHistoryModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ParentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.parent_history_adapter, parent, false);
        return new ParentHolder(v);
    }

    @Override
    public void onBindViewHolder(ParentHolder holder, int position) {
        holder.transactionPoints.setText("Transaction Points : " + arrayList.get(position).getTransaction_points() + " Points");
        holder.transactionRemarks.setText("Description : " + arrayList.get(position).getRemarks());
        Date date = null;
        String outputString = null;

        try {

            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(arrayList.get(position).getTransaction_time());

        } catch (ParseException e) {

            e.printStackTrace();

        }

        outputString = new SimpleDateFormat("dd-MM-yyyy").format(date);
        String newString = new SimpleDateFormat("HH:mm").format(date);

        holder.transactionDateTime.setText("Transaction Date : " + outputString + " Time : " + newString);

      //  holder.transactionDateTime.setText("Transaction Date : "+arrayList.get(position).getTransaction_time());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ParentHolder extends RecyclerView.ViewHolder {

        TextView transactionPoints, transactionRemarks,transactionDateTime;

        public ParentHolder(View itemView) {
            super(itemView);
            transactionPoints = itemView.findViewById(R.id.transactionPoints);
            transactionRemarks = itemView.findViewById(R.id.transactionRemarks);
            transactionDateTime = itemView.findViewById(R.id.transactionDateTime);

        }

    }

}
