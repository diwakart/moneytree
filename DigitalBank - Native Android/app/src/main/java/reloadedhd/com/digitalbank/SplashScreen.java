package reloadedhd.com.digitalbank;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Activity.MainActivity2;

public class SplashScreen extends AppCompatActivity {
    private static int TIME_OUT = 2000;
    String token = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e("logindiv", (5/2) + "//");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                SharedPreferences sharedPreferences = getSharedPreferences("LoginPref", MODE_PRIVATE);
                token = sharedPreferences.getString("token", "");

               /*SharedPreferences.Editor editor = sharedPreferences.edit();
              //editor.clear();
                editor.commit();*/
                Log.e("spsp", token + "/");

             /*if (token!=null && !token.isEmpty())
                {
                    startActivity(new Intent(SplashScreen.this, MainActivity2.class));
                    finish();
                }else {*/

                startActivity(new Intent(SplashScreen.this, LoginScreen.class));
                finish();

                //}

            }

        }, TIME_OUT);

    }

}
