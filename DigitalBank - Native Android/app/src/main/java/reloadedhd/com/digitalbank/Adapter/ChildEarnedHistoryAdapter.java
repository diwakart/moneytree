package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import reloadedhd.com.digitalbank.Fragment.HistoryDatail;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.model.ChildEarnModelNew;

public class ChildEarnedHistoryAdapter extends RecyclerView.Adapter<ChildEarnedHistoryAdapter.Holder> {
    private Context context;
    private ArrayList<ChildEarnModelNew> arrayList;

    public ChildEarnedHistoryAdapter(Context context, ArrayList<ChildEarnModelNew> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.child_earned_history_adapter, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        holder.earnHistoryRemarks.setText(arrayList.get(position).getRemarks());
        holder.earnHistoryPoint.setText(arrayList.get(position).getTransaction_points() + " Points");

        Date date = null;
        String outputString = null;

        try {

            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(arrayList.get(position).getTransaction_time());

        } catch (ParseException e) {

            e.printStackTrace();

        }

        outputString = new SimpleDateFormat("dd-MM-yyyy").format(date);
        String newString = new SimpleDateFormat("HH:mm").format(date);

        //holder.auctionStartTime.setText("Auction Start Date : " + outputString + " Time : " + newString);

        holder.earnHistoryTime.setText("Date : " + outputString + " Time : " + newString);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView earnHistoryRemarks, earnHistoryPoint, earnHistoryTime;

        public Holder(View itemView) {
            super(itemView);
            earnHistoryRemarks = itemView.findViewById(R.id.earnHistoryRemarks);
            earnHistoryPoint = itemView.findViewById(R.id.earnHistoryPoint);
            earnHistoryTime = itemView.findViewById(R.id.earnHistoryTime);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HistoryDatail detail = new HistoryDatail();
                    Bundle b = new Bundle();
                    b.putString("id", arrayList.get(getAdapterPosition()).getTransaction_id());
                    detail.setArguments(b);
                    Global.changeFragment(context, detail);
                }
            });
        }
    }
}
