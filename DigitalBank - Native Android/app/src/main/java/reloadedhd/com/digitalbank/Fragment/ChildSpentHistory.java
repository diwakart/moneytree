package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.ChildSpentHistoryAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.ChildSpentHistoryModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChildSpentHistory extends Fragment {
    private RecyclerView childSpentList;
    private LinearLayoutManager manager;
    private TextView childSpentNoData;
    private SharedPreferences preferences;
    private ArrayList<ChildSpentHistoryModel> arrayList;
    private ChildSpentHistoryAdapter spentParentAdapter;
    private ShimmerFrameLayout historyShimmer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.child_spent_list, null);
        childSpentList = v.findViewById(R.id.childSpentList);
        childSpentNoData = v.findViewById(R.id.childSpentNoData);

        historyShimmer = v.findViewById(R.id.historyShimmer);

        arrayList = new ArrayList<>();
        manager = new LinearLayoutManager(getActivity());
        childSpentList.setLayoutManager(manager);
        spentParentAdapter = new ChildSpentHistoryAdapter(getActivity(), arrayList);
        childSpentList.setAdapter(spentParentAdapter);

        preferences = DigitalBankContoller.getGlobalPreferences();
        getSpendList(preferences.getString("user_id", ""));

        return v;

    }

    void getSpendList(String id) {
        historyShimmer.setVisibility(View.VISIBLE);
        historyShimmer.startShimmer();
        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user/spent_list.json");
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                historyShimmer.stopShimmer();
                historyShimmer.setVisibility(View.GONE);

                if (response.code()==401){

                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                try {

                    if (response.isSuccessful()) {

                        String res = response.body().toString();
                        Log.e("spentdata", res);
                        JSONObject jsonObject = new JSONObject(res);
                        JSONArray array = jsonObject.optJSONArray("data");

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);
                            ChildSpentHistoryModel model = new ChildSpentHistoryModel();
                            model.setRemarks(object.optString("remarks"));
                            model.setTransaction_id(object.optString("transaction_id"));
                            model.setTransaction_points(object.optString("transaction_points"));
                            model.setTransaction_time(object.optString("transaction_time"));
                            model.setType(object.optString("type"));
                            arrayList.add(model);

                        }

                        if (!arrayList.isEmpty()) {

                            spentParentAdapter.notifyDataSetChanged();
                            childSpentList.setVisibility(View.VISIBLE);
                            childSpentNoData.setVisibility(View.GONE);

                        } else {

                            childSpentList.setVisibility(View.GONE);
                            childSpentNoData.setVisibility(View.VISIBLE);

                        }

                    } else {

                        childSpentList.setVisibility(View.GONE);
                        childSpentNoData.setVisibility(View.VISIBLE);

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                // pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

}
