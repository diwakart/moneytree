package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.model.ChildProductModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import reloadedhd.com.digitalbank.transform.CircleTransform;

public class ChildProductListAdapter extends RecyclerView.Adapter<ChildProductListAdapter.ChildHolder> {
    private Context context;
    private ArrayList<ChildProductModel> arrayList;
    ChildAdapterClick click;

    public ChildProductListAdapter(Context context, ArrayList<ChildProductModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ChildHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.child_product_adapter, parent, false);
        return new ChildHolder(v);

    }

    @Override
    public void onBindViewHolder(final ChildHolder holder, int position) {

        if (arrayList.get(position).isAdded()) {

            holder.childAdapterAddProduct.setVisibility(View.GONE);

        } else {

            holder.childAdapterAddProduct.setVisibility(View.VISIBLE);

        }

        holder.childProductName.setText(arrayList.get(position).getProduct_name());
        holder.childProductPoints.setText(arrayList.get(position).getProduct_price() + " Points");
        //holder.childProductCategoryName.setText(arrayList.get(position).getCategory_name());

        if (arrayList.get(position).getProduct_image() != null && !arrayList.get(position).getProduct_image().equalsIgnoreCase("null")) {

            Picasso.with(context).load(ServiceURLs.PRODUCTIMAGEBASEURL + arrayList.get(position).getProduct_image()).transform(new CircleTransform(20, 0)).fit().into(holder.childProductAdapterImage);

        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ChildHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView childProductAdapterImage;
         // Button childAdapterAddProduct;
        TextView childProductName, childProductPoints, childProductCategoryName,childAdapterAddProduct;

        public ChildHolder(View itemView) {
            super(itemView);
            childProductAdapterImage = itemView.findViewById(R.id.childProductAdapterImage);
            childProductName = itemView.findViewById(R.id.childProductName);
            childProductPoints = itemView.findViewById(R.id.childProductPoints);
            //childProductCategoryName = itemView.findViewById(R.id.childProductCategoryName);
            childAdapterAddProduct = itemView.findViewById(R.id.childAdapterAddProduct);
            childAdapterAddProduct.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.childAdapterAddProduct:

                    click.onAddItem(getAdapterPosition());

                    break;

            }

        }

    }

    public void onAddItemCLick(ChildAdapterClick click) {
        this.click = click;
    }

    public interface ChildAdapterClick {

        void onAddItem(int pos);

    }

}
