package reloadedhd.com.digitalbank.Fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Activity.MainActivity2;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.WebService.UpdateUserProfile_Api;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.global.DigitalGlobal;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ParentProfile extends Fragment {
    RelativeLayout relLayout_back_Parentprofile, relLayout_edit_Parentprofile;
    ImageView profile_pic_Parentprofile, img_ParentProfile_background;
    EditText edt_username_parentProfile, edt_contact_parentProfile, edt_email_userProfile;
    private static final String IMAGE_DIRECTORY_NAME = "DigiBank";
    String image_path1 = "";
    String image_path2 = "";
    String image_path3 = "";
    byte[] byteArray;
    Uri fileUri;
    private SharedPreferences preferences;
    private String imageName;
    private TextView userNameEdit;
    private Button parentEditProfileSubmit;
    private RadioGroup radioGroupEdit;
    private RadioButton radioButtonMale, radioButtonFeMale;

    public ParentProfile() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_parent_profile, container, false);

        relLayout_back_Parentprofile = view.findViewById(R.id.relLayout_back_Parentprofile);
        relLayout_edit_Parentprofile = view.findViewById(R.id.relLayout_edit_Parentprofile);
        profile_pic_Parentprofile = view.findViewById(R.id.profile_pic_Parentprofile);
        img_ParentProfile_background = view.findViewById(R.id.img_ParentProfile_background);
        parentEditProfileSubmit = view.findViewById(R.id.parentEditProfileSubmit);
        edt_username_parentProfile = view.findViewById(R.id.edt_username_parentProfile);
        edt_contact_parentProfile = view.findViewById(R.id.edt_contact_parentProfile);
        edt_email_userProfile = view.findViewById(R.id.edt_email_userProfile);
        userNameEdit = view.findViewById(R.id.userNameEdit);
        radioButtonMale = view.findViewById(R.id.radioButtonMale);
        radioButtonFeMale = view.findViewById(R.id.radioButtonFeMale);
        radioGroupEdit = view.findViewById(R.id.radioGroupEdit);

        preferences = DigitalBankContoller.getGlobalPreferences();

        setData();

        edt_email_userProfile.setEnabled(false);
        edt_contact_parentProfile.setEnabled(false);
        edt_username_parentProfile.setEnabled(false);
        profile_pic_Parentprofile.setEnabled(false);
        parentEditProfileSubmit.setEnabled(false);
        radioButtonMale.setEnabled(false);
        radioButtonFeMale.setEnabled(false);

        parentEditProfileSubmit.setVisibility(View.GONE);

        for (int i = 0; i < radioGroupEdit.getChildCount(); i++) {
            radioGroupEdit.getChildAt(i).setEnabled(false);
        }

        relLayout_edit_Parentprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edt_email_userProfile.setEnabled(true);
                edt_contact_parentProfile.setEnabled(true);
                edt_username_parentProfile.setEnabled(true);
                relLayout_edit_Parentprofile.setVisibility(View.GONE);
                profile_pic_Parentprofile.setEnabled(true);
                parentEditProfileSubmit.setEnabled(true);
                parentEditProfileSubmit.setVisibility(View.VISIBLE);

                for (int i = 0; i < radioGroupEdit.getChildCount(); i++) {
                    radioGroupEdit.getChildAt(i).setEnabled(true);
                }

            }
        });

        relLayout_back_Parentprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity2.rel_header.setVisibility(View.VISIBLE);
                getActivity().onBackPressed();
            }
        });

        profile_pic_Parentprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                insertDummyContactWrapper();

            }
        });

        parentEditProfileSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edt_username_parentProfile.getText().toString().trim().length() < 4) {

                    edt_username_parentProfile.setError("User Name must be atleast 4 characters");

                } else {

                    saveUserData();
                }

            }
        });

        return view;

    }

    public void saveUserData() {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("name", RequestBody.create(MediaType.parse("multipart/form-data"), edt_username_parentProfile.getText().toString().trim()));

        if (edt_contact_parentProfile.getText().toString().trim().length() > 0) {
            body.put("mobile", RequestBody.create(MediaType.parse("multipart/form-data"), edt_contact_parentProfile.getText().toString().trim()));
        }

        if (radioButtonFeMale.isChecked()) {
            body.put("gender", RequestBody.create(MediaType.parse("multipart/form-data"), "2"));
        } else if (radioButtonMale.isChecked()) {
            body.put("gender", RequestBody.create(MediaType.parse("multipart/form-data"), "1"));
        }

        if (edt_email_userProfile.getText().toString().trim().length() > 0 && !edt_email_userProfile.getText().toString().trim().equalsIgnoreCase("N/A"))
            body.put("email", RequestBody.create(MediaType.parse("multipart/form-data"), edt_email_userProfile.getText().toString().trim()));


        Log.e("bddd", body.toString());

        //picture optional key
        //will modify according to the requirement
        Call<JsonObject> call = service.saveUserProfileData(Global.acceptHeader, Global.token, body);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                    String res = response.body().toString();
                    Log.e("updateprofile", res);
                    try {
                        JSONObject object = new JSONObject(res);
                        // getUserData();
                        new CustomAlert().showAlert(getActivity(), 1, object.optString("message"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.cancel();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    public void setData() {

        if (preferences.contains("bitmap") && preferences.getString("bitmap", "") != null) {
            profile_pic_Parentprofile.setImageBitmap(new DigitalGlobal().StringToBitMap(preferences.getString("bitmap", "").trim()));
            //img_ParentProfile_background.setImageBitmap(new DigitalGlobal().StringToBitMap(preferences.getString("bitmap", "").trim()));

        } else if (preferences.contains("avtar") && preferences.getString("avtar", "") != null && !preferences.getString("avtar", "").equalsIgnoreCase("null")) {

            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(ServiceURLs.USERPROFILEIMAGEBASEURL + preferences.getString("avtar", "").trim(), profile_pic_Parentprofile, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    Log.e("imgstrt", "ok/" + imageUri);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    Log.e("imgfail", "fail" + failReason.toString());
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    profile_pic_Parentprofile.setImageBitmap(loadedImage);
                    // img_ParentProfile_background.setImageBitmap(loadedImage);

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    Log.e("imgcanc", imageUri + "");
                }

            });

        } else {

            profile_pic_Parentprofile.setImageDrawable(getResources().getDrawable(R.drawable.profile_holder));
            //img_ParentProfile_background.setImageDrawable(getResources().getDrawable(R.drawable.parent_background));

        }
        if (preferences.contains("name")) {
            edt_username_parentProfile.setText(preferences.getString("name", ""));
            userNameEdit.setText(preferences.getString("name", ""));
        }
        if (preferences.contains("mobile") && !preferences.getString("mobile", "").equalsIgnoreCase("") && !preferences.getString("mobile", "").equalsIgnoreCase("null")) {
            edt_contact_parentProfile.setText(preferences.getString("mobile", ""));
        }
        if (preferences.contains("email")) {
            edt_email_userProfile.setText(preferences.getString("email", ""));
        } else {
            edt_email_userProfile.setText("N/A");
        }
        if (preferences.contains("gender") && !preferences.getString("gender", "").equalsIgnoreCase("") && !preferences.getString("gender", "").equalsIgnoreCase("null")) {
            if (preferences.getString("gender", "").equalsIgnoreCase("1")) {
                radioButtonMale.setChecked(true);
            } else if (preferences.getString("gender", "").equalsIgnoreCase("2")) {
                radioButtonFeMale.setChecked(true);
            }
        }

    }

    //************************************************Camera Permission**********************************************
    @TargetApi(Build.VERSION_CODES.M)
    private void insertDummyContactWrapper() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 11);
        } else
            selectImage();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        // TODO Auto-generated method stub
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 11:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();

                } else {
                    //Permission Denied
                    Toast.makeText(getActivity(), "CAMERA Denied", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
    //**********************image picker gallery or camera********************
    private void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {

                    openCamera();

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, 1);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        //External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
        //Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;

        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == 1) {

                try {

                    profile_pic_Parentprofile.setImageURI(fileUri);
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), fileUri);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                    byteArray = byteArrayOutputStream.toByteArray();

                    updateUserImage(byteArray);

                } catch (IOException e) {

                    e.printStackTrace();

                } catch (NullPointerException e) {

                }


            } else if (requestCode == 2) {


                try {
                    Uri resultUri = data.getData();
                    profile_pic_Parentprofile.setImageURI(resultUri);
                    //  img_ParentProfile_background.setImageURI(resultUri);

                    Log.e("refgd", resultUri.getPath() + "");

                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), resultUri);


                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                    byteArray = byteArrayOutputStream.toByteArray();
                    updateUserImage(byteArray);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                }


            }

        }


    }

    public void updateUserImage(byte[] byteArr) {

        RequestBody imagefile1;
        MultipartBody.Part image1 = null;
        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);

        imagefile1 = RequestBody.create(MediaType.parse("image/*"), byteArr);
        image1 = MultipartBody.Part.createFormData("picture", System.currentTimeMillis() + "a.jpeg", imagefile1);

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.uploadPic(Global.acceptHeader, Global.token, image1);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }


                String res = response.body().toString();
                try {
                    pd.dismiss();
                    JSONObject jsonObject = new JSONObject(res);
                    String status = jsonObject.optString("status").toString();

                    if (status.equalsIgnoreCase("success")) {

                        String message = jsonObject.optString("message").toString();
                        new CustomAlert().showAlert(getActivity(), 1, message);
                        JSONObject object = jsonObject.optJSONObject("data");
                        imageName = object.optString("avtar");

                        final SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("avtar", object.optString("avtar"));
                        editor.commit();

                        ImageLoader imageLoader = ImageLoader.getInstance();
                        imageLoader.displayImage(ServiceURLs.USERPROFILEIMAGEBASEURL + imageName, profile_pic_Parentprofile, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                profile_pic_Parentprofile.setImageBitmap(loadedImage);
                                ParentScreen.profile_pic_profile.setImageBitmap(loadedImage);
                                MainActivity2.profile_pic_main2.setImageBitmap(loadedImage);
                                //  img_ParentProfile_background.setImageBitmap(loadedImage);

                                SharedPreferences.Editor editor1 = preferences.edit();
                                editor1.putString("bitmap", new DigitalGlobal().BitMapToString(loadedImage));
                                editor1.commit();

                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {

                            }
                        });


                    } else {

                        String message = jsonObject.optString("message").toString();
                        Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();

            }

        });

    }

}
