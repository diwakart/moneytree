package reloadedhd.com.digitalbank.WebService;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import reloadedhd.com.digitalbank.Adapter.ParentScreenAdapter;
import reloadedhd.com.digitalbank.Fragment.ParentScreen;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.model.ChildListModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 6/6/2018.
 */

public class GetChildListDetail_Api {
    Context context;
    String acceptHeader, token;
    RecyclerView recyclerview_parentScreen;
    ArrayList<ChildListModel> arrayList = new ArrayList<>();
    ProgressDialog progressDialog;

    public void getChildListDetail(FragmentActivity activity, String acceptHeader, String token, final RecyclerView recyclerview_parentScreen) {
        this.context = activity;
        this.acceptHeader = acceptHeader;
        this.token = token;
        this.recyclerview_parentScreen = recyclerview_parentScreen;

        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getChildListDetail(acceptHeader, token);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();

                String res = response.isSuccessful() + "";

                if (res.equalsIgnoreCase("true"))

                    try {

                        Log.e("resss", res);
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        String status = jsonObject.getString("status");
                        Log.e("Egvrtgv", status);
                        Log.e("ressss", jsonObject.toString());
                        arrayList.clear();

                        if (status.equalsIgnoreCase("success")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            //Global.getArrayList_getChildListDetail().clear();

                            for (int j = 0; j < jsonArray.length(); j++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                                ChildListModel model = new ChildListModel();
                                model.setStudent_id(jsonObject1.optString("student_id"));
                                model.setId(jsonObject1.optString("id"));
                                model.setRequest_type(jsonObject1.optString("request_type"));
                                model.setRequest_status(jsonObject1.optString("request_status"));
                                model.setRequested_time(jsonObject1.optString("requested_time"));
                                model.setPoints(jsonObject1.optString("points"));
                                model.setName(jsonObject1.optString("name"));
                                model.setDob(jsonObject1.optString("dob"));
                                model.setMobile(jsonObject1.optString("mobile"));
                                model.setEmail(jsonObject1.optString("email"));
                                model.setAvtar(jsonObject1.optString("avtar"));
                                model.setCurrent_point(jsonObject1.optString("current_point"));
                                model.setAccount_type(jsonObject1.optString("account_type"));

                                arrayList.add(model);

                                // Global.setArrayList_getChildListDetail(arrayList);
                            }

                            if (!arrayList.isEmpty()) {

                               // ParentScreen.noChilds.setVisibility(View.GONE);
                            //    ParentScreen.recyclerview_parentScreen.setVisibility(View.VISIBLE);

                                ParentScreenAdapter parentScreenAdapter = new ParentScreenAdapter(context, arrayList);
                                recyclerview_parentScreen.setAdapter(parentScreenAdapter);

                            } else {

                            //    ParentScreen.noChilds.setVisibility(View.VISIBLE);
                             //   ParentScreen.recyclerview_parentScreen.setVisibility(View.GONE);

                            }

                        } else {

                            Toast.makeText(context, response.message() + "", Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }

        });

    }

}
