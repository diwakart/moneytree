package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import github.nisrulz.recyclerviewhelper.RVHItemDividerDecoration;
import github.nisrulz.recyclerviewhelper.RVHItemTouchHelperCallback;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.TaskDetailAdapter;
import reloadedhd.com.digitalbank.CustomVideoPlayer.CustomVideoView;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.OptionModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LessonOfDay extends Fragment implements View.OnClickListener {
    private CustomVideoView lessonDayVideo;
    private ImageView lessonDayImage, playVideoImage, playImage, pauseImage;
    private LinearLayout playerLinear, topLessonLinear, lessonDayAnswerLinear, timerLinear;
    private String url = "https://www.hdwplayer.com/videos/300.mp4", audioUrl = null, lessonId = null, alreadyAttempted = "0";
    private RelativeLayout toolbarRelativeBack;
    private TextView noLesson, toolbarTitle, startTimeText, endTimeText, lessonQuestionText, lessonTimer, lessonDescription, lessonQuestionAnswer;
    private MediaPlayer mPlayer = null;
    private SeekBar seekbar;
    private double startTime = 0;
    private double finalTime = 0, lastPos = 0.0;
    private int firstTime = 0;
    private Handler myHandler = new Handler();
    private MediaController mediaController = null;
    private boolean value = false;
    private RecyclerView lessonDayOptionList;
    private EditText lessonEditText;
    private Button lessonDayButton;
    private CardView lessonAudioCard, imageVideoCard;
    private ArrayList<OptionModel> optionModelArrayList;
    private TaskDetailAdapter adapter;
    private WebView lessonWebview;
    private SharedPreferences preferences;
    private ShimmerFrameLayout lessonDayShimmer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.lesson_of_day, container, false);
        preferences = DigitalBankContoller.getGlobalPreferences();
        optionModelArrayList = new ArrayList<>();

        optionModelArrayList.clear();
        initView(v);
        return v;
    }

    private void initView(View v) {

        toolbarRelativeBack = v.findViewById(R.id.toolbarRelativeBack);
        toolbarTitle = v.findViewById(R.id.toolbarTitle);
        lessonDayVideo = v.findViewById(R.id.lessonDayVideo);
        lessonDayImage = v.findViewById(R.id.lessonDayImage);
        playVideoImage = v.findViewById(R.id.playVideoImage);

        //todo for Audio play

        playImage = v.findViewById(R.id.playImage);
        pauseImage = v.findViewById(R.id.pauseImage);
        playerLinear = v.findViewById(R.id.playerLinear);
        startTimeText = v.findViewById(R.id.startTimeText);
        endTimeText = v.findViewById(R.id.endTimeText);
        lessonQuestionText = v.findViewById(R.id.lessonQuestionText);

        lessonDayAnswerLinear = v.findViewById(R.id.lessonDayAnswerLinear);
        lessonQuestionAnswer = v.findViewById(R.id.lessonQuestionAnswer);

        lessonDayShimmer = v.findViewById(R.id.lessonDayShimmer);

        timerLinear = v.findViewById(R.id.timerLinear);

        noLesson = v.findViewById(R.id.noLesson);

        lessonWebview = v.findViewById(R.id.lessonWebview);

        lessonDescription = v.findViewById(R.id.lessonDescription);
        lessonAudioCard = v.findViewById(R.id.lessonAudioCard);
        imageVideoCard = v.findViewById(R.id.imageVideoCard);

        seekbar = v.findViewById(R.id.seekbar);
        lessonTimer = v.findViewById(R.id.lessonTimer);
        lessonDayOptionList = v.findViewById(R.id.lessonDayOptionList);
        lessonEditText = v.findViewById(R.id.lessonEditText);
        lessonDayButton = v.findViewById(R.id.lessonDayButton);
        topLessonLinear = v.findViewById(R.id.topLessonLinear);

        lessonDayOptionList.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new TaskDetailAdapter(getActivity(), optionModelArrayList);
        lessonDayOptionList.setAdapter(adapter);

        //Setup onItemTouchHandler to enable drag and drop , swipe left or right
        ItemTouchHelper.Callback callback = new RVHItemTouchHelperCallback(adapter, true, true,
                true);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(lessonDayOptionList);

        //Set the divider in the recyclerview
        lessonDayOptionList.addItemDecoration(new RVHItemDividerDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        toolbarTitle.setText("Lesson of the day");

        //Creating MediaController
        mediaController = new MediaController(getActivity());
        mediaController.setAnchorView(lessonDayVideo);

        //specify the location of media file

        playVideoImage.setOnClickListener(this);
        pauseImage.setOnClickListener(this);
        toolbarRelativeBack.setOnClickListener(this);
        playImage.setOnClickListener(this);
        lessonDayButton.setOnClickListener(this);

        lessonDayVideo.setPlayPauseListener(new CustomVideoView.PlayPauseListener() {
            @Override
            public void onPlay() {

                if (mPlayer != null && mPlayer.isPlaying()) {

                    lastPos = mPlayer.getCurrentPosition();
                    playImage.setImageDrawable(getResources().getDrawable(R.drawable.play_video));
                    mPlayer.stop();

                }

            }

            @Override
            public void onPause() {

            }

        });

        //todo for start the timer
        //setVideoUrl(url);
        //startCounter(120000);
        //getLessonData();

        getLessonData();

    }

    private void getLessonData() {

        lessonDayShimmer.setVisibility(View.VISIBLE);
        lessonDayShimmer.startShimmer();
        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/lessons_of_day.json");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                lessonDayShimmer.stopShimmer();
                lessonDayShimmer.setVisibility(View.GONE);

                if (response.code() == 401) {

                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                    String s = response.body().toString();
                    Log.e("lessonres", s);

                    try {

                        optionModelArrayList.clear();
                        JSONObject jsonObject = new JSONObject(s);

                        if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                            JSONArray array = jsonObject.optJSONArray("data");

                            if (array.length() > 0) {

                                topLessonLinear.setVisibility(View.VISIBLE);
                                noLesson.setVisibility(View.GONE);

                            } else {

                                topLessonLinear.setVisibility(View.GONE);
                                noLesson.setVisibility(View.VISIBLE);

                            }

                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object = array.optJSONObject(i);
                                lessonId = object.optString("lesson_id");

                                if (object.has("lesson_description") && !object.optString("lesson_description").isEmpty() && !object.optString("lesson_description").trim().equalsIgnoreCase("null")) {

                                    lessonDescription.setVisibility(View.VISIBLE);
                                    lessonDescription.setText(object.optString("lesson_description"));

                                } else {

                                    lessonDescription.setVisibility(View.GONE);

                                }

                                alreadyAttempted = object.optString("already_attempted");

                                lessonDescription.setText(object.optString("lesson_description"));
                                lessonQuestionText.setText(object.optString("lesson_title"));
//lesson_title
                                DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                                Date date = sdf.parse(object.optString("skipped_time"));
                              /*Log.e("seconds", date.getSeconds() + "//");
                                Log.e("seconds", date.getTime() + "//");*/

                                long timerTime = (((date.getHours() * 60 * 60) + (date.getMinutes() * 60) + date.getSeconds()) * 1000);

                                Log.e("timermili", timerTime + "//");

                                if (!alreadyAttempted.equalsIgnoreCase("1")) {
                                    timerLinear.setVisibility(View.VISIBLE);
                                    lessonTimer.setVisibility(View.VISIBLE);
                                    lessonQuestionAnswer.setVisibility(View.GONE);
                                    lessonDayAnswerLinear.setAlpha(0.10f);
                                    timerLinear.setBackgroundColor(getResources().getColor(R.color.blackTransparent));
                                    startCounter(timerTime);
                                } else {
                                    timerLinear.setVisibility(View.GONE);
                                    lessonTimer.setVisibility(View.GONE);
                                    lessonDayButton.setVisibility(View.GONE);
                                    lessonDayAnswerLinear.setAlpha(1f);
                                    lessonQuestionAnswer.setVisibility(View.VISIBLE);

                                }

                                if (!object.optString("file_attached").trim().isEmpty() && !object.optString("file_attached").equalsIgnoreCase("null")) {

                                    lessonWebview.setVisibility(View.GONE);

                                    if (object.optString("file_attached").contains(".mp4")) {

                                        lessonDayVideo.setVisibility(View.VISIBLE);
                                        playVideoImage.setVisibility(View.VISIBLE);
                                        imageVideoCard.setVisibility(View.VISIBLE);
                                        lessonDayImage.setVisibility(View.GONE);
                                        lessonAudioCard.setVisibility(View.GONE);
                                        url = object.optString("base_url").trim() + object.optString("file_attached").trim();

                                        setVideoUrl(url);

                                    } else if (object.optString("file_attached").contains(".mp3")) {

                                        imageVideoCard.setVisibility(View.GONE);
                                        lessonAudioCard.setVisibility(View.VISIBLE);
                                        audioUrl = object.optString("base_url").trim() + object.optString("file_attached");

                                    } else {

                                        imageVideoCard.setVisibility(View.VISIBLE);
                                        lessonDayImage.setVisibility(View.VISIBLE);
                                        lessonAudioCard.setVisibility(View.GONE);
                                        lessonDayVideo.setVisibility(View.GONE);
                                        playVideoImage.setVisibility(View.GONE);
                                        Picasso.with(getActivity()).load(object.optString("base_url").trim() + object.optString("file_attached")).fit().into(lessonDayImage);

                                    }

                                } else if (!object.optString("link_url").isEmpty() && object.optString("link_url") != null && !object.optString("link_url").equalsIgnoreCase("null")) {

                                    lessonWebview.setVisibility(View.VISIBLE);
                                    imageVideoCard.setVisibility(View.VISIBLE);

                                    lessonDayImage.setVisibility(View.VISIBLE);
                                    lessonAudioCard.setVisibility(View.GONE);
                                    lessonDayVideo.setVisibility(View.GONE);
                                    playVideoImage.setVisibility(View.GONE);

                                    lessonWebview.setWebViewClient(new WebViewClient());
                                    lessonWebview.getSettings().setLoadsImagesAutomatically(true);
                                    lessonWebview.getSettings().setJavaScriptEnabled(true);
                                    lessonWebview.getSettings().setPluginState(WebSettings.PluginState.ON);
                                    lessonWebview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                                    lessonWebview.loadUrl(object.optString("link_url"));

                                } else {

                                    imageVideoCard.setVisibility(View.GONE);

                                    lessonAudioCard.setVisibility(View.GONE);

                                }

                                lessonQuestionAnswer.setText("Answer is : " + object.optString("correct_answer"));

                                if (object.optString("question_type").equalsIgnoreCase("1")) {

                                    lessonDayOptionList.setVisibility(View.GONE);
                                    lessonEditText.setVisibility(View.VISIBLE);
                                    //question_type

                                } else if (!object.optString("question_type").equalsIgnoreCase("1")) {

                                    lessonEditText.setVisibility(View.GONE);

                                    JSONArray jsonArray = object.optJSONArray("options");

                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject object1 = jsonArray.optJSONObject(j);
                                        OptionModel optionModel = new OptionModel();
                                        optionModel.setLabel(object1.optString("label"));
                                        optionModel.setValue(object1.optString("value"));
                                        optionModel.setQuestionType(object.optString("question_type"));
                                        optionModelArrayList.add(optionModel);

                                    }

                                    if (!optionModelArrayList.isEmpty()) {

                                        adapter.notifyDataSetChanged();
                                        lessonDayOptionList.setVisibility(View.VISIBLE);

                                    } else {

                                        lessonDayOptionList.setVisibility(View.GONE);

                                    }

                                }

                            }

                        } else {

                            new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    } catch (ParseException e) {

                        e.printStackTrace();

                    }

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //pd.cancel();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();

            }

        });

    }

    private void setVideoUrl(String ur) {
        Uri uri = Uri.parse(ur);

        //Setting MediaController and URI, then starting the videoView
        lessonDayVideo.setMediaController(mediaController);
        lessonDayVideo.setVideoURI(uri);
        lessonDayVideo.requestFocus();

    }

    private void startCounter(long milisec) {

        new CountDownTimer(milisec, 1000) {

            public void onTick(long millisUntilFinished) {

                lessonTimer.setText((String.format("%02d:%02d:%02d",
                        /*TimeUnit.MILLISECONDS.toDays(millisUntilFinished),*/
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)))) + " Seconds ");

            }

            public void onFinish() {

                lessonTimer.setText("done!");
                lessonTimer.setVisibility(View.GONE);
                timerLinear.setVisibility(View.GONE);
                lessonDayAnswerLinear.setAlpha(1f);

                if (!alreadyAttempted.equalsIgnoreCase("1")) {

                    lessonDayButton.setVisibility(View.VISIBLE);

                }

            }

        }.start();

    }

    private void playAudio() {
        value = true;
        mPlayer = new MediaPlayer();

        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {

            //mPlayer.setDataSource("https://quizme.com.my/digital_bank/storage/app/audio/audio.mp3");
            mPlayer.setDataSource(audioUrl);
            mPlayer.prepare();

        } catch (IllegalArgumentException e) {

            Toast.makeText(getActivity(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();

        } catch (SecurityException e) {

            Toast.makeText(getActivity(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();

        } catch (IllegalStateException e) {

            Toast.makeText(getActivity(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();

        } catch (IOException e) {

            e.printStackTrace();

        }

        mPlayer.start();

        if (mPlayer.isPlaying()) {

            startTime = mPlayer.getCurrentPosition();
            finalTime = mPlayer.getDuration();

            startTimeText.setText(String.format("%d min, %d sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                    startTime)))
            );

            endTimeText.setText(String.format("%d min, %d sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                    finalTime)))
            );

            if (firstTime == 0) {

                seekbar.setMax((int) finalTime);
                firstTime = 1;

            }

            Log.e("ok", "//" + mPlayer.getTrackInfo());

            if (lastPos != 0.0) {

                startTime = lastPos;
                seekbar.setProgress((int) lastPos);
                mPlayer.seekTo((int) lastPos);

            } else {

                seekbar.setProgress((int) startTime);

            }

        }

        myHandler.postDelayed(UpdateSongTime, 100);

    }

    private void stopAudio() {
        value = false;
        if (mPlayer != null && mPlayer.isPlaying()) {
            lastPos = mPlayer.getCurrentPosition();
            mPlayer.release();
            mPlayer = null;
            // mPlayer.stop();
            playImage.setImageDrawable(getResources().getDrawable(R.drawable.play_video));
        }
    }

    @Override
    public void onStop() {
        value = false;

        if (mPlayer != null && mPlayer.isPlaying()) {
            lastPos = mPlayer.getCurrentPosition();
            //mPlayer.stop();
            mPlayer.stop();
            //mPlayer.release();
            playImage.setImageDrawable(getResources().getDrawable(R.drawable.play_video));

        } else if (lessonDayVideo != null && lessonDayVideo.isPlaying()) {
            lessonDayVideo.pause();

        }
        super.onStop();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.playVideoImage:

                value = false;
                if (mPlayer != null && mPlayer.isPlaying()) {
                    lastPos = mPlayer.getCurrentPosition();
                    mPlayer.stop();
                    playImage.setImageDrawable(getResources().getDrawable(R.drawable.play_video));

                }

                lessonDayVideo.start();
                playVideoImage.setVisibility(View.GONE);

                break;

            case R.id.toolbarRelativeBack:

                getActivity().onBackPressed();

                break;

            case R.id.playImage:

                if (lessonDayVideo != null && lessonDayVideo.isPlaying()) {

                    lessonDayVideo.pause();
                }

                if (mPlayer != null && mPlayer.isPlaying()) {
                    lastPos = mPlayer.getCurrentPosition();
                    //mPlayer.stop();
                    mPlayer.pause();
                    //mPlayer.release();
                    playImage.setImageDrawable(getResources().getDrawable(R.drawable.play_video));

                } else {

                    playImage.setImageDrawable(getResources().getDrawable(R.drawable.pause));
                    playAudio();

                }

                break;

            case R.id.pauseImage:

                stopAudio();

                break;

            case R.id.lessonDayButton:

                String val = null;

                if (!optionModelArrayList.isEmpty()) {

                    int positon = -1;

                    for (int i = 0; i < optionModelArrayList.size(); i++) {

                        if (optionModelArrayList.get(i).isSelected() && !optionModelArrayList.get(i).getQuestionType().equalsIgnoreCase("3")) {

                            positon = i;

                            if (val == null) {

                                val = optionModelArrayList.get(i).getValue();

                            } else {

                                val = val + "," + optionModelArrayList.get(i).getValue();

                            }

                        } else if (optionModelArrayList.get(i).getQuestionType().equalsIgnoreCase("3")) {

                            positon = i;

                            if (val == null) {

                                val = optionModelArrayList.get(i).getValue();

                            } else {

                                val = val + "," + optionModelArrayList.get(i).getValue();

                            }

                        }

                    }

                    if (positon == -1) {

                        new CustomAlert().showAlert(getActivity(), 0, "Kindly select any options");
                        return;
                    }

                } else if (lessonEditText.getText().toString().trim().length() == 0) {

                    new CustomAlert().showAlert(getActivity(), 0, "Enter your answer");
                    return;

                } else {

                    val = lessonEditText.getText().toString().trim();

                }

                submitAnswer(val);

                break;

        }

    }

    private void submitAnswer(final String answer) {

        Log.e("tassskidd", lessonId + "/" + answer);
        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Verifying Answer ...", true);
        Service service = Service.retrofit.create(Service.class);
        //https://quizme.com.my/digital_bank/api/v1/lesson/{id}/answer.json
        Call<JsonObject> call = service.taskAnswerResponse(Global.acceptHeader, Global.token, "v1/lesson/" + lessonId.trim() + "/answer.json", answer);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                pd.cancel();

                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                    lessonDayButton.setVisibility(View.GONE);

                    try {

                        String res = response.body().toString();
                        Log.e("answerres", res);

                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.optString("status").equalsIgnoreCase("success")) {

                            String point = "0";

                            if (preferences.getString("point", "") != null && !preferences.getString("point", "").equalsIgnoreCase("null")) {

                                point = String.valueOf(Integer.valueOf(preferences.getString("point", "")) + Integer.valueOf(jsonObject.optString("earn_points")));

                            } else {

                                point = jsonObject.optString("earn_points");

                            }

                            Log.e("uppoint", point);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("point", point);
                            editor.apply();

                            // lessonEditText.setText("");
                            // lessonEditText.setHint("Enter your answer here");

                            new CustomAlert().showAlertWithAnimation(getActivity(), "Congratulations", "Your answer is correct.\nYou have earned " + jsonObject.optString("earn_points") + " Points.\nNow your total points are " + point + ".");

                            //getNextQuestion();

                        } else if (!answer.trim().isEmpty() || !answer.equalsIgnoreCase(" ")) {

                            new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    private Runnable UpdateSongTime = new Runnable() {

        public void run() {

            if (value) {

                startTime = mPlayer.getCurrentPosition();
                finalTime = mPlayer.getDuration();

                startTimeText.setText(String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                        TimeUnit.MILLISECONDS.toSeconds((long) startTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) startTime))));

                if (firstTime == 0) {

                    seekbar.setMax((int) finalTime);
                    firstTime = 1;

                }

                seekbar.setProgress((int) startTime);
                seekbar.setProgress((int) startTime);
                myHandler.postDelayed(this, 100);

            }

        }

    };

}
