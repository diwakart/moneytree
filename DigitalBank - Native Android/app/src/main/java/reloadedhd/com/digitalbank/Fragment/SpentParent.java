package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.SpentParentAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.SpentParentModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SpentParent extends Fragment implements View.OnClickListener {
    RecyclerView recyclerView_spentParent;
    LinearLayoutManager linearLayoutManager;
    RelativeLayout relLayout_back_button_spentParent;
    private Bundle b;
    private String id;
    private ArrayList<SpentParentModel> arrayList = new ArrayList<>();
    private TextView noSpentData;
    private SpentParentAdapter spentParentAdapter;
    private ShimmerFrameLayout spentChildShimmer;

    public SpentParent() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spent_parent, container, false);

        recyclerView_spentParent = view.findViewById(R.id.recyclerView_spentParent);
        relLayout_back_button_spentParent = view.findViewById(R.id.relLayout_back_button_spentParent);
        noSpentData = view.findViewById(R.id.noSpentData);

        spentChildShimmer = view.findViewById(R.id.spentChildShimmer);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_spentParent.setLayoutManager(linearLayoutManager);

        spentParentAdapter = new SpentParentAdapter(getActivity(), arrayList);
        recyclerView_spentParent.setAdapter(spentParentAdapter);

        arrayList.clear();
        b = getArguments();

        if (b != null) {

            getSpendList(b.getString("id"));

        }

        relLayout_back_button_spentParent.setOnClickListener(this);

        return view;
    }

    void getSpendList(String id) {

        spentChildShimmer.startShimmer();
        spentChildShimmer.setVisibility(View.VISIBLE);
      //  final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Log.e("spentlsss", id + "//");
        Service service = Service.retrofit.create(Service.class);
        //Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user/spent_list.json");
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/child/" + id + "/spentlist.json");
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                spentChildShimmer.setVisibility(View.GONE);
                spentChildShimmer.stopShimmer();
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                try {

                    //pd.cancel();
                    if (response.isSuccessful()) {

                        String res = response.body().toString();
                        Log.e("spentdata", res);

                        JSONObject jsonObject = new JSONObject(res);
                        JSONArray array = jsonObject.optJSONArray("spent_data");

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);
                            SpentParentModel model = new SpentParentModel();
                            model.setProdcut_image(object.optString("prodcut_image"));
                            model.setProduct_name(object.optString("product_name"));
                            model.setSpent_points(object.optString("spent_points"));
                            model.setSpent_time(object.optString("spent_time"));
                            arrayList.add(model);

                        }

                        if (!arrayList.isEmpty()) {

                            spentParentAdapter.notifyDataSetChanged();
                            recyclerView_spentParent.setVisibility(View.VISIBLE);
                            noSpentData.setVisibility(View.GONE);

                        } else {

                            recyclerView_spentParent.setVisibility(View.GONE);
                            noSpentData.setVisibility(View.VISIBLE);

                        }

                    } else {

                        recyclerView_spentParent.setVisibility(View.GONE);
                        noSpentData.setVisibility(View.VISIBLE);

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

              //  pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");
            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.relLayout_back_button_spentParent:

                getActivity().onBackPressed();

                break;

        }

    }

}
