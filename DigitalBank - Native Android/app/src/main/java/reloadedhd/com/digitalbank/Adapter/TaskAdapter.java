package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.model.TaskModel;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskHolder> {
    private Context context;
    private ArrayList<TaskModel> arrayList;
    TaskClick click;

    public TaskAdapter(Context context, ArrayList<TaskModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public TaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.task_holder, parent, false);
        TaskHolder viewHolder = new TaskHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TaskHolder holder, int position) {

        if (arrayList.get(position).getCategory_name() != null) {

            holder.taskCategoryName.setText("Category : " + arrayList.get(position).getCategory_name());

        } else {

            holder.taskCategoryName.setVisibility(View.GONE);

        }

        holder.taskQuestion.setText(arrayList.get(position).getQuestion());

        if (arrayList.get(position).getCategory_boosted().equalsIgnoreCase("1")) {

            holder.taskAdapterLinear.setAlpha(0.30f);
            holder.selectedTaskImage.setVisibility(View.VISIBLE);

        } else {

            holder.taskAdapterLinear.setAlpha(1.0f);
            holder.selectedTaskImage.setVisibility(View.GONE);

        }

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    class TaskHolder extends RecyclerView.ViewHolder {

        TextView taskCategoryName, taskQuestion;
        LinearLayout taskAdapterLinear;
        ImageView selectedTaskImage;

        public TaskHolder(View itemView) {
            super(itemView);
            taskCategoryName = itemView.findViewById(R.id.taskCategoryName);
            taskQuestion = itemView.findViewById(R.id.taskQuestion);
            taskAdapterLinear = itemView.findViewById(R.id.taskAdapterLinear);
            selectedTaskImage = itemView.findViewById(R.id.selectedTaskImage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    click.onTaskClick(getAdapterPosition());

                }

            });

        }

    }

    public void onTaskClickListener(TaskClick click) {

        this.click = click;

    }

    public interface TaskClick {

        void onTaskClick(int pos);

    }

}
