package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.EarnedParentAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.EarnedParentModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EarnedParent extends Fragment {
    RecyclerView recyclerView_earnedParent;
    LinearLayoutManager linearLayoutManager;
    RelativeLayout relLayout_back_button_earnedParent;
    private TextView noEarnedData;
    private Bundle b;
    private String id;
    private ArrayList<EarnedParentModel> arrayList;
    private EarnedParentAdapter adapter;
    private ShimmerFrameLayout earnChildShimmer;

    public EarnedParent() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_earned_parent, container, false);

        recyclerView_earnedParent = view.findViewById(R.id.recyclerView_earnedParent);
        relLayout_back_button_earnedParent = view.findViewById(R.id.relLayout_back_button_earnedParent);
        noEarnedData = view.findViewById(R.id.noEarnedData);

        earnChildShimmer = view.findViewById(R.id.earnChildShimmer);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_earnedParent.setLayoutManager(linearLayoutManager);
        arrayList = new ArrayList<>();
        arrayList.clear();
        adapter = new EarnedParentAdapter(getActivity(), arrayList);
        recyclerView_earnedParent.setAdapter(adapter);
        b = getArguments();

        if (b != null) {
            id = b.getString("id");
            getEarnedData();

        }

        relLayout_back_button_earnedParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getActivity().onBackPressed();

            }
        });

        return view;

    }

    private void getEarnedData() {

        earnChildShimmer.setVisibility(View.VISIBLE);
        earnChildShimmer.startShimmer();
        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);

        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, ServiceURLs.GETCHILDDETAIL + id + "/earnlist.json");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {
                earnChildShimmer.setVisibility(View.GONE);
                earnChildShimmer.stopShimmer();
                String res = response.body().toString();
                Log.e("wishlist", res);

                try {

                    JSONObject jsonObject = new JSONObject(res);
                    JSONArray array = jsonObject.optJSONArray("earn_data");

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.optJSONObject(i);
                        EarnedParentModel model = new EarnedParentModel();
                        model.setCategory_name(object.optString("category_name"));
                        model.setEarn_points(object.optString("earn_points"));
                        model.setQuestion(object.optString("question"));
                        model.setEarn_point_time(object.optString("earn_point_time"));
                        arrayList.add(model);

                    }

                    if (!arrayList.isEmpty()) {

                        adapter.notifyDataSetChanged();
                        noEarnedData.setVisibility(View.GONE);
                        recyclerView_earnedParent.setVisibility(View.VISIBLE);
                    } else {
                        recyclerView_earnedParent.setVisibility(View.GONE);
                        noEarnedData.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

                //pd.cancel();

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                new CustomAlert().showAlert(getActivity(), 0, "Failure");

                //Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                //pd.cancel();

            }

        });

    }

}
