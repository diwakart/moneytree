package reloadedhd.com.digitalbank.Fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.ChildWishlistAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.ChildWishlistModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import reloadedhd.com.digitalbank.transform.CircleTransform;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Wishlist extends Fragment implements View.OnClickListener {

    private RelativeLayout relLayout_back_button_wishlist;
    private RecyclerView childWishList;
    private TextView addItemsWishlist, noWishlistData, wishlistTotalPoints, wishlistAvailablePoints, wishlistPointsNeed;
    private CardView childWishlistBottomLinear;
    private ArrayList<ChildWishlistModel> arrayList;
    private ChildWishlistAdapter adapter;
    private LinearLayoutManager manager;
    private boolean deleteProduct = false;
    private int totalPoints = 0, totalItems = 0,currentPage=1;
    private float totalPages;
    private SharedPreferences preferences;
    private ShimmerFrameLayout wishlistShimmer;

    public Wishlist() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wishlist, container, false);
        preferences = DigitalBankContoller.getGlobalPreferences();
        initView(view);

        arrayList = new ArrayList<>();
        arrayList.clear();

        manager = new GridLayoutManager(getActivity(), 2);
        childWishList.setLayoutManager(manager);
        adapter = new ChildWishlistAdapter(getActivity(), arrayList);
        childWishList.setAdapter(adapter);

        adapter.onChildWishClickListener(new ChildWishlistAdapter.ChildWishClick() {
            @Override
            public void onDeleteClick(final int pos) {

                Log.e("str", "delclick");
                TextView alertMessage, alertOk, alertCancel;
                ImageView customAlertImage;
                final Dialog dialog = new Dialog(getActivity());
                View v = LayoutInflater.from(getActivity()).inflate(R.layout.show_custom_alert, null);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertMessage = v.findViewById(R.id.alertMessage);
                alertOk = v.findViewById(R.id.alertOk);
                alertCancel = v.findViewById(R.id.alertCancel);
                customAlertImage = v.findViewById(R.id.customAlertImage);
                Picasso.with(getActivity()).load(ServiceURLs.PRODUCTIMAGEBASEURL + arrayList.get(pos).getProduct_image()).transform(new CircleTransform(20, 0)).fit().into(customAlertImage);
                dialog.setContentView(v);

                alertMessage.setText(arrayList.get(pos).getProduct_name() + "\n\n" + "Are you sure to delete this product from your wishlist?");
                dialog.show();

                alertOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.cancel();
                        Log.e("str", "okk");
                        deleteProduct = true;
                        getDeleteProduct(pos);

                    }
                });

                alertCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.cancel();

                    }
                });

            }
        });

        getWishList();

        this.manager = new GridLayoutManager(getActivity(), 2) {

            @Override
            public boolean canScrollVertically() {
                return true;
            }

        };

        childWishList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == 0 && totalPages > (float) currentPage) {

                    currentPage++;
                    getWishList();

                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }

        });

        return view;

    }

    private void getDeleteProduct(int pos) {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user/removewishlist.json?product_id=" + arrayList.get(pos).getProduct_id());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();

                pd.cancel();
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {
                String res = response.body().toString();
                Log.e("addPrdtRes", res);

                try {

                    JSONObject jsonObject = new JSONObject(res);
                    Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                    getWishList();

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                //Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT);
                new CustomAlert().showAlert(getActivity(), 0, "Failure");
            }

        });
    }

    private void initView(View view) {

        wishlistShimmer = view.findViewById(R.id.wishlistShimmer);
        wishlistPointsNeed = view.findViewById(R.id.wishlistPointsNeed);
        wishlistAvailablePoints = view.findViewById(R.id.wishlistAvailablePoints);
        wishlistTotalPoints = view.findViewById(R.id.wishlistTotalPoints);
        relLayout_back_button_wishlist = view.findViewById(R.id.relLayout_back_button_wishlist);
        addItemsWishlist = view.findViewById(R.id.addItemsWishlist);
        childWishList = view.findViewById(R.id.childWishList);
        noWishlistData = view.findViewById(R.id.noWishlistData);
        childWishlistBottomLinear = view.findViewById(R.id.childWishlistBottomLinear);
        wishlistShimmer.startShimmer();
        relLayout_back_button_wishlist.setOnClickListener(this);
        addItemsWishlist.setOnClickListener(this);

    }

    public void getWishList() {

        if (deleteProduct) {
            deleteProduct = false;
            arrayList.clear();
        }

        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user/wishlistproducts.json?perpage=5&current_page=" + currentPage);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                // pd.cancel();
                wishlistShimmer.stopShimmer();
                wishlistShimmer.setVisibility(View.GONE);

                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {
                String res = response.body().toString();
                Log.e("ressss", res);

                try {

                    JSONObject jsonObject = new JSONObject(res);
                    JSONArray array = jsonObject.optJSONArray("data");
                    totalPoints = 0;
                    totalItems = 0;
                    JSONObject jsonObject1 = jsonObject.optJSONObject("meta");
                    totalPages = Float.valueOf(Float.valueOf(jsonObject1.optString("total")) / Float.valueOf(jsonObject1.optString("perpage")));

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.optJSONObject(i);
                        ChildWishlistModel model = new ChildWishlistModel();
                        model.setId(object.optString("id"));
                        model.setProduct_id(object.optString("product_id"));
                        model.setProduct_image(object.optString("product_image"));
                        model.setProduct_name(object.optString("product_name"));
                        model.setProduct_price(object.optString("product_price"));
                        model.setRedeem_price(object.optString("rdm_price"));
                        Log.e("rerfrfer", object.optString("rdm_price"));
                        totalPoints = (totalPoints + Integer.valueOf(object.optString("product_price")));

                        arrayList.add(model);

                    }

                    if (!arrayList.isEmpty()) {
                        totalItems = arrayList.size();
                        childWishList.setVisibility(View.VISIBLE);
                        childWishlistBottomLinear.setVisibility(View.VISIBLE);
                        noWishlistData.setVisibility(View.GONE);
                        adapter.notifyDataSetChanged();
                        wishlistTotalPoints.setText(String.valueOf(totalPoints) + " Points");

                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("wishlistpoints", String.valueOf(totalPoints));
                        editor.putString("wishlist", String.valueOf(totalItems));
                        editor.commit();

                        wishlistAvailablePoints.setText(preferences.getString("point", "") + " Points");


                        if (Integer.valueOf(preferences.getString("point", "")) >= totalPoints) {

                            wishlistPointsNeed.setText("0 Points");

                        } else {

                            wishlistPointsNeed.setText(String.valueOf(totalPoints - Integer.valueOf(preferences.getString("point", ""))) + " Points");

                        }

                    } else {

                        childWishList.setVisibility(View.GONE);
                        childWishlistBottomLinear.setVisibility(View.GONE);
                        noWishlistData.setVisibility(View.VISIBLE);

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //  pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.relLayout_back_button_wishlist:

                Global.changeFragment(getActivity(), new Dashboard());

                break;

            case R.id.addItemsWishlist:

                Global.changeFragment(getActivity(), new ChildProductList());

                break;

        }

    }

}
