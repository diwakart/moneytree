package reloadedhd.com.digitalbank.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.global.DigitalGlobal;
import reloadedhd.com.digitalbank.model.Country;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*todo for registration for child as well as parent*/
public class RegisterScreen extends Activity implements View.OnClickListener {

    private TextView login_txt_signup, txt_terms_cond_reg, userNameAvailability, checkUserNameAvailability, passwordShow;
    boolean check_email;
    CheckBox checkBox_termsCond_register;
    EditText user_Name_register, email_register, mobile_register, password_register, full_name_register;
    RadioButton radioButton_child_login, radioButton_parent_login;
    private Button registerButton;
    String type = "0";
    private Spinner countrySpinner;
    private ArrayList<Country> countryList = new ArrayList<>();
    private ArrayList<String> countrySpinnerList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_screen);

        login_txt_signup = findViewById(R.id.login_txt_signup);
        registerButton = findViewById(R.id.registerButton);
        checkBox_termsCond_register = findViewById(R.id.checkBox_termsCond_register);

        countrySpinner = findViewById(R.id.countrySpinner);

        full_name_register = findViewById(R.id.full_name_register);
        user_Name_register = findViewById(R.id.user_Name_register);
        email_register = findViewById(R.id.email_register);
        mobile_register = findViewById(R.id.mobile_register);
        password_register = findViewById(R.id.password_register);
        passwordShow = findViewById(R.id.passwordShow);

        userNameAvailability = findViewById(R.id.userNameAvailability);
        checkUserNameAvailability = findViewById(R.id.checkUserNameAvailability);

        txt_terms_cond_reg = findViewById(R.id.txt_terms_cond_reg);

        radioButton_child_login = findViewById(R.id.radioButton_child_login);
        radioButton_parent_login = findViewById(R.id.radioButton_parent_login);

        SpannableString ss = new SpannableString("I agree to terms and policies.");

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent service = new Intent(RegisterScreen.this, TermsConditionActivity.class);
                service.putExtra("val", "2");
                startActivity(service);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent service = new Intent(RegisterScreen.this, TermsConditionActivity.class);
                service.putExtra("val", "1");
                startActivity(service);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }

        };

        ss.setSpan(clickableSpan, 11, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan1, 21, 29, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        txt_terms_cond_reg.setText(ss);
        txt_terms_cond_reg.setMovementMethod(LinkMovementMethod.getInstance());
        txt_terms_cond_reg.setHighlightColor(Color.RED);

        radioButton_child_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "2";
                mobile_register.setError(null);
                radioButton_child_login.setChecked(true);
                radioButton_parent_login.setChecked(false);

            }
        });

        radioButton_parent_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "3";
                radioButton_child_login.setChecked(false);
                radioButton_parent_login.setChecked(true);
            }
        });

        passwordShow.setOnClickListener(this);
        registerButton.setOnClickListener(this);

        login_txt_signup.setOnClickListener(this);
        checkUserNameAvailability.setOnClickListener(this);

        user_Name_register.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent keyevent) {
                //If the keyevent is a key-down event on the "enter" button
                if ((keyevent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    //...
                    // Perform your action on key press here
                    //...
                    return true;

                }

                return false;

            }

        });

        user_Name_register.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    Log.e("enterkey", "okkk");
                    if (user_Name_register.getText().toString().trim().length() == 0) {
                        user_Name_register.setFocusable(true);
                        user_Name_register.setError("Please enter the username");
                        return false;

                    } else if (user_Name_register.getText().toString().trim().length() < 4) {
                        user_Name_register.setFocusable(true);
                        user_Name_register.setError("Username must be at least 4 characters");
                        return false;

                    }
                    new DigitalGlobal().hideKeyboard(RegisterScreen.this);

                    checkUserNameAvailability(user_Name_register.getText().toString().trim());

                    return true;
                }

                return false;
            }
        });

        /*todo for get country list*/
        //getCountryList();

    }

    private boolean isValidMail(String email) {

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (email.matches(EMAIL_STRING)) {
            check_email = true;
        } else {
            check_email = false;
        }

        return check_email;
    }

    private void getCountryList() {

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, ServiceURLs.COUNTRYLIST);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful()) {

                    String res = response.body().toString();

                    try {

                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                            countrySpinnerList.clear();
                            countryList.clear();
                            JSONArray array = jsonObject.optJSONArray("data");

                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object = array.optJSONObject(i);
                                Country model = new Country();
                                model.setCountryCode(object.optString("country_code"));
                                model.setCountryId(object.optString("id"));
                                model.setCountryName(object.optString("country_name"));
                                countryList.add(model);
                                countrySpinnerList.add(object.optString("country_name"));

                            }

                            if (!countryList.isEmpty()) {

                                countrySpinnerList.set(0, "Select Country");
                                ArrayAdapter<String> adapter = new ArrayAdapter<>(RegisterScreen.this, R.layout.spinner, countrySpinnerList);
                                countrySpinner.setAdapter(adapter);

                            }

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.login_txt_signup:

                startActivity(new Intent(RegisterScreen.this, LoginScreen.class));
                finish();

                break;

            case R.id.checkUserNameAvailability:

                Log.e("userava", "ok");

                if (user_Name_register.getText().toString().trim().length() == 0) {

                    user_Name_register.setError("Please enter the username");
                    return;

                } else if (user_Name_register.getText().toString().trim().length() < 4) {

                    user_Name_register.setError("Username must be at least 4 characters");
                    return;

                }

                checkUserNameAvailability(user_Name_register.getText().toString().trim());

                break;

            case R.id.registerButton:

                new DigitalGlobal().hideKeyboard(RegisterScreen.this);
                isValidMail(email_register.getText().toString().trim());

                String user_name = user_Name_register.getText().toString().trim();
                String email = email_register.getText().toString().trim();
                String mobile = mobile_register.getText().toString().trim();
                String password = password_register.getText().toString().trim();

                if (!email.equalsIgnoreCase("")) {

                    if (!check_email) {
                        email_register.setError("Invalid Email");
                        return;
                    }

                }

                if (full_name_register.getText().toString().trim().length() > 0 && full_name_register.getText().toString().trim().length() < 4) {

                    full_name_register.setError("Enter the full name");
                    return;

                } else if (full_name_register.getText().toString().trim().length() > 0 && full_name_register.getText().toString().trim().length() < 4) {

                    full_name_register.setError("Full name must be grater than 4 character");
                    return;

                }

                if (user_name.length() == 0) {

                    user_Name_register.setError("Enter the username");
                    return;

                } else if (user_name.length() < 3) {

                    user_Name_register.setError("Username must be at least 3 characters");
                    return;

                }
/*
                if (countrySpinner.getSelectedItemPosition() == 0) {

                    new CustomAlert().showAlert(RegisterScreen.this, 0, "Select Country");
                    return;

                }*/
                if (mobile.length() > 0) {

                    if (mobile_register.getText().toString().trim().length() < 8 || mobile_register.getText().toString().trim().length() > 16) {

                        mobile_register.setError("Enter the correct mobile");
                        return;

                    }

                }
                if (password_register.length() < 6) {

                    password_register.setError("Password must be at least 6 characters");
                    return;

                } else {

                    if (!checkBox_termsCond_register.isChecked()) {

                        new CustomAlert().showAlert(RegisterScreen.this, 0, "Accept terms and conditions");
                        return;

                    } else if (radioButton_child_login.isChecked()) {

                        type = "2";

                    } else if (radioButton_parent_login.isChecked()) {

                        type = "3";

                    } else {

                        new CustomAlert().showAlert(RegisterScreen.this, 0, "Select child or parent type");
                        return;

                    }
                }
                if (type.equalsIgnoreCase("3")) {
                    if (mobile_register.getText().toString().trim().length() == 0) {
                        mobile_register.setError("Enter the mobile");
                        return;
                    } else if (mobile_register.getText().toString().trim().length() < 8 || mobile_register.getText().toString().trim().length() > 16) {
                        mobile_register.setError("Enter the correct mobile");
                        return;
                    }

                }

                Log.e("tryyt", type);
                getRegister(type);

                break;

            case R.id.passwordShow:
                if (passwordShow.getText().toString().trim().equalsIgnoreCase("HIDE")) {
                    passwordShow.setText("SHOW");
                    //  edt_password_login.setTransformationMethod(new PasswordTransformationMethod());
                    password_register.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else if (passwordShow.getText().toString().trim().equalsIgnoreCase("SHOW")) {
                    passwordShow.setText("HIDE");
                    // edt_password_login.setTransformationMethod(null);
                    password_register.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }

                break;

        }

    }

    private void getRegister(String type) {

        Log.e("trtnj", type + "/");
        final ProgressDialog pd = ProgressDialog.show(this, "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        HashMap<String, RequestBody> body = new HashMap<>();

        body.put("user_name", RequestBody.create(MediaType.parse("multipart/form-data"), user_Name_register.getText().toString().trim()));

        if (!email_register.getText().toString().trim().equalsIgnoreCase("")) {

            body.put("email", RequestBody.create(MediaType.parse("multipart/form-data"), email_register.getText().toString().trim()));

        }

        body.put("password", RequestBody.create(MediaType.parse("multipart/form-data"), password_register.getText().toString().trim()));
     //   body.put("country_id", RequestBody.create(MediaType.parse("multipart/form-data"), countryList.get(countrySpinner.getSelectedItemPosition()).getCountryId()));
        //body.put("country_id", RequestBody.create(MediaType.parse("multipart/form-data"), countryList.get(countrySpinner.getSelectedItemPosition()-1).getCountryId()));

        if (mobile_register.getText().toString().trim().length() > 0) {

            body.put("mobile", RequestBody.create(MediaType.parse("multipart/form-data"), mobile_register.getText().toString().trim()));
           // body.put("country_code", RequestBody.create(MediaType.parse("multipart/form-data"), countryList.get(countrySpinner.getSelectedItemPosition()).getCountryCode()));
            //body.put("country_code", RequestBody.create(MediaType.parse("multipart/form-data"), countryList.get(countrySpinner.getSelectedItemPosition() - 1).getCountryCode()));

        }

        if (full_name_register.getText().toString().trim().length() > 0) {

            body.put("name", RequestBody.create(MediaType.parse("multipart/form-data"), full_name_register.getText().toString().trim()));

        }

        body.put("type", RequestBody.create(MediaType.parse("multipart/form-data"), type));

        Call<JsonObject> call = service.getPostResponseWithoutHeader("register", body);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();

                try {

                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    Log.e("Fsgedf", jsonObject.toString() + "");
                    String status = jsonObject.optString("status").toString();

                    if (status.equalsIgnoreCase("success")) {

                        Toast.makeText(RegisterScreen.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(RegisterScreen.this, LoginScreen.class);
                        startActivity(intent);
                        RegisterScreen.this.finish();

                    } else if (status.equalsIgnoreCase("error")) {

                        new CustomAlert().showAlert(RegisterScreen.this, 0, jsonObject.optString("message"));

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                new CustomAlert().showAlert(RegisterScreen.this, 0, "Failure");

            }

        });

    }

    private void checkUserNameAvailability(String names) {

        final ProgressDialog pd = ProgressDialog.show(RegisterScreen.this, "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_name", RequestBody.create(MediaType.parse("multipart/form-data"), names));
        Call<JsonObject> call = service.getPostResponseWithoutHeader("check_availability.json", body);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();

                if (response.isSuccessful()) {

                    String res = response.body().toString();
                    Log.e("rtess", res + "/");

                    try {

                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.optString("status").equalsIgnoreCase("success")) {

                            userNameAvailability.setTextColor(getResources().getColor(R.color.colorGreen));
                            userNameAvailability.setText(jsonObject.optString("message"));

                        } else {

                            userNameAvailability.setTextColor(getResources().getColor(R.color.colorAccent));
                            userNameAvailability.setText(jsonObject.optString("message"));


                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                } else {

                    new CustomAlert().showAlert(RegisterScreen.this, 0, "There are some error occur");

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                new CustomAlert().showAlert(RegisterScreen.this, 0, "There are some error occur");

            }

        });

    }

}
