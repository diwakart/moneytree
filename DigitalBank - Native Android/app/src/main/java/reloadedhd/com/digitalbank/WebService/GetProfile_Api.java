package reloadedhd.com.digitalbank.WebService;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import reloadedhd.com.digitalbank.Activity.MainActivity2;
import reloadedhd.com.digitalbank.Fragment.ParentProfile;
import reloadedhd.com.digitalbank.Fragment.ParentScreen;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 6/6/2018.
 */

public class GetProfile_Api {
    Context context;
    String acceptHeader, token;
    ProgressDialog progressDialog;
    ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
    SharedPreferences preferences;

    public void getProfile(final Context context, String acceptHeader, String token) {
        this.context = context;
        this.acceptHeader = acceptHeader;
        this.token = token;

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        preferences = DigitalBankContoller.getGlobalPreferences();

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getProfile(acceptHeader, token);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();

                String res = response.isSuccessful() + "";
                if (res.equalsIgnoreCase("true")) {
                    try {

                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("success")) {
                            String message = jsonObject.getString("message");

                            //-----------------------parent-------------------------------------------
                            if (message.equalsIgnoreCase("Parent Details")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int p = 0; p < jsonArray.length(); p++) {
                                    HashMap<String, String> hashMap = new HashMap<>();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(p);
                                    String name = jsonObject1.getString("name").toString();
                                    String mobile = jsonObject1.getString("mobile").toString();
                                    String avtar = jsonObject1.getString("avtar").toString();

                                    hashMap.put("name", name);
                                    hashMap.put("mobile", mobile);
                                    hashMap.put("avtar", avtar);
                                    arrayList.add(hashMap);


                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("name", jsonObject1.optString("name"));
                                    editor.putString("mobile", jsonObject1.optString("mobile"));
                                    editor.putString("avtar", jsonObject1.optString("avtar"));
                                    if (jsonObject1.has("email")) {
                                        editor.putString("email", jsonObject1.optString("email"));
                                    }

                                    editor.commit();

                                }

                            }
                            //-----------------------child---------------------------------------------
                            else {

                            }

                            Global.getArrayList_getProfile().clear();
                            Global.setArrayList_getProfile(arrayList);


                            Intent intent = new Intent(context, MainActivity2.class);
                            context.startActivity(intent);
                            ((Activity) context).finish();

                        } else {
                            Toast.makeText(context, response.message() + "", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, response.message() + "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
