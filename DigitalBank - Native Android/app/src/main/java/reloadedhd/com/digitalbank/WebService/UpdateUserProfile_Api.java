package reloadedhd.com.digitalbank.WebService;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import reloadedhd.com.digitalbank.Utility.Global;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 5/29/2018.
 */

public class UpdateUserProfile_Api {
    Context context;
    String acceptHeader,edt_contact_userProfile,edt_username_userProfile,token,email;
    ProgressDialog progressDialog;
    RequestBody imagefile1;
    public void updateUserProfile(FragmentActivity activity, String acceptHeader, String token, String edt_username_userProfile, String edt_contact_userProfile,String email) {
        this.context=activity;
        this.acceptHeader=acceptHeader;
        this.token=token;
        this.edt_username_userProfile=edt_username_userProfile;
        this.edt_contact_userProfile=edt_contact_userProfile;
        this.email=email;

        progressDialog = new ProgressDialog(context);
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call ;
        if (email ==null || email.length()==0 || email.equalsIgnoreCase("N/A")){
            call = service.updateUserProfileOne(acceptHeader,token,edt_username_userProfile,edt_contact_userProfile);
        }else {
            call = service.updateUserProfile(acceptHeader,token,edt_username_userProfile,edt_contact_userProfile,email);
        }

         call.enqueue(new Callback<JsonObject>() {
             @Override
             public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                 progressDialog.dismiss();

                 String res = response.isSuccessful()+"";
                 if (res.equalsIgnoreCase("true"))
                 {

                     try {

                     JSONObject jsonObject = new JSONObject(response.body().toString());
                     String status = jsonObject.get("status").toString();

                    if (status.equalsIgnoreCase("success"))
                     {
                         String message = jsonObject.get("message").toString();
                         Toast.makeText(context,message+ "", Toast.LENGTH_SHORT).show();

                         GetProfile_Api getProfile_api = new GetProfile_Api();
                         getProfile_api.getProfile(context, Global.acceptHeader,Global.token);
                     }
                     else
                     {


                         JSONObject jsonObject1 = jsonObject.getJSONObject("message");
                         JSONArray jsonArray = jsonObject1.getJSONArray("email");
                         Toast.makeText(context,jsonArray.getString(0) + "", Toast.LENGTH_SHORT).show();

                     }

                 } catch (JSONException e) {
                     e.printStackTrace();
                 }

                 }
                 else
                 {
                     Toast.makeText(context,response.message()+ "", Toast.LENGTH_SHORT).show();
                 }
             }

             @Override
             public void onFailure(Call<JsonObject> call, Throwable t) {
                 progressDialog.dismiss();
                 Toast.makeText(context,  "Failure", Toast.LENGTH_SHORT).show();
             }
         });
    }
}
