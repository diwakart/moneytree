package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Activity.RegisterScreen;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.Country;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReferFriend extends Fragment implements View.OnClickListener {
    private EditText referUserName, referUserMobile, referUserEmail;
    private Button refer;
    Spinner refercountrySpinner;
    private ArrayList<Country> refercountryList = new ArrayList<>();
    private ArrayList<String> refercountrySpinnerList = new ArrayList<>();

    //todo for toolbar
    private RelativeLayout toolbarRelativeBack;
    private TextView toolbarTitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.refer_friend, container, false);

        initView(v);
        //getCountryList();
        return v;

    }

    private void initView(View v) {

        referUserName = v.findViewById(R.id.referUserName);
        refercountrySpinner = v.findViewById(R.id.refer_countrySpinner);
        referUserMobile = v.findViewById(R.id.referUserMobile);
        referUserEmail = v.findViewById(R.id.referUserEmail);
        refer = v.findViewById(R.id.refer);

        toolbarRelativeBack = v.findViewById(R.id.toolbarRelativeBack);
        toolbarTitle = v.findViewById(R.id.toolbarTitle);
        toolbarTitle.setText("Refer Friend");
        refer.setOnClickListener(this);
        toolbarRelativeBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.toolbarRelativeBack:

                getActivity().onBackPressed();

                break;

            case R.id.refer:

                if (!validateName()) {

                    return;

                }

                if (referUserMobile.getText().toString().trim().length() != 0) {

                  /*if(refercountrySpinner.getSelectedItemPosition() == 0) {
                        new CustomAlert().showAlert(getActivity(), 0, "Select Country");
                        return;
                    }
                    else*/
                    if (!validateMobile()) {

                        return;

                    }

                } else if (referUserEmail.getText().toString().trim().length() != 0) {

                    if (!validateEmail()) {

                        return;

                    }

                } else {

                    new CustomAlert().showAlert(getActivity(), 0, "To refer either enter the phone number or email");

                    return;

                }

                referFriend();

                break;

        }

    }

    private void referFriend() {
        //name:required,email,mobile
        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("name", RequestBody.create(MediaType.parse("multipart/form-data"), referUserName.getText().toString().trim()));
        if (referUserEmail.getText().toString().trim().length() > 0) {
            body.put("email", RequestBody.create(MediaType.parse("multipart/form-data"), referUserEmail.getText().toString().trim()));
        }
        if (referUserMobile.getText().toString().trim().length() > 0) {
            body.put("mobile", RequestBody.create(MediaType.parse("multipart/form-data"), referUserMobile.getText().toString().trim()));
            //body.put("country_code", RequestBody.create(MediaType.parse("multipart/form-data"), refercountryList.get(refercountrySpinner.getSelectedItemPosition()).getCountryCode()));
            //Log.d("ctry_code",refercountryList.get(refercountrySpinner.getSelectedItemPosition()).getCountryCode()+" ");
        }

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getPostResponse(Global.acceptHeader, Global.token, "v1/user/refer_friend.json", body);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                pd.cancel();
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                    String res = response.body().toString();

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                            new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));


                        } else {

                            new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");
            }

        });

    }

    private boolean validateName() {

        if (referUserName.getText().toString().trim().length() == 0) {
            new CustomAlert().showAlert(getActivity(), 0, "Enter the full name");
            return false;
        } else if (referUserName.getText().toString().trim().length() <= 4) {
            new CustomAlert().showAlert(getActivity(), 0, "Enter more than 4 character");
            return false;
        } else {
            return true;
        }

    }

    private boolean validateMobile() {
        if (referUserMobile.getText().toString().trim().length() == 0) {
            // new CustomAlert().showAlert(getActivity(),0,"Enter the phone number");
            return false;
        } else if (referUserMobile.getText().toString().trim().length() <= 8 && referUserMobile.getText().toString().trim().length() >= 16) {
            new CustomAlert().showAlert(getActivity(), 0, "Enter correct phone number");
            return false;
        } else {
            return true;
        }

    }

    private boolean validateEmail() {

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (referUserEmail.getText().toString().trim().length() == 0) {

            //new CustomAlert().showAlert(getActivity(),0,"Enter the phone number");
            return false;

        } else if (referUserEmail.getText().toString().trim().matches(EMAIL_STRING)) {


            return true;

        } else {
            new CustomAlert().showAlert(getActivity(), 0, "Enter correct email");
            return false;

        }

    }

    private void getCountryList() {

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, ServiceURLs.COUNTRYLIST);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                    if (response.isSuccessful()) {

                        String res = response.body().toString();

                        try {

                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                                refercountrySpinnerList.clear();
                                refercountryList.clear();
                                JSONArray array = jsonObject.optJSONArray("data");

                                for (int i = 0; i < array.length(); i++) {

                                    JSONObject object = array.optJSONObject(i);
                                    Country model = new Country();
                                    model.setCountryCode(object.optString("country_code"));
                                    model.setCountryId(object.optString("id"));
                                    model.setCountryName(object.optString("country_name"));
                                    refercountryList.add(model);
                                    refercountrySpinnerList.add(object.optString("country_name"));

                                }

                                if (!refercountryList.isEmpty()) {

                                    refercountrySpinnerList.set(0, "Select Country");
                                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner, refercountrySpinnerList);
                                    refercountrySpinner.setAdapter(adapter);

                                }

                            }

                        } catch (JSONException e) {

                            e.printStackTrace();

                        }

                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

}
