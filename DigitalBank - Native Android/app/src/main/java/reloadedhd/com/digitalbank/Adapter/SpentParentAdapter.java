package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.model.SpentParentModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;

public class SpentParentAdapter extends RecyclerView.Adapter<SpentParentAdapter.ViewHolder> {
    private Context context;
    private ArrayList<SpentParentModel> arrayList;

    public SpentParentAdapter(Context context, ArrayList<SpentParentModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public SpentParentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.spent_parent_adapter, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final SpentParentAdapter.ViewHolder holder, int position) {
        holder.spentProductName.setText(arrayList.get(position).getProduct_name());
        holder.spentProductPoints.setText(arrayList.get(position).getSpent_points() + " Points");

        if (arrayList.get(position).getProdcut_image() != null && !arrayList.get(position).getProdcut_image().equalsIgnoreCase(" ")) {
            ImageLoader imageLoader = ImageLoader.getInstance();

            imageLoader.displayImage(ServiceURLs.PRODUCTIMAGEBASEURL + arrayList.get(position).getProdcut_image(), holder.spentAdapterImage, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    Log.e("imgstrt", "ok/" + imageUri);

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.spentAdapterImage.setImageBitmap(loadedImage);

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });

        }
        Date date = null;
        String outputString = null;

        try {

            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(arrayList.get(position).getSpent_time());

        } catch (ParseException e) {

            e.printStackTrace();

        }

        outputString = new SimpleDateFormat("dd-MM-yyyy").format(date);
        String newString = new SimpleDateFormat("HH:mm").format(date);

        holder.spentProductDate.setText("Transaction Date : " + outputString + "\nTime : " + newString);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView spentAdapterImage;
        TextView spentProductName, spentProductPoints,spentProductDate;

        public ViewHolder(View itemView) {
            super(itemView);
            spentAdapterImage = itemView.findViewById(R.id.spentAdapterImage);
            spentProductName = itemView.findViewById(R.id.spentProductName);
            spentProductPoints = itemView.findViewById(R.id.spentProductPoints);
            spentProductDate = itemView.findViewById(R.id.spentProductDate);

        }

    }

}
