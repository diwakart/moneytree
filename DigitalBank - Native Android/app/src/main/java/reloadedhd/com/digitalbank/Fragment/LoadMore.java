package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Activity.MainActivity2;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.global.DigitalGlobal;
import reloadedhd.com.digitalbank.model.PackageModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoadMore extends Fragment implements View.OnClickListener {
    Button loadMoreCancel;
    RelativeLayout relLayout_back_loadMore;
    TextView availablepoints_loadmore_number, balancepoints_loadmore_number;
    Spinner spinner_parentPlan_loadMore;
    String amounntTotal;
    private Button img_transfer_loadMore;
    private ArrayList<PackageModel> arrayList;
    private ArrayList<String> stringList = new ArrayList<>();
    private ArrayAdapter<String> dataAdapter;
    private ImageView loadMoreParentImage;
    private SharedPreferences preferences;
    private TextView loadMoreParentName, parentPointIconText;

    public LoadMore() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_load_more, container, false);

        arrayList = new ArrayList<>();
        preferences = DigitalBankContoller.getGlobalPreferences();
        initView(view);
        stringList.clear();
        stringList.add(0, "Parent Plans");

        dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, stringList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_parentPlan_loadMore.setAdapter(dataAdapter);

        spinner_parentPlan_loadMore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String val = adapterView.getItemAtPosition(i).toString();
                ((TextView) adapterView.getChildAt(0)).setTextSize(19);
                ((TextView) adapterView.getChildAt(0)).setTypeface(Typeface.DEFAULT_BOLD);

                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorBlack));

                if (val.equalsIgnoreCase("Parent Plans")) {

                    String valAvail = availablepoints_loadmore_number.getText().toString();
                    balancepoints_loadmore_number.setText(valAvail);

                } else {

                    String valAvail = String.valueOf(Integer.valueOf(availablepoints_loadmore_number.getText().toString()) + Integer.valueOf(arrayList.get(spinner_parentPlan_loadMore.getSelectedItemPosition() - 1).getPoints()));
                    balancepoints_loadmore_number.setText(valAvail);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });

        relLayout_back_loadMore.setOnClickListener(this);
        img_transfer_loadMore.setOnClickListener(this);
        loadMoreCancel.setOnClickListener(this);

        getPackages();

        return view;

    }

    private void initView(View view) {

        loadMoreCancel = view.findViewById(R.id.loadMoreCancel);
        loadMoreParentName = view.findViewById(R.id.loadMoreParentName);
        loadMoreParentImage = view.findViewById(R.id.loadMoreParentImage);
        img_transfer_loadMore = view.findViewById(R.id.img_transfer_loadMore);
        relLayout_back_loadMore = view.findViewById(R.id.relLayout_back_loadMore);
        availablepoints_loadmore_number = view.findViewById(R.id.availablepoints_loadmore_number);
        spinner_parentPlan_loadMore = view.findViewById(R.id.spinner_parentPlan_loadMore);
        balancepoints_loadmore_number = view.findViewById(R.id.balancepoints_loadmore_number);
        parentPointIconText = view.findViewById(R.id.parentPointIconText);

        if (preferences.contains("bitmap") && preferences.getString("bitmap", "") != null && !preferences.getString("bitmap", "").equalsIgnoreCase("null")) {

            loadMoreParentImage.setImageBitmap(new DigitalGlobal().StringToBitMap(preferences.getString("bitmap", "")));

        }

        if (preferences.contains("name") && !preferences.getString("name", "").equalsIgnoreCase("null")) {

            loadMoreParentName.setText(preferences.getString("name", ""));

        }

        availablepoints_loadmore_number.setText(preferences.getString("point", ""));
        balancepoints_loadmore_number.setText(preferences.getString("point", ""));
        parentPointIconText.setText(preferences.getString("point", "") + "\nPoints");

    }

    public void getPackages() {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);

        Service service = Service.retrofit.create(Service.class);

        //will modify according to the requirement
        Call<JsonObject> call = service.getPackage(Global.acceptHeader, Global.token);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.code()==401){

                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                String res = response.body().toString();
                Log.e("parentpackage", res);
                pd.cancel();

                try {

                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has("data")) {

                        JSONArray array = jsonObject.optJSONArray("data");
                        arrayList.clear();

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);
                            PackageModel model = new PackageModel();
                            model.setId(object.optString("id"));
                            model.setS_No(object.optString("S_No"));
                            model.setAmount(object.optString("amount"));
                            model.setName(object.optString("name"));
                            model.setPoints(object.optString("points"));
                            stringList.add("Plan Name : " + object.optString("name") + "  " + object.optString("amount") + "$/" + object.optString("points") + " Points");
                            arrayList.add(model);

                        }

                        if (!arrayList.isEmpty()) {

                            dataAdapter.notifyDataSetChanged();
                            img_transfer_loadMore.setVisibility(View.VISIBLE);

                        } else {

                            img_transfer_loadMore.setVisibility(View.GONE);

                        }


                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();

            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.loadMoreCancel:

                MainActivity2.rel_header.setVisibility(View.GONE);
                getActivity().onBackPressed();
                break;

            case R.id.relLayout_back_loadMore:

                MainActivity2.rel_header.setVisibility(View.GONE);
                getActivity().onBackPressed();

                break;

            case R.id.img_transfer_loadMore:
                if (arrayList.isEmpty()) {
                    Toast.makeText(getActivity(), "Don't have any plan", Toast.LENGTH_SHORT).show();
                    return;
                }
                amounntTotal = balancepoints_loadmore_number.getText().toString();
                if (amounntTotal.equalsIgnoreCase("Parent Plans")) {

                    Toast.makeText(getActivity(), "Select Plans", Toast.LENGTH_SHORT).show();

                } else if (spinner_parentPlan_loadMore.getSelectedItemPosition() > 0) {

                    MainActivity2.rel_header.setVisibility(View.GONE);

                    PaymentGatewayStripe paymentGatewayStripe = new PaymentGatewayStripe();
                    Bundle bundle = new Bundle();
                    bundle.putString("amount", arrayList.get(spinner_parentPlan_loadMore.getSelectedItemPosition() - 1).getId());
                    bundle.putString("amountPoint", arrayList.get(spinner_parentPlan_loadMore.getSelectedItemPosition() - 1).getPoints());
                    paymentGatewayStripe.setArguments(bundle);
                    Global.changeFragment(getActivity(), paymentGatewayStripe);

                } else {

                    Toast.makeText(getActivity(), "Select any Plan", Toast.LENGTH_SHORT).show();

                }

                break;

        }

    }

}
