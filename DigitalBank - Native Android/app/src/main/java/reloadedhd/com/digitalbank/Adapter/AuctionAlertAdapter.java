package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.model.AuctionAlertModel;

public class AuctionAlertAdapter extends RecyclerView.Adapter<AuctionAlertAdapter.AuctionHolder> {
    private Context context;
    private ArrayList<AuctionAlertModel> arrayList;
    AuctionAlertClick click;

    public AuctionAlertAdapter(Context context, ArrayList<AuctionAlertModel> arrayList) {

        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public AuctionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.auction_alert_adapter, parent, false);
        return new AuctionHolder(v);
    }

    @Override
    public void onBindViewHolder(AuctionHolder holder, int position) {

        holder.alertAdapterText.setText(arrayList.get(position).getProductName());

        if (arrayList.get(position).isSelected()) {
            holder.alertAdapterTick.setVisibility(View.VISIBLE);
        } else {
            holder.alertAdapterTick.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class AuctionHolder extends RecyclerView.ViewHolder {
        ImageView alertAdapterTick;
        TextView alertAdapterText;

        public AuctionHolder(View itemView) {
            super(itemView);
            alertAdapterText = itemView.findViewById(R.id.alertAdapterText);
            alertAdapterTick = itemView.findViewById(R.id.alertAdapterTick);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click.setPriority(getAdapterPosition());
                }
            });

        }

    }

    public void setOnPriorityListener(AuctionAlertClick click) {
        this.click = click;
    }

    public interface AuctionAlertClick {

        void setPriority(int pos);
    }

}
