package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.model.BiddingModel;

public class BiddingListAdapter extends RecyclerView.Adapter<BiddingListAdapter.BiddingHolder> {
    private Context context;
    private ArrayList<BiddingModel> arrayList;

    public BiddingListAdapter(Context context, ArrayList<BiddingModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public BiddingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.bidding_list_adapter, parent, false);
        return new BiddingHolder(v);
    }

    @Override
    public void onBindViewHolder(BiddingHolder holder, int position) {
        holder.bidAdapterAuctionName.setText(arrayList.get(position).getAuction_name());
        holder.bidAdapterAuctionBidValue.setText(arrayList.get(position).getBid_value());
        Date date = null;
        String outputString = null;

        try {

            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(arrayList.get(position).getBid_time());

        } catch (ParseException e) {

            e.printStackTrace();

        }

        outputString = new SimpleDateFormat("dd-MM-yyyy").format(date);
        String newString = new SimpleDateFormat("HH:mm").format(date);

        holder.bidAdapterAuctionBidDate.setText(outputString);
        holder.bidAdapterAuctionBidTime.setText(newString);
        //holder.bidAdapterAuctionBidDate.setText(arrayList.get(position).getBid_time());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class BiddingHolder extends RecyclerView.ViewHolder {
        TextView bidAdapterAuctionName, bidAdapterAuctionBidValue, bidAdapterAuctionBidDate, bidAdapterAuctionBidTime;

        public BiddingHolder(View itemView) {
            super(itemView);
            bidAdapterAuctionName = itemView.findViewById(R.id.bidAdapterAuctionName);
            bidAdapterAuctionBidValue = itemView.findViewById(R.id.bidAdapterAuctionBidValue);
            bidAdapterAuctionBidDate = itemView.findViewById(R.id.bidAdapterAuctionBidDate);
            bidAdapterAuctionBidTime = itemView.findViewById(R.id.bidAdapterAuctionBidTime);
        }
    }

}
