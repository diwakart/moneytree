package reloadedhd.com.digitalbank.model;

import java.io.Serializable;

public class AuctionListModel implements Serializable {

    private String auction_id;
    private String category_name;
    private String auction_price;
    private String single_bid_value;
    private String current_auction_value;
    private String start_time;
    private String end_time;
    private String incremented_bid_time;
    private String bid_hash;
    private String user_participated;
    private String product_name;
    private String product_image;

    public String getLast_bidder() {
        return last_bidder;
    }

    public void setLast_bidder(String last_bidder) {
        this.last_bidder = last_bidder;
    }

    private String last_bidder;

    public String getIncremented_bid_time() {
        return incremented_bid_time;
    }

    public void setIncremented_bid_time(String incremented_bid_time) {
        this.incremented_bid_time = incremented_bid_time;
    }

    public String getBid_hash() {
        return bid_hash;
    }

    public void setBid_hash(String bid_hash) {
        this.bid_hash = bid_hash;
    }

    public String getUser_participated() {
        return user_participated;
    }

    public void setUser_participated(String user_participated) {
        this.user_participated = user_participated;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getAuction_id() {
        return auction_id;
    }

    public void setAuction_id(String auction_id) {
        this.auction_id = auction_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getAuction_price() {
        return auction_price;
    }

    public void setAuction_price(String auction_price) {
        this.auction_price = auction_price;
    }

    public String getSingle_bid_value() {
        return single_bid_value;
    }

    public void setSingle_bid_value(String single_bid_value) {
        this.single_bid_value = single_bid_value;
    }

    public String getCurrent_auction_value() {
        return current_auction_value;
    }

    public void setCurrent_auction_value(String current_auction_value) {
        this.current_auction_value = current_auction_value;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

}
