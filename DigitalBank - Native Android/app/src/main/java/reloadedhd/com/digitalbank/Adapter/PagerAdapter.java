package reloadedhd.com.digitalbank.Adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import reloadedhd.com.digitalbank.Fragment.AuctionHistory;
import reloadedhd.com.digitalbank.Fragment.ChildEarnedHistory;
import reloadedhd.com.digitalbank.Fragment.ChildSpentHistory;

public class PagerAdapter extends FragmentPagerAdapter {

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {

            case 0:
                return new ChildEarnedHistory();
            case 1:
                return new ChildSpentHistory();
            case 2:
                return new AuctionHistory();
            default:
                return null;

        }

    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {

            case 0:
                return "Earned History";

            case 1:
                return "Spent History";
            case 2:
                return "Auction History";
            default:
                return "History";

        }

    }

}
