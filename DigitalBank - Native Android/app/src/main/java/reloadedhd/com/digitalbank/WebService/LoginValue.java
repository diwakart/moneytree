package reloadedhd.com.digitalbank.WebService;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 5/25/2018.
 */

public class LoginValue
{
    @SerializedName("success")
    @Expose
    private Success success;
    @SerializedName("data")
    @Expose
    private List<Example> data = null;

    public Success getSuccess() {
        return success;
    }

    public void setSuccess(Success success) {
        this.success = success;
    }

    public List<Example> getData() {
        return data;
    }

    public void setData(List<Example> data) {
        this.data = data;
    }

}

 class Example {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("parent_id")
    @Expose
    private Integer parentId;
    @SerializedName("avtar")
    @Expose
    private Object avtar;
    @SerializedName("account_type")
    @Expose
    private String accountType;
    @SerializedName("current_point")
    @Expose
    private String currentPoint;
    @SerializedName("type")
    @Expose
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

     public String getMobile() {
         return mobile;
     }

     public void setMobile(String mobile) {
         this.mobile = mobile;
     }

     public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Object getAvtar() {
        return avtar;
    }

    public void setAvtar(Object avtar) {
        this.avtar = avtar;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getCurrentPoint() {
        return currentPoint;
    }

    public void setCurrentPoint(String currentPoint) {
        this.currentPoint = currentPoint;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}

 class Success {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("message")
    @Expose
    private String message;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
