package reloadedhd.com.digitalbank.WebService;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.JsonObject;
import org.json.JSONException;
import org.json.JSONObject;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword_Api {
    Context context;
    String forgotPassword;
    ProgressDialog progressDialog;

    public void forgotPass(LoginScreen loginScreen, String forgotPassword) {
        this.context=loginScreen;
        this.forgotPassword=forgotPassword;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.forgotPassword(forgotPassword,"123456");
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();

                Log.e("serfdf",response.isSuccessful()+"");
                String msgIsSuccess = response.isSuccessful()+"";
                if (msgIsSuccess.equalsIgnoreCase("true"))
                {
                try {

                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    String status = jsonObject.get("status").toString();
                    String message = jsonObject.get("message").toString();
                    Toast.makeText(context,  message+"", Toast.LENGTH_SHORT).show();
                    Log.e("serfdf",jsonObject+"");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
               progressDialog.dismiss();
                Toast.makeText(context,  "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
