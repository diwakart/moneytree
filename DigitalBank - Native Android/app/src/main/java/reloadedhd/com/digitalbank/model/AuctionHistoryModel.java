package reloadedhd.com.digitalbank.model;

public class AuctionHistoryModel {

    private String auction_id;
    private String category_name;
    private String auction_price;
    private String single_bid_value;
    private String incremented_bid_time;
    private String current_auction_value;
    private String start_time;
    private String end_time;
    private String bid_hash;
    private String auction_winner;

    public String getAuctionProductName() {
        return auctionProductName;
    }

    public void setAuctionProductName(String auctionProductName) {
        this.auctionProductName = auctionProductName;
    }

    private String auctionProductName;

    public String getAuctionImage() {
        return auctionImage;
    }

    public void setAuctionImage(String auctionImage) {
        this.auctionImage = auctionImage;
    }

    private String auctionImage;

    public String getAuction_id() {
        return auction_id;
    }

    public void setAuction_id(String auction_id) {
        this.auction_id = auction_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getAuction_price() {
        return auction_price;
    }

    public void setAuction_price(String auction_price) {
        this.auction_price = auction_price;
    }

    public String getSingle_bid_value() {
        return single_bid_value;
    }

    public void setSingle_bid_value(String single_bid_value) {
        this.single_bid_value = single_bid_value;
    }

    public String getIncremented_bid_time() {
        return incremented_bid_time;
    }

    public void setIncremented_bid_time(String incremented_bid_time) {
        this.incremented_bid_time = incremented_bid_time;
    }

    public String getCurrent_auction_value() {
        return current_auction_value;
    }

    public void setCurrent_auction_value(String current_auction_value) {
        this.current_auction_value = current_auction_value;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getBid_hash() {
        return bid_hash;
    }

    public void setBid_hash(String bid_hash) {
        this.bid_hash = bid_hash;
    }

    public String getAuction_winner() {
        return auction_winner;
    }

    public void setAuction_winner(String auction_winner) {
        this.auction_winner = auction_winner;
    }

}
