package reloadedhd.com.digitalbank.WebService;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import reloadedhd.com.digitalbank.Activity.MainActivity2;
import reloadedhd.com.digitalbank.Fragment.ParentScreen;
import reloadedhd.com.digitalbank.Utility.Global;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 6/9/2018.
 */

public class InviteChild_Api {
    Context context;
    String user_id;
    ProgressDialog progressDialog;

    public void inviteChild(FragmentActivity activity, String user_id) {
        this.context = activity;
        this.user_id = user_id;

        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.inviteChild(Global.acceptHeader, Global.token, user_id/*,"1"*/);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();

                    try {
                        String res = response.body().toString();
                        Log.e("cld", res);
                        JSONObject jsonObject = new JSONObject(res);

                        boolean check = jsonObject.toString().contains("status");
                        if (check) {
                            String status = jsonObject.getString("status");
                            if (status.equalsIgnoreCase("success")) {
                                MainActivity2.rel_header.setVisibility(View.VISIBLE);
                                String message = jsonObject.get("message").toString();
                                Toast.makeText(context, message + "", Toast.LENGTH_SHORT).show();
                                Global.changeFragment(context, new ParentScreen());

                            } else {

                                String message = jsonObject.get("message").toString();
                                Toast.makeText(context, message + "", Toast.LENGTH_SHORT).show();

                            }
                        } else {

                            String statusError = jsonObject.getString("error");
                            if (statusError.equalsIgnoreCase("success")) {
                                MainActivity2.rel_header.setVisibility(View.VISIBLE);
                                String message = jsonObject.get("message").toString();
                                Toast.makeText(context, message + "", Toast.LENGTH_SHORT).show();
                                Global.changeFragment(context, new ParentScreen());
                            }
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();

            }

        });

    }

}
