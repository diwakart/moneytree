package reloadedhd.com.digitalbank.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.AuctionHistoryAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.AuctionHistoryModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuctionHistory extends Fragment {

    private RecyclerView auctionHistoryList;
    private TextView auctionHistoryNodata;
    private LinearLayoutManager manager;
    private AuctionHistoryAdapter adapter;
    private ArrayList<AuctionHistoryModel> arrayList;
    private ShimmerFrameLayout historyShimmer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.auction_history, container, false);
        arrayList = new ArrayList();
        arrayList.clear();
        initView(v);
        return v;
    }

    private void initView(View v) {

        auctionHistoryNodata = v.findViewById(R.id.auctionHistoryNodata);
        auctionHistoryList = v.findViewById(R.id.auctionHistoryList);
        historyShimmer = v.findViewById(R.id.historyShimmer);
        manager = new LinearLayoutManager(getActivity());
        auctionHistoryList.setLayoutManager(manager);
        adapter = new AuctionHistoryAdapter(getActivity(), arrayList);
        auctionHistoryList.setAdapter(adapter);
        adapter.setOnHistoryClick(new AuctionHistoryAdapter.AuctionHistoryClickListener() {
            @Override
            public void onAuctionHistoryCLick(int pos) {
                BiddingList list = new BiddingList();
                Bundle bundle = new Bundle();
                bundle.putString("auctionId", arrayList.get(pos).getAuction_id());
                list.setArguments(bundle);
                Global.changeFragment(getActivity(), list);
            }
        });

        getAuctionHistory();

    }

    private void getAuctionHistory() {

        historyShimmer.startShimmer();
        historyShimmer.setVisibility(View.VISIBLE);
        //  final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);

        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user/auction_history.json");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                historyShimmer.stopShimmer();
                historyShimmer.setVisibility(View.GONE);

                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                //pd.cancel();

                try {

                    if (response.isSuccessful()) {

                        String res = response.body().toString();
                        Log.e("res", res);
                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                            JSONArray array = jsonObject.optJSONArray("data");

                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object = array.optJSONObject(i);
                                AuctionHistoryModel model = new AuctionHistoryModel();
                                model.setAuction_id(object.optString("auction_id"));
                                model.setAuction_price(object.optString("auction_price"));
                                model.setAuction_winner(object.optString("auction_winner"));
                                model.setBid_hash(object.optString("bid_hash"));
                                model.setCategory_name(object.optString("category_name"));
                                model.setCurrent_auction_value(object.optString("current_auction_value"));
                                model.setEnd_time(object.optString("end_time"));
                                model.setIncremented_bid_time(object.optString("incremented_bid_time"));
                                model.setSingle_bid_value(object.optString("single_bid_value"));
                                model.setStart_time(object.optString("start_time"));
                                model.setAuctionImage(object.optString("product_image"));
                                model.setAuctionProductName(object.optString("product_name"));
                                arrayList.add(model);

                            }

                            if (!arrayList.isEmpty()) {

                                adapter.notifyDataSetChanged();
                                auctionHistoryList.setVisibility(View.VISIBLE);
                                auctionHistoryNodata.setVisibility(View.GONE);

                            } else {

                                auctionHistoryNodata.setVisibility(View.VISIBLE);
                                auctionHistoryList.setVisibility(View.GONE);

                            }

                        }
                        else {

                            auctionHistoryList.setVisibility(View.GONE);
                            auctionHistoryNodata.setVisibility(View.VISIBLE);

                        }

                    } else {

                        auctionHistoryList.setVisibility(View.GONE);
                        auctionHistoryNodata.setVisibility(View.VISIBLE);

                    }

                } catch (Exception e) {

                    e.printStackTrace();

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                // pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

}
