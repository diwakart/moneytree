package reloadedhd.com.digitalbank.Fragment;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;

public class Quiz extends Fragment {
    private WebView quizView;
    SharedPreferences preferences;
    private String url = "http://www.quizme.com.my/quiz/login/";
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.quiz, container, false);
        preferences = DigitalBankContoller.getGlobalPreferences();
        initView(v);

        return v;

    }

    private void initView(View v) {

        url = url + preferences.getString("digital_key", "");
        Log.e("quizurl", url + "/");
        quizView = v.findViewById(R.id.quizView);
        progressBar = v.findViewById(R.id.progressBar);
        quizView.setWebViewClient(new WebViewClient());
        quizView.getSettings().setLoadsImagesAutomatically(true);
        quizView.getSettings().setJavaScriptEnabled(true);
        quizView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        quizView.loadUrl(url);

        if (quizView.canGoBack()) {

            quizView.goBack();

        }

        quizView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                //todo for checking the url
                Log.e("backurl", url + "/");
                if (url.equalsIgnoreCase("https://quizme.com.my/quiz/app/exit")) {

                    getActivity().onBackPressed();

                } else {

                    view.loadUrl(url);

                }


                return true;

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.e("on pg started", url);
                progressBar.setVisibility(View.VISIBLE);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
            }

        });

    }

}
