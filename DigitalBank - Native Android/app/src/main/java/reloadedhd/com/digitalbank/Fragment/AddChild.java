package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.InviteChild_Api;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddChild extends Fragment {
    RelativeLayout relLayout_back_button_addChild;
    TextView txt_invite_addCild;
    ImageView search_img_addChild, searchChildImage;
    EditText search_edt_addChild;
    TextView childName_addChild_txt, mailId_addChild_txt, mobileNum_addChild_txt, dob_addChild_txt, search_addChild;
    LinearLayout linLay_childDetaild_addChild, emailLinear, addChildLinearDOB;
    private String id = null;

    public AddChild() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_child, container, false);

        relLayout_back_button_addChild = view.findViewById(R.id.relLayout_back_button_addChild);
        txt_invite_addCild = view.findViewById(R.id.txt_invite_addCild);
        search_img_addChild = view.findViewById(R.id.search_img_addChild);
        search_edt_addChild = view.findViewById(R.id.search_edt_addChild);
        childName_addChild_txt = view.findViewById(R.id.childName_addChild_txt);
        mailId_addChild_txt = view.findViewById(R.id.mailId_addChild_txt);
        mobileNum_addChild_txt = view.findViewById(R.id.mobileNum_addChild_txt);
        dob_addChild_txt = view.findViewById(R.id.dob_addChild_txt);
        linLay_childDetaild_addChild = view.findViewById(R.id.linLay_childDetaild_addChild);
        searchChildImage = view.findViewById(R.id.searchChildImage);
        search_addChild = view.findViewById(R.id.search_addChild);

        emailLinear = view.findViewById(R.id.emailLinear);
        addChildLinearDOB = view.findViewById(R.id.addChildLinearDOB);

        txt_invite_addCild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InviteChild_Api inviteChild_api = new InviteChild_Api();
                inviteChild_api.inviteChild(getActivity(), id);

            }
        });

        search_addChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobile = search_edt_addChild.getText().toString();

                if (mobile.trim().length() == 0) {

                    new CustomAlert().showAlert(getActivity(), 0, "Enter mobile phone number");

                } else if (mobile.trim().length() <= 6) {

                    new CustomAlert().showAlert(getActivity(), 0, "Enter the correct phone number");

                } else {

                    getSearchResult(mobile);

                }
            }
        });

        search_img_addChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mobile = search_edt_addChild.getText().toString();

                if (mobile.trim().length() == 0) {

                    new CustomAlert().showAlert(getActivity(), 0, "Enter mobile phone number");

                } else if (mobile.trim().length() <= 6) {

                    new CustomAlert().showAlert(getActivity(), 0, "Enter the correct phone number");

                } else {

                    getSearchResult(mobile);

                }

            }
        });

        relLayout_back_button_addChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getActivity().onBackPressed();

            }
        });

        return view;

    }

    void getSearchResult(String phoneNumber) {
        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.searchChild(Global.acceptHeader, Global.token, phoneNumber, "2");
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();

                if (response.code() == 401) {

                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else if (response.isSuccessful()) {

                    try {

                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        Log.e("fsgbvsfgvc", jsonObject.toString());
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("success")) {
                            linLay_childDetaild_addChild.setVisibility(View.VISIBLE);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                            id = jsonObject1.get("id").toString();
                            String name = jsonObject1.get("name").toString();
                            String mobile = jsonObject1.get("mobile").toString();
                            String email = jsonObject1.get("email").toString();
                            String dob = jsonObject1.get("dob").toString();
                            if (jsonObject1.optString("parent_id").equalsIgnoreCase("0")) {
                                txt_invite_addCild.setVisibility(View.VISIBLE);
                            } else {
                                txt_invite_addCild.setVisibility(View.GONE);
                            }

                            childName_addChild_txt.setText(name);

                            if (email != null && !email.trim().equalsIgnoreCase("") && !email.trim().equalsIgnoreCase("null")) {
                                mailId_addChild_txt.setText(email);
                            } else {
                                mailId_addChild_txt.setText("N/A");
                            }

                            mobileNum_addChild_txt.setText(mobile);
                            if (dob != null && !dob.equalsIgnoreCase("") && !dob.trim().equalsIgnoreCase("null")) {
                                dob_addChild_txt.setText(dob);
                                addChildLinearDOB.setVisibility(View.VISIBLE);
                            } else {
                                addChildLinearDOB.setVisibility(View.GONE);
                            }

                            if (jsonObject1.optString("avtar") != null && !jsonObject1.optString("avtar").equalsIgnoreCase("null")) {

                                Picasso.with(getActivity()).load(ServiceURLs.USERPROFILEIMAGEBASEURL + jsonObject1.optString("avtar")).fit().into(searchChildImage);

                            }

                        } else {

                            String message = jsonObject.getString("message");  // here handle search by mobile by enter number and text
                            //Toast.makeText(getActivity(), "No data Found", Toast.LENGTH_SHORT).show();

                            new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                } else {

                    //Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                    new CustomAlert().showAlert(getActivity(), 0, "There are some error occur");
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.cancel();
                //Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

}
