package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Activity.MainActivity2;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountDetail extends Fragment implements View.OnClickListener {
    private CardView accountDetailTaskCard, accountDetailLessonCard;
    private RelativeLayout toolbarRelativeBack;
    private TextView toolbarTitle, accountAvailablePoints, accountBufferPoints, accountRewardPoints;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.account_detail, container, false);
        initView(v);

        return v;

    }

    private void initView(View v) {

        toolbarRelativeBack = v.findViewById(R.id.toolbarRelativeBack);
        toolbarTitle = v.findViewById(R.id.toolbarTitle);

        accountDetailTaskCard = v.findViewById(R.id.accountDetailTaskCard);
        accountDetailLessonCard = v.findViewById(R.id.accountDetailLessonCard);

        accountBufferPoints = v.findViewById(R.id.accountBufferPoints);
        accountRewardPoints = v.findViewById(R.id.accountRewardPoints);
        accountAvailablePoints = v.findViewById(R.id.accountAvailablePoints);

        toolbarTitle.setText("Account Detail");

        accountDetailTaskCard.setOnClickListener(this);
        accountDetailLessonCard.setOnClickListener(this);
        toolbarRelativeBack.setOnClickListener(this);
        getAccountDetail();

    }

    private void getAccountDetail() {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Loading ...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, ServiceURLs.ACCOUNTDETAIL);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                pd.cancel();

                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();
                }else {


                if (response.isSuccessful()) {
                    String res = response.body().toString();
                    Log.e("res", res + "/");

                    try {

                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.optString("status").equalsIgnoreCase("success")) {

                            JSONArray array = jsonObject.optJSONArray("data");

                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object=array.optJSONObject(i);
                                accountBufferPoints.setText(object.optString("buffer_point"));
                                accountRewardPoints.setText(object.optString("pool_balance"));
                                accountAvailablePoints.setText(object.optString("current_point"));

                            }

                        }
                        else {

                            new CustomAlert().showAlert(getActivity(),0,jsonObject.optString("message"));

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                }
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();

            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.accountDetailLessonCard:

                MainActivity2.rel_header.setVisibility(View.GONE);
                Global.changeFragment(getActivity(), new LessonOfDay());

                break;

            case R.id.accountDetailTaskCard:

                MainActivity2.rel_header.setVisibility(View.GONE);
                Global.changeFragment(getActivity(), new ListOfTask());

                break;

            case R.id.toolbarRelativeBack:

                getActivity().onBackPressed();

                break;

        }

    }

}
