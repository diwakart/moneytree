package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Activity.MainActivity2;
import reloadedhd.com.digitalbank.Adapter.ListOfTaskAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.CategoryModel;
import reloadedhd.com.digitalbank.model.TaskModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListOfTask extends Fragment implements View.OnClickListener {
    private RecyclerView recyclerView_list_of_task;
    private LinearLayoutManager linearLayoutManager;
    private RelativeLayout relLayout_back_button_list_of_task;
    private ListOfTaskAdapter taskAdapter;
    private TextView taskListNoData;
    private Spinner taskCategorySpinner;
    private ArrayList<CategoryModel> categoryModelArrayList;
    private ArrayList<String> spinnerList;
    private ArrayAdapter<String> spinnerAdapter;
    private ArrayList<TaskModel> taskModelArrayList;
    public static int catId = 0;
    private SharedPreferences preferences;
    private ShimmerFrameLayout taskListShimmer;
    private int currentPage = 1, totalItemCount, lastVisibleItem, pos = 0, totalPage;
    private boolean loading = false;

    public ListOfTask() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_of_task, container, false);
        preferences = DigitalBankContoller.getGlobalPreferences();

        initView(view);

        return view;
    }

    //todo for initview
    private void initView(View v) {

        recyclerView_list_of_task = v.findViewById(R.id.recyclerView_list_of_task);
        relLayout_back_button_list_of_task = v.findViewById(R.id.relLayout_back_button_list_of_task);
        taskListNoData = v.findViewById(R.id.taskListNoData);
        taskCategorySpinner = v.findViewById(R.id.taskCategorySpinner);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_list_of_task.setLayoutManager(linearLayoutManager);

        taskListShimmer = v.findViewById(R.id.taskListShimmer);

        relLayout_back_button_list_of_task.setOnClickListener(this);
        categoryModelArrayList = new ArrayList<>();

        taskModelArrayList = new ArrayList<>();

        taskAdapter = new ListOfTaskAdapter(getActivity(), taskModelArrayList);
        recyclerView_list_of_task.setAdapter(taskAdapter);

        taskAdapter.setOnTaskClickListener(new ListOfTaskAdapter.OnTaskClick() {
            @Override
            public void onTaskClick(int pos) {

                Log.e("taskClick", pos + "");
                getTaskDetail(taskModelArrayList.get(pos).getId());

            }

        });

        spinnerList = new ArrayList<>();
        spinnerList.add(0, "Select Category");
        spinnerAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner, spinnerList);
        taskCategorySpinner.setAdapter(spinnerAdapter);

        taskCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position > 0) {
                    currentPage = 1;
                    taskModelArrayList.clear();
                    pos = position - 1;
                    taskListShimmer.setVisibility(View.VISIBLE);
                    taskListShimmer.startShimmer();
                    recyclerView_list_of_task.setVisibility(View.GONE);
                    getTaskList(pos);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

        taskModelArrayList.clear();

        recyclerView_list_of_task.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                Log.e("totlitm", totalItemCount + "/" + lastVisibleItem+"//"+dx+"/"+dy);

                if (!loading && totalItemCount == (lastVisibleItem + 1) && dy>0) {

                    currentPage++;
                    Log.e("crntpg", currentPage + "/");
                    loading = true;

                    if (pos > 0) {

                        getTaskList(pos);

                        loading=false;

                    }

                }

            }

        });

        getCategoryList();

    }

    private void getTaskDetail(final String taskId) {
        Log.e("taskId", taskId);

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/tasks/" + taskId + ".json");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                pd.cancel();
                if (response.code() == 401) {

                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                String res = response.body().toString();
                Log.e("categoryList", res);


                try {

                    JSONObject jsonObject = new JSONObject(res);

                    if (!jsonObject.optString("status").equalsIgnoreCase("error")) {

                        TaskDetail detail = new TaskDetail();
                        Bundle b = new Bundle();
                        b.putString("res", res);
                        b.putString("id", taskId);
                        detail.setArguments(b);
                        MainActivity2.changeFragment(getActivity(), detail);

                    } else {

                        //Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                        new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                //Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    //todo for getcategory list of task
    void getCategoryList() {

        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, ServiceURLs.GETCATEGORYLIST);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                String res = response.body().toString();
                Log.e("categoryList", res);
                //pd.cancel();

                try {

                    JSONObject jsonObject = new JSONObject(res);
                    JSONArray array = jsonObject.optJSONArray("data");
                    categoryModelArrayList.clear();

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.optJSONObject(i);

                        CategoryModel model = new CategoryModel();
                        model.setId(object.optString("id"));
                        model.setCategoryName(object.optString("category_name"));
                        spinnerList.add(object.optString("category_name"));
                        categoryModelArrayList.add(model);

                    }

                    if (!categoryModelArrayList.isEmpty()) {

                        if (preferences.getString("catId", "") != null && !preferences.getString("catId", "").isEmpty()) {

                            taskCategorySpinner.setSelection(Integer.valueOf(preferences.getString("catId", "")) + 1);

                        } else {

                            taskCategorySpinner.setSelection(1);

                        }

                        //todo where ever you want to set the category
                        //getTaskList(0);

                    } else {

                        //Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                        new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                    }

                    spinnerAdapter.notifyDataSetChanged();

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //pd.cancel();
                //Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.relLayout_back_button_list_of_task:

                getActivity().onBackPressed();

                break;

        }

    }

    //todo for getTask list on the basis of category
    void getTaskList(int pos) {

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("catId", pos + "");
        editor.commit();

        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);

        Call<JsonObject> call = service.getResponseNormal(Global.acceptHeader, Global.token, "v1/categories/" + categoryModelArrayList.get(pos).getId() + "/tasks.json?" + "perpage=5&current_page=" + currentPage);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                taskListShimmer.stopShimmer();
                taskListShimmer.setVisibility(View.GONE);

                if (response.code() == 401) {

                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {


                String res = response.body().toString();
                Log.e("tasklst", res);
                //pd.cancel();

                try {

                    JSONObject jsonObject = new JSONObject(res);
                    JSONArray array = jsonObject.optJSONArray("data");

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.optJSONObject(i);
                        TaskModel model = new TaskModel();
                        model.setId(object.optString("task_id"));
                        model.setCategory_name(object.optString("category_name"));
                        model.setQuestion(object.optString("question"));
                        model.setQuestion_type(object.optString("question_type"));
                        model.setBonus_point(object.optString("bonus_point"));
                        model.setTask_points(object.optString("task_points"));
                        model.setAttempt_status(object.optString("attempt_status"));
                        model.setTotal_attempts(object.optString("total_attempts"));

                        if (object.has("attemp1_points")) {

                            model.setAttemp1_points(object.optString("attemp1_points"));

                        }

                        if (object.has("attemp2_points")) {

                            model.setAttemp2_points(object.optString("attemp2_points"));

                        }

                        if (object.has("attemp3_points")) {

                            model.setAttemp3_points(object.optString("attemp3_points"));

                        }

                        if (object.has("above3_points")) {

                            model.setAbove3_points(object.optString("above3_points"));

                        }

                        taskModelArrayList.add(model);

                    }

                    if (!taskModelArrayList.isEmpty()) {

                        taskListNoData.setVisibility(View.GONE);
                        recyclerView_list_of_task.setVisibility(View.VISIBLE);
                        taskAdapter.notifyDataSetChanged();

                    } else {

                        taskListNoData.setVisibility(View.VISIBLE);
                        recyclerView_list_of_task.setVisibility(View.GONE);

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                // pd.cancel();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();

            }

        });

    }

}
