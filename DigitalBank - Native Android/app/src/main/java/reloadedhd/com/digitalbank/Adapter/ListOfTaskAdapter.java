package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.model.TaskModel;

/**
 * Created by Admin on 5/24/2018.
 */

public class ListOfTaskAdapter extends RecyclerView.Adapter<ListOfTaskAdapter.ViewHolder> {
    private Context context;
    private ArrayList<TaskModel> arrayList;
    OnTaskClick click;

    public ListOfTaskAdapter(Context activity, ArrayList<TaskModel> arrayList) {
        this.context = activity;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.list_of_task_adpt, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (arrayList.get(position).getAttempt_status().equalsIgnoreCase("2")) {
            holder.taskAnswered.setVisibility(View.VISIBLE);
            //holder.taskAttempCard.setVisibility(View.GONE);
            holder.taskAttemp.setVisibility(View.GONE);
            holder.listofTaskTotalPoints.setVisibility(View.GONE);
            holder.taskAttemp.setClickable(false);
        } else {
            holder.taskAnswered.setVisibility(View.GONE);
            //holder.taskAttempCard.setVisibility(View.VISIBLE);
            holder.taskAttemp.setVisibility(View.VISIBLE);
            holder.listofTaskTotalPoints.setVisibility(View.VISIBLE);
            holder.taskAttemp.setClickable(true);
        }

        holder.listofTaskQuestion.setText(arrayList.get(position).getQuestion());
        if (arrayList.get(position).getQuestion_type().equalsIgnoreCase("1")) {
            holder.listofTaskPoints.setText("Fill in the blanks");
        } else if (arrayList.get(position).getQuestion_type().equalsIgnoreCase("2")) {
            holder.listofTaskPoints.setText("Multiple Choice");
        } else if (arrayList.get(position).getQuestion_type().equalsIgnoreCase("3")) {
            holder.listofTaskPoints.setText("Arrange order");
        }

        if (arrayList.get(position).getAttempt_status().equalsIgnoreCase("1")) {
            int total = Integer.valueOf(arrayList.get(position).getAttemp1_points()) + Integer.valueOf(arrayList.get(position).getBonus_point());
            holder.listofTaskTotalPoints.setText(total + " Points");
        } else if (arrayList.get(position).getAttempt_status().equalsIgnoreCase("2")) {
            int total = Integer.valueOf(arrayList.get(position).getAttemp2_points()) + Integer.valueOf(arrayList.get(position).getBonus_point());
            holder.listofTaskTotalPoints.setText(total + " Points");
        } else if (arrayList.get(position).getAttempt_status().equalsIgnoreCase("3")) {
            int total = Integer.valueOf(arrayList.get(position).getAttemp2_points()) + Integer.valueOf(arrayList.get(position).getBonus_point());
            holder.listofTaskTotalPoints.setText(total + " Points");
        } else {
            int total = Integer.valueOf(arrayList.get(position).getAttemp2_points()) + Integer.valueOf(arrayList.get(position).getBonus_point());
            holder.listofTaskTotalPoints.setText(total + " Points");
        }

        if (Long.valueOf(arrayList.get(position).getBonus_point()) > 0) {

            holder.boostedImage.setVisibility(View.VISIBLE);

        } else {

            holder.boostedImage.setVisibility(View.GONE);

        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView listofTaskQuestion, listofTaskPoints, taskAttemp, listofTaskTotalPoints, taskAnswered;
        ImageView boostedImage;
        //CardView taskAttempCard;

        public ViewHolder(View itemView) {
            super(itemView);

            listofTaskTotalPoints = itemView.findViewById(R.id.listofTaskTotalPoints);
            listofTaskPoints = itemView.findViewById(R.id.listofTaskPoints);
            listofTaskQuestion = itemView.findViewById(R.id.listofTaskQuestion);
            taskAttemp = itemView.findViewById(R.id.taskAttemp);
            boostedImage = itemView.findViewById(R.id.boostedImage);
            //taskAttempCard = itemView.findViewById(R.id.taskAttempCard);
            taskAnswered = itemView.findViewById(R.id.taskAnswered);

            taskAttemp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    click.onTaskClick(getAdapterPosition());

                }

            });

        }

    }

    public void setOnTaskClickListener(OnTaskClick click) {

        this.click = click;

    }

    public interface OnTaskClick {

        void onTaskClick(int pos);

    }

}
