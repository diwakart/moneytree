package reloadedhd.com.digitalbank.Fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Activity.MainActivity2;
import reloadedhd.com.digitalbank.Adapter.WishListImageAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.WishlistImageModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Child Dashboard where a child can see the wishlist items and and their available points also
 */
public class Dashboard extends Fragment implements View.OnClickListener {
    private TextView txtView_earnMore_dashboard, invitationAccept, invitationCancel, invitationMessage, childDashboardPoint,
            dashboardWishlistPoints, dashboardItem, journeryToGoal, childTotalPoints, childDashboardCurrentPoint;
    private String parentId;
    private LinearLayout invitationLinear, childDashboardTopLinear, dashboardWishlist;
    private SharedPreferences preferences, tokenPreference;
    private SeekBar dashboardProgressBar;
    private BarChart chart;
    private RecyclerView wishlistImagesList;
    private ArrayList<WishlistImageModel> arrayList;
    private WishListImageAdapter wishAdapter;
    private LinearLayout lessonDayCard;
    private RelativeLayout DashboardAvailablePoints;

    private ShimmerFrameLayout childDashboardShimmer;

    public Dashboard() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        MainActivity2.rel_header.setVisibility(View.VISIBLE);

        preferences = DigitalBankContoller.getGlobalPreferences();
        tokenPreference = DigitalBankContoller.getTokenPreferences();

        initView(view);
        getInvitation();
        getDashboardData();

        Log.e("okddd", tokenPreference.getString("fcmtoken", "") + "//" + tokenPreference.getString("add", "") + "/" + preferences.getString("user_id", ""));

        if (tokenPreference.getString("fcmtoken", "") != null && !tokenPreference.getString("fcmtoken", "").isEmpty() && !preferences.getString("add", "").equalsIgnoreCase("1")) {

            saveDeviceforNotification();

        }

        return view;

    }

    private void saveDeviceforNotification() {

        Service service = Service.retrofit.create(Service.class);
        //will modify according to the requirement
        String android_id = Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.e("deviceid", android_id + "");
        HashMap<String, RequestBody> body = new HashMap<>();
        //token:required,device_secret:required,device_type:required,user_id:required
        //token:required,device_secret:required,device_type:required
        body.put("token", RequestBody.create(MediaType.parse("multipart/form-data"), tokenPreference.getString("fcmtoken", "")));
        body.put("device_secret", RequestBody.create(MediaType.parse("multipart/form-data"), android_id));
        body.put("device_type", RequestBody.create(MediaType.parse("multipart/form-data"), "1"));
        //body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), preferences.getString("user_id", "")));
        Call<JsonObject> call = service.getPostResponse(Global.acceptHeader, Global.token, ServiceURLs.REGISTERFORNOTIFICATION, body);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();
                } else {
                    if (response.isSuccessful()) {

                        Log.e("res", response.body().toString() + "/");
                        SharedPreferences.Editor editor = tokenPreference.edit();
                        editor.putString("add", "1");
                        editor.commit();

                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
            }

        });

    }

    private void initView(View view) {

        //todo for shimmer layout
        childDashboardShimmer = view.findViewById(R.id.childDashboardShimmer);

        childDashboardTopLinear = view.findViewById(R.id.childDashboardTopLinear);
        lessonDayCard = view.findViewById(R.id.lessonDayCard);
        //todo for chart
        chart = view.findViewById(R.id.chart);
        dashboardProgressBar = view.findViewById(R.id.dashboardProgressBar);
        journeryToGoal = view.findViewById(R.id.journeryToGoal);
        invitationLinear = (LinearLayout) view.findViewById(R.id.invitationLinear);
        invitationMessage = (TextView) view.findViewById(R.id.invitationMessage);
        invitationCancel = (TextView) view.findViewById(R.id.invitationCancel);
        invitationAccept = (TextView) view.findViewById(R.id.invitationAccept);
        dashboardItem = (TextView) view.findViewById(R.id.dashboardItem);
        txtView_earnMore_dashboard = view.findViewById(R.id.txtView_earnMore_dashboard);
        childDashboardPoint = view.findViewById(R.id.childDashboardPoint);
        dashboardWishlistPoints = view.findViewById(R.id.dashboardWishlistPoints);
        dashboardWishlist = view.findViewById(R.id.dashboardWishlist);
        childTotalPoints = view.findViewById(R.id.childTotalPoints);
        childDashboardCurrentPoint = view.findViewById(R.id.childDashboardCurrentPoint);
        wishlistImagesList = view.findViewById(R.id.wishlistImagesList);
        DashboardAvailablePoints = view.findViewById(R.id.DashboardAvailablePoints);
        wishlistImagesList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        arrayList = new ArrayList<>();
        wishAdapter = new WishListImageAdapter(getActivity(), arrayList);
        wishlistImagesList.setAdapter(wishAdapter);
        invitationAccept.setOnClickListener(this);
        invitationCancel.setOnClickListener(this);
        txtView_earnMore_dashboard.setOnClickListener(this);
        //dashboardItem.setOnClickListener(this);
        dashboardWishlist.setOnClickListener(this);
        lessonDayCard.setOnClickListener(this);
        DashboardAvailablePoints.setOnClickListener(this);
        childDashboardShimmer.startShimmer();

        //todo for disable seekbar from user
        // dashboardProgressBar.setEnabled(false);
        dashboardProgressBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

    }

    private void getDashboardData() {

        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/child/dashboard.json");
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                //pd.cancel();
                //childDashboardShimmer.stopShimmerAnimation();

                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();
                } else {

                    Log.e("code", response.code() + "/");
                    if (response.isSuccessful()) {

                        childDashboardShimmer.stopShimmer();
                        childDashboardShimmer.setVisibility(View.GONE);
                        childDashboardTopLinear.setVisibility(View.VISIBLE);
                        String res = response.body().toString();

                        try {

                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                                Log.e("dashbrd", jsonObject.toString());

                                JSONArray array = jsonObject.optJSONArray("data");

                                for (int i = 0; i < array.length(); i++) {

                                    JSONObject object = array.optJSONObject(i);
                                    //childTotalPoints.setText("Available Points : " + ((Integer.valueOf(object.optString("current_balance"))) + (Integer.valueOf(object.optString("buffer_balance")))));
                                    childTotalPoints.setText(object.optString("current_balance"));
                                    childDashboardPoint.setText("Buffer Points : " + object.optString("buffer_balance"));

                                    //childDashboardCurrentPoint.setText("Available Points : " + (Integer.valueOf(object.optString("current_balance"))));
                                    //dashboardWishlist.setText(object.optString("wishlist_items"));

                                    dashboardWishlistPoints.setText(object.optString("total_wishlist_price") + " Points");

                                    SharedPreferences.Editor editor = preferences.edit();

                                    editor.putString("point", object.optString("current_balance"));
                                    editor.commit();

                                    arrayList.clear();
                                    JSONArray array1 = object.optJSONArray("wishlist_products");

                                    for (int j = 0; j < array1.length(); j++) {
                                        JSONObject imageObject = array1.optJSONObject(j);
                                        WishlistImageModel model = new WishlistImageModel();
                                        model.setProduct_image(imageObject.optString("product_image"));
                                        model.setProduct_name(imageObject.optString("product_name"));
                                        arrayList.add(model);
                                    }

                                    if (!arrayList.isEmpty()) {
                                        wishAdapter.notifyDataSetChanged();
                                        wishlistImagesList.setVisibility(View.VISIBLE);

                                    } else {

                                        wishlistImagesList.setVisibility(View.GONE);
                                    }

                                    //todo for show the alert if user has not enter the mobile
                               /* if (object.has("mobile") && object.optString("mobile").equalsIgnoreCase("") || object.optString("mobile").trim().isEmpty() || object.optString("mobile").equalsIgnoreCase("null")) {
                                    showAlertForMobile();
                                }*/

                                    int val = (int) (Float.valueOf(object.optString("current_balance")) / Float.valueOf(object.optString("total_wishlist_price")) * 100);
                                    Log.e("lflf", val + "//");
                                    JSONObject lastObject = object.optJSONObject("last_week");
                                    JSONObject currentObject = object.optJSONObject("current_week");
                                    JSONObject previousObject = object.optJSONObject("previous_week");

                                    ArrayList<BarDataSet> dataSets = null;
                                    ArrayList<BarEntry> valueSet1 = new ArrayList<>();
                                    BarEntry v1e1 = new BarEntry(Integer.valueOf(currentObject.optString("total_credit_points")), 0);
                                    valueSet1.add(v1e1);
                                    BarEntry v1e2 = new BarEntry(Integer.valueOf(previousObject.optString("total_credit_points")), 1);
                                    valueSet1.add(v1e2);
                                    BarEntry v1e3 = new BarEntry(Integer.valueOf(lastObject.optString("total_credit_points")), 2);
                                    valueSet1.add(v1e3);

                                    ArrayList<BarEntry> valueSet2 = new ArrayList<>();
                                    BarEntry v2e1 = new BarEntry(Integer.valueOf(currentObject.optString("total_debit_points")), 0);
                                    valueSet2.add(v2e1);
                                    BarEntry v2e2 = new BarEntry(Integer.valueOf(previousObject.optString("total_debit_points")), 1);
                                    valueSet2.add(v2e2);
                                    BarEntry v2e3 = new BarEntry(Integer.valueOf(lastObject.optString("total_debit_points")), 2);
                                    valueSet2.add(v2e3);

                                    BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Earn Point");
                                    barDataSet1.setColor(getResources().getColor(R.color.colorlogin));
                                    BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Spent Point");
                                    barDataSet2.setColor(getResources().getColor(R.color.colorAccent));

                                    dataSets = new ArrayList<>();
                                    dataSets.add(barDataSet1);
                                    dataSets.add(barDataSet2);

                                    ArrayList<String> xAxis = new ArrayList<>();
                                    xAxis.add("Current Week");
                                    xAxis.add("Previous Week");
                                    xAxis.add("Last Week");

                                    BarData data = new BarData(xAxis, dataSets);
                                    chart.setData(data);
                                    chart.setDescription("");
                                    chart.animateXY(2000, 2000);
                                    chart.invalidate();

                                    Log.e("ldldl", Integer.valueOf(object.optString("wishlist_items")) + "//");

                                    if (Integer.valueOf(object.optString("wishlist_items")) > 0) {

                                        dashboardProgressBar.setVisibility(View.VISIBLE);
                                        // journeryToGoal.setText("Journey to your Goals " + val + " % ");
                                        journeryToGoal.setText("Journey to your Goals");

                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                                            dashboardProgressBar.setProgress(val, true);


                                        } else {

                                            dashboardProgressBar.setProgress(val);

                                        }

                                    } else {

                                        dashboardProgressBar.setVisibility(View.GONE);
                                        journeryToGoal.setText("Set your Goal");

                                    }

                                    dashboardProgressBar.setMax(100);

                                }

                            } /*else if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("unauthenticated")) {
                            Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                            getActivity().startActivity(loginIntent);
                            getActivity().finish();
                            }*/ else {

                                new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                            }

                        } catch (JSONException e) {

                            e.printStackTrace();

                        }

                    } else {

                        new CustomAlert().showAlert(getActivity(), 0, "There are something wrong");

                    }

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //pd.cancel();
                //Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    private void showAlertForMobile() {

        TextView alertMessage, alertCancel, alertOk;
        final ImageView alertImage, customAlertImage;
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.show_custom_alert);
        alertOk = dialog.findViewById(R.id.alertOk);
        alertCancel = dialog.findViewById(R.id.alertCancel);
        alertMessage = dialog.findViewById(R.id.alertMessage);
        alertImage = dialog.findViewById(R.id.alertImage);
        customAlertImage = dialog.findViewById(R.id.customAlertImage);
        alertImage.setVisibility(View.GONE);

        alertMessage.setText("Update your mobile number to recover your Account in case of forgot password.");
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.gravity = Gravity.CENTER_VERTICAL;
        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        alertOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
                MainActivity2.rel_header.setVisibility(View.GONE);
                Global.changeFragment(getActivity(), new UserProfile());

            }

        });

        alertCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();

    }

    public void getInvitation() {

        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        //will modify according to the requirement
        Call<JsonObject> call = service.getChildInvitationNotification(Global.acceptHeader, Global.token);

        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e("cal", call.toString());

                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();
                } else if (response.isSuccessful()) {
                    String res = response.body().toString();
                    Log.e("childInvitaion1", res);

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                            JSONArray array = jsonObject.optJSONArray("data");

                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object = array.optJSONObject(i);

                                if (object.optString("request_status").equalsIgnoreCase("1")) {

                                    parentId = object.optString("parent_id");
                                    invitationMessage.setText(object.optString("parent_name") + " requested to add you as child. Accept request to add otherwise may decline.");
                                    invitationLinear.setVisibility(View.VISIBLE);

                                } else {

                                    invitationLinear.setVisibility(View.GONE);

                                }

                            }
                        }


                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                    // pd.cancel();

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                // pd.cancel();
                //  new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    public void updateParentInvitation(String var) {

        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);

        Service service = Service.retrofit.create(Service.class);

        Call<JsonObject> call = service.updateChildInvitation(Global.acceptHeader, Global.token, parentId, "1", var);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();
                } else {
                    String res = response.body().toString();
                    Log.e("childInvitaion", res);

                    try {

                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                            new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                        } else if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("unauthenticated")) {

                            Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                            startActivity(loginIntent);
                            getActivity().finish();


                        } else {

                            //Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                            new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                    //pd.cancel();

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //  pd.cancel();
                //  Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.txtView_earnMore_dashboard:

                MainActivity2.rel_header.setVisibility(View.GONE);
                Global.changeFragment(getActivity(), new ListOfTask());
                break;

            case R.id.invitationAccept:

                invitationLinear.setVisibility(View.GONE);
                updateParentInvitation("2");
                break;

            case R.id.invitationCancel:

                invitationLinear.setVisibility(View.GONE);
                updateParentInvitation("3");
                break;

            case R.id.dashboardWishlist:

                MainActivity2.rel_header.setVisibility(View.GONE);
                Global.changeFragment(getActivity(), new Wishlist());
                break;

            case R.id.lessonDayCard:

                MainActivity2.rel_header.setVisibility(View.GONE);
                Global.changeFragment(getActivity(), new LessonOfDay());
                break;

            case R.id.DashboardAvailablePoints:

             /*MainActivity2.rel_header.setVisibility(View.GONE);
                Global.changeFragment(getActivity(), new AccountDetail());*/
                break;

        }

    }

}
