package reloadedhd.com.digitalbank.retrofit;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.HashMap;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RetrofitService {

    private Call<ResponseBody> call;
    private Context context;
    private int requestCode;
    private int responseCode;
    private String url;
    private RetrofitResponse retrofitResponse;
    private RequestBody body;
    // private String auth;
    private String auth1;
    HashMap<String, String> map;

    public RetrofitService(Context context, RetrofitResponse retrofitResponse, String url, int requestCode, int responseCode, RequestBody body, String auth1) {
        this.context = context;
        this.retrofitResponse = retrofitResponse;
        this.url = url;
        this.requestCode = requestCode;
        this.responseCode = responseCode;
        this.body = body;
        this.auth1 = auth1;

    }

    public RetrofitService(Context context, RetrofitResponse retrofitResponse, String url, int requestCode, int responseCode, String auth1) {
        this.context = context;
        this.retrofitResponse = retrofitResponse;
        this.url = url;
        this.requestCode = requestCode;
        this.responseCode = responseCode;

        this.auth1 = auth1;
        this.map = map;

    }

    public void callService(boolean value) {

        final ProgressDialog dialog = new ProgressDialog(context);

        if (value) {

            dialog.setTitle("");
            dialog.setMessage("Please wait");
            dialog.setCancelable(false);
            dialog.show();

        }

        // OkHttpClient httpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();

       /*OkHttpClient httpClient = new OkHttpClient.Builder().readTimeout(5, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES)
                .build();*/

        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                final Request request = chain.request().newBuilder()
                        .addHeader("Accept", "Application/json")
                        .addHeader("Authorization", auth1)
                        .build();

                return chain.proceed(request);
            }
        };

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(interceptor);
        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ServiceURLs.BASE)
                .client(client)
                .build();


      /*  Retrofit retrofit = new Retrofit.Builder().baseUrl(ServiceURLs.BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();
*/
        ServiceAPI api = retrofit.create(ServiceAPI.class);

        if (requestCode == 1) {

            //call = api.callGetMethod(auth, auth1, url);
            call = api.callGetMethod(url);

        } else if (requestCode == 2) {

            call = api.callPostMethod(auth1, url, body);

        }

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog != null) {
                    dialog.cancel();
                }
                Log.e("boolean", response.isSuccessful() + "");
                String res = response.body().toString();
                Log.e("res", res);
                try {
                    JSONObject object = new JSONObject(res);
                    Log.e("obbb", object.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("res", res);
                retrofitResponse.onResult(responseCode, res);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                if (dialog != null) {

                    dialog.cancel();

                }
                Log.e("call", call.toString()+"/");

                Toast.makeText(context, "Failure", Toast.LENGTH_LONG).show();

            }

        });

    }
}
