package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.model.CategoryModel;

public class CategoryManagementAdapter extends RecyclerView.Adapter<CategoryManagementAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CategoryModel> categoryList;
    CategoryClick click;

    public CategoryManagementAdapter(FragmentActivity activity, ArrayList<CategoryModel> categoryList) {
        this.context = activity;
        this.categoryList = categoryList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.category_mgt_adpt, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.category_name_txtView.setText(categoryList.get(position).getCategoryName());

        if (categoryList.get(position).getCategory_boosted().equalsIgnoreCase("1")) {

            holder.linLay_categoryName.setAlpha(0.30f);
            holder.selectedCategoryImage.setVisibility(View.VISIBLE);

        } else {

            holder.linLay_categoryName.setAlpha(1.0f);
            holder.selectedCategoryImage.setVisibility(View.GONE);

        }

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView category_name_txtView;
        LinearLayout linLay_categoryName;
        ImageView selectedCategoryImage;

        public ViewHolder(View itemView) {
            super(itemView);

            category_name_txtView = itemView.findViewById(R.id.category_name_txtView);
            linLay_categoryName = itemView.findViewById(R.id.linLay_categoryName);
            selectedCategoryImage = itemView.findViewById(R.id.selectedCategoryImage);
            linLay_categoryName.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.linLay_categoryName:

                    click.onCategoryClick(getAdapterPosition());

                    break;

            }

        }

    }

    public void setOnCategoryClickListener(CategoryClick click) {

        this.click = click;

    }

    public interface CategoryClick {

        void onCategoryClick(int pos);

    }

}
