package reloadedhd.com.digitalbank.Fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.ChangePassword_Api;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.global.DigitalGlobal;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePassword extends Fragment implements View.OnClickListener {
    RelativeLayout relLayout_back_button_changePassword;
    EditText edt_newPassword, edt_confirmPassword;
    SharedPreferences sharedPreferences;
    private Button changePassSubmitButton;

    public ChangePassword() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);

        sharedPreferences = DigitalBankContoller.getGlobalPreferences();

        relLayout_back_button_changePassword = view.findViewById(R.id.relLayout_back_button_changePassword);
        edt_newPassword = view.findViewById(R.id.edt_newPassword);
        edt_confirmPassword = view.findViewById(R.id.edt_confirmPassword);
        changePassSubmitButton = view.findViewById(R.id.changePassSubmitButton);

        changePassSubmitButton.setOnClickListener(this);
        relLayout_back_button_changePassword.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.relLayout_back_button_changePassword:
                new DigitalGlobal().hideKeyboard(getActivity());
                getActivity().onBackPressed();
                break;

            case R.id.changePassSubmitButton:

                String neww = edt_newPassword.getText().toString().trim();
                String confirm = edt_confirmPassword.getText().toString().trim();

                new DigitalGlobal().hideKeyboard(getActivity());
                if (neww.length() < 6) {
                    edt_newPassword.setError("New Password must be atleast 6 characters");
                } else if (confirm.length() < 6) {
                    edt_confirmPassword.setError("New Password must be atleast 6 characters");
                } else if (!neww.equals(confirm)) {
                    edt_confirmPassword.setError("Password does not match");
                } else {
                    changePass();
                    edt_newPassword.setText("");
                    edt_confirmPassword.setText("");
                }

                break;

        }

    }

    private void changePass() {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.changePassword(Global.acceptHeader, Global.token, edt_newPassword.getText().toString().trim(), edt_confirmPassword.getText().toString().trim());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();
                }else {


                String res = response.body().toString();
                Log.e("changepas", res);
                try {

                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has("status") && !jsonObject.optString("status").equalsIgnoreCase("error")) {

                        //Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                        new CustomAlert().showAlert(getActivity(), 1, jsonObject.optString("message"));

                    } else if(jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("unauthenticated"))
                    {
                        Intent loginIntent=new Intent(getActivity(), LoginScreen.class);
                        getActivity().startActivity(loginIntent);
                        getActivity().finish();


                    }
                    else {

                        //Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                        new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");
            }

        });

    }

}
