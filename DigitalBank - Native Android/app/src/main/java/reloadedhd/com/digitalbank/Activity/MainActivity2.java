package reloadedhd.com.digitalbank.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Adapter.DrawerAdapter;
import reloadedhd.com.digitalbank.Adapter.NotificationAdapter;
import reloadedhd.com.digitalbank.Fragment.AddChild;
import reloadedhd.com.digitalbank.Fragment.Auction;
import reloadedhd.com.digitalbank.Fragment.AuctionList;
import reloadedhd.com.digitalbank.Fragment.CategoryManagement;
import reloadedhd.com.digitalbank.Fragment.ChildDetail;
import reloadedhd.com.digitalbank.Fragment.Dashboard;
import reloadedhd.com.digitalbank.Fragment.History;
import reloadedhd.com.digitalbank.Fragment.LoadMore;
import reloadedhd.com.digitalbank.Fragment.ParentProfile;
import reloadedhd.com.digitalbank.Fragment.ParentScreen;
import reloadedhd.com.digitalbank.Fragment.PaymentGatewayStripe;
import reloadedhd.com.digitalbank.Fragment.Quiz;
import reloadedhd.com.digitalbank.Fragment.ReferFriend;
import reloadedhd.com.digitalbank.Fragment.Setting;
import reloadedhd.com.digitalbank.Fragment.TagParent;
import reloadedhd.com.digitalbank.Fragment.UserProfile;
import reloadedhd.com.digitalbank.Fragment.Wishlist;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.global.DigitalGlobal;
import reloadedhd.com.digitalbank.model.NotificationModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity2 extends FragmentActivity {
    DrawerLayout drawer;
    LinearLayout linLayout_drawer_header;
    public static RelativeLayout rel_header;
    RelativeLayout relLayout_menu_button_main;
    public static ImageView profile_pic_main2;
    public static ImageView img_menu_button_main;
    String loginStatus;
    TextView txt_header_main, appName_txt_main;
    SharedPreferences sharedPreferences;
    ImageView notificationIcon;
    //todo for notification
    private ArrayList<NotificationModel> notificationModels = new ArrayList<>();
    private TextView noNotificationText;
    private RecyclerView notificationRecycler;
    private NotificationAdapter notificationAdapter = new NotificationAdapter(MainActivity2.this, notificationModels);
    DrawerAdapter drawerAdapter;
    ListView mDrawerList;
    FragmentManager fragmentManager;
    ActionBarDrawerToggle toggle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        rel_header = findViewById(R.id.rel_header);
        img_menu_button_main = findViewById(R.id.img_menu_button_main);
        txt_header_main = findViewById(R.id.txt_header_main);

        notificationIcon = findViewById(R.id.notificationIcon);

        appName_txt_main = (TextView) findViewById(R.id.appName_txt_main);

        //----------------------------drawer ids----------------------------------------
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        profile_pic_main2 = (ImageView) findViewById(R.id.profile_pic_main2);
        linLayout_drawer_header = findViewById(R.id.linLayout_drawer_header);

        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        sharedPreferences = getSharedPreferences("LoginPref", Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token", "");
        String name = sharedPreferences.getString("name", "");
        String mobile = sharedPreferences.getString("mobile", "");
        loginStatus = sharedPreferences.getString("loginStatus", "");
        appName_txt_main.setText(sharedPreferences.getString("name", ""));

        Global.token = sharedPreferences.getString("token", "");

        Log.e("loginstats", loginStatus + "//");


        if (sharedPreferences.contains("avtar") && sharedPreferences.getString("avtar", "") != null && !sharedPreferences.getString("avtar", "").equalsIgnoreCase("null")) {

            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(ServiceURLs.USERPROFILEIMAGEBASEURL + sharedPreferences.getString("avtar", "").trim(), profile_pic_main2, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    Log.e("mainimg", "ok/" + imageUri);

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    Log.e("imgfail", "fail" + failReason.toString());
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                    if (loginStatus.equalsIgnoreCase("2")) {
                        profile_pic_main2.setImageBitmap(loadedImage);

                    } else if (loginStatus.equalsIgnoreCase("3")) {
                        ParentScreen.profile_pic_profile.setImageBitmap(loadedImage);
                        profile_pic_main2.setImageBitmap(loadedImage);
                    }

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("bitmap", new DigitalGlobal().BitMapToString(loadedImage));
                    editor.commit();
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    Log.e("imgcanc", imageUri + "");

                }

            });

        }

        notificationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNotification();
            }
        });

        //--------------custom navigation using listview----------------------------------------------------

        if (loginStatus.equalsIgnoreCase("2")) {

            profile_pic_main2.setImageDrawable(getResources().getDrawable(R.drawable.profile_holder));
            txt_header_main.setText("Dashboard");

            ArrayList arrayList = new ArrayList();
            arrayList.add("Dashboard");
            arrayList.add("Wishlist");
            arrayList.add("TagParent");
            arrayList.add("History");
            arrayList.add("Auction");
            arrayList.add("Quiz");
            arrayList.add("Settings");
            arrayList.add("Refer A Friend");
            arrayList.add("Sign Out");

            int[] drawerIcon = new int[]{R.drawable.dashboard_icon, R.drawable.wishlist_icon, R.drawable.tag, R.drawable.history_icon,
                    R.drawable.auction_icon, R.drawable.auction_icon, R.drawable.setting, R.drawable.setting, R.drawable.logout_icon};

            drawerAdapter = new DrawerAdapter(MainActivity2.this, arrayList, drawerIcon);
            mDrawerList.setAdapter(drawerAdapter);

        } else if (loginStatus.equalsIgnoreCase("3")) {

            profile_pic_main2.setImageDrawable(getResources().getDrawable(R.drawable.profile_holder));
            txt_header_main.setText("Home");

            ArrayList arrayList = new ArrayList();
            arrayList.add("Home");
            arrayList.add("Settings");
            arrayList.add("Sign Out");

            int[] drawerIcon = new int[]{R.drawable.dashboard_icon, R.drawable.setting, R.drawable.logout_icon};
            drawerAdapter = new DrawerAdapter(MainActivity2.this, arrayList, drawerIcon);
            mDrawerList.setAdapter(drawerAdapter);

        }

        //---to handle drawer------------------------------------
        toggle = new ActionBarDrawerToggle(this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                new DigitalGlobal().hideKeyboard(MainActivity2.this);
            }

        };

        toggle.syncState();
        drawer.setDrawerListener(toggle);

        //-----------------setting view dashboard on first time----------------------
        if (loginStatus.equalsIgnoreCase("2")) {
            Global.changeFragment(this, new Dashboard());
            //  txt_header_main.setText("Dashboard");
        } else if (loginStatus.equalsIgnoreCase("3")) {
            Global.changeFragment(this, new ParentScreen());
            //  txt_header_main.setText("Load More");
        }

        //-----------------Menu onClick listener------------------------------------------------
        relLayout_menu_button_main = findViewById(R.id.relLayout_menu_button_main);
        relLayout_menu_button_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.openDrawer(GravityCompat.START);
                }

            }
        });

        //-------------------------Profile pic onClick Listener---------------------
        profile_pic_main2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer.closeDrawer(GravityCompat.START);
                rel_header.setVisibility(View.GONE);

                if (loginStatus.equalsIgnoreCase("2")) {
                    Global.changeFragment(MainActivity2.this, new UserProfile());
                } else if (loginStatus.equalsIgnoreCase("3")) {
                    Global.changeFragment(MainActivity2.this, new ParentProfile());
                }

            }
        });

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.containers);
                drawer.closeDrawer(GravityCompat.START);
                //------------------------child drawer list---------------------------------------
                if (loginStatus.equalsIgnoreCase("2")) {

                    ViewGroup viewgroup = (ViewGroup) view;
                    View view0 = viewgroup.getChildAt(0);
                    View view1 = viewgroup.getChildAt(1);
                    View view2 = viewgroup.getChildAt(2);
                    View view3 = viewgroup.getChildAt(3);
                    View view4 = viewgroup.getChildAt(4);
                    View view5 = viewgroup.getChildAt(5);
                    View view6 = viewgroup.getChildAt(6);

                    if (position == 0) {

                        if (mFragment instanceof Dashboard) {

                        } else {

                            rel_header.setVisibility(View.VISIBLE);
                            Global.changeFragment(MainActivity2.this, new Dashboard());
                            displayScreen(R.id.containers, new Dashboard(), "Tag");
                        }

                    } else if (position == 1) {

                        if (mFragment instanceof Wishlist) {

                        } else {

                            rel_header.setVisibility(View.GONE);
                            Global.changeFragment(MainActivity2.this, new Wishlist());

                        }

                    } else if (position == 2) {

                        if (mFragment instanceof TagParent) {

                        } else {

                            rel_header.setVisibility(View.GONE);
                            Global.changeFragment(MainActivity2.this, new TagParent());

                        }

                    } else if (position == 3) {

                        if (mFragment instanceof History) {

                        } else {

                            rel_header.setVisibility(View.GONE);
                            Global.changeFragment(MainActivity2.this, new History());

                        }

                    } else if (position == 4) {

                        if (mFragment instanceof Auction) {

                        } else {

                            rel_header.setVisibility(View.GONE);
                            Global.changeFragment(MainActivity2.this, new AuctionList());

                        }

                    } else if (position == 5) {

                        if (mFragment instanceof Quiz) {

                        } else {

                            rel_header.setVisibility(View.GONE);
                            Global.changeFragment(MainActivity2.this, new Quiz());

                        }

                    } else if (position == 6) {

                        if (mFragment instanceof Setting) {

                        } else {

                            rel_header.setVisibility(View.GONE);
                            Global.changeFragment(MainActivity2.this, new Setting());

                        }

                    } else if (position == 7) {

                        if (mFragment instanceof ReferFriend) {

                        } else {

                            rel_header.setVisibility(View.GONE);
                            Global.changeFragment(MainActivity2.this, new ReferFriend());

                        }

                    } else if (position == 8) {

                      /*SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.remove("bitmap");
                        //editor.remove("bitmap");
                        editor.remove("add");
                        editor.clear();
                        editor.apply();
                        startActivity(new Intent(MainActivity2.this, LoginScreen.class));
                        finish();*/

                        getLogout();

                    }

                } else if (loginStatus.equalsIgnoreCase("3")) {
                    //-------------to show highlisted menu item on drawer---------
                    ViewGroup viewgroup = (ViewGroup) view;
                    View view1 = viewgroup.getChildAt(0);
                    View view2 = viewgroup.getChildAt(1);
                    View view3 = viewgroup.getChildAt(2);

                    if (position == 0) {

                        if (mFragment instanceof ParentScreen) {

                        } else {

                            rel_header.setVisibility(View.VISIBLE);
                            Global.changeFragment(MainActivity2.this, new ParentScreen());

                        }

                    } else if (position == 1) {

                        if (mFragment instanceof Setting) {

                        } else {

                            rel_header.setVisibility(View.GONE);
                            Global.changeFragment(MainActivity2.this, new Setting());

                        }

                    } else if (position == 2) {

                       /*SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.remove("bitmap");
                        //editor.remove("bitmap");
                        editor.remove("add");
                        editor.clear();
                        editor.apply();
                        startActivity(new Intent(MainActivity2.this, LoginScreen.class));
                        finish();*/

                        getLogout();

                    }

                }

            }

        });

        //Setting the item click listener for listView
        //mDrawerList.setOnItemClickListener(itemClickListener);

    }

    //todo for set the back icon and drawer icon dynamically as per requirement
    /* private void enableViews(boolean enable){
         if(enable) {
             drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
             toggle.setDrawerIndicatorEnabled(false);
             getSupportActionBar().setDisplayHomeAsUpEnabled(true);
             if(!mToolBarNavigationListenerIsRegistered) {
                 toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View v) {
                         onBackPressed();
                     }
                 });
                 mToolBarNavigationListenerIsRegistered = true;
             }
         } else {
             drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
             getSupportActionBar().setDisplayHomeAsUpEnabled(false);
             toggle.setDrawerIndicatorEnabled(true);
             toggle.setToolbarNavigationClickListener(null);
             mToolBarNavigationListenerIsRegistered = false;
         }}*/

    private void getLogout() {

        final ProgressDialog pd = ProgressDialog.show(this, "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user/logout.json");


        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(MainActivity2.this, LoginScreen.class);
                    startActivity(loginIntent);
                    finish();

                }

                String res = response.body().toString();
                Log.e("logoutres", res);
                pd.cancel();

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove("bitmap");
                //editor.remove("bitmap");
                editor.remove("add");
                editor.clear();
                editor.apply();
                startActivity(new Intent(MainActivity2.this, LoginScreen.class));
                finish();

                try {

                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();

            }

        });

    }

    private void showNotification() {

        TextView toolbarTitle;
        RelativeLayout toolbarRelativeBack;
        //todo for show the popup
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.alert_notification);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.gravity = Gravity.CENTER_VERTICAL;
        // dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        notificationRecycler = dialog.findViewById(R.id.notificationRecycler);
        noNotificationText = dialog.findViewById(R.id.noNotificationText);
        toolbarTitle = dialog.findViewById(R.id.toolbarTitle);
        toolbarRelativeBack = dialog.findViewById(R.id.toolbarRelativeBack);
        toolbarTitle.setText("Notifications");
        toolbarRelativeBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        notificationRecycler.setLayoutManager(new LinearLayoutManager(MainActivity2.this));
        notificationRecycler.setAdapter(notificationAdapter);

        notificationModels.clear();
        dialog.show();
        getNotificationData();

    }

    void getNotificationData() {

        final ProgressDialog pd = ProgressDialog.show(this, "", "Loading wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user_notification.json");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(MainActivity2.this, LoginScreen.class);
                    startActivity(loginIntent);
                    finish();

                }

                try {

                    pd.cancel();

                    String res = response.body().toString();
                    Log.e("notiiii", res);
                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {

                        JSONArray array = jsonObject.optJSONArray("data");

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);
                            NotificationModel model = new NotificationModel();
                            model.setId(object.optString("id"));
                            model.setFrom(object.optString("from"));
                            model.setMessage(object.optString("message"));
                            model.setTo(object.optString("To"));
                            notificationModels.add(model);

                        }

                        if (!notificationModels.isEmpty()) {

                            notificationAdapter.notifyDataSetChanged();
                            noNotificationText.setVisibility(View.GONE);
                            notificationRecycler.setVisibility(View.VISIBLE);

                        } else {

                            noNotificationText.setVisibility(View.VISIBLE);
                            notificationRecycler.setVisibility(View.GONE);

                        }

                    } else {

                        noNotificationText.setVisibility(View.VISIBLE);
                        notificationRecycler.setVisibility(View.GONE);

                    }

                } catch (Exception e) {


                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                Toast.makeText(MainActivity2.this, "Failure", Toast.LENGTH_SHORT).show();

            }

        });

    }

    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();
        //------------id(Container) to fit in frame layout(exculding app bar)------------------
        Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.containers);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        if (loginStatus.equalsIgnoreCase("2")) {

            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            } else if (mFragment instanceof Dashboard) {
                finish();
            } else if (mFragment instanceof UserProfile) {
                rel_header.setVisibility(View.VISIBLE);
                /*  Global.*/
                changeFragment(MainActivity2.this, new Dashboard());
            } else if (mFragment instanceof UserProfile) {
                /*  Global.*/
                changeFragment(MainActivity2.this, new Dashboard());
            } else if (mFragment instanceof History) {
                rel_header.setVisibility(View.VISIBLE);
                /*  Global.*/
                changeFragment(MainActivity2.this, new Dashboard());
            } else if (mFragment instanceof Wishlist) {
                rel_header.setVisibility(View.VISIBLE);
                /*  Global.*/
                changeFragment(MainActivity2.this, new Dashboard());
            } /*else if (mFragment instanceof AuctionList) {
                rel_header.setVisibility(View.VISIBLE);
                Global.changeFragment(MainActivity2.this, new Dashboard());
            }*/ else if (mFragment instanceof Setting) {
                rel_header.setVisibility(View.VISIBLE);
                /*  Global.*/
                changeFragment(MainActivity2.this, new Dashboard());
            } else if (count > 1) {
                super.onBackPressed();
                //finish();
            }
        } else if (loginStatus.equalsIgnoreCase("3")) {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            } else if (mFragment instanceof LoadMore) {
                rel_header.setVisibility(View.VISIBLE);
                /*  Global.*/
                changeFragment(MainActivity2.this, new ParentScreen());
            } else if (mFragment instanceof Setting) {
                rel_header.setVisibility(View.VISIBLE);
                /*  Global.*/
                changeFragment(MainActivity2.this, new ParentScreen());
            } else if (mFragment instanceof ParentScreen) {
                finish();
            } else if (mFragment instanceof PaymentGatewayStripe) {
                rel_header.setVisibility(View.VISIBLE);
                /*  Global.*/
                changeFragment(MainActivity2.this, new ParentScreen());
                // Global.changeFragment(MainActivity2.this, new LoadMore());
            } else if (mFragment instanceof ChildDetail) {
                rel_header.setVisibility(View.VISIBLE);
                /* Global.*/
                changeFragment(MainActivity2.this, new ParentScreen());
            } else if (mFragment instanceof CategoryManagement) {
                /*Global.*/
                changeFragment(MainActivity2.this, new ChildDetail());
            } else if (mFragment instanceof ParentProfile) {
                rel_header.setVisibility(View.VISIBLE);
                /*Global.*/
                changeFragment(MainActivity2.this, new ParentScreen());
            } else if (mFragment instanceof AddChild) {

                rel_header.setVisibility(View.VISIBLE);
                /*Global.*/
                changeFragment(MainActivity2.this, new ParentScreen());

            } else if (count > 1) {
                super.onBackPressed();
                // finish();

            }

        }

    }

    //todo for replace the fragment
    public static void changeFragment(Context context, Fragment fragment) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.containers, fragment).addToBackStack("tag").commit();

    }

    public void displayScreen(int id, Fragment _frag, String tag) {

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        fragmentManager = getSupportFragmentManager();
        if (fragment == null) {
            fragmentManager.beginTransaction().replace(id, _frag, tag).addToBackStack(tag).commit();
        } else {
            fragmentManager.popBackStack(tag, 0);
        }
    }

}
