package reloadedhd.com.digitalbank.model;

public class PaymentHistoryModel {
    private String id;
    private String FROM;
    private String TO;
    private String transaction_points;

    public String getTransaction_time() {
        return transaction_time;
    }

    public void setTransaction_time(String transaction_time) {
        this.transaction_time = transaction_time;
    }

    private String transaction_time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFROM() {
        return FROM;
    }

    public void setFROM(String FROM) {
        this.FROM = FROM;
    }

    public String getTO() {
        return TO;
    }

    public void setTO(String TO) {
        this.TO = TO;
    }

    public String getTransaction_points() {
        return transaction_points;
    }

    public void setTransaction_points(String transaction_points) {
        this.transaction_points = transaction_points;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    private String remarks;
}
