package reloadedhd.com.digitalbank.model;

public class BiddingModel {
    private String user_id;
    private String auction_id;
    private String auction_name;
    private String bid_value;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAuction_id() {
        return auction_id;
    }

    public void setAuction_id(String auction_id) {
        this.auction_id = auction_id;
    }

    public String getAuction_name() {
        return auction_name;
    }

    public void setAuction_name(String auction_name) {
        this.auction_name = auction_name;
    }

    public String getBid_value() {
        return bid_value;
    }

    public void setBid_value(String bid_value) {
        this.bid_value = bid_value;
    }

    public String getBid_time() {
        return bid_time;
    }

    public void setBid_time(String bid_time) {
        this.bid_time = bid_time;
    }

    private String bid_time;
}
