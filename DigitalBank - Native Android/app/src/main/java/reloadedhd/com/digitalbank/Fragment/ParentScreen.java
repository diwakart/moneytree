package reloadedhd.com.digitalbank.Fragment;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Activity.MainActivity2;
import reloadedhd.com.digitalbank.Adapter.ParentInviteAdapter;
import reloadedhd.com.digitalbank.Adapter.ParentScreenAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.global.DigitalGlobal;
import reloadedhd.com.digitalbank.model.ChildListModel;
import reloadedhd.com.digitalbank.model.ParentInviteModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ParentScreen extends Fragment implements View.OnClickListener {
    private RelativeLayout relLayout_back_profile;
    private ImageView loadMore_parentsprofile_button, img_parents_addChild;
    public RecyclerView recyclerview_parentScreen;
    private LinearLayoutManager linearLayoutManager;
    private TextView parentPoints, parentUserName;
    public TextView noChilds;
    private SharedPreferences preferences, tokenPreference;
    private ArrayList<ChildListModel> arrayList;
    private ParentScreenAdapter parentScreenAdapter;
    public static CircleImageView profile_pic_profile;

    //todo for child invitationlist
    private RecyclerView childInvitationList;
    private ParentInviteAdapter childInviteAdapter;
    private ArrayList<ParentInviteModel> childArrayList;

    private ShimmerFrameLayout parentDashboardShimmer;

    public ParentScreen() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.parent, container, false);

        preferences = DigitalBankContoller.getGlobalPreferences();
        tokenPreference = DigitalBankContoller.getTokenPreferences();

        relLayout_back_profile = view.findViewById(R.id.relLayout_back_profile);
        //relLayout_childDetailView = view.findViewById(R.id.relLayout_childDetailView);
        loadMore_parentsprofile_button = view.findViewById(R.id.loadMore_parentsprofile_button);
        img_parents_addChild = view.findViewById(R.id.img_parents_addChild);
        recyclerview_parentScreen = view.findViewById(R.id.recyclerview_parentScreen);
        noChilds = (TextView) view.findViewById(R.id.noChilds);
        parentPoints = (TextView) view.findViewById(R.id.parentPoints);
        parentUserName = (TextView) view.findViewById(R.id.parentUserName);
        profile_pic_profile = (CircleImageView) view.findViewById(R.id.profile_pic_profile);

        parentDashboardShimmer = view.findViewById(R.id.parentDashboardShimmer);

        childInvitationList = view.findViewById(R.id.childInvitationList);
        childInvitationList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        childArrayList = new ArrayList<>();
        childArrayList.clear();
        childInviteAdapter = new ParentInviteAdapter(getActivity(), childArrayList);
        childInvitationList.setAdapter(childInviteAdapter);

        childInviteAdapter.onParentInviteAdapterClick(new ParentInviteAdapter.ParentInviteClick() {
            @Override
            public void onYesClick(int pos) {
                updateParentInvitation("2", childArrayList.get(pos).getStudent_id());
            }

            @Override
            public void onCancelClick(int pos) {
                updateParentInvitation("3", childArrayList.get(pos).getStudent_id());
            }

        });

        if (preferences.contains("bitmap") && preferences.getString("bitmap", "") != null) {

            profile_pic_profile.setImageBitmap(new DigitalGlobal().StringToBitMap(preferences.getString("bitmap", "").trim()));

        } else if (preferences.getString("avtar", "") != null && !preferences.getString("avtar", "").equalsIgnoreCase("null")) {

            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(ServiceURLs.USERPROFILEIMAGEBASEURL + preferences.getString("avtar", ""), profile_pic_profile, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    Log.e("imgstrt", "ok/" + imageUri);

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    Log.e("imgfail", "fail" + failReason.toString());

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    profile_pic_profile.setImageBitmap(loadedImage);

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    Log.e("imgcanc", imageUri + "");

                }

            });

        } else {

            profile_pic_profile.setImageDrawable(getResources().getDrawable(R.drawable.profile_holder));

        }

        linearLayoutManager = new LinearLayoutManager(getActivity());
        //linearLayoutManager.setStackFromEnd(false);
        recyclerview_parentScreen.setLayoutManager(linearLayoutManager);

        arrayList = new ArrayList<>();
        arrayList.clear();
        parentScreenAdapter = new ParentScreenAdapter(getActivity(), arrayList);
        recyclerview_parentScreen.setAdapter(parentScreenAdapter);

        parentScreenAdapter.onAdapterClickListener(new ParentScreenAdapter.ParentAdapterClick() {
            @Override
            public void onParentResendClick(int pos) {

                resendChildInvitation(arrayList.get(pos).getStudent_id());
                arrayList.get(pos).setRequest_status("1");
                parentScreenAdapter.notifyDataSetChanged();

            }
        });

        if (preferences.contains("point")) {

            parentPoints.setText(preferences.getString("point", ""));

        }

        parentUserName.setText(preferences.getString("name", ""));

        relLayout_back_profile.setOnClickListener(this);
        loadMore_parentsprofile_button.setOnClickListener(this);
        img_parents_addChild.setOnClickListener(this);

        getChildInvitation();

        scrollPosition();

        Log.e("okddd", tokenPreference.getString("fcmtoken", "") + "//" + tokenPreference.getString("add", "") + "/" + preferences.getString("user_id", ""));

        if (tokenPreference.getString("fcmtoken", "") != null && !tokenPreference.getString("fcmtoken", "").isEmpty() && !preferences.getString("add", "").equalsIgnoreCase("1")) {

            saveDeviceforNotification();

        }

        return view;

    }

    private void saveDeviceforNotification() {

        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        //will modify according to the requirement
        String android_id = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e("deviceid", android_id + "");
        HashMap<String, RequestBody> body = new HashMap<>();
        //token:required,device_secret:required,device_type:required,user_id:required
        body.put("token", RequestBody.create(MediaType.parse("multipart/form-data"), tokenPreference.getString("fcmtoken", "")));
        body.put("device_secret", RequestBody.create(MediaType.parse("multipart/form-data"), android_id));
        body.put("device_type", RequestBody.create(MediaType.parse("multipart/form-data"), "1"));
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), preferences.getString("user_id", "")));

        Call<JsonObject> call = service.getPostResponse(Global.acceptHeader, Global.token, ServiceURLs.REGISTERFORNOTIFICATION, body);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                    if (response.isSuccessful()) {

                        Log.e("res", response.body().toString() + "/");
                        SharedPreferences.Editor editor = tokenPreference.edit();
                        editor.putString("add", "1");
                        editor.commit();

                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
            }

        });

    }

    public void scrollPosition() {

        if (childArrayList.size() > 1) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    for (int i = 0; i < childArrayList.size(); i++) {

                        if (i == childArrayList.size() - 1) {

                            //scrollPosition();
                            i = 0;

                        } else {

                            childInvitationList.scrollToPosition(i);

                        }

                    }

                }

            }, 1000);
        }

    }

    public void updateParentInvitation(String var, String childId) {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);

        Service service = Service.retrofit.create(Service.class);

        Call<JsonObject> call = service.updateChildInvitation(Global.acceptHeader, Global.token, childId, "1", var);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                    String res = response.body().toString();

                    try {
                        JSONObject jsonObject = new JSONObject(res);
                        Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                        getChildInvitation();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.cancel();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
            }

        });

    }

    private void getChildInvitation() {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        //will modify according to the requirement
        Call<JsonObject> call = service.getChildInvitationNotification(Global.acceptHeader, Global.token);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                String res = response.body().toString();
                Log.e("childInvitaion1", res);

                try {
                    int val = -1;

                    JSONObject jsonObject = new JSONObject(res);
                    JSONArray array = jsonObject.optJSONArray("data");
                    childArrayList.clear();

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.optJSONObject(i);
                        ParentInviteModel model = new ParentInviteModel();
                        model.setId(object.optString("id"));
                        model.setName(object.optString("name"));
                        model.setPoints(object.optString("points"));
                        model.setRequest_status(object.optString("request_status"));
                        model.setRequest_type(object.optString("request_type"));
                        model.setStudent_id(object.optString("student_id"));

                        if (Integer.valueOf(object.optString("request_status")) == 1 && Integer.valueOf(object.optString("request_type")) == 1) {

                            val = i;

                        }

                        childArrayList.add(model);

                    }
                    if (!childArrayList.isEmpty()) {

                        if (val >= 0) {

                            childInvitationList.setVisibility(View.VISIBLE);
                            childInviteAdapter.notifyDataSetChanged();

                        } else {

                            childInvitationList.setVisibility(View.GONE);

                        }

                    } else {

                        childInvitationList.setVisibility(View.GONE);

                    }

                    getChildList();

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();

            }

        });

    }

    private void resendChildInvitation(String user_id) {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.resendInviteChild(Global.acceptHeader, Global.token, user_id, "1");
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.dismiss();
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                String res = response.body().toString();

                try {

                    Log.e("cld", res);
                    JSONObject jsonObject = new JSONObject(res);

                    boolean check = jsonObject.toString().contains("status");
                    if (check) {
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("success")) {
                            MainActivity2.rel_header.setVisibility(View.VISIBLE);
                            String message = jsonObject.get("message").toString();
                            Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                        } else {
                            String message = jsonObject.get("message").toString();
                            Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        String statusError = jsonObject.getString("error");
                        if (statusError.equalsIgnoreCase("success")) {
                            MainActivity2.rel_header.setVisibility(View.VISIBLE);
                            String message = jsonObject.get("message").toString();
                            Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                            Global.changeFragment(getActivity(), new ParentScreen());
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
            }

        });

    }

    private void getChildList() {

        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        parentDashboardShimmer.startShimmer();
        parentDashboardShimmer.setVisibility(View.VISIBLE);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getChildListDetail(Global.acceptHeader, Global.token);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                parentDashboardShimmer.setVisibility(View.GONE);
                parentDashboardShimmer.stopShimmer();

                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {
                //pd.dismiss();

                String res = response.isSuccessful() + "";

                if (res.equalsIgnoreCase("true"))

                    try {

                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        String status = jsonObject.getString("status");
                        Log.e("ressss", jsonObject.toString());
                        arrayList.clear();

                        if (status.equalsIgnoreCase("success")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int j = 0; j < jsonArray.length(); j++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                                ChildListModel model = new ChildListModel();
                                model.setStudent_id(jsonObject1.optString("student_id"));
                                model.setId(jsonObject1.optString("id"));
                                model.setRequest_type(jsonObject1.optString("request_type"));
                                model.setRequest_status(jsonObject1.optString("request_status"));
                                model.setRequested_time(jsonObject1.optString("requested_time"));
                                model.setPoints(jsonObject1.optString("points"));
                                model.setName(jsonObject1.optString("name"));
                                model.setDob(jsonObject1.optString("dob"));
                                model.setMobile(jsonObject1.optString("mobile"));
                                model.setEmail(jsonObject1.optString("email"));
                                model.setAvtar(jsonObject1.optString("avtar"));
                                model.setCurrent_point(jsonObject1.optString("current_point"));
                                model.setAccount_type(jsonObject1.optString("account_type"));
                                arrayList.add(model);

                            }

                            if (!arrayList.isEmpty()) {

                                noChilds.setVisibility(View.GONE);
                                // set the view to visible without a circular reveal animation below Lollipop
                                recyclerview_parentScreen.setVisibility(View.VISIBLE);

                                //  }

                                parentScreenAdapter.notifyDataSetChanged();

                            } else {

                                noChilds.setVisibility(View.VISIBLE);
                                recyclerview_parentScreen.setVisibility(View.GONE);

                            }


                        } else {

                            Toast.makeText(getActivity(), response.message() + "", Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

            }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                new CustomAlert().showAlert(getActivity(), 0, "Failure");
                //Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                // pd.dismiss();

            }

        });

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.relLayout_back_profile:

                MainActivity2.rel_header.setVisibility(View.VISIBLE);
                getActivity().onBackPressed();

                break;

            case R.id.loadMore_parentsprofile_button:

                MainActivity2.rel_header.setVisibility(View.GONE);
                Global.changeFragment(getActivity(), new LoadMore());

                break;

            case R.id.img_parents_addChild:

                MainActivity2.rel_header.setVisibility(View.GONE);
                Global.changeFragment(getActivity(), new AddChild());

                break;

        }

    }

}
