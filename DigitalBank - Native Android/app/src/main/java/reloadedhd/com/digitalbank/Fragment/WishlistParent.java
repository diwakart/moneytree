package reloadedhd.com.digitalbank.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.WishlistAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.WishlistParentModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class WishlistParent extends Fragment implements View.OnClickListener {
    private RelativeLayout relLayout_back_button_wishlistParent;
    private TextView noWishListData;
    private RecyclerView wishlist;
    private LinearLayoutManager manager;
    private ArrayList<WishlistParentModel> arrayList = new ArrayList<>();
    private WishlistAdapter adapter;
    private Bundle b;
    private String id;
    private ShimmerFrameLayout childWhishlistShimmer;

    public WishlistParent() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wishlist_parent, container, false);
        initView(view);

        b = getArguments();

        if (b != null) {

            id = b.getString("id");
            getWishList(id);

        }

        return view;

    }

    void getWishList(String id) {

        childWhishlistShimmer.setVisibility(View.VISIBLE);
        childWhishlistShimmer.startShimmer();
        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, ServiceURLs.GETCHILDDETAIL + id + "/wishlist.json");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                childWhishlistShimmer.stopShimmer();
                childWhishlistShimmer.setVisibility(View.GONE);
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                    String res = response.body().toString().trim();
                    Log.e("wishlst", res);
                    //pd.cancel();

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        JSONArray array = jsonObject.optJSONArray("wishlist_data");
                        arrayList.clear();

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);
                            WishlistParentModel model = new WishlistParentModel();
                            model.setId(object.optString("id"));
                            model.setProduct_id(object.optString("product_id"));
                            model.setProduct_image(object.optString("product_image"));
                            model.setProduct_name(object.optString("product_name"));
                            model.setProduct_price(object.optString("product_price"));
                            arrayList.add(model);

                        }

                        if (!arrayList.isEmpty()) {

                            adapter.notifyDataSetChanged();
                            noWishListData.setVisibility(View.GONE);
                            wishlist.setVisibility(View.VISIBLE);

                        } else {

                            noWishListData.setVisibility(View.VISIBLE);
                            wishlist.setVisibility(View.GONE);

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    private void initView(View view) {

        wishlist = view.findViewById(R.id.wishlist);
        noWishListData = view.findViewById(R.id.noWishListData);
        childWhishlistShimmer = view.findViewById(R.id.childWhishlistShimmer);

        manager = new GridLayoutManager(getActivity(), 2);
        wishlist.setLayoutManager(manager);

        adapter = new WishlistAdapter(getActivity(), arrayList);
        wishlist.setAdapter(adapter);

        relLayout_back_button_wishlistParent = view.findViewById(R.id.relLayout_back_button_wishlistParent);

        relLayout_back_button_wishlistParent.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.relLayout_back_button_wishlistParent:

                getActivity().onBackPressed();

                break;

        }

    }

}
