package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import java.util.ArrayList;
import reloadedhd.com.digitalbank.Activity.MainActivity2;
import reloadedhd.com.digitalbank.Fragment.ChildDetail;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.model.ChildListModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;

public class ParentScreenAdapter extends RecyclerView.Adapter<ParentScreenAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ChildListModel> arrayList;
    public ParentAdapterClick click;

    public ParentScreenAdapter(Context activity, ArrayList<ChildListModel> arrayList) {
        this.context = activity;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.parentscreen_adapter, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.txtView_parent_childName.setText(arrayList.get(position).getName());

        if (arrayList.get(position).getRequest_status().equalsIgnoreCase("1")) {

            holder.invited.setVisibility(View.VISIBLE);
            holder.declined.setVisibility(View.GONE);
            holder.resend.setVisibility(View.GONE);
            holder.forwardArrow.setVisibility(View.GONE);

        } else if (arrayList.get(position).getRequest_status().equalsIgnoreCase("2")) {

            holder.invited.setVisibility(View.GONE);
            holder.declined.setVisibility(View.GONE);
            holder.resend.setVisibility(View.GONE);
            holder.forwardArrow.setVisibility(View.VISIBLE);

        } else if (arrayList.get(position).getRequest_status().equalsIgnoreCase("3")) {

            holder.invited.setVisibility(View.GONE);
            holder.declined.setVisibility(View.VISIBLE);
            //todo for parent can not resend the invitation
            holder.resend.setVisibility(View.VISIBLE);
            holder.forwardArrow.setVisibility(View.GONE);

        }

        if (!arrayList.get(position).getDob().isEmpty() && !arrayList.get(position).getDob().equalsIgnoreCase("null")) {

            holder.dobText.setText(arrayList.get(position).getDob());
            holder.linearDOB.setVisibility(View.VISIBLE);

        } else {

            holder.linearDOB.setVisibility(View.GONE);

        }

        if (arrayList.get(position).getAvtar() != null && !arrayList.get(position).getAvtar().equalsIgnoreCase(" ") && !arrayList.get(position).getAvtar().equalsIgnoreCase("null")) {

            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(ServiceURLs.USERPROFILEIMAGEBASEURL + arrayList.get(position).getAvtar(), holder.parentAdapterImage, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    Log.e("imgstrt", "ok/" + imageUri);

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.parentAdapterImage.setImageBitmap(loadedImage);

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }

            });

        }

        holder.relLayout_childDetailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (arrayList.get(position).getRequest_status().equalsIgnoreCase("2")) {

                    MainActivity2.rel_header.setVisibility(View.GONE);
                    ChildDetail detail = new ChildDetail();
                    Bundle b = new Bundle();
                    b.putString("id", arrayList.get(position).getStudent_id());
                    detail.setArguments(b);
                    Global.changeFragment(context, detail);

                }

            }

        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RelativeLayout relLayout_childDetailView;
        TextView txtView_parent_childName, dobText;
        ImageView invited, resend, declined, forwardArrow, parentAdapterImage;
        LinearLayout linearDOB;

        public ViewHolder(View itemView) {
            super(itemView);
            invited = (ImageView) itemView.findViewById(R.id.invited);
            resend = (ImageView) itemView.findViewById(R.id.resend);
            declined = (ImageView) itemView.findViewById(R.id.declined);
            forwardArrow = (ImageView) itemView.findViewById(R.id.forwardArrow);
            parentAdapterImage = (ImageView) itemView.findViewById(R.id.parentAdapterImage);

            relLayout_childDetailView = itemView.findViewById(R.id.relLayout_childDetailView);
            txtView_parent_childName = itemView.findViewById(R.id.txtView_parent_childName);
            dobText = itemView.findViewById(R.id.dobText);
            linearDOB = itemView.findViewById(R.id.linearDOB);

            resend.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.resend:

                    click.onParentResendClick(getAdapterPosition());

                    break;

            }

        }

    }

    public void onAdapterClickListener(ParentAdapterClick click) {
        this.click = click;
    }

    public interface ParentAdapterClick {
        void onParentResendClick(int pos);
    }

}
