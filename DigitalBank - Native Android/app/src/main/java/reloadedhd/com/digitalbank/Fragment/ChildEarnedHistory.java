package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.ChildEarnedHistoryAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.ChildEarnModelNew;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChildEarnedHistory extends Fragment {

    private RecyclerView childEarnedHistory;
    private LinearLayoutManager manager;
    private ArrayList<ChildEarnModelNew> arrayList;
    private ChildEarnedHistoryAdapter adapter;
    private SharedPreferences preferences;
    private TextView childEarnedNoData;
    private int totalPages;
    private ShimmerFrameLayout historyShimmer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.child_earned_history, null);
        childEarnedHistory = v.findViewById(R.id.childEarnedHistory);
        childEarnedNoData = v.findViewById(R.id.childEarnedNoData);
        historyShimmer = v.findViewById(R.id.historyShimmer);

        manager = new LinearLayoutManager(getActivity());
        childEarnedHistory.setLayoutManager(manager);
        preferences = DigitalBankContoller.getGlobalPreferences();
        arrayList = new ArrayList<>();
        arrayList.clear();
        adapter = new ChildEarnedHistoryAdapter(getActivity(), arrayList);
        childEarnedHistory.setAdapter(adapter);
        getEarnedData(preferences.getString("user_id", ""));
        return v;

    }

    private void getEarnedData(String id) {

        historyShimmer.setVisibility(View.VISIBLE);
        historyShimmer.startShimmer();
        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/user/earn_list.json");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                historyShimmer.startShimmer();
                historyShimmer.setVisibility(View.GONE);

                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                if (response.isSuccessful()) {

                    String res = response.body().toString();
                    Log.e("wishlist", res);

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        JSONArray array = jsonObject.optJSONArray("data");

                        totalPages = Integer.valueOf(jsonObject.optJSONObject("meta").optString("total"));

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);
                            ChildEarnModelNew model = new ChildEarnModelNew();
                            model.setRemarks(object.optString("remarks"));
                            model.setTransaction_id(object.optString("transaction_id"));
                            model.setTransaction_points(object.optString("transaction_points"));
                            model.setTransaction_time(object.optString("transaction_time"));
                            model.setType(object.optString("type"));

                            arrayList.add(model);

                        }

                        if (!arrayList.isEmpty()) {

                            adapter.notifyDataSetChanged();
                            childEarnedNoData.setVisibility(View.GONE);
                            childEarnedHistory.setVisibility(View.VISIBLE);

                        } else {

                            childEarnedHistory.setVisibility(View.GONE);
                            childEarnedNoData.setVisibility(View.VISIBLE);

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                } else {

                    childEarnedHistory.setVisibility(View.GONE);
                    childEarnedNoData.setVisibility(View.VISIBLE);

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                new CustomAlert().showAlert(getActivity(), 0, "Failure");
                //pd.cancel();

            }

        });

    }

}
