package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryDatail extends Fragment implements View.OnClickListener {

    private RelativeLayout toolbarRelativeBack;
    private LinearLayout detailCategoryLinear, detailTaskLinear, detailDescriptionLinear, detailPointsLinear, descriptionDateLinear;
    private TextView toolbarTitle, detailCategoryName, detailTask, detailDescription, detailPoints, detailDate, detailTime;
    private Bundle b;
    private CardView detailCard;
    private ShimmerFrameLayout historyDetailShimmer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.history_detail, container, false);
        b = getArguments();
        initView(v);

        return v;
    }

    private void initView(View v) {

        toolbarRelativeBack = v.findViewById(R.id.toolbarRelativeBack);
        toolbarTitle = v.findViewById(R.id.toolbarTitle);
        toolbarTitle.setText("Detail");
        //todo for linear layouts
        detailCategoryLinear = v.findViewById(R.id.detailCategoryLinear);
        detailTaskLinear = v.findViewById(R.id.detailTaskLinear);
        detailDescriptionLinear = v.findViewById(R.id.detailDescriptionLinear);
        detailPointsLinear = v.findViewById(R.id.detailPointsLinear);
        descriptionDateLinear = v.findViewById(R.id.descriptionDateLinear);
        //todo for textViews
        detailCategoryName = v.findViewById(R.id.detailCategoryName);
        detailTask = v.findViewById(R.id.detailTask);
        detailDescription = v.findViewById(R.id.detailDescription);
        detailPoints = v.findViewById(R.id.detailPoints);
        detailDate = v.findViewById(R.id.detailDate);
        detailTime = v.findViewById(R.id.detailTime);
        detailCard = v.findViewById(R.id.detailCard);

        historyDetailShimmer = v.findViewById(R.id.historyDetailShimmer);

        toolbarRelativeBack.setOnClickListener(this);

        if (b != null) {

            getDetail(b.getString("id"));

        }

    }

    private void getDetail(String id) {

        Log.e("idddddddd", id + "/");
        historyDetailShimmer.setVisibility(View.VISIBLE);
        historyDetailShimmer.startShimmer();
        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("transaction_id", RequestBody.create(MediaType.parse("multipart/form-data"), id));

        Call<JsonObject> call = service.getPostResponse(Global.acceptHeader, Global.token, "v1/user/transaction_details.json", body);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                historyDetailShimmer.stopShimmer();
                historyDetailShimmer.setVisibility(View.GONE);

                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {
                //pd.cancel();

                if (response.isSuccessful()) {

                    String res = response.body().toString();
                    Log.e("details", res + "");

                    try {

                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.optString("status").equalsIgnoreCase("success")) {

                            JSONArray array = jsonObject.optJSONArray("data");

                            if (array.length() > 0) {
                                detailCard.setVisibility(View.VISIBLE);
                            } else {
                                detailCard.setVisibility(View.GONE);
                            }

                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object = array.optJSONObject(i);
                                detailDescription.setText(object.optString("remarks"));

                                if (object.has("category_name") && !object.optString("category_name").isEmpty()) {

                                    detailCategoryLinear.setVisibility(View.VISIBLE);
                                    detailCategoryName.setText(object.optString("category_name"));

                                } else {

                                    detailCategoryLinear.setVisibility(View.GONE);

                                }

                                if (object.has("task_name") && !object.optString("task_name").isEmpty()) {

                                    detailTask.setText(object.optString("task_name"));
                                    detailTaskLinear.setVisibility(View.VISIBLE);

                                } else {

                                    detailTaskLinear.setVisibility(View.GONE);

                                }

                                if (object.has("transaction_points") && !object.optString("transaction_points").isEmpty()) {

                                    detailPoints.setText(object.optString("transaction_points") + " Points");
                                    detailPointsLinear.setVisibility(View.VISIBLE);

                                } else {

                                    detailPointsLinear.setVisibility(View.GONE);

                                }

                                Date date = null;
                                String outputString = null;

                                try {

                                    date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(object.optString("created_at"));

                                } catch (ParseException e) {

                                    e.printStackTrace();

                                }

                                outputString = new SimpleDateFormat("dd-MM-yyyy").format(date);
                                String newString = new SimpleDateFormat("HH:mm").format(date);

                                detailDate.setText(outputString);
                                detailTime.setText(newString);

                            }

                        } else {

                            new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                } else {

                    new CustomAlert().showAlert(getActivity(), 0, "Try after some time");

                }

            }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

             //   pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.toolbarRelativeBack:

                getActivity().onBackPressed();

                break;

        }

    }

}
