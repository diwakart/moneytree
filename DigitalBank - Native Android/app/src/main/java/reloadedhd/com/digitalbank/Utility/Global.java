package reloadedhd.com.digitalbank.Utility;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.inputmethod.InputMethodManager;

import java.util.ArrayList;
import java.util.HashMap;

import reloadedhd.com.digitalbank.R;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class Global {

    public static String token="";
    public static String acceptHeader="application/json";

    public static void changeFragment(Context context, Fragment fragment) {
        FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.containers,fragment).addToBackStack("tag").commit();

    }

    public static ArrayList<HashMap<String,String>> arrayList_categoriesList = new ArrayList<>();

    public static ArrayList<HashMap<String, String>> getArrayList_categoriesList() {
        return arrayList_categoriesList;
    }

    public static void setArrayList_categoriesList(ArrayList<HashMap<String, String>> arrayList_categoriesList) {
        Global.arrayList_categoriesList = arrayList_categoriesList;
    }

    public static ArrayList<HashMap<String,String>> arrayList_getProfile = new ArrayList<>();

    public static ArrayList<HashMap<String, String>> getArrayList_getProfile() {
        return arrayList_getProfile;
    }

    public static void setArrayList_getProfile(ArrayList<HashMap<String, String>> arrayList_getProfile) {
        Global.arrayList_getProfile = arrayList_getProfile;
    }

    public static ArrayList<HashMap<String,String>> arrayList_getChildListDetail = new ArrayList<>();

    public static ArrayList<HashMap<String, String>> getArrayList_getChildListDetail() {
        return arrayList_getChildListDetail;
    }

    public static void setArrayList_getChildListDetail(ArrayList<HashMap<String, String>> arrayList_getChildListDetail) {
        Global.arrayList_getChildListDetail = arrayList_getChildListDetail;
    }

}
