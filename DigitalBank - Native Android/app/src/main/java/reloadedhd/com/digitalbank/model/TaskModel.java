package reloadedhd.com.digitalbank.model;

import java.io.Serializable;

public class TaskModel implements Serializable {
    private String id;
    private String category_name;
    private String question;
    private String question_type;
    private String task_points;
    private String attempt_status;
    private String total_attempts;
    private String bonus_point;
    private String attemp1_points;
    private String attemp2_points;
    private String attemp3_points;
    private String category_boosted;
    private String boosted_value;
    private String above3_points;
    private boolean selected = false;

    public String getBoosted_value() {
        return boosted_value;
    }

    public void setBoosted_value(String boosted_value) {
        this.boosted_value = boosted_value;
    }

    public String getCategory_boosted() {
        return category_boosted;
    }

    public void setCategory_boosted(String category_boosted) {
        this.category_boosted = category_boosted;
    }

    public String getAttemp1_points() {
        return attemp1_points;
    }

    public void setAttemp1_points(String attemp1_points) {
        this.attemp1_points = attemp1_points;
    }

    public String getAttemp2_points() {
        return attemp2_points;
    }

    public void setAttemp2_points(String attemp2_points) {
        this.attemp2_points = attemp2_points;
    }

    public String getAttemp3_points() {
        return attemp3_points;
    }

    public void setAttemp3_points(String attemp3_points) {
        this.attemp3_points = attemp3_points;
    }

    public String getAbove3_points() {
        return above3_points;
    }

    public void setAbove3_points(String above3_points) {
        this.above3_points = above3_points;
    }

    public String getAttempt_status() {
        return attempt_status;
    }

    public void setAttempt_status(String attempt_status) {
        this.attempt_status = attempt_status;
    }

    public String getTotal_attempts() {
        return total_attempts;
    }

    public void setTotal_attempts(String total_attempts) {
        this.total_attempts = total_attempts;
    }

    public String getTask_points() {
        return task_points;
    }

    public void setTask_points(String task_points) {
        this.task_points = task_points;
    }

    public String getBonus_point() {
        return bonus_point;
    }

    public void setBonus_point(String bonus_point) {
        this.bonus_point = bonus_point;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(String question_type) {
        this.question_type = question_type;
    }

}
