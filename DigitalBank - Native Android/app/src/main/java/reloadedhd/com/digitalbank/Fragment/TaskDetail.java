package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import github.nisrulz.recyclerviewhelper.RVHItemDividerDecoration;
import github.nisrulz.recyclerviewhelper.RVHItemTouchHelperCallback;
import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.TaskDetailAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.model.OptionModel;
import reloadedhd.com.digitalbank.model.TaskDetailModel;
import reloadedhd.com.digitalbank.model.TaskModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskDetail extends Fragment implements View.OnClickListener {
    private RelativeLayout toolbarRelativeBack;
    private Bundle b;
    private int pos;
    private TextView toolbarTitle, questionText, questionTypeText, taskDetailAttemp, taskDetailPoint;
    private Button taskDetailSubmit, taskDetailPrevious, taskDetailNext;
    private RecyclerView taskOptionList;
    private EditText editTextAnswer;
    private LinearLayoutManager manager;
    private ArrayList<OptionModel> optionModelArrayList;
    private TaskDetailAdapter adapter;
    private LinearLayout topLinearTaskDetail;
    private CardView taskDetailTopCard;
    private String taskId, correctAnswer;
    private SharedPreferences preferences;
    private int attempQuestion = 0;

    public TaskDetail() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.task_detail, container, false);
        preferences = DigitalBankContoller.getGlobalPreferences();

        initView(view);

        taskId = getArguments().getString("id");

        getTaskDetail(getArguments().getString("res"));
        return view;

    }

    private void getTaskDetail(String res) {


        try {

            JSONObject jsonObject = new JSONObject(res);
            String arrangeOrder = null;

            if (!jsonObject.optString("status").equalsIgnoreCase("error")) {

                JSONArray array = jsonObject.optJSONArray("data");
                topLinearTaskDetail.setVisibility(View.VISIBLE);
                taskDetailTopCard.setVisibility(View.VISIBLE);
                taskDetailSubmit.setVisibility(View.VISIBLE);

                for (int i = 0; i < array.length(); i++) {

                    JSONObject object = array.optJSONObject(i);
                    TaskDetailModel model = new TaskDetailModel();
                    model.setId(object.optString("id"));
                    model.setCategory_name(object.optString("category_name"));
                    model.setQuestion(object.optString("question"));
                    model.setQuestion_type(object.optString("question_type"));
                    questionText.setText(object.optString("question"));

                    taskDetailAttemp.setText("Total Attemp \n" + object.optString("total_attempt"));
                    taskDetailPoint.setText("Task Points \n" + object.optString("earn_points"));

                    correctAnswer = object.optString("correct_answer");

                    JSONArray jsonArray = object.optJSONArray("options");

                    optionModelArrayList.clear();

                    if (!object.optString("question_type").equalsIgnoreCase("1")) {

                        for (int j = 0; j < jsonArray.length(); j++) {

                            JSONObject object1 = jsonArray.optJSONObject(j);
                            OptionModel optionModel = new OptionModel();
                            optionModel.setLabel(object1.optString("label"));
                            optionModel.setValue(object1.optString("value"));
                            optionModel.setQuestionType(object.optString("question_type"));
                            optionModelArrayList.add(optionModel);

                            if (object.optString("question_type").equalsIgnoreCase("3") && !object.optString("label").equalsIgnoreCase("null")) {

                                if (j == 0) {

                                    arrangeOrder = object1.optString("label");

                                } else {

                                    arrangeOrder = arrangeOrder + "," + object1.optString("label");

                                }

                            }

                        }

                    }

                    if (!optionModelArrayList.isEmpty() && object.optString("question_type").equalsIgnoreCase("2") || object.optString("question_type").equalsIgnoreCase("3")) {

                        taskOptionList.setVisibility(View.VISIBLE);
                        editTextAnswer.setVisibility(View.GONE);
                        adapter = new TaskDetailAdapter(getActivity(), optionModelArrayList);
                        taskOptionList.setAdapter(adapter);

                        //Setup onItemTouchHandler to enable drag and drop , swipe left or right
                        ItemTouchHelper.Callback callback = new RVHItemTouchHelperCallback(adapter, true, true,
                                true);
                        ItemTouchHelper helper = new ItemTouchHelper(callback);
                        helper.attachToRecyclerView(taskOptionList);

                        //Set the divider in the recyclerview
                        taskOptionList.addItemDecoration(new RVHItemDividerDecoration(getActivity(), LinearLayoutManager.VERTICAL));

                        //adapter.notifyDataSetChanged();
                        if (object.optString("question_type").equalsIgnoreCase("3")) {

                            questionTypeText.setText("Arrange order");

                        } else {

                            questionTypeText.setText("Multiple Choice");

                        }

                    } else {

                        questionTypeText.setText("Fill in the blanks");
                        taskOptionList.setVisibility(View.GONE);
                        editTextAnswer.setVisibility(View.VISIBLE);

                    }

                    model.setOptionModelArrayList(optionModelArrayList);
                    Log.e("tastdetail", model.toString() + "/");

                }

            } else {

                topLinearTaskDetail.setVisibility(View.GONE);
                taskDetailSubmit.setVisibility(View.GONE);
                taskDetailTopCard.setVisibility(View.GONE);
                Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();

            }

        } catch (JSONException e) {

            e.printStackTrace();

        }

    }

    private void initView(View view) {

        //todo for toolbar
        toolbarTitle = view.findViewById(R.id.toolbarTitle);
        toolbarRelativeBack = view.findViewById(R.id.toolbarRelativeBack);

        topLinearTaskDetail = view.findViewById(R.id.topLinearTaskDetail);
        taskDetailTopCard = view.findViewById(R.id.taskDetailTopCard);
        questionText = view.findViewById(R.id.questionText);
        taskOptionList = view.findViewById(R.id.taskOptionList);
        editTextAnswer = view.findViewById(R.id.editTextAnswer);
        manager = new LinearLayoutManager(getActivity());
        taskOptionList.setLayoutManager(manager);

        taskDetailSubmit = view.findViewById(R.id.taskDetailSubmit);
        questionTypeText = view.findViewById(R.id.questionTypeText);

        taskDetailAttemp = view.findViewById(R.id.taskDetailAttemp);
        taskDetailPoint = view.findViewById(R.id.taskDetailPoint);

        taskDetailPrevious = view.findViewById(R.id.taskDetailPrevious);
        taskDetailNext = view.findViewById(R.id.taskDetailNext);

        optionModelArrayList = new ArrayList<>();

        toolbarTitle.setText("Task Detail");

        taskDetailPrevious.setOnClickListener(this);
        taskDetailNext.setOnClickListener(this);
        toolbarRelativeBack.setOnClickListener(this);
        taskDetailSubmit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.taskDetailPrevious:
//have remove this functionality for now if you wan tto implement this then it is in 23August zip file

                break;

            case R.id.taskDetailNext:

                break;

            case R.id.toolbarRelativeBack:

                if (attempQuestion == 0) {

                    submitAnswer(" ");
                    getActivity().onBackPressed();

                } else {

                    getActivity().onBackPressed();

                }


                break;

            case R.id.taskDetailSubmit:

                String val = null;

                if (!optionModelArrayList.isEmpty()) {

                    int positon = -1;

                    for (int i = 0; i < optionModelArrayList.size(); i++) {

                        if (optionModelArrayList.get(i).isSelected() && !optionModelArrayList.get(i).getQuestionType().equalsIgnoreCase("3")) {

                            positon = i;

                            if (val == null) {

                                val = optionModelArrayList.get(i).getValue();

                            } else {

                                val = val + "," + optionModelArrayList.get(i).getValue();

                            }

                        } else if (optionModelArrayList.get(i).getQuestionType().equalsIgnoreCase("3")) {

                            positon = i;

                            if (val == null) {

                                val = optionModelArrayList.get(i).getValue();

                            } else {

                                val = val + "," + optionModelArrayList.get(i).getValue();

                            }

                        }

                    }

                    if (positon == -1) {

                        new CustomAlert().showAlert(getActivity(), 0, "Kindly select any options");
                        return;

                    }

                } else if (editTextAnswer.getText().toString().trim().length() == 0) {

                    new CustomAlert().showAlert(getActivity(), 0, "Enter your answer");
                    return;

                } else {

                    val = editTextAnswer.getText().toString().trim();

                }

                submitAnswer(val);

                break;

        }

    }

    private void submitAnswer(final String answer) {
//correctAnswer=answer;
        Log.e("tassskidd", taskId + "/" + answer);
        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Verifying Answer ...", true);
        Service service = Service.retrofit.create(Service.class);

        Call<JsonObject> call = service.taskAnswerResponse(Global.acceptHeader, Global.token, "v1/tasks/" + taskId.trim() + "/answer.json", answer);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();
                if (response.code()==401){
                    Intent loginIntent=new Intent(getActivity(),LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }else {

                try {

                    if (!answer.trim().isEmpty() || !answer.equalsIgnoreCase(" ")) {

                        attempQuestion = 1;

                    }

                    String res = response.body().toString();
                    Log.e("answerres", res);

                    JSONObject jsonObject = new JSONObject(res);

                    if (jsonObject.optString("status").equalsIgnoreCase("success")) {

                        String point = "0";

                        if (preferences.getString("point", "") != null && !preferences.getString("point", "").equalsIgnoreCase("null")) {

                            point = String.valueOf(Integer.valueOf(preferences.getString("point", "")) + Integer.valueOf(jsonObject.optString("earn_points")));

                        } else {

                            point = jsonObject.optString("earn_points");

                        }

                        Log.e("uppoint", point);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("point", point);
                        editor.apply();

                        taskDetailAttemp.setText("Answer is \n" + correctAnswer);
                        taskDetailPoint.setText("Earn Points \n" + jsonObject.optString("earn_points"));

                        taskDetailSubmit.setVisibility(View.GONE);

                        editTextAnswer.setText("");
                        editTextAnswer.setHint("Fill in your answer here");

                        new CustomAlert().showAlertWithAnimation(getActivity(), "Congratulations", "Your answer is correct.\nYou have earned " + jsonObject.optString("earn_points") + " Points.\nNow your total points are " + point + ".");

                        //getNextQuestion();

                    } else if (!answer.trim().isEmpty() || !answer.equalsIgnoreCase(" ")) {

                        JSONArray array = jsonObject.optJSONArray("data");

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);
                            taskDetailAttemp.setText("Total Attemp \n" + object.optString("total_attempt"));
                            taskDetailPoint.setText("Task Points \n" + object.optString("updated_task_points"));

                        }

                        new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }


}
