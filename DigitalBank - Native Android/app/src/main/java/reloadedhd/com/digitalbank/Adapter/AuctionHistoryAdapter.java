package reloadedhd.com.digitalbank.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Fragment.BiddingList;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.model.AuctionHistoryModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;

public class AuctionHistoryAdapter extends RecyclerView.Adapter<AuctionHistoryAdapter.Holder> {

    private Context context;
    private ArrayList<AuctionHistoryModel> arrayList;
    AuctionHistoryClickListener click;

    public AuctionHistoryAdapter(Context context, ArrayList<AuctionHistoryModel> arrayList) {
        this.arrayList = arrayList;
        this.context = context;

    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.auction_history_adapter, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        holder.auctionProductName.setText(arrayList.get(position).getCategory_name());
        holder.biddingCurrrentPoint.setText(arrayList.get(position).getCurrent_auction_value() + " Points");
        holder.biddingEndDate.setText(arrayList.get(position).getEnd_time());
        Picasso.with(context).load(ServiceURLs.PRODUCTIMAGEBASEURL + arrayList.get(position).getAuctionImage()).fit().into(holder.auctionHistoryAdapterImage);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    class Holder extends RecyclerView.ViewHolder {
        TextView auctionProductName, biddingCurrrentPoint, biddingEndDate;
        ImageView auctionHistoryAdapterImage;

        public Holder(View itemView) {
            super(itemView);
            auctionProductName = itemView.findViewById(R.id.auctionProductName);
            biddingCurrrentPoint = itemView.findViewById(R.id.biddingCurrrentPoint);
            biddingEndDate = itemView.findViewById(R.id.biddingEndDate);
            auctionHistoryAdapterImage = itemView.findViewById(R.id.auctionHistoryAdapterImage);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click.onAuctionHistoryCLick(getAdapterPosition());
                }
            });
        }

    }

    public void setOnHistoryClick(AuctionHistoryClickListener click) {
        this.click = click;
    }

    public interface AuctionHistoryClickListener {
        void onAuctionHistoryCLick(int pos);
    }

}
