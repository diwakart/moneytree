package reloadedhd.com.digitalbank.retrofit;

public class ServiceURLs {

    //todo for load image of the user
    //https://quizme.com.my/digital_bank/api/

  /*public static String USERPROFILEIMAGEBASEURL = "https://103.233.2.220/digital_bank/storage/app/profile/";
    public static String PRODUCTIMAGEBASEURL = "https://103.233.2.220/digital_bank/storage/app/products/";
    public static String BASE = "https://103.233.2.220/digital_bank/api/";*/

    public static String USERPROFILEIMAGEBASEURL = "https://quizme.com.my/digital_bank/storage/app/profile/";
    public static String PRODUCTIMAGEBASEURL = "https://quizme.com.my/digital_bank/storage/app/products/";
    public static String BASE = "https://quizme.com.my/digital_bank/api/";
    public static String GETCHILDLIST = "v1/user/childlist.json";
    public static String INVITECHILD = "v1/child/tag.json";
    public static String SETAUCTIONPRODUCTPRIORITY = "product/set_priority.json";
    public static String PRODUCTLISTOFCATEGORY = "category/{id}/products.json";

    //https://103.233.2.220/digital_bank/api/v1/child/{id}/wishlist.json
    public static String GETCHILDDETAIL = "v1/child/";

    public static String GETCATEGORYLIST = "v1/categories.json";
    public static String GETTASKDETAIL = "v1/tasks/";
    public static String REGISTERFORNOTIFICATION = "v1/user/device_register.json";
    public static String ACCOUNTDETAIL = "v1/user/account_details.json";
    public static String COUNTRYLIST = "country_details.json";

}
