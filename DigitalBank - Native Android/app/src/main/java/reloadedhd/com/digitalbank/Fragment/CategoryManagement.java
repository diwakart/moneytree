package reloadedhd.com.digitalbank.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Adapter.CategoryManagementAdapter;
import reloadedhd.com.digitalbank.Adapter.TaskAdapter;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.Utility.Global;
import reloadedhd.com.digitalbank.WebService.Service;
import reloadedhd.com.digitalbank.application.DigitalBankContoller;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import reloadedhd.com.digitalbank.global.DigitalGlobal;
import reloadedhd.com.digitalbank.model.CategoryModel;
import reloadedhd.com.digitalbank.model.TaskModel;
import reloadedhd.com.digitalbank.retrofit.ServiceURLs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryManagement extends Fragment implements View.OnClickListener {
    private RelativeLayout relLayout_back_button_categoryMgt;
    private TextView txt_boostCategory_catMgt, txt_boostTask_catMgt, txt_boostNow_catMgt, noData, parentName, childName;
    private LinearLayoutManager linearLayoutManager;
    private EditText edt_search_catMgt;
    private ImageView img_search_catMgt, img_parents_categoryMgt, img_child_categoryMgt;
    private RecyclerView recyclerView_catMgt;
    private ArrayList<CategoryModel> categoryModelArrayList;
    private ArrayList<TaskModel> taskModelArrayList;
    private int cattPos = -1, value = 0, taskPos = -1;
    private SharedPreferences preferences;
    private Bundle b;
    private Spinner boostValueSpinner;
    private String childId;
    int perpage = 4;
    int catCurrentPage = 1;
    private ShimmerFrameLayout catMgtDashboardShimmer;
    public boolean isScrolling = false;
    int currentItem, totalItem, scrolledoutItem;

    public CategoryManagement() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.category_management, container, false);
        categoryModelArrayList = new ArrayList<>();
        taskModelArrayList = new ArrayList<>();
        preferences = DigitalBankContoller.getGlobalPreferences();
        initView(view);

        img_search_catMgt.setOnClickListener(this);
        txt_boostTask_catMgt.setOnClickListener(this);
        txt_boostCategory_catMgt.setOnClickListener(this);
        txt_boostNow_catMgt.setOnClickListener(this);
        relLayout_back_button_categoryMgt.setOnClickListener(this);

        getCategoryList();

        return view;

    }

    private void initView(View view) {

        relLayout_back_button_categoryMgt = view.findViewById(R.id.relLayout_back_button_categoryMgt);
        txt_boostCategory_catMgt = view.findViewById(R.id.txt_boostCategory_catMgt);
        txt_boostTask_catMgt = view.findViewById(R.id.txt_boostTask_catMgt);
        txt_boostNow_catMgt = view.findViewById(R.id.txt_boostNow_catMgt);
        edt_search_catMgt = view.findViewById(R.id.edt_search_catMgt);
        img_search_catMgt = view.findViewById(R.id.img_search_catMgt);
        childName = view.findViewById(R.id.childName);
        parentName = view.findViewById(R.id.parentName);
        noData = view.findViewById(R.id.noData);

        catMgtDashboardShimmer = view.findViewById(R.id.catMgtDashboardShimmer);

        boostValueSpinner = view.findViewById(R.id.boostValueSpinner);

        img_child_categoryMgt = view.findViewById(R.id.img_child_categoryMgt);
        img_parents_categoryMgt = view.findViewById(R.id.img_parents_categoryMgt);
        txt_boostCategory_catMgt.setSelected(true);
        recyclerView_catMgt = view.findViewById(R.id.recyclerView_catMgt);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_catMgt.setLayoutManager(linearLayoutManager);

        //recycler paging
        recyclerView_catMgt.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                isScrolling = true;
                Log.e("scroligggg", "ok" + newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.d("23232323", "okkk" + "" + dx + " " + dy + "");
                currentItem = linearLayoutManager.getChildCount();
                totalItem = linearLayoutManager.getItemCount();
                scrolledoutItem = linearLayoutManager.findFirstVisibleItemPosition();
                Log.d("scrolled_item", scrolledoutItem + "");

                if (isScrolling) {

                    Log.d("bool", isScrolling + "");

                    Log.d("scrolling", "scrlll");

                    isScrolling = false;
                    if (value == 1) {

                        if (cattPos != -1) {
                            catCurrentPage += 1;
                            getTaskList(cattPos);

                        } else {
                            catCurrentPage += 1;
                            getTaskList(0);

                        }

                    } else {
                        catCurrentPage += 1;
                        getCategoryList();

                    }

                }

            }

        });

        b = getArguments();

        if (b != null) {

            childName.setText(b.getString("name"));
            childId = b.getString("id");

        }

        if (preferences.contains("bitmap") && preferences.getString("bitmap", "") != null && !preferences.getString("bitmap", "").equalsIgnoreCase("null")) {

            img_parents_categoryMgt.setImageBitmap(new DigitalGlobal().StringToBitMap(preferences.getString("bitmap", "")));

        }

        if (preferences.getString("name", "") != null && !preferences.getString("name", "").equalsIgnoreCase("null")) {

            parentName.setText(preferences.getString("name", ""));

        }

        if (preferences.contains("childImage") && preferences.getString("childImage", "") != null) {

            img_child_categoryMgt.setImageBitmap(new DigitalGlobal().StringToBitMap(preferences.getString("childImage", "")));

        }

    }

    void getCategoryList() {

        catMgtDashboardShimmer.setVisibility(View.VISIBLE);
        catMgtDashboardShimmer.startShimmer();

        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, ServiceURLs.GETCATEGORYLIST + "?child_id=" + childId + "&perpage=4&current_page=" + catCurrentPage);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                catMgtDashboardShimmer.setVisibility(View.GONE);
                catMgtDashboardShimmer.stopShimmer();
                isScrolling = false;
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                    String res = response.body().toString();
                    Log.e("categoryList", res);
                    //pd.cancel();

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                            JSONArray array = jsonObject.optJSONArray("data");
                            //  categoryModelArrayList.clear();

                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object = array.optJSONObject(i);

                                CategoryModel model = new CategoryModel();
                                model.setId(object.optString("id"));
                                model.setCategoryName(object.optString("category_name"));
                                model.setCategory_boosted(object.optString("category_boosted"));
                                model.setBoosted_value(object.optString("boosted_value"));
                                categoryModelArrayList.add(model);

                            }

                            if (!categoryModelArrayList.isEmpty()) {

                                //todo where ever you want to set the category
                                setCategoryAdapter();

                                recyclerView_catMgt.setVisibility(View.VISIBLE);
                                noData.setVisibility(View.GONE);

                            } else {

                                recyclerView_catMgt.setVisibility(View.GONE);
                                noData.setVisibility(View.VISIBLE);

                            }
                        } else if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("unauthenticated")) {
                            Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                            getActivity().startActivity(loginIntent);
                            getActivity().finish();
                        }


                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //pd.cancel();
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();

            }

        });

    }

    private void setCategoryAdapter() {

        final CategoryManagementAdapter categoryManagementAdapter = new CategoryManagementAdapter(getActivity(), categoryModelArrayList);
        recyclerView_catMgt.setAdapter(categoryManagementAdapter);

        categoryManagementAdapter.setOnCategoryClickListener(new CategoryManagementAdapter.CategoryClick() {
            @Override
            public void onCategoryClick(int pos) {

                cattPos = pos;
                Log.d("catt", cattPos + "  ");
                for (int i = 0; i < categoryModelArrayList.size(); i++) {
                    if (pos == i) {
                        Log.d("poss", pos + "  ");
                        categoryModelArrayList.get(i).setSelected(true);
                        categoryModelArrayList.get(i).setCategory_boosted("1");
                     /*if(!categoryModelArrayList.get(pos).getBoosted_value().equalsIgnoreCase("0")) {
                            Log.e("insideadptclk", "okkk");
                            boostValueSpinner.setSelection(Math.round(Float.valueOf(categoryModelArrayList.get(pos).getBoosted_value())));
                        } else {
                            boostValueSpinner.setSelection(0);
                        }*/

                    } else {
                        categoryModelArrayList.get(i).setSelected(false);
                        categoryModelArrayList.get(i).setCategory_boosted("0");
                    }
                }
                categoryManagementAdapter.notifyDataSetChanged();

            }

        });

    }

    void getTaskList(int pos) {

        catMgtDashboardShimmer.setVisibility(View.VISIBLE);
        catMgtDashboardShimmer.startShimmer();
        isScrolling = false;
        String url = "v1/categories/" + categoryModelArrayList.get(pos).getId() + "/tasks.json?child_id=" + childId ;//"&perpage=10&current_page=" + catCurrentPage
        Log.e("url", url);
        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, url.trim());

        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                catMgtDashboardShimmer.stopShimmer();
                catMgtDashboardShimmer.setVisibility(View.GONE);

                if (response.code() == 401) {

                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {

                    String res = response.body().toString();
                    Log.e("tasklst", res);
                    //pd.cancel();

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        JSONArray array = jsonObject.optJSONArray("data");
                        //taskModelArrayList.clear();

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.optJSONObject(i);
                            TaskModel model = new TaskModel();
                            model.setId(object.optString("task_id"));
                            model.setCategory_name(object.optString("category_name"));
                            model.setQuestion(object.optString("question"));
                            model.setQuestion_type(object.optString("question_type"));
                            model.setCategory_boosted(object.optString("task_boosted"));
                            model.setBoosted_value(object.optString("boosted_value"));
                        /*"category_boosted": "0",
 		                  "task_boosted": "0",*/

                            taskModelArrayList.add(model);

                        }

                        if (!taskModelArrayList.isEmpty()) {

                            noData.setVisibility(View.GONE);
                            recyclerView_catMgt.setVisibility(View.VISIBLE);
                            setTaskAdapter();

                        } else {

                            noData.setVisibility(View.VISIBLE);
                            recyclerView_catMgt.setVisibility(View.GONE);

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                // pd.cancel();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    private void setTaskAdapter() {

        final TaskAdapter taskAdapter = new TaskAdapter(getActivity(), taskModelArrayList);
        recyclerView_catMgt.setAdapter(taskAdapter);

        taskAdapter.onTaskClickListener(new TaskAdapter.TaskClick() {
            @Override
            public void onTaskClick(int pos) {

                taskPos = pos;

                for (int i = 0; i < taskModelArrayList.size(); i++) {

                    if (pos == i) {

                        taskModelArrayList.get(i).setSelected(true);
                        taskModelArrayList.get(i).setCategory_boosted("1");

                       /* if (!taskModelArrayList.get(pos).getBoosted_value().equalsIgnoreCase("0")) {
                            boostValueSpinner.setSelection(Math.round(Float.valueOf(taskModelArrayList.get(pos).getBoosted_value())));

                        } else {
                            boostValueSpinner.setSelection(0);
                        }
*/
                    } else {

                        taskModelArrayList.get(i).setSelected(false);
                        taskModelArrayList.get(i).setCategory_boosted("0");

                    }

                }

                taskAdapter.notifyDataSetChanged();

            }

        });

    }


    void searchCategory(String catName) {

        value = 4;

        catMgtDashboardShimmer.setVisibility(View.VISIBLE);
        catMgtDashboardShimmer.startShimmer();
        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/category/search.json?category_name=" + catName /*+ "&child_id=" + childId+"&perpage=5&current_page="+catCurrentPage*/);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                catMgtDashboardShimmer.setVisibility(View.GONE);
                catMgtDashboardShimmer.stopShimmer();
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {
                    String res = response.body().toString();
                    //pd.cancel();
                    Log.e("catres", res);

                    try {

                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.optString("status").equalsIgnoreCase("success")) {

                            JSONArray array = jsonObject.optJSONArray("data");
                            categoryModelArrayList.clear();

                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object = array.optJSONObject(i);
                                CategoryModel model = new CategoryModel();
                                model.setId(object.optString("id"));
                                model.setCategoryName(object.optString("category_name"));
                                model.setCategory_boosted(object.optString("category_boosted"));
                                model.setBoosted_value(object.optString("boosted_value"));
                                categoryModelArrayList.add(model);
                            }
                            if (!categoryModelArrayList.isEmpty()) {
                                setCategoryAdapter();
                                recyclerView_catMgt.setVisibility(View.VISIBLE);
                                noData.setVisibility(View.GONE);
                            } else {
                                recyclerView_catMgt.setVisibility(View.GONE);
                                noData.setVisibility(View.VISIBLE);
                            }
                        } else if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("unauthenticated")) {
                            Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                            getActivity().startActivity(loginIntent);
                            getActivity().finish();


                        } else {
                            noData.setVisibility(View.VISIBLE);
                            recyclerView_catMgt.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                new CustomAlert().showAlert(getActivity(), 0, "Failure");
                //pd.cancel();

            }

        });

    }

    void searchTask(final String taskName) {

        catMgtDashboardShimmer.setVisibility(View.VISIBLE);
        catMgtDashboardShimmer.startShimmer();

        Log.e("tsknm", taskName);
        //final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.getResponse(Global.acceptHeader, Global.token, "v1/task/search.json?task_name=" + taskName + "&child_id=" + childId);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                catMgtDashboardShimmer.setVisibility(View.GONE);
                catMgtDashboardShimmer.stopShimmer();
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                } else {
                    String res = response.body().toString();

                    Log.e("lllll", res);

                    try {

                        //pd.cancel();
                        JSONObject jsonObject = new JSONObject(res);
                        taskModelArrayList.clear();

                        if (jsonObject.optString("status").equalsIgnoreCase("success")) {

                            JSONArray array = jsonObject.optJSONArray("data");

                            for (int i = 0; i < array.length(); i++) {

                                JSONObject object = array.optJSONObject(i);
                                TaskModel model = new TaskModel();
                                model.setId(object.optString("id"));
                                model.setQuestion(object.optString("task_name"));
                                model.setCategory_boosted(object.optString("task_boosted"));
                                model.setBoosted_value(object.optString("boosted_value"));
                                taskModelArrayList.add(model);

                            }

                            if (!taskModelArrayList.isEmpty()) {

                                setTaskAdapter();
                                recyclerView_catMgt.setVisibility(View.VISIBLE);
                                noData.setVisibility(View.GONE);

                            } else {

                                recyclerView_catMgt.setVisibility(View.GONE);
                                noData.setVisibility(View.VISIBLE);

                            }

                        } else if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("unauthenticated")) {
                            Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                            getActivity().startActivity(loginIntent);
                            getActivity().finish();


                        } else {

                            noData.setVisibility(View.VISIBLE);
                            recyclerView_catMgt.setVisibility(View.GONE);

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.txt_boostTask_catMgt:
                boostValueSpinner.setSelection(0);
                value = 1;
                taskModelArrayList.clear();
                catCurrentPage = 1;
                new DigitalGlobal().hideKeyboard(getActivity());
                txt_boostTask_catMgt.setBackgroundResource(R.color.gradient_start);
                txt_boostTask_catMgt.setSelected(true);
                txt_boostCategory_catMgt.setBackgroundResource(R.color.colorloginDark);
                txt_boostCategory_catMgt.setSelected(false);

                if (!categoryModelArrayList.isEmpty()) {

                    if (cattPos != -1) {


                        getTaskList(cattPos);

                    } else {

                        getTaskList(0);

                    }

                }

                break;

            case R.id.txt_boostCategory_catMgt:
                boostValueSpinner.setSelection(0);
                value = 2;
                categoryModelArrayList.clear();
                catCurrentPage = 1;
                new DigitalGlobal().hideKeyboard(getActivity());
                txt_boostTask_catMgt.setBackgroundResource(R.color.colorloginDark);
                txt_boostCategory_catMgt.setBackgroundResource(R.color.gradient_start);
                txt_boostTask_catMgt.setSelected(false);
                txt_boostCategory_catMgt.setSelected(true);

                getCategoryList();

                break;

            case R.id.txt_boostNow_catMgt:

                new DigitalGlobal().hideKeyboard(getActivity());

                //todo for add boost category
                if (boostValueSpinner.getSelectedItemPosition() == -1 || boostValueSpinner.getSelectedItemPosition() == 0) {

                    new CustomAlert().showAlert(getActivity(), 0, "Select boost value");
                    return;

                }

                if (txt_boostTask_catMgt.isSelected()) {

                    if (taskPos > 0) {

                        addTaskBoost(taskModelArrayList.get(taskPos).getId());

                    } else {

                        addTaskBoost(taskModelArrayList.get(0).getId());

                    }

                } else if (txt_boostCategory_catMgt.isSelected()) {

                    if (cattPos != -1) {

                        addCatogoryBoost(categoryModelArrayList.get(cattPos).getId());

                    } else {

                        addCatogoryBoost(categoryModelArrayList.get(0).getId());

                    }

                }

                break;

            case R.id.relLayout_back_button_categoryMgt:

                getActivity().onBackPressed();

                break;

            case R.id.img_search_catMgt:

                Log.e("clik", "okk");
                new DigitalGlobal().hideKeyboard(getActivity());

                if (edt_search_catMgt.getText().toString().trim().length() != 0) {

                    if (value == 1) {

                        searchTask(edt_search_catMgt.getText().toString().trim());
                        edt_search_catMgt.setText("");
                        edt_search_catMgt.setHint("Search...");

                    } else if (value == 0 || value == 2 || value == 4) {

                        searchCategory(edt_search_catMgt.getText().toString().trim());
                        edt_search_catMgt.setText("");
                        edt_search_catMgt.setHint("Search...");

                    }

                } else {

                    new CustomAlert().showAlert(getActivity(), 0, "Enter something to search");

                }

                break;

        }

    }

    private void addTaskBoost(String id) {

        Log.e("boosttaskval", boostValueSpinner.getSelectedItemPosition() + 1 + "//" + id);
        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.boostTaskResponse(Global.acceptHeader, Global.token, "v1/tasks/" + id + "/addBonusPoint.json", childId, String.valueOf(boostValueSpinner.getSelectedItemPosition()));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                pd.cancel();

                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }
                {

                    String res = response.body().toString();

                    try {


                        JSONObject jsonObject = new JSONObject(res);
                        Log.e("taskboost", jsonObject.toString().trim());
                        if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                            new CustomAlert().showAlert(getActivity(), 1, jsonObject.optString("message"));
                        } else if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("unauthenticated")) {
                            Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                            startActivity(loginIntent);
                            getActivity().finish();


                        } else {
                            new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pd.cancel();

                //  Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

    private void addCatogoryBoost(String catId) {

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Please wait...", true);
        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call = service.boostTaskResponse(Global.acceptHeader, Global.token, "v1/categories/" + catId + "/addBonusPoint.json", childId, String.valueOf(boostValueSpinner.getSelectedItemPosition()));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                    getActivity().startActivity(loginIntent);
                    getActivity().finish();

                }
                {

                    String res = response.body().toString();

                    try {

                        pd.cancel();

                        JSONObject jsonObject = new JSONObject(res);
                        Log.e("catboost", jsonObject.toString().trim());
                        if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                            new CustomAlert().showAlert(getActivity(), 1, jsonObject.optString("message"));
                        } else if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("unauthenticated")) {
                            Intent loginIntent = new Intent(getActivity(), LoginScreen.class);
                            startActivity(loginIntent);
                            getActivity().finish();


                        } else {
                            new CustomAlert().showAlert(getActivity(), 0, jsonObject.optString("message"));
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();

                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                pd.cancel();
                //Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                new CustomAlert().showAlert(getActivity(), 0, "Failure");

            }

        });

    }

}
