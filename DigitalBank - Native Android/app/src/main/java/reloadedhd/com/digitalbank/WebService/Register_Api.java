package reloadedhd.com.digitalbank.WebService;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import reloadedhd.com.digitalbank.Activity.LoginScreen;
import reloadedhd.com.digitalbank.Activity.RegisterScreen;
import reloadedhd.com.digitalbank.R;
import reloadedhd.com.digitalbank.customAlert.CustomAlert;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 5/28/2018.
 */

public class Register_Api {
    Context context;
    String user_name, email, mobile, password, type;
    ProgressDialog progressDialog;

    public void register(RegisterScreen registerScreen, String user_name, String email, String mobile, String password, String type) {
        this.context = registerScreen;
        this.user_name = user_name;
        this.email = email;
        this.mobile = mobile;
        this.password = password;
        this.type = type;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        Log.e("Fsgedf", user_name);
        Log.e("Fsgedf", email);

        Service service = Service.retrofit.create(Service.class);
        Call<JsonObject> call;
        //Call<JsonObject> call = service.register(user_name,email,type,password);
        if (email != null && !email.equalsIgnoreCase(" ") && email.length() > 0) {
            call = service.register(user_name, email, type, password, mobile);
        } else {
            call = service.registerOne(user_name, type, password, mobile);
        }


        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressDialog.dismiss();

            //    String messageIsSucess = response.isSuccessful() + "";
             //   if (messageIsSucess.equalsIgnoreCase("true")) {

                    try {

                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        Log.e("Fsgedf", jsonObject.toString() + "");
                        String status = jsonObject.optString("status").toString();

                        if (status.equalsIgnoreCase("success")) {

                            Toast.makeText(context, jsonObject.get("message") + "", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, LoginScreen.class);
                            context.startActivity(intent);
                            ((Activity) context).finish();

                        } else if (status.equalsIgnoreCase("error")) {

                            JSONObject jsonObject1 = jsonObject.getJSONObject("message");

                            new CustomAlert().showAlert(context,0,jsonObject.optString("message"));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();

            }

        });

    }

}
