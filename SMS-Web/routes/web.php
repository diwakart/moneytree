<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Auth::routes();

//Route::get('/LL/getallschool', 'Admin\CommonController@index')->name('test');

// Here is the API
Route::get('API/lesson/getLsubL', 'Admin\CommonController@getLsubL');
Route::get('API/lesson/getLesson/{level}/{sublevel}', 'Admin\CommonController@getLesson');

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['isAdmin','permission']], function () {

	/***************************Profile*******************/

	Route::get('/profile', 'HomeController@profile')->name('profilePage');
	Route::post('update_profile', 'HomeController@updateProfile')->name('updateProfile');
	Route::post('update_password', 'HomeController@updatePassword')->name('updatePassword');

	/*******************Countries***********************/
	Route::get('/countries', 'Admin\CountriesController@index')->name('viewCountry');
	Route::post('countries/store', 'Admin\CountriesController@store')->name('saveCountry');
	Route::get('countries/edit', 'Admin\CountriesController@edit')->name('EditCountry');
	Route::post('countries/destroy', 'Admin\CountriesController@destroy')->name('DeleteCountry');
	Route::get('countries/getAllCountry', 'Admin\CountriesController@getAllCountry')->name('getAllCountry');
	Route::post('countries/activate', 'Admin\CountriesController@activate')->name('activateCountry');
	Route::post('countries/deactivate', 'Admin\CountriesController@deactivate')->name('deactivateCountry');
	/************************GET TL,LL,School for all system route***********/
	Route::post('countries/getallTLs', 'Admin\CommonController@getallTLs')->name('getAllCountryTls');
	Route::post('territory-licensee/getallLLs', 'Admin\CommonController@getallLLs')->name('getAllLLOfTL');
	Route::post('/LL/getallschools', 'Admin\CommonController@getallschools')->name('getallLLschools');
	Route::get('/school/{id}/getAllStudentsOfSchool', 'Admin\CommonController@getAllStudentsOfSchool')->name('getAllStudentsOfSchool');
	Route::get('/school/{id}/getAllTeachers', 'Admin\CommonController@getTecherFromSchool')->name('getTecherFromSchool');

	/***********************Module Territory Licensee***************************/
	Route::get('territory-licensee', 'Admin\TerritoryLicenseeController@index')->name('viewTL');
	Route::post('getCountryCode', 'Admin\TerritoryLicenseeController@getCountryCode')->name('getCountryCode');
	Route::post('territory-licensee/store', 'Admin\TerritoryLicenseeController@store')->name('saveTL');
	Route::get('territory-licensee/show', 'Admin\TerritoryLicenseeController@show')->name('getAllTLs');
	Route::post('territory-licensee/destroy', 'Admin\TerritoryLicenseeController@destroy')->name('deleteTL');
	Route::get('territory-licensee/changeTLStatus', 'Admin\TerritoryLicenseeController@changeTLStatus')->name('changeTLStatus');
	Route::get('territory-licensee/edit', 'Admin\TerritoryLicenseeController@edit')->name('editTL');

	/*******************Module Location Licensee************************/
	Route::get('location-licensee', 'Admin\LocationLicenseeController@index')->name('viewLL');
	Route::post('location-licensee/store', 'Admin\LocationLicenseeController@store')->name('saveLL');
	Route::get('location-licensee/show', 'Admin\LocationLicenseeController@show')->name('getAllLLs');
	Route::post('location-licensee/destroy', 'Admin\LocationLicenseeController@destroy')->name('deleteLL');
	Route::get('location-licensee/changeLLStatus', 'Admin\LocationLicenseeController@changeLLStatus')->name('changeLLStatus');
	Route::get('location-licensee/edit', 'Admin\LocationLicenseeController@edit')->name('editLL');

	/*************************Module School Licensee***************************/
	Route::get('school', 'Admin\SchoolController@index')->name('viewSchool');
	Route::get('school/show', 'Admin\SchoolController@show')->name('getAllSchools');

	Route::post('school/store', 'Admin\SchoolController@store')->name('saveSchool');

	Route::get('school/edit', 'Admin\SchoolController@edit')->name('editSchool');
	Route::post('school/destroy', 'Admin\SchoolController@destroy')->name('deleteSchool');
	Route::post('school/getallschools', 'Admin\SchoolController@getallschools')->name('getAllSchools');
	Route::get('school/changeSchoolStatus', 'Admin\SchoolController@changeSchoolStatus')->name('changeSchoolStatus');

	Route::post('school/getallTeachers', 'Admin\SchoolController@getallTeachers')->name('getAllTeachersOfSchool');

	/**********************Module Teacher*****************************/
	Route::get('teacher', 'Admin\TeacherController@index')->name('viewTeacher');
	
	Route::get('teacher/show', 'Admin\TeacherController@show')->name('getAllTeachers');


	Route::post('teacher/store', 'Admin\TeacherController@store')->name('saveTeacher');
	Route::get('teacher/show', 'Admin\TeacherController@show')->name('getAllTeachers');

	Route::get('teacher/edit', 'Admin\TeacherController@edit')->name('editTeacher');
	Route::post('teacher/destroy', 'Admin\TeacherController@destroy')->name('deleteTeacher');
	// Route::get('teacher/getallteachers', 'Admin\TeacherController@getallteachers')->name('getAllTeachers');
	Route::get('teacher/changeTeacherStatus', 'Admin\SchoolController@changeTeacherStatus')->name('changeTeacherStatus');

	Route::post('teacher/uploadcsv','Admin\TeacherController@uploadcsv')->name('teacherUpload');

	/*************************Module Parent***************************/
	Route::get('parent', 'Admin\ParentController@index')->name('viewParent');	
	Route::post('/parent_upload', 'Admin\ParentController@uploadParentPic')->name('uploadParentPic');
	Route::get('parent/show', 'Admin\ParentController@show')->name('getAllParents');

	Route::post('parent/store', 'Admin\ParentController@store')->name('saveParent');
	Route::get('parent/edit', 'Admin\ParentController@edit')->name('editParent');
	Route::post('parent/destroy', 'Admin\ParentController@destroy')->name('deleteParent');
	Route::get('parent/changeParentStatus', 'Admin\SchoolController@changeParentStatus')->name('changeParentStatus');
	


	/**********************Module Student*****************************/
	Route::get('student', 'Admin\StudentController@index')->name('viewStudent');
	Route::get('student/show', 'Admin\StudentController@show')->name('getAllStudents');

	Route::post('student/store', 'Admin\StudentController@store')->name('saveStudent');
	Route::get('student/edit', 'Admin\StudentController@detail')->name('editStudent');
	Route::post('student/destroy', 'Admin\StudentController@destroy')->name('deleteStudent');
	Route::post('parent/tag', 'Admin\StudentController@tagParent')->name('tagParent');
	Route::get('student/changeStudentStatus', 'Admin\StudentController@changeStudentStatus')->name('changeStudentStatus');	
	Route::post('class/tagclass', 'Admin\StudentController@assignClass')->name('assignClass');

	/**********************Module class*****************************/
	Route::get('class', 'Admin\ClassController@index')->name('viewClass');
	Route::get('class/show', 'Admin\ClassController@show')->name('getAllClass');
    Route::get('class/add', 'Admin\ClassController@addClassView')->name('addClassView');
	Route::post('class/store', 'Admin\ClassController@store')->name('saveClass');
	Route::get('class/edit/{id}', 'Admin\ClassController@EditClassView')->name('editClass');
	Route::post('class/destroy', 'Admin\ClassController@destroy')->name('deleteClass');
	Route::get('class/changeClassStatus', 'Admin\ClassController@changeClassStatus')->name('changeClassStatus');		
	
	/**********************Module Student Attendance*****************************/
	Route::get('view-attendance', 'Admin\AttendanceController@index')->name('view-attendance');
	Route::get('view-attendance/show', 'Admin\AttendanceController@show')->name('showAllAttendance');
	Route::get('add_attendance/class', 'Admin\AttendanceController@addAttendance')->name('addAttendance');

	Route::post('attempt/attendence', 'Admin\ClassController@AttemptAttendence');
	Route::get('attendence/delete/{classid}', 'Admin\AttendanceController@DeleteAttendance');
	Route::get('class/{id}/getAllClassChild', 'Admin\AttendanceController@getAllClassChild')->name('getAllClassChild');
	Route::post('class/submit_attendance', 'Admin\AttendanceController@submit_attendance')->name('submit_attendance');

	/**********************Module Report*****************************/
	Route::get('report', 'Admin\ReportController@index')->name('report');
	/*Route::get('territory-licensee/show', 'Admin\ReportController@show')->name('territory-licensee.show');*/
	Route::get('viewreport', 'Admin\ReportController@viewReport')->name('viewReportPage');

	/**********************Module Permission*****************************/
	Route::get('permission_module', 'Admin\PermissionController@index')->name('permission_module');

	Route::get('module_assignment', 'Admin\PermissionController@module_assignment')->name('module_assignment');
	Route::post('module_assignment/store', 'Admin\PermissionController@module_assignment_store')->name('module_assignment.store');
	Route::get('module_assignment/show', 'Admin\PermissionController@module_assignment_show')->name('module_assignment.show');
	Route::get('module_assignment/edit', 'Admin\PermissionController@module_assignment_edit')->name('module_assignment.edit');
	Route::post('module_assignment/destroy', 'Admin\PermissionController@module_assignment_destroy')->name('module_assignment.destroy');
	Route::get('module_assignment/create', 'Admin\PermissionController@module_assignment_create')->name('module_assignment.create');

	Route::get('module', 'Admin\PermissionController@module')->name('module');
	Route::post('module/store', 'Admin\PermissionController@module_store')->name('module.store');
	Route::get('module/show', 'Admin\PermissionController@module_show')->name('module.show');
	Route::get('module/edit', 'Admin\PermissionController@module_edit')->name('module.edit');
	Route::post('module/destroy', 'Admin\PermissionController@module_destroy')->name('module.destroy');
	Route::get('module/create', 'Admin\PermissionController@module_create')->name('module.create');

	Route::get('role', 'Admin\PermissionController@role')->name('role');
	Route::post('role/store', 'Admin\PermissionController@role_store')->name('role.store');
	Route::get('role/show', 'Admin\PermissionController@role_show')->name('role.show');
	Route::get('role/details', 'Admin\PermissionController@getRoleDetails')->name('role.edit');
	Route::post('role/destroy', 'Admin\PermissionController@role_destroy')->name('role.destroy');
	Route::get('role/create', 'Admin\PermissionController@role_create')->name('role.create');

	
	Route::get('route_permission', 'Admin\PermissionController@route_permission')->name('route_permission');
	Route::post('route_permission/store', 'Admin\PermissionController@route_permission_store')->name('route_permission.store');
	Route::get('route_permission/show', 'Admin\PermissionController@route_permission_show')->name('route_permission.show');
	Route::get('route_permission/edit', 'Admin\PermissionController@route_permission_edit')->name('route_permission.edit');
	Route::post('route_permission/destroy', 'Admin\PermissionController@route_permission_destroy')->name('route_permission.destroy');
	Route::get('route_permission/create', 'Admin\PermissionController@route_permission_create')->name('routes.create');


	Route::get('user_restriction_module', 'Admin\PermissionController@user_restriction_module')->name('user_restriction_module');

	Route::post('user_restriction_module/store', 'Admin\PermissionController@user_restriction_module_store')->name('user_restriction_module.store');

	Route::get('user_restriction_module/show', 'Admin\PermissionController@user_restriction_module_show')->name('user_restriction_module.show');

	Route::get('user_restriction_module/edit', 'Admin\PermissionController@user_restriction_module_edit')->name('user_restriction_module.edit');

	Route::post('user_restriction_module/destroy', 'Admin\PermissionController@user_restriction_module_destroy')->name('user_restriction_module.destroy');


	/*****************************************Attendance View*************************************/
	Route::get('school/getclass/{id}', 'Admin\ClassController@getClassbySchool');
	Route::get('school/getteacher/{id}', 'Admin\ClassController@getClassbyTeacher');
	
	//Related to report
	Route::post('School/getallclass', 'Admin\CommonController@getAllClass');
	Route::post('Class/getalllessons', 'Admin\CommonController@getAllLesson');
	Route::post('Class/getallteachers', 'Admin\CommonController@getallteachers');
		
});