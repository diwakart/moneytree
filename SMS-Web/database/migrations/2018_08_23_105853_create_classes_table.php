<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',100);
            $table->unsignedInteger('school_id');
            $table->foreign('school_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('lesson_id');
            $table->string('teacher_id');
            $table->string('student_id')->nullable();
            $table->date('start_date')->format('d.m.Y')->nullable();
            $table->date('end_date')->format('d.m.Y')->nullable();
            $table->enum('attendance_marked', [1,2])->comment('marked->1, not_marked->2')->default(2);
            $table->enum('status', [1,2])->comment('active->1, inactive->2')->default(1);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class');
    }
}
