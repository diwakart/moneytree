<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('email',100)->unique()->nullable();
            $table->string('country_code',10)->nullable();
            $table->bigInteger('contact')->unique()->nullable();
            $table->enum('user_type', ['1', '2','3','4','5','6','7'])->nullable()->comment('admin->1, tl_user->2, ll_user->3, school->4, teacher->5, parent->6, student->7');
            $table->string('password');
            $table->text('picture')->nullable();
            $table->enum('status', ['1','2'])->default('1')->comment('Active->1, Inactive->2');
            $table->rememberToken();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
