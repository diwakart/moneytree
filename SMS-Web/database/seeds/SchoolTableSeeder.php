<?php

use Illuminate\Database\Seeder;

class SchoolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schools')->insert([
    		[
    			'id' => '1',
    			'user_id' => '4',
    			'tl_id' => '2',
    			'll_id' => '3',    			
    		]
    	]);
    }
}
