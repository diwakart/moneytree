<?php

use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert([
    		[
    			'id' => '1',
    			'user_id' => '7',    			
    			'school_id' => '4',
    		]
    	]);
    }
}
