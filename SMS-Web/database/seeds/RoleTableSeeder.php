<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert(array (
            
            0 => 
            array (
                'role' => 'Admin',
            ),
            1 => 
            array (
                'role' => 'TL',
            ),
            2 => 
            array (
                'role' => 'LL',
            ),
            3 => 
            array (
                'role' => 'School',
            ),
            4 => 
            array (
                'role' => 'Teacher',
            ),
            5 => 
            array (
                'role' => 'Parent',
            )
        )); 
    }
}
