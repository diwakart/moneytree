<?php

use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('modules')->delete();
        
        \DB::table('modules')->insert(array (
            0 => 
            array (
                'module_name' => 'CountryModule',
            ),
            2 => 
            array (
                'module_name' => 'TLModule',
            ),
            3 => 
            array (
                'module_name' => 'LLModule',
            ),
            4 => 
            array (
                'module_name' => 'SchoolModule',
            ),
            5 => 
            array (
                'module_name' => 'TeacherModule',
            ),
            6 => 
            array (
                'module_name' => 'ParentModule',
            ),
            7 =>
            array (
                'module_name' => 'StudentModule',
            ),
            8 =>
            array (
                'module_name' => 'ClassModule',
            ),
            9 =>
            array (
                'module_name' => 'AttendanceModule',
            ),
            10 =>
            array (
                'module_name' => 'PermissionModule',
            ),
            11 =>
            array (
                'module_name' => 'SchoolReportModule',
            ),
            12 =>
            array (
                'module_name' => 'ClassReportModule',
            ),
            13 =>
            array (
                'module_name' => 'LessonReportModule',
            ),
            14 =>
            array (
                'module_name' => 'TeacherReportModule',
            )
        ));
    }
}
