<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('countries')->delete();
        
        \DB::table('countries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'AF',
                'name' => 'Afghanistan',
                'country_code' => '93',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'AX',
                'name' => 'Åland Islands',
                'country_code' => '358',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'AL',
                'name' => 'Albania',
                'country_code' => '355',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'DZ',
                'name' => 'Algeria',
                'country_code' => '213',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'AS',
                'name' => 'American Samoa',
                'country_code' => '1-684',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'AD',
                'name' => 'Andorra',
                'country_code' => '376',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'AO',
                'name' => 'Angola',
                'country_code' => '244',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'code' => 'AI',
                'name' => 'Anguilla',
                'country_code' => '1-264',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'code' => 'AQ',
                'name' => 'Antarctica',
                'country_code' => '672',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'code' => 'AG',
                'name' => 'Antigua and Barbuda',
                'country_code' => '1-268',
                'default_country' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-06-28 06:28:17',
            ),
            10 => 
            array (
                'id' => 11,
                'code' => 'AR',
                'name' => 'Argentina',
                'country_code' => '54',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'code' => 'AM',
                'name' => 'Armenia',
                'country_code' => '374',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'code' => 'AW',
                'name' => 'Aruba',
                'country_code' => '297',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'code' => 'AU',
                'name' => 'Australia',
                'country_code' => '61',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'code' => 'AT',
                'name' => 'Austria',
                'country_code' => '43',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'code' => 'AZ',
                'name' => 'Azerbaijan',
                'country_code' => '994',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'code' => 'BS',
                'name' => 'Bahamas',
                'country_code' => '1-242',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'code' => 'BH',
                'name' => 'Bahrain',
                'country_code' => '973',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'code' => 'BD',
                'name' => 'Bangladesh',
                'country_code' => '880',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'code' => 'BB',
                'name' => 'Barbados',
                'country_code' => '1-246',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'code' => 'BY',
                'name' => 'Belarus',
                'country_code' => '375',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'code' => 'BE',
                'name' => 'Belgium',
                'country_code' => '32',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'code' => 'BZ',
                'name' => 'Belize',
                'country_code' => '501',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'code' => 'BJ',
                'name' => 'Benin',
                'country_code' => '229',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'code' => 'BM',
                'name' => 'Bermuda',
                'country_code' => '1-441',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'code' => 'BT',
                'name' => 'Bhutan',
                'country_code' => '975',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'code' => 'BO',
                'name' => 'Bolivia, Plurinational State of',
                'country_code' => '591',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'code' => 'BQ',
                'name' => 'Bonaire, Sint Eustatius and Saba',
                'country_code' => '599',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'code' => 'BA',
                'name' => 'Bosnia and Herzegovina',
                'country_code' => '387',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'code' => 'BW',
                'name' => 'Botswana',
                'country_code' => '267',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'code' => 'BV',
                'name' => 'Bouvet Island',
                'country_code' => '',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'code' => 'BR',
                'name' => 'Brazil',
                'country_code' => '55',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'code' => 'IO',
                'name' => 'British Indian Ocean Territory',
                'country_code' => '246',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'code' => 'BN',
                'name' => 'Brunei Darussalam',
                'country_code' => '673',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'code' => 'BG',
                'name' => 'Bulgaria',
                'country_code' => '359',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'code' => 'BF',
                'name' => 'Burkina Faso',
                'country_code' => '226',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'code' => 'BI',
                'name' => 'Burundi',
                'country_code' => '257',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'code' => 'KH',
                'name' => 'Cambodia',
                'country_code' => '855',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'code' => 'CM',
                'name' => 'Cameroon',
                'country_code' => '237',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'code' => 'CA',
                'name' => 'Canada',
                'country_code' => '1',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'code' => 'CV',
                'name' => 'Cape Verde',
                'country_code' => '238',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'code' => 'KY',
                'name' => 'Cayman Islands',
                'country_code' => '1-345',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'code' => 'CF',
                'name' => 'Central African Republic',
                'country_code' => '236',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'code' => 'TD',
                'name' => 'Chad',
                'country_code' => '235',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'code' => 'CL',
                'name' => 'Chile',
                'country_code' => '56',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'code' => 'CN',
                'name' => 'China',
                'country_code' => '86',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'code' => 'CX',
                'name' => 'Christmas Island',
                'country_code' => '61',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'code' => 'CC',
            'name' => 'Cocos (Keeling) Islands',
                'country_code' => '61',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'code' => 'CO',
                'name' => 'Colombia',
                'country_code' => '57',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'code' => 'KM',
                'name' => 'Comoros',
                'country_code' => '269',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'code' => 'CG',
                'name' => 'Congo',
                'country_code' => '242',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'code' => 'CD',
                'name' => 'Congo, the Democratic Republic of the',
                'country_code' => '243',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'code' => 'CK',
                'name' => 'Cook Islands',
                'country_code' => '682',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'code' => 'CR',
                'name' => 'Costa Rica',
                'country_code' => '506',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'code' => 'CI',
                'name' => 'Côte d\'Ivoire',
                'country_code' => '225',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'code' => 'HR',
                'name' => 'Croatia',
                'country_code' => '385',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'code' => 'CU',
                'name' => 'Cuba',
                'country_code' => '53',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'code' => 'CW',
                'name' => 'Curaçao',
                'country_code' => '599',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'code' => 'CY',
                'name' => 'Cyprus',
                'country_code' => '357',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'code' => 'CZ',
                'name' => 'Czech Republic',
                'country_code' => '420',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'code' => 'DK',
                'name' => 'Denmark',
                'country_code' => '45',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'code' => 'DJ',
                'name' => 'Djibouti',
                'country_code' => '253',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'code' => 'DM',
                'name' => 'Dominica',
                'country_code' => '1-767',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'code' => 'DO',
                'name' => 'Dominican Republic',
                'country_code' => '1-809, 1-829, 1-849',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'code' => 'EC',
                'name' => 'Ecuador',
                'country_code' => '593',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'code' => 'EG',
                'name' => 'Egypt',
                'country_code' => '20',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'code' => 'SV',
                'name' => 'El Salvador',
                'country_code' => '503',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'code' => 'GQ',
                'name' => 'Equatorial Guinea',
                'country_code' => '240',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'code' => 'ER',
                'name' => 'Eritrea',
                'country_code' => '291',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'code' => 'EE',
                'name' => 'Estonia',
                'country_code' => '372',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'code' => 'ET',
                'name' => 'Ethiopia',
                'country_code' => '251',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'code' => 'FK',
            'name' => 'Falkland Islands (Malvinas)',
                'country_code' => '500',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'code' => 'FO',
                'name' => 'Faroe Islands',
                'country_code' => '298',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'code' => 'FJ',
                'name' => 'Fiji',
                'country_code' => '679   ',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'code' => 'FI',
                'name' => 'Finland',
                'country_code' => '358',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'code' => 'FR',
                'name' => 'France',
                'country_code' => '33',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'code' => 'GF',
                'name' => 'French Guiana',
                'country_code' => '689',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'code' => 'PF',
                'name' => 'French Polynesia',
                'country_code' => '689',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'code' => 'TF',
                'name' => 'French Southern Territories',
                'country_code' => '262',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'code' => 'GA',
                'name' => 'Gabon',
                'country_code' => '241',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'code' => 'GM',
                'name' => 'Gambia',
                'country_code' => '220',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'code' => 'GE',
                'name' => 'Georgia',
                'country_code' => '995',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'code' => 'DE',
                'name' => 'Germany',
                'country_code' => '49',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'code' => 'GH',
                'name' => 'Ghana',
                'country_code' => '233',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'code' => 'GI',
                'name' => 'Gibraltar',
                'country_code' => '350',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'code' => 'GR',
                'name' => 'Greece',
                'country_code' => '30',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'code' => 'GL',
                'name' => 'Greenland',
                'country_code' => '299',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'code' => 'GD',
                'name' => 'Grenada',
                'country_code' => '1-473',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'code' => 'GP',
                'name' => 'Guadeloupe',
                'country_code' => '502',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'code' => 'GU',
                'name' => 'Guam',
                'country_code' => '1-671',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'code' => 'GT',
                'name' => 'Guatemala',
                'country_code' => '502',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'code' => 'GG',
                'name' => 'Guernsey',
                'country_code' => '44-1481',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'code' => 'GN',
                'name' => 'Guinea',
                'country_code' => '224',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'code' => 'GW',
                'name' => 'Guinea-Bissau',
                'country_code' => '245',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'code' => 'GY',
                'name' => 'Guyana',
                'country_code' => '592',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'code' => 'HT',
                'name' => 'Haiti',
                'country_code' => '509',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'code' => 'HM',
                'name' => 'Heard Island and McDonald Mcdonald Islands',
                'country_code' => '',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'code' => 'VA',
            'name' => 'Holy See (Vatican City State)',
                'country_code' => '379',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'code' => 'HN',
                'name' => 'Honduras',
                'country_code' => '504',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'code' => 'HK',
                'name' => 'Hong Kong',
                'country_code' => '852',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'code' => 'HU',
                'name' => 'Hungary',
                'country_code' => '36',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'code' => 'IS',
                'name' => 'Iceland',
                'country_code' => '354',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'code' => 'IN',
                'name' => 'India',
                'country_code' => '91',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'code' => 'ID',
                'name' => 'Indonesia',
                'country_code' => '62',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'code' => 'IR',
                'name' => 'Iran, Islamic Republic of',
                'country_code' => '98',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'code' => 'IQ',
                'name' => 'Iraq',
                'country_code' => '964',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'code' => 'IE',
                'name' => 'Ireland',
                'country_code' => '353',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'code' => 'IM',
                'name' => 'Isle of Man',
                'country_code' => '44-1624',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'code' => 'IL',
                'name' => 'Israel',
                'country_code' => '972',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'code' => 'IT',
                'name' => 'Italy',
                'country_code' => '39',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'code' => 'JM',
                'name' => 'Jamaica',
                'country_code' => '1-876',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'code' => 'JP',
                'name' => 'Japan',
                'country_code' => '81',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'code' => 'JE',
                'name' => 'Jersey',
                'country_code' => '44-1534',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'code' => 'JO',
                'name' => 'Jordan',
                'country_code' => '962',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'code' => 'KZ',
                'name' => 'Kazakhstan',
                'country_code' => '7',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'code' => 'KE',
                'name' => 'Kenya',
                'country_code' => '254',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'code' => 'KI',
                'name' => 'Kiribati',
                'country_code' => '686',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'code' => 'KP',
                'name' => 'Korea, Democratic People\'s Republic of',
                'country_code' => '850',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'code' => 'KR',
                'name' => 'Korea, Republic of',
                'country_code' => '850',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => 120,
                'code' => 'KW',
                'name' => 'Kuwait',
                'country_code' => '965',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => 121,
                'code' => 'KG',
                'name' => 'Kyrgyzstan',
                'country_code' => '996',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => 122,
                'code' => 'LA',
                'name' => 'Lao People\'s Democratic Republic',
                'country_code' => '856',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => 123,
                'code' => 'LV',
                'name' => 'Latvia',
                'country_code' => '371',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => 124,
                'code' => 'LB',
                'name' => 'Lebanon',
                'country_code' => '961',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => 125,
                'code' => 'LS',
                'name' => 'Lesotho',
                'country_code' => '266',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => 126,
                'code' => 'LR',
                'name' => 'Liberia',
                'country_code' => '231',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => 127,
                'code' => 'LY',
                'name' => 'Libya',
                'country_code' => '218',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => 128,
                'code' => 'LI',
                'name' => 'Liechtenstein',
                'country_code' => '423',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => 129,
                'code' => 'LT',
                'name' => 'Lithuania',
                'country_code' => '370',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => 130,
                'code' => 'LU',
                'name' => 'Luxembourg',
                'country_code' => '352',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => 131,
                'code' => 'MO',
                'name' => 'Macao',
                'country_code' => '853',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => 132,
                'code' => 'MK',
                'name' => 'Macedonia, the Former Yugoslav Republic of',
                'country_code' => '389',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => 133,
                'code' => 'MG',
                'name' => 'Madagascar',
                'country_code' => '261',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => 134,
                'code' => 'MW',
                'name' => 'Malawi',
                'country_code' => '265',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => 135,
                'code' => 'MY',
                'name' => 'Malaysia',
                'country_code' => '60',
                'default_country' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-06-28 23:37:53',
            ),
            135 => 
            array (
                'id' => 136,
                'code' => 'MV',
                'name' => 'Maldives',
                'country_code' => '960',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => 137,
                'code' => 'ML',
                'name' => 'Mali',
                'country_code' => '223',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => 138,
                'code' => 'MT',
                'name' => 'Malta',
                'country_code' => '356',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => 139,
                'code' => 'MH',
                'name' => 'Marshall Islands',
                'country_code' => '692',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => 140,
                'code' => 'MQ',
                'name' => 'Martinique',
                'country_code' => '596',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => 141,
                'code' => 'MR',
                'name' => 'Mauritania',
                'country_code' => '222',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => 142,
                'code' => 'MU',
                'name' => 'Mauritius',
                'country_code' => '230',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => 143,
                'code' => 'YT',
                'name' => 'Mayotte',
                'country_code' => '262',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => 144,
                'code' => 'MX',
                'name' => 'Mexico',
                'country_code' => '52',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => 145,
                'code' => 'FM',
                'name' => 'Micronesia, Federated States of',
                'country_code' => '691',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => 146,
                'code' => 'MD',
                'name' => 'Moldova, Republic of',
                'country_code' => '373',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => 147,
                'code' => 'MC',
                'name' => 'Monaco',
                'country_code' => '377',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => 148,
                'code' => 'MN',
                'name' => 'Mongolia',
                'country_code' => '976',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => 149,
                'code' => 'ME',
                'name' => 'Montenegro',
                'country_code' => '382',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => 150,
                'code' => 'MS',
                'name' => 'Montserrat',
                'country_code' => '1-664',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => 151,
                'code' => 'MA',
                'name' => 'Morocco',
                'country_code' => '212',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => 152,
                'code' => 'MZ',
                'name' => 'Mozambique',
                'country_code' => '258',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => 153,
                'code' => 'MM',
                'name' => 'Myanmar',
                'country_code' => '95',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => 154,
                'code' => 'NA',
                'name' => 'Namibia',
                'country_code' => '264',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => 155,
                'code' => 'NR',
                'name' => 'Nauru',
                'country_code' => '674',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => 156,
                'code' => 'NP',
                'name' => 'Nepal',
                'country_code' => '977',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => 157,
                'code' => 'NL',
                'name' => 'Netherlands',
                'country_code' => '31',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => 158,
                'code' => 'NC',
                'name' => 'New Caledonia',
                'country_code' => '687',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => 159,
                'code' => 'NZ',
                'name' => 'New Zealand',
                'country_code' => '64',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => 160,
                'code' => 'NI',
                'name' => 'Nicaragua',
                'country_code' => '505',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => 161,
                'code' => 'NE',
                'name' => 'Niger',
                'country_code' => '227',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => 162,
                'code' => 'NG',
                'name' => 'Nigeria',
                'country_code' => '234',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => 163,
                'code' => 'NU',
                'name' => 'Niue',
                'country_code' => '683',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => 164,
                'code' => 'NF',
                'name' => 'Norfolk Island',
                'country_code' => '672',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => 165,
                'code' => 'MP',
                'name' => 'Northern Mariana Islands',
                'country_code' => '1-670',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => 166,
                'code' => 'NO',
                'name' => 'Norway',
                'country_code' => '47',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => 167,
                'code' => 'OM',
                'name' => 'Oman',
                'country_code' => '968',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => 168,
                'code' => 'PK',
                'name' => 'Pakistan',
                'country_code' => '92',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => 169,
                'code' => 'PW',
                'name' => 'Palau',
                'country_code' => '680',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => 170,
                'code' => 'PS',
                'name' => 'Palestine, State of',
                'country_code' => '970',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => 171,
                'code' => 'PA',
                'name' => 'Panama',
                'country_code' => '507',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => 172,
                'code' => 'PG',
                'name' => 'Papua New Guinea',
                'country_code' => '675',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => 173,
                'code' => 'PY',
                'name' => 'Paraguay',
                'country_code' => '595',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => 174,
                'code' => 'PE',
                'name' => 'Peru',
                'country_code' => '51',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => 175,
                'code' => 'PH',
                'name' => 'Philippines',
                'country_code' => '63',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => 176,
                'code' => 'PN',
                'name' => 'Pitcairn',
                'country_code' => '      64',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => 177,
                'code' => 'PL',
                'name' => 'Poland',
                'country_code' => '48',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => 178,
                'code' => 'PT',
                'name' => 'Portugal',
                'country_code' => '351',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => 179,
                'code' => 'PR',
                'name' => 'Puerto Rico',
                'country_code' => '1-787, 1-939',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => 180,
                'code' => 'QA',
                'name' => 'Qatar',
                'country_code' => '974',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => 181,
                'code' => 'RE',
                'name' => 'Réunion',
                'country_code' => '262',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => 182,
                'code' => 'RO',
                'name' => 'Romania',
                'country_code' => '40',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => 183,
                'code' => 'RU',
                'name' => 'Russian Federation',
                'country_code' => '7',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => 184,
                'code' => 'RW',
                'name' => 'Rwanda',
                'country_code' => '250',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => 185,
                'code' => 'BL',
                'name' => 'Saint Barthélemy',
                'country_code' => '590',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => 186,
                'code' => 'SH',
                'name' => 'Saint Helena, Ascension and Tristan da Cunha',
                'country_code' => '290',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => 187,
                'code' => 'KN',
                'name' => 'Saint Kitts and Nevis',
                'country_code' => '1-869',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => 188,
                'code' => 'LC',
                'name' => 'Saint Lucia',
                'country_code' => '1-758',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => 189,
                'code' => 'MF',
            'name' => 'Saint Martin (French part)',
                'country_code' => '590',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => 190,
                'code' => 'PM',
                'name' => 'Saint Pierre and Miquelon',
                'country_code' => '508',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => 191,
                'code' => 'VC',
                'name' => 'Saint Vincent and the Grenadines',
                'country_code' => '1-784',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => 192,
                'code' => 'WS',
                'name' => 'Samoa',
                'country_code' => '685',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => 193,
                'code' => 'SM',
                'name' => 'San Marino',
                'country_code' => '378',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => 194,
                'code' => 'ST',
                'name' => 'Sao Tome and Principe',
                'country_code' => '239',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => 195,
                'code' => 'SA',
                'name' => 'Saudi Arabia',
                'country_code' => '966',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => 196,
                'code' => 'SN',
                'name' => 'Senegal',
                'country_code' => '221',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => 197,
                'code' => 'RS',
                'name' => 'Serbia',
                'country_code' => '381',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => 198,
                'code' => 'SC',
                'name' => 'Seychelles',
                'country_code' => '248',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => 199,
                'code' => 'SL',
                'name' => 'Sierra Leone',
                'country_code' => '232',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => 200,
                'code' => 'SG',
                'name' => 'Singapore',
                'country_code' => '65',
                'default_country' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => 201,
                'code' => 'SX',
            'name' => 'Sint Maarten (Dutch part)',
                'country_code' => '1-721',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => 202,
                'code' => 'SK',
                'name' => 'Slovakia',
                'country_code' => '421',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => 203,
                'code' => 'SI',
                'name' => 'Slovenia',
                'country_code' => '386',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => 204,
                'code' => 'SB',
                'name' => 'Solomon Islands',
                'country_code' => '677',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => 205,
                'code' => 'SO',
                'name' => 'Somalia',
                'country_code' => '252',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => 206,
                'code' => 'ZA',
                'name' => 'South Africa',
                'country_code' => '27',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => 207,
                'code' => 'GS',
                'name' => 'South Georgia and the South Sandwich Islands',
                'country_code' => '500',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => 208,
                'code' => 'SS',
                'name' => 'South Sudan',
                'country_code' => '211',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => 209,
                'code' => 'ES',
                'name' => 'Spain',
                'country_code' => '34',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => 210,
                'code' => 'LK',
                'name' => 'Sri Lanka',
                'country_code' => '94',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => 211,
                'code' => 'SD',
                'name' => 'Sudan',
                'country_code' => '249',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => 212,
                'code' => 'SR',
                'name' => 'Suriname',
                'country_code' => '597',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => 213,
                'code' => 'SJ',
                'name' => 'Svalbard and Jan Mayen',
                'country_code' => '47',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => 214,
                'code' => 'SZ',
                'name' => 'Swaziland',
                'country_code' => '268',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => 215,
                'code' => 'SE',
                'name' => 'Sweden',
                'country_code' => '46',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => 216,
                'code' => 'CH',
                'name' => 'Switzerland',
                'country_code' => '41',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => 217,
                'code' => 'SY',
                'name' => 'Syrian Arab Republic',
                'country_code' => '963',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => 218,
                'code' => 'TW',
                'name' => 'Taiwan',
                'country_code' => '886',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => 219,
                'code' => 'TJ',
                'name' => 'Tajikistan',
                'country_code' => '992',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => 220,
                'code' => 'TZ',
                'name' => 'Tanzania, United Republic of',
                'country_code' => '255',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => 221,
                'code' => 'TH',
                'name' => 'Thailand',
                'country_code' => '66',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => 222,
                'code' => 'TL',
                'name' => 'Timor-Leste',
                'country_code' => '670',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => 223,
                'code' => 'TG',
                'name' => 'Togo',
                'country_code' => '228',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => 224,
                'code' => 'TK',
                'name' => 'Tokelau',
                'country_code' => '690',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => 225,
                'code' => 'TO',
                'name' => 'Tonga',
                'country_code' => '676',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => 226,
                'code' => 'TT',
                'name' => 'Trinidad and Tobago',
                'country_code' => '1-868',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => 227,
                'code' => 'TN',
                'name' => 'Tunisia',
                'country_code' => '216',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => 228,
                'code' => 'TR',
                'name' => 'Turkey',
                'country_code' => '90',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => 229,
                'code' => 'TM',
                'name' => 'Turkmenistan',
                'country_code' => '993',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => 230,
                'code' => 'TC',
                'name' => 'Turks and Caicos Islands',
                'country_code' => '1-649',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => 231,
                'code' => 'TV',
                'name' => 'Tuvalu',
                'country_code' => '688',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => 232,
                'code' => 'UG',
                'name' => 'Uganda',
                'country_code' => '256',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => 233,
                'code' => 'UA',
                'name' => 'Ukraine',
                'country_code' => '380',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => 234,
                'code' => 'AE',
                'name' => 'United Arab Emirates',
                'country_code' => '971',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => 235,
                'code' => 'GB',
                'name' => 'United Kingdom',
                'country_code' => '44',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => 236,
                'code' => 'US',
                'name' => 'United States',
                'country_code' => '1',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => 237,
                'code' => 'UM',
                'name' => 'United States Minor Outlying Islands',
                'country_code' => '',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => 238,
                'code' => 'UY',
                'name' => 'Uruguay',
                'country_code' => '598',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => 239,
                'code' => 'UZ',
                'name' => 'Uzbekistan',
                'country_code' => '998',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => 240,
                'code' => 'VU',
                'name' => 'Vanuatu',
                'country_code' => '678',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => 241,
                'code' => 'VE',
                'name' => 'Venezuela, Bolivarian Republic of',
                'country_code' => '58',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => 242,
                'code' => 'VN',
                'name' => 'Viet Nam',
                'country_code' => '84',
                'default_country' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            242 => 
            array (
                'id' => 243,
                'code' => 'VG',
                'name' => 'Virgin Islands, British',
                'country_code' => '',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            243 => 
            array (
                'id' => 244,
                'code' => 'VI',
                'name' => 'Virgin Islands, U.S.',
                'country_code' => '',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => 245,
                'code' => 'WF',
                'name' => 'Wallis and Futuna',
                'country_code' => '681',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => 246,
                'code' => 'EH',
                'name' => 'Western Sahara',
                'country_code' => '212',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => 247,
                'code' => 'YE',
                'name' => 'Yemen',
                'country_code' => '967',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => 248,
                'code' => 'ZM',
                'name' => 'Zambia',
                'country_code' => '260',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => 249,
                'code' => 'ZW',
                'name' => 'Zimbabwe',
                'country_code' => '263',
                'default_country' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            )
        ));
    }
}
