<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('users')->insert([
        [ 
          'id' => '1',
          'name' => 'SMS Admin',
          'email' => 'admin@sms.com',
          'user_type' => 1,
          'country_code' => '+91',
          'contact' => '8888888888',
          'remember_token' => 'sQO42naS1qyE1xhfyLOAiYtcuqHM2ikk1unHIUpJgI2vKRYBaXQ6aXXrpTnI',
          'password' => Hash::make('secret'),
          'status' => 1,
        ],
        [ 
          'id' => '2',
          'name' => 'SMS TL',
          'email' => 'tl@sms.com',
          'user_type' => 2,
          'country_code' => '+91',
          'contact' => '7777777777',
          'remember_token' => 'sQO42naS1qyE1xhfyLOAiYtcuqHM2ikk1unHIUpJgI2vKRYBaXQ6aXXrpTnI',
          'password' => Hash::make('secret'),
          'status' => 1,
        ],
        [ 
          'id' => '3',
          'name' => 'SMS LL',
          'email' => 'll@sms.com',
          'user_type' => 3,
          'country_code' => '+91',
          'contact' => '888888889',
          'remember_token' => 'sQO42naS1qyE1xhfyLOAiYtcuqHM2ikk1unHIUpJgI2vKRYBaXQ6aXXrpTnI',
          'password' => Hash::make('secret'),
          'status' => 1,
        ],
        [ 
          'id' => '4',
          'name' => 'SMS SCHOOL',
          'email' => 'school@sms.com',
          'user_type' => 4,
          'country_code' => '+91',
          'contact' => '778888889',
          'remember_token' => 'sQO42naS1qyE1xhfyLOAiYtcuqHM2ikk1unHIUpJgI2vKRYBaXQ6aXXrpTnI',
          'password' => Hash::make('secret'),
          'status' => 1,
        ],
        [ 
          'id' => '5',
          'name' => 'SMS TEACHER',
          'email' => 'teacher@sms.com',
          'user_type' => 5,
          'country_code' => '+91',
          'contact' => '778988889',
          'remember_token' => 'sQO42naS1qyE1xhfyLOAiYtcuqHM2ikk1unHIUpJgI2vKRYBaXQ6aXXrpTnI',
          'password' => Hash::make('secret'),
          'status' => 1,
        ],
        [ 
          'id' => '6',
          'name' => 'SMS PARENT',
          'email' => 'parent@sms.com',
          'user_type' => 6,
          'country_code' => '+91',
          'contact' => '778987889',
          'remember_token' => 'sQO42naS1qyE1xhfyLOAiYtcuqHM2ikk1unHIUpJgI2vKRYBaXQ6aXXrpTnI',
          'password' => Hash::make('secret'),
          'status' => 1,
        ],
        [ 
          'id' => '7',
          'name' => 'SMS STUDENT',
          'email' => 'student@sms.com',
          'user_type' => 7,
          'country_code' => '+91',
          'contact' => '779987889',
          'remember_token' => 'sQO42naS1qyE1xhfyLOAiYtcuqHM2ikk1unHIUpJgI2vKRYBaXQ6aXXrpTnI',
          'password' => Hash::make('secret'),
          'status' => 1,
        ]
      ]);
    }
  }
