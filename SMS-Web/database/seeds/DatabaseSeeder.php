<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TLTableSeeder::class);
        $this->call(LLTableSeeder::class);
        $this->call(SchoolTableSeeder::class);
        $this->call(TeacherTableSeeder::class);
        $this->call(ParentTableSeeder::class);
        $this->call(StudentTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(ModuleTableSeeder::class);
        $this->call(RouteTableSeeder::class);
    }

}
