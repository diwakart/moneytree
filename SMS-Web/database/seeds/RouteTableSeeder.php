<?php

use Illuminate\Database\Seeder;

class RouteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('routes')->insert(array (
            0 => 
            array (
                'route_path' => '/countries',
                'route_name' => 'viewCountry',
                'module_id' => '1',
            ),
            1 => 
            array (
                'route_path' => 'countries/store',
                'route_name' => 'saveCountry',
                'module_id' => '1',
            ),
            2 => 
            array (
                'route_path' => 'countries/edit',
                'route_name' => 'EditCountry',
                'module_id' => '1',
            ),
            3 => 
            array (
                'route_path' => 'countries/destroy',
                'route_name' => 'DeleteCountry',
                'module_id' => '1',
            ),
            4 => 
            array (
                'route_path' => 'countries/getAllCountry',
                'route_name' => 'getAllCountry',
                'module_id' => '1',
            ),
            5 => 
            array (
                'route_path' => 'countries/activate',
                'route_name' => 'activateCountry',
                'module_id' => '1',
            ),
            6 => 
            array (
                'route_path' => 'countries/deactivate',
                'route_name' => 'deactivateCountry',
                'module_id' => '1',
            ),
            7 =>
            array (
                'route_path' => 'countries/getallTLs',
                'route_name' => 'getAllCountryTls',
                'module_id' => '1',
            ),
            8 =>
            array (
                'route_path' => 'territory-licensee',
                'route_name' => 'viewTL',
                'module_id' => '2',
            ),
            9 =>
            array (
                'route_path' => 'getCountryCode',
                'route_name' => 'getCountryCode',
                'module_id' => '2',
            ),
            10 =>
            array (
                'route_path' => 'territory-licensee/store',
                'route_name' => 'saveTL',
                'module_id' => '2',
            ),
            11 =>
            array (
                'route_path' => 'territory-licensee/show',
                'route_name' => 'getAllTLs',
                'module_id' => '2',
            ),
            12 =>
            array (
                'route_path' => 'territory-licensee/destroy',
                'route_name' => 'deleteTL',
                'module_id' => '2',
            ),
            13 =>
            array (
                'route_path' => 'territory-licensee/changeTLStatus',
                'route_name' => 'changeTLStatus',
                'module_id' => '2',
            ),
            14 =>
            array (
                'route_path' => 'territory-licensee/edit',
                'route_name' => 'editTL',
                'module_id' => '2',
            ),
            15 =>
            array (
                'route_path' => 'territory-licensee/getallLLs',
                'route_name' => 'getAllLLOfTL',
                'module_id' => '2',
            ),
            16 =>
            array (
                'route_path' => 'location-licensee',
                'route_name' => 'viewLL',
                'module_id' => '2',
            ),
            17 =>
            array (
                'route_path' => 'location-licensee/store',
                'route_name' => 'saveLL',
                'module_id' => '2',
            ),
            18 =>
            array (
                'route_path' => 'location-licensee/show',
                'route_name' => 'getAllLLs',
                'module_id' => '2',
            ),
            19 =>
            array (
                'route_path' => 'location-licensee/destroy',
                'route_name' => 'deleteLL',
                'module_id' => '2',
            ),
            20 =>
            array (
                'route_path' => 'location-licensee/changeLLStatus',
                'route_name' => 'changeLLStatus',
                'module_id' => '2',
            ),
            21 =>
            array (
                'route_path' => 'location-licensee/edit',
                'route_name' => 'editLL',
                'module_id' => '2',
            ),
            22 =>
            array (
                'route_path' => 'school',
                'route_name' => 'viewSchool',
                'module_id' => '2',
            ),
            23 =>
            array (
                'route_path' => 'school/show',
                'route_name' => 'getAllSchools',
                'module_id' => '2',
            ),
            24 =>
            array (
                'route_path' => 'school/store',
                'route_name' => 'saveSchool',
                'module_id' => '2',
            ),
            25 =>
            array (
                'route_path' => 'school/edit',
                'route_name' => 'editSchool',
                'module_id' => '2',
            ),
            26 =>
            array (
                'route_path' => 'school/destroy',
                'route_name' => 'deleteSchool',
                'module_id' => '2',
            ),
            27 =>
            array (
                'route_path' => 'school/destroy',
                'route_name' => 'deleteSchool',
                'module_id' => '2',
            ),
            28 =>
            array (
                'route_path' => 'school/getallschools',
                'route_name' => 'getAllSchools',
                'module_id' => '2',
            ),
            29 =>
            array (
                'route_path' => 'school/changeSchoolStatus',
                'route_name' => 'changeSchoolStatus',
                'module_id' => '2',
            ),
            30 =>
            array (
                'route_path' => 'teacher',
                'route_name' => 'viewTeacher',
                'module_id' => '2',
            ),
            31 =>
            array (
                'route_path' => 'teacher/show',
                'route_name' => 'getAllTeachers',
                'module_id' => '2',
            ),
            32 =>
            array (
                'route_path' => 'teacher/edit',
                'route_name' => 'editTeacher',
                'module_id' => '2',
            ),
            33 =>
            array (
                'route_path' => 'teacher/destroy',
                'route_name' => 'deleteTeacher',
                'module_id' => '2',
            ),
            34 =>
            array (
                'route_path' => 'teacher/getallteachers',
                'route_name' => 'getAllTeachers',
                'module_id' => '2',
            ),
            35 =>
            array (
                'route_path' => 'teacher/changeTeacherStatus',
                'route_name' => 'changeTeacherStatus',
                'module_id' => '2',
            ),
            36 =>
            array (
                'route_path' => 'parent',
                'route_name' => 'viewParent',
                'module_id' => '2',
            ),
            37 =>
            array (
                'route_path' => 'parent/show',
                'route_name' => 'getAllParents',
                'module_id' => '2',
            ),
            38 =>
            array (
                'route_path' => 'parent/store',
                'route_name' => 'saveParent',
                'module_id' => '2',
            ),
            39 =>
            array (
                'route_path' => 'parent/edit',
                'route_name' => 'editParent',
                'module_id' => '2',
            ),
            40 =>
            array (
                'route_path' => 'parent/destroy',
                'route_name' => 'deleteParent',
                'module_id' => '2',
            ),
            41 =>
            array (
                'route_path' => 'parent/changeParentStatus',
                'route_name' => 'changeParentStatus',
                'module_id' => '2',
            ),
            42 =>
            array (
                'route_path' => 'student',
                'route_name' => 'viewStudent',
                'module_id' => '2',
            ),
            43 =>
            array (
                'route_path' => 'student/show',
                'route_name' => 'getAllStudents',
                'module_id' => '2',
            ),
            44 =>
            array (
                'route_path' => 'student/store',
                'route_name' => 'saveStudent',
                'module_id' => '2',
            ),
            45 =>
            array (
                'route_path' => 'student/edit',
                'route_name' => 'editStudent',
                'module_id' => '2',
            ),
            46 =>
            array (
                'route_path' => 'student/destroy',
                'route_name' => 'deleteStudent',
                'module_id' => '2',
            ),
            47 =>
            array (
                'route_path' => 'student/changeStudentStatus',
                'route_name' => 'changeStudentStatus',
                'module_id' => '2',
            ),
            48 =>
            array (
                'route_path' => 'class',
                'route_name' => 'viewClass',
                'module_id' => '2',
            ),
            49 =>
            array (
                'route_path' => 'class/show',
                'route_name' => 'getAllClass',
                'module_id' => '2',
            ),
            50 =>
            array (
                'route_path' => 'class/store',
                'route_name' => 'saveClass',
                'module_id' => '2',
            ),
            51 =>
            array (
                'route_path' => 'class/edit',
                'route_name' => 'editClass',
                'module_id' => '2',
            ),
            52 =>
            array (
                'route_path' => 'class/destroy',
                'route_name' => 'deleteClass',
                'module_id' => '2',
            ),
            53 =>
            array (
                'route_path' => 'class/changeClassStatus',
                'route_name' => 'changeClassStatus',
                'module_id' => '2',
            )


        ));
    }
}
