<?php

use Illuminate\Database\Seeder;

class ParentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parents')->insert([
    		[
    			
    			'user_id' => '6',
                'country_id' => '200',
    		]
    	]);
    }
}
