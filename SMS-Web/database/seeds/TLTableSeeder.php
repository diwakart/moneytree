<?php

use Illuminate\Database\Seeder;

class TLTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('territory_licensees')->insert([
    		[
    			'user_id' => '2',
    			'country_id' => '200',
    		]
    	]);
    }
}
