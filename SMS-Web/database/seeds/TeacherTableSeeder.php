<?php

use Illuminate\Database\Seeder;

class TeacherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teachers')->insert([
    		[
    			'id' => '1',
    			'user_id' => '5',    			
    			'school_id' => '4'    			
    		]
    	]);
    }
}
