<?php

use Illuminate\Database\Seeder;

class LLTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location_licensees')->insert([
    		[
    			'id' => '1',
    			'user_id' => '3',
    			'tl_id' => '2',
    			'location' => 'California',    			
    		]
    	]);
    }
}
