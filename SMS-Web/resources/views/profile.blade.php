@extends('layouts.admin')
@section('page_title') 
Dashboard
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title ">
                Profile
            </h3>
        </div>
        @if(Session::has('errormessage'))
           <div class="alert alert-danger alert-autocloseable-info">
               {{ Session::get('errormessage') }}
          </div>
          @endif

          @if(Session::has('successmessage'))
          <div class="alert alert-success alert-autocloseable-info">
                {{ Session::get('successmessage') }}
          </div>
          @endif
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
   
   <div class="row">

    <div class="col-xl-3 col-lg-4">
        <div class="m-portlet m-portlet--full-height  ">
            <div class="m-portlet__body">
                

                <div class="m-card-profile">
                    <div class="m-card-profile__title m--hide">
                        Your Profile
                    </div>
                    <div class="m-card-profile__pic">
                        <div class="m-card-profile__pic-wrapper">
                            @if(Auth::user()->picture!='')
                             @php($profile_img = url('/').Storage::url('app/'.Auth::user()->picture))
                            @else
                             @php($profile_img = url('assets/app/media/img/users/user4.jpg'))
                            @endif
                            <img src="{{$profile_img}}" alt="" class="previewHolder" style="width: 90px;height: 90px;"/>
                        </div>
                    </div>
                    <div class="m-card-profile__details">
                        <span class="m-card-profile__name">
                            {{Auth::user()->name}}
                        </span>
                        <a href="javascript:;" class="m-card-profile__email m-link">
                            {{Auth::user()->email}}
                        </a>
                        <br>
                        <a href="javascript:;" class="btn btn-password" data-toggle="modal" data-target="#ChangePasswordModal"><i class="fa fa-lock"></i>&nbsp;Change Password</a>
                    </div>

                </div>
                
            </div>
        </div>
    </div>
    <div class="col-xl-9 col-lg-8">
        <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                <i class="flaticon-share m--hide"></i>
                                Update Profile
                            </a>
                        </li>
                    </ul>
                </div>
               
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="m_user_profile_tab_1">
                    <form class="m-form m-form--fit m-form--label-align-right" action="{{ route('updateProfile') }}" method="post" enctype = "multipart/form-data" >
                        {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group m--margin-top-10 m--hide">
                                <div class="alert m-alert m-alert--default" role="alert">
                                    The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h3 class="m-form__section">
                                       Personal Details
                                    </h3>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Full Name
                                </label>
                                <div class="col-7">
                                    <input class="form-control m-input" type="text" value="{{Auth::user()->name}}" name="name" id="name">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Email Address
                                </label>
                                <div class="col-7">
                                    <input class="form-control m-input" type="email" value="{{Auth::user()->email}}" name="email" id="email" required="">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Country Code
                                </label>
                                <div class="col-7">
                                    <select name="country_code" id="country_code" class="form-control m-input">
                                          @php($countries = array('+60'=>'Malaysia (+60)','+91'=>'India (+91)','+44'=>'United Kingdom (+44)','+65'=>'Singapore (+65)','+1'=>'United State (+1)'))
                                            @foreach($countries as $key => $value)
                                              <option value="{{$key}}" @if($key== Auth::user()->country_code) {{ 'selected' }}@endif>{{$value}}</option>
                                              @endforeach
                                        </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Contact
                                </label>
                                <div class="col-7">
                                    <input class="form-control m-input" type="number" value="{{Auth::user()->contact}}" name="contact" id="contact">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Profile Image
                                </label>
                                <div class="col-7">
                                    <input class="form-control m-input" type="file" id="profileimg" name="picture" accept="image/x-png,image/jpeg">
                                </div>
                            </div>
                            
                           
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-7">
                                        <button type="submit" id="ValidateButn" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                            Save changes
                                        </button>
                                        &nbsp;&nbsp;
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
   

</div>

<!--begin::Change Password Modal-->
<div class="modal fade" id="ChangePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Change Password
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
             <form action="{{ route('updatePassword') }}" method="post">
            <div class="modal-body">
               
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">
                           New Password:
                        </label>
                        <input type="password" class="form-control" name="new_password" id="new-password">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">
                            Confirm New Password:
                        </label>
                        <input type="password" class="form-control" name="confirm-passoword" id="confirm-passoword">
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" id="ChangePasswordBtn" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                    Change Password
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
<!--end::Change Password Modal-->
  
@endsection
@section('page_script')
<script>
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.previewHolder').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#profileimg").change(function() {
  readURL(this);
});

$("#ValidateButn").click(function(){
      
      if($("#name").val()=='' || $("#email").val()==''){
        swal("Error","Name and Email must be filled","error");
        return false;
      }
    })

$("#ChangePasswordBtn").on('click',function(){
    var password = $("#new-password").val();
    var confirm_password = $("#confirm-passoword").val();
     if(password=='' || confirm_password==''){
        swal("","All fields are required","warning");
        return false;
      }
      if(password != confirm_password){
        swal("","Password not matched","warning");
        return false;
      }
})

</script>
@endsection

