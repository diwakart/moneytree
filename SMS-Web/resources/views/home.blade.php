@extends('layouts.admin')
@section('page_title') 
Dashboard
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Dashboard
            </h3>
          </div> 
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-4">
          <div class="m-portlet m-portlet--border-bottom-brand active background_gradient">
            <div class="m-portlet__body text-right">
              <div class="m-widget26 display_flex">
                <div class="m-widget26__icon">
                  <i class="flaticon-interface-9"></i>
                </div>
                <div class="m-widget26__number">
                  {{$class}}          
                </div>          
              </div>
              <small class="count_name">Classes Tagged</small>
            </div>
          </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-4">
          <div class="m-portlet m-portlet--border-bottom-brand ">
            <div class="m-portlet__body text-right">
              <div class="m-widget26 display_flex">
                <div class="m-widget26__icon">
                  <i class="flaticon-location"></i>
                </div>
                <div class="m-widget26__number">
                  {{$student}} 
                </div>          
              </div>
              <small class="count_name">Students From All There Classes</small>
            </div>
          </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-4">
          <div class="m-portlet m-portlet--border-bottom-brand ">
            <div class="m-portlet__body text-right">
              <div class="m-widget26 display_flex">
                <div class="m-widget26__icon">
                  <i class="flaticon-coins"></i>
                </div>

                <div class="m-widget26__number">
                  {{$parent}}            
                </div>  

              </div>
              <small class="count_name">Parents From All There Classes</small>
            </div>
          </div>
        </div>
      </div>
      <div class="space50"></div>
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Activity 
            </h3>
          </div> 
          <div class="mr-auto" style="margin-right: 0 !important;"> 
            <a  class="activity_more" href="javascript:(0);">See All > </a>
          </div> 
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="owl-carousel owl-theme col-md-12 activity_slider">
          <div class="item" style="background: url(assets/app/media/img//products/product11.jpg);">
            <div class="activity_content">
              <h4>Activity</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry scrambled</p>
            </div>
          </div>  
          <div class="item" style="background: url(assets/app/media/img//products/product11.jpg);">
            <div class="activity_content">
              <h4>Activity</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry scrambled</p>
            </div>
          </div>  

          <div class="item" style="background: url(assets/app/media/img//products/product11.jpg);">
            <div class="activity_content">
              <h4>Activity</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry scrambled</p>
            </div>
          </div>  
          <div class="item" style="background: url(assets/app/media/img//products/product11.jpg);">
            <div class="activity_content">
              <h4>Activity</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry scrambled</p>
            </div>
          </div>  
          <div class="item" style="background: url(assets/app/media/img//products/product11.jpg);">
            <div class="activity_content">
              <h4>Activity</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry scrambled</p>
            </div>
          </div>  
        </div>
      </div>
    </div>
  
@endsection
