<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
     @yield('page_title')
 </title>
 <meta name="description" content="Latest updates and statistic charts">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 <meta name="csrf-token" content="{{ csrf_token() }}">
 <!--begin::Web font -->
 <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
 <script>
  WebFont.load({
    google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
    active: function() {
        sessionStorage.fonts = true;
    }
});
</script>
<script>
 var APP_URL = {!! json_encode(url('/')) !!} 
</script>
<!--end::Web font -->
<!--begin::Base Styles -->  
<!--begin::Page Vendors -->
<link href="{{ url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors -->
<link href="{{ url('assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
<!-- <link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" /> -->
<link href="{{ url('assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/demo/default/base/Main.css')}}" rel="stylesheet" type="text/css" />
<!--end::Base Styles -->
<link rel="shortcut icon" href="{{ url('assets/demo/default/media/img/logo/favicon.ico')}}" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<style>

.select2-container {
  width: 100% !important;
}

.pac-container { 
  position: absolute!important;
  z-index: 9999;
  display: block !important;

}
.pac-container:after {
    
    display: none !important;
    height: 0px;
    width: 0px !important;
    content:none !important;
}
.pac-container { 
    border: none !important;
    box-shadow: none !important; 
}

</style>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyC3Ws7EvlZS2PRlNFZfFrqOnlWM_XHYO1o&libraries=places" type="text/javascript"></script>

@yield('page_css')

</head>
  <!-- end::Head -->
    <!-- end::Body -->
  <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default m-scroll-top--shown m-topbar--on"  >
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        @include('header.app_header')   
        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            <!-- BEGIN: Left Aside -->
            @include('header.app_sidebar')
                <div class="m-grid__item m-grid__item--fluid m-wrapper">
                @yield('content')
                </div>
        </div>
    <footer class="m-grid__item     m-footer ">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--last">
                    <span class="m-footer__copyright">
                        2018 &copy; Student Managment System by
                        <a href="#" class="m-link">
                            Moneytree
                        </a>
                    </span>
                </div> 
            </div>
        </div>
    </footer> 
    </div>     
    <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
      <i class="la la-arrow-up"></i>
    </div>
    <!-- end::Scroll Top -->      

      <!--begin::Base Scripts -->
    <script src="{{url('assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
    <!--end::Base Scripts -->   
        <!--begin::Page Vendors -->
    <script src="{{url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
    <!--end::Page Vendors -->  
        <!--begin::Page Snippets -->
        <!--Owl slider start-->
        <link rel="stylesheet" type="text/css" href="{{url('assets/demo/default/custom/components/owl-slider/owl.carousel.css')}}"> 
     <script src="{{url('assets/demo/default/custom/components/owl-slider/owl.carousel.js')}}" type="text/javascript"></script>
     <script src="{{url('assets/demo/default/custom/components/owl-slider/owl.carousel.js')}}" type="text/javascript"></script>
     <!--Owl slider End-->

    <!--end::Page Snippets -->
    <script src="{{url('assets/demo/default/base/Main.js')}}" type="text/javascript"></script>

    <!--Custom Search Box-->
    <div class="modal-header second_modal" id="searchBox" style="display: none;">
      <div class="form-group">
         @if(isset($Settings) && isset($Settings['country_field']) && $Settings['country_field'])
           <div class="mr-auto" id="country_list_group">
              <label for="recipient-name" class="form-control-label font13">
                Country
              </label>
              <select class="form-control m-input m-input--square no_border_field pl0" id="country_list" name="country">
                <option value="">Please Select Country</option>
                @inject('countries','App\Model\Countries')
                @php($countries = $countries->where(['default_country'=>1])->get())
                @foreach($countries as $name)
                <option value="{{$name->id}}" short_name="{{$name->code}}">{{$name->name}}</option>
                @endforeach
              </select>
           </div>
         @endif
         @if(isset($Settings) && isset($Settings['ll_field']) && $Settings['ll_field'])
            <div class="mr-auto" id="ll_list_group">
              <label for="ll_user" class="form-control-label font13">
                Location Licensee
              </label>
              <select class="form-control m-input m-input--square no_border_field pl0" id="ll_list" name="ll_user">
                <option value="">Select Location Licensee</option>
              </select>
           </div>
         @endif
         
      </div>
      <div class="form-group">
          
        @if(isset($Settings) && isset($Settings['tl_field']) && $Settings['tl_field'])
           <div class="mr-auto" id="tl_list_group">
              <label for="tl_user" class="form-control-label font13">
                Territory Licensee
              </label>
              <select class="form-control m-input m-input--square no_border_field pl0" id="tl_list" name="tl_user">
                <option value="">Select Territory Licensee</option>
              </select>
           </div>
         @endif
         @if(isset($Settings) && isset($Settings['school_field']) && $Settings['school_field'])
           <div class="mr-auto" id="school_list_group">
              <label for="ll_user" class="form-control-label font13">
                School
              </label>
              <select class="form-control m-input m-input--square no_border_field pl0" id="school_list" name="school_user">
                <option value="">Select School</option>
              </select>
           </div>
         @endif
      </div>
    </div>
    <!--End Custom Search Box-->

  </body>
  <!-- end::Body -->
</html>



<!--Here is the common functon of searching-->
<script>
var settings = {};
// Here when user click search button
$('.GlobleSearch').on('click', function(e){
    // Check If Settings is passed or not
    @if(!isset($Settings))
      console.log('Yes Click');
      return true;
    @endif

    console.log('Yes Click');
    $('#searchBox').appendTo('.AppendSearchDiv');
   // $('.modal-dialog').addClass('hide_background');
    $('#searchBox').toggle();
    // Get all the setting for this user
    settings = $.parseJSON('<?= json_encode((isset($Settings) ? $Settings : '')); ?>');
    if(settings.country_field == true){
      $('#country_list_group').show();
    }

    if(settings.tl_field == true){
      if(!settings.country_field){
        getTL();
      }
      $('#tl_list_group').show();
    } else {
      $('#tl_list_group').hide();
    }

    if(settings.ll_field == true){
      if(!settings.tl_field){
        getLL();
      }
      $('#ll_list_group').show();
    } else {
      $('#ll_list_group').hide();
    }

    if(settings.school_field == true){
      if(!settings.ll_field){
        getSchool();
      }
      $('#school_list_group').show();
    } else {
      $('#school_list_group').hide();
    }
});

// On change event of country
$('#country_list').on('change', function(){
  let country_id = $(this).val();

  // Here we Can Check One Step Up with setting value
  if(settings.tl_field != true){
    $('.GlobleSearchResult').val(country_id);
    // Hide the Search Box
    $('.GlobelSearchDisplayName').val($('#country_list option:selected').text());
    $('#searchBox').hide();
    $(".m-form").removeClass("overlay");
    $("body").removeClass("body_fix");
    $('.GlobleSearchResult').click();
  }

  if(country_id){
    getTL(country_id);
  } else {
    swal("Error","Please Browse Country.","error");
    return false;
  }
});

// On change event of TL
$('#tl_list').on('change', function(){
  let tl_id = $(this).val();

  // Here we Can Check One Step Up with setting value
  if(settings.ll_field != true){
    $('.GlobleSearchResult').val(tl_id);
    // Hide the Search Box
    $('.GlobelSearchDisplayName').val($('#tl_list option:selected').text());
    $('#searchBox').hide();
    $(".m-form").removeClass("overlay");
    $("body").removeClass("body_fix");
    $('.GlobleSearchResult').click();
  }

  if(tl_id){
    getLL(tl_id);
  } else {
    swal("Error","Please Browse TL.","error");
    return false;
  }
});


//On chnage event of LL
$('#ll_list').on('change', function(){
  let ll_id = $(this).val();

  // Here we Can Check One Step Up with setting value
  if(settings.school_field != true){
    $('.GlobleSearchResult').val(ll_id);
    // Hide the Search Box
    $('.GlobelSearchDisplayName').val($('#ll_list option:selected').text());
    $('#searchBox').hide();
    $(".m-form").removeClass("overlay");
    $("body").removeClass("body_fix");
    $('.GlobleSearchResult').click();
  }

  if(ll_id){
    getSchool(ll_id);
  } else {
    swal("Error","Please Browse LL.","error");
    return false;
  }
});


// On change event of school
$('#school_list').on('change', function(){
  let school_id = $(this).val();
  $('.GlobleSearchResult').val(school_id);
  // Hide the Search Box
  $('.GlobelSearchDisplayName').val($('#school_list option:selected').text());
  $('#searchBox').hide();
  $(".m-form").removeClass("overlay");
  $("body").removeClass("body_fix");
  $('.GlobleSearchResult').click();
});

function getTL(id="")
{
    $.ajax({
        url: "{{url('countries/getallTLs')}}",
        type: 'POST',              
        data: {id:id},
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
       success: function (data) {
        var res = $.parseJSON(data);
        if(res.length!=0){
            let school_option = $('#tl_list');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select Territory Licensee'));
            $.each(res, function(idx,values){
              var school_name = values.name;
              school_option.append($('<option></option>').val(values.id).html(school_name));
            });
        } else {
            let school_option = $('#tl_list');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select Territory Licensee'));
        }
       },
       cache: false,
    });
}

function getLL(id="")
{
    $.ajax({
        url: "{{url('territory-licensee/getallLLs')}}",
        type: 'POST',              
        data: {id:id},
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
       success: function (data) {
        var res = $.parseJSON(data);
        if(res.length!=0){
            let school_option = $('#ll_list');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select Location Licensee'));
            $.each(res, function(idx,values){
              var school_name = values.name;
              school_option.append($('<option></option>').val(values.id).html(school_name));
            });
        } else {
            let school_option = $('#ll_list');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select Location Licensee'));
        }
       },
       cache: false,
    });
}

function getSchool(id="")
{
    $.ajax({
        url: "{{url('LL/getallschools')}}",
        type: 'POST',              
        data: {id:id},
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
       success: function (data) {
        var res = $.parseJSON(data);
        if(res.length!=0){
            let school_option = $('#school_list');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select School'));
            $.each(res, function(idx,values){
              var school_name = values.name;
              school_option.append($('<option></option>').val(values.id).html(school_name));
            });
        } else {
            let school_option = $('#school_list');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select School'));
        }
       },
       cache: false,
    });
}
</script>


<style type="">
  
  .second_modal {
        position: absolute;
    width: 100%;
    top: 40%;
    background: #ffff;
    box-shadow: 0px 0px 115px 40px #00000021;
    z-index: 9999;
}
.modal-body form, .m-form__section.m-form__section--first{
  position: relative;
}
#StoreClass .second_modal {
            position: absolute; 
    top: 20%;
    box-shadow: 0px 0px 115px 40px #2b2b2b21;
}
/*.overlay{
  position: fixed;
}*/
.body_fix {
    position: fixed;
}
.overlay:before {
       background: #0000004d;
    content: "";
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    z-index: 999;
    position: fixed;
}
</style>
<script>
  $(document).mouseup(function(e) 
{
    var hideDIv = $(".second_modal"); 
    if (!hideDIv.is(e.target) && hideDIv.has(e.target).length === 0) 
    {
        hideDIv.hide();
        $(".m-form").removeClass("overlay");
        $("body").removeClass("body_fix");
    }
});
  
$(document).ready(function(){
    $(".GlobleSearch").click(function(){
        $(".m-form").toggleClass("overlay");
         $("body").toggleClass("body_fix");
    });
});
</script>
@yield('page_script')
