@extends('layouts.admin')
@section('page_title') 
Parent Tagged Student
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Report
            </h3>
          </div> 
          <!--begin: Search Form -->
          <div class="m-form m-form--label-align-right">
            <div class="align-items-center">
              <div class="display_flex">
                <!-- <div class="col-xl-12 col-8">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="">
                      <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search Child Name.." id="generalSearch">
                        
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span>
                            <i class="la la-search"></i>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div> -->
              </div>
            </div>
          </div>
          <!--end: Search Form -->
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="m-portlet--mobile"> 
              <div class="">
                
    <!--begin: Datatable -->
                <table class="table table-bordered table-striped" id="html_table" width="100%">
                  <thead>
                    <tr>
                      <th style="width:10%;">
                        NO.
                      </th>
                      <th>
                        PARENTS INFO
                      </th>
                      <th>
                        EMAIL
                      </th>
                      <th>
                        Contact
                      </th>
                      <th>
                        ACTIVE
                      </th>
                      <th>
                        STATUS
                      </th>
                      <th>
                        ACTION
                      </th> 
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <td>
                        1
                      </td>
                      <td class="parents_info">
                      <div class="parents_td">
                        <div class="m-topbar__userpic">
                          <img src="assets/app/media/img/users/user4.jpg" class="m--img-rounded ">
                        </div>
                        <div class="m-topbar__username">
                          Sachin Sharma
                          <small>Teacher</small>  
                        </div> 
                      </div>
                      </td>
                      <td>
                        Jhonson6137523@gmail.com
                      </td>
                      <td>
                        (916) 6137523
                      </td>
                      <td>
                        <span class="m-switch m-switch--success m-switch--sm">
                                    <label>
                                      <input type="checkbox" checked="checked" name="">
                                      <span></span>
                                    </label>
                                  </span>
                      </td>
                      <td>
                        <button type="button" class="btn btn-success background_gradient btn-view">
                        View
                      </button>
                      </td>
                      <td>
                        <span style="overflow: visible; width: 110px;">   
                      <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btn_hover_gradient" title="Edit ">  
                       <i class="la la-edit"></i>  
                                             </a> 
                                             <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btn_hover_gradient" title="Delete ">  
                       <i class="la la-trash text-danger"></i>  
                                             </a  
                                                  </span>
                                                </td>

                    </tr>

                    <tr>
                      <td>
                        1
                      </td>
                      <td class="parents_info">
                      <div class="parents_td">
                        <div class="m-topbar__userpic">
                          <img src="assets/app/media/img/users/user4.jpg" class="m--img-rounded ">
                        </div>
                        <div class="m-topbar__username">
                          Alina Jhonson
                          <small>Teacher</small>  
                        </div> 
                      </div>
                      </td>
                      <td>
                        Jhonson6137523@gmail.com
                      </td>
                      <td>
                        (916) 6137523
                      </td>
                      <td>
                        <span class="m-switch m-switch--success m-switch--sm">
                                    <label>
                                      <input type="checkbox" checked="checked" name="">
                                      <span></span>
                                    </label>
                                  </span>
                      </td>
                      <td>
                        <button type="button" class="btn btn-success background_gradient btn-view">
                        View
                      </button>
                      </td>
                      <td>
                        <span style="overflow: visible; width: 110px;">   
                      <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btn_hover_gradient" title="Edit ">  
                       <i class="la la-edit"></i>  
                                             </a> 
                                             <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btn_hover_gradient" title="Delete ">  
                       <i class="la la-trash text-danger"></i>  
                                             </a  
                                                  </span>
                                                </td>

                    </tr>
                    
                  </tbody>
                </table>
                <!--end: Datatable -->
              </div>
            </div>
        </div>
      </div>
      <div class="space50"></div>
    </div>
  <!-- Modal for response -->

  
@endsection
@section('page_script')
<style type="text/css">
  .table tr{
        background-color: #fff;
  }
</style>
@endsection

