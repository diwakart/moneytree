@extends('layouts.admin')
@section('page_title') 
Student Report
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
#searchBox1{
  display: block !important;
}
.flex_div{
  display: flex;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title ">
                Report
              </h3>
            </div> 
          </div>
        </div>
      <!-- END: Subheader -->

        <div class="row m-row--full-height">
          <div class="col-md-5 col-sm-6 margin_auto" >
            <div class="m-portlet report_block space50">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title"> 
                    <h3 class="m-portlet__head-text">
                      Attendance
                      <small>Select Wise Report</small>
                    </h3>

                  </div>
                </div>
              </div>
            <div class="m-portlet__body">
              <div class="m-radio-list">
                <div class="form-group m-form__group">  
                  <label class="m-radio m-radio--state-success font13">
                  <input type="radio" name="example_2" value="1" checked="" id="school_check">
                    School
                  <span></span>
                  </label>
                </div>
                <div class="form-group m-form__group">  
                  <label class="m-radio m-radio--state-success font13">
                  <input type="radio" name="example_2" value="2" id="class_check">
                    Class
                  <span></span>
                  </label>
                </div>
                <div class="form-group m-form__group">
                  <label class="m-radio m-radio--state-success font13">
                  <input type="radio" name="example_2" value="3" id="lesson_check">
                  Lesson
                  <span></span>
                  </label>
                </div>
                <div class="form-group m-form__group">
                  <label class="m-radio m-radio--state-success font13">
                  <input type="radio" name="example_2" value="4" id="teacher_check">
                  Teacher
                  <span></span>
                  </label>
                </div>
                <!-- <div class="form-group text-center m-form__group">
                  <button type="reset" class="btn btn-accent background_gradient btn-view theme_shadow no_border_field">
                    View Report
                  </button>
                </div> -->
              </div> 
            </div>
            </div>
          </div>
        </div>
    </div>
  <!--end::Modal-->
    
  <!-- Modal for response -->
  <div class="modal fade" id="SearchModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <h5 class="modal-title" id="exampleModalLabel">
              Add Location Licensee
            </h5>
        <form name="fm-student">
          <div class="modal-body">
            <h5 id="ResponseHeading"></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" data-dismiss="modal" id="LoadReportDatatable">
              OK
            </button>
          </div>
        </form>

      </div>
    </div>
  </div>


   <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <h5 class="modal-title" id="exampleModalLabel">
              Search
            </h5>
            <form id="StoreSchool" action="{{route('saveSchool')}}">
              <!--Custom Search Box-->
                <div class="modal-header second_modal1" id="searchBox1">
                  <div class="form-group flex_div">
                       <div class="mr-auto" id="country_list_group">
                          <label for="recipient-name" class="form-control-label font13">
                            Country
                          </label>
                          <select class="form-control m-input m-input--square no_border_field pl0" id="country_list_report" name="country_list_report">
                            <option value="">Please Select Country</option>
                            @inject('countries','App\Model\Countries')
                            @php($countries = $countries->where(['default_country'=>1])->get())
                            @foreach($countries as $name)
                            <option value="{{$name->id}}" short_name="{{$name->code}}">{{$name->name}}</option>
                            @endforeach
                          </select>
                       </div>

                       <div class="mr-auto" id="tl_list_group">
                          <label for="tl_user" class="form-control-label font13">
                            Territory Licensee
                          </label>
                          <select class="form-control m-input m-input--square no_border_field pl0" id="tl_list_report" name="tl_list_report">
                            <option value="">Select Territory Licensee</option>
                          </select>
                       </div>
                  </div>

                  <div class="form-group">
                       <div class="mr-auto" id="ll_list_group">
                          <label for="ll_user" class="form-control-label font13">
                            Location Licensee
                          </label>
                          <select class="form-control m-input m-input--square no_border_field pl0" id="ll_list_report" name="ll_list_report">
                            <option value="">Select Location Licensee</option>
                          </select>
                       </div> 
                  </div>


                  <div class="form-group">
                   <div class="mr-auto" id="school_list_group">
                      <label for="ll_user" class="form-control-label font13">
                        School
                      </label>
                      <select class="form-control m-bootstrap-select m_selectpicker" multiple data-max-options="10" id="school_list_report" name="school_list_report[]" >
                            <option value="">Select School</option>
                      </select>
                   </div>
                  </div>

                  <div class="form-group">
                     <div class="mr-auto" id="class_list_group">
                        <label for="ll_user" class="form-control-label font13">
                          Class
                        </label>
                        <select class="form-control m-bootstrap-select m_selectpicker" multiple data-max-options="10" id="class_list_report" name="school_list_report[]" >
                              <option value="">Select Class</option>
                        </select>
                     </div>
                  </div>

                  <div class="form-group">
                     <div class="mr-auto" id="lesson_list_group">
                        <label for="ll_user" class="form-control-label font13">
                          Lesson
                        </label>
                        <select class="form-control m-bootstrap-select m_selectpicker" multiple data-max-options="10" id="lesson_list_report" name="school_list_report[]" >
                              <option value="">Select Lesson</option>
                        </select>
                     </div>
                  </div>

                  <div class="form-group">
                     <div class="mr-auto" id="teacher_list_group">
                        <label for="ll_user" class="form-control-label font13">
                          Teacher
                        </label>
                        <select class="form-control m-bootstrap-select m_selectpicker" multiple data-max-options="10" id="teacher_list_report" name="teacher_list_report[]" >
                              <option value="">Select teacher</option>
                        </select>
                     </div>
                  </div>
                </div>
                <!--End Custom Search Box-->
            </form>
          </div>
          <div class="text-center moda_footer">
            <button type="button" class="btn btn-danger btn-view no_border_field" data-dismiss="modal">
              Cancel
            </button>
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="ViewReportBtn">
              View Report
            </button>
          </div>
        </div>
      </div>
    </div>
  
@endsection
@section('page_script')
<script>
  $('.m_selectpicker').selectpicker();
  var CurrentCheckingReport = '';
  // Here When User Click on School Wise Report
  $('#school_check').click(function(){
      ClearEveryThing();
      CurrentCheckingReport = 'school';
      ////////////////////////////////
      $('#class_list_group').hide();
      $('#lesson_list_group').hide();
      $('#teacher_list_group').hide();
      ///////////////////////////////
      $('#country_list_group').show();
      $('#tl_list_group').show();
      $('#ll_list_group').show();
      $('#school_list_group').show();

      if("{{$roleId}}" == 2){ // TL
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        getLLs("{{$userId}}");
      }

      if("{{$roleId}}" == 3){ // LL
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
        getSchools("{{$userId}}");
      }

      if("{{$roleId}}" == 4){ // School
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();

        // Its School So, We don't need to get filter durectly display the report
        window.location.href = "{{url('/viewreport')}}"+"?Schoolid={{$userId}}";
      }

      if("{{$roleId}}" == 5){ // Teacher
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();

        // Its School So, We don't need to get filter durectly display the report
        window.location.href = "{{url('/viewreport')}}"+"?Schoolid={{$school_id}}";
      }

      if("{{$roleId}}" == 6){ // Parent
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
      }
      $("#ResponseSuccessModal").modal('show');
  });


  // Here When User Click on Class Wise Report
  $('#class_check').click(function(){
    ClearEveryThing();
    CurrentCheckingReport = 'class';
    ////////////////////////////////
      $('#class_list_group').show();
      $('#lesson_list_group').hide();
      $('#teacher_list_group').hide();
      ///////////////////////////////
      $('#country_list_group').show();
      $('#tl_list_group').show();
      $('#ll_list_group').show();
      $('#school_list_group').show();

      if("{{$roleId}}" == 2){ // TL
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        getLLs("{{$userId}}");
      }

      if("{{$roleId}}" == 3){ // LL
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
        getSchools("{{$userId}}");
      }

      if("{{$roleId}}" == 4){ // School
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
        $('#school_list_group').hide();
        getClassS("{{$userId}}");
      }

      if("{{$roleId}}" == 5){ // Teacher
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
        $('#school_list_group').hide();
        getClassS("{{$userId}}");
      }

      if("{{$roleId}}" == 6){ // Parent
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
      }

      // Reset All DropDown
      ClearEveryThing();
      $("#ResponseSuccessModal").modal('show');
  });

  // Here When User Click on Lesson Wise Report
  $('#lesson_check').click(function(){
      ClearEveryThing();
      CurrentCheckingReport = 'lesson'; 
      ////////////////////////////////
      $('#class_list_group').show();
      $('#lesson_list_group').show();
      $('#teacher_list_group').hide();
      ///////////////////////////////
      $('#country_list_group').show();
      $('#tl_list_group').show();
      $('#ll_list_group').show();
      $('#school_list_group').show();

      if("{{$roleId}}" == 2){ // TL
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        getLLs("{{$userId}}");
      }

      if("{{$roleId}}" == 3){ // LL
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
        getSchools("{{$userId}}");
      }

      if("{{$roleId}}" == 4){ // School
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
        $('#school_list_group').hide();
        getClassS("{{$userId}}");
      }

      if("{{$roleId}}" == 5){ // Teacher
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
        $('#school_list_group').hide();
        getClassS("{{$userId}}");
      }

      if("{{$roleId}}" == 6){ // Parent
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
      }
      $("#ResponseSuccessModal").modal('show');
  });


  $('#teacher_check').click(function(){
      ClearEveryThing();
      CurrentCheckingReport = 'teacher';
      ////////////////////////////////
      $('#class_list_group').show();
      $('#lesson_list_group').hide();
      $('#teacher_list_group').show();
      ///////////////////////////////
      $('#country_list_group').show();
      $('#tl_list_group').show();
      $('#ll_list_group').show();
      $('#school_list_group').show();

      if("{{$roleId}}" == 2){ // TL
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        getLLs("{{$userId}}");
      }

      if("{{$roleId}}" == 3){ // LL
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
        getSchools("{{$userId}}");
      }

      if("{{$roleId}}" == 4){ // School
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
        $('#school_list_group').hide();
        getClassS("{{$school_id}}");
      }

      if("{{$roleId}}" == 5){ // Teacher
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
        $('#teacher_list_group').hide();

        // Its School So, We don't need to get filter durectly display the report
        window.location.href = "{{url('/viewreport')}}"+"?Teacherlid={{$userId}}";
      }

      if("{{$roleId}}" == 6){ // Parent
        $('#country_list_group').hide();
        $('#tl_list_group').hide();
        $('#ll_list_group').hide();
      }
      $("#ResponseSuccessModal").modal('show');
  });

  $('#ViewReportBtn').click(function(){
      if(CurrentCheckingReport == 'school'){
        let schoolIds = $('#school_list_report').val().join();
        window.location.href = "{{url('/viewreport')}}"+"?Schoolid="+schoolIds;
      }
      if(CurrentCheckingReport == 'class'){
        let classIds = $('#class_list_report').val().join();
        window.location.href = "{{url('/viewreport')}}"+"?Classid="+classIds;
      }
      if(CurrentCheckingReport == 'lesson'){
        let lessonIds = $('#lesson_list_report').val().join();
        window.location.href = "{{url('/viewreport')}}"+"?Lessonid="+lessonIds;
      }
      if(CurrentCheckingReport == 'teacher'){
        let teacherIds = $('#teacher_list_report').val().join();
        window.location.href = "{{url('/viewreport')}}"+"?Teacherid="+teacherIds;
      }
  });

  // On change event of country
  $('#country_list_report').on('change', function(){
    let country_id = $(this).val();
    if(country_id){
      getTLs(country_id);
    } else {
      swal("Error","Please Browse Country.","error");
      return false;
    }
  });

// On change event of TL
$('#tl_list_report').on('change', function(){
  let tl_id = $(this).val();
  if(tl_id){
    getLLs(tl_id);
  } else {
    swal("Error","Please Browse TL.","error");
    return false;
  }
});


//On chnage event of LL
$('#ll_list_report').on('change', function(){
  let ll_id = $(this).val();
  if(ll_id){
    getSchools(ll_id);
  } else {
    swal("Error","Please Browse LL.","error");
    return false;
  }
});

//On chnage event of School
$('#school_list_report').on('change', function(){
  let ll_id = $(this).val().join();
  if(ll_id){
    getClassS(ll_id);
  } else {
    swal("Error","Please Browse LL.","error");
    return false;
  }
});

//On chnage event of Class
$('#class_list_report').on('change', function(){
  let ll_id = $(this).val().join();
  if(ll_id){
    if(CurrentCheckingReport == "teacher"){
      getTeacherS(ll_id);
    } else {
      getLessonS(ll_id); 
    }
    
  } else {
    swal("Error","Please Browse LL.","error");
    return false;
  }
});

function getTLs(id="")
{
    $.ajax({
        url: "{{url('countries/getallTLs')}}",
        type: 'POST',              
        data: {id:id},
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
       success: function (data) {
        var res = $.parseJSON(data);
        if(res.length!=0){
            let school_option = $('#tl_list_report');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select Territory Licensee'));
            $.each(res, function(idx,values){
              var school_name = values.name;
              school_option.append($('<option></option>').val(values.id).html(school_name));
            });
        } else {
            let school_option = $('#tl_list_report');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select Territory Licensee'));
        }
       },
       cache: false,
    });
}

function getLLs(id="")
{
    $.ajax({
        url: "{{url('territory-licensee/getallLLs')}}",
        type: 'POST',              
        data: {id:id},
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
       success: function (data) {
        var res = $.parseJSON(data);
        if(res.length!=0){
            let school_option = $('#ll_list_report');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select Location Licensee'));
            $.each(res, function(idx,values){
              var school_name = values.name;
              school_option.append($('<option></option>').val(values.id).html(school_name));
            });
        } else {
            let school_option = $('#ll_list_report');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select Location Licensee'));
        }
       },
       cache: false,
    });
}

function getSchools(id="")
{
    $.ajax({
        url: "{{url('LL/getallschools')}}",
        type: 'POST',              
        data: {id:id},
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
       success: function (data) {
        var res = $.parseJSON(data);
        if(res.length!=0){
            let school_option = $('#school_list_report');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select School'));
            $.each(res, function(idx,values){
              var school_name = values.name;
              school_option.append($('<option></option>').val(values.id).html(school_name));
            });
            $('.m_selectpicker').selectpicker('refresh');
        } else {
            let school_option = $('#school_list_report');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select School'));
            $('.m_selectpicker').selectpicker('refresh');
        }
       },
       cache: false,
    });
}

function getClassS(id="")
{
    $.ajax({
        url: "{{url('School/getallclass')}}",
        type: 'POST',              
        data: {id:id},
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
       success: function (data) {
        var res = data;
        // var res = $.parseJSON(data);
        // console.log(res);
        if(res.length!=0){
            let school_option = $('#class_list_report');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select Class'));
            $.each(res, function(idx,values){
              var school_name = values.title;
              school_option.append($('<option></option>').val(values.id).html(school_name));
            });
            $('.m_selectpicker').selectpicker('refresh');
        } else {
            let school_option = $('#class_list_report');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select Class'));
            $('.m_selectpicker').selectpicker('refresh');
        }
       },
       cache: false,
    });
}

function getLessonS(id="")
{
    $.ajax({
        url: "{{url('Class/getalllessons')}}",
        type: 'POST',              
        data: {id:id},
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
       success: function (data) {
        console.log('--------------');
        console.log(data);
        var res = data.lessonId.split(",");
        console.log(res);
        if(res.length!=0){
            let school_option = $('#lesson_list_report');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select Lesson'));
            res.forEach(function($val){
              var school_name =$val;
              school_option.append($('<option></option>').val($val).html(school_name));
            });
            // $.each(res, function(idx,values){
            //   var school_name = values;
            //   school_option.append($('<option></option>').val(values).html(school_name));
            // });
            $('.m_selectpicker').selectpicker('refresh');
        } else {
            let school_option = $('#lesson_list_report');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select Lesson'));
            $('.m_selectpicker').selectpicker('refresh');
        }
       },
       cache: false,
    });
}

function getTeacherS(id="")
{
    $.ajax({
        url: "{{url('Class/getallteachers')}}",
        type: 'POST',              
        data: {id:id},
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
       success: function (data) {
        var res = data;
        if(res.length!=0){
            let school_option = $('#teacher_list_report');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select Teacher'));
            $.each(res, function(idx,values){
              var school_name = values.name;
              school_option.append($('<option></option>').val(values.id).html(school_name));
            });
            $('.m_selectpicker').selectpicker('refresh');
        } else {
            let school_option = $('#teacher_list_report');
            school_option.html('');
            school_option.append($('<option></option>').val('').html('Select School'));
            $('.m_selectpicker').selectpicker('refresh');
        }
       },
       cache: false,
    });
}

function ClearEveryThing(){
  $('#country_list_report').val('');
  $('#tl_list_report').html('<option>Select Territory Licensee</option>');
  $('#ll_list_report').html('<option>Select Location Licensee</option>');
  $('#school_list_report').html('<option>Select School</option>');
  $('#teacher_list_report').html('<option>Select Teacher</option>');
  $('#lesson_list_report').html('<option>Select Lesson</option>');
  $('#class_list_report').html('<option>Select Class</option>');
  $('.m_selectpicker').selectpicker('refresh');
}

</script>
@endsection

