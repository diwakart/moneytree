@extends('layouts.admin')
@section('page_title') 
Student
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Student
            </h3>
          </div> 
          <!--begin: Search Form -->
          <div class="m-form m-form--label-align-right">
            <div class="align-items-center">
              <div class="display_flex">
                <div class="col-xl-8 col-8">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="">
                      <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search Student Name.." id="generalSearch">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span>
                            <i class="la la-search"></i>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                 <div class="col-xl-4 col-4 m--align-right">
                  <a href="javascript:void(0);" id="addStudent" class="btn background_gradient btn-accent add_btn theme_shadow" data-toggle="modal" data-target="#addStudentModal">
                    <span>
                      <i class="la la-plus"></i> 
                    </span>
                  </a> 
                </div>
              </div>
            </div>
          </div>
          <!--end: Search Form -->
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="Student_datatable"></div>
        </div>
      </div>
      <div class="space50"></div>
    </div>

    <!--begin:: Add Student Modal-->
    <div class="modal fade" id="addStudentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <h5 class="modal-title" id="exampleModalLabel">
              Add Student
            </h5>
            <form id="StoreStudent" action="{{route('saveStudent')}}">
              <div class="form-group display_flex AppendSearchDiv">
              </div>
              <div class="form-group">
                <!-- Here is I have check the role If the same then hide the search and put Id into the field -->
                @if(Auth::user()->user_type != 4 && Auth::user()->user_type != 5)
                  <label for="state" class="form-control-label font13">
                    School
                  </label>
                  <input type="text" class="form-control border_bottom pl0 GlobelSearchDisplayName" id="selected_school" name="selected_school" placeholder="Search School Name" readonly>
                  <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field GlobleSearch" id="school_search1">Search</button>
                  <input type="hidden" name="school_user" value="" id="school_user" class="GlobleSearchResult">
                @else
                  @if(Auth::user()->user_type == 5)
                  <input type="hidden" name="school_user" value="{{$school_data->school_id}}" id="school_user" class="GlobleSearchResult">
                  @else
                  <input type="hidden" name="school_user" value="{{Auth::id()}}" id="school_user" class="GlobleSearchResult">
                  @endif
                @endif
              </div>
              <div class="form-group">
                <label for="name" class="form-control-label font13">
                  Student Name *
                </label>
                <input type="text" class="form-control border_bottom pl0" id="name" name="name" placeholder="Enter name of Student">
              </div>
              <div class="form-group display_flex">
                <div class="mr-auto">
                  <label for="country_code" class="form-control-label font13">
                    Country Code *
                  </label>
                  <input type="text" class="form-control border_bottom pl0" id="country_code" name="country_code" placeholder="Country code for mobile" maxlength="6">
                </div>
                <div class="mr-auto">
                  <label for="contact" class="form-control-label font13">
                  Contact Number *
                  </label>
                  <input type="text" class="form-control border_bottom pl0" id="contact" name="contact" placeholder="Enter contact number">
                </div>
              </div>
              <div class="form-group display_flex">
                <div class="mr-auto">
                  <label for="email" class="form-control-label font13">
                  Email *
                  </label>
                  <input type="email" class="form-control border_bottom pl0" id="email" name="email" placeholder="Enter email of Student">
                </div>
              </div>
              
            </form>
          </div>
          <div class="text-center moda_footer">
            <button type="button" class="btn btn-danger btn-view no_border_field" data-dismiss="modal">
              Cancel
            </button>
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="addStudentbtn">
              Save Changes
            </button>
          </div>
        </div>
      </div>
    </div>
  <!--end::Modal-->
  
  <!-- Modal for response -->

  <!-- Modal for success response -->
  <div class="modal fade" id="tagParentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>

      
      <form name="fm-student" id="StoreParent" action="{{route('tagParent')}}" autocomplete="off">
        <input type="hidden" id="stu_tag_id" name="stu_tag_id">
        <div class="modal-body">
          <div class="form-group" id="schoolDiv">
            <label for="country" class="form-control-label">
              Parent
            </label>
            <select name="tagname" id="tagname" multiple="multiple" class="form-control m-input" required>
                @foreach($parents as $item)
                  <option value="{{$item->id}}">{{$item->name}}</option>
                  @endforeach
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">
            Close
          </button>
          <button type="button" class="btn btn-primary" id="tagformSubmit">
            Save changes
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
  
  <!-- Modal for success response -->
  <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <form name="fm-student">
          <div class="modal-body">
            <h5 id="ResponseHeading"></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" data-dismiss="modal" id="LoadStudentDatatable">
              OK
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
@endsection
@section('page_script')
  <script>
      $('#addStudent').click(function(e){
        e.preventDefault();
        $('#name').val('');
        $('#email').val('');
        $('#contact').val('');
        $('#country_code').val('');
        $('#state').val('');
        $('#country').val('');
        $('#selected_school').val('');
        });
      var datatable;
      (function() {
        $('.loader_msg').css('display','none');
        datatable = $('.Student_datatable').mDatatable({
                // datasource definition
                data: {
                  type: 'remote',
                  source: {
                    read: {
                      url: APP_URL+'/student/show',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                            country_list: '',
                            ll_name: '',
                          }
                        },
                        map: function(raw) {

                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                           dataSet = raw.data;
                         }
                         console.log('Result');
                         console.log(dataSet);
                         return dataSet;
                       },
                     }
                   },
                   pageSize: 10,
                   saveState: {
                    cookie: false,
                    webstorage: false
                  },

                  serverPaging: true,
                  serverFiltering: false,
                  serverSorting: false
                },
          // layout definition
          layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

           /* search: {
              input: $('#generalSearch')
            },*/

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
              field: "S_No",
              title: "S.No",
              width: 40
            },
            {
              field: "name",
              title: "Name",
              textAlign: 'center'
            },
            {
              field: "school",
              title: "School",
              textAlign: 'center'
            },
            {
              field: "email",
              title: "Email",
              textAlign: 'center'
            },
            {
              field: "contact",
              title: "Contact",
              textAlign: 'center'
            },
            {
              field: "status",
              title: "Status",
              textAlign: 'center',
              template: function(row) {
                if(row.status==1){
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Deactivate" onclick=activate_deactivateStudent('+row.id+')>\
                    Deactivate\
                    </a>\
                 ';
                }
                else
                {
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Activate" onclick=activate_deactivateStudent('+row.id+')>\
                   Activate\
                   </a>\
                 ';
                }

              }
            },
           {
              width: 150,
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              field: 'Actions',
              template: function(row) {
                 return '\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                   <i class="la la-edit" onclick=getStudentDetail('+row.id+')></i>\
                   </a>\
                   <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Tag Parents">\
                   <i class="la la-tags" onclick=tagParent('+row.id+')></i>\
                   </a>\
                   <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Assign Class">\
                   <i class="la la-university" onclick=assignClass('+row.id+')></i>\
                   </a>\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteStudent('+row.id+')>\
                   <i class="la la-trash"></i>\
                   </a>\
                   \
                 ';
             },
           }]
         });

$('#addStudentbtn').on('click',function(){
  datatable.reload();
  });

$('#generalSearch').on('change',function(){
  var value = $(this).val();
  var country_id = $('#country_list').val();
  datatable.setDataSourceQuery({ll_name:value,country_id:country_id});
  datatable.reload();
});

$("#LoadStudentDatatable").on('click',function(){
  datatable.reload();
});

})();

function assignClass(id)
{
    alert(id);
}
function tagParent(id)
{
    $('#stu_tag_id').val(id);
    $.ajax
    ({
        url: APP_URL + "/school/getAllTaggedParents",
        type: 'POST',              
        data: {id:id},
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       success: function (data) {
    
        var res = $.parseJSON(data);
        if(res.length==0){
          $('#tl_user').find('option').remove().end();
          var tl_option = $('#tl_user');
          tl_option.append(
          $('<option></option>').val('').html('Select Territory Licensee')
          );
        }else{
         $('#tl_user').find('option').remove().end();
         var tl_option = $('#tl_user');
         tl_option.append(
          $('<option></option>').val('').html('Select Territory Licensee')
          );
         $.each(res, function(idx,values){
          var tl_name = values.name;
            tl_option.append($('<option></option>').val(values.id).html(tl_name));
        });
       } 
     },
     cache: false,
    });
    $('#tagParentModal').find('.modal-title').html('Tag Parent');
    $("#tagParentModal").modal('show');
}
function getStudentDetail(id){
  var path = APP_URL + "/student/edit";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
      console.log("here is the data");
            console.log(result);
            var res = $.parseJSON(result);
            var tl_id = "";
            var ll_id = "";
            var school_id = "";
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              $('#addStudentModal').data('id',data.id);
              $('#addStudentModal').find('.modal-title').html('Update Student ' + data.name);
              $("#PasswordDiv").html('');
              $('#name').val(data.name);
              $('#email').val(data.email);
              $('#contact').val(data.contact);
              $('#country_code').val(data.country_code);
              $('#country').val(data.country);
              $('#state').val(data.location);
              school_id = data.school_id;
              $('#selected_school').val(data.school_name);
              //loadTLData(data.country,data.tl_id);
              $('#addStudentModal').modal('show');
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
}

function deleteStudent(id){
  var path = APP_URL + "/student/destroy";
  var _this = $(this);
  swal({
    title: "Are you sure to delete this Student?",
    text: "You will not be able to recover this.",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-warning",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm) {
    if (isConfirm) {
      var data = id;
      $.ajax({
        method: 'POST',
        url: path,
        data: {
          id: data,
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          var res = $.parseJSON(data);
          if(res.status == 'error'){
            swal('Error',res.message,'error');
          }else{
           $('.sweet-overlay').remove();
           $('.showSweetAlert ').remove();
           $("#ResponseSuccessModal").modal('show');
           $("#ResponseSuccessModal #ResponseHeading").text(res.message);
         } 
       },
       error: function(data) {
        swal('Error',data,'error');
      }
    });
    } else {

    }
  });
}

$('#addStudentbtn').click(function(e){
  e.preventDefault();

  var id = $('#addStudentModal').data('id');
  var country  = $('#country').val();
  var tl_id  = $('#tl_user').val();
  var state  = $('#ll_user').val();
  var school_id  = $('#school_user').val();
  var name  = $('#name').val();
  var email  = $('#email').val();
  var country_code  = $('#country_code').val();
  var contact  = $('#contact').val();
  
  if(country=='' && settings != "undefined" && settings.country_field){
    swal("Error","Country is required","error");
    return false;
  }
  if(tl_id=='' && settings != "undefined" && settings.tl_field){
    swal("Error","Territory Licensee is required","error");
    return false;
  }
  if(state=='' && settings != "undefined" && settings.ll_field){
    swal("Error","State is required","error");
    return false;
  }
  if(school_id==''){
    swal("Error","School is required","error");
    return false;
  }
  if(name==''){
    swal("Error","Name is required","error");
    return false;
  }
  if(email==''){
    swal("Error","Email is required","error");
    return false;
  }
  if(country_code==''){
    swal("Error","Country code for contact number is required","error");
    return false;
  }
  if(contact==''){
    swal("Error","Contact number is required","error");
    return false;
  }
$.ajax({
  method: 'POST',
  url: $("#StoreStudent").attr('action'),
  data: {
    id: id,
    country: country,
    tl_id: tl_id,
    state: state,
    school_id: school_id,
    name:name,
    email:email,
    country_code:country_code,
    contact:contact,
  },
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  success: function(data) {
    var res = $.parseJSON(data);
    if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
      $('#addStudentModal').modal('hide');
      $("#ResponseSuccessModal").modal('show');
      $("#ResponseSuccessModal #ResponseHeading").text(res.message);
    } 
  },
  error: function(data) {
    swal('Error',data,'error');
  }
});
});
function activate_deactivateStudent(id){
  var path = APP_URL + "/student/create";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
     var res = $.parseJSON(result);
     if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
     $('.sweet-overlay').remove();
     $('.showSweetAlert ').remove();
     $("#ResponseSuccessModal").modal('show');
     $("#ResponseSuccessModal #ResponseHeading").text(res.message);
   } 
 },
 error: function(){
  alert("Error");
}
}); 
}

$('#country').on('change',function(){
  var id = $(this).val();
  $.ajax({
    method: 'POST',
    url: APP_URL+'/getCountryCode',
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data) {
      var res = $.parseJSON(data);
      if(res.status == 'error'){
        /*swal('Error',res.message,'error');*/
        $('#country_code').val('');
      }else{
        $('#country_code').val(res.data.country_code);
      } 
    },
    error: function(data) {
      swal('Error',data,'error');
    }
  });
  $.ajax
  ({
    url: APP_URL + "/countries/getallTLs",
    type: 'POST',              
    data: {id:id},
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   success: function (data) {

    var res = $.parseJSON(data);
    if(res.length==0){
      $('#tl_user').find('option').remove().end();
      var tl_option = $('#tl_user');
      tl_option.append(
      $('<option></option>').val('').html('Select Territory Licensee')
      );
    }else{
     $('#tl_user').find('option').remove().end();
     var tl_option = $('#tl_user');
     tl_option.append(
      $('<option></option>').val('').html('Select Territory Licensee')
      );
     $.each(res, function(idx,values){
      var tl_name = values.name;
        tl_option.append($('<option></option>').val(values.id).html(tl_name));
    });
   } 
 },
 cache: false,
});
});

$('#tl_user').on('change',function(){
  var id = $(this).val();
$.ajax
  ({
    url: APP_URL + "/territory-licensee/getallLLs",
    type: 'POST',              
    data: {id:id},
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   success: function (data) {

    var res = $.parseJSON(data);
    if(res.length==0){
      $('#ll_user').find('option').remove().end();
      var ll_option = $('#ll_user');
      ll_option.append(
      $('<option></option>').val('').html('Select Location Licensee')
      );
    }else{
     $('#ll_user').find('option').remove().end();
     var ll_option = $('#ll_user');
     ll_option.append(
      $('<option></option>').val('').html('Select Location Licensee')
      );
     $.each(res, function(idx,values){
      var ll_name = values.name;
        ll_option.append($('<option></option>').val(values.id).html(ll_name));
    });
   } 
 },
 cache: false,
});
});

$('#ll_user').on('change',function(){
  var id = $("#ll_user").val();
$.ajax
  ({
    url: APP_URL + "/LL/getallschools",
    type: 'POST',              
    data: {id:id},
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   success: function (data) {

    var res = $.parseJSON(data);
    if(res.length==0){
      $('#school_user').find('option').remove().end();
      var school_option = $('#school_user');
      school_option.append(
      $('<option></option>').val('').html('Select School')
      );
    }else{
     $('#school_user').find('option').remove().end();
     var school_option = $('#school_user');
     school_option.append(
      $('<option></option>').val('').html('Select School  ')
      );
     $.each(res, function(idx,values){
      var school_name = values.name;
        school_option.append($('<option></option>').val(values.id).html(school_name));
    });
   } 
 },
 cache: false,
});
});


</script>
@endsection

