@extends('layouts.admin')
@section('page_title') 
Student Attendance
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Student Attendance
            </h3>
          </div> 
          <!--begin: Search Form -->
          <div class="m-form m-form--label-align-right">
            <div class="align-items-center">
              <div class="display_flex">
                <div class="col-xl-8 col-8">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="">
                      <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search Name.." id="generalSearch">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span>
                            <i class="la la-search"></i>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-4 m--align-right">
                  <a href="javascript:void(0);" id="addSA" class="btn background_gradient btn-accent add_btn theme_shadow" data-toggle="modal" data-target="#addStudentAttendanceModal">
                    <span>
                      <i class="la la-plus"></i> 
                    </span>
                  </a> 
                </div>
              </div>
            </div>
          </div>
          <!--end: Search Form -->
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="Student_Attendance_datatable"></div>
        </div>
      </div>
      <div class="space50"></div>
    </div>

    <!--begin:: Add Student Attendance Modal-->
    <div class="modal fade" id="addStudentAttendanceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <h5 class="modal-title" id="exampleModalLabel">
              Add Student Attendance
            </h5>
            <form id="StoreStudentAttendance" class="modal_form" action="{{route('saveTeacher')}}" >
              <div class="form-group display_flex AppendSearchDiv">
              </div>
              <div class="form-group">
                <!-- Here is I have check the role If the same then hide the search and put Id into the field -->
                @if(Auth::user()->user_type != 4 && Auth::user()->user_type != 5)
                  <label for="state" class="form-control-label font13">
                    School
                  </label>
                  <input type="text" class="form-control border_bottom pl0 GlobelSearchDisplayName" id="selected_school" name="selected_school" placeholder="Search School Name" readonly>
                  <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field GlobleSearch" id="school_search1">Search</button>
                  <input type="hidden" name="school_user" value="" id="school_user" class="GlobleSearchResult">
                @else
                  @if(Auth::user()->user_type == 5)
                  <input type="hidden" name="school_user" value="{{$school_data->school_id}}" id="school_user" class="GlobleSearchResult">
                  @else
                  <input type="hidden" name="school_user" value="{{Auth::id()}}" id="school_user" class="GlobleSearchResult">
                  @endif
                @endif
              </div>
              <div class="form-group display_flex">
                <div class="mr-auto">
                  <label for="country_code" class="form-control-label font13">
                    Class *
                  </label>
                  <select class="form-control m-input m-input--square no_border_field pl0" id="class" name="class">
                      <option value="">Please Select Class</option>
                  </select>
                </div>
                <div class="mr-auto">
                  <label for="country_code" class="form-control-label font13">
                    Teacher *
                  </label>
                  <select class="form-control m-input m-input--square no_border_field pl0" id="teacher" name="teacher">
                      <option value="">Please Select Teacher</option>
                  </select>
                </div>
                <div class="mr-auto">
                  <label for="contact" class="form-control-label font13">
                  Lesson *
                  </label>
                  <select class="form-control m-input m-input--square no_border_field pl0" id="lesson" name="lesson">
                      <option value="">Please Select Lesson</option>
                  </select>
                </div>
              </div>
              <!-- <div class="form-group display_flex">
                <div class="mr-auto">
                  <label for="recipient-name" class="form-control-label font13">
                    Start Date
                  </label>
                  <div class='input-group date' id='m_datepicker_2'>
                    <input type='text' class="form-control m-input no_border_field pl0" id="start_date" name="start_date" readonly  placeholder="Start date"/>
                    <span class="input-group-addon no_border_field bg_transparent">
                    <i class="la la-calendar-check-o"></i>
                    </span>
                  </div>
                </div>
                <div class="mr-auto">
                  <label for="recipient-name" class="form-control-label font13">
                    End Date
                  </label>
                  <div class='input-group date' id='m_datepicker_2'>
                    <input type='text' class="form-control m-input no_border_field pl0"id="end_date" name="end_dat" readonly  placeholder="End date"/>
                    <span class="input-group-addon no_border_field bg_transparent">
                    <i class="la la-calendar-check-o"></i>
                    </span>
                  </div>
                </div>
              </div> -->
              <div class="form-group">
                <label for="name" class="form-control-label font13">
                  Class Date*
                </label>
                <div class="input-daterange input-group" id="m_datepicker_5">
                  <input type="text" class="form-control m-input" name="start_date" id="start_date" placeholder="Start date of class" readonly>
                  <span class="input-group-addon">
                    <i class="la la-calendar-check-o"></i>
                  </span>
                  <input type="text" class="form-control" name="end_date" id="end_date" placeholder="End date of class" readonly>
                  <span class="input-group-addon">
                    <i class="la la-calendar-check-o"></i>
                  </span>
                </div>
              </div>
            </form>
          </div>
          <div class="text-center moda_footer">
            <button type="button" class="btn btn-danger btn-view no_border_field" data-dismiss="modal">
              Cancel
            </button>
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="submit">
              Start Class
            </button>
          </div>
        </div>
      </div>
    </div>
  <!--end::Modal-->
    
  <!-- Modal for response -->

  <!-- Modal for success response -->
  <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <form name="fm-student">
          <div class="modal-body">
            <h5 id="ResponseHeading"></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" data-dismiss="modal" id="LoadSADatatable">
              OK
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
@endsection
@section('page_script')
  <script>
      var IfStudentExist = 0;
      var CheckAttendenceNotFill = 0;
      $('.GlobleSearchResult').click(function(){
        let school_id = $('#school_user').val();
        $.ajax({
            method: 'GET',
            url: "{{url('school/getclass')}}"+"/"+school_id+"",
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
              var res = $.parseJSON(data);
              if(res.status == 'error'){
                swal('Error',res.message,'error');
              }else{
               let HTML = "<option selected disabled>Select Class</option>";
               $.each(res,function(k,v){
                 HTML += "<option value='"+v.id+"'>"+v.title+"</option>";
               });
               $('#class').html(HTML);
             } 
           },
           error: function(data) {
            swal('Error',data,'error');
          }
        });
      });

      $('#class').on('change',function(){
        let class_id = $(this).val();
        $.ajax({
            method: 'GET',
            url: "{{url('school/getteacher')}}"+"/"+class_id+"",
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
              var res = $.parseJSON(data);
              if(res.status == 'error'){
                swal('Error',res.message,'error');
              }else{
               let teacher = "<option selected disabled>Select Teacher</option>";
               let lesson = "<option selected disabled>Select Lesson</option>";
               IfStudentExist = res.checkifStudent;
               CheckAttendenceNotFill = res.checkifAttendenceNotFill;
               console.log('L')
               $.each(res,function(k,v){ 
                if(v instanceof Array){
                  $.each(v,function(k1,v1){
                    if(k == "allTeacher"){
                      teacher += "<option value='"+v1.id+"'>"+v1.name+"</option>";
                      $('#teacher').html(teacher);
                    }
                    if(k == "lessonAll"){
                      lesson += "<option value='"+v1+"'>A</option>";
                      $('#lesson').html(lesson);
                    }
                    console.log('$$$$');
                    console.log(k1,v1);
                  });
                }
               });

             } 
           },
           error: function(data) {
            swal('Error',data,'error');
          }
        });
      });

      $('#submit').on('click',function(){
        if(CheckAttendenceNotFill){
          swal('Error','This Class Attendence Already filled.','error');
          return false;
        }

        let start_date = $('#start_date').val();
        let end_date = $('#end_date').val();
        let class_id = $('#class').val();
        let teacher_id = $('#teacher').val();
        let lesson_id = $('#lesson').val();
        let school_id = $('#school_user').val();

        if(start_date == ""){
          swal('Error','Start date is required.','error');
          return false;
        }
        if(end_date == ""){
          swal('Error','End date is required.','error');
          return false;
        }
        if(class_id == ""){
          swal('Error','Class is required.','error');
          return false;
        }
        if(teacher_id == ""){
          swal('Error','Teacher is required.','error');
          return false;
        }
        if(lesson_id == ""){
          swal('Error','Lesson is required.','error');
          return false;
        }

        if(IfStudentExist == 0){
           swal('Error','There is no any student please select another class','error');
           return false;
        }

        $.ajax({
              method: 'POST',
              url: "{{url('attempt/attendence')}}",
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {"start_date":start_date,"end_date":end_date,"class_id":class_id,"teacher_id":teacher_id,"lesson_id":lesson_id,"school_id":school_id},
              success: function(data) {
                var res = $.parseJSON(data);
                if(res.status == 'error'){
                  swal('Error',res.message,'error');
                }else{
                  url = "?school_id="+school_id+"&class_id="+class_id+"&lesson_id="+lesson_id+"&teacher_id="+teacher_id;
                  window.location.href = "{{url('add_attendance/class')}}/"+url;
               } 
             },
             error: function(data) {
              swal('Error',data,'error');
            }
        });
      });


      /*$('#start_date, #end_date').datepicker({
        todayHighlight: true,
        orientation: "bottom left",
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
      });*/
      $('#m_datepicker_5').datepicker({
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
      });
      $('#addSA').click(function(e){
        e.preventDefault();
        $('#selected_school').val('');
        $('#start_date').val('');
        $('#end_date').val('');
        $('#class').html('<option selected disabled>Select Class</option>');
        $('#teacher').html('<option selected disabled>Select Teacher</option>');
        $('#lesson').html('<option selected disabled>Select Lesson</option>');

      });



      var datatable;
      (function() {
        $('.loader_msg').css('display','none');
        datatable = $('.Student_Attendance_datatable').mDatatable({
                // datasource definition
                data: {
                  type: 'remote',
                  source: {
                    read: {
                      url: APP_URL+'/view-attendance/show',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                            country_list: '',
                            ll_name: '',
                          }
                        },
                        map: function(raw) {

                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                           dataSet = raw.data;
                         }
                         console.log('Result');
                         console.log(dataSet);
                         return dataSet;
                       },
                     }
                   },
                   pageSize: 10,
                   saveState: {
                    cookie: false,
                    webstorage: false
                  },

                  serverPaging: true,
                  serverFiltering: false,
                  serverSorting: false
                },
          // layout definition
          layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

           /* search: {
              input: $('#generalSearch')
            },*/

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
              field: "S.no",
              title: "S.No",
              width: 40
            },
            {
              field: "school_name",
              title: "School",
              textAlign: 'center'
            },
            // {
            //   field: "student_name",
            //   title: "Student",
            //   textAlign: 'center'
            // },         
            {
              field: "class_name",
              title: "Class",
              textAlign: 'center'
            },
            // {
            //   field: "lesson",
            //   title: "Lesson",
            //   textAlign: 'center'
            // },            
            {
              field: "status",
              title: "Status",
              textAlign: 'center',
              template: function(row) {
                if(row.status==1){
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Deactivate" onclick=activate_deactivateSA('+row.id+')>\
                    Deactivate\
                    </a>\
                 ';
                }
                else
                {
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Activate" onclick=activate_deactivateSA('+row.id+')>\
                   Activate\
                   </a>\
                 ';
                }

              }
            },
           {
              width: 110,
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              field: 'Actions',
              template: function(row) {
                let url = "?school_id="+row.school_id+"&class_id="+row.class_id+"&lesson_id="+row.lesson_id+"&teacher_id="+row.teacher_id;
                let EditPath = "{{url('add_attendance/class')}}/"+url;
                 return '\
                 <a href="'+EditPath+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                   <i class="la la-edit"></i>\
                   </a>\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick=deleteSA('+row.class_id+')>\
                   <i class="la la-trash"></i>\
                   </a>\
                   \
                 ';
             },
           }]
         });

$('#addSAbtn').on('click',function(){
  datatable.reload();
});

$('#generalSearch').on('change',function(){
  var value = $(this).val();
  var country_id = $('#country_list').val();
  datatable.setDataSourceQuery({ll_name:value,country_id:country_id});
  datatable.reload();
});

$("#LoadSADatatable").on('click',function(){
  datatable.reload();
});

})();


function getSADetail(id){
  var path = APP_URL + "/student-attendance/edit";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);
            var tl_id = "";
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              $('#addStudentAttendanceModal').data('id',data.id);
              $('#addStudentAttendanceModal').find('.modal-title').html('Update Student Attendance ' + data.name);
              $("#PasswordDiv").html('');
              $('#name').val(data.name);
              $('#email').val(data.email);
              $('#contact').val(data.contact);
              $('#country_code').val(data.country_code);
              $('#country').val(data.country);
              $('#state').val(data.location);
              tl_id = data.tl_id;
              $.ajax
              ({
                url: APP_URL + "/location-licensee/getallTLs",
                type: 'POST',              
                data: {id:data.country},
                headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               success: function (data) {

                var res = $.parseJSON(data);
                if(res.length==0){
                  $('#tl_user').find('option').remove().end();
                  var tl_option = $('#tl_user');
                  tl_option.append(
                  $('<option></option>').val('').html('Select Territory Licensee')
                  );
                }else{
                 $('#tl_user').find('option').remove().end();
                 var tl_option = $('#tl_user');
                 tl_option.append(
                  $('<option></option>').val('').html('Select Territory Licensee')
                  );
                 var tl_selected="";
                 $.each(res, function(idx,values){
                  var tl_name = values.name;
                  if(tl_id==values.id)
                  {
                    tl_selected = "selected";
                  }
                  else
                  {
                    tl_selected="";
                  }
                    tl_option.append($('<option '+tl_selected+'></option>').val(values.id).html(tl_name));
                });
               } 
               },
               cache: false,
              });
              //loadTLData(data.country,data.tl_id);
              $('#addStudentAttendanceModal').modal('show');
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
}


function deleteSA(class_id){
  var path = APP_URL + "/attendence/delete/"+class_id;
  swal({
    title: "Are you sure to delete this LL?",
    text: "You will not be able to recover this.",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-warning",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm) {
    if (isConfirm) {
      $.ajax({
        method: 'GET',
        url: path,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          var res = $.parseJSON(data);
          if(res.status == 'error'){
            swal('Error',res.message,'error');
          }else{
           $('.sweet-overlay').remove();
           $('.showSweetAlert ').remove();
           $("#ResponseSuccessModal").modal('show');
           $("#ResponseSuccessModal #ResponseHeading").text(res.message);
         } 
       },
       error: function(data) {
        swal('Error',data,'error');
      }
    });
    } else {

    }
  });
}


$('#addSAbtn').click(function(e){
  e.preventDefault();

  var id = $('#addStudentAttendanceModal').data('id');
  var country  = $('#country').val();
  var tl_id  = $('#tl_user').val();
  var state  = $('#state').val();
  var school  = $('#school').val();
  var name  = $('#name').val();
  var email  = $('#email').val();
  var country_code  = $('#country_code').val();
  var contact  = $('#contact').val();
  var password  = $('#password').val();
  
  if(country==''){
    swal("Error","Country is required","error");
    return false;
  }
  if(tl_id==''){
    swal("Error","Territory Licensee is required","error");
    return false;
  }
  if(state==''){
    swal("Error","State is required","error");
    return false;
  }
  if(school==''){
    swal("Error","school is required","error");
    return false;
  }
  if(name==''){
    swal("Error","Name is required","error");
    return false;
  }
  if(email==''){
    swal("Error","Email is required","error");
    return false;
  }
  if(country_code==''){
    swal("Error","Country code for contact number is required","error");
    return false;
  }
  if(contact==''){
    swal("Error","Contact number is required","error");
    return false;
  }

  if(password=='undefined'){
   var llpassword  = '';
 }else{

   if($('#password').val()==''){
    swal("","Password field is required","error");
    return false;
  }
  var llpassword  = $('#password').val();
}


$.ajax({
  method: 'POST',
  url: $("#StoreStudentAttendance").attr('action'),
  data: {
    id: id,
    country: country,
    tl_id: tl_id,
    state: state,
    school: school,
    name:name,
    email:email,
    country_code:country_code,
    contact:contact,
    password:llpassword,
  },
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  success: function(data) {
    var res = $.parseJSON(data);
    if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
      $('#addStudentAttendanceModal').modal('hide');
      $("#ResponseSuccessModal").modal('show');
      $("#ResponseSuccessModal #ResponseHeading").text(res.message);
    } 
  },
  error: function(data) {
    swal('Error',data,'error');
  }
});
});

</script>
@endsection

