@extends('layouts.admin')
@section('page_title') 
Student Attendance
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Attendance & MT Earning
            </h3>
          </div> 
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row">
        <input type="hidden" id="class_id" value="@php if(isset($_GET['class_id'])) {echo $_GET['class_id'];}@endphp">
        <input type="hidden" id="teacher_id" value="@php if(isset($_GET['teacher_id'])) {echo $_GET['teacher_id'];}@endphp">
        <input type="hidden" id="lesson_id" value="@php if(isset($_GET['lesson_id'])) {echo $_GET['lesson_id'];}@endphp">
        <input type="hidden" id="school_id" value="@php if(isset($_GET['school_id'])) {echo $_GET['school_id'];}@endphp">
      <div class="col-md-4 col-lg-5">
        <div class="m-portlet m-portlet--full-height  ">
          <div class="m-portlet__body">
            <h5>Assigned Teacher</h5>
            <div class="m-card-profile display_flex"> 
              <div class="m-card-profile__pic">
                <div class="m-card-profile__pic-wrapper">
                  <img src="{{url('assets/app/media/img/users/user4.jpg')}}" alt="">
                </div>
              </div>
              <div class="m-card-profile__details">
                <span class="m-card-profile__name">
                  {{$teacher_data->name}}
                </span>
                <a href="" class="m-card-profile__email m-link font13">
                  {{$teacher_data->email}}
                </a>
              </div>
            </div>     
            <div class="m-widget1 m-widget1--paddingless">
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title">
                      Lesson
                    </h3> 
                  </div>
                  <div class="col m--align-right">
                    <span class="m-widget1__number m--font-brand">
                      Lesson 1
                    </span>
                  </div>
                </div>
              </div>
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title">
                      Start to end Date
                    </h3> 
                  </div>
                  <div class="col m--align-right">
                    <span class="m-widget1__number m--font-brand">
                      {{$class_data->start_date}} to {{$class_data->end_date}}
                    </span>
                  </div>
                </div>
              </div>
              <div class="m-portlet__body-separator"></div>

              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title">
                      School
                    </h3> 
                  </div>
                  <div class="col m--align-right">
                    <span class="m-widget1__number m--font-brand">
                      {{$class_data->school_name}}
                    </span>
                  </div>
                </div>
              </div>
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title">
                      Class
                    </h3> 
                  </div>
                <div class="col m--align-right">
                    <span class="m-widget1__number m--font-brand">
                      {{$class_data->title}}
                    </span>
                </div>
                </div>
              </div>
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title">
                      Total Students
                    </h3> 
                  </div>
                  <div class="col m--align-right">
                    <span class="m-widget1__number m--font-brand">
                      {{$class_data->total_students}}
                    </span>
                  </div>
                </div>
              </div>
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <p><small><span class="text-danger">* </span> If you want your invoices addressed to a company. Leave blank to use your full name.</small>
                    </p>
                  </div> 
                </div>
              </div>
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col text-center">
                    <button type="button" class="btn btn-accent background_gradient theme_shadow btn-view" id="submit_attendance">Submit</button>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <div class="col-md-8 col-lg-7">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="child_datatable"></div>
        </div>
        <!--end: Datatable -->
        </div>
      </div>
    </div>

  <!--end::Modal-->
    
  <!-- Modal for response -->

  <!-- Modal for success response -->
  <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <form name="fm-student">
          <div class="modal-body">
            <h5 id="ResponseHeading"></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" data-dismiss="modal" id="LoadSADatatable">
              OK
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
@endsection
@section('page_script')
<script>

    var globleEditTable;
    var OldSelectedUser = [];
    $(document).ready(function(){
      setTimeout(function(){
        DataTableCheckBoxRerender(globleEditTable);
      },1500);
    });

    function DataTableCheckBoxRerender(globleEditTable){

        $('.mt_box input').on('change',function(){
            let id = $(this).data('id');
            if(!$('input[type=checkbox][value='+id+']').prop('checked')){
              $('input[type=checkbox][value='+id+']').trigger('click');
            }
        });

        // For CheckBox of MT Earning based
        if(globleEditTable){
          console.log(globleEditTable);
          $.each(globleEditTable,function(k,v){
            if(v.attendance === "1"){
              OldSelectedUser.push(v.child_id);
              $('input[type=checkbox][value='+v.child_id+']').trigger('click');
            }
            //$('input[type=checkbox][value='+v.child_id+']').trigger('click');
          });
        }
    }

      var class_id = $('#class_id').val();
      var datatable;
      (function() {
        datatable = $('.child_datatable').mDatatable({
                // datasource definition
                data: {
                  type: 'remote',
                  source: {
                    read: {
                      url: APP_URL+'/class/'+class_id+'/getAllClassChild',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                            child_name: '',
                          }
                        },
                        map: function(raw) {
                          globleEditTable = raw.data;
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                           dataSet = raw.data;
                         }
                         console.log('Result');
                         console.log(dataSet);
                         return dataSet;
                       },
                     }
                   },
                   pageSize: 10,
                   saveState: {
                    cookie: false,
                    webstorage: false
                  },

                  serverPaging: true,
                  serverFiltering: false,
                  serverSorting: false
                },
          // layout definition
          layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

           /* search: {
              input: $('#generalSearch')
            },*/

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
              field: "child_id",
              title: "#",
              selector: {class:'m-checkbox--solid m-checkbox--brand send_mail'},
              width: 40
            },
            {
              field: "S_No",
              title: "S.No",
              width: 40
            },
            {
              field: "name",
              title: "Name",
              textAlign: 'center'
            },                        
            {
              field: "contact",
              title: "Contact",
              textAlign: 'center'
            },
            {
              field: "action",
              title: "MT Earning",
              textAlign: 'center',
              template: function(row) {
                  return '<div class="mt_box"><input type="text" class="form-control mt_box_earning mt_earning_input mt_earning_0" value="'+row.mt_earning+'" data-id="'+row.child_id+'"></div>';
              }
            }]
         });
        
        $('.child_datatable').on('m-datatable--on-check', function (e, args) 
        {
            var count = datatable.getSelectedRecords().length;
            /** WebQuiz changes for Digital Bank merger **/

            selected_id = $.map(datatable.getSelectedRecords(), function (item) {
              var data_array = {};
              data_array['id'] =  $(item).find("td").eq(0).find("input").val();
              data_array['mt_points'] = $(item).find("td").eq(4).find("input").val();
              //data_array.push(id,mt_points);
              return data_array
            });
            //console.log(selected_id);
            $('#m_datatable_selected_number').html(count);
            if (count > 0) {
            $('#store_reward').show();
            }
        })
          .on('m-datatable--on-uncheck m-datatable--on-layout-updated', function (e, args) {
             if(!args.table){
                selected_id = $.map(datatable.getSelectedRecords(), function (item) {
                  var data_array = {};
                  data_array['id'] =  $(item).find("td").eq(0).find("input").val();
                  data_array['mt_points'] = $(item).find("td").eq(4).find("input").val();
                  //data_array.push(id,mt_points);
                  return data_array
                });
             }
          });

          $('#submit_attendance').on('click',function(){
            var path        = APP_URL + "/class/submit_attendance";
            var school_id   = $('#school_id').val();
            var class_id    = $('#class_id').val();
            var lesson_id   = $('#lesson_id').val();
            var teacher_id  = $('#teacher_id').val();
            var student_data = JSON.parse(JSON.stringify(selected_id));
            $.ajax({
              method: "POST",
              url: path,
              data: {
                school_id:school_id,
                class_id:class_id,
                lesson_id:lesson_id,
                teacher_id:teacher_id,
                student_data:student_data,
                OldSelectedUser:OldSelectedUser.join()
              },
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(result){
                      //console.log(result);
                      var res = $.parseJSON(result);
                      
                      if(res.status == 'error'){

                      }else{
                        var data = $.parseJSON(JSON.stringify(res.message));
                        $('#addParentModal').data('id',data.id);
                        $('#addParentModal').find('.modal-title').html('Update Parent ' + data.name);
                        $("#PasswordDiv").html('');
                        $('#name').val(data.name);
                        $('#email').val(data.email);
                        $('#contact').val(data.contact);
                        $('#country_code').val(data.country_code);
                        $('#country').val(data.country_id);
                        $('#addParentModal').modal('show');
                      }
                      swal('Success',res.message,'success');
                    },
                    error: function(){
                      swal('error',res.message,'error');
                      alert("Error");
                    }
            }); 
          });

})();
</script>
@endsection

