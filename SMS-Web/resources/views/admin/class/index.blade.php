@extends('layouts.admin')
@section('page_title') 
Class
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
             Classes
            </h3>
          </div> 
          <!--begin: Search Form -->
          <div class="m-form m-form--label-align-right">
            <div class="align-items-center">
              <div class="display_flex">
                <div class="col-xl-8 col-8">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="">
                      <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search class name" id="generalSearch">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span>
                            <i class="la la-search"></i>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-4 m--align-right">
                  <a href="{{route('addClassView')}}" class="btn background_gradient btn-accent add_btn theme_shadow" >
                    <span>
                      <i class="la la-plus"></i> 
                    </span>
                  </a> 
                </div>
              </div>
            </div>
          </div>
          <!--end: Search Form -->
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="class_datatable"></div>
        </div>
      </div>
      <div class="space50"></div>
    </div>
  <!--end::Modal-->
  <!--begin:: Add Class Modal-->
    <!--<div class="modal fade" id="addClassModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" id="search_form" style="display: none;">
            <form>
              <div class="form-group display_flex">
                 <div class="mr-auto">
                    <label for="recipient-name" class="form-control-label font13">
                      Country
                    </label>
                    <select class="form-control m-input m-input--square no_border_field pl0" id="country" name="country">
                      <option value="">Please Select Country</option>
                      @inject('countries','App\Model\Countries')
                      @php($countries = $countries->where(['default_country'=>1])->get())
                      @foreach($countries as $name)
                      <option value="{{$name->id}}" short_name="{{$name->code}}">{{$name->name}}</option>
                      @endforeach
                  </select>
                 </div>
                 <div class="mr-auto">
                    <label for="tl_user" class="form-control-label font13">
                      Territory Licensee
                    </label>
                    <select class="form-control m-input m-input--square no_border_field pl0" id="tl_user" name="tl_user">
                      <option value="">Select Territory Licensee</option>
                    </select>
                 </div>
              </div>
              <div class="form-group display_flex">
                  <div class="mr-auto">
                    <label for="ll_user" class="form-control-label font13">
                      Location Licensee
                    </label>
                    <select class="form-control m-input m-input--square no_border_field pl0" id="ll_user" name="ll_user">
                      <option value="">Select Location Licensee</option>
                    </select>
                 </div>
                 <div class="mr-auto">
                    <label for="ll_user" class="form-control-label font13">
                      School
                    </label>
                    <select class="form-control m-input m-input--square no_border_field pl0" id="school_user" name="school_user">
                      <option value="">Select School</option>
                    </select>
                 </div>
              </div>
            </form>
          </div>
          <div class="modal-body">
            <h5 class="modal-title" id="exampleModalLabel">
              Add Class
            </h5>
            <form id="StoreClass" action="{{route('saveClass')}}">
              <div class="form-group">
                <label for="state" class="form-control-label font13">
                  School
                </label>
                <input type="text" class="form-control border_bottom pl0" id="selected_school" name="selected_school" placeholder="Selected School" readonly>
                <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="school_search">Search</button>
              </div>
              <div class="form-group">
                <label for="name" class="form-control-label font13">
                  Class Name *
                </label>
                <input type="text" class="form-control border_bottom pl0" id="name" name="name" placeholder="Enter name of Class">
              </div>                                         
            </form>
          </div>
          <div class="text-center moda_footer">
            <button type="button" class="btn btn-danger btn-view no_border_field" data-dismiss="modal">
              Cancel
            </button>
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="addClassbtn">
              Save Changes
            </button>
          </div>
        </div>
      </div>
    </div>-->
  <!--end::Modal-->
  
  <!-- Modal for response -->

  <!-- Modal for success response -->
  <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <form name="fm-student">
          <div class="modal-body">
            <h5 id="ResponseHeading"></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" data-dismiss="modal" id="LoadClassDatatable">
              OK
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
@endsection
@section('page_script')
  <script>
    var datatable;
    (function() {
      datatable = $('.class_datatable').mDatatable({
                // datasource definition
                data: {
                  type: 'remote',
                  source: {
                    read: {
                      url: APP_URL+'/class/show',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                            class_name: '',
                           
                          }
                        },
                        map: function(raw) {
                          console.log('ITS RUN');
                          console.log(raw);
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                           dataSet = raw.data;
                         }
                         console.log('Result');
                         console.log(dataSet);
                         return dataSet;
                       },
                     }
                   },
                   pageSize: 10,
                   saveState: {
                    cookie: false,
                    webstorage: false
                  },

                  serverPaging: true,
                  serverFiltering: false,
                  serverSorting: false
                },
          // layout definition
          layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: false,

            pagination: true,

           /* search: {
              input: $('#generalSearch')
            },*/

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
              field: "S_No",
              title: "S.No",
              width: 40
            },
            {
              field: "title",
              title: "Class Title",
              textAlign: 'center'
            },
            {
              field: "school_name",
              title: "School",
              textAlign: 'center'
            },
            {
              field: "status",
              title: "Status",
              textAlign: 'center',
              template: function(row) {
                if(row.status==1){
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Deactivate" onclick=activate_deactivateClass('+row.id+')>\
                    Deactivate\
                    </a>\
                 ';
                }
                else
                {
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Activate" onclick=activate_deactivateClass('+row.id+')>\
                   Activate\
                   </a>\
                 ';
                }

              }
            },
           {
              width: 110,
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              field: 'Actions',
              template: function(row) {
                 let path = "{{url('class/edit/')}}/"+row.id;
                 return '\
                 <a href='+path+' class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                   <i class="la la-edit"></i>\
                   </a>\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteClass('+row.id+')>\
                   <i class="la la-trash"></i>\
                   </a>\
                   \
                 ';
               }

             }]
          });

  $('#generalSearch').on('change',function(){
    var value = $(this).val();
    datatable.setDataSourceQuery({class_name:value});
    datatable.reload();
  });
  })();

function deleteClass(id){
  var path = APP_URL + "/class/destroy";
  var _this = $(this);
  swal({
    title: "Are you sure to delete this Class?",
    text: "You will not be able to recover this.",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-warning",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm) {
    if (isConfirm) {
      var data = id;
      $.ajax({
        method: 'POST',
        url: path,
        data: {
          id: data,
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          var res = $.parseJSON(data);
          if(res.status == 'error'){
            swal('Error',res.message,'error');
            datatable.reload();
          }else{
           $('.sweet-overlay').remove();
           $('.showSweetAlert ').remove();
           $("#ResponseSuccessModal").modal('show');
           $("#ResponseSuccessModal #ResponseHeading").text(res.message);
           datatable.reload();
         } 
       },
       error: function(data) {
        swal('Error',data,'error');
        datatable.reload();
      }
    });
    } else {
      datatable.reload();
    }
  });
}

function activate_deactivateClass(id){
  var path = APP_URL + "/class/changeClassStatus";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
     var res = $.parseJSON(result);
     if(res.status == 'error'){
      swal('Error',res.message,'error');
      datatable.reload();
    }else{
     $('.sweet-overlay').remove();
     $('.showSweetAlert ').remove();
     $("#ResponseSuccessModal").modal('show');
     $("#ResponseSuccessModal #ResponseHeading").text(res.message);
     datatable.reload();
   } 
 },
 error: function(){
  alert("Error");
  datatable.reload();
}
}); 
}
</script>
@endsection