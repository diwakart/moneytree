@extends('layouts.admin')
@section('page_title') 
Add Class
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
                @if(isset($editdata))
								Edit Class
                @else
                Add Class
                @endif
							</h3>
						</div>
					</div>
				</div>
				<!--begin::Form-->
				<form id="StoreClass" action="{{route('saveClass')}}" class="m-form">
					<div class="m-portlet__body">
						<div class="m-form__section m-form__section--first">
                <div class="form-group display_flex AppendSearchDiv">
                </div>
              <!-- Here is I have check the role If the same then hide the search and put Id into the field -->
                @if(Auth::user()->user_type != 4)
                  <label for="state" class="form-control-label font13">
                    School
                  </label>
                  <input type="text" class="form-control border_bottom pl0 GlobelSearchDisplayName" id="state" name="state" placeholder="Enter School" readonly>
                  <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field GlobleSearch" id="add_ll_search1">Search</button>
                  <input type="hidden" name="school_user" value="" id="school_user" class="GlobleSearchResult">
                @else
                  <input type="hidden" name="school_user" value="{{Auth::id()}}" id="school_user" class="GlobleSearchResult">
                @endif
                  <input type="hidden" name="id" value="" id="id">
							<!-- <div class="form-group">
                                <label for="recipient-name" class="form-control-label font13">
                                    Country
                                </label>
                                <select class="form-control border_bottom pl0" id="country" name="country">
                                    <option value="">Please Select Country</option>
                                    @inject('countries','App\Model\Countries')
                                    @php($countries = $countries->where(['default_country'=>1])->get())
                                    @foreach($countries as $name)
                                        <option value="{{$name->id}}" short_name="{{$name->code}}">{{$name->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                              <label for="tl_user" class="form-control-label font13">
                                Territory Licensee
                              </label>
                              <select class="form-control m-input m-input--square no_border_field pl0" id="tl_user" name="tl_user">
                                <option value="">Select Territory Licensee</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="ll_user" class="form-control-label font13">
                                Location Licensee
                              </label>
                              <select class="form-control m-input m-input--square no_border_field pl0" id="ll_user" name="ll_user">
                                <option value="">Select Location Licensee</option>
                              </select>
                            </div>
                
                            <div class="form-group">
                              <label for="ll_user" class="form-control-label font13">
                                School
                              </label>
                              <select class="form-control m-input m-input--square no_border_field pl0" id="school_user" name="school_user">
                                <option value="">Select School</option>
                              </select>
                            </div> -->
                            <div class="form-group">
                              <label class="col-form-label font13">
                                Teacher
                              </label>
                              <select class="form-control m-bootstrap-select m_selectpicker" multiple data-max-options="10" id="teacherDropDown" name="teacher[]" >
                                <option value="">Select Teacher</option>
                              </select>
                            </div>
                            <input type="hidden" value="" name="assignedStudents" id="assignedStudents">
                            <div class="form-group" id="assined_student_div" style="display:none">
                                <label for="name" class="form-control-label font13">
                                  Assign Students *
                                </label>
                                <div class="student_datatable"></div>
                            </div>
                            <div class="form-group">
                              <label class="col-form-label font13">
                                Lesson Level
                              </label>
                                <select class="form-control m-input m-input--square" id="lesson_level" name="lesson_level">
                                  <option value="">Select Lesson Level</option>
                                </select>
                            </div>
                            <div class="form-group">
                              <label class="col-form-label font13">
                                Lesson Sub Level
                              </label>
                              <select class="form-control m-input m-input--square" name="lesson_sub_level" id="lesson_sub_level">
                                <option value="">Select Lesson Sub Level</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label class="col-form-label font13">
                                Lesson
                              </label>
                              <select class="form-control m-bootstrap-select m1_selectpicker" multiple data-max-options="10" id="lessondropdown" name="lesson[]" >
                                <option value="">Select Lesson</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="name" class="form-control-label font13">
                                Class Title *
                              </label>
                              <input type="text" class="form-control" id="class_title" name="class_title" placeholder="Enter Title Of Class">
                            </div>
                            <!--<div class="form-group">
                                <label for="name" class="form-control-label font13">
                                Range Date of Class *
                              </label>
                                <div class="input-daterange input-group" id="m_datepicker_5">
    								<input type="text" class="form-control m-input" name="start" placeholder="Please select Start date of class" readonly>
    								<span class="input-group-addon">
    									<i class="la la-ellipsis-h"></i>
    								</span>
    								<input type="text" class="form-control" name="end" placeholder="Please select End date of class" readonly>
    							</div>
							</div>-->
                            <!--<div class="form-group input-daterange input-group">
                              <label for="name" class="form-control-label font13">
                                Start Date *
                              </label>
                              <input type="date" class="form-control" id="start_date" name="start_date" placeholder="Please Select start date of class" readonly>
                            </div>
                
                            <div class="form-group">
                              <label for="name" class="form-control-label font13">
                                End Date *
                              </label>
                              <input type="text" class="form-control" id="end_date" name="end_date" placeholder="Please Select End date of class" readonly>
                            </div>--> 
                            <div class="form-group">
                              <label for="ll_user" class="form-control-label font13">
                                Status
                              </label>
                              <select class="form-control m-input m-input--square no_border_field pl0" id="class_status" name="class_status">
                                <option value="">Select Status</option>
                                <option value="1">Active</option>
                                <option value="2">Inactive</option>
                              </select>
                            </div>
						</div>
					</div>
					<div class="m-portlet__foot m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions">
							<button type="reset" class="btn btn-primary" id="addClassbtn">
								Submit
							</button>
							<button type="reset" class="btn btn-secondary">
								Cancel
							</button>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Portlet-->
		</div>
	</div>
</div>
@endsection
@section('page_script')
  <script>
    // Get level and sulevel from API
    getLevelSubLevel();
    $('#lesson_sub_level').change(function(){
      getLesson();
    });

    var selected_id;
    var AllOldStudentId;
    $('.m1_selectpicker').selectpicker();

    $('.m_selectpicker').selectpicker();
    $('#m_datepicker_5').datepicker({
        todayHighlight: true,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });
    
$('.GlobleSearchResult').on('click',function(){
    var id = $(this).val();
    getAllTeacher(id);
});

function getAllTeacher(id="")
{
  $.ajax({
      method: 'GET',
      url: APP_URL+'/school/'+id+'/getAllTeachers',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(data) {
          let HTML = "";
          $.each(data, function(key,value){
            HTML += "<option value="+value.id+">"+value.name+"</option>";
          });
          $('.m_selectpicker').html('');
          $('.m_selectpicker').html(HTML);
          $('.m_selectpicker').selectpicker('refresh');
        
      },
      error: function(data) {
        swal('Error',data,'error');
      }
    });
}

function getLevelSubLevel(){
  $.ajax({
    method: 'GET',
    url: "{{url('API/lesson/getLsubL')}}",
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data) {
      //console.log(data);
      var level = "<option>Select Lesson Level</option>";
      var sublevel = "<option>Select Lesson Sub Level</option>";
      $.each(data,function(key,val){
        $.each(val,function(key1,val1){
          if(key == "level"){
            level+="<option value="+val1.id+">"+val1.name+"</option>";

            $('#lesson_level').html(level);
          }
          if(key == "Sublevel"){
            sublevel+="<option value="+val1.id+">"+val1.name+"</option>";
            $('#lesson_sub_level').html(sublevel);
          }
        });

      });
    },
    error: function(data) {
      swal('Error',data,'error');
    }
  });
}

function getLesson(){
  let lessonLevel = $('#lesson_level').val();
  let lessonSubLevel = $('#lesson_sub_level').val();
  $.ajax({
    method: 'GET',
    url: "{{url('API/lesson/getLesson')}}/"+lessonLevel+"/"+lessonSubLevel,
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data) {
      //console.log(data);
      var lesson = "<option>Select Lesson</option>";
      $.each(data.lesson,function(key,val){
        lesson+="<option value="+val.id+">"+val.name+"</option>";
      });
      $('#lessondropdown').html(lesson);
      $('.m1_selectpicker').selectpicker('refresh');
    },
    error: function(data) {
      swal('Error',data,'error');
    }
  });
}

$('.m_selectpicker').on('change',function(){
    $('#assined_student_div').show();
    var id = $('.GlobleSearchResult').val();
    var datatable;
      (function() {
        $('.loader_msg').css('display','none');
        datatable = $('.student_datatable').mDatatable({
                // datasource definition
                data: {
                  type: 'remote',
                  source: {
                    read: {
                      url: APP_URL+'/school/'+id+'/getAllStudentsOfSchool',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                            country_list: '',
                            ll_name: '',
                          }
                        },
                        map: function(raw) {

                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                           dataSet = raw.data;
                         }
                         console.log('Result');
                         console.log(dataSet);
                         return dataSet;
                       },
                     }
                   },
                   pageSize: 10,
                   saveState: {
                    cookie: false,
                    webstorage: false
                  },

                  serverPaging: true,
                  serverFiltering: false,
                  serverSorting: false
                },
                 draw: function( settings ) {
                        console.log('INNN');
                        checkBoxLoop();
                       },
          // layout definition
          layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,
            columns: [{
              field: "id",
              title: "#",
              selector: {class:'m-checkbox--solid m-checkbox--brand StudentCheckBox'},
              width: 40,
            },
            {
              field: "name",
              title: "Name",
              textAlign: 'center'
            },
            {
              field: "email",
              title: "Email",
              textAlign: 'center'
            },
            {
              field: "contact",
              title: "Contact",
              textAlign: 'center'
            }]
         });
         $('.student_datatable').on('m-datatable--on-check', function (e, args) 
         {
            //var count = datatable.getSelectedRecords().length;
            
            /** WebQuiz changes for Digital Bank merger **/
            
            selected_id = $.map(datatable.getSelectedRecords(), function (item) {
            return $(item).find("td").eq(0).find("input").val();
            });
            console.log('IIIIII');
            console.log(selected_id);
                /*$('#m_datatable_selected_number').html(count);
            if (count > 0) {
                $('#store_reward').show();
            }*/
        })
        .on('m-datatable--on-uncheck m-datatable--on-layout-updated', function (e, args) {
            if(!args.table){
               selected_id1 = selected_id
               selected_id1 = $.map(datatable.getSelectedRecords(), function (item) {
                return $(item).find("td").eq(0).find("input").val();
                });
               selected_id = selected_id1;
             }
        });
})();
});


  $('#addClassbtn').click(function(e){
  e.preventDefault();
  let school_id  = $('#school_user').val();
  let teacher_id = $('#teacherDropDown').val().join();
  let class_title  = $('#class_title').val();
  let class_status  = $('#class_status').val();

  let lesson1 = $('.m1_selectpicker').val();
  let lessonLevel = $('#lesson_level').val();
  let lessonSubLevel = $('#lesson_sub_level').val();

  let student_ids;

  if(lesson1==""){
    swal("Error","Please Assigned Lesson","error");
    return false;
  }

  if(AllOldStudentId instanceof Array){
    AllOldStudentId = AllOldStudentId.join();
  }
  if(lesson1 instanceof Array){
    lesson1 = lesson1.join();
  }

  if(selected_id)
  {
    student_ids =selected_id.join();
    
  }else{
    @if(!isset($editdata))
      swal("Error","Student is required","error");
      return false;
    @endif
  }

  if(teacher_id==''){
    swal("Error","Please Assigned teachers in class","error");
    return false;
  }

  if(lessonLevel==""){
    swal("Error","Please Assigned Lesson Level","error");
    return false;
  } 

  if(lessonSubLevel==""){
    swal("Error","Please Assigned Lesson Sub Level","error");
    return false;
  }

$.ajax({
  method: 'POST',
  url: $("#StoreClass").attr('action'),
  data: {
    id:$('#id').val(),
    school_id: school_id,
    teacher_id:teacher_id,
    student_ids:student_ids,
    class_title:class_title,
    class_status:class_status,
    lesson_id:lesson1,
    lessonLevel:lessonLevel,
    lessonSubLevel:lessonSubLevel,
    AllOldStudentId:AllOldStudentId
  },
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  success: function(data) {
    var res = $.parseJSON(data);
    if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
      swal('Success',res.message,'success');
      setTimeout(function(){ window.location.replace("{{url('/')}}/class"); }, 2000);
    } 
  },
  error: function(data) {
    swal('Error',data,'error');
  }
});
});





@if(isset($editdata))
  let data = '<?php echo json_encode($editdata->toArray()); ?>';
  let StudentList = '<?php echo $assignedstudent; ?>';
  data = JSON.parse(data);
  console.log(data);
  console.log('OKKKK');
  console.log(StudentList.split(','));
  $('.GlobelSearchDisplayName').val(data.name);
  $('#id').val(data.id);
  $('#school_user').val(data.school_id);
  $('#class_title').val(data.title);
  $('#class_status').val(data.status);

  getAllTeacher(data.school_id);

  setTimeout(function(){
    $('#lesson_level').val(data.level_id);
    $('#lesson_sub_level').val(data.sublevel_id);
    getLesson();
  },800);

  setTimeout(function(){
    $('#teacherDropDown').val(data.teacher_id.split(','));
    $('.m1_selectpicker').val(data.lesson_id.split(','));
    $('.m1_selectpicker').selectpicker('refresh');
    $('.m_selectpicker').selectpicker('refresh');
    $('.m_selectpicker').trigger('change');
    selected_id = (StudentList ? StudentList.split(',') : '');
    AllOldStudentId = (StudentList ? StudentList.split(',') : '');
    checkBoxLoop();
  },1000);

  // $('input[type=checkbox]').click(function(){
  //   checkBoxLoop();
  // });

  function checkBoxLoop(){
   setTimeout(function(){
      let selected_idConst = selected_id;
      $('input[type=checkbox]').each(function(){
        if(selected_idConst){
          let res = checkArrayValueExist(selected_idConst,$(this).val());
          if(res && $(this).val().toString() != "on"){
            $(this).trigger('click');
          }
        }
      });
   },1000);
  }

  function checkArrayValueExist(arr, key)
  {
    let flag = false;
    if(arr.length != 0){
      $.each(arr,function(k,v){
        if(parseInt(v) === parseInt(key)){
          console.log('$$$$$$');
          flag = true;
        }
      });
    }
    return flag;
  }

@endif

@if(Auth::user()->user_type == 4 )
  getAllTeacher({{Auth::id()}});
@endif
</script>

@endsection