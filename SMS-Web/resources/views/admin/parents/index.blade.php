@extends('layouts.admin')
@section('page_title') 
Parent
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Parent
            </h3>
          </div> 
          <!--begin: Search Form -->
          <div class="m-form m-form--label-align-right">
            <div class="align-items-center">
              <div class="display_flex">
                <div class="col-xl-8 col-8">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="">
                      <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search Parent Name.." id="generalSearch">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span>
                            <i class="la la-search"></i>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-4 m--align-right">
                  <a href="javascript:void(0);" id="addParent" class="btn background_gradient btn-accent add_btn theme_shadow" data-toggle="modal" data-target="#addParentModal">
                    <span>
                      <i class="la la-plus"></i> 
                    </span>
                  </a> 
                </div>
              </div>
            </div>
          </div>
          <!--end: Search Form -->
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="Parent_datatable"></div>
        </div>
      </div>
      <div class="space50"></div>
    </div>

    <!--begin:: Add Parent Modal-->
  <div class="modal fade" id="addParentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content"> 
        <div class="modal-body pb0">
          <h5 class="modal-title" id="exampleModalLabel">
          Add New parents
          </h5>
          <div class="row">
            <div class="col-md-4">
                    <div class="m-dropzone dropzone no_border_field" action="{{url('parent_upload')}}" id="m-dropzone-one">
                        <div class="m-dropzone__msg dz-message needsclick">
                          <h3 class="m-dropzone__msg-title">
                            <i class="la la-cloud-upload"></i>
                          </h3>
                          <span class="m-dropzone__msg-desc">
                            Drop Files To Upload
                            <strong>
                              OR
                            </strong>
                            
                          </span>
                          <br>
                          <p class="text-success browse_txt">Browse</p>
                        </div>
                      </div>
                      <!--<div id="user_profile" style="display: none;">
                        <label>User Profile</label>
                        <p id="uploaded_image">adsd</p>
                      </div>-->
                  </div>

            <div class="col-md-8">
              <form id="StoreParent" action="{{route('saveParent')}}">
                <div class="form-group">
                  <label for="recipient-name" class="form-control-label font13">
                    Country
                  </label>
                  <select class="form-control m-input m-input--square no_border_field pl0" id="country" name="country">
                      <option value="">Please Select Country</option>
                      @inject('countries','App\Model\Countries')
                      @php($countries = $countries->where(['default_country'=>1])->get())
                      @foreach($countries as $name)
                      <option value="{{$name->id}}" short_name="{{$name->code}}">{{$name->name}}</option>
                      @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="recipient-name" class="form-control-label font13">
                    Name
                  </label>
                  <input type="text" class="form-control border_bottom pl0" id="name" name="name" placeholder="Enter name of Parent">
                </div>
                <div class="form-group">
                  <label for="recipient-name" class="form-control-label font13">
                    Email
                  </label>
                  <input type="email" class="form-control border_bottom pl0" id="email" name="email" placeholder="Enter email of Parent">
                </div>
                <div class="form-group display_flex">
                <div class="mr-auto">
                  <label for="recipient-name" class="form-control-label font13">
                  Country Code
                  </label>
                  <input type="text" class="form-control border_bottom pl0" id="country_code" name="country_code" placeholder="Country code for mobile">
                </div>
                <div class="mr-auto">
                  <label for="recipient-name" class="form-control-label font13">
                  Contact Number
                  </label>
                  <input type="text" class="form-control border_bottom pl0" id="contact" name="contact" placeholder="Enter contact number">
                </div>
                </div>
                <div class="mr-auto" id="PasswordDiv">
                </div>
                <!-- <div class="form-group">
                  <label for="recipient-name" class="form-control-label font13">
                  Password
                  </label>
                  <input type="Password" class="form-control border_bottom pl0" id="recipient-name">
                </div> -->
                <div class="text-center moda_footer">
                  <button type="button" class="btn btn-danger btn-view no_border_field" data-dismiss="modal">
                    Cancel
                  </button>
                  <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="addParentbtn">
                    Save Changes
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
            <!--end::Modal-->
    
  <!-- Modal for response -->

  <!-- Modal for success response -->
  <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <form name="fm-student">
          <div class="modal-body">
            <h5 id="ResponseHeading"></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" data-dismiss="modal" id="LoadParentDatatable">
              OK
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
@endsection
@section('page_script')
  <!-- <script src="{{url('assets/demo/default/custom/components/forms/widgets/dropzone.js')}}" type="text/javascript"></script> -->
  <script>
      $('#addParent').click(function(e){
        e.preventDefault();
        $('#name').val('');
        $('#email').val('');
        $('#contact').val('');
        $('#country_code').val('');
        $('#state').val('');
        $('#country').val('');
        $('#selected_tl').val('');
        $('#user_profile').css('display','none');
        $("#PasswordDiv").html('<label for="password" class="form-control-label">Password</label><input type="password" class="form-control border_bottom pl0" id="password" placeholder="Enter Password" name="password">');});
      var datatable;
      (function() {
        $('.loader_msg').css('display','none');
        datatable = $('.Parent_datatable').mDatatable({
                // datasource definition
                data: {
                  type: 'remote',
                  source: {
                    read: {
                      url: APP_URL+'/parent/show',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                            country_list: '',
                            ll_name: '',
                          }
                        },
                        map: function(raw) {

                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                           dataSet = raw.data;
                         }
                         console.log('Result');
                         console.log(dataSet);
                         return dataSet;
                       },
                     }
                   },
                   pageSize: 10,
                   saveState: {
                    cookie: false,
                    webstorage: false
                  },

                  serverPaging: true,
                  serverFiltering: false,
                  serverSorting: false
                },
          // layout definition
          layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

           /* search: {
              input: $('#generalSearch')
            },*/

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
              field: "S_No",
              title: "S.No",
              width: 40
            },
            {
              field: "name",
              title: "Name",
              textAlign: 'center'
            },                        
            {
              field: "email",
              title: "Email",
              textAlign: 'center'
            },
            {
              field: "contact",
              title: "Contact",
              textAlign: 'center'
            },
            {
              field: "status",
              title: "Status",
              textAlign: 'center',
              template: function(row) {
                if(row.status==1){
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Deactivate" onclick=activate_deactivateParent('+row.id+')>\
                    Deactivate\
                    </a>\
                 ';
                }
                else
                {
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Activate" onclick=activate_deactivateParent('+row.id+')>\
                   Activate\
                   </a>\
                 ';
                }

              }
            },
           {
              width: 110,
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              field: 'Actions',
              template: function(row) {
                 return '\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                   <i class="la la-edit" onclick=getParentDetail('+row.id+')></i>\
                   </a>\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteParent('+row.id+')>\
                   <i class="la la-trash"></i>\
                   </a>\
                   \
                 ';
             },
           }]
         });



$('#addParentbtn').on('click',function(){
  datatable.reload();
});

// $('#generalSearch').on('change',function(){
//   var value = $(this).val();
//   var country_id = $('#country_list').val();
//   datatable.setDataSourceQuery({ll_name:value,country_id:country_id});
//   datatable.reload();
// });

$("#LoadParentDatatable").on('click',function(){
  datatable.reload();
});

})();


function getParentDetail(id){
  var path = APP_URL + "/parent/edit";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);
            
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              $('#addParentModal').data('id',data.id);
              $('#addParentModal').find('.modal-title').html('Update Parent ' + data.name);
              $("#PasswordDiv").html('');
              $('#name').val(data.name);
              $('#email').val(data.email);
              $('#contact').val(data.contact);
              $('#country_code').val(data.country_code);
              if(data.picture!='' || data.picture!='null')
              {
                
                $('#uploaded_image').html('<img height="100" width="100" src="'+APP_URL+'/storage/app/'+data.picture+'">');
                $('#user_profile').css('display','block');
              }
              $('#country').val(data.country_id);
              
              $('#addParentModal').modal('show');
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
}

function deleteParent(id){
  var path = APP_URL + "/parent/destroy";
  var _this = $(this);
  swal({
    title: "Are you sure to delete this Parent?",
    text: "You will not be able to recover this.",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-warning",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm) {
    if (isConfirm) {
      var data = id;
      $.ajax({
        method: 'POST',
        url: path,
        data: {
          id: data,
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          var res = $.parseJSON(data);
          if(res.status == 'error'){
            swal('Error',res.message,'error');
          }else{
           $('.sweet-overlay').remove();
           $('.showSweetAlert ').remove();
           $("#ResponseSuccessModal").modal('show');
           $("#ResponseSuccessModal #ResponseHeading").text(res.message);
         } 
       },
       error: function(data) {
        swal('Error',data,'error');
      }
    });
    } else {

    }
  });
}
var image_name = "";
Dropzone.options.mDropzoneOne = {
  paramName: "user_image", // The name that will be used to transfer the file
  maxFiles: 1,
  acceptedFiles: "image/*",
  maxFilesize: 5, // MB
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  accept: function(user_image, done) {
      if (user_image.name == "justinbieber.jpg") {
          done("Naha, you don't.");
      } else { 
          done(); 
      }
  },
  success: function(file,res) {
    image_name = res;
  }  
};
$('#addParentbtn').click(function(e){
  e.preventDefault();

  var id = $('#addParentModal').data('id');
  var country  = $('#country').val();
  //alert(country); return false;
  var name  = $('#name').val();
  var email  = $('#email').val();
  var country_code  = $('#country_code').val();
  var contact  = $('#contact').val();
  var password  = $('#password').val();
  
  // if(country==''){
  //   swal("Error","Country is required","error");
  //   return false;
  // }  
  if(name==''){
    swal("Error","Name is required","error");
    return false;
  }
  if(email==''){
    swal("Error","Email is required","error");
    return false;
  }
  if(country_code==''){
    swal("Error","Country code for contact number is required","error");
    return false;
  }
  if(contact==''){
    swal("Error","Contact number is required","error");
    return false;
  }

  if(password=='undefined'){
   var llpassword  = '';
 }else{

   if($('#password').val()==''){
    swal("","Password field is required","error");
    return false;
  }
  var llpassword  = $('#password').val();
}


$.ajax({
  method: 'POST',
  url: $("#StoreParent").attr('action'),
  data: {
    id: id,
    country: country,    
    name:name,
    email:email,
    country_code:country_code,
    contact:contact,
    password:llpassword,
    image_name:image_name,
  },
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  success: function(data) {
    var res = $.parseJSON(data);
    if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
      $('#addParentModal').modal('hide');
      $("#ResponseSuccessModal").modal('show');
      $("#ResponseSuccessModal #ResponseHeading").text(res.message);
    } 
  },
  error: function(data) {
    swal('Error',data,'error');
  }
});
});

function activate_deactivateParent(id){
  var path = APP_URL + "/parent/create";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
     var res = $.parseJSON(result);
     if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
     $('.sweet-overlay').remove();
     $('.showSweetAlert ').remove();
     $("#ResponseSuccessModal").modal('show');
     $("#ResponseSuccessModal #ResponseHeading").text(res.message);
   } 
 },
 error: function(){
  alert("Error");
}
}); 
}

$('#country').on('change',function(){
  var id = $(this).val();
  $.ajax({
    method: 'POST',
    url: APP_URL+'/getCountryCode',
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data) {
      var res = $.parseJSON(data);
      if(res.status == 'error'){
        /*swal('Error',res.message,'error');*/
        $('#country_code').val('');
      }else{
        $('#country_code').val(res.data.country_code);
      } 
    },
    error: function(data) {
      swal('Error',data,'error');
    }
  });
});
</script>
@endsection
