@extends('layouts.admin')
@section('page_title') 
Teacher
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Teacher
            </h3>
          </div> 
          <!--begin: Search Form -->
          <div class="m-form m-form--label-align-right">
            <div class="align-items-center">
              <div class="display_flex">
                <div class="col-xl-8 col-8">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="">
                      <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search Teacher Name.." id="generalSearch">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span>
                            <i class="la la-search"></i>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="col-xl-2 col-2 m--align-right">
                  <a href="javascript:void(0);" id="addTeacher" class="btn background_gradient btn-accent add_btn theme_shadow" data-toggle="modal" data-target="#addTeacherModal">
                    <span>
                      <i class="la la-plus"></i> 
                    </span>
                  </a> 
                </div>
                @if(Helper::CanIf('saveTeacher'))
                <div class="col-xl-2 col-2 m--align-right">
                  <a href="javascript:void(0);" id="uploadTeacher" class="btn background_gradient btn-accent add_btn theme_shadow" data-toggle="modal" data-target="#addTeacherModal">
                    <span>
                      <i class="la la-upload"></i> 
                    </span>
                  </a> 
                </div>
                @endif
              </div>
            </div>
          </div>
          <!--end: Search Form -->
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="Teacher_datatable"></div>
        </div>
      </div>
      <div class="space50"></div>
    </div>

    <!--begin:: Add Teacher Modal-->
    <div class="modal fade" id="addTeacherModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-id="">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <h5 class="modal-title" id="exampleModalLabel">
              Add Teacher
            </h5>
            <form id="StoreTeacher" class="modal_form" action="{{route('saveTeacher')}}">
              <div class="form-group display_flex AppendSearchDiv">
              </div>
              <div class="form-group">
                @if(Auth::user()->user_type != 4)
                <label for="school" class="form-control-label font13">
                  School
                </label>
                <input type="text" class="form-control border_bottom pl0 GlobelSearchDisplayName" id="selected_school" name="selected_school" placeholder="Search School Name" readonly >
                <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field GlobleSearch" id="school_search1">Search</button>
                <input type="hidden" name="school_user" value="" id="school_user" class="GlobleSearchResult">
                @else
                  <input type="hidden" name="school_user" value="{{Auth::id()}}" id="school_user" class="GlobleSearchResult">
                @endif
              </div>

            <div class="bulkUpload" style="display: none;">
              <a href="{{url('public/samplefile/SampleCSVTeacher.csv')}}">Download Sample CSV Doc.</a>
              <div class="form-group">
                <label for="name" class="form-control-label font13">
                  Browse file *
                </label>
                <input type="file" class="form-control border_bottom pl0" id="teacherCsvFile" name="file">
              </div>
            </div>

            <div class="manualAdd">
              <div class="form-group">
                <label for="name" class="form-control-label font13">
                  Teacher Name *
                </label>
                <input type="text" class="form-control border_bottom pl0" id="name" name="name" placeholder="Enter name of Teacher">
              </div>
              <div class="form-group display_flex">
                <div class="mr-auto">
                  <label for="country_code" class="form-control-label font13">
                    Country Code *
                  </label>
                  <input type="text" class="form-control border_bottom pl0" id="country_code" name="country_code" placeholder="Country code for mobile" maxlength="6">
                </div>
                <div class="mr-auto">
                  <label for="contact" class="form-control-label font13">
                  Contact Number *
                  </label>
                  <input type="text" class="form-control border_bottom pl0" id="contact" name="contact" placeholder="Enter contact number">
                </div>
              </div>
              <div class="form-group display_flex">
                <div class="mr-auto">
                  <label for="email" class="form-control-label font13">
                  Email *
                  </label>
                  <input type="email" class="form-control border_bottom pl0" id="email" name="email" placeholder="Enter email of Teacher">
                </div>
                <div class="mr-auto" id="PasswordDiv">
                </div>
              </div>
            </div>
              
            </form>
          </div>
          <div class="bulkUpload" style="display: none;">
            <div class="text-center moda_footer">
              <button type="button" class="btn btn-danger btn-view no_border_field" data-dismiss="modal">
                Cancel
              </button>
              <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="uploadTeachers">
                Upload
              </button>
            </div>
          </div>
          <div class="manualAdd">
            <div class="text-center moda_footer">
              <button type="button" class="btn btn-danger btn-view no_border_field" data-dismiss="modal">
                Cancel
              </button>
              <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="addTeacherbtn">
                Save Changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  <!--end::Modal-->
  
  <!-- Modal for response -->

  <!-- Modal for success response -->
  <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <form name="fm-student">
          <div class="modal-body">
            <h5 id="ResponseHeading"></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" data-dismiss="modal" id="LoadTeacherDatatable">
              OK
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
@endsection
@section('page_script')
  <script>
    $('#uploadTeacher').on('click',function(e){
        e.preventDefault();
        // Hide manualUpload input box and display bulk upload
        $('.bulkUpload').css('display','block');
        $('.manualAdd').css('display','none');
    });
      $('#addTeacher').click(function(e){
        e.preventDefault();
        // Hide BulkUpload input box and display rest
        $('.bulkUpload').css('display','none');
        $('.manualAdd').css('display','block');
        // End of code

        $('#name').val('');
        $('#email').val('');
        $('#contact').val('');
        $('#country_code').val('');
        $('#state').val('');        
        $('#country').val('');
        $('#selected_tl').val('');
        $("#PasswordDiv").html('<label for="password" class="form-control-label">Password</label><input type="password" class="form-control border_bottom pl0" id="password" placeholder="Enter Password" name="password">');});
      var datatable;
      (function() {
        $('.loader_msg').css('display','none');
        datatable = $('.Teacher_datatable').mDatatable({
                // datasource definition
                data: {
                  type: 'remote',
                  source: {
                    read: {
                      url: APP_URL+'/teacher/show',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                            country_list: '',
                            ll_name: '',
                          }
                        },
                        map: function(raw) {

                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                           dataSet = raw.data;
                         }
                         console.log('Result');
                         console.log(dataSet);
                         return dataSet;
                       },
                     }
                   },
                   pageSize: 10,
                   saveState: {
                    cookie: false,
                    webstorage: false
                  },

                  serverPaging: true,
                  serverFiltering: false,
                  serverSorting: false
                },
          // layout definition
          layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: false,

            pagination: true,

           /* search: {
              input: $('#generalSearch')
            },*/

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
              field: "S_No",
              title: "S.No",
              width: 40
            },
            {
              field: "name",
              title: "Name",
              textAlign: 'center'
            },
            {
              field: "school_name",
              title: "School Name",
              textAlign: 'center'
            },
            {
              field: "email",
              title: "Email",
              textAlign: 'center'
            },
            {
              field: "contact",
              title: "Contact",
              textAlign: 'center'
            },
            {
              field: "status",
              title: "Status",
              textAlign: 'center',
              template: function(row) {
                if(row.status==1){
                  @if(Helper::CanIf('changeTeacherStatus'))
                    return '\
                   <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Deactivate" onclick=activate_deactivateTeacher('+row.id+')>\
                      Deactivate\
                      </a>\
                   ';
                  @endif
                }
                else
                {
                  @if(Helper::CanIf('changeTeacherStatus'))
                    return '\
                   <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Activate" onclick=activate_deactivateTeacher('+row.id+')>\
                     Activate\
                     </a>\
                   ';
                  @endif
                }

              }
            },
           {
              width: 110,
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              field: 'Actions',
              template: function(row) {
                let html = "";
                 @if(Helper::CanIf('editTeacher'))
                 html += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"><i class="la la-edit" onclick=getTeacherDetail('+row.id+')></i></a>';
                 @endif

                 @if(Helper::CanIf('deleteTeacher'))
                 html += '<a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteTeacher('+row.id+')><i class="la la-trash"></i></a>';
                 @endif

                 return html;
             },
           }]
         });



 $('#addTeacherbtn').on('click',function(){
  datatable.reload();
  });

$('#generalSearch').on('change',function(){
  var value = $(this).val();
  var country_id = $('#country_list').val();
  datatable.setDataSourceQuery({ll_name:value,country_id:country_id});
  datatable.reload();
});

$("#LoadTeacherDatatable").on('click',function(){
  datatable.reload();
});

})();


function getTeacherDetail(id){
  var path = APP_URL + "/teacher/edit";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
      console.log("here is the data");
            console.log(result);
            var res = $.parseJSON(result);
            var tl_id = "";
            var ll_id = "";
            var school_id = "";
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              $('#addTeacherModal').data('id',data.id);
              $('#addTeacherModal').find('.modal-title').html('Update Teacher ' + data.name);
              $("#PasswordDiv").html('');
              $('#name').val(data.name);
              $('#email').val(data.email);
              $('#contact').val(data.contact);
              $('#country_code').val(data.country_code);
              $('#country').val(data.country);
              $('#state').val(data.location);
              $('#selected_school').val(data.school_name);
              tl_id = data.tl_id;
              ll_id = data.ll_id;
              school_id = data.school_id;
              //loadTLData(data.country,data.tl_id);
              $('#addTeacherModal').modal('show');
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
}


function deleteTeacher(id){
  var path = APP_URL + "/teacher/destroy";
  var _this = $(this);
  swal({
    title: "Are you sure to delete this LL?",
    text: "You will not be able to recover this.",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-warning",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm) {
    if (isConfirm) {
      var data = id;
      $.ajax({
        method: 'POST',
        url: path,
        data: {
          id: data,
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          var res = $.parseJSON(data);
          if(res.status == 'error'){
            swal('Error',res.message,'error');
          }else{
           $('.sweet-overlay').remove();
           $('.showSweetAlert ').remove();
           $("#ResponseSuccessModal").modal('show');
           $("#ResponseSuccessModal #ResponseHeading").text(res.message);
         } 
       },
       error: function(data) {
        swal('Error',data,'error');
      }
    });
    } else {

    }
  });
}


$('#addTeacherbtn').click(function(e){
  e.preventDefault();

  var id = $('#addTeacherModal').data('id');
  var country  = $('#country').val();
  var tl_id  = $('#tl_user').val();
  var state  = $('#ll_user').val();
  var school_id  = $('#school_user').val();
  var name  = $('#name').val();
  var email  = $('#email').val();
  var country_code  = $('#country_code').val();
  var contact  = $('#contact').val();
  var password  = $('#password').val();
  
  if(country==''){
    swal("Error","Country is required","error");
    return false;
  }
  if(tl_id==''){
    swal("Error","Territory Licensee is required","error");
    return false;
  }
  if(state==''){
    swal("Error","State is required","error");
    return false;
  }
  if(school_id==''){
    swal("Error","School is required","error");
    return false;
  }
  if(name==''){
    swal("Error","Name is required","error");
    return false;
  }
  if(email==''){
    swal("Error","Email is required","error");
    return false;
  }
  if(country_code==''){
    swal("Error","Country code for contact number is required","error");
    return false;
  }
  if(contact==''){
    swal("Error","Contact number is required","error");
    return false;
  }

  if(password=='undefined'){
   var llpassword  = '';
 }else{

   if($('#password').val()==''){
    swal("","Password field is required","error");
    return false;
  }
  var llpassword  = $('#password').val();
}


$.ajax({
  method: 'POST',
  url: $("#StoreTeacher").attr('action'),
  data: {
    id: id,
    country: country,
    tl_id: tl_id,
    state: state,
    school_id: school_id,
    name:name,
    email:email,
    country_code:country_code,
    contact:contact,
    password:llpassword,
  },
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  success: function(data) {
    var res = $.parseJSON(data);
    if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
      $('#addTeacherModal').modal('hide');
      $("#ResponseSuccessModal").modal('show');
      $("#ResponseSuccessModal #ResponseHeading").text(res.message);
    } 
  },
  error: function(data) {
    swal('Error',data,'error');
  }
});
});

function activate_deactivateTeacher(id){
  var path = APP_URL + "/teacher/changeTeacherStatus";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
     var res = $.parseJSON(result);
     if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
     $('.sweet-overlay').remove();
     $('.showSweetAlert ').remove();
     $("#ResponseSuccessModal").modal('show');
     $("#ResponseSuccessModal #ResponseHeading").text(res.message);
   } 
 },
 error: function(){
  alert("Error");
}
}); 
}

$('#country').on('change',function(){
  var id = $(this).val();
  $.ajax({
    method: 'POST',
    url: APP_URL+'/getCountryCode',
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data) {
      var res = $.parseJSON(data);
      if(res.status == 'error'){
        /*swal('Error',res.message,'error');*/
        $('#country_code').val('');
      }else{
        $('#country_code').val(res.data.country_code);
      } 
    },
    error: function(data) {
      swal('Error',data,'error');
    }
  });
});



// Here is the upload Code

var uploadCsvFile = new FormData();
    $('#teacherCsvFile').change(function(){
        uploadCsvFile.append('file', this.files[0]); // since this is your file input
    });
    $('#uploadTeachers').click(function(){
        let schoolId = $('#school_user').val();
        if(schoolId == ""){
          swal("Error","School is required","error");
          return false;
        } else if(uploadCsvFile == ""){
          swal("Error","Browse CSV File","error");
          return false;
        }
        uploadCsvFile.append('schoolId',schoolId);
        $.ajax({
            url: APP_URL+'/teacher/uploadcsv',
            type: "post",
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            processData: false, // important
            contentType: false, // important
            data: uploadCsvFile,
            success: function(text) {
                if(text == "success") {
                    // alert("Your Device was uploaded successfully");
                }
            },
            error: function() {
                // alert("An error occured, please try again.");
            }
        });
    });
// End
</script>
@endsection

