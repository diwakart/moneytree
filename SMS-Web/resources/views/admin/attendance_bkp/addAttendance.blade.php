@extends('layouts.admin')
@section('page_title') 
Student Attendance
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Attendance & MT Earning
            </h3>
          </div> 
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="col-md-4">
                  <div class="m-portlet m-portlet--full-height  ">

                  <div class="m-portlet__body">
                    <h5>Assigned Teacher</h5>
                    <div class="m-card-profile display_flex"> 
                      <div class="m-card-profile__pic">
                        <div class="m-card-profile__pic-wrapper">
                          <img src="{{url('assets/app/media/img/users/user4.jpg')}}" alt="">
                        </div>
                      </div>
                      <div class="m-card-profile__details">
                        <span class="m-card-profile__name">
                          Mark Andre
                        </span>
                        <a href="" class="m-card-profile__email m-link font13">
                          mark.andre@gmail.com
                        </a>
                      </div>
                    </div>

                    
                    <div class="m-widget1 m-widget1--paddingless">
                      <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                          <div class="col">
                            <h3 class="m-widget1__title">
                              Lesson
                            </h3> 
                          </div>
                          <div class="col m--align-right">
                            <span class="m-widget1__number m--font-brand">
                              Lesson 1
                            </span>
                          </div>
                        </div>
                      </div>
                        <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                          <div class="col">
                            <h3 class="m-widget1__title">
                              Start to end Date
                            </h3> 
                          </div>
                          <div class="col m--align-right">
                            <span class="m-widget1__number m--font-brand">
                              31 July to 2 August
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="m-portlet__body-separator"></div>

                      <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                          <div class="col">
                            <h3 class="m-widget1__title">
                              School
                            </h3> 
                          </div>
                          <div class="col m--align-right">
                            <span class="m-widget1__number m--font-brand">
                              SMS School
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                          <div class="col">
                            <h3 class="m-widget1__title">
                              3th Class
                            </h3> 
                          </div>
                          <div class="col m--align-right">
                            <span class="m-widget1__number m--font-brand">
                              31 July to 2 August
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                          <div class="col">
                            <h3 class="m-widget1__title">
                              Total Students
                            </h3> 
                          </div>
                          <div class="col m--align-right">
                            <span class="m-widget1__number m--font-brand">
                              31
                            </span>
                          </div>
                        </div>
                      </div>
                        <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                          <div class="col">
                            <p><small><span class="text-danger">* </span> If you want your invoices addressed to a company. Leave blank to use your full name.</small>
                            </p>
                          </div> 
                        </div>
                      </div>
                      <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                          <div class="col text-center">
                            <button type="button" class="btn btn-accent background_gradient theme_shadow btn-view">Submit</button>
                          </div> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
                <div class="col-md-8">
                <!--begin: Datatable -->
                asdsdasd
                <!--end: Datatable -->
                </div>
    </div>

  <!--end::Modal-->
    
  <!-- Modal for response -->

  <!-- Modal for success response -->
  <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <form name="fm-student">
          <div class="modal-body">
            <h5 id="ResponseHeading"></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" data-dismiss="modal" id="LoadSADatatable">
              OK
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
@endsection
@section('page_script')

@endsection

