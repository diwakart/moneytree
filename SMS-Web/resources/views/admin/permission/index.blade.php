@extends('layouts.admin')
@section('page_title') 
User Permissions
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Permissions
            </h3>
          </div>           
        </div>
      </div>
      <!-- END: Subheader -->
        <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-4">
            <a href="{{ url('role') }}" style="text-decoration:none">
          <div class="m-portlet m-portlet--border-bottom-brand active background_gradient">
            <div class="m-portlet__body text-right">
              <div class="m-widget26 display_flex">
                <div class="m-widget26__icon">
                  <i class="flaticon-interface-9"></i>
                </div>
                <div class="m-widget26__number">
                  {{$roles}}          
                </div>          
              </div>
              <small class="count_name">User Roles</small>
            </div>
          </div>
          </a>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-4">
            <a href="{{ url('module') }}" style="text-decoration:none">
          <div class="m-portlet m-portlet--border-bottom-brand active background_gradient">
            <div class="m-portlet__body text-right">
              <div class="m-widget26 display_flex">
                <div class="m-widget26__icon">
                  <i class="flaticon-interface-9"></i>
                </div>
                <div class="m-widget26__number">
                  {{$modules}}          
                </div>          
              </div>
              <small class="count_name">Total Modules </small>
            </div>
          </div>
          </a>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-4">
            <a href="{{ url('route_permission') }}" style="text-decoration:none">
          <div class="m-portlet m-portlet--border-bottom-brand active background_gradient">
            <div class="m-portlet__body text-right">
              <div class="m-widget26 display_flex">
                <div class="m-widget26__icon">
                  <i class="flaticon-interface-9"></i>
                </div>
                <div class="m-widget26__number">
                  {{$routes}}          
                </div>          
              </div>
              <small class="count_name">Total Routes</small>
            </div>
          </div>
          </a>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-4">
            <a href="{{ url('module_assignment') }}" style="text-decoration:none">
          <div class="m-portlet m-portlet--border-bottom-brand active background_gradient">
            <div class="m-portlet__body text-right">
              <div class="m-widget26 display_flex">
                <div class="m-widget26__icon">
                  <i class="flaticon-interface-9"></i>
                </div>
                <div class="m-widget26__number">
                  {{$module_assigned}}          
                </div>          
              </div>
              <small class="count_name">Module assigned to roles</small>
            </div>
          </div>
          </a>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-4">
            <a href="{{ url('user_restriction_module') }}" style="text-decoration:none">
          <div class="m-portlet m-portlet--border-bottom-brand active background_gradient">
            <div class="m-portlet__body text-right">
              <div class="m-widget26 display_flex">
                <div class="m-widget26__icon">
                  <i class="flaticon-interface-9"></i>
                </div>
                <div class="m-widget26__number">
                  {{$restricted_routes}}          
                </div>          
              </div>
              <small class="count_name">Restricted modules for roles</small>
            </div>
          </div>
          </a>
        </div>
        </div>
  </div>
  
@endsection
@section('page_script')
  
@endsection

