@extends('layouts.admin')
@section('page_title') 
Location Licensee
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Role Panel
            </h3>
          </div>
          
          <!--begin: Search Form -->
          <div class="m-form m-form--label-align-right">
            <div class="align-items-center">
              <div class="display_flex">
                <div class="col-xl-8 col-8">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="">
                      <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search TL Name.." id="generalSearch">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span>
                            <i class="la la-search"></i>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-4 m--align-right">
                  <a href="javascript:void(0);" id="addLL" class="btn background_gradient btn-accent add_btn theme_shadow" data-toggle="modal" data-target="#addLocationLicenseeModal">
                    <span>
                      <i class="la la-plus"></i> 
                    </span>
                  </a> 
                </div>
              </div>
            </div>
          </div>
          <!--end: Search Form -->
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="location_licensee_datatable"></div>
        </div>
      </div>
      <div class="space50"></div>
    </div>

    <!--begin:: Add TL Modal-->
    <div class="modal fade" id="addLocationLicenseeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" id="search_form" style="display: none;">
            
          </div>
          <div class="modal-body">
            <h5 class="modal-title" id="exampleModalLabel">
              Add Permission
            </h5>
            <form id="StoreLocationLicensee" action="{{route('role.store')}}">
              <input type="hidden" class="form-control border_bottom pl0" id="permission_id" name="permission_id" value="">              
              
              <div class="form-group">
                <label for="name" class="form-control-label font13">
                  Role 
                </label>                
                <select class="form-control border_bottom pl0" id="role" name="role">
                  <option value="">Select Role</option>
                  <option value="1">Admin</option>
                  <option value="2">TL</option>
                  <option value="3">LL</option>
                  <option value="4">School</option>
                  <option value="5">Teacher</option>
                  <option value="6">Parent</option>
                </select>
              </div>                           
            </form>
          </div>
          <div class="text-center moda_footer">
            <button type="button" class="btn btn-danger btn-view no_border_field" data-dismiss="modal">
              Cancel
            </button>
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="addLLbtn">
              Save Changes
            </button>
          </div>
        </div>
      </div>
    </div>
  <!--end::Modal-->
    
  <!-- Modal for response -->

  <!-- Modal for success response -->
  <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <form name="fm-student">
          <div class="modal-body">
            <h5 id="ResponseHeading"></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" data-dismiss="modal" id="LoadLLDatatable">
              OK
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
@endsection
@section('page_script')
  <script>
    $('#add_ll_search').on('click',function(){
      $('#search_form').toggle(1000);
    });
    $('#tl_user').on('change',function(){
      if($(this).val()!='')
      {
        $('#selected_tl').val($('#tl_user option:selected').text());
        $('#search_form').hide(1000);
      }
    });
      $('#addLL').click(function(e){
        e.preventDefault();
        $('#name').val('');
        $('#email').val('');
        $('#contact').val('');
        $('#country_code').val('');
        $('#state').val('');
        $('#country').val('');
        $('#selected_tl').val('');
        $("#PasswordDiv").html('<label for="password" class="form-control-label">Password</label><input type="password" class="form-control border_bottom pl0" id="password" placeholder="Enter Password" name="password">');});
      var datatable;
      (function() {
        $('.loader_msg').css('display','none');
        datatable = $('.location_licensee_datatable').mDatatable({
                // datasource definition
                data: {
                  type: 'remote',
                  source: {
                    read: {
                      url: APP_URL+'/role/show',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                            country_list: '',
                            ll_name: '',
                          }
                        },
                        map: function(raw) {
                          console.log('raw');

                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                           dataSet = raw.data;
                         }
                         console.log('Result');
                         console.log(dataSet);
                         return dataSet;
                       },
                     }
                   },
                   pageSize: 10,
                   saveState: {
                    cookie: false,
                    webstorage: false
                  },

                  serverPaging: true,
                  serverFiltering: false,
                  serverSorting: false
                },
          // layout definition
          layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

           /* search: {
              input: $('#generalSearch')
            },*/

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
              field: "S_No",
              title: "S.No",
              width: 40
            },
            {
              field: "role",
              title: "Role",
              textAlign: 'center'
            },            
            {
              field: "status",
              title: "Status",
              textAlign: 'center',
              template: function(row) {
                if(row.status==1){
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Deactivate" onclick=activate_deactivateLL('+row.id+')>\
                    Deactivate\
                    </a>\
                 ';
                }
                else
                {
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Activate" onclick=activate_deactivateLL('+row.id+')>\
                   Activate\
                   </a>\
                 ';
                }

              }
            },
           {
              width: 110,
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              field: 'Actions',
              template: function(row) {
                 return '\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                   <i class="la la-edit" onclick=getLLDetail('+row.id+')></i>\
                   </a>\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteLL('+row.id+')>\
                   <i class="la la-trash"></i>\
                   </a>\
                   \
                 ';
             },
           }]
         });



$('#addLLbtn').on('click',function(){
  datatable.reload();
});

$('#generalSearch').on('change',function(){
  var value = $(this).val();
  var country_id = $('#country_list').val();
  datatable.setDataSourceQuery({ll_name:value,country_id:country_id});
  datatable.reload();
});

$("#LoadLLDatatable").on('click',function(){
  datatable.reload();
});

})();


function getLLDetail(id){
  var path = APP_URL + "/role/edit";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);
            var tl_id = "";
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              $('#permission_id').val(data.id);
              $('#addLocationLicenseeModal').find('.modal-title').html('Update module ');
              
              $('#role').val(data.role);
                                          
              $('#addLocationLicenseeModal').modal('show');
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
}


function deleteLL(id){
  var path = APP_URL + "/role/destroy";
  var _this = $(this);
  swal({
    title: "Are you sure to delete this Permission?",
    text: "You will not be able to recover this.",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-warning",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm) {
    if (isConfirm) {
      var data = id;
      $.ajax({
        method: 'POST',
        url: path,
        data: {
          id: data,
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          var res = $.parseJSON(data);
          if(res.status == 'error'){
            swal('Error',res.message,'error');
          }else{
           $('.sweet-overlay').remove();
           $('.showSweetAlert ').remove();
           $("#ResponseSuccessModal").modal('show');
           $("#ResponseSuccessModal #ResponseHeading").text(res.message);
         } 
       },
       error: function(data) {
        swal('Error',data,'error');
      }
    });
    } else {

    }
  });
}


$('#addLLbtn').click(function(e){
  e.preventDefault();

  var id = $('#permission_id').val();
  var role  = $('#role').val();  
  
  if(role==''){
    swal("Error","role name is required","error");
    return false;
  }   

$.ajax({
  method: 'POST',
  url: $("#StoreLocationLicensee").attr('action'),
  data: {
    id: id,
    role: role,    
  },
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  success: function(data) {
    var res = $.parseJSON(data);
    if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
      $('#addLocationLicenseeModal').modal('hide');
      $("#ResponseSuccessModal").modal('show');
      $("#ResponseSuccessModal #ResponseHeading").text(res.message);
    } 
  },
  error: function(data) {
    swal('Error',data,'error');
  }
});
});

function activate_deactivateLL(id){
  var path = APP_URL + "/location-licensee/create";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
     var res = $.parseJSON(result);
     if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
     $('.sweet-overlay').remove();
     $('.showSweetAlert ').remove();
     $("#ResponseSuccessModal").modal('show');
     $("#ResponseSuccessModal #ResponseHeading").text(res.message);
   } 
 },
 error: function(){
  alert("Error");
}
}); 
}

</script>
@endsection

