<button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn">
  <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-light ">
<!-- BEGIN: Aside Menu -->
  <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light" data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
      @if(Route::currentRouteName() === "home")
      <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
      @else
      <li class="m-menu__item  m-menu__item" aria-haspopup="true" >
      @endif
        <a  href="{{url('home')}}" class="m-menu__link " title="Dashboard">
          <i class="m-menu__link-icon flaticon-line-graph"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Dashboard
              </span> 
            </span>
          </span>
        </a>
      </li>
      @if(Auth::user()->user_type==1)
      @if(Route::currentRouteName() === "viewCountry")
      <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
      @else
      <li class="m-menu__item  m-menu__item" aria-haspopup="true" >
      @endif
        <a  href="{{url('countries')}}" class="m-menu__link " title="Countries">
          <i class="m-menu__link-icon fa fa-flag"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Countries
              </span> 
            </span>
          </span>
        </a>
      </li>
      @endif
      @if(Auth::user()->user_type==1)
      @if(Route::currentRouteName() === "viewTL")
      <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
      @else
      <li class="m-menu__item  m-menu__item" aria-haspopup="true" >
      @endif
        <a  href="{{url('territory-licensee')}}" class="m-menu__link " title="Territory Licensee">
          <i class="m-menu__link-icon fa fa-map-marker"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Territory Licensee
              </span> 
            </span>
          </span>
        </a>
      </li>
      @endif
      @if(Auth::user()->user_type==1 || Auth::user()->user_type==2)
      @if(Route::currentRouteName() === "viewLL")
      <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
      @else
      <li class="m-menu__item  m-menu__item" aria-haspopup="true" >
      @endif
        <a  href="{{url('location-licensee')}}" class="m-menu__link " title="Location Licensee">
          <i class="m-menu__link-icon fa fa-location-arrow"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Location Licensee
              </span> 
            </span>
          </span>
        </a>
      </li>
      @endif
      @if(Auth::user()->user_type==1 || Auth::user()->user_type==2 || Auth::user()->user_type==3)
      @if(Route::currentRouteName() === "viewSchool")
      <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
      @else
      <li class="m-menu__item  m-menu__item" aria-haspopup="true" >
      @endif
        <a  href="{{url('school')}}" class="m-menu__link " title="School">
          <i class="m-menu__link-icon fa fa-institution"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                School
              </span> 
            </span>
          </span>
        </a>
      </li>
      @endif
      @if(Auth::user()->user_type==1 || Auth::user()->user_type==2 || Auth::user()->user_type==3 || Auth::user()->user_type==4)
      @if(Route::currentRouteName() === "viewTeacher")
      <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
      @else
      <li class="m-menu__item  m-menu__item" aria-haspopup="true" >
      @endif
        <a  href="{{url('teacher')}}" class="m-menu__link " title="Teacher">
          <i class="m-menu__link-icon fa fa-user-circle"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Teacher
              </span> 
            </span>
          </span>
        </a>
      </li>
      @endif
      @if(Auth::user()->user_type==1 || Auth::user()->user_type==2 || Auth::user()->user_type==3 || Auth::user()->user_type==4 || Auth::user()->user_type==5)
      @if(Route::currentRouteName() === "viewParent")
      <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
      @else
      <li class="m-menu__item  m-menu__item" aria-haspopup="true" >
      @endif
        <a  href="{{url('parent')}}" class="m-menu__link " title="Parent">
          <i class="m-menu__link-icon fa fa-user-circle-o"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Parent
              </span> 
            </span>
          </span>
        </a>
      </li>
      @endif
      @if(Auth::user()->user_type==1 || Auth::user()->user_type==2 || Auth::user()->user_type==3 || Auth::user()->user_type==4 || Auth::user()->user_type==5 || Auth::user()->user_type==6)
      @if(Route::currentRouteName() === "viewStudent")
      <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
      @else
      <li class="m-menu__item  m-menu__item" aria-haspopup="true" >
      @endif
        <a  href="{{url('student')}}" class="m-menu__link " title="Student">
          <i class="m-menu__link-icon fa fa-users"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Student
              </span> 
            </span>
          </span>
        </a>
      </li>
      @endif
      @if(Auth::user()->user_type==1 || Auth::user()->user_type==2 || Auth::user()->user_type==3 || Auth::user()->user_type==4 || Auth::user()->user_type==5)
      @if(Route::currentRouteName() === "viewClass")
      <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
      @else
      <li class="m-menu__item  m-menu__item" aria-haspopup="true" >
      @endif
        <a  href="{{url('class')}}" class="m-menu__link " title="Class">
          <i class="m-menu__link-icon fa fa-graduation-cap"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Class
              </span> 
            </span>
          </span>
        </a>
      </li>
      @endif
      @if(Auth::user()->user_type==1 || Auth::user()->user_type==2 || Auth::user()->user_type==3 || Auth::user()->user_type==4 || Auth::user()->user_type==5)
      @if(Route::currentRouteName() === "view-attendance")
      <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
      @else
      <li class="m-menu__item  m-menu__item" aria-haspopup="true" >
      @endif
        <a  href="{{url('view-attendance')}}" class="m-menu__link " title="Student Attendance">
          <i class="m-menu__link-icon fa fa-th-list"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Student Attendance &amp; MT Earning 
              </span> 
            </span>
          </span>
        </a>
      </li>
      @endif
      <!--@if(Auth::user()->user_type==1 || Auth::user()->user_type==2 || Auth::user()->user_type==3 || Auth::user()->user_type==4 || Auth::user()->user_type==5)
      @if(Route::currentRouteName() === "report")
      <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
      @else
      <li class="m-menu__item  m-menu__item" aria-haspopup="true" >
      @endif
        <a  href="{{url('report')}}" class="m-menu__link " title="Student Report">
          <i class="m-menu__link-icon fa fa-signal"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Student Report
              </span> 
            </span>
          </span>
        </a>
      </li>
      @endif-->
      <!-- <li class="m-menu__item  m-menu__item" aria-haspopup="true" >
        <a  href="{{url('permission')}}" class="m-menu__link " title="Permissions">
          <i class="m-menu__link-icon fa fa-flag"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Permissions
              </span> 
            </span>
          </span>
        </a>
      </li> -->
      @if(Auth::user()->user_type==1 )
      @if(Route::currentRouteName() === "permission_module")
      <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
      @else
      <li class="m-menu__item  m-menu__item" aria-haspopup="true" >  
      @endif    
        <a  href="{{url('permission_module')}}" class="m-menu__link " title="Student Report">
          <i class="m-menu__link-icon fa fa-signal"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Permission
              </span> 
            </span>
          </span>
        </a>
      </li>
      @endif
    </ul>
  </div>
<!-- END: Aside Menu -->
</div>
<!-- END: Left Aside -->