<!-- BEGIN: Header -->
<header class="m-grid__item m-header" data-minimize-offset="200" data-minimize-mobile-offset="200" >
  <div class="m-container m-container--fluid m-container--full-height">
    <div class="m-stack m-stack--ver m-stack--desktop">
    <!-- BEGIN: Brand -->
    <div class="m-stack__item m-brand  m-brand--skin-light ">
      <div class="m-stack m-stack--ver m-stack--general">
        <div class="m-stack__item m-stack__item--middle m-brand__logo hide_large">
          <a href="index.html" class="m-brand__logo-wrapper">
            <img alt="" src="{{url('assets/Images/Money-Tree-Logo.jpg')}}" width="120" />
          </a> 
        </div>
        <div class="m-stack__item m-stack__item--middle m-brand__tools">
        <!-- BEGIN: Left Aside Minimize Toggle -->
          <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block 
          ">
            <span></span>
          </a>
          <!-- END -->
          <!-- BEGIN: Responsive Aside Left Menu Toggler -->
          <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
            <span></span>
          </a>
          <!-- END --> 
        </div>
      </div>
    </div>
    <!-- END: Brand --> 
    <!-- BEGIN: Topbar -->
    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
      <div class="m-stack__item m-topbar__nav-wrapper">
        <ul class="m-topbar__nav m-nav m-nav--inline"> 
          <li class="m-nav__item money_logo hide_small"><a href="#"><img alt="" src="{{url('assets/Images/Money-Tree-Logo.jpg')}}" width="100" /></a></li>

          <li class="m-nav__item hide_small"><h5 class="top_title">SCHOOL MANAGEMENT SYSTEM</h5></li>



          <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
            <a href="#" class="m-nav__link m-dropdown__toggle">
              <span class="m-topbar__userpic">
                @if(Auth::user()->picture!='')
                  <img src="{{url('storage/app/').'/'.Auth::user()->picture}}" class="m--img-rounded m--marginless m--img-centered" alt=""/>
                @else
                <img src="{{url('assets/app/media/img/users/user4.jpg')}}" class="m--img-rounded m--marginless m--img-centered" alt=""/>
                @endif
              </span>
              <span class="m-topbar__username">
                {{ Auth::user()->name }}
                <!-- <small>Admin</small>   -->
              </span> 
            </a>
            <div class="m-dropdown__wrapper">
              <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
              <div class="m-dropdown__inner">                    
                <div class="m-dropdown__body">
                  <div class="m-dropdown__content">
                  <ul class="m-nav m-nav--skin-light"> 
                    <li class="m-nav__item">
                      <a href="{{url('/profile')}}" class="m-nav__link">                        
                        <span class="m-nav__link-text">
                          My Profile
                        </span>
                        <i class="m-nav__link-icon flaticon-settings"></i>
                      </a>
                    </li>
                    <li class="m-nav__separator m-nav__separator--fit"></li>
                    <li class="m-nav__item">
                      <a href="{{ route('logout') }}" class="m-nav__link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">                        
                        <span class="m-nav__link-text">
                          Logout
                        </span>
                        <i class="m-nav__link-icon la la-power-off text-danger"></i>
                      </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          </li>
        </ul>
      </div>

      <!-- END: Topbar -->
    </div>
    </div>
  </div>
</header>
<!-- END: Header --> 