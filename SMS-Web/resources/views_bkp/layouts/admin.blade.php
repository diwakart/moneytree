<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
     @yield('page_title')
 </title>
 <meta name="description" content="Latest updates and statistic charts">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 <meta name="csrf-token" content="{{ csrf_token() }}">
 <!--begin::Web font -->
 <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
 <script>
  WebFont.load({
    google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
    active: function() {
        sessionStorage.fonts = true;
    }
});
</script>
<script>
 var APP_URL = {!! json_encode(url('/')) !!} 
</script>
<!--end::Web font -->
<!--begin::Base Styles -->  
<!--begin::Page Vendors -->
<link href="{{ url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors -->
<link href="{{ url('assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
<!-- <link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" /> -->
<link href="{{ url('assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/demo/default/base/Main.css')}}" rel="stylesheet" type="text/css" />
<!--end::Base Styles -->
<link rel="shortcut icon" href="{{ url('assets/demo/default/media/img/logo/favicon.ico')}}" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<style>

.select2-container {
  width: 100% !important;
}

.pac-container { 
  position: absolute!important;
  z-index: 9999;
  display: block !important;

}
.pac-container:after {
    
    display: none !important;
    height: 0px;
    width: 0px !important;
    content:none !important;
}
.pac-container { 
    border: none !important;
    box-shadow: none !important; 
}

</style>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyC3Ws7EvlZS2PRlNFZfFrqOnlWM_XHYO1o&libraries=places" type="text/javascript"></script>

@yield('page_css')

</head>
  <!-- end::Head -->
    <!-- end::Body -->
  <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default m-scroll-top--shown m-topbar--on"  >
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        @include('header.app_header')   
        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            <!-- BEGIN: Left Aside -->
            @include('header.app_sidebar')
                <div class="m-grid__item m-grid__item--fluid m-wrapper">
                @yield('content')
                </div>
        </div>
    <footer class="m-grid__item     m-footer ">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--last">
                    <span class="m-footer__copyright">
                        2018 &copy; Student Managment System by
                        <a href="#" class="m-link">
                            Moneytree
                        </a>
                    </span>
                </div> 
            </div>
        </div>
    </footer> 
    </div>     
    <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
      <i class="la la-arrow-up"></i>
    </div>
    <!-- end::Scroll Top -->      

      <!--begin::Base Scripts -->
    <script src="{{url('assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
    <!--end::Base Scripts -->   
        <!--begin::Page Vendors -->
    <script src="{{url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
    <!--end::Page Vendors -->  
        <!--begin::Page Snippets -->
        <!--Owl slider start-->
        <link rel="stylesheet" type="text/css" href="{{url('assets/demo/default/custom/components/owl-slider/owl.carousel.css')}}"> 
     <script src="{{url('assets/demo/default/custom/components/owl-slider/owl.carousel.js')}}" type="text/javascript"></script>
     <script src="{{url('assets/demo/default/custom/components/owl-slider/owl.carousel.js')}}" type="text/javascript"></script>
     <!--Owl slider End-->

    <!--end::Page Snippets -->
    <script src="assets/demo/default/base/Main.js" type="text/javascript"></script>
    @yield('page_script')
  </body>
  <!-- end::Body -->
</html>
