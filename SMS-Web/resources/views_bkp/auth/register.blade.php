<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Version: 5.0.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="{{ app()->getLocale() }}">
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />
        <title>
            {{ config('app.name', 'Student Management System | Register ') }}
        </title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!-- <script src="{{ asset('assets/js/app.js') }}" defer></script> -->
        <!--end::Web font -->
        <!--begin::Base Styles -->
        <link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/vendors/base/login.bundle.css') }}" rel="stylesheet" type="text/css" />
        <!--end::Base Styles -->
        <link rel="shortcut icon" href="{{ asset('assets/demo/default/media/img/logo/favicon.ico') }}" />

    </head>
    <!-- end::Head -->
    <!-- end::Body -->
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url({{asset('assets/app/media/img//bg/bg-2.jpg')}})">
                <div class="m-grid__item m-grid__item--fluid    m-login__wrapper">
                    <div class="m-login__container">
                        <div class="m-login__logo">
                            <a href="{{url('/')}}">
                                <img src="{{asset('assets/app/media/img/logos/logo-1.png')}}">
                                
                            </a>
                        </div>
                        <div class="m-login__signin">
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                    Sign In To Admin
                                </h3>
                            </div>
                            <form class="m-login__form m-form" method="POST" action="{{ route('register') }}">
                                 @csrf
                                <div class="form-group m-form__group">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} m-input" name="name" value="{{ old('name') }}" required autofocus placeholder="{{ __('Name') }}">
                                     @if ($errors->has('name'))
                                     <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                     </span>
                                     @endif
                                </div>

                                <div class="form-group m-form__group">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} m-input" name="email" value="{{ old('email') }}" placeholder="{{ __('E-Mail Address') }}" required>
                                      @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                      @endif
                                </div>

                                <div class="form-group m-form__group">
                                    <select id="country_code"  class="form-control{{ $errors->has('country_code') ? ' is-invalid' : '' }} m-input" name="country_code"  style="height: 100%;">
                                    <option value="">{{ __('Country Code') }}</option>
                                    @php($countries = array('+60'=>'Malaysia (+60)','+91'=>'India (+91)','+44'=>'United Kingdom (+44)','+65'=>'Singapore (+65)','+1'=>'United State (+1)'))
                                    @foreach($countries as $key => $value)

                                     
                                      <option value="{{$key}}" @if($key== '+60') {{ 'selected' }}@endif>{{$value}}</option>
                                      @endforeach
                                    </select>

                                    @if ($errors->has('country_code'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('country_code') }}</strong>
                                        </span>
                                    @endif
                                     
                                </div>

                                <div class="form-group m-form__group">
                                    <input id="contact" type="number" class="form-control{{ $errors->has('contact') ? ' is-invalid' : '' }} m-input" name="contact" value="{{ old('contact') }}" placeholder="{{ __('Contact Number') }}">
                                      @if ($errors->has('contact'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('contact') }}</strong>
                                        </span>
                                      @endif
                                </div>
                                
                                <div class="form-group m-form__group">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} m-input" name="password" required placeholder="{{ __('Password') }}" >
                                     @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                     @endif
                                </div>

                                <div class="form-group m-form__group">
                                    <input id="password-confirm" type="password" class="form-control m-input" name="password_confirmation" required placeholder="{{ __('Confirm Password') }}" >
                                </div>
                                
                                <div class="m-login__form-action">
                                    <button type="submit" id="" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn" >
                                        Sign In
                                    </button>
                                </div>
                            </form>
                        </div>
                        
                        
                        <div class="m-login__account">
                            <span class="m-login__account-msg">
                                Want to Login ?
                            </span>
                            &nbsp;&nbsp;
                            <a href="{{ route('login') }}" id="" class="m-link m-link--light m-login__account-link">
                                Sign In
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Page -->
        <!--begin::Base Scripts -->
        <script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
        <!-- <script src="{{ asset('assets/vendors/base/login.bundle.js') }}" type="text/javascript"></script> -->
        <!--end::Base Scripts -->   
        <!--begin::Page Snippets -->
        <script src="{{ asset('assets/snippets/pages/user/login.js') }}" type="text/javascript"></script>
        <!--end::Page Snippets -->
    </body>
    <!-- end::Body -->
</html>
