@extends('layouts.admin')
@section('page_title') 
Location Licensee
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Permissions1
            </h3>
          </div>           
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="location_licensee_datatable"></div>
        </div>
      </div>
      <div class="space50"></div>
    </div>
  
  <div class="row m-row--full-height">

    <div class="col-sm-12 col-md-12 col-lg-12">
      <div class="m-portlet m-portlet--border-bottom-brand ">
        <div class="m-portlet__body text-right">
          <div class="m-widget26 display_flex">
            <div class="m-widget26__icon">
              <a href="{{ url('role') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="">
                  <span>
                    <i class="fa fa-book"></i>
                    <span>
                      Role
                    </span>
                  </span>
                </a>
            </div>
            <div class="m-widget26__icon">
              <a href="{{ url('module') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="">
                <span>
                  <i class="fa fa-book"></i>
                  <span>
                    Module
                  </span>
                </span>
              </a>
            </div>
            <div class="m-widget26__icon">
              <a href="{{ url('route_permission') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="">
                <span>
                  <i class="fa fa-book"></i>
                  <span>
                    Routes
                  </span>
                </span>
              </a>
            </div>
            <div class="m-widget26__icon">
              <a href="{{ url('module_assignment') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="">
                <span>
                  <i class="fa fa-book"></i>
                  <span>
                    Add Module Assigment
                  </span>
                </span>
              </a>
            </div>
            <div class="m-widget26__icon">
              <a href="{{ url('user_restriction_module') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="">
                <span>
                  <i class="fa fa-book"></i>
                  <span>
                    Add User Restriction Module
                  </span>
                </span>
              </a>
            </div>
            <!-- <div class="m-widget26__number">
              <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                <a href="{{ url('role') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="">
                  <span>
                    <i class="fa fa-book"></i>
                    <span>
                      Role
                    </span>
                  </span>
                </a>
                <div class="m-separator m-separator--dashed d-xl-none"></div>
              </div>
            </div> -->          
          </div>
          <!-- <small class="count_name">Add Role</small> -->
        </div>
      </div>
    </div>

    <!-- <div class="col-sm-12 col-md-12 col-lg-6">
      <div class="m-portlet m-portlet--border-bottom-brand ">
        <div class="m-portlet__body text-right">
          <div class="m-widget26 display_flex">
            <div class="m-widget26__icon">
              <i class="flaticon-location"></i>
            </div>
            <div class="m-widget26__number">
              <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                <a href="{{ url('module') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="">
                  <span>
                    <i class="fa fa-book"></i>
                    <span>
                      Module
                    </span>
                  </span>
                </a>
                <div class="m-separator m-separator--dashed d-xl-none"></div>
              </div>
            </div>          
          </div>
          <small class="count_name">Module</small>
        </div>
      </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-6">
      <div class="m-portlet m-portlet--border-bottom-brand ">
        <div class="m-portlet__body text-right">
          <div class="m-widget26 display_flex">
            <div class="m-widget26__icon">
              <i class="flaticon-location"></i>
            </div>
            <div class="m-widget26__number">
              <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                <a href="{{ url('route_permission') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="">
                  <span>
                    <i class="fa fa-book"></i>
                    <span>
                      Routes
                    </span>
                  </span>
                </a>
                <div class="m-separator m-separator--dashed d-xl-none"></div>
              </div>
            </div>          
          </div>
          <small class="count_name">Routes</small>
        </div>
      </div>
    </div>
       
    <div class="col-sm-12 col-md-12 col-lg-6">
      <div class="m-portlet m-portlet--border-bottom-brand ">
        <div class="m-portlet__body text-right">
          <div class="m-widget26 display_flex">
            <div class="m-widget26__icon">
              <i class="flaticon-location"></i>
            </div>
            <div class="m-widget26__number">
              <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                <a href="{{ url('module_assignment') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="">
                  <span>
                    <i class="fa fa-book"></i>
                    <span>
                      Add Module Assigment
                    </span>
                  </span>
                </a>
                <div class="m-separator m-separator--dashed d-xl-none"></div>
              </div>
            </div>          
          </div>
          <small class="count_name">Add Module Assigment</small>
        </div>
      </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-6">
      <div class="m-portlet m-portlet--border-bottom-brand ">
        <div class="m-portlet__body text-right">
          <div class="m-widget26 display_flex">
            <div class="m-widget26__icon">
              <i class="flaticon-location"></i>
            </div>
            <div class="m-widget26__number">
              <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                <a href="{{ url('user_restriction_module') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"  id="">
                  <span>
                    <i class="fa fa-book"></i>
                    <span>
                      Add User Restriction Module
                    </span>
                  </span>
                </a>
                <div class="m-separator m-separator--dashed d-xl-none"></div>
              </div>
            </div>          
          </div>
          <small class="count_name">Add user_restriction_module</small>
        </div>
      </div>
    </div> -->

  </div>
  
@endsection
@section('page_script')
  
@endsection

