@extends('layouts.admin')
@section('page_title') 
Countries
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Countries
            </h3>
          </div> 
          <!--begin: Search Form -->
          <div class="m-form m-form--label-align-right">
            <div class="align-items-center">
              <div class="display_flex">
                <div class="col-xl-8 col-8">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="">
                      <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search country name" id="generalSearch">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span>
                            <i class="la la-search"></i>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-4 m--align-right">
                  <a href="javascript:void(0);" id="add_country" class="btn background_gradient btn-accent add_btn theme_shadow" data-toggle="modal" data-target="#addCountryModal">
                    <span>
                      <i class="la la-plus"></i> 
                    </span>
                  </a> 
                </div>
              </div>
            </div>
          </div>
          <!--end: Search Form -->
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="countries_datatable"></div>
        </div>
      </div>
      <div class="space50"></div>
    </div>
    <!--begin::Add Country Modal-->
    <div class="modal fade" id="addCountryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <h5 class="modal-title" id="exampleModalLabel">
              Add Country
            </h5>

            <form id="StoreCountry" action="{{route('countries.store')}}">
              <div class="form-group">
                <label for="country_name" class="form-control-label font13">
                  Country Name *
                </label>
                <input type="text" class="form-control border_bottom pl0" id="country_name" name="country_name" placeholder="Enter Country name">
              </div>
              <div class="form-group">
                <label for="short_code" class="form-control-label font13">
                  Short Code *
                </label>
                <input type="text" class="form-control border_bottom pl0" id="short_code" name="short_code" placeholder="Enter Short code for country max 2 char length" maxlength="2">
              </div>
              <div class="form-group">
                <label for="country_code" class="form-control-label font13">
                  Country Code *
                </label>
                <input type="text" class="form-control border_bottom pl0" id="country_code" name="country_code" placeholder="Enter country code for mobile number max 6 char length" maxlength="6">
              </div>
            </form>
          </div>
          <div class="text-center moda_footer">
            <button type="button" class="btn btn-danger btn-view no_border_field" data-dismiss="modal">
              Cancel
            </button>
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="addCountry">
              Save Changes
            </button>
          </div>
        </div>
      </div>
    </div>
  <!--end::Modal-->
  <!-- Modal for response -->

  <!-- Modal for success response -->
  <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <form name="fm-student">
          <div class="modal-body">
            <h5 id="ResponseHeading"></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" data-dismiss="modal" id="LoadCountryDatatable">
              OK
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
@endsection
@section('page_script')
  <script>
    var datatable;
    (function() {
      datatable = $('.countries_datatable').mDatatable({
        // datasource definition
        data: {
          type: 'remote',
          source: {
            read: {
              url: APP_URL+'/countries/getAllCountry',
              method: 'GET',
              // custom headers
              headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
              params: {
                  // custom query params
                  query: {
                   generalSearch: '',
                   country_name: '',
                 }
               },
               map: function(raw) {
                 
                var dataSet = raw;
                if (typeof raw.data !== 'undefined') {
                 dataSet = raw.data;
               }
               console.log('Result');
               console.log(dataSet);
               return dataSet;
             },
           }
         },
         pageSize: 10,
         saveState: {
          cookie: false,
          webstorage: false
        },

        serverPaging: true,
        serverFiltering: false,
        serverSorting: false
      },
  // layout definition
  layout: {
      theme: 'default', // datatable theme
      class: '', // custom wrapper class
      scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: false,

    pagination: true,

   /* search: {
      input: $('#generalSearch')
    },*/

    // inline and bactch editing(cooming soon)
    // editable: false,

    // columns definition
    columns: [{
      field: "S_No",
      title: "S.No",
      width: 30
    },
    {
      field: "name",
      title: "Name",
      textAlign: 'center'
    },
    {
      field: "code",
      title: "Short Code",
      textAlign: 'center'
    },
    {
      field: "country_code",
      title: "Country Code",
      textAlign: 'center'
    },
    {
      field: "default_country",
      title: "Status",
      textAlign: 'center',
      template: function(row) {
        if(row.default_country==0){
          return '\
          <a href="javascript:void(0);" class="btn btn-success background_gradient btn-view" title="Activate" onclick=activateCountry('+row.id+')>\
          Activate\
          </a>\
           ';
         }
         else
         {
            return '\
          <a href="javascript:void(0);" class="btn btn-success background_gradient btn-view" title="Deactivate" onclick=deactivateCountry('+row.id+')>\
           Deactivate\
          </a>\
          ';
         }
      }
    },
    {
      width: 110,
      title: 'Actions',
      sortable: false,
      overflow: 'visible',
      field: 'Actions',
      template: function(row) {
         return '\
         <a href="javascript:void(0);" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
         <i class="la la-edit" onclick=getCountryDetail('+row.id+')></i>\
         </a>\
         <a href="javascript:void(0);" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteCountry('+row.id+')>\
         <i class="la la-trash"></i>\
         </a>\
         ';
     },
   }]
  });
  $('#addCountry').on('click',function(){
    datatable.reload();
  });

  $("#LoadCountryDatatable").on('click',function(){
    datatable.reload();
    //$('body').css('overflow', 'auto');
  });


  $('#generalSearch').on('change',function(){
    var value = $(this).val();
    if(value!='')
    {
      datatable.setDataSourceQuery({country_name:value});
      datatable.reload();
    }
    else
    {
      datatable.setDataSourceQuery({country_name:''});
      datatable.reload();
    }

  });
  })();
  $('#add_country').click(function(e)
  {
    $('#country_name').val('');
    $('#short_code').val('');
    $('#country_code').val('');
  });
  $('#addCountry').click(function(e)
  {
    e.preventDefault();
    var id = $('#addCountryModal').data('id');
    var name  = $('#country_name').val();
    var code  = $('#short_code').val();
    var country_code  = $('#country_code').val();
    if(name=='')
    {
      swal("Error","Name is required","error");
      return false;
    }
    if(code=='')
    {
      swal("Error","Code is required","error");
      return false;
    }
    if(country_code=='')
    {
      swal("Error","Country code for contact number is required","error");
      return false;
    }
    $.ajax({
      method: 'POST',
      url: $("#StoreCountry").attr('action'),
      data: {
        id: id,
        name: name,
        code: code,
        country_code:country_code
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(data) {
        var res = $.parseJSON(data);
        if(res.status == 'error'){
          swal('Error',res.message,'error');
        }else{
          $('#addCountryModal').modal('hide');
          $("#ResponseSuccessModal").modal('show');
          $("#ResponseSuccessModal #ResponseHeading").text(res.message);
        } 
      },
      error: function(data) {
        swal('Error',data,'error');
      }
    });
  });
  $("#country_code").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 189]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
  function getCountryDetail(id){
  var path = APP_URL + "/countries/edit";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);
            //console.log(result);

            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              $('#addCountryModal').data('id',data.id);
              $('#addCountryModal').find('.modal-title').html('Update Country ' + data.name);
              $('#country_name').val(data.name);
              $('#short_code').val(data.code);
              $('#country_code').val(data.country_code); 

              $('#addCountryModal').modal('show');
            }
          },
          error: function(){
            swal("Error",'Something wrong please check',"error");
          }
        }); 
}

function deleteCountry(id){
  var path = APP_URL + "/countries/destroy";
  var _this = $(this);
  swal({
    title: "Are you sure to delete this country?",
    text: "You will not be able to recover this.",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-warning",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm) {
    if (isConfirm) {
      var data = id;
      $.ajax({
        method: 'POST',
        url: path,
        data: {
          id: data,
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          var res = $.parseJSON(data);
          if(res.status == 'error'){
            swal('Error',res.message,'error');
          }else{
           $('.sweet-overlay').remove();
           $('.showSweetAlert ').remove();
           //swal("Success",res.message,"success");
           $("#ResponseSuccessModal").modal('show');
           $("#ResponseSuccessModal #ResponseHeading").text(res.message);
         } 
       },
       error: function(data) {
        swal('Error',data,'error');
      }
    });
    } else {

    }
  });
}


function activateCountry(id){
  var path = APP_URL + "/countries/activate";
  var _this = $(this);
  swal({
    title: "Are you sure to activate this Country?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-info",
    confirmButtonText: "Yes, Activate it!",
    closeOnConfirm: false
  },
  function(isConfirm) {
    if (isConfirm) {
      var data = id;
      $.ajax({
        type: 'POST',
        url: path,
        data: {
          id: data,
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          var res = $.parseJSON(data);
          if(res.status == 'error'){
            swal('Error',res.message,'error');
          }else{
           $('.sweet-overlay').remove();
           $('.showSweetAlert ').remove();
           //swal("Success",res.message,"success");
           $("#ResponseSuccessModal").modal('show');
           $("#ResponseSuccessModal #ResponseHeading").text(res.message);
         } 
       },
       error: function(data) {
        swal('Error',data,'error');
      }
    });
    } else {

    }
  });
}


function deactivateCountry(id){
  var path = APP_URL + "/countries/deactivate";
  var _this = $(this);
  swal({
    title: "Are you sure to deactivate this Country?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-info",
    confirmButtonText: "Yes, Deactivate it!",
    closeOnConfirm: false
  },
  function(isConfirm) {
    if (isConfirm) {
      var data = id;
      $.ajax({
        type: 'POST',
        url: path,
        data: {
          id: data,
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          var res = $.parseJSON(data);
          if(res.status == 'error'){
            swal('Error',res.message,'error');
          }else{
           $('.sweet-overlay').remove();
           $('.showSweetAlert ').remove();
           //swal("Success",res.message,"success");
           $("#ResponseSuccessModal").modal('show');
           $("#ResponseSuccessModal #ResponseHeading").text(res.message);
         } 
       },
       error: function(data) {
        swal('Error',data,'error');
      }
    });
    } else {

    }
  });
}
</script>
@endsection

