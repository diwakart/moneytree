@extends('layouts.admin')
@section('page_title') 
Territory Licensee
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Territory Licensee
            </h3>
          </div> 
          <!--begin: Search Form -->
          <div class="m-form m-form--label-align-right">
            <div class="align-items-center">
              <div class="display_flex">
                <div class="col-xl-8 col-8">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="">
                      <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search TL Name.." id="generalSearch">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span>
                            <i class="la la-search"></i>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-4 m--align-right">
                  <a href="javascript:void(0);" id="addTL" class="btn background_gradient btn-accent add_btn theme_shadow" data-toggle="modal" data-target="#addTerritoryLicenseeModal">
                    <span>
                      <i class="la la-plus"></i> 
                    </span>
                  </a> 
                </div>
              </div>
            </div>
          </div>
          <!--end: Search Form -->
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="territory_licensee_datatable"></div>
        </div>
      </div>
      <div class="space50"></div>
    </div>

    <!--begin:: Add TL Modal-->
    <div class="modal fade" id="addTerritoryLicenseeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <h5 class="modal-title" id="exampleModalLabel">
              Add Territory Licensee
            </h5>

            <form id="StoreTerritoryLicensee" action="{{route('territory-licensee.store')}}">
              <div class="form-group">
                  <label for="recipient-name" class="form-control-label font13">
                  Country
                </label>
                  <select class="form-control m-input m-input--square no_border_field pl0" id="country" name="country">
                  <option value="">Please Select Country</option>
                  @inject('countries','App\Model\Countries')
                  @php($countries = $countries->where(['default_country'=>1])->get())
                  @foreach($countries as $name)
                  <option value="{{$name->id}}">{{$name->name}}</option>
                  @endforeach
                </select>
               </div>
               <div class="form-group">
                <label for="country_code" class="form-control-label font13">
                  Country Code *
                </label>
                <input type="text" class="form-control border_bottom pl0" id="country_code" name="country_code" placeholder="Enter country code for mobile number max 6 char length" maxlength="6">
              </div>
              <div class="form-group">
                <label for="name" class="form-control-label font13">
                  Name *
                </label>
                <input type="text" class="form-control border_bottom pl0" id="name" name="name" placeholder="Enter name of TL">
              </div>
              <div class="form-group">
                <label for="email" class="form-control-label font13">
                  Email *
                </label>
                <input type="email" class="form-control border_bottom pl0" id="email" name="email" placeholder="Enter email of TL">
              </div>
              <div class="form-group">
                <label for="contact" class="form-control-label font13">
                  Contact Number *
                </label>
                <input type="text" class="form-control border_bottom pl0" id="contact" name="contact" placeholder="Enter contact number">
              </div>
              <div class="form-group" id="PasswordDiv">

              </div>
            </form>
          </div>
          <div class="text-center moda_footer">
            <button type="button" class="btn btn-danger btn-view no_border_field" data-dismiss="modal">
              Cancel
            </button>
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="addTLbtn">
              Save Changes
            </button>
          </div>
        </div>
      </div>
    </div>
  <!--end::Modal-->
    
  <!-- Modal for response -->

  <!-- Modal for success response -->
  <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <form name="fm-student">
          <div class="modal-body">
            <h5 id="ResponseHeading"></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" data-dismiss="modal" id="LoadTLDatatable">
              OK
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
@endsection
@section('page_script')
  <script>
    var datatable;
              (function() {
                $('.loader_msg').css('display','none');
                datatable = $('.territory_licensee_datatable').mDatatable({
                // datasource definition
                data: {
                  type: 'remote',
                  source: {
                    read: {
                      url: APP_URL+'/territory-licensee/show',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                            generalSearch: '',
                            tlname: '',
                          }
                        },
                        map: function(raw) {

                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                           dataSet = raw.data;
                         }
                         console.log('Result');
                         console.log(dataSet);
                         return dataSet;
                       },
                     }
                   },
                   pageSize: 10,
                   saveState: {
                    cookie: false,
                    webstorage: false
                  },

                  serverPaging: true,
                  serverFiltering: false,
                  serverSorting: false
                },
          // layout definition
          layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

           /* search: {
              input: $('#generalSearch')
            },*/

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
              field: "S_No",
              title: "S.No",
              width: 30
            },
            {
              field: "name",
              title: "Name",
              textAlign: 'center'
            },
            {
              field: "country",
              title: "Country",
              textAlign: 'center'
            },
            {
              field: "email",
              title: "Email",
              textAlign: 'center'
            },
            {
              field: "contact",
              title: "Contact",
              textAlign: 'center'
            },
            {
              field: "status",
              title: "Status",
              textAlign: 'center',
              template: function(row) {
                if(row.status=='1')
                {
                  return '\
                 <span class="m-switch m-switch--success m-switch--sm" style="width: 127px;"><label>\
                  <input type="checkbox" class="checkbox" checked="checked" name="">\
                    <span></span>\
                  </label></span>\
                 ';
                }
                else
                {
                  return '\
                 <span class="m-switch m-switch--success m-switch--sm" style="width: 127px;"><label>\
                  <input type="checkbox" class="checkbox" name="">\
                    <span></span>\
                  </label></span>\
                 ';
                }
              }
            },
            {
              width: 110,
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              field: 'Actions',
              template: function(row) {
                 return '\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                    <i class="la la-edit" onclick=getTLDetail('+row.id+')></i>\
                    </a>\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteTL('+row.id+')>\
                  <i class="la la-trash"></i>\
                  </a>\
                 ';
             },
           }]
          });

$('#addTLbtn').on('click',function(){
  datatable.reload();
});



$('#generalSearch').on('change',function(){
  var value = $(this).val();
  if(value!=''){
    datatable.setDataSourceQuery({tlname:value});
    datatable.reload();
  }else{
    datatable.setDataSourceQuery({tlname:''});
    datatable.reload();
  }

});


$("#LoadTLDatatable").on('click',function(){
  datatable.reload();
  $('body').css('overflow', 'auto');
});

})();
$(".checkbox").change(function() {
  alert('asdasd');
    if(this.checked) {
        //Do stuff
    }
});
  function getTLDetail(id){
  var path = APP_URL + "/territory-licensee/edit";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){

      var res = $.parseJSON(result);
      console.log(res);

      if(res.status == 'error'){

      }else{
        var data = $.parseJSON(JSON.stringify(res.message));
              //console.log(data);
              
              $('#addTerritoryLicenseeModal').data('id',data.id);
              $('#addTerritoryLicenseeModal').find('.modal-title').html('Update Territory Licensee ' + data.name);
              $("#PasswordDiv").html('');
              $('#name').val(data.name);
              $('#email').val(data.email);
              $('#contact').val(data.contact);
              $('#country_code').val(data.country_code);
              $('#country').val(data.country);


              $('#addTerritoryLicenseeModal').modal('show');
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
}


function activate_deactivateTL(id){
  var path = APP_URL + "/territory-licensee/create";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
     var res = $.parseJSON(result);
     if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
     $('.sweet-overlay').remove();
     $('.showSweetAlert ').remove();
     $("#ResponseSuccessModal").modal('show');
     $("#ResponseSuccessModal #ResponseHeading").text(res.message);
   } 
 },
 error: function(){
  alert("Error");
}
}); 
}


function deleteTL(id){
  var path = APP_URL + "/territory-licensee/destroy";
  var _this = $(this);
  swal({
    title: "Are you sure to delete this TL?",
    text: "You will not be able to recover this.",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-warning",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm) {
    if (isConfirm) {
      var data = id;
      $.ajax({
        method: 'POST',
        url: path,
        data: {
          id: data,
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          var res = $.parseJSON(data);
          if(res.status == 'error'){
            swal('Error',res.message,'error');
          }else{
           $('.sweet-overlay').remove();
           $('.showSweetAlert ').remove();
           $("#ResponseSuccessModal").modal('show');
           $("#ResponseSuccessModal #ResponseHeading").text(res.message);
         } 
       },
       error: function(data) {
        swal('Error',data,'error');
      }
    });
    } else {

    }
  });
}

var countries={
  "1": {
    "name": "India"
  },
  "2": {
    "name": "Malaysia"
  },
  "3": {
    "name": "Singapor"
  }
}
$('#addTL').click(function(e)
{
  $('#country').val('');
  $('#name').val('');
  $('#email').val('');
  $('#country_code').val('');
  $('#contact').val('');
  $("#PasswordDiv").html('<label for="password" class="form-control-label">Password</label><input type="password" class="form-control border_bottom pl0" id="password" placeholder="Enter password" name="password">');
});
$("#country_code").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 189]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
$("#contact").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 189]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
$('#addTLbtn').click(function(e){
  e.preventDefault();

  var id = $('#addTerritoryLicenseeModal').data('id');
  var country  = $('#country').val();
  var name  = $('#name').val();
  var email  = $('#email').val();
  var country_code  = $('#country_code').val();
  var contact  = $('#contact').val();
  
  var password  = $('#password').val();

  if(country==''){
    swal("Error","Country is required","error");
    return false;
  }
  if(country_code==''){
    swal("Error","Country code for contact number is required","error");
    return false;
  }
  if(name==''){
    swal("Error","Name is required","error");
    return false;
  }
  if(email==''){
    swal("Error","Email is required","error");
    return false;
  }
  if(contact==''){
    swal("Error","Contact number is required","error");
    return false;
  }

  if(password=='undefined'){
   var tlpassword  = '';
 }else{

   if($('#password').val()==''){
    swal("","Password field is required","error");
    return false;
  }
  var tlpassword  = $('#password').val();
}


$.ajax({
  method: 'POST',
  url: $("#StoreTerritoryLicensee").attr('action'),
  data: {
    id: id,
    country: country,
    name:name,
    email:email,
    country_code:country_code,
    contact:contact,
    password:tlpassword,
  },
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  success: function(data) {
    var res = $.parseJSON(data);
    if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
      $('#addTerritoryLicenseeModal').modal('hide');
      $("#ResponseSuccessModal").modal('show');
      $("#ResponseSuccessModal #ResponseHeading").text(res.message);
    } 
  },
  error: function(data) {
    swal('Error',data,'error');
  }
});
});
$('#country').on('change',function(){
  var id = $(this).val();
  $.ajax({
    method: 'POST',
    url: APP_URL+'/getCountryCode',
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data) {
      var res = $.parseJSON(data);
      if(res.status == 'error'){
        /*swal('Error',res.message,'error');*/
        $('#country_code').val('');
      }else{
        $('#country_code').val(res.data.country_code);
      } 
    },
    error: function(data) {
      swal('Error',data,'error');
    }
  });
});
</script>
@endsection
