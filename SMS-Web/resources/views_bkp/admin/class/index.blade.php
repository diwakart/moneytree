@extends('layouts.admin')
@section('page_title') 
Class
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
             Classes
            </h3>
          </div> 
          <!--begin: Search Form -->
          <div class="m-form m-form--label-align-right">
            <div class="align-items-center">
              <div class="display_flex">
                <div class="col-xl-8 col-8">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="">
                      <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search class name" id="generalSearch">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span>
                            <i class="la la-search"></i>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-4 m--align-right">
                  <a href="javascript:void(0);" id="addClass" class="btn background_gradient btn-accent add_btn theme_shadow" data-toggle="modal" data-target="#addClassModal">
                    <span>
                      <i class="la la-plus"></i> 
                    </span>
                  </a> 
                </div>
              </div>
            </div>
          </div>
          <!--end: Search Form -->
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="class_datatable"></div>
        </div>
      </div>
      <div class="space50"></div>
    </div>
  <!--end::Modal-->
  <!--begin:: Add Class Modal-->
    <div class="modal fade" id="addClassModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" id="search_form" style="display: none;">
            <form>
              <div class="form-group display_flex">
                 <div class="mr-auto">
                    <label for="recipient-name" class="form-control-label font13">
                      Country
                    </label>
                    <select class="form-control m-input m-input--square no_border_field pl0" id="country" name="country">
                      <option value="">Please Select Country</option>
                      @inject('countries','App\Model\Countries')
                      @php($countries = $countries->where(['default_country'=>1])->get())
                      @foreach($countries as $name)
                      <option value="{{$name->id}}" short_name="{{$name->code}}">{{$name->name}}</option>
                      @endforeach
                  </select>
                 </div>
                 <div class="mr-auto">
                    <label for="tl_user" class="form-control-label font13">
                      Territory Licensee
                    </label>
                    <select class="form-control m-input m-input--square no_border_field pl0" id="tl_user" name="tl_user">
                      <option value="">Select Territory Licensee</option>
                    </select>
                 </div>
                 <div class="mr-auto">
                    <label for="ll_user" class="form-control-label font13">
                      Location Licensee
                    </label>
                    <select class="form-control m-input m-input--square no_border_field pl0" id="ll_user" name="ll_user">
                      <option value="">Select Location Licensee</option>
                    </select>
                 </div>
                 <div class="mr-auto">
                    <label for="ll_user" class="form-control-label font13">
                      School
                    </label>
                    <select class="form-control m-input m-input--square no_border_field pl0" id="school_user" name="school_user">
                      <option value="">Select Location Licensee</option>
                    </select>
                 </div>
                 <div class="mr-auto">
                    <label for="ll_user" class="form-control-label font13">
                      Teacher
                    </label>
                    <select class="form-control m-input m-input--square no_border_field pl0" id="teacher_user" name="teacher_user">
                      <option value="">Select Teacher</option>
                    </select>
                 </div>
              </div>
            </form>
          </div>
          <div class="modal-body">
            <h5 class="modal-title" id="exampleModalLabel">
              Add Class
            </h5>
            <form id="StoreClass" action="{{route('AddClass')}}">
              <div class="form-group">
                <label for="selected_tl" class="form-control-label font13">
                  TL Name *
                </label>
                <input type="text" class="form-control border_bottom pl0" id="selected_tl" name="selected_tl" placeholder="Seletcted TL after search" readonly>
                <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="add_ll_search">Search</button>
              </div>
              <div class="form-group">
                <label for="state" class="form-control-label font13">
                  Location
                </label>
                <input type="text" class="form-control border_bottom pl0" id="state" name="state" placeholder="Enter Location" readonly>
              </div>
              <div class="form-group">
                <label for="state" class="form-control-label font13">
                  School
                </label>
                <input type="text" class="form-control border_bottom pl0" id="selected_school" name="selected_school" placeholder="Enter School" readonly>
              </div>
              <div class="form-group">
                <label for="state" class="form-control-label font13">
                  Teacher
                </label>
                <input type="text" class="form-control border_bottom pl0" id="selected_teacher" name="selected_teacher" placeholder="Enter Teacher" readonly>
              </div>                                          
            </form>
          </div>
          <div class="text-center moda_footer">
            <button type="button" class="btn btn-danger btn-view no_border_field" data-dismiss="modal">
              Cancel
            </button>
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="addClassbtn">
              Save Changes
            </button>
          </div>
        </div>
      </div>
    </div>
  <!--end::Modal-->
  
  <!-- Modal for response -->

  <!-- Modal for success response -->
  <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <form name="fm-student">
          <div class="modal-body">
            <h5 id="ResponseHeading"></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" data-dismiss="modal" id="LoadClassDatatable">
              OK
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
@endsection
@section('page_script')
  <script>

    $('#add_ll_search').on('click',function(){
      $('#search_form').toggle(1000);
    });
    $('#tl_user').on('change',function(){
      if($(this).val()!='')
      {
        $('#selected_tl').val($('#tl_user option:selected').text());
        
      }
    });
    $('#ll_user').on('change',function(){
      if($(this).val()!='')
      {
        $('#state').val($('#ll_user option:selected').text());
        
      }
    });
    $('#school_user').on('change',function(){
      if($(this).val()!='')
      {
        $('#selected_school').val($('#school_user option:selected').text());
        //$('#search_form').hide(1000);
      }
    });
    $('#teacher_user').on('change',function(){
      if($(this).val()!='')
      {
        $('#selected_teacher').val($('#teacher_user option:selected').text());
        $('#search_form').hide(1000);
      }
    });
    var datatable;
    (function() {
      datatable = $('.class_datatable').mDatatable({
                // datasource definition
                data: {
                  type: 'remote',
                  source: {
                    read: {
                      url: APP_URL+'/class/show',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                            class_name: '',
                           
                          }
                        },
                        map: function(raw) {
                          console.log('ITS RUN');
                          console.log(raw);
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                           dataSet = raw.data;
                         }
                         console.log('Result');
                         console.log(dataSet);
                         return dataSet;
                       },
                     }
                   },
                   pageSize: 10,
                   saveState: {
                    cookie: false,
                    webstorage: false
                  },

                  serverPaging: true,
                  serverFiltering: false,
                  serverSorting: false
                },
          // layout definition
          layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: false,

            pagination: true,

           /* search: {
              input: $('#generalSearch')
            },*/

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
              field: "S_No",
              title: "S.No",
              width: 40
            },
            {
              field: "id",
              title: "class Id",
              textAlign: 'center'
            },            
            {
              field: "status",
              title: "Status",
              textAlign: 'center',
              template: function(row) {
                if(row.status==1){
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Deactivate" onclick=activate_deactivateClass('+row.id+')>\
                    Deactivate\
                    </a>\
                 ';
                }
                else
                {
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Activate" onclick=activate_deactivateClass('+row.id+')>\
                   Activate\
                   </a>\
                 ';
                }

              }
            },
           {
              width: 110,
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              field: 'Actions',
              template: function(row) {
                 return '\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                   <i class="la la-edit" onclick=getClassDetail('+row.id+')></i>\
                   </a>\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteClass('+row.id+')>\
                   <i class="la la-trash"></i>\
                   </a>\
                   \
                 ';
               }

             }]
          });

 $('#addClassbtn').on('click',function(){
  datatable.reload();
  });
  

  $("#LoadClassDatatable").on('click',function(){
    datatable.reload();
  });

  $('#generalSearch').on('change',function(){
    var value = $(this).val();
    if(value!='')
    {
      datatable.setDataSourceQuery({school_name:value});
      datatable.reload();
    }
    else
    {
      datatable.setDataSourceQuery({school_name:''});
      datatable.reload();
    }

  });
  })();

  $('#addClassbtn').click(function(e){
  e.preventDefault();

  var id = $('#addClassModal').data('id');
  var country  = $('#country').val();
  
  var school_id  = $('#school_user').val();
  var teacher_id  = $('#teacher_user').val(); 

  if(school_id==''){
    swal("Error","School is required","error");
    return false;
  }


$.ajax({
  method: 'POST',
  url: $("#StoreClass").attr('action'),
  data: {
    id: id,    
    school_id: school_id,
    teacher_id:teacher_id,    
  },
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  success: function(data) {
    var res = $.parseJSON(data);
    if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
      $('#addClassModal').modal('hide');
      $("#ResponseSuccessModal").modal('show');
      $("#ResponseSuccessModal #ResponseHeading").text(res.message);
    } 
  },
  error: function(data) {
    swal('Error',data,'error');
  }
});
});

$('#country').on('change',function(){
  var autocomplete;
  var geocoder;
  var input = document.getElementById('state');
  var country_name = $("#country option:selected").attr('short_name');
  console.log('here');
  console.log(input);
  var options = {
      types: ['(regions)'], // (cities)
      componentRestrictions: {country: country_name}
    };

    autocomplete = new google.maps.places.Autocomplete(input,options);
    $(".pac-container").css('display','none');
  var id = $(this).val();
  
  $.ajax
  ({
    url: APP_URL + "/class/getallTLs",
    type: 'POST',              
    data: {id:id},
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   success: function (data) {

    var res = $.parseJSON(data);
    if(res.length==0){
      $('#tl_user').find('option').remove().end();
      var tl_option = $('#tl_user');
      tl_option.append(
      $('<option></option>').val('').html('Select Territory Licensee')
      );
    }else{
     $('#tl_user').find('option').remove().end();
     var tl_option = $('#tl_user');
     tl_option.append(
      $('<option></option>').val('').html('Select Territory Licensee')
      );
     $.each(res, function(idx,values){
      var tl_name = values.name;
        tl_option.append($('<option></option>').val(values.id).html(tl_name));
    });
   } 
 },
 cache: false,
});
});

$('#tl_user').on('change',function(){
  var autocomplete;
  var geocoder;
  var input = document.getElementById('state');
  var country_name = $("#country option:selected").attr('short_name');
  console.log('here in ll');
  console.log(input);
  var options = {
      types: ['(regions)'], // (cities)
      componentRestrictions: {country: country_name}
    };

    autocomplete = new google.maps.places.Autocomplete(input,options);
    $(".pac-container").css('display','none');
  var id = $(this).val();
  console.log("tl id");
  console.log(id);  
$.ajax
  ({
    url: APP_URL + "/class/getallLLs",
    type: 'POST',              
    data: {id:id},
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   success: function (data) {

    var res = $.parseJSON(data);
    if(res.length==0){
      $('#ll_user').find('option').remove().end();
      var ll_option = $('#ll_user');
      ll_option.append(
      $('<option></option>').val('').html('Select Location Licensee')
      );
    }else{
     $('#ll_user').find('option').remove().end();
     var ll_option = $('#ll_user');
     ll_option.append(
      $('<option></option>').val('').html('Select Location Licensee')
      );
     $.each(res, function(idx,values){
      var ll_name = values.name;
        ll_option.append($('<option></option>').val(values.id).html(ll_name));
    });
   } 
 },
 cache: false,
});
});

$('#ll_user').on('change',function(){
  var autocomplete;
  var geocoder;
  var input = document.getElementById('state');
  var country_name = $("#country option:selected").attr('short_name');
  console.log('here in school');
  console.log(input);
  var options = {
      types: ['(regions)'], // (cities)
      componentRestrictions: {country: country_name}
    };

    autocomplete = new google.maps.places.Autocomplete(input,options);
    $(".pac-container").css('display','none');
  var id = $("#ll_user").val();
  console.log("ll id");
  console.log(id);
    
$.ajax
  ({
    url: APP_URL + "/class/getallschools",
    type: 'POST',              
    data: {id:id},
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   success: function (data) {

    var res = $.parseJSON(data);
    if(res.length==0){
      $('#school_user').find('option').remove().end();
      var school_option = $('#school_user');
      school_option.append(
      $('<option></option>').val('').html('Select School')
      );
    }else{
     $('#school_user').find('option').remove().end();
     var school_option = $('#school_user');
     school_option.append(
      $('<option></option>').val('').html('Select School  ')
      );
     $.each(res, function(idx,values){
      var school_name = values.name;
        school_option.append($('<option></option>').val(values.id).html(school_name));
    });
   } 
 },
 cache: false,
});
});

$('#school_user').on('change',function(){
  var autocomplete;
  var geocoder;
  var input = document.getElementById('state');
  var country_name = $("#country option:selected").attr('short_name');
  console.log('here in teacher');
  console.log(input);
  var options = {
      types: ['(regions)'], // (cities)
      componentRestrictions: {country: country_name}
    };

    autocomplete = new google.maps.places.Autocomplete(input,options);
    $(".pac-container").css('display','none');
  var id = $("#school_user").val();
  console.log("school id");
  console.log(id);
    
$.ajax
  ({
    url: APP_URL + "/class/getallteachers",
    type: 'POST',              
    data: {id:id},
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   success: function (data) {

    var res = $.parseJSON(data);
    if(res.length==0){
      $('#teacher_user').find('option').remove().end();
      var teacher_option = $('#teacher_user');
      teacher_option.append(
      $('<option></option>').val('').html('Select Teacher')
      );
    }else{
     $('#teacher_user').find('option').remove().end();
     var teacher_option = $('#teacher_user');
     teacher_option.append(
      $('<option></option>').val('').html('Select Teacher  ')
      );
     $.each(res, function(idx,values){
      var teacher_name = values.name;
        teacher_option.append($('<option></option>').val(values.id).html(teacher_name));
    });
   } 
 },
 cache: false,
});
});

function getClassDetail(id){
  var path = APP_URL + "/class/edit";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
      console.log("here is the data");
            console.log(result);
            //return false;
            var res = $.parseJSON(result);
            var tl_id = "";
            var ll_id = "";
            var school_id = "";
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              $('#addClassModal').data('id',data.id);
              $('#addClassModal').find('.modal-title').html('Update Class ' + data.name);
              $("#PasswordDiv").html('');
              $('#name').val(data.name);
              $('#email').val(data.email);
              $('#contact').val(data.contact);
              $('#country_code').val(data.country_code);
              $('#country').val(data.country);
              $('#state').val(data.location);
              tl_id = data.tl_id;
              ll_id = data.ll_id;
              school_id = data.school_id;
              $.ajax
              ({
                url: APP_URL + "/student/getallTLs",
                type: 'POST',              
                data: {id:data.country},
                headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               success: function (data) {

                var res = $.parseJSON(data);
                if(res.length==0){
                  $('#tl_user').find('option').remove().end();
                  var tl_option = $('#tl_user');
                  tl_option.append(
                  $('<option></option>').val('').html('Select Territory Licensee')
                  );
                }else{
                 $('#tl_user').find('option').remove().end();
                 var tl_option = $('#tl_user');
                 tl_option.append(
                  $('<option></option>').val('').html('Select Territory Licensee')
                  );
                 var tl_selected="";
                 $.each(res, function(idx,values){
                  var tl_name = values.name;
                  if(tl_id==values.id)
                  {
                    tl_selected = "selected";
                  }
                  else
                  {
                    tl_selected="";
                  }
                    tl_option.append($('<option '+tl_selected+'></option>').val(values.id).html(tl_name));
                });
               } 
               },
               cache: false,
              });
              $.ajax
              ({
                url: APP_URL + "/class/getallLLs",
                type: 'POST',              
                data: {id:data.tl_id},
                headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               success: function (data) {

                var res = $.parseJSON(data);
                if(res.length==0){
                  $('#ll_user').find('option').remove().end();
                  var ll_option = $('#ll_user');
                  ll_option.append(
                  $('<option></option>').val('').html('Select Location Licensee')
                  );
                }else{
                 $('#ll_user').find('option').remove().end();
                 var ll_option = $('#ll_user');
                 ll_option.append(
                  $('<option></option>').val('').html('Select Location Licensee')
                  );
                 var ll_selected="";
                 $.each(res, function(idx,values){
                  var ll_name = values.name;
                  if(ll_id==values.id)
                  {
                   ll_selected = "selected";
                  }
                  else
                  {
                    ll_selected="";
                  }
                    ll_option.append($('<option '+ll_selected+'></option>').val(values.id).html(ll_name));
                });
               } 
               },
               cache: false,
              });
              $.ajax
              ({
                url: APP_URL + "/class/getallschools",
                type: 'POST',              
                data: {id:data.ll_id},
                headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               success: function (data) {

                var res = $.parseJSON(data);
                if(res.length==0){
                  $('#school_user').find('option').remove().end();
                  var school_option = $('#school_user');
                  school_option.append(
                  $('<option></option>').val('').html('Select School')
                  );
                }else{
                 $('#school_user').find('option').remove().end();
                 var school_option = $('#school_user');
                 school_option.append(
                  $('<option></option>').val('').html('Select School')
                  );
                 var school_selected="";
                 $.each(res, function(idx,values){
                  var school_name = values.name;
                  if(school_id==values.id)
                  {
                   school_selected = "selected";
                  }
                  else
                  {
                    school_selected="";
                  }
                    school_option.append($('<option '+school_selected+'></option>').val(values.id).html(school_name));
                });
               } 
               },
               cache: false,
              });
              //loadTLData(data.country,data.tl_id);
              $('#addClassModal').modal('show');
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
}


function deleteClass(id){
  var path = APP_URL + "/class/destroy";
  var _this = $(this);
  swal({
    title: "Are you sure to delete this Class?",
    text: "You will not be able to recover this.",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-warning",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm) {
    if (isConfirm) {
      var data = id;
      $.ajax({
        method: 'POST',
        url: path,
        data: {
          id: data,
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          var res = $.parseJSON(data);
          if(res.status == 'error'){
            swal('Error',res.message,'error');
          }else{
           $('.sweet-overlay').remove();
           $('.showSweetAlert ').remove();
           $("#ResponseSuccessModal").modal('show');
           $("#ResponseSuccessModal #ResponseHeading").text(res.message);
         } 
       },
       error: function(data) {
        swal('Error',data,'error');
      }
    });
    } else {

    }
  });
}
</script>
@endsection