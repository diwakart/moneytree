@extends('layouts.admin')
@section('page_title') 
Location Licensee
@endsection 

@section('page_css')
<style>
.text_decoration{
  text-decoration:none!important;
}
</style>
@endsection
@section('content')
  <!--main content-->
    <div class="m-content"> 
      <!-- BEGIN: Subheader -->
      <div class="m-subheader ">
        <div class="d-flex align-items-center">
          <div class="mr-auto">
            <h3 class="m-subheader__title ">
              Location Licensee
            </h3>
          </div> 
          <!--begin: Search Form -->
          <div class="m-form m-form--label-align-right">
            <div class="align-items-center">
              <div class="display_flex">
                <div class="col-xl-8 col-8">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="">
                      <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search TL Name.." id="generalSearch">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span>
                            <i class="la la-search"></i>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-4 m--align-right">
                  <a href="javascript:void(0);" id="addLL" class="btn background_gradient btn-accent add_btn theme_shadow" data-toggle="modal" data-target="#addLocationLicenseeModal">
                    <span>
                      <i class="la la-plus"></i> 
                    </span>
                  </a> 
                </div>
              </div>
            </div>
          </div>
          <!--end: Search Form -->
        </div>
      </div>
      <!-- END: Subheader -->
      <div class="row m-row--full-height">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="location_licensee_datatable"></div>
        </div>
      </div>
      <div class="space50"></div>
    </div>

    <!--begin:: Add TL Modal-->
    <div class="modal fade" id="addLocationLicenseeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" id="search_form" style="display: none;">
            <form>
              <div class="form-group display_flex">
                 <div class="mr-auto">
                    <label for="recipient-name" class="form-control-label font13">
                      Country
                    </label>
                    <select class="form-control m-input m-input--square no_border_field pl0" id="country" name="country">
                      <option value="">Please Select Country</option>
                      @inject('countries','App\Model\Countries')
                      @php($countries = $countries->where(['default_country'=>1])->get())
                      @foreach($countries as $name)
                      <option value="{{$name->id}}" short_name="{{$name->code}}">{{$name->name}}</option>
                      @endforeach
                  </select>
                 </div>
                 <div class="mr-auto">
                    <label for="tl_user" class="form-control-label font13">
                      Territory Licensee
                    </label>
                    <select class="form-control m-input m-input--square no_border_field pl0" id="tl_user" name="tl_user">
                      <option value="">Select Territory Licensee</option>
                    </select>
                 </div>
              </div>
            </form>
          </div>
          <div class="modal-body">
            <h5 class="modal-title" id="exampleModalLabel">
              Add Location Licensee
            </h5>
            <form id="StoreLocationLicensee" action="{{route('location-licensee.store')}}">
              <div class="form-group">
                <label for="selected_tl" class="form-control-label font13">
                  TL Name *
                </label>
                <input type="text" class="form-control border_bottom pl0" id="selected_tl" name="selected_tl" placeholder="Seletcted TL after search" readonly>
                <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="add_ll_search">Search</button>
              </div>
              <div class="form-group">
                <label for="state" class="form-control-label font13">
                  Location
                </label>
                <input type="text" class="form-control border_bottom pl0" id="state" name="state" placeholder="Enter Location">
              </div>
              <div class="form-group">
                <label for="name" class="form-control-label font13">
                  Name *
                </label>
                <input type="text" class="form-control border_bottom pl0" id="name" name="name" placeholder="Enter name of LL">
              </div>
              <div class="form-group display_flex">
                <div class="mr-auto">
                  <label for="country_code" class="form-control-label font13">
                    Country Code *
                  </label>
                  <input type="text" class="form-control border_bottom pl0" id="country_code" name="country_code" placeholder="Country code for mobile" maxlength="6">
                </div>
                <div class="mr-auto">
                  <label for="contact" class="form-control-label font13">
                  Contact Number *
                  </label>
                  <input type="text" class="form-control border_bottom pl0" id="contact" name="contact" placeholder="Enter contact number">
                </div>
              </div>
              <div class="form-group display_flex">
                <div class="mr-auto">
                  <label for="email" class="form-control-label font13">
                  Email *
                  </label>
                  <input type="email" class="form-control border_bottom pl0" id="email" name="email" placeholder="Enter email of LL">
                </div>
                <div class="mr-auto" id="PasswordDiv">
                </div>
              </div>
              
            </form>
          </div>
          <div class="text-center moda_footer">
            <button type="button" class="btn btn-danger btn-view no_border_field" data-dismiss="modal">
              Cancel
            </button>
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" id="addLLbtn">
              Save Changes
            </button>
          </div>
        </div>
      </div>
    </div>
  <!--end::Modal-->
    
  <!-- Modal for response -->

  <!-- Modal for success response -->
  <div class="modal fade" id="ResponseSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        
        <form name="fm-student">
          <div class="modal-body">
            <h5 id="ResponseHeading"></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-accent  background_gradient btn-view no_border_field" data-dismiss="modal" id="LoadLLDatatable">
              OK
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
@endsection
@section('page_script')
  <script>
    $('#add_ll_search').on('click',function(){
      $('#search_form').toggle(1000);
    });
    $('#tl_user').on('change',function(){
      if($(this).val()!='')
      {
        $('#selected_tl').val($('#tl_user option:selected').text());
        $('#search_form').hide(1000);
      }
    });
      $('#addLL').click(function(e){
        e.preventDefault();
        $('#name').val('');
        $('#email').val('');
        $('#contact').val('');
        $('#country_code').val('');
        $('#state').val('');
        $('#country').val('');
        $('#selected_tl').val('');
        $("#PasswordDiv").html('<label for="password" class="form-control-label">Password</label><input type="password" class="form-control border_bottom pl0" id="password" placeholder="Enter Password" name="password">');});
      var datatable;
      (function() {
        $('.loader_msg').css('display','none');
        datatable = $('.location_licensee_datatable').mDatatable({
                // datasource definition
                data: {
                  type: 'remote',
                  source: {
                    read: {
                      url: APP_URL+'/location-licensee/show',
                      method: 'GET',
                      // custom headers
                      headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
                      params: {
                          // custom query params
                          query: {
                            country_list: '',
                            ll_name: '',
                          }
                        },
                        map: function(raw) {

                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                           dataSet = raw.data;
                         }
                         console.log('Result');
                         console.log(dataSet);
                         return dataSet;
                       },
                     }
                   },
                   pageSize: 10,
                   saveState: {
                    cookie: false,
                    webstorage: false
                  },

                  serverPaging: true,
                  serverFiltering: false,
                  serverSorting: false
                },
          // layout definition
          layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

           /* search: {
              input: $('#generalSearch')
            },*/

            // inline and bactch editing(cooming soon)
            // editable: false,

            // columns definition
            columns: [{
              field: "S_No",
              title: "S.No",
              width: 40
            },
            {
              field: "name",
              title: "Name",
              textAlign: 'center'
            },
            {
              field: "country",
              title: "Country",
              textAlign: 'center'
            },
            {
              field: "location",
              title: "location",
              textAlign: 'center'
            },
            {
              field: "email",
              title: "Email",
              textAlign: 'center'
            },
            {
              field: "contact",
              title: "Contact",
              textAlign: 'center'
            },
            {
              field: "status",
              title: "Status",
              textAlign: 'center',
              template: function(row) {
                if(row.status==1){
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Deactivate" onclick=activate_deactivateLL('+row.id+')>\
                    Deactivate\
                    </a>\
                 ';
                }
                else
                {
                  return '\
                 <a href="javascript:;" class="btn btn-success background_gradient btn-view" title="Activate" onclick=activate_deactivateLL('+row.id+')>\
                   Activate\
                   </a>\
                 ';
                }

              }
            },
           {
              width: 110,
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              field: 'Actions',
              template: function(row) {
                 return '\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                   <i class="la la-edit" onclick=getLLDetail('+row.id+')></i>\
                   </a>\
                 <a href="javascript:;" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"  onclick=deleteLL('+row.id+')>\
                   <i class="la la-trash"></i>\
                   </a>\
                   \
                 ';
             },
           }]
         });



$('#addLLbtn').on('click',function(){
  datatable.reload();
});

$('#generalSearch').on('change',function(){
  var value = $(this).val();
  var country_id = $('#country_list').val();
  datatable.setDataSourceQuery({ll_name:value,country_id:country_id});
  datatable.reload();
});

$("#LoadLLDatatable").on('click',function(){
  datatable.reload();
});

})();


function getLLDetail(id){
  var path = APP_URL + "/location-licensee/edit";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
            //console.log(result);
            var res = $.parseJSON(result);
            var tl_id = "";
            if(res.status == 'error'){

            }else{
              var data = $.parseJSON(JSON.stringify(res.message));
              $('#addLocationLicenseeModal').data('id',data.id);
              $('#addLocationLicenseeModal').find('.modal-title').html('Update Location Licensee ' + data.name);
              $("#PasswordDiv").html('');
              $('#name').val(data.name);
              $('#email').val(data.email);
              $('#contact').val(data.contact);
              $('#country_code').val(data.country_code);
              $('#country').val(data.country);
              $('#state').val(data.location);
              tl_id = data.tl_id;
              $.ajax
              ({
                url: APP_URL + "/location-licensee/getallTLs",
                type: 'POST',              
                data: {id:data.country},
                headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               success: function (data) {

                var res = $.parseJSON(data);
                if(res.length==0){
                  $('#tl_user').find('option').remove().end();
                  var tl_option = $('#tl_user');
                  tl_option.append(
                  $('<option></option>').val('').html('Select Territory Licensee')
                  );
                }else{
                 $('#tl_user').find('option').remove().end();
                 var tl_option = $('#tl_user');
                 tl_option.append(
                  $('<option></option>').val('').html('Select Territory Licensee')
                  );
                 var tl_selected="";
                 $.each(res, function(idx,values){
                  var tl_name = values.name;
                  if(tl_id==values.id)
                  {
                    tl_selected = "selected";
                  }
                  else
                  {
                    tl_selected="";
                  }
                    tl_option.append($('<option '+tl_selected+'></option>').val(values.id).html(tl_name));
                });
               } 
               },
               cache: false,
              });
              //loadTLData(data.country,data.tl_id);
              $('#addLocationLicenseeModal').modal('show');
            }
          },
          error: function(){
            alert("Error");
          }
        }); 
}


function deleteLL(id){
  var path = APP_URL + "/location-licensee/destroy";
  var _this = $(this);
  swal({
    title: "Are you sure to delete this LL?",
    text: "You will not be able to recover this.",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-warning",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(isConfirm) {
    if (isConfirm) {
      var data = id;
      $.ajax({
        method: 'POST',
        url: path,
        data: {
          id: data,
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          var res = $.parseJSON(data);
          if(res.status == 'error'){
            swal('Error',res.message,'error');
          }else{
           $('.sweet-overlay').remove();
           $('.showSweetAlert ').remove();
           $("#ResponseSuccessModal").modal('show');
           $("#ResponseSuccessModal #ResponseHeading").text(res.message);
         } 
       },
       error: function(data) {
        swal('Error',data,'error');
      }
    });
    } else {

    }
  });
}


$('#addLLbtn').click(function(e){
  e.preventDefault();

  var id = $('#addLocationLicenseeModal').data('id');
  var country  = $('#country').val();
  var tl_id  = $('#tl_user').val();
  var state  = $('#state').val();
  var name  = $('#name').val();
  var email  = $('#email').val();
  var country_code  = $('#country_code').val();
  var contact  = $('#contact').val();
  var password  = $('#password').val();
  
  if(country==''){
    swal("Error","Country is required","error");
    return false;
  }
  if(tl_id==''){
    swal("Error","Territory Licensee is required","error");
    return false;
  }
  if(state==''){
    swal("Error","State is required","error");
    return false;
  }
  if(name==''){
    swal("Error","Name is required","error");
    return false;
  }
  if(email==''){
    swal("Error","Email is required","error");
    return false;
  }
  if(country_code==''){
    swal("Error","Country code for contact number is required","error");
    return false;
  }
  if(contact==''){
    swal("Error","Contact number is required","error");
    return false;
  }

  if(password=='undefined'){
   var llpassword  = '';
 }else{

   if($('#password').val()==''){
    swal("","Password field is required","error");
    return false;
  }
  var llpassword  = $('#password').val();
}


$.ajax({
  method: 'POST',
  url: $("#StoreLocationLicensee").attr('action'),
  data: {
    id: id,
    country: country,
    tl_id: tl_id,
    state: state,
    name:name,
    email:email,
    country_code:country_code,
    contact:contact,
    password:llpassword,
  },
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  success: function(data) {
    var res = $.parseJSON(data);
    if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
      $('#addLocationLicenseeModal').modal('hide');
      $("#ResponseSuccessModal").modal('show');
      $("#ResponseSuccessModal #ResponseHeading").text(res.message);
    } 
  },
  error: function(data) {
    swal('Error',data,'error');
  }
});
});

function activate_deactivateLL(id){
  var path = APP_URL + "/location-licensee/create";
  $.ajax({
    method: "GET",
    url: path,
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(result){
     var res = $.parseJSON(result);
     if(res.status == 'error'){
      swal('Error',res.message,'error');
    }else{
     $('.sweet-overlay').remove();
     $('.showSweetAlert ').remove();
     $("#ResponseSuccessModal").modal('show');
     $("#ResponseSuccessModal #ResponseHeading").text(res.message);
   } 
 },
 error: function(){
  alert("Error");
}
}); 
}

$('#country').on('change',function(){
  var autocomplete;
  var geocoder;
  var input = document.getElementById('state');
  var country_name = $("#country option:selected").attr('short_name');
  console.log('here');
  console.log(input);
  var options = {
      types: ['(regions)'], // (cities)
      componentRestrictions: {country: country_name}
    };

    autocomplete = new google.maps.places.Autocomplete(input,options);
    $(".pac-container").css('display','none');
  var id = $(this).val();
  $.ajax({
    method: 'POST',
    url: APP_URL+'/getCountryCode',
    data: {
      id: id
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data) {
      var res = $.parseJSON(data);
      if(res.status == 'error'){
        /*swal('Error',res.message,'error');*/
        $('#country_code').val('');
      }else{
        $('#country_code').val(res.data.country_code);
      } 
    },
    error: function(data) {
      swal('Error',data,'error');
    }
  });
  $.ajax
  ({
    url: APP_URL + "/location-licensee/getallTLs",
    type: 'POST',              
    data: {id:id},
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   success: function (data) {

    var res = $.parseJSON(data);
    if(res.length==0){
      $('#tl_user').find('option').remove().end();
      var tl_option = $('#tl_user');
      tl_option.append(
      $('<option></option>').val('').html('Select Territory Licensee')
      );
    }else{
     $('#tl_user').find('option').remove().end();
     var tl_option = $('#tl_user');
     tl_option.append(
      $('<option></option>').val('').html('Select Territory Licensee')
      );
     $.each(res, function(idx,values){
      var tl_name = values.name;
        tl_option.append($('<option></option>').val(values.id).html(tl_name));
    });
   } 
 },
 cache: false,
});
});


</script>
@endsection

