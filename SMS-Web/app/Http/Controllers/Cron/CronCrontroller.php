<?php

namespace App\Http\Controllers\Cron;

use Illuminate\Http\Request;
use App\Model\FileUpload;
use Storage;
use DB;
use Auth;
use Excel;
use App\User;
use App\Model\Teacher;
use App\Model\Student;
use App\Model\Classes;
use Log;

class CronCrontroller extends Controller
{
	public function index()
	{
		Log::info('hi Cron is running');
		if(!FileUpload::count()){
			return true;
		}
		$data = FileUpload::select()->first()->toArray();
		if($data){
			if($data['file_type']=="student"){
				$this->RunStudent($data['settings'],$data['start_from'],$data['end_from'],$data['id'],$data['file_name'],10);
			} elseif($data['file_type']=="teacher"){
				$this->RunTeacher($data['settings'],$data['start_from'],$data['end_from'],$data['id'],$data['file_name'],10);
			} elseif($data['file_type']=="class"){
				$this->RunClass($data['settings'],$data['start_from'],$data['end_from'],$data['id'],$data['file_name'],10);
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	public function RunStudent($settings,$start,$end,$id,$fileName,$incremment) {
		$Arrsettings = json_decode($settings);
		$school = $Arrsettings->schoolId;
		$flag = false;
		$path = Storage::url('app/csv/').$fileName;
		$arr = Excel::load($path, function($reader) {
		})->get()->toArray();
		$output = array_slice($arr,$start,$end);
		if(!empty($output) && count($output)){
			$flag = true;
			foreach($output as $key=>$value){
				try { 
					$userId = User::insertGetId(['name'=>$value['name'],
						'email'=>$value['email'],
						'country_code'=>$value['country_code'],
						'contact'=>$value['contact'],
						'password'=>bcrypt($value['password']),
						'user_type'=>5
					]);
					Student::insert(['user_id'=>$userId,'tl_id'=>$TL,'ll_id'=>$LL,'school_id'=>$school,'country_id'=>$country,'class_id'=>$value['class_id']]);
				}catch(\Illuminate\Database\QueryException $ex){ 
					//dd($ex->getMessage());
					continue;
				}
			}
		}
		if($flag){
			FileUpload::where('id',$id)->update(['file_type'=>'student','file_name'=>$fileName,'start_from'=>$start+$incremment,'end_from'=>$end+$incremment,'settings'=>$settings]);
		} else {
			FileUpload::where('id',$id)->delete();
		}
	}

	public function RunTeacher($settings,$start,$end,$id,$fileName,$incremment) {
		$Arrsettings = json_decode($settings);
		$TL = $Arrsettings->TL;
		$LL = $Arrsettings->LL;
		$school = $Arrsettings->school;
		$country = $Arrsettings->country;
		$flag = false;
		$path = Storage::url('app/csv/').$fileName;
		$arr = Excel::load($path, function($reader) {
		})->get()->toArray();
		$output = array_slice($arr,$start,$end);
		if(!empty($output) && count($output)){
			$flag = true;
			foreach($output as $key=>$value){
				try{
					$userId = User::insertGetId(['name'=>$value['name'],
						'email'=>$value['email'],
						'country_code'=>$value['country_code'],
						'contact'=>$value['contact'],
						'password'=>bcrypt($value['password']),
						'user_type'=>5
					]);
					Teacher::insert(['user_id'=>$userId,'tl_id'=>$TL,'ll_id'=>$LL,'school_id'=>$school,'country_id'=>$country]);
				}catch(\Illuminate\Database\QueryException $ex){ 
					//dd($ex->getMessage());
					continue;
				}
			}
		}
		if($flag){
			FileUpload::where('id',$id)->update(['file_type'=>'teacher','file_name'=>$fileName,'start_from'=>$start+$incremment,'end_from'=>$end+$incremment,'settings'=>$settings]);
		} else {
			FileUpload::where('id',$id)->delete();
		}
	}

	public function RunClass($settings,$start,$end,$id,$fileName,$incremment) {
		$Arrsettings = json_decode($settings);
		$school = $Arrsettings->school_id;
		$flag = false;
		$path = Storage::url('app/csv/').$fileName;
		$arr = Excel::load($path, function($reader) {
		})->get()->toArray();
		$output = array_slice($arr,$start,$end);
		if(!empty($output) && count($output)){
			$flag = true;
			$arr = [];
			foreach($output as $key=>$value){
				array_push($arr,['title'=>$value['class_name'],
					'lesson_id'=>$value['lesson_id'],
					'teacher_id'=>$value['teacher_id'],
					'school_id'=>$school,
					'attendance_marked'=>2,
					'status'=>1
				]);
			}
			try{
				$res = Classes::insert($arr);
			}catch(\Illuminate\Database\QueryException $ex){ 
        //$ex->getMessage();
			}
		}
		if($flag){
			FileUpload::where('id',$id)->update(['file_type'=>'class','file_name'=>$fileName,'start_from'=>$start+$incremment,'end_from'=>$end+$incremment,'settings'=>$settings]);
		} else {
			FileUpload::where('id',$id)->delete();
		}
	}
}
