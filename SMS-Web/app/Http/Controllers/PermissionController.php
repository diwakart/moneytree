<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\LocationLicensee;
use App\Model\TerritoryLicensee;
use App\Model\School;
use App\Model\Teacher;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use App\Model\Role;
use App\Model\Modules;
use App\Model\Route;
use App\Model\Module_Assignment;
use App\Model\User_Restriction_Permission;
use Helper;

class PermissionController extends Controller
{
   public function get_permissions (Request $request){

      $role = Role::get()->toArray();
      $modules = Modules::get()->toArray();
      $routes = Route::get()->toArray();
      $module_id = explode(',',$routes[0]['module_id']);
      $module_assignment = Module_Assignment::select('id','role_id','module_id')->get()->where('role_id' , '3')->toArray();
      $user_restriction_permission = User_Restriction_Permission::get()->where('user_id','3')->toArray();
      $restricted_module = explode(',',$user_restriction_permission[0]['restricted_module']);
      $matched_restricted_modules = array_intersect($module_id, $restricted_module);
      if(!empty($matched_restricted_modules)){
          echo "You can't Acess this Route.";
      } else {
        echo "Welcome User";
      }
    } 
}
