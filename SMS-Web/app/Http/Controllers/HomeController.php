<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Countries;
use App\User;
use App\Model\Classes;
use DB;
use Illuminate\Support\Facades\Hash;
use App\Model\Lesson;
use Auth;
use Redirect;
use Helper;
use Illuminate\Support\Facades\Cache;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
    * @created : August 25, 2018
    * @access  : public
    * @Purpose : This function is use to view Admin dashboard.
    * @params  : None
    * @return  : None
    */
    public function index()
    {
        // After Login Get All Permission and put into Session
        if(Auth::user()){
            $accessRoutes = Helper::AllAccessRoutes();
            Session::put('AllAccessRoutes',  $accessRoutes);
            $value = Session::get('AllAccessRoutes');
        }

        $countries = Countries::count('id');
        $class =  Classes::count('id');
        $parent = User::where('user_type','6')->count('id');
        $student = User::where('user_type','7')->count('id');

        return view('home',compact('countries','class','parent','student'));
    }

    public function profile()
    {
        $user = Auth::user();
        return view('profile',compact('user'));
    }

    /*public function uploadProfilePic(Request $request)
    {
        //echo "Asdas"; die;
        print_r($_FILES); die;
    }*/

    public function updateProfile(Request $request)
    {
        $id = $_POST['id'];
        if($request->contact==''){
          $check=User::where(['email'=>$request->email])
        ->where(function($query) use ($id){
            if(isset($id)){
              $query->where('id' , '<>' ,$id);
            }
        })->exists();
        }else{
          $check=User::where(['email'=>$request->email,'contact'=>$request->contact])
        ->where(function($query) use ($id){
            if(isset($id)){
              $query->where('id' , '<>' ,$id);
            }
        })->exists();
        }
        

      if($check){
          Session::flash('errormessage', 'Contact or Email Address already exist.'); 
          return back();
        }else{
          if($id){
        $user = User::find($id);
      }else{
        $user = new User();
      }


    $user->name = $_POST['name'];
    $user->email = $_POST['email'];
    $user->contact =$_POST['contact'];
    $user->country_code =$_POST['country_code'];
    
      if ($request->hasFile('picture'))
      {
         $image = $request->file('picture');
         $path = storage_path('app/profile');
         $filename = time() . '.' . $image->getClientOriginalExtension();
         $image->move($path, $filename);

         $user->picture = 'profile/'.$filename;
       
      }

      try { //->update(['name' => $request->name])
          $user->save();
          Session::flash('successmessage', 'Profile Updated Successfully'); 
          return Redirect::to(url('/').'/profile');
        }catch(\Illuminate\Database\QueryException $ex){ 
                   $error_code = $ex->errorInfo[1];
                   if($error_code == 1062){
                    $result = $ex->getMessage();
                     Session::flash('errormessage','Contact or Email already exist');
                     return Redirect::to(url('/').'/profile');
                 
              }
         }
        }
    }

     public function updatePassword(Request $request)
    {
        $id = $_POST['id'];
        if($id){
            $user = User::find($id);
            $user->password =  Hash::make($_POST['new_password']);
         
            if($user->save()){

             Session::flash('successmessage', 'Password Updated Successfully'); 
             return Redirect::to(url('/').'/profile');
            } else{
             Session::flash('errormessage', 'Failed to update'); 
             return Redirect::to(url('/').'/profile');
           }
      }    
      
    }


    
}
