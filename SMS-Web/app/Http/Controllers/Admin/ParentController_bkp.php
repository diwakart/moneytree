<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\LocationLicensee;
use App\Model\TerritoryLicensee;
use App\Model\School;
use App\Model\Teacher;
use App\Model\Parents;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use App\Model\Role;
use App\Model\Modules;
use App\Model\Route;
use App\Model\Module_Assignment;
use App\Model\User_Restriction_Permission;
use Helper;

class ParentController extends Controller
{

    /*
  |--------------------------------------------------------------------------
  | TeacherController
  |--------------------------------------------------------------------------
  |
  | Here is where you can register new Teacher for your application. These
  | controller used to add,edit, delete Teacher with show all Teacher in data
  | table.
  |
  */

    /*
   * Create a new controller instance.
   *
   * @return void
  */
  public function __construct()
  {
      $this->middleware('isAdmin');
  }
    
  public function index()
  {
      // $user = Auth::user();
      // if($user->user_type=='2')
      // {
      //   config(['settings.country_field' => 'false']);
      //   config(['settings.tl_field' => 'false']);
      // }
        return view("admin.parents.index");
  }

  public function show(Request $request)
    {
      try
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $ll_name=(isset($request->datatable['query']['ll_name']) && ($request->datatable['query']['ll_name']!="") ? $request->datatable['query']['ll_name'] : '');
        $country_id = (isset($request->datatable['query']['country_list']) && ($request->datatable['query']['country_list']!="all") ? $request->datatable['query']['country_list'] : '');
        $parent_data = [];
        $parentuser_data = new User();
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $parentuser_data = $parentuser_data        
        ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','name','email','contact','status')
        ->where(array('user_type'=>6));
        
        // if(Auth::user()->user_type=='2')
        // {
        //   $lluser_data->where('location_licensees.tl_id',Auth::user()->id);
        // }
        // if($country_id !='')
        // {
        //   $lluser_data = $lluser_data->where(array('location_licensees.country_id'=>$country_id));
        // }
       
        // if($ll_name!='')
        // {
        //   $lluser_data = $lluser_data->where('users.name', 'like', $ll_name.'%');
        // }

        $parentuser_data = $parentuser_data->orderby('users.id','asc')->offset($offset)->limit($perpage)->get();
        $total = $parentuser_data->count();
        $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=> $parentuser_data,'meta'=> $meta));
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }
    }
}