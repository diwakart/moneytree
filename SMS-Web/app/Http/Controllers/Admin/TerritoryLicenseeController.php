<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TerritoryLicensee;
use App\Model\LocationLicensee;
use App\Model\Countries;
use App\User;
use App\Model\School;
use App\Model\Teacher;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use Helper;

class TerritoryLicenseeController extends Controller
{
   public $Settings = ['country_field' => true, 'tl_field' => true, 'll_field' => true, 'school_field' => true];

  /*
  |--------------------------------------------------------------------------
  | TerritoryLicenseeController
  |--------------------------------------------------------------------------
  |
  | Here is where you can register new TL for your application. These
  | controller used to add,edit, delete TL with show all TL in data
  | table.
  |
  */


  /*
   * Create a new controller instance.
   *
   * @return void
  */
    public function __construct()
    {
      $this->middleware('isAdmin');
    }

  /*
  * @created : Sep 29, 2018
  * @author  : Anup Singh
  * @access  : public
  * @Purpose : This function is use to view index page of country
  * @params  : None
  * @return  : None
  */
    public function index()
    {
      $user = Auth::user();
      if($user->user_type=='1')
      {
        $this->Settings['country_field'] = true;
        $this->Settings['tl_field']      = true;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='2')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = false;
      }
      //echo \Request::route()->getName();die;
      try
      {
        $all_tl_country = DB::table('countries')->where('default_country','1')->get();
        return view("admin.territoryLicensee.index",compact('all_tl_country',['all_tl_country'=>$all_tl_country,'Settings'=>$this->Settings]));
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      } 
    }

    /*
    * @created : Sep 29, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to change status of TL.
    * @params  : \Illuminate\Http\Request  $request and id of user 
    * @return  : \Illuminate\Http\Response with message status changed *    successfully or not
    */
    public function changeTLStatus(Request $request)
    {
      try
      {
        if($request->ajax())
        {
          if($request->id){
            $user_detail = User::where(['id'=>$request->id])->first();
             $user = User::find($user_detail->id);
            if($user_detail->status==1){
              $user->status = 2;
            }else{
              $user->status = 1;
            }
             
            if ($user->save())
              return (json_encode(array('status'=>'success','message'=>"Updated Successfully")));
          }
          return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
        }
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }
    }

    /*
    * @created : Sep 29, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to get Countyry code of selected country.
    * @params  : \Illuminate\Http\Request  $request country id 
    * @return  : \Illuminate\Http\Response with message and country code.
    */

    public function getCountryCode(Request $request)
    {
      try
      {
        if($request->ajax())
        {
          if($request->id){
            $data = Countries::where(['id'=>$request->id])->first();
            if(!empty($data))
            {
              return (json_encode(array('status'=>'success','message'=>"Country code details.",'data'=>$data)));
            }
            else
            {
              return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
            }
            
          }
          return (json_encode(array('status'=>'error','message'=>"please select country first.",'data'=>'')));
          
        }
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }

    }

    
    /*
    * @created : Sep 29, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to show all TL users.
    * @params  : \Illuminate\Http\Request  $request, search TL name 
    * @return  : \Illuminate\Http\Response with all TL data and also
    *            filter TL name response data.
    */
    public function show(Request $request)
    {
      try
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $tlname=(isset($request->datatable['query']['tlname']) && ($request->datatable['query']['tlname']!="") ? $request->datatable['query']['tlname'] : '');
        $territory_licensees = new User();
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $territory_licensees = $territory_licensees->join('territory_licensees', 'users.id', '=', 'territory_licensees.user_id')->join('countries', 'territory_licensees.country_id', '=', 'countries.id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'countries.name as country','users.name','users.email','users.contact','users.id','users.status')->where('users.user_type',2);
        if($tlname!='')
        {
          $territory_licensees = $territory_licensees->where('users.name','like',$tlname.'%');
        }
        $total = $territory_licensees->count();
        $territory_licensees = $territory_licensees->orderby('users.id','asc')->offset($offset)->limit($perpage)->get();
        $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=> $territory_licensees,'meta'=> $meta));
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }
    }

    /*
    * @created : Sep 30, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to add new TL users.
    * @params  : \Illuminate\Http\Request  
    * @return  : \Illuminate\Http\Response 
    */
    public function store(Request $request)
    {
        if($request->ajax())
      {


        $rules = array(
          'country'     => 'required',
          'name'     => 'required',
          'email'     => 'required',
          'contact'     => 'required',
          'country_code'     => 'required',
          
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
           if(isset($failedRules['country']))
            $errorMsg = $failedRules['country'][0] . "\n";
            if(isset($failedRules['name']))
                $errorMsg = $failedRules['name'][0] . "\n";
            if(isset($failedRules['email']))
                $errorMsg = $failedRules['email'][0] . "\n";
            if(isset($failedRules['contact']))
                $errorMsg = $failedRules['contact'][0] . "\n";
            if(isset($failedRules['country_code']))
                $errorMsg = $failedRules['country_code'][0] . "\n";
           
            
        
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{
           $id=$request->id;
              $check=User::where(['email'=>$request->email,'contact'=>$request->contact])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();

              if($check){
                  return (json_encode(array('status'=>'error','message'=>"Email or Contact already exist." )));
                }


              if($request->id){
                $tl_user = User::find($request->id);
              }else{
                $tl_user = new User();
                $tl_user->password = Hash::make($request->password);
              }
        }


        $tl_user->name = $request->name;
        $tl_user->email = $request->email;
        $tl_user->contact = $request->contact;
        $tl_user->country_code = $request->country_code;
       
        $tl_user->user_type = 2;


        try {
                 if($tl_user->save()){
                  $tl_id = TerritoryLicensee::where(['user_id'=>$request->id])->first();
                  if($request->id){
                    $tl_detail = TerritoryLicensee::find($tl_id->id);
                    $tl_detail->country_id =$request->country;
                    $tl_detail->user_id =$tl_user->id;
                  }else{
                    $tl_detail = new TerritoryLicensee();
                    $tl_detail->country_id = $request->country;
                    $tl_detail->user_id = $tl_user->id;
                  }
                  if($tl_detail->save()){
                    return (json_encode(array('status'=>'success','message'=>sprintf('Territory Licensee "%s" successfully saved', $tl_user->name))));
                  }
                 }
                   
          }catch(\Illuminate\Database\QueryException $ex){ 
                   $error_code = $ex->errorInfo[1];
                   if($error_code == 1062){
                    $result = $ex->getMessage();
                    return (json_encode(array('status'=>'error','message'=>'Territory Licensee already exist'))) ;
              }else{
                $result = $ex->getMessage();
                return (json_encode(array('status'=>'error','message'=>$result))) ;
              }
            }
      }
    }
    /*
    * @created : Sep 30, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to get details of specific TL.
    * @params  : \Illuminate\Http\Request  
    * @return  : \Illuminate\Http\Response 
    */

    public function edit(Request $request)
    {
       
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = User::join('territory_licensees', 'users.id', '=', 'territory_licensees.user_id')->join('countries', 'territory_licensees.country_id', '=', 'countries.id')->select('countries.name as country','users.name','users.email','users.contact','users.id','users.status')->where(['users.id'=>$request->id])->select('users.name','users.contact','users.country_code','users.email','users.status','countries.id as country','users.id')->first();
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }

    /*
    * @created : August 30, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to delete specific TL.
    * @params  : \Illuminate\Http\Request  
    * @return  : \Illuminate\Http\Response 
    */
    public function destroy(Request $request)
    {
      if($request->ajax())
      {
        if($request->id){
          $user = User::find($request->id);
          $user_id = TerritoryLicensee::where(['user_id'=>$request->id])->first();
          $user_detail = TerritoryLicensee::find($user_id->id);
          if ($user->delete() &&  $user_detail->delete())
            return (json_encode(array('status'=>'success','message'=>"TL Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }
    }
}

    