<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\LocationLicensee;
use App\Model\TerritoryLicensee;
use App\Model\School;
use App\Model\Teacher;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use Helper;


class LocationLicenseeController extends Controller
{

    /*
  |--------------------------------------------------------------------------
  | LocationLicenseeController
  |--------------------------------------------------------------------------
  |
  | Here is where you can register new LL for your application. These
  | controller used to add,edit, delete LL with show all LL in data
  | table.
  |
  */
  public $Settings = ['country_field' => true, 'tl_field' => true, 'll_field' => true, 'school_field' => true];

  /*
   * Create a new controller instance.
   *
   * @return void
  */
  public function __construct()
  {
      $this->middleware('isAdmin');
      //$this->middleware('checkPermission');
  }
    
  public function index()
  {

    $user = Auth::user();
    if($user->user_type=='1')
    {
      $this->Settings['country_field'] = true;
      $this->Settings['tl_field']      = true;
      $this->Settings['ll_field']      = false;
      $this->Settings['school_field']  = false;
    }
    if($user->user_type=='2')
    {
      $this->Settings['country_field'] = false;
      $this->Settings['tl_field']      = false;
      $this->Settings['ll_field']      = false;
      $this->Settings['school_field']  = false;
    }
    //print_r($settings); die;
    return view("admin.locationLicensee.index",['Settings'=>$this->Settings]);
  }

  /*
  * @created : Sep 29, 2018
  * @author  : Anup Singh
  * @access  : public
  * @Purpose : This function is use to view index page of country
  * @params  : None
  * @return  : None
  */
    public function changeLLStatus(Request $request)
    {
      try
      {
        if($request->ajax())
        {
          if($request->id){
            $user_detail = User::where(['id'=>$request->id])->first();
             $user = User::find($user_detail->id);
            if($user_detail->status==1){
              $user->status = 2;
            }else{
              $user->status = 1;
            }
             
            if ($user->save())
              return (json_encode(array('status'=>'success','message'=>"Updated Successfully")));
          }
          return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
        }
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        try
        {
            if($request->ajax())
            {      
                $rules = array(
                    'name'     => 'required',
                    'email'     => 'required',
                    'contact'     => 'required',
                    'country_code'=> 'required',
                    'state'     => 'required',
                    'tl_id'     => 'required',
                );
                $tl_id = $request->tl_id;
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails())
                {
                    $failedRules = $validator->getMessageBag()->toArray();
                    $errorMsg = "";
                    if(isset($failedRules['name']))
                    $errorMsg = $failedRules['name'][0] . "\n";
                    if(isset($failedRules['email']))
                    $errorMsg = $failedRules['email'][0] . "\n";
                    if(isset($failedRules['contact']))
                    $errorMsg = $failedRules['contact'][0] . "\n";
                    if(isset($failedRules['country_code']))
                    $errorMsg = $failedRules['country_code'][0] . "\n";
                    if(isset($failedRules['state']))
                    $errorMsg = $failedRules['state'][0] . "\n";
                    if(isset($failedRules['tl_id']))
                    $errorMsg = $failedRules['tl_id'][0] . "\n";
                    return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
                }
                else
                {
                    $id=$request->id;
                    $check=User::where(['email'=>$request->email,'contact'=>$request->contact])
                    ->where(function($query) use ($id)
                    {
                    if(isset($id)){
                    $query->where('id' , '<>' ,$id);
                    }
                    })->exists();
                    
                    if($check){
                        return (json_encode(array('status'=>'error','message'=>"Email or Contact already exist." )));
                    }
                    
                    
                    if($request->id){
                        $ll_user = User::find($request->id);
                    }else{
                        $ll_user = new User();
                        $ll_user->password = Hash::make($request->password);
                    }
                    
                    
                    $ll_user->name = $request->name;
                    $ll_user->email = $request->email;
                    $ll_user->contact = $request->contact;
                    $ll_user->country_code = $request->country_code;
                    $ll_user->user_type = 3;
                    if($ll_user->save()){
                    $ll_id = LocationLicensee::where(['user_id'=>$request->id])->first();
                    if($request->id){
                        $ll_detail = LocationLicensee::find($ll_id->id);
                    }else{
                        $ll_detail = new LocationLicensee();
                    }
                    $ll_detail->user_id = $ll_user->id;
                    //$ll_detail->country_id = $country_id;
                    $ll_detail->location = $request->state;
                    $ll_detail->tl_id = $tl_id;
                    if($ll_detail->save()){
                        return (json_encode(array('status'=>'success','message'=>sprintf('Location Licensee "%s" successfully saved', $ll_user->name))));  
                    }else{
                        return (json_encode(array('status'=>'error','message'=>sprintf('Failed to update location for Location Licensee "%s"', $ll_user->name))));
                    }
                    }
                }
            }
        }
        catch(\Illuminate\Database\QueryException $ex)
        { 
            $error_code = $ex->errorInfo[1];
            if($error_code == 1062)
            {
                $result = $ex->getMessage();
                return (json_encode(array('status'=>'error','message'=>'Location Licensee already exist'))) ;
            }else
            {
                $result = $ex->getMessage();
                return (json_encode(array('status'=>'error','message'=>$result))) ;
            }
        }
    }

    /*
    * @created : August 29, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to show all LL users.
    * @params  : \Illuminate\Http\Request  $request, search LL name 
    * @return  : \Illuminate\Http\Response with all LL data and also
    *            filter LL name response data.
    */

    public function show(Request $request)
    {
      try
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $ll_name=(isset($request->datatable['query']['ll_name']) && ($request->datatable['query']['ll_name']!="") ? $request->datatable['query']['ll_name'] : '');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $lluser_data = new User();
        $lluser_data = $lluser_data
                        ->join('location_licensees', 'users.id', '=', 'location_licensees.user_id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'users.name','users.email','users.contact','users.id','users.status','location_licensees.location')
                        ->where(array('users.user_type'=>3));
        if(Auth::user()->user_type=='2')
        {
          $lluser_data->where('location_licensees.tl_id',Auth::user()->id);
        }
       
        if($ll_name!='')
        {
          $lluser_data = $lluser_data->where('users.name', 'like', $ll_name.'%');
        }

        $lluser_data = $lluser_data->orderby('users.id','asc');
        $total = $lluser_data->count();
        $lluser_data = $lluser_data->offset($offset)->limit($perpage)->get();
        $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=> $lluser_data,'meta'=> $meta));
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try { //->update(['name' => $request->name])
          $result = User::join('location_licensees', 'users.id', '=', 'location_licensees.user_id')->where(['users.id'=>$request->id])->select('users.name','users.contact','users.country_code','users.email','users.status','users.id','location_licensees.location','location_licensees.tl_id as tl_id',DB::raw("(select name from sms_users where id = tl_id) as tl_name"))->first();  
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }

    /**
     * Remove the specified resource from storage (Modified for implementing Permission Handling).
     * @modified on : September 22, 2018         
     * @param  \Illuminate\Http\Request  $request     
     * @return \Illuminate\Http\Response
     **/     

    public function destroy(Request $request)
    {   
      if($request->ajax())
      {
        if($request->id) {
          $user = User::find($request->id);
          $user_id = LocationLicensee::where(['user_id'=>$request->id])->first();
          $user_detail = LocationLicensee::find($user_id->id);
          if ($user->delete() &&  $user_detail->delete())
            return (json_encode(array('status'=>'success','message'=>"LL Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }          
    }    
}
