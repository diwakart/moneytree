<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\LocationLicensee;
use App\Model\TerritoryLicensee;
use App\Model\School;
use App\Model\Teacher;
use App\Model\Student;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use App\Model\Role;
use App\Model\Modules;
use App\Model\Route;
use App\Model\Module_Assignment;
use App\Model\User_Restriction_Permission;
use Helper;

class StudentController extends Controller
{

    /*
  |--------------------------------------------------------------------------
  | TeacherController
  |--------------------------------------------------------------------------
  |
  | Here is where you can register new Teacher for your application. These
  | controller used to add,edit, delete Teacher with show all Teacher in data
  | table.
  |
  */
  public $Settings = ['country_field' => true, 'tl_field' => true, 'll_field' => true, 'school_field' => true];

    /*
   * Create a new controller instance.
   *
   * @return void
  */
  public function __construct()
  {
      $this->middleware('isAdmin');
  }
    
  public function index()
  {
      $user = Auth::user();
      $school_data = array();
      if($user->user_type=='1')
      {
        $this->Settings['country_field'] = true;
        $this->Settings['tl_field']      = true;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='2')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='3')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = false;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='4')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = false;
        $this->Settings['school_field']  = false;
      }
      if($user->user_type=='5')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = false;
        $this->Settings['school_field']  = false;
        $school_data = DB::table('teachers')->select('school_id')->where('user_id',Auth::user()->id)->first();
      }
      //print_r($school_data); die;
      $parents = User::select('id','name')->where(['status'=>'1','user_type'=>'6'])->get();
      return view("admin.students.index",['Settings'=>$this->Settings,'parents'=>$parents,'school_data'=>$school_data]);
  }

  public function show(Request $request)
    {
      try
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $studentuser_data = new User();
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        DB::statement(DB::raw('set @rownumber='.$offset.''));

        $studentuser_data = $studentuser_data->join('students', 'users.id', '=', 'students.user_id')
        ->join('schools', 'schools.user_id', '=', 'students.school_id')
        ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'users.id as id','users.name as name','users.email as email','users.contact as contact','users.status as status','students.school_id as school_id',DB::raw("(select name from sms_users where id = school_id) as school"))
        ->where(array('users.user_type'=>7));                
        if(Auth::user()->user_type=='2'){
          $studentuser_data->where('schools.tl_id',Auth::user()->id);
        }
        else if(Auth::user()->user_type=='3'){

          $studentuser_data->where('schools.ll_id',Auth::user()->id);
        }
        else if(Auth::user()->user_type=='4'){
          $studentuser_data->where('students.school_id',Auth::user()->id);
        }
        else if(Auth::user()->user_type=='5'){
          $school_data = DB::table('teachers')->select('school_id')->where('user_id',Auth::user()->id)->first();
          $studentuser_data->where('students.school_id',$school_data->school_id);
        }
        
        $studentuser_data = $studentuser_data->where('user_type','7')->orderby('users.id','asc');
        $total = $studentuser_data->count();
        $studentuser_data = $studentuser_data->offset($offset)->limit($perpage)->get();
        
        $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=> $studentuser_data,'meta'=> $meta));
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }
    }

    public function store(Request $request)
    {
      //print_r($request->all());die;                 

        if($request->ajax())
        {      
           $rules = array(
          'name'     => 'required',
          'email'     => 'required',
          'contact'     => 'required',
          'country_code'=> 'required',
          'school_id'     => 'required',
          );

           $school_id = $request->school_id;

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
            if(isset($failedRules['name']))
                $errorMsg = $failedRules['name'][0] . "\n";
            if(isset($failedRules['email']))
                $errorMsg = $failedRules['email'][0] . "\n";
            if(isset($failedRules['contact']))
                $errorMsg = $failedRules['contact'][0] . "\n";
            if(isset($failedRules['country_code']))
                $errorMsg = $failedRules['country_code'][0] . "\n";
              if(isset($failedRules['school_id']))
                $errorMsg = $failedRules['school_id'][0] . "\n";
            
        
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{
           $id=$request->id;
              $check=User::where(['email'=>$request->email,'contact'=>$request->contact])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();

              if($check){
                  return (json_encode(array('status'=>'error','message'=>"Email or Contact already exist." )));
                }


              if($request->id){
                $student_user = User::find($request->id);
              }else{
                $student_user = new User();
                $student_user->password = Hash::make('secret');
              }
        }


        $student_user->name = $request->name;
        $student_user->email = $request->email;
        $student_user->contact = $request->contact;
        $student_user->country_code = $request->country_code;
        $student_user->user_type = 7;


        try {
                 if($student_user->save()){
                    $teacher_id = Student::where(['user_id'=>$request->id])->first();
                  if($request->id){

                    $student_detail = Student::find($teacher_id->id);
                  }else{
                    $student_detail = new Student();
                  }
                 
                  $student_detail->user_id = $student_user->id;
                  $student_detail->school_id = $school_id;

                  if($student_detail->save()){
                    return (json_encode(array('status'=>'success','message'=>sprintf('Student "%s" successfully saved', $student_user->name))));  
                  }else{
                    return (json_encode(array('status'=>'error','message'=>sprintf('Failed to update Student "%s"', $teacher_user->name))));
                  }
                 }
                  
                   
          }catch(\Illuminate\Database\QueryException $ex){ 
                   $error_code = $ex->errorInfo[1];
                   if($error_code == 1062){
                    $result = $ex->getMessage();
                    return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
              }else{
                $result = $ex->getMessage();
                return (json_encode(array('status'=>'error','message'=>$result))) ;
              }
            }
      }      
    }

    public function status(Request $request)
    {
     if($request->ajax())
     {
      if($request->id){
        $user_detail = User::where(['id'=>$request->id])->first();
        $user = User::find($user_detail->id);
        if($user_detail->status==1){
          $user->status = 2;
        }else{
          $user->status = 1;
        }

        if ($user->save())
          return (json_encode(array('status'=>'success','message'=>"Updated Successfully")));
      }
      return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
    }
  }

  public function destroy(Request $request)
  {
    if($request->ajax())
    {
      if($request->id){
        $user = User::find($request->id);
        $user_id = Student::where(['user_id'=>$request->id])->first();
        $user_detail = Student::find($user_id->id);
        if ($user->delete() &&  $user_detail->delete())
          return (json_encode(array('status'=>'success','message'=>"Student Deleted Successfully")));
      }
      return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
    }
  }

  public function detail(Request $request)
  {
    if($request->ajax())
    {
      $status = "success";
      try {
        $id = $request->get('id');
        $result = User::join('students', 'users.id', '=', 'students.user_id')
        ->select('users.id as id','users.name as name','users.email as email','users.country_code as country_code','users.contact as contact','users.status as status','students.school_id as school_id',DB::raw("(select name from sms_users where id = school_id) as school_name"))
        ->where('users.id',$id)->first();

      } catch(QueryException $ex){ 
        dd($ex->getMessage());
        $status = "error";
        $result = $ex->getMessage();
      }
      return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
    }
  }

  public function tagParent(Request $request) {

      if($request->ajax())
      {      
        $rules = array(
          'tagname'     => 'required',                    
        );

        $student_id = $request->id;        

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['tagname']))
            $errorMsg = $failedRules['tagname'][0] . "\n";        
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" )));
        }

        $student_detail = StudentTag::where(['student_id'=>$request->id])->get();        

        if(!empty($student_detail)){
          foreach($student_detail as $students) {
            $students_ids = $students->id;
            $students = StudentTag::find($students_ids);
            \DB::table('student_tag')->delete(); 
          }
        }        
        $tagparent = new StudentTag();

        $parent_id = $request->tagname;        

        foreach($parent_id as $parents) {
          $tagparent->parent_id = $parents;
          $tagparent->student_id = $student_id;
          $data = array(
            'parent_id'  => $parents,
            'student_id' => $student_id
          );
          \DB::table('student_tag')->insert($data);  
        }
        return (json_encode(array('status'=>'success','message'=>sprintf('Student Tagged Parent successfully saved'))));                           
      }
    }

  public function getAllTaggedParents(Request $request) {      

      if($request->ajax())
      {
        $student_id = $request->id;      

        $parents = StudentTag::select('id','parent_id')->where(['student_id'=>$student_id])->get();
        $ids='';
        foreach ($parents as $key => $value) {

          $ids = $ids.','.$value->parent_id; 
        }        
        return (json_encode(array('message'=>$parents ))) ;
      }
    }

public function assignClass(Request $request) {
      //print_r($request->all());die;

      if($request->ajax())
      {      
        $rules = array(
          'tagclass'     => 'required',                    
        );

        $class_id = $request->tagclass;
        $student_id = $request->id;        

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['tagclass']))
            $errorMsg = $failedRules['tagclass'][0] . "\n";        
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" )));
        }
      
        \DB::table('students')
            ->where('user_id', $student_id)
            ->update(['class_id' => $class_id]);        
        return (json_encode(array('status'=>'success','message'=>sprintf('Student Tagged Parent successfully saved'))));                           
      }
    }
}
