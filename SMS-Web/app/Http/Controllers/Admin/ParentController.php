<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\LocationLicensee;
use App\Model\TerritoryLicensee;
use App\Model\School;
use App\Model\Teacher;
use App\Model\Parents;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use App\Model\Role;
use App\Model\Modules;
use App\Model\Route;
use App\Model\Module_Assignment;
use App\Model\User_Restriction_Permission;
use Helper;

class ParentController extends Controller
{

    /*
  |--------------------------------------------------------------------------
  | TeacherController
  |--------------------------------------------------------------------------
  |
  | Here is where you can register new Teacher for your application. These
  | controller used to add,edit, delete Teacher with show all Teacher in data
  | table.
  |
  */

    /*
   * Create a new controller instance.
   *
   * @return void
  */
  public function __construct()
  {
      $this->middleware('isAdmin');
  }
    
  public function index()
  {    
    return view("admin.parents.index");
  }

  public function show(Request $request)
    {
      try
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $parentuser_data = new User();
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $parentuser_data = $parentuser_data        
        ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','name','email','contact','status','picture')
        ->where(array('user_type'=>6));
        $parentuser_data = $parentuser_data->orderby('users.id','asc')->offset($offset)->limit($perpage)->get();
        $total = $parentuser_data->count();
        $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=> $parentuser_data,'meta'=> $meta));
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }
    }

    public function uploadParentPic(Request $request)
    {
      if ($request->hasFile('user_image'))
      {
         $image = $request->file('user_image');
         $path = storage_path('app/profile');
         $filename = time() . '.' . $image->getClientOriginalExtension();
         $image->move($path, $filename);

         return $file_name = 'profile/'.$filename;
      }
      else
      {
        return $file_name = "";
      }
      //print_r($_FILES); die;
    }

    public function store(Request $request)
    {
    //print_r($request->all());die;                       
        if($request->ajax())
        {      
           $rules = array(
          // 'country'     => 'required',
          'name'     => 'required',
          'email'     => 'required',
          'contact'     => 'required',
          'country_code'=> 'required',          
          );
          $country_id = $request->country;           

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
           if(isset($failedRules['country']))
            $errorMsg = $failedRules['country'][0] . "\n";
            if(isset($failedRules['name']))
                $errorMsg = $failedRules['name'][0] . "\n";
            if(isset($failedRules['email']))
                $errorMsg = $failedRules['email'][0] . "\n";
            if(isset($failedRules['contact']))
                $errorMsg = $failedRules['contact'][0] . "\n";
            if(isset($failedRules['country_code']))
                $errorMsg = $failedRules['country_code'][0] . "\n";                        
        
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{
           $id=$request->id;
              $check=User::where(['email'=>$request->email,'contact'=>$request->contact])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();

              if($check){
                  return (json_encode(array('status'=>'error','message'=>"Email or Contact already exist." )));
                }

              if($request->id){
                $parent_user = User::find($request->id);
              }else{
                $parent_user = new User();
                $parent_user->password = Hash::make($request->password);
              }
        }
        $parent_user->name = $request->name;
        $parent_user->email = $request->email;
        $parent_user->contact = $request->contact;
        $parent_user->country_code = $request->country_code;
        $parent_user->user_type = 6;
        $parent_user->picture = $request->image_name;
        //$user_image = uploadParentPic

        try 
        {                                                     
            if($parent_user->save()){
                $parent_id = Parents::where(['user_id'=>$request->id])->first();
                if($request->id){
                $parent_detail = Parents::find($parent_id->id);
                }else{
                $parent_detail = new Parents();
                }
                $parent_detail->user_id = $parent_user->id;
                $parent_detail->country_id = $country_id;
            if($parent_detail->save()){
                return (json_encode(array('status'=>'success','message'=>sprintf('Parent "%s" successfully saved', $parent_user->name)))); 
            }
            }else{
            return (json_encode(array('status'=>'error','message'=>sprintf('Failed to update Parent "%s"', $parent_user->name))));
            }
        } 
             catch(\Illuminate\Database\QueryException $ex){ 
                   $error_code = $ex->errorInfo[1];
                   if($error_code == 1062){
                    $result = $ex->getMessage();
                    return (json_encode(array('status'=>'error','message'=>'Parent already exist'))) ;
              }else{
                $result = $ex->getMessage();
                return (json_encode(array('status'=>'error','message'=>$result))) ;
              }
            }
      }      
    }

    public function edit(Request $request)
    {           
      
      if($request->ajax())
      {
        $status = "success";
        try {
          $result = User::select('users.name','users.picture','users.contact','users.country_code','users.email','users.status','users.id as id','parents.country_id')->join('parents','parents.user_id','=','users.id')->where(['users.id'=>$request->id])->first();  
        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }

    public function destroy(Request $request)
    {
      if($request->ajax())
      {
        if($request->id) {
          $user = User::find($request->id);
          if ($user->delete())
            return (json_encode(array('status'=>'success','message'=>"Parent Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }          
    }
}