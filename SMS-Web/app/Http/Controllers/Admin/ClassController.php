<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Classes;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Model\LocationLicensee;
use App\Model\TerritoryLicensee;
use App\Model\School;
use App\Model\Teacher;
use App\Model\Student;
use App\Model\Lesson;
use App\Model\StudentAttendance;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use App\Model\Role;
use App\Model\Modules;
use App\Model\Route;
use App\Model\Module_Assignment;
use App\Model\User_Restriction_Permission;
use Helper;


class ClassController extends Controller
{
  public $Settings = ['country_field' => true, 'tl_field' => true, 'll_field' => true, 'school_field' => true];
  public function __construct()
  {
      $this->middleware('isAdmin');
  }
    
  public function index()
  {
    return view("admin.class.index");
  }
  
  public function addClassView()
  {
      $user = Auth::user();
      if($user->user_type=='1')
      {
        $this->Settings['country_field'] = true;
        $this->Settings['tl_field']      = true;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='2')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='3')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = false;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='4')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = false;
        $this->Settings['school_field']  = false;
      }
    return view("admin.class.addClass",['Settings'=>$this->Settings]);
  }

  public function EditClassView($id)
  {   
      $user=Auth::user();
      $obj = Classes::select('users.name','class.*','lessons.level_id','lessons.sublevel_id','lessons.lesson_id')->join('users', 'users.id', '=', 'class.school_id')->join('lessons', 'class.lesson_id', '=', 'lessons.id')->where('class.id',$id)->first();
      $AssignedStudent = Student::select('user_id')->where('class_id',$id)->get();
      if($AssignedStudent){
        $AssignedStudent=array_column($AssignedStudent->toArray(), 'user_id');
        $AssignedStudent=implode(',',$AssignedStudent);
      }
      if($user->user_type=='1')
      {
        $this->Settings['country_field'] = true;
        $this->Settings['tl_field']      = true;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='2')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='3')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = false;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='4')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = false;
        $this->Settings['school_field']  = false;
      }
    return view("admin.class.addClass",['Settings'=>$this->Settings,'editdata'=>$obj,'assignedstudent'=>$AssignedStudent]);
  }

  public function show(Request $request)
  {

    $perpage = $request->datatable['pagination']['perpage'];
    $page = $request->datatable['pagination']['page'];
    $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
    $class_name=(isset($request->datatable['query']['school_name']) && ($request->datatable['query']['school_name']!="") ? $request->datatable['query']['school_name'] : '');
   
    $allclass = new Classes();
    if($page=='1')
    {
      $offset = 0;
    }
    else {
      $offset = ($page-1)*$perpage;
    }         
    DB::statement(DB::raw('set @rownumber='.$offset.''));
      
    $allclass = $allclass
    ->join('schools', 'schools.id', '=', 'class.school_id')    
    ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'class.id as id','class.school_id as school_id','class.lesson_id as lesson_id','class.title as title','class.status as status',DB::raw("(select name from sms_users where id = school_id) as school_name"));
    if(Auth::user()->user_type=='2'){
      $allclass->where('schools.tl_id',Auth::user()->id);
    }
    else if(Auth::user()->user_type=='3'){

      $allclass->where('schools.ll_id',Auth::user()->id);
    }
    else if(Auth::user()->user_type=='4'){
      $allclass->where('class.school_id',Auth::user()->id);
    }
    else if(Auth::user()->user_type=='5'){
      $school_data = DB::table('teachers')->select('school_id')->where('user_id',Auth::user()->id)->first();
      $allclass->where('class.school_id',$school_data->school_id);
      $allclass->whereRaw('FIND_IN_SET(teacher_id,?)',Auth::user()->id);
    }
    $total = $allclass->count();
    $allclass = $allclass->orderby('id','asc')->offset($offset)->limit($perpage)->get();
    $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
    return Response::json(array('data'=> $allclass,'meta'=> $meta));
  }

    public function store(Request $request)
    {                       
        if($request->ajax())
        {      
            $rules = array(           
                'school_id'           => 'required',
                'teacher_id'          => 'required',
                'class_title'         => 'required',
                'lesson_id'          => 'required',
            );
      
            $validator = Validator::make($request->all(), $rules);
        
            if ($validator->fails()) 
            {
                $failedRules = $validator->getMessageBag()->toArray();
                $errorMsg = "";           
                if(isset($failedRules['school_id']))
                $errorMsg = $failedRules['school_id'][0] . "\n";
                if(isset($failedRules['teacher_id']))
                $errorMsg = $failedRules['teacher_id'][0] . "\n";
                if(isset($failedRules['class_title']))
                $errorMsg = $failedRules['class_title'][0] . "\n";  
                if(isset($failedRules['lessonLevel']))
                  $errorMsg = $failedRules['lessonLevel'][0] . "\n";    
                if(isset($failedRules['lessonSubLevel']))
                  $errorMsg = $failedRules['lessonSubLevel'][0] . "\n";    
                if(isset($failedRules['lesson_id']))
                $errorMsg = $failedRules['lesson_id'][0] . "\n";    
                return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
            }
            else
            {
                try
                {
                    $school_id = $request->school_id;
                    $teacher_id = $request->teacher_id;
                    $title = $request->class_title;
                    $student_ids = $request->student_ids;

                    if($request->id)
                    {
                        $class_detail = Classes::find($request->id);
                        $lesson_detail = Lesson::find($class_detail->lesson_id);
                        $lesson_detail->level_id = $request->lessonLevel;
                        $lesson_detail->sublevel_id = $request->lessonSubLevel;
                        $lesson_detail->lesson_id = $request->lesson_id;
                        $lesson_detail->save();

                        // Here we find result which is deselected at the time of edit
                        // print_r(explode(",",$student_ids));
                        // print_r(explode(",",$request->AllOldStudentId));
                        $allAssignedOldStudent = explode(",",$request->AllOldStudentId);
                        $currentallAssignedStudent = explode(",",$student_ids);

                        $totalUnassgnedStudent = array_diff($allAssignedOldStudent,$currentallAssignedStudent);

                        if($totalUnassgnedStudent){
                          Student::whereRaw('FIND_IN_SET(user_id,?)',implode(",",$totalUnassgnedStudent))->update(['class_id'=>'0']);
                        }

                        $totalAssgnedStudent = array_diff($currentallAssignedStudent,$allAssignedOldStudent);
                        if($totalAssgnedStudent){
                          Student::whereRaw('FIND_IN_SET(user_id,?)',implode(",",$totalAssgnedStudent))->update(['class_id'=>$request->id]);
                        }
                    }
                    else
                    {
                        $class_detail = new Classes();
                        $lesson_detail = new Lesson();

                        $lesson_detail->level_id = $request->lessonLevel;
                        $lesson_detail->sublevel_id = $request->lessonSubLevel;
                        $lesson_detail->lesson_id = $request->lesson_id;
                        $lesson_detail->save();

                        $class_detail->lesson_id = $lesson_detail->id;
                    }

                    $class_detail->school_id = $school_id;
                    $class_detail->title = $title;
                    $class_detail->teacher_id = $teacher_id;

                    if($class_detail->save()){
                        if($student_ids){
                          $lastInsertId = $class_detail->id;
                          Student::whereRaw('FIND_IN_SET(user_id,?)',$student_ids)->update(['class_id'=>$lastInsertId]);
                        }
                        return (json_encode(array('status'=>'success','message'=>sprintf('Class successfully saved'))));  
                    }else{
                        return (json_encode(array('status'=>'error','message'=>sprintf('Failed to update Class'))));
                    }
                }
                catch(\Illuminate\Database\QueryException $ex)
                { 
                    $error_code = $ex->errorInfo[1];
                    if($error_code == 1062)
                    {
                        $result = $ex->getMessage();
                        return (json_encode(array('status'=>'error','message'=>'Class already exist')));
                    }
                    else
                    {
                        $result = $ex->getMessage();
                        return (json_encode(array('status'=>'error','message'=>$result)));
                    }
                }
            }
        }      
    }

    public function destroy(Request $request)
    {
      $id = $request->get('id');
      // if($id){
      //   $result = Classes::find($id)->first();
      //   $count=StudentAttendance::where('school_id',$result['school_id'])->where('class_id',$result['id'])->where('lesson_id',$result['lesson_id'])->count();
      //   if($count){
      //     return (json_encode(array('status'=>200,'count'=>'The Class is in Process for Assigned Teacher'))) ;
      //   }
      // }

      if($id){
         $res = Classes::find($id)->delete();
         if($res){
           return (json_encode(array('status'=>200,'message'=>'Delete Successfully'))) ; 
         }
       } else {
        return (json_encode(array('status'=>401,'message'=>'Error'))) ;  
      }
    }

    public function changeClassStatus(Request $request)
    {
        if($request->ajax())
        {
        if($request->id){
          $user_detail = Classes::where(['id'=>$request->id])->first();
           $user = Classes::find($user_detail->id);
          if($user_detail->status==1){
            $user->status = 2;
          }else{
            $user->status = 1;
          }
          if ($user->save())
            return (json_encode(array('status'=>'success','message'=>"Updated Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }
    }


    public function getClassbySchool($id,Request $request){
      if($request->ajax())
      {
        $allClass = Classes::where(['school_id'=>$id])->get();
        if($allClass){
          return (json_encode($allClass));
        }else{
           return (json_encode(array('status'=>'success','message'=>"Class Not Found.")));
        }
      }
    }

    public function getClassbyTeacher($id,Request $request){
      $data = [];
      if($request->ajax())
      {
        $allTeacherId = Classes::select('teacher_id','lesson_id')->where(['id'=>$id])->first();
        if($allTeacherId){
          $data['checkifStudent'] = Student::where('class_id',$id)->count();
          $data['checkifAttendenceNotFill'] = StudentAttendance::where('class_id',$id)->count();
          $lesson = Lesson::select('lesson_id')->where('id',$allTeacherId->lesson_id)->first();
          $data['lessonAll']  = explode(',',$lesson->lesson_id);
          $data['allTeacher'] = User::select('id','name')->whereRaw('FIND_IN_SET(id,?)',$allTeacherId->teacher_id)->get();
          return (json_encode($data));
        }else{
          return (json_encode(array('status'=>'success','message'=>"Class Not Found.")));
        }
      }
    }

    public function AttemptAttendence(Request $request){
      if($request->ajax())
        {      
            $rules = array(           
                'start_date'       => 'required',
                'end_date'         => 'required',
                'class_id'         => 'required',
                'teacher_id'       => 'required',
                'lesson_id'        => 'required',
            );
      
            $validator = Validator::make($request->all(), $rules);
        
            if ($validator->fails()) 
            {
                $failedRules = $validator->getMessageBag()->toArray();
                $errorMsg = "";           
                if(isset($failedRules['lesson_id']))
                $errorMsg = $failedRules['lesson_id'][0] . "\n";
                if(isset($failedRules['teacher_id']))
                $errorMsg = $failedRules['teacher_id'][0] . "\n";
                if(isset($failedRules['class_id']))
                $errorMsg = $failedRules['class_id'][0] . "\n";  
                if(isset($failedRules['start_date']))
                  $errorMsg = $failedRules['start_date'][0] . "\n";    
                if(isset($failedRules['end_date']))
                  $errorMsg = $failedRules['end_date'][0] . "\n";   
                return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
            }

            $countAtt = StudentAttendance::where('class_id',$request->class_id)->count();
            if($countAtt){
              return (json_encode(array('status'=>'error','message'=>'Attendence Already Filled.'. "\n" ))) ;
            }

            // Here we update the Class
            $classs = Classes::where('id',$request->class_id)->update(['start_date'=>$request->start_date,'end_date'=>$request->end_date]);
            $res = Student::select(DB::raw('user_id as student_id'),DB::raw('"'.$request->school_id.'" as school_id'),DB::raw('"'.$request->teacher_id.'" as teacher_id'),DB::raw('"'.$request->class_id.'" as class_id'),DB::raw('"'.$request->lesson_id.'" as lesson_id'))->where('class_id',$request->class_id);
            if($res){
              $Attendencedata = $res->get()->toArray();
              $res = StudentAttendance::insert($Attendencedata);
              if($res){
                return (json_encode(array('status'=>'success','message'=>'Attendence created successfully'. "\n" ))) ;
              }
            } else {
              return (json_encode(array('status'=>'error','message'=>'Somthing Went Wrong'. "\n" )));
            }
        }
    }
}
