<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\LocationLicensee;
use App\Model\TerritoryLicensee;
use App\Model\School;
use App\Model\Teacher;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use App\Model\Role;
use App\Model\Modules;
use App\Model\Route;
use App\Model\Module_Assignment;
use App\Model\User_Restriction_Permission;
use App\Model\FileUpload;
use Helper;

class TeacherController extends Controller
{
  public $Settings = ['country_field' => true, 'tl_field' => true, 'll_field' => true, 'school_field' => true];
    /*
  |--------------------------------------------------------------------------
  | TeacherController
  |--------------------------------------------------------------------------
  |
  | Here is where you can register new Teacher for your application. These
  | controller used to add,edit, delete Teacher with show all Teacher in data
  | table.
  |
  */

    /*
   * Create a new controller instance.
   *
   * @return void
  */
  public function __construct()
  {
      $this->middleware('isAdmin');
  }
    
  public function index()
  {
      $user = Auth::user();
      if($user->user_type=='1')
      {
        $this->Settings['country_field'] = true;
        $this->Settings['tl_field']      = true;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='2')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='3')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = false;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='4')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = false;
        $this->Settings['school_field']  = false;
      }
      return view("admin.teacher.index",['Settings'=>$this->Settings]);
  }

  public function show(Request $request)
    {
      try
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $teacheruser_data = new User();
        $teacheruser_data = $teacheruser_data
        ->join('teachers', 'users.id', '=', 'teachers.user_id')
        ->join('schools', 'schools.user_id', '=', 'teachers.school_id')
        ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'users.id as id','users.name as name','users.email as email','users.contact as contact','users.status as status',DB::raw("(select name from sms_users where id = school_id) as school_name"))
        ->where(array('users.user_type'=>5));
        if(Auth::user()->user_type=='2'){
          $teacheruser_data->where('schools.tl_id',Auth::user()->id);
        }
        else if(Auth::user()->user_type=='3'){

          $teacheruser_data->where('schools.ll_id',Auth::user()->id);
        }
        else if(Auth::user()->user_type=='4'){

          $teacheruser_data->where('teachers.school_id',Auth::user()->id);
        }
        $total = $teacheruser_data->count();
        $teacheruser_data = $teacheruser_data->offset($offset)->limit($perpage)->get();
        $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=> $teacheruser_data,'meta'=> $meta));
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }
    }
    public function store(Request $request)
    {               

        if($request->ajax())
        {      
           $rules = array(
          'name'     => 'required',
          'email'     => 'required',
          'contact'     => 'required',
          'country_code'=> 'required',
          'school_id'     => 'required',
          );

           $school_id = $request->school_id;

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
           
            if(isset($failedRules['name']))
                $errorMsg = $failedRules['name'][0] . "\n";
            if(isset($failedRules['email']))
                $errorMsg = $failedRules['email'][0] . "\n";
            if(isset($failedRules['contact']))
                $errorMsg = $failedRules['contact'][0] . "\n";
            if(isset($failedRules['country_code']))
                $errorMsg = $failedRules['country_code'][0] . "\n";
            if(isset($failedRules['state']))
                $errorMsg = $failedRules['state'][0] . "\n";
                $errorMsg = $failedRules['school_id'][0] . "\n";
            
        
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{
           $id=$request->id;
              $check=User::where(['email'=>$request->email,'contact'=>$request->contact])
                ->where(function($query) use ($id){
                    if(isset($id) && $id != ""){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();

              if($check){
                  return (json_encode(array('status'=>'error','message'=>"Email or Contact already exist." )));
                }


              if($request->id){
                $teacher_user = User::find($request->id);
              }else{
                $teacher_user = new User();
                $teacher_user->password = Hash::make($request->password);
              }
        }


        $teacher_user->name = $request->name;
        $teacher_user->email = $request->email;
        $teacher_user->contact = $request->contact;
        $teacher_user->country_code = $request->country_code;
        $teacher_user->user_type = 5;

        try {

                 if($teacher_user->save()){
                  if($request->id){
                    $teacher_id = Teacher::where(['user_id'=>$request->id])->first();
                    $teacher_detail = Teacher::find($teacher_id->id);
                  }else{
                    $teacher_detail = new Teacher();
                  }
                 
                  $teacher_detail->user_id = $teacher_user->id;
                  $teacher_detail->school_id = $school_id;

                  if($teacher_detail->save()){
                    return (json_encode(array('status'=>'success','message'=>sprintf('Teacher "%s" successfully saved', $teacher_user->name))));  
                  }else{
                    return (json_encode(array('status'=>'error','message'=>sprintf('Failed to update Teacher "%s"', $teacher_user->name))));
                  }
                 }
                  
                   
          }catch(\Illuminate\Database\QueryException $ex){ 
                   $error_code = $ex->errorInfo[1];
                   if($error_code == 1062){
                    $result = $ex->getMessage();
                    return (json_encode(array('status'=>'error','message'=>$result = $ex->getMessage()))) ;
              }else{
                $result = $ex->getMessage();
                return (json_encode(array('status'=>'error','message'=>$result))) ;
              }
            }
      }      
    }

    public function edit(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try {
          $result = User::join('teachers', 'users.id', '=', 'teachers.user_id')->join('schools', 'schools.user_id', '=', 'teachers.school_id')->where(['users.id'=>$request->id])->select('users.name','users.contact','users.country_code','users.email','users.status','users.id',DB::raw("(select name from sms_users where id = school_id) as school_name"))->first();

        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }

    public function destroy(Request $request)
    {
      if($request->ajax())
      {
        if($request->id) {
          $user = User::find($request->id);
          $user_id = Teacher::where(['user_id'=>$request->id])->first();
          $user_detail = Teacher::find($user_id->id);
          if ($user->delete())
            return (json_encode(array('status'=>'success','message'=>"Teacher Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }          
    }
    // Here is CSV upload of teacher
    public function uploadcsv(Request $request)
    {
        $Data = [];
        $school = Input::get('schoolId');
        $settings = ['schoolId'=>$school];
        if (Input::hasFile('file')) {
          try{
            $path = Input::file('file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get()->toArray();
            $csv_file='';
            $csv_file=time().'.'.$request->file('file')->getClientOriginalExtension();
            $NewFilePath=$request->file('file')->move(storage_path('app/csv'), $csv_file);
            FileUpload::insert(['file_type'=>'teacher','file_name'=>$csv_file,'start_from'=>1,'end_from'=>20,'settings'=>$settings]);
          }catch(QueryException $ex){
            $Data['status'] = 500;
            $Data['errors'] = $ex->getMessage();
            return Response($Data, 500);
          }catch(Exception $e){
            $Data['status'] = 500;
            $Data['errors'] = $e->getMessage();
            return Response($Data, 500);
          }
          $Data['status'] = 200;
          $Data['success'] = 'File Uploaded Successfully.';
        } else {
            $Data['status'] = 300;
            $Data['errors'] = 'Please upload file.';
        }
        return Response($Data, $Data['status']);
    }
}
