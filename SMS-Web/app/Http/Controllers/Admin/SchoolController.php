<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\Countries;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\Model\LocationLicensee;
use App\Model\TerritoryLicensee;
use App\User;
use App\Model\Role;
use App\Model\Modules;
use App\Model\Route;
use App\Model\Module_Assignment;
use App\Model\User_Restriction_Permission;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use App\Model\School;
use Helper;



class SchoolController extends Controller
{

  /*
  |--------------------------------------------------------------------------
  | SchoolController
  |--------------------------------------------------------------------------
  |
  | Here is where you can register new country for your application. These
  | controller used to add,edit, delete country with show all country in data
  | table.
  |
  */
  public $Settings = ['country_field' => true, 'tl_field' => true, 'll_field' => true, 'school_field' => true];


  /**
   * Create a new controller instance.
   *
   * @return void
  */
  public function __construct()
  {
      $this->middleware('isAdmin');
  }
  /*
    * @created : Sept 01, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to view index page of school
    * @params  : None
    * @return  : None
  */
  public function index(){
      $user = Auth::user();
      if($user->user_type=='1')
      {
        $this->Settings['country_field'] = true;
        $this->Settings['tl_field']      = true;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = false;
      }
      if($user->user_type=='2')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = false;
      }
      if($user->user_type=='3')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = false;
        $this->Settings['school_field']  = false;
      }
     return view('admin.school.index',['Settings'=>$this->Settings]);
  }

  /*
    * @created : Sept 01, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to get All schools
    * @params  : None
    * @return  : None
  */
  public function show(Request $request)
  {
    $perpage = $request->datatable['pagination']['perpage'];
    $page = $request->datatable['pagination']['page'];
    $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
    $school_name=(isset($request->datatable['query']['school_name']) && ($request->datatable['query']['school_name']!="") ? $request->datatable['query']['school_name'] : '');
    $school_id = (isset($request->datatable['query']['country_list']) && ($request->datatable['query']['country_list']!="all") ? $request->datatable['query']['country_list'] : '');
    $allschool = new User();
    if($page=='1')
    {
      $offset = 0;
    }
    else {
      $offset = ($page-1)*$perpage;
    }         
    DB::statement(DB::raw('set @rownumber='.$offset.''));
    $allschool = $allschool
    ->join('schools', 'users.id', '=', 'schools.user_id')
    ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'users.id as id','users.name as name','users.email as email','users.contact as contact','users.status as status','schools.tl_id as tl_id','schools.ll_id as ll_id',DB::raw("(select name from sms_users where id = tl_id) as tl_name"),DB::raw("(select name from sms_users where id = ll_id) as ll_name"))
    ->where(array('users.user_type'=>4));
    if(Auth::user()->user_type=='2'){
      $allschool->where('schools.tl_id',Auth::user()->id);
    }
    if(Auth::user()->user_type=='3'){

      $allschool->where('schools.ll_id',Auth::user()->id);
    }
    if($school_name!='')
    {
      $allschool = $allschool->where('users.name',$school_name);
    }
    $allschool = $allschool->orderby('users.id','asc');
    $total = $allschool->count();
    $allschool = $allschool->offset($offset)->limit($perpage)->get();
    $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
    return Response::json(array('data'=> $allschool,'meta'=> $meta));
  }

  public function getallTLs(Request $request) {

      if($request->ajax()){
        $country_id = $request->id;
        if($country_id)
        {
          $allTLs = User::join('territory_licensees', 'users.id', '=', 'territory_licensees.user_id')->where(['territory_licensees.country_id'=>$country_id,'users.status'=>1])->select('users.name','users.id')->get();

          $res = array();
          if(!empty($allTLs))
          {
            foreach($allTLs as $tl)
            {
              $data = array(
                  'id'  => $tl->id,
                  'name'=> $tl->name
              );
              array_push($res,$data);
            }
            echo json_encode($res);
            exit;                   
          }
        }
      }    
    }

    public function getallLLs(Request $request) {      

      if($request->ajax()){
        $tl_id = $request->id;
        if($tl_id)
        {
          $allLLs = User::join('location_licensees', 'users.id', '=', 'location_licensees.user_id')->where(['location_licensees.tl_id'=>$tl_id,'users.status'=>1])->select('users.name','users.id')->get();

          $res = array();
          if(!empty($allLLs))
          {
            foreach($allLLs as $ll)
            {
              $data = array(
                  'id'  => $ll->id,
                  'name'=> $ll->name
              );
              array_push($res,$data);
            }
            echo json_encode($res);
            exit;                   
          }
        }
      }    
    }

    public function store(Request $request)
    {
        if($request->ajax())
        {      
           $rules = array(
          'name'     => 'required',
          'email'     => 'required',
          'contact'     => 'required',
          'country_code'=> 'required',
          'state'     => 'required',
          //'tl_id'     => 'required',
          );

           // Here I have put default value
           $tl_id = $request->tl_id;
           $ll_id = $request->state;

           // Multi User Add
           $user = Auth::user();
           $tl_id = LocationLicensee::where('user_id',$request->state)->first()->tl_id;
           $ll_id = $request->state;
           

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
            if(isset($failedRules['name']))
                $errorMsg = $failedRules['name'][0] . "\n";
            if(isset($failedRules['email']))
                $errorMsg = $failedRules['email'][0] . "\n";
            if(isset($failedRules['contact']))
                $errorMsg = $failedRules['contact'][0] . "\n";
            if(isset($failedRules['country_code']))
                $errorMsg = $failedRules['country_code'][0] . "\n";
            if(isset($failedRules['state']))
                $errorMsg = $failedRules['state'][0] . "\n";
            if(isset($failedRules['tl_id']))
                $errorMsg = $failedRules['tl_id'][0] . "\n";
            
        
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{
           $id=$request->id;
              $check=User::where(['email'=>$request->email,'contact'=>$request->contact])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();

              if($check){
                  return (json_encode(array('status'=>'error','message'=>"Email or Contact already exist." )));
                }


              if($request->id){
                $school_user = User::find($request->id);
              }else{
                $school_user = new User();
                $school_user->password = Hash::make($request->password);
              }
        }


        $school_user->name = $request->name;
        $school_user->email = $request->email;
        $school_user->contact = $request->contact;
        $school_user->country_code = $request->country_code;
        $school_user->user_type = 4;


        try {
                 if($school_user->save()){
                    $school_id = School::where(['user_id'=>$request->id])->first();
                  if($request->id){

                    $school_detail = School::find($school_id->id);
                  }else{
                    $school_detail = new School();
                  }
                 
                  $school_detail->user_id = $school_user->id;
                  /*$school_detail->country_id = $country_id;*/
                  
                  $school_detail->tl_id = $tl_id;
                  $school_detail->ll_id = $ll_id;

                  if($school_detail->save()){
                    return (json_encode(array('status'=>'success','message'=>sprintf('School "%s" successfully saved', $school_user->name))));  
                  }else{
                    return (json_encode(array('status'=>'error','message'=>sprintf('Failed to update School "%s"', $school_user->name))));
                  }
                 }
                  
                   
          }catch(\Illuminate\Database\QueryException $ex){ 
                   $error_code = $ex->errorInfo[1];
                   if($error_code == 1062){
                    $result = $ex->getMessage();
                    return (json_encode(array('status'=>'error','message'=>'School already exist'))) ;
              }else{
                $result = $ex->getMessage();
                return (json_encode(array('status'=>'error','message'=>$result))) ;
              }
            }
      }      
    }

    public function edit(Request $request)
    {
      if($request->ajax())
      {
        $status = "success";
        try {
          $result = User::join('schools', 'users.id', '=', 'schools.user_id')->where(['users.id'=>$request->id])->select('users.name','users.contact','users.country_code','users.email','users.status','users.id','schools.ll_id as ll_id','schools.tl_id as tl_id',DB::raw("(select name from sms_users where id = tl_id) as tl_name"),DB::raw("(select name from sms_users where id = ll_id) as ll_name"))->first();

        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;         
      }
    }

    public function destroy(Request $request)
    {
      // $role_id = Role::where(['role'=>Auth::user()->user_type])->first()->id;      
      // if(\Request::route()->getName() !='' && \Request::route()->getName() == 'location-licensee.destroy'){   
      //   $current_module = 'LLModule';
      // }      
      // $module_id = Modules::select('id')->where(['module_name'=>$current_module])->first()->id;     
      // $route_name_id = Route::select('id')->where(['route_name'=>\Request::route()->getname()])->first()->id;
      // $user_restriction_permission = User_Restriction_Permission::where(['user_id'=>Auth::user()->id])->first();
      // if($user_restriction_permission->restricted_module !='') {
      //   $restricted_modules_array = explode(',',$user_restriction_permission->restricted_module);        
      //   if(in_array($module_id,$restricted_modules_array)) {
      //     return (json_encode(array('status'=>'error','message'=>sprintf('You dont have the permission to delete LL'))));
      //   } 
      // }    
      // if($user_restriction_permission->restricted_route !='') {
      //   $restricted_routes_array = explode(',',$user_restriction_permission->restricted_route);          
      //   if(in_array($route_name_id,$restricted_routes_array)) {          
      //     return (json_encode(array('status'=>'error','message'=>sprintf('You dont have the permission to delete LL'))));
      //   } 
      // }      
      if($request->ajax())
      {
        if($request->id) {
          $user = User::find($request->id);
          $user_id = School::where(['user_id'=>$request->id])->first();
          $user_detail = School::find($user_id->id);
          if ($user->delete() &&  $user_detail->delete())
            return (json_encode(array('status'=>'success','message'=>"School Deleted Successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
      }          
    }
}
