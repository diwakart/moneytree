<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\LocationLicensee;
use App\Model\TerritoryLicensee;
use App\Model\Permission_add;
use App\Model\Modules;
use App\Model\Role;
use App\Model\Routes;
use App\Model\User_Restriction_Module;
use App\Model\Module_Assignment;
use App\Model\School;
use App\Model\Teacher;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use Helper;

class PermissionController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | PermissionController
    |--------------------------------------------------------------------------
    |
    | where you can register roles,routes,modules,permission
    | user restricted permission and all setting of admin panel
    | also restrict for access particular routes.
    |
    */
    
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
      
    public function index()
    {  
        $roles = Role::count('id');
        $routes =  Routes::count('id');
        $modules = Modules::count('id');
        $module_assigned = Module_Assignment::count('id');
        $restricted_routes = User_Restriction_Module::count('id');
        return view("admin.permission.index",compact('roles','routes','modules','restricted_routes','module_assigned'));
    }    

    public function role()
    {        
        return view("admin.permission.role");
    }

    public function module()
    {        
        return view("admin.permission.module");
    }
    
    public function route_permission()
    {
    	$modules = Modules::select('id','module_name','status')->get()->toArray();   	
        return view("admin.permission.routes",compact(['modules']));
    }

    public function module_assignment()
    {        
        return view("admin.permission.module_assignment");
    }

    public function user_restriction_module()
    {
    	$modules = Modules::select('id','module_name','status')->get()->toArray();    	
        return view("admin.permission.user_restriction_module",compact(['modules']));
    }

    public function role_store(Request $request)
    {    	
		if($request->ajax())
		{
			$rules = array(
				'role'     		=> 		'required',				
			);           

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				$failedRules = $validator->getMessageBag()->toArray();
				$errorMsg = "";
				if(isset($failedRules['role']))
				$errorMsg = $failedRules['role'][0] . "\n";				
				return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
			}
			else {
				$id=$request->id;
					$check=Role::where(['role'=>$request->role])
					 ->where(function($query) use ($id){
					     if(isset($id)){
					       $query->where('id' , '<>' ,$id);
					     }
					 })->exists();

					if($check){
					   return (json_encode(array('status'=>'error','message'=>"Role already exist." )));
					}
				if($request->id){					
					$role = Role::find($request->id);
				}else{
					$role = new Role();
				}				
			}

			$role->role = $request->role;			

			try {
			     
				if($role->save()){
					return (json_encode(array('status'=>'success','message'=>sprintf('role successfully saved'))));  
				} 
				else {
					return (json_encode(array('status'=>'error','message'=>sprintf('Failed to update role'))));
				}
			} 
			catch(\Illuminate\Database\QueryException $ex)
			{ 
				$error_code = $ex->errorInfo[1];
				if($error_code == 1062){
					$result = $ex->getMessage();
					return (json_encode(array('status'=>'error','message'=>'Role already exist')));
				} else {
					$result = $ex->getMessage();
					return (json_encode(array('status'=>'error','message'=>$result)));
				}
			}
		}
    }

    public function role_show(Request $request)
    {
		try
		{
			$perpage = $request->datatable['pagination']['perpage'];
			$page = $request->datatable['pagination']['page'];
			$draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
			if($page=='1')
			{
				$offset = 0;
			}
			else{
				$offset = ($page-1)*$perpage;
			}
			$roles_data = new Role();
			DB::statement(DB::raw('set @rownumber='.$offset.''));
			$roles_data = $roles_data
                ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','role');
			$roles_data = $roles_data->orderby('id','asc');
			$total = $roles_data->count();
			$roles_data = $roles_data->offset($offset)->limit($perpage)->get();
			$meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
			return Response::json(array('data'=> $roles_data,'meta'=> $meta));
		}
		catch(\Illuminate\Database\QueryException $ex)
		{
			return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
		}
    }

    public function getRoleDetails(Request $request)
    {       
		if($request->ajax())
		{
			$status = "success";
			try {

				$result = Role::where(['id'=>$request->id])->select('id','role')->first();

			} catch(QueryException $ex){ 
				dd($ex->getMessage());
				$status = "error";
				$result = $ex->getMessage();
			}
			return (json_encode(array('status'=>$status,'message'=>$result )));         
		}
    }
    
    public function role_destroy(Request $request)   
    {
		if($request->ajax())
		{
			if($request->id){
				$Roles = Role::find($request->id);
				if ($Roles->delete())
				return (json_encode(array('status'=>'success','message'=>"Role Deleted Successfully")));
			}
			return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
		}
    }

    public function module_store(Request $request)
    {   	
		if($request->ajax())
		{
			$rules = array(
				'module_name'     	=> 	'required',				
			);           

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				$failedRules = $validator->getMessageBag()->toArray();
				$errorMsg = "";
				if(isset($failedRules['module_name']))
				$errorMsg = $failedRules['module_name'][0] . "\n";				
				return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
			}
			else {
				$id=$request->id;
					$check=Modules::where(['module_name'=>$request->module_name])
					 ->where(function($query) use ($id){
					     if(isset($id)){
					       $query->where('id' , '<>' ,$id);
					     }
					 })->exists();				

				if($check){
					return (json_encode(array('status'=>'error','message'=>sprintf('Module already existed.'))));
				}
				if($request->id){					
						$modules = Modules::find($request->id);
				}else{
					$modules = new Modules();
				}				
			}

			$modules->module_name = $request->module_name;			

			try {
			     
				if($modules->save()){
					return (json_encode(array('status'=>'success','message'=>sprintf('Modules successfully saved'))));  
				} 
				else {
					return (json_encode(array('status'=>'error','message'=>sprintf('Failed to update Module'))));
				}

			} 
			catch(\Illuminate\Database\QueryException $ex)
			{ 
				$error_code = $ex->errorInfo[1];
				if($error_code == 1062){
					$result = $ex->getMessage();
					return (json_encode(array('status'=>'error','message'=>'Module already exist')));
				} else {
					$result = $ex->getMessage();
					return (json_encode(array('status'=>'error','message'=>$result)));
				}
			}
		}
    }

    public function module_show(Request $request)
    {
		try
		{
			$perpage = $request->datatable['pagination']['perpage'];
			$page = $request->datatable['pagination']['page'];
			$draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
			$ll_name=(isset($request->datatable['query']['ll_name']) && ($request->datatable['query']['ll_name']!="") ? $request->datatable['query']['ll_name'] : '');
			
			//$ll_data = [];
			$modules_data = [];
			$modules_data = new Modules();
			if($page=='1')
			{
				$offset = 0;
			}
			else{
				$offset = ($page-1)*$perpage;
			}
			DB::statement(DB::raw('set @rownumber='.$offset.''));
			// $lluser_data = $lluser_data
   //              ->join('location_licensees', 'users.id', '=', 'location_licensees.user_id')->join('countries', 'location_licensees.country_id', '=', 'countries.id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'countries.name as country','users.name','users.email','users.contact','users.id','users.status','location_licensees.location')
   //              ->where(array('users.user_type'=>3));
			// if(Auth::user()->user_type=='2')
			// {
			// 	$lluser_data->where('location_licensees.tl_id',Auth::user()->id);
			// }
			// if($country_id !='')
			// {
			// 	$lluser_data = $lluser_data->where(array('location_licensees.country_id'=>$country_id));
			// }

			// if($ll_name!='')
			// {
			// 	$lluser_data = $lluser_data->where('users.name', 'like', $ll_name.'%');
			// }

			$modules_data = $modules_data
                ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','module_name');

			$modules_data = $modules_data->orderby('id','asc')->offset($offset)->limit($perpage)->get();
			$total = $modules_data->count();
			$meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
			return Response::json(array('data'=> $modules_data,'meta'=> $meta));
		}
		catch(\Illuminate\Database\QueryException $ex)
		{
			return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
		}
    }

    public function module_edit(Request $request)
    {       
		if($request->ajax())
		{
			$status = "success";
			try {

				$result = Modules::where(['id'=>$request->id])->select('id','module_name')->first();

			} catch(QueryException $ex){ 
				dd($ex->getMessage());
				$status = "error";
				$result = $ex->getMessage();
			}
			return (json_encode(array('status'=>$status,'message'=>$result )));         
		}
    }
    
    public function module_destroy(Request $request)   
    {
		if($request->ajax())
		{
			if($request->id){
				$Modules = Modules::find($request->id);
				        
				if ($Modules->delete())
				return (json_encode(array('status'=>'success','message'=>"Modules Deleted Successfully")));
			}
			return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
		}
    }

    public function route_permission_store(Request $request)
    {
    	
		if($request->ajax())
		{
			$rules = array(
				'route_path'     	=> 'required',
				'route_name'     	=> 'required',
				'module_id'   		=> 'required',
			);           

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				$failedRules = $validator->getMessageBag()->toArray();
				$errorMsg = "";
				if(isset($failedRules['route_path']))
				$errorMsg = $failedRules['route_path'][0] . "\n";
				if(isset($failedRules['route_name']))
				    $errorMsg = $failedRules['route_name'][0] . "\n";
				if(isset($failedRules['module_id']))
				    $errorMsg = $failedRules['module_id'][0] . "\n";
				return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
			}
			else {
				$id=$request->id;
				$check=Routes::where(['route_path'=>$request->route_path])
				 ->where(function($query) use ($id){
				     if(isset($id)){
				       $query->where('id' , '<>' ,$id);
				     }
				 })->exists();

				if($check){
				   return (json_encode(array('status'=>'error','message'=>"Route already exist." )));
				}				
				if($request->id){					
						$route = Routes::find($request->id);
				}else{
					$route = new Routes();
				}				
			}

			$route->route_path = $request->route_path;
			$route->route_name = $request->route_name;
			$route->module_id = $request->module_id;

			try {
			     
				if($route->save()){
					return (json_encode(array('status'=>'success','message'=>sprintf('route successfully saved'))));  
				} 
				else {
					return (json_encode(array('status'=>'error','message'=>sprintf('Failed to update route'))));
				}

			} 
			catch(\Illuminate\Database\QueryException $ex)
			{ 
				$error_code = $ex->errorInfo[1];
				if($error_code == 1062){
					$result = $ex->getMessage();
					return (json_encode(array('status'=>'error','message'=>'route already exist')));
				} else {
					$result = $ex->getMessage();
					return (json_encode(array('status'=>'error','message'=>$result)));
				}
			}
		}
    }

    public function route_permission_show(Request $request)
    {
		try
		{
			$perpage = $request->datatable['pagination']['perpage'];
			$page = $request->datatable['pagination']['page'];
			$draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
			$ll_name=(isset($request->datatable['query']['ll_name']) && ($request->datatable['query']['ll_name']!="") ? $request->datatable['query']['ll_name'] : '');
			
			//$ll_data = [];
			$route_data = [];
			$route_data = new Routes();
			if($page=='1')
			{
				$offset = 0;
			}
			else{
				$offset = ($page-1)*$perpage;
			}
			DB::statement(DB::raw('set @rownumber='.$offset.''));
			// $lluser_data = $lluser_data
   //              ->join('location_licensees', 'users.id', '=', 'location_licensees.user_id')->join('countries', 'location_licensees.country_id', '=', 'countries.id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'countries.name as country','users.name','users.email','users.contact','users.id','users.status','location_licensees.location')
   //              ->where(array('users.user_type'=>3));
			// if(Auth::user()->user_type=='2')
			// {
			// 	$lluser_data->where('location_licensees.tl_id',Auth::user()->id);
			// }
			// if($country_id !='')
			// {
			// 	$lluser_data = $lluser_data->where(array('location_licensees.country_id'=>$country_id));
			// }

			// if($ll_name!='')
			// {
			// 	$lluser_data = $lluser_data->where('users.name', 'like', $ll_name.'%');
			// }
			
			// $route_data = $route_data
   //              ->join('routes', 'modules.id', '=', 'routes.id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'modules.module_name as module','id','route_path','route_name','module_id');

			$route_data = $route_data
                ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','route_path','route_name','module_id');

			$route_data = $route_data->orderby('id','asc')->offset($offset)->limit($perpage)->get();
			$total = $route_data->count();
			$meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
			return Response::json(array('data'=> $route_data,'meta'=> $meta));
		}
		catch(\Illuminate\Database\QueryException $ex)
		{
			return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
		}
    }

    public function route_permission_edit(Request $request)
    {       
		if($request->ajax())
		{
			$status = "success";
			try {

				$result = Routes::where(['id'=>$request->id])->select('id','route_path','route_name','module_id')->first();

			} catch(QueryException $ex){ 
				dd($ex->getMessage());
				$status = "error";
				$result = $ex->getMessage();
			}
			return (json_encode(array('status'=>$status,'message'=>$result )));         
		}
    }
    
    public function route_permission_destroy(Request $request)   
    {
		if($request->ajax())
		{
			if($request->id){
				$route = Routes::find($request->id);
				        
				if ($route->delete())
				return (json_encode(array('status'=>'success','message'=>"route Deleted Successfully")));
			}
			return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
		}
    }
       
    
    public function module_assignment_store(Request $request)
    {    	    	
    	
		if($request->ajax())
		{
			$rules = array(
				'role_id'     					=> 'required',
				'module_id'     				=> 'required',				
			);           

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				$failedRules = $validator->getMessageBag()->toArray();
				$errorMsg = "";
				if(isset($failedRules['role_id']))
				$errorMsg = $failedRules['role_id'][0] . "\n";
				if(isset($failedRules['module_id']))
				    $errorMsg = $failedRules['module_id'][0] . "\n";				
				return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
			}
			else {
				$id=$request->id;
				$check=Module_Assignment::where(['role_id'=>$request->role_id])
				 ->where(function($query) use ($id){
				     if(isset($id)){
				       $query->where('id' , '<>' ,$id);
				     }
				 })->exists();

				if($check){
				   return (json_encode(array('status'=>'error','message'=>"Role already exist." )));
				}				
				if($request->id){					
					$module_assignment = Module_Assignment::find($request->id);
				}else{
					$module_assignment = new Module_Assignment();
				}
			}

			$module_assignment->role_id = $request->role_id;
			$module_assignment->module_id = $request->module_id;			

			try {
			     
				if($module_assignment->save()){
					return (json_encode(array('status'=>'success','message'=>sprintf('module_assignment successfully saved'))));  
				} 
				else {
					return (json_encode(array('status'=>'error','message'=>sprintf('Failed to update module_assignment'))));
				}

			} 
			catch(\Illuminate\Database\QueryException $ex)
			{ 
				$error_code = $ex->errorInfo[1];
				if($error_code == 1062){
					$result = $ex->getMessage();
					return (json_encode(array('status'=>'error','message'=>'module_assignment already exist')));
				} else {
					$result = $ex->getMessage();
					return (json_encode(array('status'=>'error','message'=>$result)));
				}
			}
		}
    }

    public function module_assignment_show(Request $request)
    {
		try
		{
			$perpage = $request->datatable['pagination']['perpage'];
			$page = $request->datatable['pagination']['page'];
			$draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
			$ll_name=(isset($request->datatable['query']['ll_name']) && ($request->datatable['query']['ll_name']!="") ? $request->datatable['query']['ll_name'] : '');
			
			//$ll_data = [];
			$module_assignment_data = [];
			$module_assignment_data = new Module_Assignment();
			if($page=='1')
			{
				$offset = 0;
			}
			else{
				$offset = ($page-1)*$perpage;
			}
			DB::statement(DB::raw('set @rownumber='.$offset.''));
			// $lluser_data = $lluser_data
   //              ->join('location_licensees', 'users.id', '=', 'location_licensees.user_id')->join('countries', 'location_licensees.country_id', '=', 'countries.id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'countries.name as country','users.name','users.email','users.contact','users.id','users.status','location_licensees.location')
   //              ->where(array('users.user_type'=>3));
			// if(Auth::user()->user_type=='2')
			// {
			// 	$lluser_data->where('location_licensees.tl_id',Auth::user()->id);
			// }
			// if($country_id !='')
			// {
			// 	$lluser_data = $lluser_data->where(array('location_licensees.country_id'=>$country_id));
			// }

			// if($ll_name!='')
			// {
			// 	$lluser_data = $lluser_data->where('users.name', 'like', $ll_name.'%');
			// }

			$module_assignment_data = $module_assignment_data
                ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','role_id','module_id');

			$module_assignment_data = $module_assignment_data->orderby('id','asc')->offset($offset)->limit($perpage)->get();
			$total = $module_assignment_data->count();
			$meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
			return Response::json(array('data'=> $module_assignment_data,'meta'=> $meta));
		}
		catch(\Illuminate\Database\QueryException $ex)
		{
			return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
		}
    }

    public function module_assignment_edit(Request $request)
    {       
		if($request->ajax())
		{
			$status = "success";
			try {

				$result = Module_Assignment::where(['id'=>$request->id])->select('id','role_id','module_id')->first();

			} catch(QueryException $ex){ 
				dd($ex->getMessage());
				$status = "error";
				$result = $ex->getMessage();
			}
			return (json_encode(array('status'=>$status,'message'=>$result )));         
		}
    }
    
    public function module_assignment_destroy(Request $request)   
    {
		if($request->ajax())
		{
			if($request->id){
				$module_assignment_data = Module_Assignment::find($request->id);
				        
				if ($module_assignment_data->delete())
				return (json_encode(array('status'=>'success','message'=>"module_assignment Deleted Successfully")));
			}
			return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
		}
    }

    public function user_restriction_module_store(Request $request)
    {
    	
		if($request->ajax())
		{
			$rules = array(
				'user_id'     				=> 'required',
				'restricted_module'     	=> 'required',
				'restricted_route'   		=> 'required',
			);           

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				$failedRules = $validator->getMessageBag()->toArray();
				$errorMsg = "";
				if(isset($failedRules['user_id']))
				$errorMsg = $failedRules['user_id'][0] . "\n";
				if(isset($failedRules['restricted_module']))
				    $errorMsg = $failedRules['restricted_module'][0] . "\n";
				if(isset($failedRules['restricted_route']))
				    $errorMsg = $failedRules['restricted_route'][0] . "\n";
				return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
			}
			else {
				$id=$request->id;
				$check=User_Restriction_Module::where(['user_id'=>$request->user_id])
				 ->where(function($query) use ($id){
				     if(isset($id)){
				       $query->where('id' , '<>' ,$id);
				     }
				 })->exists();

				if($check){
				   return (json_encode(array('status'=>'error','message'=>"User already exist." )));
				}
				
				if($request->id){					
					$restriction_module = User_Restriction_Module::find($request->id);
				}else{
					$restriction_module = new User_Restriction_Module();
				}				
			}

			$restriction_module->user_id = $request->user_id;
			$restriction_module->restricted_module = $request->restricted_module;
			$restriction_module->restricted_route = $request->restricted_route;

			try {
			     
				if($restriction_module->save()){
					return (json_encode(array('status'=>'success','message'=>sprintf('Restriction successfully saved'))));  
				} 
				else {
					return (json_encode(array('status'=>'error','message'=>sprintf('Failed to update Restriction'))));
				}

			} 
			catch(\Illuminate\Database\QueryException $ex)
			{ 
				$error_code = $ex->errorInfo[1];
				if($error_code == 1062){
					$result = $ex->getMessage();
					return (json_encode(array('status'=>'error','message'=>'Restriction already exist')));
				} else {
					$result = $ex->getMessage();
					return (json_encode(array('status'=>'error','message'=>$result)));
				}
			}
		}
    }

    public function user_restriction_module_show(Request $request)
    {
		try
		{
			$perpage = $request->datatable['pagination']['perpage'];
			$page = $request->datatable['pagination']['page'];
			$draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
			$ll_name=(isset($request->datatable['query']['ll_name']) && ($request->datatable['query']['ll_name']!="") ? $request->datatable['query']['ll_name'] : '');
			
			//$ll_data = [];
			$restriction_module = [];
			$restriction_module = new User_Restriction_Module();
			if($page=='1')
			{
				$offset = 0;
			}
			else{
				$offset = ($page-1)*$perpage;
			}
			DB::statement(DB::raw('set @rownumber='.$offset.''));
			// $lluser_data = $lluser_data
   //              ->join('location_licensees', 'users.id', '=', 'location_licensees.user_id')->join('countries', 'location_licensees.country_id', '=', 'countries.id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'countries.name as country','users.name','users.email','users.contact','users.id','users.status','location_licensees.location')
   //              ->where(array('users.user_type'=>3));
			// if(Auth::user()->user_type=='2')
			// {
			// 	$lluser_data->where('location_licensees.tl_id',Auth::user()->id);
			// }
			// if($country_id !='')
			// {
			// 	$lluser_data = $lluser_data->where(array('location_licensees.country_id'=>$country_id));
			// }

			// if($ll_name!='')
			// {
			// 	$lluser_data = $lluser_data->where('users.name', 'like', $ll_name.'%');
			// }

			$restriction_module = $restriction_module
                ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','user_id','restricted_module','restricted_route');

			$restriction_module = $restriction_module->orderby('id','asc')->offset($offset)->limit($perpage)->get();
			$total = $restriction_module->count();
			$meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
			return Response::json(array('data'=> $restriction_module,'meta'=> $meta));
		}
		catch(\Illuminate\Database\QueryException $ex)
		{
			return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
		}
    }

    public function user_restriction_module_edit(Request $request)
    {       
		if($request->ajax())
		{
			$status = "success";
			try {

				$result = User_Restriction_Module::where(['id'=>$request->id])->select('id','user_id','restricted_module','restricted_route')->first();

			} catch(QueryException $ex){ 
				dd($ex->getMessage());
				$status = "error";
				$result = $ex->getMessage();
			}
			return (json_encode(array('status'=>$status,'message'=>$result )));         
		}
    }
    
    public function user_restriction_module_destroy(Request $request)   
    {
		if($request->ajax())
		{
			if($request->id){
				$restriction_module = User_Restriction_Module::find($request->id);
				        
				if ($restriction_module->delete())
				return (json_encode(array('status'=>'success','message'=>"Restriction Deleted Successfully")));
			}
			return (json_encode(array('status'=>'error','message'=>"No Such Record is Found!!!")));
		}
    }     
}
