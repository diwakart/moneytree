<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Classes;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Model\LocationLicensee;
use App\Model\TerritoryLicensee;
use App\Model\School;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use Helper;


class CommonController extends Controller
{
    public function __construct()
    {
    $this->middleware('isAdmin');
    }
    
    public function index()
    {
        echo "adad"; die;
    }
    /*
    * @created : August 27, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to get all TL of specific country
    * @params  : None
    * @return  : None
    */
    public function getallTLs(Request $request)
    {
        try
        {
          if($request->ajax()){
            $country_id = ($request->id != "" ? $request->id : '');
            // Get All country of User
            if($country_id == ""){
              $country_id = Auth::id();
            }

            if($country_id)
            {
              $allTLs = User::join('territory_licensees', 'users.id', '=', 'territory_licensees.user_id')->where(['territory_licensees.country_id'=>$country_id,'users.status'=>1])->select('users.name','users.id')->get();
              
              $res = array();
              if(!empty($allTLs))
              {
                foreach($allTLs as $tl)
                {
                  $data = array(
                    'id'  => $tl->id,
                    'name'=> $tl->name
                  );
                  array_push($res,$data);
                }
              
                echo json_encode($res);
                exit;
              }
            }
          }
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
          return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
        }   
    }
    
    /*
    * @created : August 30, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to get all LL of specific TL.
    * @params  : \Illuminate\Http\Request  
    * @return  : \Illuminate\Http\Response 
    */
    public function getallLLs(Request $request)
    {
      if($request->ajax())
      {
        $tl_id = ($request->id != "" ? $request->id : '');
        // Get All country of User
        if($tl_id == ""){
          $tl_id = Auth::id();
        }
        if($tl_id)
        {
          $allLLs = User::join('location_licensees', 'users.id', '=', 'location_licensees.user_id')->where(['location_licensees.tl_id'=>$tl_id,'users.status'=>1])->select('users.name','users.id','location_licensees.location')->get();
          $res = array();
          if(!empty($allLLs))
          {
            foreach($allLLs as $ll)
            {
              $data = array(
                  'id'  => $ll->id,
                  'name'=> $ll->name,
                  'location'=> $ll->location
              );
              array_push($res,$data);
            }
            echo json_encode($res);
            exit;
          }
        }
      }
    }
    
    /*
    * @created : August 30, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to get all LL of specific TL.
    * @params  : \Illuminate\Http\Request  
    * @return  : \Illuminate\Http\Response 
    */
    public function getallschools(Request $request) {
          

      if($request->ajax()){
        $ll_id = ($request->id != "" ? $request->id : '');
        // Get All country of User
        if($ll_id == ""){
          $ll_id = Auth::id();
        }

        if($ll_id)
        {
          $allschools = User::join('schools', 'users.id', '=', 'schools.user_id')->where(['schools.ll_id'=>$ll_id,'users.status'=>1])->select('users.name','users.id')->get();
          //print_r($allschools);die;

          $res = array();
          if(!empty($allschools))
          {
            foreach($allschools as $schools)
            {
              $data = array(
                  'id'  => $schools->id,
                  'name'=> $schools->name
              );
              array_push($res,$data);
            }

            echo json_encode($res);
            exit;                   
          }
        }
      }    
    }
    
    public function getAllStudentsOfSchool(Request $request,$id)
    {
        try
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $studentuser_data = new User();
        $studentuser_data = $studentuser_data
        ->join('students', 'users.id', '=', 'students.user_id')
        ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'users.id as id','users.name as name','users.email as email','users.contact as contact','users.status as status')->where('students.school_id',$id)
        ->where(array('users.user_type'=>7));
        $total = $studentuser_data->count();
        $studentuser_data = $studentuser_data->offset($offset)->limit($perpage)->get();
        $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=> $studentuser_data,'meta'=> $meta));
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }
    }

    public function getTecherFromSchool(Request $request,$id)
    {
      $teacheruser_data = new User();
        $teacheruser_data = $teacheruser_data
        ->join('teachers', 'users.id', '=', 'teachers.user_id')
        ->select('users.id as id','users.name as name','users.email as email','users.contact as contact','users.status as status')->where('teachers.school_id',$id)->get();
        if($teacheruser_data){
          $teacheruser_data = $teacheruser_data->toArray();
        }
      return Response::json($teacheruser_data,200);

    }

    public function getLsubL(Request $request){
      $data['level'] = [["name" => "L1", "id"=>"1"],["name" => "L2", "id"=>"2"]];
      $data['Sublevel'] = [["name" => "SubL1", "id"=>"1"],["name" => "SubL2", "id"=>"2"]];
      return Response::json($data,200);
    }

    public function getLesson($level,$sublevel){
      $data['lesson'] = [["name" => "Lesson1", "id"=>"1"],["name" => "Lesson2", "id"=>"2"]];
      return Response::json($data,200);
    }
}
