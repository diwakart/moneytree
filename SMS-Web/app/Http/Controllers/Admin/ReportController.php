<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\LocationLicensee;
use App\Model\TerritoryLicensee;
use App\Model\School;
use App\Model\StudentAttendance;
use App\Model\Report;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use App\Model\Role;
use App\Model\Teacher;
use App\Model\Modules;
use App\Model\Route;
use App\Model\Module_Assignment;
use App\Model\User_Restriction_Permission;
use Helper;

class ReportController extends Controller
{

    /*
  |--------------------------------------------------------------------------
  | TeacherController
  |--------------------------------------------------------------------------
  |
  | Here is where you can register new Teacher for your application. These
  | controller used to add,edit, delete Teacher with show all Teacher in data
  | table.
  |
  */

    /*
   * Create a new controller instance.
   *
   * @return void
  */
  public function __construct()
  {
      $this->middleware('isAdmin');
  }
    
  public function index()
  {
      // $user = Auth::user();
      // if($user->user_type=='2')
      // {
      //   config(['settings.country_field' => 'false']);
      //   config(['settings.tl_field' => 'false']);
      // }
        $userObj = Auth::user();
        $roleId = $userObj->user_type;
        $userId = $userObj->id;
        $school_id = '';
        if($userObj->user_type == 5){
          $school_id = Teacher::select('school_id')->where('user_id',$userId)->first()->school_id;
        }
        return view("admin.student_report.index",compact('roleId','userId','school_id'));
  }

  public function viewReport(Request $request)
  {
    try{
      if(isset($request->Schoolid)){
         $data = StudentAttendance::join('users','users.id','=','student_attendance.school_id')->select('users.name as school_name','school_id as SchoolId','teacher_id',DB::raw('sum(mt_earning) as total_mt_earning'),DB::raw('count(DISTINCT student_id) as student_count'),DB::raw('(SELECT COUNT(attendance) FROM sms_student_attendance WHERE attendance = 1 AND school_id = SchoolId) as TotalPresent'),DB::raw('(((SELECT COUNT(attendance) FROM sms_student_attendance WHERE attendance = 1 AND school_id = SchoolId) * 100) / count(DISTINCT student_id)) as percentage'))->whereRaw('FIND_IN_SET(school_id,?)',$request->Schoolid)->groupBy('school_id')->get()->toArray();
      } 
      if(isset($request->Classid)){
        $data = StudentAttendance::join('class','class.id','=','student_attendance.class_id')->select('class.title as class_name','class.id as classId','student_attendance.teacher_id',DB::raw('sum(mt_earning) as total_mt_earning'),DB::raw('count(DISTINCT sms_student_attendance.student_id) as student_count'),DB::raw('(SELECT COUNT(attendance) FROM sms_student_attendance WHERE attendance = 1 AND class_id = classId) as TotalPresent'),DB::raw('(((SELECT COUNT(attendance) FROM sms_student_attendance WHERE attendance = 1 AND class_id = classId) * 100) / count(DISTINCT sms_student_attendance.student_id)) as percentage'))->whereRaw('FIND_IN_SET(class_id,?)',$request->Classid)->groupBy('class_id')->get()->toArray();
      }
      if(isset($request->Lessonid)){
        $data = StudentAttendance::join('lessons','lessons.id','=','student_attendance.lesson_id')->select('lessons.id as lesson_name','lessons.id as lessonId','student_attendance.lesson_id',DB::raw('sum(mt_earning) as total_mt_earning'),DB::raw('count(DISTINCT sms_student_attendance.student_id) as student_count'),DB::raw('(SELECT COUNT(attendance) FROM sms_student_attendance WHERE attendance = 1 AND lesson_id = lessonId) as TotalPresent'),DB::raw('(((SELECT COUNT(attendance) FROM sms_student_attendance WHERE attendance = 1 AND lesson_id = lessonId) * 100) / count(DISTINCT sms_student_attendance.student_id)) as percentage'))->whereRaw('FIND_IN_SET(sms_student_attendance.lesson_id,?)',$request->Lessonid)->groupBy('student_attendance.lesson_id')->get()->toArray();
      }
      if(isset($request->Teacherid)){
        $data = StudentAttendance::join('users','users.id','=','student_attendance.teacher_id')->select('users.name as teacher_name','users.id as teacherId','student_attendance.teacher_id',DB::raw('sum(mt_earning) as total_mt_earning'),DB::raw('count(DISTINCT sms_student_attendance.student_id) as student_count'),DB::raw('(SELECT COUNT(attendance) FROM sms_student_attendance WHERE attendance = 1 AND teacher_id = teacherId) as TotalPresent'),DB::raw('(((SELECT COUNT(attendance) FROM sms_student_attendance WHERE attendance = 1 AND teacher_id = teacherId) * 100) / count(DISTINCT sms_student_attendance.student_id)) as percentage'))->whereRaw('FIND_IN_SET(teacher_id,?)',$request->Teacherid)->groupBy('teacher_id')->get()->toArray();
      }
      return view("admin.student_report.viewReport",compact(['data']));
      //return Response::json($data,200);
    }
    catch(\Illuminate\Database\QueryException $ex)
    {
      return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    }
  }

  public function show(Request $request)
    {
      try
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $tlname=(isset($request->datatable['query']['tlname']) && ($request->datatable['query']['tlname']!="") ? $request->datatable['query']['tlname'] : '');
        $territory_licensees = new User();
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        DB::statement(DB::raw('set @rownumber='.$offset.''));
        $territory_licensees = $territory_licensees->join('territory_licensees', 'users.id', '=', 'territory_licensees.user_id')->join('countries', 'territory_licensees.country_id', '=', 'countries.id')->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'countries.name as country','users.name','users.email','users.contact','users.id','users.status')->where('users.user_type',2);
        if($tlname!='')
        {
          $territory_licensees = $territory_licensees->where('users.name','like',$tlname.'%');
        }
        $total = $territory_licensees->count();
        $territory_licensees = $territory_licensees->orderby('users.id','asc')->offset($offset)->limit($perpage)->get();
        $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=> $territory_licensees,'meta'=> $meta));
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }
    }

    /*
    * @created : August 30, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to add new TL users.
    * @params  : \Illuminate\Http\Request  
    * @return  : \Illuminate\Http\Response 
    */
    public function store(Request $request)
    {
        if($request->ajax())
      {


        $rules = array(
          'country'     => 'required',
          'name'     => 'required',
          'email'     => 'required',
          'contact'     => 'required',
          'country_code'     => 'required',
          
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
           if(isset($failedRules['country']))
            $errorMsg = $failedRules['country'][0] . "\n";
            if(isset($failedRules['name']))
                $errorMsg = $failedRules['name'][0] . "\n";
            if(isset($failedRules['email']))
                $errorMsg = $failedRules['email'][0] . "\n";
            if(isset($failedRules['contact']))
                $errorMsg = $failedRules['contact'][0] . "\n";
            if(isset($failedRules['country_code']))
                $errorMsg = $failedRules['country_code'][0] . "\n";
           
            
        
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

        }
        else{
           $id=$request->id;
              $check=User::where(['email'=>$request->email,'contact'=>$request->contact])
                ->where(function($query) use ($id){
                    if(isset($id)){
                      $query->where('id' , '<>' ,$id);
                    }
                })->exists();

              if($check){
                  return (json_encode(array('status'=>'error','message'=>"Email or Contact already exist." )));
                }


              if($request->id){
                $tl_user = User::find($request->id);
              }else{
                $tl_user = new User();
                $tl_user->password = Hash::make($request->password);
              }
        }


        $tl_user->name = $request->name;
        $tl_user->email = $request->email;
        $tl_user->contact = $request->contact;
        $tl_user->country_code = $request->country_code;
       
        $tl_user->user_type = 2;


        try {
                 if($tl_user->save()){
                  $tl_id = TerritoryLicensee::where(['user_id'=>$request->id])->first();
                  if($request->id){
                    $tl_detail = TerritoryLicensee::find($tl_id->id);
                    $tl_detail->country_id =$request->country;
                    $tl_detail->user_id =$tl_user->id;
                  }else{
                    $tl_detail = new TerritoryLicensee();
                    $tl_detail->country_id = $request->country;
                    $tl_detail->user_id = $tl_user->id;
                  }
                  if($tl_detail->save()){
                    return (json_encode(array('status'=>'success','message'=>sprintf('Territory Licensee "%s" successfully saved', $tl_user->name))));
                  }
                 }
                   
          }catch(\Illuminate\Database\QueryException $ex){ 
                   $error_code = $ex->errorInfo[1];
                   if($error_code == 1062){
                    $result = $ex->getMessage();
                    return (json_encode(array('status'=>'error','message'=>'Territory Licensee already exist'))) ;
              }else{
                $result = $ex->getMessage();
                return (json_encode(array('status'=>'error','message'=>$result))) ;
              }
            }
      }
    }
}

