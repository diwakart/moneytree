<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\Countries;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\User;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use Helper;



class SchoolController extends Controller
{

  /*
  |--------------------------------------------------------------------------
  | SchoolController
  |--------------------------------------------------------------------------
  |
  | Here is where you can register new country for your application. These
  | controller used to add,edit, delete country with show all country in data
  | table.
  |
  */


  /**
   * Create a new controller instance.
   *
   * @return void
  */
  public function __construct()
  {
      $this->middleware('isAdmin');
  }
  /*
    * @created : Sept 01, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to view index page of school
    * @params  : None
    * @return  : None
  */
  public function index(){
     return view('admin.school.index');
  }

  /*
    * @created : Sept 01, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to get All schools
    * @params  : None
    * @return  : None
  */
  public function show(Request $request)
  {

    $perpage = $request->datatable['pagination']['perpage'];
    $page = $request->datatable['pagination']['page'];
    $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
    $school_name=(isset($request->datatable['query']['school_name']) && ($request->datatable['query']['school_name']!="") ? $request->datatable['query']['school_name'] : '');
    $school_id = (isset($request->datatable['query']['country_list']) && ($request->datatable['query']['country_list']!="all") ? $request->datatable['query']['country_list'] : '');
    $allschool = new User();
    if($page=='1')
    {
      $offset = 0;
    }
    else {
      $offset = ($page-1)*$perpage;
    }         
    DB::statement(DB::raw('set @rownumber='.$offset.''));
    $allschool = $allschool
    ->join('schools', 'users.id', '=', 'schools.user_id')
    ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'users.id as id','users.name as name','users.email as email','users.contact as contact','users.status as status','schools.tl_id as tl_id','schools.country_id as country_id','schools.ll_id as ll_id',DB::raw("(select name from sms_users where id = tl_id) as tl_name"),DB::raw("(select name from sms_countries where id = country_id) as country_name"),DB::raw("(select name from sms_users where id = ll_id) as ll_name"))
    ->where(array('users.user_type'=>4));
    if(Auth::user()->user_type=='2'){
      $allschool->where('schools.tl_id',Auth::user()->id);
    }
    if(Auth::user()->user_type=='3'){

      $allschool->where('schools.ll_id',Auth::user()->id);
    }

    $total = $allschool->count();
    
    if($school_id !='')
    {
      $allschool = $allschool->where('users.country_id',$school_id);
    }
    if($school_name!='')
    {
      $allschool = $allschool->where('users.name',$school_name);
    }
    $total = $allschool->count();
    $allschool = $allschool->orderby('users.id','asc')->offset($offset)->limit($perpage)->get();
    $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
    return Response::json(array('data'=> $allschool,'meta'=> $meta));
  }
}
