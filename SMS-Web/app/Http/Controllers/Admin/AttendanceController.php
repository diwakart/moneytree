<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\LocationLicensee;
use App\Model\TerritoryLicensee;
use App\Model\School;
use App\Model\Lesson;
use App\Model\Teacher;
use App\Model\StudentAttendance;
use App\Model\Classes;
use App\Model\Student;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use App\Model\Role;
use App\Model\Modules;
use App\Model\Route;
use App\Model\Module_Assignment;
use App\Model\User_Restriction_Permission;

class AttendanceController extends Controller
{
  
  public $Settings = ['country_field' => true, 'tl_field' => true, 'll_field' => true, 'school_field' => true];
    /*
  |--------------------------------------------------------------------------
  | TeacherController
  |--------------------------------------------------------------------------
  |
  | Here is where you can register new Teacher for your application. These
  | controller used to add,edit, delete Teacher with show all Teacher in data
  | table.
  |
  */

    /*
   * Create a new controller instance.
   *
   * @return void
  */
  public function __construct()
  {
      $this->middleware('isAdmin');
  }
    
  public function index()
  {
      $user = Auth::user();
      $school_data = array();
      if($user->user_type=='1')
      {
        $this->Settings['country_field'] = true;
        $this->Settings['tl_field']      = true;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='2')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = true;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='3')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = false;
        $this->Settings['school_field']  = true;
      }
      if($user->user_type=='4')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = false;
        $this->Settings['school_field']  = false;
      }
      if($user->user_type=='5')
      {
        $this->Settings['country_field'] = false;
        $this->Settings['tl_field']      = false;
        $this->Settings['ll_field']      = false;
        $this->Settings['school_field']  = false;
        $school_data = DB::table('teachers')->select('school_id')->where('user_id',Auth::user()->id)->first();
      }
      return view("admin.attendance.index",['Settings'=>$this->Settings,'school_data'=>$school_data]);
  }

  public function addAttendance(Request $request)
  {
    $school_id = $request->school_id;
    $class_id =  $request->class_id;
    $teacher_data = User::select('id','name','email')->where('id',$request->teacher_id)->first();
    $class_data = Classes::select('id','title',DB::raw('DATE_FORMAT(start_date, "%e %M") as start_date'),DB::raw("(select name from sms_users where id = school_id) as school_name"),DB::raw("(SELECT COUNT(*) from sms_students where school_id = school_id) as total_students"),DB::raw('DATE_FORMAT(end_date, "%e %M") as end_date'))->where('id',$class_id)->first();
      /*echo "<pre>";
      print_r($class_data); die;*/
    return view("admin.attendance.addAttendance",compact(['class_data','teacher_data']));
  }

  public function DeleteAttendance($class_id, Request $request)
  {
    try{
      $res = StudentAttendance::where('class_id',$class_id)->delete();
      if($res){
        return (json_encode(array('status'=>'success','message'=>'Attendence Deleted Succesfully')));
      } else {
        return (json_encode(array('status'=>'error','message'=>'Somthing Went Wrong!')));
      }
    }catch(\Illuminate\Database\QueryException $ex){
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    }
  }

    public function show(Request $request)
    {
      $perpage = $request->datatable['pagination']['perpage'];
      $page = $request->datatable['pagination']['page'];
      $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
      $student_data = [];
      $all_data = new StudentAttendance();
      if($page=='1')
      {
        $offset = 0;
      }
      else{
        $offset = ($page-1)*$perpage;
      }
      $all_data = $all_data->select('id','school_id','class_id','lesson_id','teacher_id',DB::raw("(select name from sms_users where id = school_id) as school_name"),DB::raw("(select title from sms_class where id = class_id) as title"));
      $total =  $all_data->count();
      $all_data = $all_data->orderby('student_attendance.id','asc')->groupBy('student_attendance.class_id')->offset($offset)->limit($perpage)->get();
      $all_data = json_decode(json_encode($all_data));
      if(!empty($all_data))
      {
        $i = 1;
        foreach ($all_data as $key => $value) {
          $student_data[] = (array)$value;
          $student_data[$key]['S.no'] = $offset+1;
          $student_data[$key]['class_name'] = Classes::find($value->class_id)->title;
          $offset++;
        }
      }
      $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
      return Response::json(array('data'=> $student_data,'meta'=> $meta));
  }

    public function getAllClassChild($id,Request $request)
    {
      try
      {
        $perpage = $request->datatable['pagination']['perpage'];
        $page = $request->datatable['pagination']['page'];
        $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
        $child_data = new Student();
        if($page=='1')
        {
          $offset = 0;
        }
        else{
          $offset = ($page-1)*$perpage;
        }
        DB::statement(DB::raw('set @rownumber='.$offset.''));
       $child_data = $child_data        
        ->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'students.id','users.name','users.contact','users.id as child_id','sa.mt_earning','sa.attendance')->join('users','users.id','=','students.user_id')->join('student_attendance as sa', function($join)
            {
                $join->on('sa.student_id', '=', 'students.user_id');
                $join->on('sa.class_id','=', 'students.class_id');
            })
        ->where('students.class_id',$id);
        $total = $child_data->count();
        $child_data = $child_data->offset($offset)->limit($perpage)->get();
        
        $meta = ['draw'=>$draw,'perpage'=>$perpage,'total'=>$total,'page'=>$page];
        return Response::json(array('data'=> $child_data,'meta'=> $meta));
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }
    }

    public function submit_attendance(Request $request)
    {
      try
      {
        if($request->ajax())
        {
          $rules = array(
            'class_id'      => 'required',
            'school_id'     => 'required',
            'lesson_id'     => 'required',
            'teacher_id'  => 'required',
          );
          
          $validator = Validator::make($request->all(), $rules);

          if ($validator->fails()) {
            $failedRules = $validator->getMessageBag()->toArray();
            $errorMsg = "";
            if(isset($failedRules['class_id']))
              $errorMsg = $failedRules['class_id'][0] . "\n";
            if(isset($failedRules['school_id']))
              $errorMsg = $failedRules['school_id'][0] . "\n";
            if(isset($failedRules['lesson_id']))
              $errorMsg = $failedRules['lesson_id'][0] . "\n";
            if(isset($failedRules['teacher_id']))
              $errorMsg = $failedRules['teacher_id'][0] . "\n";

            return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;

          }
          else{

            $SelectedStudentArr = [];
            $student_data = $request->student_data;
            $class_id = $request->class_id;
            foreach ($student_data as $value) {
              // Get all checked Attendence and put into array
              array_push($SelectedStudentArr,$value['id']);
              StudentAttendance::where('student_id',$value['id'])->where('class_id',$class_id)->update(['attendance'=>1,'mt_earning'=>$value['mt_points']]); 
            }

            // This is for when attendence update from existing.
            if(isset($request->OldSelectedUser) && $request->OldSelectedUser != "") {
              $OldSelectedUser = explode(',', $request->OldSelectedUser);
              $AbsentUserArr = array_diff($OldSelectedUser,$SelectedStudentArr);
              if($AbsentUserArr){
                foreach ($AbsentUserArr as $key => $value) {
                  StudentAttendance::where('student_id',$value)->where('class_id',$class_id)->update(['attendance'=>2,'mt_earning'=>0]); 
                }
              }
            }
            return (json_encode(array('status'=>'success','message'=>'Attendance And Mt_Point Updated Succesfully')));
          }
        }
      }
      catch(\Illuminate\Database\QueryException $ex)
      {
        return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
      }
    }
}
