<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\Countries;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\User;
use Redirect;
use Session;
use Response;
use Validator;
use Storage;
use DB;
use Auth;
use Helper;



class CountriesController extends Controller
{

  /*
  |--------------------------------------------------------------------------
  | CountriesController
  |--------------------------------------------------------------------------
  |
  | Here is where you can register new country for your application. These
  | controller used to add,edit, delete country with show all country in data
  | table.
  |
  */


  /**
   * Create a new controller instance.
   *
   * @return void
  */
  public function __construct()
  {
      $this->middleware('isAdmin');
  }
  /*
    * @created : August 27, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to view index page of country
    * @params  : None
    * @return  : None
  */
  public function index(){
     return view('admin.countries.index');
  }

  /*
    * @created : August 27, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to add new country in database
    * @params  : None
    * @return  : None
  */

  public function store(Request $request)
  {
    try 
    {
      if($request->id){
        $countries = Countries::find($request->id);
        $id = $request->id;
        if($request->name!='')
        {
          $check_name=Countries::where(['name'=>$request->name])
            ->where(function($query) use ($id){
                if(isset($id)){
                  $query->where('id' , '<>' ,$id);
                }
            })->exists();
          if($check_name)
          {
            return (json_encode(array('status'=>'error','message'=>'Country name already exist'))) ;
          }
          $countries->name = $request->name;
        }
        if($request->code!='')
        {
          $check_code=Countries::where(['code'=>$request->code])
            ->where(function($query) use ($id){
                if(isset($id)){
                  $query->where('id' , '<>' ,$id);
                }
            })->exists();
          if($check_code)
          {
            return (json_encode(array('status'=>'error','message'=>'Country short code already exist'))) ;
          }
          $countries->code = $request->code;
        }
        if($request->country_code!='')
        {
          $country_code=Countries::where(['country_code'=>$request->country_code])
            ->where(function($query) use ($id){
                if(isset($id)){
                  $query->where('id' , '<>' ,$id);
                }
            })->exists();
          if($country_code)
          {
            return (json_encode(array('status'=>'error','message'=>'Country Code already exist'))) ;
          }
          $countries->country_code = $request->country_code;
        }
        try {
          $countries->save();
          return (json_encode(array('status'=>'success','message'=>sprintf('Country "%s" successfully updated', $countries->name))));
        }catch(\Illuminate\Database\QueryException $ex){
          $error_code = $ex->errorInfo[1];
          if($error_code == 1062){
            $result = $ex->getMessage();
            return (json_encode(array('status'=>'error','message'=>'Country already exist'))) ;
          }
        }

      }
      else
      {
        $rules = array(
          'name'     => 'required|unique:countries',
          'code'    =>  'required|unique:countries',
          'country_code'    =>  'required|unique:countries',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) 
        {
          $failedRules = $validator->getMessageBag()->toArray();
          $errorMsg = "";
          if(isset($failedRules['name']))
            $errorMsg = $failedRules['name'][0] . "\n";
          if(isset($failedRules['code']))
            $errorMsg = $failedRules['code'][0] . "\n";
          if(isset($failedRules['country_code']))
            $errorMsg = $failedRules['country_code'][0] . "\n";
        
          return (json_encode(array('status'=>'error','message'=>$errorMsg. "\n" ))) ;
        }
        else
        {
          $countries = new Countries();
          $countries->name = $request->name;
          $countries->code = $request->code;
          $countries->country_code = $request->country_code;
          try 
          {
            $countries->save();
            return (json_encode(array('status'=>'success','message'=>sprintf('Country "%s" successfully saved', $countries->name))));
          }
          catch(\Illuminate\Database\QueryException $ex)
          {
            $error_code = $ex->errorInfo[1];
            if($error_code == 1062)
            {
              $result = $ex->getMessage();
              return (json_encode(array('status'=>'error','message'=>'Country already exist'))) ;
            }
          }
        }
      }
    }
    catch(\Illuminate\Database\QueryException $ex)
    {
      return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    }
  }
  
  /*
    * @created : August 27, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to edit country
    * @params  : None
    * @return  : None
  */
   public function edit(Request $request)
  {
    try
    {
      if($request->ajax())
      {
        $status = "success";

        try { 
          $result = Countries::select('code','name','country_code','id')->where(['id'=>$request->id])->first();

        } catch(QueryException $ex){ 
          dd($ex->getMessage());
          $status = "error";
          $result = $ex->getMessage();
        }
        return (json_encode(array('status'=>$status,'message'=>$result ))) ;
      }
    }
    catch(\Illuminate\Database\QueryException $ex)
    {
      return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    }
  }

  /*
    * @created : August 28, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to get All countries
    * @params  : None
    * @return  : None
  */
  public function getAllCountry(Request $request)
  {
    try
    {
      $perpage = $request->datatable['pagination']['perpage'];
      $page = $request->datatable['pagination']['page'];
      $draw=(isset($request->datatable['pagination']['draw']) && ($request->datatable['pagination']['draw']!="") ? $request->datatable['pagination']['draw'] : '1');
      $country_name=(isset($request->datatable['query']['country_name']) && ($request->datatable['query']['country_name']!="") ? $request->datatable['query']['country_name'] : '');
      if($page=='1')
      {
        $offset = 0;
      }
      else{
        $offset = ($page-1)*$perpage;
      }
      $countries = new Countries();
      DB::statement(DB::raw('set @rownumber='.$offset.''));
      $countries = $countries->select(DB::raw('@rownumber:=@rownumber+1 as S_No'),'id','code','name','country_code','default_country');
      if($country_name!='')
      {
        $countries = $countries->where('name','like',$country_name.'%');
      }
      $total = $countries->count();
      $countries = $countries->orderby('name','asc')->offset($offset)->limit($perpage)->get();
      $meta = ['perpage'=>$perpage,'total'=>$total,'page'=>$page];
      return Response::json(array('data'=>$countries,'meta'=>$meta));
    }
    catch(\Illuminate\Database\QueryException $ex)
    {
      return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    }
  }

  /*
    * @created : August 27, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to change status of specific country
    * @params  : None
    * @return  : None
  */
  public function activate(Request $request)
  {
    try
    {
      if($request->ajax())
      {
        if($request->id){
          $country = Countries::find($request->id);
          $country->default_country=1;
          
          if ($country->save())
            return (json_encode(array('status'=>'success','message'=>"Country activated successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No such record found!!!")));
      }
    }
    catch(\Illuminate\Database\QueryException $ex)
    {
      return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    }
  }

  /*
    * @created : August 27, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to delete specific country
    * @params  : None
    * @return  : None
  */
   public function destroy(Request $request)
  {
    try
    {
      if($request->ajax())
      {
        if($request->id){
          $country = Countries::find($request->id);
          $country_id = Countries::where(['id'=>$request->id])->first();
          
          if ($country->delete() )
            return (json_encode(array('status'=>'success','message'=>"Country deleted successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"No such record found!!!")));
      }
    }
    catch(\Illuminate\Database\QueryException $ex)
    {
      return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    }
  }

  /*
    * @created : August 27, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to change status of specific country
    * @params  : None
    * @return  : None
  */
  public function deactivate(Request $request)
  {
    try
    {
      if($request->ajax())
      {
        if($request->id){
          $country = Countries::find($request->id);
            $country->default_country=0;
          
          if ($country->save())
            return (json_encode(array('status'=>'success','message'=>"Country deactivated successfully")));
        }
        return (json_encode(array('status'=>'error','message'=>"no such record found!!!")));
      }
    }
    catch(\Illuminate\Database\QueryException $ex)
    {
      return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    }
  }

  /*
    * @created : August 27, 2018
    * @author  : Anup Singh
    * @access  : public
    * @Purpose : This function is use to get all TL of specific country
    * @params  : None
    * @return  : None
  */
  public function getallTLs(Request $request)
  {
    try
    {
      if($request->ajax()){
        $country_id = $request->id;
        if($country_id)
        {
          $allTLs = User::join('territory_licensees', 'users.id', '=', 'territory_licensees.user_id')->where(['territory_licensees.country_id'=>$country_id,'users.status'=>1])->select('users.name','users.id')->get();
          
          $res = array();
          if(!empty($allTLs))
          {
            foreach($allTLs as $tl)
            {
              $data = array(
                'id'  => $tl->id,
                'name'=> $tl->name
              );
              array_push($res,$data);
            }
          
            echo json_encode($res);
            exit;
          }
        }
      }
    }
    catch(\Illuminate\Database\QueryException $ex)
    {
      return (json_encode(array('status'=>'error','message'=>$ex->getMessage()))) ;
    }   
  }
}
