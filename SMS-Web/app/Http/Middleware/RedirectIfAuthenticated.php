<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if(Auth::user()->user_type == '1' || Auth::user()->user_type == '2' || Auth::user()->user_type == '3' || Auth::user()->user_type == '4' || Auth::user()->user_type == '5' || Auth::user()->user_type == '6'){
              return redirect('/home');
            }else if(Auth::user()->user_type == '' || Auth::user()->user_type == NULL){
              return redirect('/');
            }
        }

        return $next($request);
    }
}
