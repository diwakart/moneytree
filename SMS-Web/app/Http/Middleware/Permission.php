<?php

namespace App\Http\Middleware;

use Closure;
use Helper;
use Request;
use Auth;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check())
        {
            if(Auth::user()->user_type == '1'){
                return $next($request);
            }else{
                $RouteName = Request::route()->getname();
                $res = Helper::CanIf($RouteName);
                if($res){
                  return $next($request);
                }else{
                  \Session::flash('message', "Access Not Allow!");
                  return redirect('/');
                }
            }
        }
        \Session::flash('message', "Please Login First");
        return redirect('/');
    }
}
