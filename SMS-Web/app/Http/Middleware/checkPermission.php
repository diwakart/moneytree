<?php 

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Model\Role;
use App\Model\Modules;
use App\Model\Routes;
use App\Model\Module_Assignment;
use App\Model\User_Restriction_Permission;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Facades\URL;
use DB;
use Helper;
use Request;

    /**
     * Handle an incoming request.*/
class checkPermission {


    /* *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check())
        {
            if(Auth::user()->user_type == '1'){
                return $next($request);
            }else{
                $RouteName = Request::route()->getname();
                //$res = Helper::CanIf($RouteName);
                $res = true;
                if($res){
                  return $next($request);
                }else{
                  \Session::flash('message', "Access Not Allow!");
                  return redirect('/');
                }
                // $user = Auth::user();
                // $role_data = Role::select('role')->where(['role'=>$user->user_type])->first();
                // $role_id = $user->user_type;
                // if(!empty($role_data))
                // {
                //     $role_id = $role_data->role;
                // }
                // $module_assigned = Module_Assignment::select('module_id')->where(['role_id'=>$role_id])->get();    
                // $route_data = Routes::select('id','module_id')->where(['route_name'=>\Request::route()->getname()])->first();   
                // //print_r($route_data); die;
                // $user_restriction_permission = DB::table('user_restriction_permission')->where(['user_id'=>$user->id ])->first(); 
                // print_r($user_restriction_permission); die;
                // if(!empty($user_restriction_permission))
                // {
                //     if($user_restriction_permission->restricted_module !='') {
                //       $restricted_modules_array = explode(',',$user_restriction_permission->restricted_module);      
                //       if(in_array($route_data->module_id,$restricted_modules_array)) {
                //             \Session::flash('message', "Not Permitted");
                //             return redirect('/');
                //       } else {
                //         return $next($request);
                //       }
                //     }
                //     if($user_restriction_permission->restricted_route !='') {
                //       $restricted_routes_array = explode(',',$user_restriction_permission->restricted_route);        
                //       if(in_array($route_data->id,$restricted_routes_array)) {
                //         return Redirect::back();
                //       } else {
                //         return $next($request);
                //       }
                //     }
                // }
                // else
                // {
                //     return $next($request);
                // }
            }
        }
        \Session::flash('message', "Please Login First");
        return redirect('/');
    }
}
