<?php 

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Contracts\Routing\ResponseFactory;

    /**
     * Handle an incoming request.*/
class isAdmin {


    /* *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if ( Auth::check())
        {
            if(Auth::user()->user_type == '1' || Auth::user()->user_type == '2' || Auth::user()->user_type == '3' || Auth::user()->user_type == '4' || Auth::user()->user_type == '5' || Auth::user()->user_type == '6'){
                return $next($request);
            }
        }
        \Session::flash('message', "Please Login First");
        return redirect('/');
    }
}
