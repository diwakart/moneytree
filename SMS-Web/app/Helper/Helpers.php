<?php
namespace App\Helper;
use App\Model\Module_Assignment;
use App\Model\User_Restriction_Module;
use App\Model\Routes;
use App\Model\Modules;
use Auth;
use Session;

class Helpers {
    public static function CanIf($route) {
        return true;
        if(Auth::user()->user_type == 1){
            return true;
        }
        $allAllowRoutes = Session::get('AllAccessRoutes');
        if(empty($allAllowRoutes)){
            if(Auth::user()){
                $accessRoutes = Helpers::AllAccessRoutes();
                Session::put('AllAccessRoutes',  $accessRoutes);
                $allAllowRoutes = Session::get('AllAccessRoutes');
            }
        }
        $res = in_array($route,$allAllowRoutes);
        if($res)
            return true;
        else
            return false;
        // $routeDetails = Routes::where('route_name',$route)->first();
        // if($routeDetails){
        //     // Here we get Module Id and Route Id
        //     $moduleId = $routeDetails->module_id;
        //     $routeId = $routeDetails->id;
        //     if($moduleId && $routeId){
        //         // Here we check Module_Assignment Table
        //         $CheckInModuleAssign = Module_Assignment::where('role_id',Auth::user()->user_type)->whereRaw('FIND_IN_SET(?,module_id)',$moduleId)->count();
        //         $flag = false;
        //         if(!$CheckInModuleAssign)
        //             return false;
        //         else
        //             $flag = true;

        //         $moduleCheck = Modules::find($moduleId)->status;
        //         if($moduleCheck){
        //             // Here we get current user restrict Module and route
        //             $permission = User_Restriction_Module::where('user_id',Auth::id())->first();
        //             if($permission){
        //                 // Passed route Module Check
        //                 $RestCheckMod = User_Restriction_Module::where('user_id',Auth::id())->whereRaw('FIND_IN_SET(?,restricted_module)',$moduleId)->count();
        //                 if($RestCheckMod){
        //                     return false;
        //                 }else{
        //                     // Passed route Check
        //                     $RestCheckRoute = User_Restriction_Module::where('user_id',Auth::id())->whereRaw('FIND_IN_SET(?,restricted_route)',$routeId)->count();
        //                     if($RestCheckRoute)
        //                         return false;
        //                     else
        //                         return true;
        //                 }       
        //             } else {
        //                 if($flag)
        //                     return true;
        //                 else
        //                     return false;
        //             }
        //         } else {
        //             if($flag)
        //                 return true;
        //             else
        //                 return false;
        //         }
        //     } else {
        //         return false;
        //     }
        // } else {
        //     return false;
        // }
    }

    // Function for return all access route for login user
    public static function AllAccessRoutes() {
        $GlobleRestrictArr = [];
        $allModule = [];
        $restrictedRoute = [];

        // Get login user object
        $user = Auth::user();
        if($user){
            // Get login user all module assign ID
            $allModule = Module_Assignment::where('role_id',$user->user_type)->first();
            if($allModule){
                $allModule = explode(",",$allModule->module_id);
                // Get All Restricted Module for login user
                $User_Restrt = User_Restriction_Module::where('user_id',$user->id);
                if($User_Restrt->count()){
                    // Convert into Array
                    $restrictedModule = explode(",",$User_Restrt->first()->restricted_module);
                    $restrictedRoute = explode(",",$User_Restrt->first()->restricted_route);
                    // remove all restricted module from all allow module
                    $allModule = array_diff($allModule,$restrictedModule);
                }
                // Here we get all the route name
                $data = Routes::select('route_name')->whereRaw('FIND_IN_SET(module_id,?)',implode(',',$allModule))->
                            whereRaw('NOT FIND_IN_SET(id,?)',implode(',',$restrictedRoute))->get();
                // Make a loop and put all route name into GlobleRestrict Array
                if($data){
                    foreach ($data->toArray() as $key => $value) {
                        if($value['route_name']){
                            $GlobleRestrictArr[]=$value['route_name'];
                        }
                    }
                }
            }
        }
        // Here we return the all allow routes for user.
        return $GlobleRestrictArr;
    }
}
?>