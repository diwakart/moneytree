<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentTag extends Model
{
    public $table = 'student_tag';
}
