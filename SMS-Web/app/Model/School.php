<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    public $table = "schools";

    function GetUserDetail() {
	  return $this->hasOne('App\User','id','user_id');
	}
}
