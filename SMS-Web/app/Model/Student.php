<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public $table = "students";

    function GetUserDetail() {
	  return $this->hasOne('App\User','id','user_id');
	}
}
