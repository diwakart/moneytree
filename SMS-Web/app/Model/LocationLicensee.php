<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LocationLicensee extends Model
{
     public $table = "location_licensees";

    function GetUserDetail() {
	  return $this->hasOne('App\User','id','user_id');
	}
}
