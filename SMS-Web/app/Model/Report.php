<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public $table = "reports";

    function GetUserDetail() {
	  return $this->hasOne('App\User','id','user_id');
	}
}
