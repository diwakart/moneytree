<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FileUpload extends Model
{
	public $table = "cron_file_uploads";
}
