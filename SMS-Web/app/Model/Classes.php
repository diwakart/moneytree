<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
	public $table = "class";

	

	function country() {
		return $this->hasOne('App\Model\TerritoryLicensee','user_id','tl_id');
	}

	function TL() {
		return $this->hasOne('App\User','id','tl_id');
	}

	function LL() {
		return $this->hasOne('App\User','id','ll_id');
	}

	function School() {
		return $this->hasOne('App\User','id','school_id');
	}

	function Teacher() {
		return $this->hasOne('App\User','id','teacher_id');
	}

	function Lesson() {
		return $this->hasMany('App\Model\Lesson','id','with_ids');
	}

	

	
}
