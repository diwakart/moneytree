<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TerritoryLicensee extends Model {

     public $table = "territory_licensees";

    function GetUserDetail() {
	  return $this->hasOne('App\User','id','user_id');
	}

}
