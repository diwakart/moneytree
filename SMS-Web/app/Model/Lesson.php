<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model {

    public $table = "lessons";

    function GetUserDetail() {
	  return $this->hasOne('App\User','id','user_id');
	}

}
