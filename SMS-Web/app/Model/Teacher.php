<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
     public $table = "teachers";

    function GetUserDetail() {
	  return $this->hasOne('App\User','id','user_id');
	}
}
