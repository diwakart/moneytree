<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentAttendance extends Model
{
   public $table = "student_attendance";
 	
 	protected $casts = [
    	'lesson_id' => 'array'
    ];

    protected $fillable = [
        'school_id', 'student_id', 'class_id','lesson_id','attendance','mt_earning'
    ];

 	function GetAllLesson() {
		return $this->hasMany('App\Model\Classes','lesson_id','lesson_id');
	}  
}
