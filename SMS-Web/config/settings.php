<?php
    /*
    |--------------------------------------------------------------------------
    | Application Setting for permission
    |--------------------------------------------------------------------------
    |
    | This value is the setting of application for permission where admin
    | set which input is available or not.
    |
    */
    return [

            'country_field'     => 'true',
            'tl_field'          => 'true',
            'll_field'          => 'true',
            'school_field'      => 'true',
    ];
?>